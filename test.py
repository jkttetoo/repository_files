import datetime
import serial, time, platform


def convertToHex(x):
    HexVal = hex(int(len(x) /2))
    HexVal = str(HexVal[2:]).upper()
    if(len(HexVal) == 1):
        HexVal = str('0'+HexVal)
    return HexVal

def HextoDec(x):
    DecVal = int(x,16)
    return DecVal

def DectoHex(x):
    HexVal2 = hex(x).split('x')[-1].upper()
    return HexVal2

print(DectoHex(1234568555))

dataCSIM = 'D15F82028381068B51407F16000000000000283D027000003815163912120000015F36E58D814E1CD1324A3DE06725EDF61A37C184EE8AC6339B1C1015A3BC04119B33946D535422119BB5B868695DB34DD15F82028381068B51407F16000000000000283D027000003815163912120000015F36E58D814E1CD1324A3DE06725EDF61A37C184EE8AC6339B1C1015A3BC04119B33946D535422119BB5B868695DB34D'
if(HextoDec(convertToHex(dataCSIM[4:])) >= HextoDec('7F')):
    dataCSIM = dataCSIM.replace(str(dataCSIM[2:4]), '81' + str(convertToHex(dataCSIM[4:])), 1)
print(dataCSIM[2:4])
print(dataCSIM)
#dataCSIM = str(list_csim[17]+convertToHex(dataCSIM)+dataCSIM)
    
def IntervalPerCase():
    time.sleep(0)

def CSIM(apdu):
    apdu = apdu.replace(' ','')
    CSIMCommand(apdu,'_outAUTO_',gui)

def envCSIM(dataCSIM):
    if(dataCSIM[2:4] is '81'):
        dataCSIM = dataCSIM.replace(str(dataCSIM[4:6]), str(convertToHex(dataCSIM[6:])), 1)
    else:
        dataCSIM = dataCSIM.replace(str(dataCSIM[2:4]), str(convertToHex(dataCSIM[4:])), 1)
    dataCSIM = str(list_csim[17]+convertToHex(dataCSIM)+dataCSIM)
    CSIM(dataCSIM)


def logTitlex(lt):
    LogFiles('_outAUTO_','# ============================\n')
    LogFiles('_outAUTO_','# '+str(lt)+' =\n')
    LogFiles('_outAUTO_','# ============================\n')

def logTitle(lt):
    print('# ============================\n')
    print('# '+str(lt)+' =\n')
    print('# ============================\n')


def test_Date():   
    dt = datetime.datetime.now()
    logTitle(dt)

def test_DK4801():
    IntervalPerCase()
    logTitle('DK-4801')
#     Check Connectivity
#    APDU_PSM(list_csim[15])
#    CSIM('00 A4 0 0 04 02 3F 0 0')
#    assert (csim_result[0][-4:] == '9000' or csim_result[0][-4:-2] == '91')
#    envCSIM('D14F8202838106'+SMSCProfile2+'8B4140'+OAProfile2+'7F16000000000000282D027000002815163912120000012F18897B550314C3560E64E71088731C5362CFF05B8C0A84E51C99479A166D92')
#    assert (csim_result[0][-4:] == '9000' or csim_result[0][-4:-2] == '91')    

start_time = datetime.datetime.now()
test_Date()
test_DK4801()
print('Duration: {}'.format(datetime.datetime.now() - start_time))

