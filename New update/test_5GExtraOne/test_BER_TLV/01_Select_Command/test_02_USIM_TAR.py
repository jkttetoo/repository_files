import pytest
import datetime
import configparser
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *

gui = False
config = configparser.ConfigParser()
config.read("./ServerConfiguration.ini")
PiPort = '/dev/ttyACM0'
openSerial()

# Detail Envelope Server on ServerConfiguration.ini
OAProfile1 = config.get("Profile1", "OA_Number")
OAProfile2 = config.get("Profile2", "OA_Number")
SMSCProfile1 = config.get("Profile1", "SMSC")
SMSCProfile2 = config.get("Profile2", "SMSC")
SPI = config.get("SPIKICKID", "SPI")
KIC = config.get("SPIKICKID", "KIC")
KID = config.get("SPIKICKID", "KID")
KiC_Key = config.get("SPIKICKID", "KiC_Key")
KiD_Key = config.get("SPIKICKID", "KiD_Key")
# Note: If the Initial Condition process is failed, please do manually (perso the card via PCOM)

def IntervalPerCase():
    time.sleep(30)
    
def test_Date():
    """Create Datetime"""
    dt = datetime.datetime.now()
    logTitle(dt)

def test_EO9909():
    """(USIM TAR) Select BER TLV FIle"""
    IntervalPerCase()
    logTitle('EO-9909')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000005
    #Data: 0020000A08933F57845F706921 00A40000023F00 00A40000027FF1 00A40000021088 00C0000026
    envCSIM('D1678202838106069126180100008B59400D91261892285626F17F160000000000002845027000004015162115153F00019962DA33AF7716D1F2A35A228DFB4DF8F4E03A8195D13329668DFFCB79CF61AE7B098B3F8CD9E9FD92B6C60A8275D97EDC43AAFDA990ACFD')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')

def test_EO9913():
    """(USIM TAR) Select an invalidated BER TLV FIle"""
    IntervalPerCase()
    logTitle('EO-9913')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000006
    #Data: 0020000A08933F57845F706921 00A40000023F00 00A40000027FF1 00A40000021088 0004000000 00A40000021088 00C0000026
    envCSIM('D1778202838106069126180100008B69400D91261892285626F17F160000000000002855027000005015162115153F0001E499563902CED965CD2E3D349F18DE55D560602361831780F93FC7A9783029E5508D215981935FB1CA493D7AC2BF9867F079A60F9DD6E2AD02A5D6EF85B6C0D0AC44ABAB46AA3E47')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')

def test_EO9914():
    """(USIM TAR) Select an rehabilitated BER TLV FIle"""
    IntervalPerCase()
    logTitle('EO-9914')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000007
    #Data: 0020000A08933F57845F706921 00A40000023F00 00A40000027FF1 00A40000021088 0044000000 00A40000021088 00C0000026
    envCSIM('D1778202838106069126180100008B69400D91261892285626F17F160000000000002855027000005015162115153F0001F2C3580D648E518324D1C41FF7BD494FA9E22DCFEE277D8381FAF301612754ECF85019A10BC4C3DF47B7555BA912DF577B2FF57357BA92518CC0B04A3DBBB20657752554E2FFAFE9')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    
