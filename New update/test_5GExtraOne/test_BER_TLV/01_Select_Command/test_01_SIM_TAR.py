import pytest
import datetime
import configparser
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *


config = configparser.ConfigParser()
config.read("./ServerConfiguration.ini")
PiPort = '/dev/ttyACM0'
openSerial()

# Detail Envelope Server on ServerConfiguration.ini
OAProfile1 = config.get("Profile1", "OA_Number")
OAProfile2 = config.get("Profile2", "OA_Number")
SMSCProfile1 = config.get("Profile1", "SMSC")
SMSCProfile2 = config.get("Profile2", "SMSC")
SPI = config.get("SPIKICKID", "SPI")
KIC = config.get("SPIKICKID", "KIC")
KID = config.get("SPIKICKID", "KID")
KiC_Key = config.get("SPIKICKID", "KiC_Key")
KiD_Key = config.get("SPIKICKID", "KiD_Key")
# Note: If the Initial Condition process is failed, please do manually (perso the card via PCOM)

def IntervalPerCase():
    time.sleep(30)

@pytest.mark.skip(reason="This scenario is not tested")
def test_Date():
    """Create Datetime"""
    dt = datetime.datetime.now()
    logTitle(dt)

@pytest.mark.skip(reason="SIM TAR is not available")
def test_EO9910():
    """(SIM TAR) Select BER TLV FIle"""
    IntervalPerCase()
    logTitle('EO-9910')
    #SPI KIC KID: 1621 15 15
    #TAR: 
    #Counter: 0000000000
    #Data: A020000008933F57845F706921 A0A40000023F00 A0A40000027FF1 A0A40000021088 A004000000 A0A40000021088 A0C0000026
    envCSIM('')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')

@pytest.mark.skip(reason="SIM TAR is not available")
def test_EO9911():
    """(SIM TAR) Select an invalidated BER TLV FIle"""
    IntervalPerCase()
    logTitle('EO-9911')
    #SPI KIC KID: 1621 15 15
    #TAR: 
    #Counter: 0000000001
    #Data: A020000008933F57845F706921 A0A40000023F00 A0A40000027FF1 A0A40000021088 A004000000 A0A40000021088 A0C0000026
    envCSIM('')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')

@pytest.mark.skip(reason="SIM TAR is not available")
def test_EO9912():
    """(SIM TAR) Select an rehabilitated BER TLV FIle"""
    IntervalPerCase()
    logTitle('EO-9912')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000002
    #Data: A020000008933F57845F706921 A0A40000023F00 A0A40000027FF1 A0A40000021088 A044000000 A0A40000021088 A0C0000026
    envCSIM('')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    
