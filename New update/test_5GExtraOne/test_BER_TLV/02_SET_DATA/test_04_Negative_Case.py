import pytest
import datetime
import configparser
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *


config = configparser.ConfigParser()
config.read("./ServerConfiguration.ini")
PiPort = '/dev/ttyACM0'
openSerial()

# Detail Envelope Server on ServerConfiguration.ini
OAProfile1 = config.get("Profile1", "OA_Number")
OAProfile2 = config.get("Profile2", "OA_Number")
SMSCProfile1 = config.get("Profile1", "SMSC")
SMSCProfile2 = config.get("Profile2", "SMSC")
SPI = config.get("SPIKICKID", "SPI")
KIC = config.get("SPIKICKID", "KIC")
KID = config.get("SPIKICKID", "KID")
KiC_Key = config.get("SPIKICKID", "KiC_Key")
KiD_Key = config.get("SPIKICKID", "KiD_Key")
# Note: If the Initial Condition process is failed, please do manually (perso the card via PCOM)

def IntervalPerCase():
    time.sleep(30)
    
def test_Date():
    """Create Datetime"""
    dt = datetime.datetime.now()
    logTitle(dt)

def test_EO9962():
    """SET DATA with wrong P1"""
    IntervalPerCase()
    logTitle('EO-9962')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000070
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880DBFF800B8A09010203040506070809
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001E2BDFA669F532A14F84E1EDAE4776E37F0CA1EBA508F0E8EFA4FF095C0B158F07A275DE3E52DCB630EA8BD0C14F96A185D5B76C531A56E2F7FC97EB2EF570207')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    time.sleep(10)
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000071
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880CB0080018A00C0000000
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F000181D4E82951A54F55AC8AD5366FE0056C2033334E669F64BCF1CF40ABE59A9B91BF2B008FA07F2E9ED7E1B203D0D164B53861544B114B40BECBC7889DD6CBC021')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')

def test_EO9963():
    """SET DATA with wrong P2"""
    IntervalPerCase()
    logTitle('EO-9963')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000072
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880DB00FF0B8A09010203040506070809
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001F3E23D69271FAC513B3FD27FB6F42B0F6587B44B8689E1251413E75D4788C2A55C4125E715218E0A48D5FFD560ABBBB37B8FBDB0C2BB261A8B643431E8EFC263')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    time.sleep(10)
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000073
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880CB0080018A00C0000000
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F000144A6CF7C391B3DAC8F9ADAE3C8226006D34586BCA740B44F4AC53387379F0AC51E97DCDC0373B5956CD53409122266DB6C2332040E452C0AB2DE09228ED97EC1')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')    

def test_EO9964():
    """SET DATA with wrong P3"""
    IntervalPerCase()
    logTitle('EO-9964')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000074
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880DB0080018A09010203040506070809    
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F000137A3C1AF007494C065D5453CA3DD75177FEFEA286F8C006F8608CB990E3362A03DA9442950599511394C1A7245F88E051F489C4B17CA1CFD6C74DDA2589C4806')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    time.sleep(10)
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000075
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880CB0080018A00C0000000 
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001B9B11881DB9F9B44DD80FEB262D1314D0FE1655FD91EB31D3D6F84EBC3F040F9AB2632B9E8ECAFBE68DDAC70ED34D16CFC56477AB627F8EC0F0D4282138E85E7')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')

def test_EO9965():
    """SET DATA with wrong CLASS"""
    IntervalPerCase()
    logTitle('EO-9965')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000076
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A40000021088A0DB00800B8A09010203040506070809    
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001CC3BD45019F5A670A81B861AF90591C71ED71D8B5552EF595B5FE9E246DC9295919D7E466E7C8DFE04BCB640EC28A9118F7C947E25F9D759362DAF3B53456D5A')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    time.sleep(10)
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000077
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880CB0080018A00C0000000 
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001A73D5D64A0C25E4E53B7BB66F720687DC4405CCCA5CDD80DE612523909FF6F5DAA4CF65B3B0EB18ED76894E5F2A48ED8751054A03192F1239E96A1BE36B7F525')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
