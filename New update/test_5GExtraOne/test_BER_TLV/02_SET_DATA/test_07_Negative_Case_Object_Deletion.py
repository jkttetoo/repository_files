import pytest
import datetime
import configparser
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *


config = configparser.ConfigParser()
config.read("./ServerConfiguration.ini")
PiPort = '/dev/ttyACM0'
openSerial()

# Detail Envelope Server on ServerConfiguration.ini
OAProfile1 = config.get("Profile1", "OA_Number")
OAProfile2 = config.get("Profile2", "OA_Number")
SMSCProfile1 = config.get("Profile1", "SMSC")
SMSCProfile2 = config.get("Profile2", "SMSC")
SPI = config.get("SPIKICKID", "SPI")
KIC = config.get("SPIKICKID", "KIC")
KID = config.get("SPIKICKID", "KID")
KiC_Key = config.get("SPIKICKID", "KiC_Key")
KiD_Key = config.get("SPIKICKID", "KiD_Key")
# Note: If the Initial Condition process is failed, please do manually (perso the card via PCOM)

def IntervalPerCase():
    time.sleep(30)
    
def test_Date():
    """Create Datetime"""
    dt = datetime.datetime.now()
    logTitle(dt)

def test_EO9976():
    """Pre-condition for Object Deletion Test Suite"""
    IntervalPerCase()
    logTitle('EO-9976')
    CSIM('0020000A08933F57845F706921')
    assert (csim_result[0][-4:] == '9000')
    CSIM('00A40000023F00')
    assert (csim_result[0][-4:-2] == '61' or csim_result[0][-4:-2] == '90')
    CSIM('00A40000027FF1')
    assert (csim_result[0][-4:-2] == '61' or csim_result[0][-4:-2] == '90')
    CSIM('00A40000021088')
    assert (csim_result[0][-4:-2] == '61' or csim_result[0][-4:-2] == '90')
    CSIM('80 DB 00 80 0B 82 09 01 01 01 01 01 01 01 01 01')
    assert (csim_result[0][-4:] == '9000')
    CSIM('80 DB 00 80 0B 83 09 02 02 02 02 02 02 02 02 02')
    assert (csim_result[0][-4:] == '9000')    

def test_EO9977():
    """Delete an Object with wrong tag format"""
    IntervalPerCase()
    logTitle('EO-9977')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 00000000A0
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880DB00800B9F22
    envCSIM('D1678202838106069126180100008B59400D91261892285626F17F160000000000002845027000004015162115153F0001D1E7DC16C04E2549F6A612C4342B403DEED2F94E2E53923CA9C886B0C970E3ABF757D808BA734015CA39A04F1E52FF667F3BAF83AA8A2325')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    time.sleep(10)
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 00000000A1
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880CB0080018200C0000000
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001E43227B051B03862655015ECCD522C1711174B7A3CED54A1D90109DCB6192903BE36B0B058B7CD7743EA0A1B9B63E4A55B705B19E6BEF654B0604311789AE4D3')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    time.sleep(10)
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 00000000A2
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880CB0080018300C0000000
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001F306822230041E99D15B14192C0E7B673C1D2B4D18FBC36CA63E86278914271582FB4469C4B2BC3B21ADFC08B789541B6EC0BFF7ED7211080849ECC91BB36698')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')

def test_EO9978():
    """Delete 2 objects in one command"""
    IntervalPerCase()
    logTitle('EO-9978')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 00000000A4
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880DB0080028283
    envCSIM('D1678202838106069126180100008B59400D91261892285626F17F160000000000002845027000004015162115153F0001CBD3F992376D31E01D2FEE52F3AC26E8CC2B6A8EB9E741F3E4E2047D022F0298E97F8E31741E1A1DA8D363F60B9740ACBDC6B752D652CB7B')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    time.sleep(10)
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 00000000A5
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880CB0080018200C0000000
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001D763C1A0D6633544B600F2939F2FB972B4B1441D172FEDE0BCEC9193CFB7BE8FCAD438B6726DF293A77AEC10AD4EE223F6817C5FA025A1A4D628B34BB8B1AFB2')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
    time.sleep(10)
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 00000000A6
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880CB0080018300C0000000
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F0001BE551BF1E1C81E342581D7800E3402DB24EAB650D94E55D9F7379A69525DD2C3CE5AC32B2395617720E4859C4B614E6F6BD4D82D5B1C3A1680FFC025A8547E4C')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
