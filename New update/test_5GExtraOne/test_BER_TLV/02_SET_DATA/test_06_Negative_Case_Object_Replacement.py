import pytest
import datetime
import configparser
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *


config = configparser.ConfigParser()
config.read("./ServerConfiguration.ini")
PiPort = '/dev/ttyACM0'
openSerial()

# Detail Envelope Server on ServerConfiguration.ini
OAProfile1 = config.get("Profile1", "OA_Number")
OAProfile2 = config.get("Profile2", "OA_Number")
SMSCProfile1 = config.get("Profile1", "SMSC")
SMSCProfile2 = config.get("Profile2", "SMSC")
SPI = config.get("SPIKICKID", "SPI")
KIC = config.get("SPIKICKID", "KIC")
KID = config.get("SPIKICKID", "KID")
KiC_Key = config.get("SPIKICKID", "KiC_Key")
KiD_Key = config.get("SPIKICKID", "KiD_Key")
# Note: If the Initial Condition process is failed, please do manually (perso the card via PCOM)

def IntervalPerCase():
    time.sleep(30)
    
def test_Date():
    """Create Datetime"""
    dt = datetime.datetime.now()
    logTitle(dt)

def test_EO9973():
    """Pre-condition for Object Replacement Test Suite"""
    IntervalPerCase()
    logTitle('EO-9973')
    CSIM('0020000A08933F57845F706921')
    assert (csim_result[0][-4:] == '9000')
    CSIM('00A40000023F00')
    assert (csim_result[0][-4:-2] == '61' or csim_result[0][-4:-2] == '90')
    CSIM('00A40000027FF1')
    assert (csim_result[0][-4:-2] == '61' or csim_result[0][-4:-2] == '90')
    CSIM('00A40000021088')
    assert (csim_result[0][-4:-2] == '61' or csim_result[0][-4:-2] == '90')
    CSIM('80 DB 00 80 0B 81 09 01 02 03 04 05 06 07 08 09')
    assert (csim_result[0][-4:] == '9000')

def test_EO9974():
    """Replace an Object with no value"""
    IntervalPerCase()
    logTitle('EO-9974')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000092
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880DB0080028100
    envCSIM('D1678202838106069126180100008B59400D91261892285626F17F160000000000002845027000004015162115153F000103F1AF86779212EB774862E192311E6093EAE15CEE8455339785B6012E99DB848FD730FD63B7AD984F0E1408D421FEEFFBD29E90C075AC65')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')

def test_EO9975():
    """Replace an Object with wrong length (value > length)"""
    IntervalPerCase()
    logTitle('EO-9975')
    #SPI KIC KID: 1621 15 15
    #TAR: 3F0001
    #Counter: 0000000094
    #Data: 0020000A08933F57845F70692100A40000023F0000A40000027FF100A4000002108880DB00800B8101010101010101010101
    envCSIM('D16F8202838106069126180100008B61400D91261892285626F17F16000000000000284D027000004815162115153F000177DD81458C92B2E6FA6CAF441AB2B0B373FCFDEF982A20B0A4A8F7B5BDDB8CBDBD9EF593720A16C050AF6AC0B8CA6FA51F9FA6D6F60B125326C30B851B643AF8')
    assert (csim_result[0][-4:] == '915E' or csim_result[0][-4:-2] == '91')
