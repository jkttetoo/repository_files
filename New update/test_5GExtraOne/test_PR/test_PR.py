import pytest
import datetime
import configparser
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *

gui = False
config = configparser.ConfigParser()
config.read("./ServerConfiguration.ini")
PiPort = '/dev/ttyACM0'
openSerial()

# Detail Envelope Server on ServerConfiguration.ini
OAProfile1 = config.get("Profile1", "OA_Number")
OAProfile2 = config.get("Profile2", "OA_Number")
SMSCProfile1 = config.get("Profile1", "SMSC")
SMSCProfile2 = config.get("Profile2", "SMSC")
SPI = config.get("SPIKICKID", "SPI")
KIC = config.get("SPIKICKID", "KIC")
KID = config.get("SPIKICKID", "KID")
KiC_Key = config.get("SPIKICKID", "KiC_Key")
KiD_Key = config.get("SPIKICKID", "KiD_Key")
# Note: If the Initial Condition process is failed, please do manually (perso the card via PCOM)

def IntervalPerCase():
    time.sleep(30)
    
def test_Date():
    """Create Datetime"""
    dt = datetime.datetime.now()
    logTitle(dt)

def test_PR():
    """(USIM TAR) Select BER TLV FIle"""
    logTitle('PR')
    CSIM('8012000029 12 D02781030140018202818235010339020200470908696E7465726E65743C0302480C3E0521052707F5')
    CSIM('8014000017 8103014001020282810301003802810035010339020200')    
    CSIM('801200004E 12 D04C8103014301820281213641160303003C010000380303000000000000000000000000000000000000008080808080808080808080808000000A00AE008C008B00B0002C010000050001000101')
    CSIM('801400000F 8103014301020282810301003701FF')
    CSIM('8012000013 12 D0118103012700820281822401022503000003')
    CSIM('801400000F 810301270002028281030100240102')
    CSIM('80C2000010 D60E190109020282813802810037013A')

    
