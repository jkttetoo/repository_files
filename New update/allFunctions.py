import serial, time, platform
import allLists as al
import PySimpleGUI as sg

#=================================================================#
#=================================================================#
#Create All Functions                                             #
#=================================================================#
#=================================================================#

#define variable
global val_SMS
global lenview
global val_Profiles
global aid
global csim_result

val_SMS = []
val_Profiles = []
aid = []
csim_result = []


# Function to try connect to SerialPort ttyACM0
def openSerial():
    global SERIALPORT
    global BAUDRATE
    global ser
    global connStatus
    
    ser = serial.Serial()
    if(platform.system() is 'Windows'):
        ser.port = "COM19"
        ser.baudrate = 115200
    else:
        ser.port = "/dev/ttyACM0"
        ser.baudrate = 19200
    ser.bytesize = 8        #number of bits per bytes
    ser.parity = 'N'        #set parity check: no parity
    ser.stopbits = 1        #number of stop bits
    ser.timeout = None      #timeout block read
    ser.xonxoff = False     #disable software flow control
    ser.rtscts = False      #disable hardware (RTS/CTS) flow control
    ser.dsrdtr = False      #disable hardware (DSR/DTR) flow control
    ser.writeTimeout = 0    #timeout for write
    ser.open()
    connStatus = ser.isOpen()
    
    
#Call Function for Check Port Connection
def check_PortConnection():
    if(connStatus is False):
        openSerial()
    try:
        if(connStatus is True):
            sg.Popup('Port successfully opened', title=' ', button_type='POPUP_BUTTONS_OK')
        else:
            sg.Popup('Cannot open serial port')
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Check Connection ERROR', button_type='POPUP_BUTTONS_ERROR')
    

def closeSerial():
    if(connStatus is False):
        openSerial()
#    ser.flushInput()    #flush input buffer, discarding all its contents
#    ser.flushOutput()   #flush output buffer, aborting current output    
    ser.close()

#Call Function for Decode Readline
def decodeReadline(val):
    global response
    response = val.decode()
    response = response.replace('\r','')
    response = response.replace('\n','')
    
            
#Call Function for Check Modem Connection
def connect():
    if(connStatus is False):
        openSerial()    
    try:
        temp = ('AT\r').encode()
        ser.write(temp)
        time.sleep(0.5)
        while True:
            response = ser.readline()
            if ('OK' in str(response)):
                sg.Popup('Modem successfully opened', title=' ')
                break
            elif ('ERROR' in str(response)):
                sg.Popup('Modem cannot opened', title=' ')
                break
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Connection ERROR', button_type='POPUP_BUTTONS_ERROR')


#Call Function for Check SIM PIN
def checkPIN():
    global statusSIM
    global statusPIN
    txt = []
    
    if(connStatus is False):
        openSerial()
    try:
        temp = ('AT+CPIN?\r').encode()
        ser.write(temp)
        time.sleep(0.5)
        while True:
            decodeReadline(ser.readline())
            txt.append(response)            
            if('OK' in str(response)):
                if('+CPIN: READY' in str(txt)):
                    statusSIM = True
                    statusPIN = True
                else:
                    statusPIN = False
                    sg.Popup('SIM PIN is Not Ready \nPlease Input SIM PIN First', title=' ', auto_close=True, auto_close_duration=5)
                break
            elif('ERROR' in str(response)):
                statusSIM = False
                sg.Popup('SIM Card is Not Ready \nPlease Insert SIM Card First', title=' ', auto_close=True, auto_close_duration=5)
                break

    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Check PIN ERROR', button_type='POPUP_BUTTONS_ERROR')
    

#Call Function to write all results to LogFiles
def LogFiles(dest,val):
    if('Manual' in dest):
        file = 'ManualLog.txt'
    elif('_outCall_' in dest):
        file = 'Call&USSDLog.txt'
    elif('_outGSA_' in dest):
        file = 'GenericSIMAccessLog.txt'
    elif('_outDKT_' in dest):
        file = 'DakotaLog.txt'
    elif('_outAUTO_' in dest):
        file = 'AutomationLog.txt'        
    else:
        file = 'ObtanaLog.txt'
    if(platform.system() is 'Windows'):
#     For Windows platform        
        LogFile = open('LogResults/'+str(file),'a')
    else:
#     For Raspberry Pi3 without auto start
#        LogFile = open('LogResults/'+str(file),'a')        
# =================================================
#     For Raspberry Pi3 with auto start
        LogFile = open('/home/pi/Documents/ATCA/LogResults/'+str(file),'a')
# =================================================    
    LogFile.write(str(val))
    LogFile.close()

#Call Function for manual input
def response_manual(values):
    if(connStatus is False):
        openSerial()
    try:
        if(values is '' or values is ' '):
            sg.PopupOK('Please Input the AT Command', title=' ')
        else:
            temp = (values + '\r').encode()
            ser.write(temp)
#             if('AT+CMGS' in values):
#                 sg.Popup(sg.Text('Input Message', size=(15, 1)),
#                          sg.InputText(size=(50,1), key='_inMessManual_'),
#                          sg.Button('Response', key='_bResManual_'))
            print("write data: ", values)
            LogFiles('Manual','write data: ' + values +'\n')
            time.sleep(0.5)
            while True:
                try:
                    decodeReadline(ser.readline())
                    if(response is not ''):
                        print('read data: ', response)
                        LogFiles('Manual','read data: ' + response +'\n')
                    if ('OK' in str(response)):
                        print(' ')
                        LogFiles('Manual','\n')
                        break
                    elif ('ERROR' in str(response)):
                        print(' ')
                        LogFiles('Manual','\n')
                        break                    
                except Exception as e:
                    print('Error: ')
                    break
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Manual Input ERROR', button_type='POPUP_BUTTONS_ERROR')


#Call Function for Snapshot
def snapshot():
    temp_txt = []
    txt = []
    i = 1
    x = -1
    cmd = al.list_snapshot
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if((statusSIM is True) and (statusPIN is True)):
            for i in range(len(cmd)):
                temp = (cmd[i] + '\r').encode()
                ser.write(temp)
                time.sleep(0.5)
                while True:
                    decodeReadline(ser.readline())
                    temp_txt.append(response)
                    if ('OK' in str(response)):
                        break
                    elif('ERROR' in str(response)):
                        temp_txt[1] = '-'
                        break
                x = temp_txt[1].find(': ')
                if(x is -1):
                    txt.append(temp_txt[1])
                else:
                    x += 2
                    txt.append(temp_txt[1][x:])
                temp_txt.clear()
                progress_bar.UpdateBar(i+1, len(cmd))
            al.UpdateSnapshot(txt)                          
        
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Snapshot ERROR', button_type='POPUP_BUTTONS_ERROR')
    
        
#Call Function for SMS
def send_sms(num, message):
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            temp_num = ('AT+CMGS="'+ num + '"\r').encode()
            ser.write(temp_num)
            time.sleep(0.5)
            temp_mes = (message + '\x1A\r').encode()
            ser.write(temp_mes)       
            sg.Popup('Destination : '+num+'\n'+'Message :'+message+'\n'+'Status : Sent', title=' ')

    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Send SMS ERROR', button_type='POPUP_BUTTONS_ERROR')


#Call Function to read SMSC
def SMSC():
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            temp = ('AT+CSCA?\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('OK' in str(response)):
                    window.Element('_inSMSC_').Update(txt[1].split('"')[1])
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('Error read SMSC')
                    break                

    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Send SMS ERROR', button_type='POPUP_BUTTONS_ERROR')
    
    
#Call Function to set SMSC
def setSMSC(x):
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            temp = ('AT+CSCA="'+str(x)+'",145\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                if ('OK' in str(response)):
                    sg.Popup('SMSC updated')
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('Error update SMSC')
                    break                

    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Send SMS ERROR', button_type='POPUP_BUTTONS_ERROR')

    
# ------------------ Create a table ------------------
def table(num_rows, val_view):
    data = [j for j in range(num_rows)]
    data[0] = ['ID','   Status   ','  Source ','','Date In',' Clock In ','                        Message                        ']

    if(len(val_view) is 0):
        for i in range(num_rows-1):
            data[i+1] = ['','','','','','','']
    elif(len(val_view) > 0):
        for i in range(num_rows-1):
            data[i+1]=[val_view[i][j].replace('"','') for j in range(7)]
            if(len(data[i+1][6]) > 80 and len(data[i+1][6]) < 161):
                data[i+1][6] = data[i+1][6][0:80]+'\n'+data[i+1][6][80:160]
            elif(len(data[i+1][6]) > 160 and len(data[i+1][6]) < 240):
                data[i+1][6] = data[i+1][6][0:80]+'\n'+data[i+1][6][80:160]+'\n'+data[i+1][6][160:240]
    return data

val_view = []
TabledataSMS = table(num_rows=4, val_view=val_view)
TableheadingsSMS = [TabledataSMS[0][x] for x in range(len(TabledataSMS[0]))]

#Call Function to view all SMS
def view_sms():
    txt = []
    y = 1
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            temp = ('AT+CMGL="ALL"\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('OK' in str(response)):
                    lenview = len(txt)
                    if(lenview > 4):
                        lenview = int((lenview-2) / 3)
                        for x in range(lenview):
                            txt[y] = txt[y][7:].split(',')
                            txt[y].append(txt[y+1])
                            val_SMS.append(txt[y])
                            y = int(y+3)
                    lenview += 1
                    data = table(num_rows=lenview, val_view=val_SMS)
                    window.Element('_table_').Update(data[1:][:])
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('Error View SMS')
                    break                
    
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='View SMS ERROR', button_type='POPUP_BUTTONS_ERROR')
   

#Call Function to Delete SMS
def delete_sms(id):
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):        
            if(len(id) is 0):
                sg.Popup(len(id))
            else:
                view_sms()
                x = val_SMS[int(id[0])][0]
                temp = ('AT+CMGD='+x+',0\r').encode()
                ser.write(temp)
                time.sleep(0.5)
                while True:
                    response = ser.readline()
                    if ('OK' in str(response)):
                        view_sms()
                        sg.Popup('Successfully deleted', title=' ')
                        break
                    elif('ERROR' in str(response)):
                        sg.Popup('Please try again', title=' ')
                        break
    
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Delete SMS ERROR', button_type='POPUP_BUTTONS_ERROR')
        
        
#Call Function to make a call(outgoing call)
def call(call_num):
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):        
            temp = ('ATD'+ call_num + ';\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('NO CARRIER' in str(response)):
                    txt[0] = str('Destination : ' + call_num) 
                    window.Element('_outCall_').Update('\nResponse : '.join(txt)+'\n', append=True)
                    LogFiles('_outCall_','\nResponse : '.join(txt)+'\n')
                    break             
                if ('ERROR' in str(response)):
                    window.Element('_outCall_').Update('Destination : '+call_num+'\nResponse : Please input the correct number'+'\n', append=True)
                    LogFiles('_outCall_','Destination : '+call_num+'\nResponse : Please input the correct number'+'\n')
                    break            
                if ('OK' in str(response)):
                    txt[0] = str('Destination : ' + call_num) 
                    window.Element('_outCall_').Update('\nResponse : '.join(txt)+'\n', append=True)
                    LogFiles('_outCall_','\nResponse : '.join(txt)+'\n')
                    break
                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Call ERROR', button_type='POPUP_BUTTONS_ERROR')
    

#Call Function to accept incoming call
def accept():
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):        
            temp = ('ATA\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                response = ser.readline()           
                if ('ERROR' in str(response)):
                    window.Element('_outCall_').Update('Response : Error to accepted\n', append=True)
                    LogFiles('_outCall_','Response : Error to accepted\n')
                    break            
                if ('OK' in str(response)):
                    window.Element('_outCall_').Update('Response : Successfully accepted\n', append=True)
                    LogFiles('_outCall_','Response : Successfully accepted\n')
                    break
                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Incoming Call ERROR', button_type='POPUP_BUTTONS_ERROR')       


#Call Function to Rejected Incoming call or Disconnected call 
def hang_up():
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):        
            temp = ('ATH\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                response = ser.readline()           
                if ('ERROR' in str(response)):
                    window.Element('_outCall_').Update('Response : Error to rejected\n', append=True)
                    LogFiles('_outCall_','Response : Error to rejected\n')
                    break            
                if ('OK' in str(response)):
                    window.Element('_outCall_').Update('Response : Successfully rejected\n', append=True)
                    LogFiles('_outCall_','Response : Successfully rejected\n')
                    break
                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Hangup ERROR', button_type='POPUP_BUTTONS_ERROR')
       
        
#Call Function for USSD
def ussd(ussd_num):
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True): 
            temp = ('AT+CUSD=1,"'+ ussd_num + '"\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            x = 0
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                x += 1
                if ('ERROR' in str(response)):
                    window.Element('_outCall_').Update('Destination : '+ussd_num+'\nResponse : Please input the correct number\n', append=True)
                    LogFiles('_outCall_','Destination : '+ussd_num+'\nResponse : Please input the correct number\n')
                    break
                if ('OK' in str(response)):
                    txt[0] = str('Destination : ' + ussd_num)
                    txt[1] = txt[1][9:]
                    txt[x-1] = txt[x-1].replace('OK','')
                    window.Element('_outCall_').Update('\nResponse : '.join(txt), append=True)
                    LogFiles('_outCall_','\nResponse : '.join(txt))
                    break
                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='USSD ERROR', button_type='POPUP_BUTTONS_ERROR')
    
        
#Call Function to Get Radio(RAT)
def get_radio():
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True): 
            temp = ('AT+KSRAT?\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('OK' in str(response)):
                    fillLR = al.list_RAT.get(str(txt[1][8:]))
                    window.Element('_txtRAT').Update(fillLR)
                    break                
                elif ('ERROR' in str(response)):
                    sg.Popup('ERROR get radio')
                    break                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Get Radio ERROR', button_type='POPUP_BUTTONS_ERROR')


#Call Function to Set Radio(RAT)
def set_radio(listradio):
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            if(listradio is not al.list_radio[0]):
                if(listradio is al.list_radio[1]):
                    rat = 1
                elif(listradio is al.list_radio[2]):
                    rat = 2
                elif(listradio is al.list_radio[3]):
                    rat = 3
                else:
                    rat = 4
                temp = ('AT+KSRAT='+str(rat)+'\r').encode()
                ser.write(temp)
                time.sleep(0.5)
                while True:
                    response = ser.readline()
                    if ('OK' in str(response)):
                        fillLR = al.list_RAT.get(str(rat))
                        window.Element('_txtRAT').Update(fillLR)
                        window.Element('_inlistRadio_').Update(al.list_radio[0])
                        sg.Popup('Successfully updated', title=' ')
                        break
                    elif ('ERROR' in str(response)):
                        sg.Popup('ERROR set radio')
                        break
            else:
                sg.Popup('Please choose the radio ', title=' ')
                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Set Radio ERROR', button_type='POPUP_BUTTONS_ERROR')


#Call Function to Get Airplane Mode
def get_airplane():
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True): 
            temp = ('AT+CFUN?\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('OK' in str(response)):
                    if(str(txt[1][7:]) is '1'):
                        fill = 'Disable'
                    else:
                        fill = 'Enable'
                    window.Element('_txtAIR').Update(fill)
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('ERROR get airplane')
                    break                
                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Get Airplane ERROR', button_type='POPUP_BUTTONS_ERROR')


#Call Function to Get Airplane Mode
def set_airplane(mode):
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            if(mode is True):
                temp = ('AT+CFUN=1,0\r').encode()
            else:
                temp = ('AT+CFUN=4,0\r').encode()                
            ser.write(temp)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('OK' in str(response)):
                    if(mode is True):
                        fill = 'Disable'
                    else:
                        fill = 'Enable'
                    window.Element('_txtAIR').Update(fill)
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('ERROR set airplane')
                    break
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Set Airplane ERROR', button_type='POPUP_BUTTONS_ERROR')

    
#Call Function to Set operator
def setOperator(operator):
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try: 
        if(statusSIM is True and statusPIN is True):
            if operator == al.list_SPN[1]:    #Automatic
                atcmd = 'AT+COPS=0,0\r'
            elif operator == al.list_SPN[2]:  #No Service
                atcmd = 'AT+COPS=2\r'
            else:                             #SIMPATI/XL/INDOSAT/3
                mcc_mnc = al.list_operator[operator]
                atcmd = 'AT+COPS=1,2,"'+ mcc_mnc + '"\r'
            
            ser.write((atcmd).encode())
            time.sleep(0.5)
            
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('OK' in str(response)):
                    sg.Popup ('Successfully Choose Operator '+ operator, auto_close=True, auto_close_duration=5)
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('ERROR!\nNOTE: Ignore if set network operator manually', auto_close=True, auto_close_duration=5)
                    #Note: set network operator manually will return response "ERROR"
                    break
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Network Operator is not Choosen!', button_type='POPUP_BUTTONS_ERROR')
    
        
#Call Function to Get BAND
def get_band():
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            temp = ('AT+KBND?\r').encode()
            ser.write(temp)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('OK' in str(response)):
                    fillLB = al.list_band.get(str(txt[1][7:]))
                    window.Element('_txtBAND_').Update(fillLB)
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('ERROR get band')
                    break
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Get Band ERROR', button_type='POPUP_BUTTONS_ERROR')

        
#Call Function to Scan Network        
def netscan():
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True): 
            netscan = ('AT+KNETSCAN=1\r').encode()
            ser.write(netscan)
            time.sleep(0.5)
            while True:
                response = ser.readline()
                if ('OK' in str(response)):
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('ERROR netscan')
                    break                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Netscan ERROR', button_type='POPUP_BUTTONS_ERROR')


#Call Function to Set BAND
def set_band(listband):
    txt = []
    band = 0
    y = 1
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True): 
            if(True in listband):
                if(False not in listband):
                    band = 2045
                else:
                    for x in range(len(listband)):
                        if(x == 1):
                            y = 4
                        if(listband[x] is True):
                            band = band + y
                        y = int(y*2)
                temp = ('AT*PSRDBS=1,'+str(band)+'\r').encode()
                ser.write(temp)
                time.sleep(0.5)
                while True:
                    response = ser.readline()
                    if ('OK' in str(response)):
                        netscan()
                        get_band()
                        window.Element('_gsm850_').Update(False),window.Element('_gsm900_').Update(False),
                        window.Element('_dcs1800_').Update(False),window.Element('_pcs1900_').Update(False),
                        window.Element('_umts1_').Update(False),window.Element('_umts2_').Update(False),
                        window.Element('_umts5_').Update(False),window.Element('_umts6_').Update(False),
                        window.Element('_umts8_').Update(False),window.Element('_umtsx_').Update(False) 
                        window.Element('_rnoBAND_').Update(True)
                        sg.Popup('Successfully updated', title=' ')
                        break
                    elif ('ERROR' in str(response)):
                        sg.Popup('ERROR set band')
                        break                    
        else:
            sg.Popup('Please choose the Frequency BAND', title=' ')
                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Set Band ERROR', button_type='POPUP_BUTTONS_ERROR')
    
   
#Call Function to Check Cell Details
def cell_details():
    txt = []
    
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True): 
            netscan = ('AT+CGED=0\r').encode()
            ser.write(netscan)
            time.sleep(0.5)
            while True:
                decodeReadline(ser.readline())
                if(response is not ''):
                    txt.append(response)
                if ('OK' in str(response)):
                    for x in range(len(txt)):
                        txt[x] = txt[x].replace('"','')
                        txt[x] = txt[x].replace(',','')
                        if('RAT:' in txt[x]):
                            window.Element('_txtcRAT_').Update(txt[x][11:])
                        if('Serving PLMN:' in txt[x]):
                            window.Element('_txtcMCC_').Update(txt[x+1][4:7])
                            window.Element('_txtcMNC_').Update(txt[x+1][13:15])
                            window.Element('_txtcLAC_').Update(txt[x+1][22:26])
                            window.Element('_txtcRAC_').Update(txt[x+1][34:35])
                    break
                elif ('ERROR' in str(response)):
                    sg.Popup('ERROR cell details')
                    break                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Cell ERROR', button_type='POPUP_BUTTONS_ERROR')
    

#DAKOTA Functions
def GeteUICCInfo():
    if(connStatus is False):
        openSerial()    
    checkPIN()
    if(statusSIM is True and statusPIN is True):
        Channel(opench=True, ch='01')
        CSIMCommand(al.list_csim[3],'_outDKT_')
        CSIMCommand(al.list_csim[2],'_outDKT_')
        view_Profiles(dataSW)
        Channel(opench=False, ch='01')

def SetEmergencyProfile(ID):
    if(connStatus is False):
        openSerial()    
    checkPIN()
    if(statusSIM is True and statusPIN is True):    
        Channel(opench=True, ch='01')
        CSIMCommand(al.list_csim[4],'_outDKT_')
        EmergencySetProfile(ID,True)
        Channel(opench=False, ch='01')    

def EnableEmergencyProfile():
    if(connStatus is False):
        openSerial()    
    checkPIN()
    if(statusSIM is True and statusPIN is True):      
        Channel(opench=True, ch='01')
        CSIMCommand(al.list_csim[4],'_outDKT_')
        CSIMCommand(al.list_csim[7],'_outDKT_')
        Channel(opench=False, ch='01')
#         simpelCode(str("0070000001"))
#         simpelCode(str("01A4040010A000000077010C60110000FE00000400"))
#         simpelCode(str("8120000000"))
#         simpelCode(str("00A4000C023F00"))    

def DisableEmergencyProfile():
    if(connStatus is False):
        openSerial()    
    checkPIN()
    if(statusSIM is True and statusPIN is True):      
        Channel(opench=True, ch='01')
        CSIMCommand(al.list_csim[4],'_outDKT_')
        CSIMCommand(al.list_csim[9],'_outDKT_')
        Channel(opench=False, ch='01')


def SetAPF(enable):
    if(connStatus is False):
        openSerial()    
    checkPIN()
    if enable is True:
        apfMode = al.list_csim[13]+al.list_apf[0]
    else:
         apfMode = al.list_csim[13]+al.list_apf[1]
    if(statusSIM is True and statusPIN is True):
        Channel(opench=True, ch='01')
        CSIMCommand(al.list_csim[12], '_outDKT_')
        CSIMCommand(apfMode, '_outDKT_')
        Channel(opench=False, ch='01')
        CSIMCommand(al.list_csim[16], '_outDKT_')

def SetAPFDuration(pollIntVal, fbCntr):
    if(connStatus is False):
        openSerial()    
    checkPIN()
    apfDuration = al.list_csim[13] + '0A' + '3708' + '8102'+pollIntVal + '8202'+fbCntr
    if(statusSIM is True and statusPIN is True):
        Channel(opench=True, ch='01')
        CSIMCommand(al.list_csim[12], '_outDKT_')
        CSIMCommand(apfDuration, '_outDKT_')
        Channel(opench=False, ch='01')
        CSIMCommand(al.list_csim[16], '_outDKT_')

def PSMAudit(audit):
    if(connStatus is False):
        openSerial()    
    checkPIN()
    if(statusSIM is True and statusPIN is True):
        Channel(opench=True, ch='01')
        CSIMCommand(al.list_csim[12], '_outDKT_')        
        if audit == 'proprietary':
            CSIMCommand(al.list_csim[14], '_outDKT_')
        elif audit == 'connectivity':
            CSIMCommand(al.list_csim[15], '_outDKT_')
        Channel(opench=False, ch='01')
        CSIMCommand(al.list_csim[16], '_outDKT_')


#Call Function to execute CSIM commands
def CSIMCommand(apdu,log,gui=True):
    global dataSW
    txt = []
    temp = []
    
    if(connStatus is False):
        openSerial()    
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            temp = ('AT+CSIM='+str(len(apdu))+',"'+apdu+'"\r').encode()
            ser.write(temp)
            time.sleep(0.01)
            while True:
                decodeReadline(ser.readline())
                txt.append(response)
                if ('OK' in str(response)):
                    restxt = str(txt[0].split(',')[1]).replace('"','')
                    sw = str(txt[1].split(',')[1]).replace('"','')
                    if(len(sw) <= 4):
                        if(gui is True):
                            window.Element(log).Update('APDU : '+restxt+' ('+sw+')\n', append=True)
                        LogFiles(log,'APDU : '+restxt+' ('+sw+')\n')
                    else:
                        if(gui is True):
                            window.Element(log).Update('APDU : '+restxt+' ['+sw[0:-4]+']'+' ('+sw[-4:]+')\n', append=True)
                        LogFiles(log,'APDU : '+restxt+' ['+sw[0:-4]+']'+' ('+sw[-4:]+')\n')
                    dataSW = sw
                    csim_result.clear()
                    csim_result.append(restxt+sw)
                    break
                elif ('ERROR' in str(response)):
                    restxt = str(txt[0].split(',')[1]).replace('"','')
                    if(gui is True):
                        window.Element(log).Update('APDU : '+restxt+' ERROR\n', append=True)
                    break                
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='ERROR', button_type='POPUP_BUTTONS_ERROR')

#Call Function to execute CSIM commands from automation test script
def CSIM(apduCSIM):
    apduCSIM = apduCSIM.replace(' ','')
    CSIMCommand(apduCSIM,'_outAUTO_',gui=False)

#Call Function to execute Envelope CSIM commands from automation test script
def envCSIM(dataCSIM):
    if(dataCSIM[2:4] == '81'):
        dataCSIM = dataCSIM.replace(str(dataCSIM[4:6]), str(convertToHex(dataCSIM[6:])), 1)
    else:
        dataCSIM = dataCSIM.replace(str(dataCSIM[2:4]), str(convertToHex(dataCSIM[4:])), 1)
    dataCSIM = str(al.list_csim[17]+convertToHex(dataCSIM)+dataCSIM)
    CSIM(dataCSIM)
    

#Call Function for execute Generic SIM Access
def gsaAPDU(apdu):
    CSIMCommand(apdu,'_outGSA_')


#Call Function for Open/Close Channel
def Channel(opench,ch):
    if(opench is True):
        cAPDU = ('00700000'+str(ch))
    else:
        cAPDU = ('007080'+str(ch)+'00')
    CSIMCommand(cAPDU,'_outDKT_')


#Call Function EmergencyMode to Set Profile
def EmergencySetProfile(ID,via):
    if(via is True):
        cAPDU = al.list_csim[5]
        cAPDU = (cAPDU[0:8] + convertToHex(ID + 'EDXX') + cAPDU[10:12] + convertToHex(ID) + ID)
    else:
        cAPDU = al.list_csim[6]
        cAPDU = (cAPDU[0:8] + convertToHex(ID + 'EEXXEF0126EDXX') + cAPDU[10:12] + convertToHex(ID + 'EF0126EDXX') +
                 cAPDU[14:22] + convertToHex(ID) + ID)
    CSIMCommand(cAPDU,'_outDKT_')


#simple code
def simpelCode(dataCSIM):
    if(connStatus is False):
        openSerial()
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            temp = ('AT+CSIM='+str(len(dataCSIM))+',"'+str(dataCSIM)+'"\r').encode()
            ser.write(temp)
            
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='Emergency Mode ERROR', button_type='POPUP_BUTTONS_ERROR')
    closeSerial()                    
        
    
# ------------------ Create a table ------------------
def tableLP(num_rows, val_viewLP):
    dataLP = [j for j in range(num_rows)]
    dataLP[0] = ['ID','ISDP-AID','STATE','F','ICCID']
    if(len(val_viewLP) is 0):
        for i in range(num_rows-1):
            dataLP[i+1] = ['  ','                             ','       ','  ','                  ']
    elif(len(val_viewLP) > 0):
        for i in range(num_rows-1):
            dataLP[i+1]=[val_viewLP[i][j].replace('"','') for j in range(5)]
    return dataLP

val_viewLP = []
TabledataProfile = tableLP(num_rows=4, val_viewLP=val_viewLP)
TableheadingsProfile = [TabledataProfile[0][x] for x in range(len(TabledataProfile[0]))]

#Call Function to view all Profiles
def view_Profiles(valProfile):
    txt = []

    if(connStatus is False):
        openSerial()    
    checkPIN()
    try:
        if(statusSIM is True and statusPIN is True):
            tag_get_status = 'E3'
            tag_ISDP_AID = '4F'
            rapdu = valProfile[4:]
            numRows = 0
            x = 0
            y = 5
            while(rapdu.find(tag_ISDP_AID) != -1):
                isdp_base16 = rapdu[rapdu.find(tag_ISDP_AID) +2 : rapdu.find(tag_ISDP_AID) +4]
                isdp_aid = rapdu[rapdu.find(tag_ISDP_AID) +4 : rapdu.find(tag_ISDP_AID) +4+2*int(isdp_base16, 16)]
                rapdu = rapdu[rapdu.find(tag_ISDP_AID) +4+2*int(isdp_base16, 16):]
                isdp_lifecycle = rapdu[rapdu.find('9F70') +6 : rapdu.find('9F70') +8]
                isdp_fallback = rapdu[rapdu.find('5301') +4 : rapdu.find('5301') +6]
                isdp_iccid = rapdu[rapdu.find('2C0A') +4 : rapdu.find('2C0A') +24]
                rapdu = rapdu[rapdu.find('2C0A') +24:]
                txt.append(str(numRows+1))
                txt.append(isdp_aid)
                aid.append(isdp_aid)
                if('3F' in isdp_lifecycle):
                    isdp_lifecycle = 'Enable'
                else:
                    isdp_lifecycle = 'Disable'
                txt.append(isdp_lifecycle)
                if('01' in isdp_fallback):
                    isdp_fallback = 'F'
                else:
                    isdp_fallback = ''
                txt.append(isdp_fallback)
                txt.append(isdp_iccid)
                val_Profiles.append(txt[x:y])
                numRows = numRows+1
                x = y
                y += 5
            numRows += 1 #Add 1 Row for Header of Table
            #Reset Value
            TabledataProfile = tableLP(num_rows=numRows, val_viewLP='')            
            window.Element('_tableLP_').Update(TabledataProfile[1:][:])
            #===========
            TabledataProfile = tableLP(num_rows=numRows, val_viewLP=val_Profiles)
            window.Element('_tableLP_').Update(TabledataProfile[1:][:])
    
    except Exception as e:
        sg.Popup('error open serial port: '+ str(e), title='View Profiles ERROR', button_type='POPUP_BUTTONS_ERROR')

    
#Call Function ConvertToHex
#Ex: convertToHex(A0A40000023F00) HexVal=07
def convertToHex(x):
    HexVal = hex(int(len(x) /2))
    HexVal = str(HexVal[2:]).upper()
    if(len(HexVal) == 1):
        HexVal = str('0'+HexVal)
    return HexVal

#Call Function to change Hex to Dec
#Ex: HextoDec(10) DecVal=16
def HextoDec(x):
    DecVal = int(x,16)
    return DecVal

#Call Function to change Dec to Hex
#Ex: DectoHex(16) DecVal=F
def DectoHex(x):
    DectoHex = hex(x).split('x')[-1].upper()
    return DectoHex

#Call Function to Create Log Title
def logTitle(lt):
    LogFiles('_outAUTO_','# ============================\n')
    LogFiles('_outAUTO_','# '+str(lt)+' =\n')
    LogFiles('_outAUTO_','# ============================\n')
