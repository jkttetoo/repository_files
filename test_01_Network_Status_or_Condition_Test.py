import pytest
import datetime
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *

global gui


gui = False
openSerial()

# Detail Envelope Server
# OA: +6281298265649
# SPI KIC KID: 1639 12 12
# KiC Key: F8195839102919DEAB908947198438FA
# KiD Key: F8195839102919DEAB908947198438FA
# Note: For Initial Condition if the process is failed, please do manually (perso the card via PCOM)

def CSIM(csimApdu):
    CSIMCommand(csimApdu,'_outAUTO_',gui)

def APDU_PSM(apdu,SW='9000'):
#    Open Channel
    CSIM('0070000001')
    assert csim_result[0][-4:] == '9000'
#    Select PSM Applet
    CSIM(list_csim[12])
    assert csim_result[0][-4:] == '9000'    
#    Apdu
    CSIM(apdu)
    assert csim_result[0][-4:] == SW    
#    Close Channel
    CSIM('0070800100')
    assert csim_result[0][-4:] == '9000'    
#    Select USIM AID
    CSIM(al.list_csim[16])
    assert (csim_result[0][-4:] == '6127' or csim_result[0][-4:] == '9000')
        
def AuditISDP():
#    Open Channel    
    CSIM('0070000001')
    assert csim_result[0][-4:] == '9000'     
#    Select LM Applet
    CSIM(al.list_csim[3])
    assert csim_result[0][-4:] == '9000'     
#    Get eUICC Info
    CSIM(al.list_csim[2])
    assert csim_result[0][-4:] == '9000'
#    Close Channel
    CSIM('0070800100')
    assert csim_result[0][-4:] == '9000'    
#    Select USIM AID    
    CSIM(al.list_csim[16])
    assert (csim_result[0][-4:] == '6127' or csim_result[0][-4:] == '9000')

# ==========================================
# Test Suite Network Status/Condition Test =
# ==========================================
def test_Date():
    x = datetime.datetime.now()
    LogFiles('_outAUTO_','# ============================\n')
    LogFiles('_outAUTO_','# '+str(x)+' =\n')
    LogFiles('_outAUTO_','# ============================\n') 

def test_InitialConditionForNetworkStatus():
    LogFiles('_outAUTO_','# ================================\n')    
    LogFiles('_outAUTO_','# InitialConditionForNetworkStatus\n')
    LogFiles('_outAUTO_','# ================================\n')    
    
#    Update PoR Connectivity 1100
# TAR: 000001
# Counter: 000000001F
# Data: 
# 80E6200016000010A0000005591010FFFFFFFF8900001100000000
# 80E28800133A0710A00E060691261881000081018F820118
    CSIM('80C2000081D17F8202838106069126180100008B71400D91261892285646F97F16000000000000285D027000005815163912120000015E49E407F08F5A0C281488B8050A8C7639027F76CF1EFF1865D49F238D08169490DAC652D5301CB7721B3880DB6309027D3F7DF955EB404A6EE7FA0F7EC03A4699BA802CB958F295021C8A19891F5A7D')
    assert csim_result[0][-4:] == '914C'
    time.sleep(30)

#     Set Fallback 0010
    APDU_PSM(list_csim[13] + '0A37088102001E82020010')

def test_DK4812():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4812\n')
    LogFiles('_outAUTO_','# =======\n')     
#    Enable Profile 1000
# TAR: 000001
# Counter: 0000000020
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D027000003815163912120000010E6B6D14EA8985AA3005D7118F0EF618BB90F3A0313AC5DCC37F8130C109532110D32B5DF991E8E40F127D5B696C0686')
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0001
# Counter: 0000000021
# Data: 80E28900073A08044E020001
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001D5B68D02C18C244D703EE88D74A22D7F645382AF7FEF23CF5B2C5D66CB159896')
    time.sleep(3)

#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(510)
    
#     fallback to 1100
    setOperator('Automatic')
    time.sleep(30)

# Wait for 30s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0002
# Counter: 0000000022
# Data: 80E28900073A08044E020002
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001B041C1AE8327F824A409D26600C43EFA11BD55E809678EBC0F7103A79F505293')
#     audit ISDP
    AuditISDP()

def test_DK4813():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4813\n')
    LogFiles('_outAUTO_','# =======\n')     
#    Enable Profile 1000
# TAR: 000001
# Counter: 0000000023
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D02700000381516391212000001AF4CCA83F002DC335FC2679DDE4D200421AAC8F98B6BE4FF6D8BFB987ABB28B49E135C60D066DC24278162F618810899')
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0003
# Counter: 0000000024
# Data: 80E28900073A08044E020003
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D027000002815163912120000012B9CE0624462D7C2B6414701AF3C2C52486DE9BC6E7D3AC416067260BC500168')
    time.sleep(30)

# Wait for 30s after receive notif profile changed
# Send set Fallback to 1000
# Counter: 0000000025
# Data: 80E28800153A05124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D027000003815163912120000011EC61FD53E44EC4F44896EB5559F74611605E40B8556BA4315D0FC3BCF53642A4716F8D9991073C053E44B9B05C8DF5B')
    time.sleep(3)
    
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(510)

# Wait for 510s There is no Refresh and profile changed
    setOperator('Automatic')
    time.sleep(30)
    
# Wait for 30s
    AuditISDP()

def resetDK4813():
    time.sleep(30)
    LogFiles('_outAUTO_','# ============\n')     
    LogFiles('_outAUTO_','# ResetDK-4813\n')
    LogFiles('_outAUTO_','# ============\n')
#    Enable Profile 1000
# TAR: 000001
# Counter: 0000000026
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001100
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D02700000381516391212000001A5D8BE83FCA23D01F16E0FB31CAD0D727F44527C02D04376BCCAAEA8AEED17C91D79E0BF72F7B6B674783409BE656A0A')
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0004
# Counter: 0000000027
# Data: 80E28900073A08044E020004
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001CA1B3E05D1FFB11555ADCF7880D2B73726900703291CFE6201D1FC4BAB63CFBD')
    time.sleep(30)

# Wait for 30s after receive notif profile changed
# Send set Fallback to 1100
# Counter: 0000000028
# Data: 80E28800153A05124F10A0000005591010FFFFFFFF8900001100
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D02700000381516391212000001EDE205EC02096241E8C8C5E96C6089964C05B405F11CBE4569754A2798BED9E2DCF23C1E24B94B66A24BF212722CABC3')
    time.sleep(30)
    
# Wait for 30s
    AuditISDP()    
    
def DK4814():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')     
    LogFiles('_outAUTO_','# DK-4814\n')
    LogFiles('_outAUTO_','# =======\n')
#    Enable Profile 1000
# TAR: 000001
# Counter: 0000000029
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D02700000381516391212000001FAEA22A3167180F8CF143A2D58A4E7EB06E91EE424E34BDB8EA3D824EAB0DC479E6ECC2F6ECB1DE8394F6A928A0317C1')
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0005
# Counter: 000000002A
# Data: 80E28900073A08044E020005
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001E17CEA86E273EAF8E66DA8C3D32BBF68445A92BC9F6586DDD2153AA6867AD79F')
    time.sleep(30)
    
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(130)

# Wait for 130s
    setOperator('Automatic')
    time.sleep(30)

#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(510)

# Wait for 510s
    setOperator('Automatic')
    time.sleep(30)

# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0006
# Counter: 000000002B
# Data: 80E28900073A08044E020006
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D027000002815163912120000010A8BCD8BCB221DDE0F3F244B18981E8702BEA15459D771B7B453BAA7C9F25DC3')
    time.sleep(30)
    
# Wait for 30s
    AuditISDP()

def DK4815():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')     
    LogFiles('_outAUTO_','# DK-4815\n')
    LogFiles('_outAUTO_','# =======\n')
#    Enable Profile 1000
# TAR: 000001
# Counter: 000000002C
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D0270000038151639121200000155C96B5FE673A4A7B4F8C078A0DEC5414A0461272E7A9377EC971F05B76CB2F9E18A9D0807EE5086E7BDB4FEFB77EA97')
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0007
# Counter: 000000002D
# Data: 80E28900073A08044E020007
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D0270000028151639121200000193135AAEE8B92A4417268B5BD671FD4D466C0AF387A4F83DE9F2EDFEE84BA9E3')
    time.sleep(30)

# Send AT Command to set network to Limited Service
    CSIM('80C200000CD60A190103020282811B0101')
    time.sleep(480)
    
# Wait for 480s
    AuditISDP()
    time.sleep(120)   
    
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(510)

# Wait for 510s
    setOperator('Automatic')
    time.sleep(30)

# Wait for 30s until switch on profile 1100 and receive notif profile changed
# Send notif confirmation 0008
# Counter: 000000002E
# Data: 80E28900073A08044E020008
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001321CE6A2D09A7EFAE75D09343FEA8E01F7F5F74FD0EAD8748C77516A83F9E36D')
    time.sleep(30)
    
# Wait for 30s
    AuditISDP()

def DK4816():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')     
    LogFiles('_outAUTO_','# DK-4816\n')
    LogFiles('_outAUTO_','# =======\n')
#    Enable Profile 1000
# TAR: 000001
# Counter: 000000002F
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D027000003815163912120000015D2E498945A712EB684F90ED995BD586A72455C9BA7079BE4F359335D11CA77A98F1645AFEBA2AE9065FF56E28B71D6F')
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0009
# Counter: 0000000030
# Data: 80E28900073A08044E020009
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001145FEA844C4445E98D9AEDFE01D055C53EF83CBB509E01FE425D3769C3D09203')
    time.sleep(30)
    
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(130)

# Wait for 130s
    setOperator('No Service')
    time.sleep(130)

#   Wait for 130s  set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(130)

# Wait for 130s
    setOperator('No Service')
    time.sleep(130)    

#   Wait for 130s  set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(250)

# Wait for 250s
    setOperator('Automatic')
    time.sleep(30)
    
# Wait for 30s until switch on profile 1100 and receive notif profile changed
# Send notif confirmation 000A
# Counter: 0000000031
# Data: 80E28900073A08044E02000A
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001754C7CDF77C006C6FF8581F329384B983DD0366FEE874EAE5983BEA13B459F71')
    time.sleep(30)
    
# Wait for 30s
    AuditISDP()

def DK4817():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')     
    LogFiles('_outAUTO_','# DK-4817\n')
    LogFiles('_outAUTO_','# =======\n')
#    Enable Profile 1000
# TAR: 000001
# Counter: 0000000032
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D02700000381516391212000001A1BD5AC426D1BB28F554B55A7AF5199584F8CDD0C9197EA4F2BF2A4355B85E360B6D82498BCA9089350C4F857CBF9843')
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 000B
# Counter: 0000000033
# Data: 80E28900073A08044E02000B
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D027000002815163912120000019FBAF04B4BFA0976FE28FF1922811EB08462AE9F87673C33C672F7CA9C3140CE')
    time.sleep(30)
    
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(130)

# Wait for 130s
    setOperator('Automatic')
    time.sleep(130)

#   Wait for 130s  set network to No Service
    setOperator('No Service')
    time.sleep(130)    

#   Wait for 130s  set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(510)

# Wait for 510s
    setOperator('Automatic')
    time.sleep(30)
    
# Wait for 30s until switch on profile 1100 and receive notif profile changed
# Send notif confirmation 000C
# Counter: 0000000034
# Data: 80E28900073A08044E02000C
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D027000002815163912120000014D5F5D429E0922DF2C5B867E74A85CB14EDC85980033AECAFFF7283F3DA46246')
    time.sleep(30)
    
# Wait for 30s
    AuditISDP()    
