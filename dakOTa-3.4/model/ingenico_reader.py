from Oberthur import *
from Mobile import *

import const
from util import allow_short_aid_for_isdp, d2h, compute_receipt

from Oberthur import LogInfo, lv, berLv
from Ot.GlobalPlatform import *
from util import pprint
import model


local_management_applet = model.LocalManagementApplet()
euicc = model.Euicc()

class IngenicoReader:

	def open_logical_channel(self, ch_number):
		LogInfo('open logical channel')
		capdu	 = '00700000' + ch_number
		data, sw = SendAPDU(capdu)
		return capdu, data, sw

	def close_logical_channel(self, ch_number):
		LogInfo('close logical channel')
		capdu	 = '007080' + ch_number + '00'
		data, sw = SendAPDU(capdu)
		return capdu, data, sw

	def select_applet_local_management(self, ch_number):
		capdu, data, sw = local_management_applet.select(ch_number =ch_number)
		if sw != '9000' : LogInfo("No Local Management applet or AID is wrong")
		return capdu, data, sw

	def enable_profile(self,aid_isdp):
		capdu, data, sw = self.open_logical_channel(ch_number='01')
		capdu, data, sw = self.select_applet_local_management(ch_number='01')
		capdu, data, sw_enable = local_management_applet.enable_profile(aid_isdp)
		capdu, data, sw_closech = self.close_logical_channel(ch_number='01')
		return sw_enable, sw_closech
		
	def disable_profile(self,aid_isdp):
		capdu, data, sw = self.open_logical_channel(ch_number='01')
		capdu, data, sw = self.select_applet_local_management(ch_number='01')
		capdu, data, sw_disable = local_management_applet.disable_profile(aid_isdp)
		capdu, data, sw_closech = self.close_logical_channel(ch_number='01')
		return sw_disable, sw_closech

	def delete_profile(self,aid_isdp):
		capdu, data, sw = self.open_logical_channel(ch_number='01')
		capdu, data, sw = self.select_applet_local_management(ch_number='01')
		capdu, data, sw_delete = local_management_applet.delete_profile(aid_isdp)
		capdu, data, sw_closech = self.close_logical_channel(ch_number='01')
		return sw_delete, sw_closech

	def set_fallback_profile(self, aid_isdp):
		capdu, data, sw = self.open_logical_channel(ch_number='01')
		capdu, data, sw = self.select_applet_local_management(ch_number='01')
		capdu, data, sw_setfallback = local_management_applet.set_fallback_profile(aid_isdp)
		capdu, data, sw_closech = self.close_logical_channel(ch_number='01')
		return sw_setfallback, sw_closech

	@property
	def get_euicc(self):
		capdu, data, sw = self.open_logical_channel(ch_number='01')
		capdu, data, sw = self.select_applet_local_management(ch_number='01')
		capdu, data, sw = local_management_applet.get_euicc_info()
		rapdu_parsed	= self.audit_uicc_info_LM(rapdu=data)
		data_close, data, sw_closech = self.close_logical_channel(ch_number='01')
		return rapdu_parsed

	def open_logical_channel_1(self):
		LogInfo('ISO: open logical channel 1')
		capdu = '0070000001 01'
		data, sw = SendAPDU(capdu)
		return capdu, data, sw


	def close_logical_channel_1(self):
		LogInfo('ISO: close logical channel 1')
		capdu = '00708001 00'
		data, sw = SendAPDU(capdu)
		return capdu, data, sw

	@property
	def select_applet_local_management_via_ch_1(self):
		return SendAPDU('01A4040010A0000000770307601100FE0000300001')

	def enable_profile(self,aid_isdp):
		self.open_logical_channel_1()
		self.select_applet_local_management_via_ch_1
		capdu, data, sw_enable_profile = local_management_applet.enable_profile(aid_isdp)
		capdu, data, sw_close_channel = self.close_logical_channel_1()
		return sw_enable_profile, sw_close_channel
		
	def disable_profile(self,aid_isdp):
		sw_opench = self.open_logical_channel_1()
		self.select_applet_local_management_via_ch_1
		capdu, data, sw_disable_profile = local_management_applet.disable_profile(aid_isdp)
		capdu, data, sw_close_channel = self.close_logical_channel_1()
		return sw_disable_profile, sw_close_channel

	def delete_profile(self,aid_isdp):
		sw_opench = self.open_logical_channel_1()
		self.select_applet_local_management_via_ch_1
		capdu, data, sw_delete_profile = local_management_applet.delete_profile(aid_isdp)
		capdu, data, sw_close_channel = self.close_logical_channel_1()
		return sw_delete_profile, sw_close_channel
	
	@property
	def get_eid(self):
		self.select_applet_local_management(ch_number='00')
		return local_management_applet.get_eid()

	@property
	def get_local_management_configuration(self):
		self.select_applet_local_management(ch_number='00')
		return local_management_applet.get_local_management_config()

	@property
	def activate_euicc_notification(self):
		self.select_applet_local_management(ch_number='00')
		return local_management_applet.activate_euicc_notification()

	@property
	def deactivate_euicc_notification(self):
		self.select_applet_local_management(ch_number='00')
		return local_management_applet.deactivate_euicc_notification() 

	@property
	def allow_every_local_management_command(self):
		self.select_applet_local_management(ch_number='00')
		return local_management_applet.allow_every_command()

	@property
	def disallow_every_local_management_command(self):
		self.select_applet_local_management(ch_number='00')
		return local_management_applet.disallow_every_command()

	# @property
	# def disallow_enable_command(self):
	# 	data, sw = self.get_local_management_configuration
	# 	bs_0 = BytesString(data[0:2])
	# 	bs_0.SetBit(0,0,0)
	# 	LogDebug('before:' + data[0:2])
	# 	LogDebug('after: {}'.format(bs_0))
	# 	data, sw = Send('8020000001{}'.format(bs_0))
	# 	return (data,sw)


	def pre_disable_check(self, aid_isdp):

		if 	aid_isdp == 'A0000005591010FFFFFFFF8900001100' :
			isdr_audit = self.get_euicc
			list_index_first_disabled_profile = isdr_audit['list_life_cycle_state'].index('1F Disabled')
			aid_isdp_first_enabled_profile = isdr_audit['list_aid'][list_index_first_disabled_profile]
			self.enable_profile(aid_isdp_first_enabled_profile)
			isdr_audit = self.get_euicc	
		else : aid_isdp_first_enabled_profile = aid_isdp
		return aid_isdp_first_enabled_profile

	@property
	def disallow_enable_command(self):
		capdu, data, sw = self.get_local_management_configuration
		bs_0 = BytesString(data[0:2])
		bs_0.SetBit(0,0,0)
		LogInfo('before:' + data[0:2])
		LogInfo('after: {}'.format(bs_0))
		capdu = '8020000001{}'.format(bs_0)
		data, sw = SendAPDU(capdu)
		return capdu, data, sw


	@property
	def allow_enable_command(self):
		capdu, data, sw = self.get_local_management_configuration
		bs_0 = BytesString(data[0:2])
		bs_0.SetBit(0,0,1)
		LogInfo('before:' + data[0:2])
		LogInfo('after: {}'.format(bs_0))
		capdu = '8020000001{}'.format(bs_0)
		data, sw = Send(capdu)
		return capdu, data, sw

	@property
	def disallow_disable_command(self):
		capdu, data, sw = self.get_local_management_configuration
		capdu, data, sw = self.get_local_management_configuration
		bs_1 = BytesString(data[0:2])
		bs_1.SetBit(0,1,0)
		LogInfo('before:' + data[0:2])
		LogInfo('after: {}'.format(bs_1))
		data, sw = Send('8020000001{}'.format(bs_1))
		return (data,sw)

	@property
	def allow_disable_command(self):
		capdu, data, sw = self.get_local_management_configuration
		capdu, data, sw = self.get_local_management_configuration
		bs_1 = BytesString(data[0:2])
		bs_1.SetBit(0,1,1)
		LogInfo('before:' + data[0:2])
		LogInfo('after: {}'.format(bs_1))
		data, sw = Send('8020000001{}'.format(bs_1))
		return (data,sw)

	@property
	def disallow_delete_command(self):
		capdu, data, sw = self.get_local_management_configuration
		bs_2 = BytesString(data[0:2])
		bs_2.SetBit(0,2,0)
		LogInfo('before:' + data[0:2])
		LogInfo('after: {}'.format(bs_2))
		data, sw = Send('8020000001{}'.format(bs_2))

		return (data,sw)

	@property
	def allow_delete_command(self):
		capdu, data, sw = self.get_local_management_configuration
		bs_2 = BytesString(data[0:2])
		bs_2.SetBit(0,2,1)
		LogInfo('before:' + data[0:2])
		LogInfo('after: {}'.format(bs_2))
		data, sw = Send('8020000001{}'.format(bs_2))
		return (data,sw)

	@property
	def disallow_local_fallback(self):
		capdu, data, sw = self.get_local_management_configuration
		bs_3 = BytesString(data[0:2])
		bs_3.SetBit(0,3,0)
		LogInfo('before:' + data[0:2])
		LogInfo('after: {}'.format(bs_3))
		data, sw = Send('8020000001{}'.format(bs_3))
		return (data, sw)

	@property
	def allow_local_fallback(self):
		capdu, data, sw = self.get_local_management_configuration
		bs_3 = BytesString(data[0:2])
		bs_3.SetBit(0,3,1)
		LogInfo('before:' + data[0:2])
		LogInfo('after: {}'.format(bs_3))
		data, sw = Send('8020000001{}'.format(bs_3))
		return (data, sw)

	def audit_uicc_info_LM(self,rapdu):

		rapdu = rapdu[4:]

		life_cycle_state = {
			''   : 'unknown',
			'3F' : 'Enabled',
			'1F' : 'Disabled',
			'0F' : 'Personalized',
			'07' : 'Selectable',
			'03' : 'Installed'
			}

		result = {
			'aid_profile_enabled' : None,
			'aid_profile_has_fallback_attribute' : None,
			'list_aid' : ['AID ISD-P'],
			'list_life_cycle_state' : ['Status'],
			'list_life_cycle_state_meaning' : ['Life Cycle State Meaning'],
			'list_has_fallback_attribute': ['Fallback'],
			'list_iccid_isdp' : ['ICCID'],
				}
                     
		tag_start = '4F'

		while(rapdu.find(tag_start) != -1):
			isdp_aid_len_base_16 = rapdu[ rapdu.find(tag_start) +2 : rapdu.find(tag_start) +4 ]
			isdp_aid_val = rapdu[ rapdu.find(tag_start) +4 : rapdu.find(tag_start) +4+2*int(isdp_aid_len_base_16, 16)] #### value ISDP
			rapdu = rapdu[rapdu.find(tag_start) +4+2*int(isdp_aid_len_base_16, 16):] ### Next
			isdp_life_cycle_state = rapdu[rapdu.find('9F7001')+6:rapdu.find('9F7001')+8] ### Lifecycle state
			isdp_is_fallback = rapdu[rapdu.find('5301')+4:rapdu.find('5301')+6]
			isdp_iccid = rapdu[rapdu.find('2C')+4:rapdu.find('2C')+24]	

			result['list_aid'].append( isdp_aid_val )
			result['list_iccid_isdp'].append(isdp_iccid)
			result['list_life_cycle_state'].append( '{} {}'.format( isdp_life_cycle_state, life_cycle_state[isdp_life_cycle_state] ))
			result['list_has_fallback_attribute'].append( 'X' if isdp_is_fallback == '01' else '')
			
		result['aid_profile_enabled'] = result['list_aid'][ result['list_life_cycle_state'].index('3F Enabled') ]
		result['aid_profile_has_fallback_attribute'] = result['list_aid'][ result['list_has_fallback_attribute'].index('X') ]

		pprint.table( [ result['list_aid'], result['list_iccid_isdp'], result['list_life_cycle_state'], result['list_has_fallback_attribute'] ] )

		return result

	def resetIngenicoToPrecondition(self):
		pprint.h3("allow enable command")
		self.allow_enable_command
		pprint.h3("allow disable command")
		self.allow_disable_command
		pprint.h3("allow evert local management command")
		self.allow_every_local_management_command
		pprint.h3("allow local fallback")
		self.allow_local_fallback
		pprint.h3("activate euicc notification")
		self.activate_euicc_notification


#### to be moved to automation script

		# def pre_disable_check(self,aid_isdp):

		# if 	aid_isdp == 'A0000005591010FFFFFFFF8900001100' :
		# 	isdr_audit = self.get_uicc
		# 	list_index_first_disabled_profile = isdr_audit['list_life_cycle_state'].index('1F Disabled')
		# 	aid_isdp_first_enabled_profile = isdr_audit['list_aid'][list_index_first_disabled_profile]
		# 	self.enable_profile(aid_isdp_first_enabled_profile)
		# 	isdr_audit = self.get_uicc
		
		# else : aid_isdp_first_enabled_profile = aid_isdp

		# return aid_isdp_first_enabled_profile












