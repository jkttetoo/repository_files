from Oberthur import *
from Mobile import *
from Ot.GlobalPlatform import *
import model

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER	= model.Smsr()

class EraGlonass:

	aid = Constants.config['Era_Glonass_Applet']['AID']
	cap_file = None
	version = '1.0'
	
	@property	
	def select(self):
		capdu = '00A4040010' + self.aid
		data, sw = SendAPDU(capdu)
		LogInfo('ISO: select applet Era Glonass ({})'.format(sw))

		return data, sw

	def select_naa(self):
		capdu = '00A4040C07A0000000090001'
		# capdu = '00A4040C0F A0000000 871002FF FFFFFF89 030100'
		data, sw = SendAPDU(capdu)
		LogInfo('ISO: select NAA ({})'.format(sw))

		return data, sw
	

	def glonass_profile_activation_via_applet_selection(self):
		capdu = '8020000000'
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	def glonass_profile_activation_via_process_toolkit(self):
		capdu = 'A0C2000005EE03EF0120'
		''' [Need to find select NAA] '''
		data, sw = self.select_naa()
		data, sw = SendAPDU(capdu)

		return data, sw

	def glonass_profile_deactivation_via_applet_selection(self):
		capdu = '8024000000'
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	def glonass_profile_deactivation_via_process_toolkit(self):
		capdu = 'A0C2000005EE03EF0124'
		''' [Need to find select NAA] '''
		data, sw = self.select_naa()
		data, sw = SendAPDU(capdu)

		return data, sw

	def set_glonass_profile_via_applet_selection(self, aid):
		capdu = '80260000 12 ED 10' + aid
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	def set_glonass_profile_via_process_toolkit(self, aid):
		capdu = 'A0C20000 17 EE15EF0126 ED 10 '+ aid
		''' [Need to find select NAA] '''
		data, sw = self.select_naa()
		data, sw = SendAPDU(capdu)

		return data, sw


	def get_glonass_profile_via_applet_selection(self):
		capdu = '8026010002'
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	def get_glonass_profile_via_process_toolkit(self):
		capdu = 'A0C20000 05 EE03EF0126'
		''' [Need to find select NAA] '''
		data, sw = self.select_naa()
		data, sw = SendAPDU(capdu)

		return data, sw

	def activate_notification_via_applet_selection(self):
		capdu = '8028000000'
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	def activate_notification_via_process_toolkit(self):
		capdu = 'A0C20000 08 EE06EF0128EC0100'
		''' [Need to find select NAA] '''
		data, sw = self.select_naa()
		data, sw = SendAPDU(capdu)

		return data, sw

	def deactivate_notification_via_applet_selection(self):
		capdu = '8028010000'
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	def deactivate_notification_via_process_toolkit(self):
		capdu = 'A0C20000 08 EE06EF0128EC0101'
		''' [Need to find select NAA] '''
		data, sw = self.select_naa()
		data, sw = SendAPDU(capdu)

		return data, sw


	def mconnect_lock_via_applet_selection(self):
		capdu = '8032000000'
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	def mconnect_unlock_via_applet_selection(self):
		capdu = '8032010000'
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	

if __name__ == '__main__':
	eraglonass = EraGlonass()
