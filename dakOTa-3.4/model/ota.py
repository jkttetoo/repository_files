import allure
import const
import json
import HttpAmdB
from Oberthur import *
from Ot.PyCom import Dll, OtCALCULWrapper, Buffer
from OtDLL_TLS_PSKWrapper import OtDLL_TLS_PSKWrapper
from OtOTA2Wrapper import OtOTA2Wrapper
from BipModule import BipClass
from Tls import TlsServer
from Mobile import Send, Fetch, TerminalResponse, Envelope, InitHandset
from Utilities import ComputeLength, IncrementHexValue, HexToByte
from util.hexastring import subByte
from util import pprint
from util import file
from model import Device
from util.tools import *
from time import time

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

Dll.DLL_TLS_PSK = OtDLL_TLS_PSKWrapper(file.dll("tls_psk.dll"))
Dll.OTA2        = OtOTA2Wrapper(file.dll("ota2.dll"))
Dll.CALCUL      = OtCALCULWrapper(file.dll("calcul.dll"))

http_client     = HttpAmdB.HttpClient()
http_server     = HttpAmdB.HttpServer()
tls_server      = TlsServer() 
bip_channel     = BipClass()
device          = Device()



class SCP80:

	scp80_kic = Constants.config['ISDR']['scp80_kic']
	scp80_kid = Constants.config['ISDR']['scp80_kid']
	scp80_kik = Constants.config['ISDR']['scp80_kik']

	def __init__(self, parent_aid='A0000005591010FFFFFFFF8900000100', spi1=0x12, spi2=0x21, kic=0x10, kic_key=scp80_kic, kid=0x10, kid_key=scp80_kid, tar='000001', algo_cipher=const.hexa.ciphers['AES'], algo_crypto_verif=const.hexa.ciphers['AES'], tpoa='0681214365'):
		self.spi1               = spi1
		self.spi2               = spi2
		self.kic                = kic
		self.kid                = kid
		self.kic_key            = kic_key
		self.kid_key            = kid_key
		self.tar                = tar
		self.algo_cipher        = algo_cipher
		self.algo_crypto_verif  = algo_crypto_verif
		self.parent_aid         = parent_aid
		self.tpoa               = tpoa
	 

	def next_cntr(self):
		with open(file.db('counters.txt')) as f:
			counters = json.load(f)
			counters[self.parent_aid] = increment(counters.get(self.parent_aid, "0000000000"), 1)
		with open(file.db('counters.txt'), 'w') as f:
			json.dump(counters, f)
		return counters[self.parent_aid]

	@allure.step
	def remote_management(self, apdus, apdu_format='definite',tpoa='0681214365', payload=False):

		LogInfo('SCP80: {}'.format(apdus))

		# Dll.OTA2.init_env_0348('') 
		# Dll.OTA2.change_tar(self.tar)
		# Dll.OTA2.change_counter(self.next_cntr())
		# Dll.OTA2.change_cnt_chk(subByte(self.spi1,4,5))
		# Dll.OTA2.change_cipher(subByte(self.spi1,3))
		# Dll.OTA2.change_crypto_verif(subByte(self.spi1,1,2))
		# Dll.OTA2.change_por_format(subByte(self.spi2,6))
		# Dll.OTA2.change_por_cipher(subByte(self.spi2,5))
		# Dll.OTA2.change_por_security(subByte(self.spi2,3,4))
		# Dll.OTA2.change_por(subByte(self.spi2,1,2))
		# if self.spi1 == 0x16: Dll.OTA2.change_algo_cipher(self.algo_cipher)
		# Dll.OTA2.change_algo_crypto_verif(self.algo_crypto_verif)
		# Dll.OTA2.change_kic(intToHexString(self.kic))
		# Dll.OTA2.change_kid(intToHexString(self.kid))
		# Dll.OTA2.set_dlkey_kic(self.kic_key)
		# Dll.OTA2.set_dlkey_kid(self.kid_key)
		# Dll.OTA2.change_tp_oa(self.tpoa)

		Dll.OTA2.init_env_0348('') 
		Dll.OTA2.change_tar(self.tar)
		Dll.OTA2.change_counter(self.next_cntr())
		Dll.OTA2.change_cnt_chk(subByte(self.spi1,4,5))
		Dll.OTA2.change_cipher(subByte(self.spi1,3))
		Dll.OTA2.change_crypto_verif(subByte(self.spi1,1,2))
		Dll.OTA2.change_por_format(subByte(self.spi2,6))
		Dll.OTA2.change_por_cipher(subByte(self.spi2,5))
		Dll.OTA2.change_por_security(subByte(self.spi2,3,4))
		Dll.OTA2.change_por(subByte(self.spi2,1,2))
		if self.spi1 == 0x16: 
			Dll.OTA2.change_algo_cipher(self.algo_cipher)
		Dll.OTA2.change_algo_crypto_verif(self.algo_crypto_verif)
		Dll.OTA2.set_dlkey_kic(self.kic_key)
		Dll.OTA2.set_dlkey_kid(self.kid_key)
		Dll.OTA2.change_kic(intToHexString(self.kic))
		Dll.OTA2.change_kid(intToHexString(self.kid))
		Dll.OTA2.change_tp_oa(self.tpoa)
		Dll.OTA2.change_tp_pid('7F')
		Dll.OTA2.change_device_identities_tlv('82028381')
		# Dll.OTA2.change_tp_dcs('16')
		

		if apdu_format == "compact"     : LogInfo('Remote APDU format: Compact')
		if apdu_format == "definite"    : LogInfo('Remote APDU format: Expanded format, Definite Mode')
		if apdu_format == "indefinite"  : LogInfo('Remote APDU format: Expanded format, Indefinite Mode')

		if isinstance(apdus, str):
			apdus = [apdus]

		command_script = ''

		for idx, apdu in enumerate(apdus):

			if apdu_format == "compact":
				Dll.OTA2.append_script(apdu)

			if apdu_format == "definite":
				Dll.OTA2.append_script(apdu + "/EXPANDED")

			if apdu_format == "indefinite":
				command_script += '22' + berLv(apdu)
				if idx == len(apdus)-1:
					command_script = 'AE80' + command_script + '0000'
					Dll.OTA2.append_script(command_script)
		
		Dll.OTA2.end_message("GI")       
		Dll.OTA2.get_message_nb("K")        
		total_nb_of_sms = Buffer.K.Get()
		current_sms = '01'
		
		Dll.OTA2.display_message(Buffer.I.Get())

		command_script=[]
		if payload:
			por = None
			sw = None
			command_script.append(Buffer.I.Get())
		else:
			data, sw = Envelope(Buffer.I.Get(), "91XX,9000")
		
			LogInfo('{}/{} envelope sms-pp download ({})'.format(current_sms, total_nb_of_sms, sw))

			while current_sms != total_nb_of_sms:
				current_sms = increment(current_sms, 1)
				Dll.OTA2.get_next_message("GI")
				Dll.OTA2.display_message(Buffer.I.Get())
				if payload:
					command_script.append(Buffer.I.Get())
				else:					
					data, sw = Envelope(Buffer.I.Get(), "91XX,9000")
					LogInfo('{}/{} envelope sms-pp download ({})'.format(current_sms, total_nb_of_sms, sw))

			if payload != True:
				if self.spi2 % 2 == 1:
					por, more_get_status, sw = device.fetch_por_sms(sw, self)

					if command_script[8:14] == '80F240': #ie get_status, not necessary if is it the only comand returning 6310? to be verified
						cmd_get_status_next_occurence = command_script[:14] + increment(command_script[14:16], 1) + command_script[16:]
						while more_get_status:
							por += ' -> '
							Dll.OTA2.clear_script('')
							Dll.OTA2.change_counter(self.next_cntr())
							Dll.OTA2.append_script(cmd_get_status_next_occurence)
							Dll.OTA2.end_message("GI")
							Dll.OTA2.display_message(Buffer.I.Get())
							LogInfo('envelope sms-pp download: {}'.format(cmd_get_status_next_occurence))
							data, sw = Envelope(Buffer.I.Get(), "91XX")
							new_por_partial, more_get_status, sw = device.fetch_por_sms(sw, self)
							por += new_por_partial # ugly because header definite is included but audit parser (pprint_audit manages this issue)

				else:
					por = None

				LogInfo('por: {}'.format(por)) # if len(por) <= 73 else por[:70] + '...')
				if por == 'AB0780010123026985': LogInfo('6985, conditions of use not satisfied!')
				if por == 'AB07800101230269E1': LogInfo('PCF-Rules do not allow this request')

		return command_script, por, sw
		
	def push_sms(self, connection_param = None):
		#self.kic_key = '00'*16
		# self.connection_param=connection_param
		if connection_param:
			# push_sms = "81"+lv("83"+lv("84"+lv(connection_param)))
			if(getSAAAAXCode() == "41594"):
				return self.remote_management(connection_param, apdu_format='compact')
			else:
				return self.remote_management(connection_param, apdu_format='compact')
		else:
			return self.remote_management('8100', apdu_format='compact')
			# return self.remote_management('81298327841B01030140010202818205003902058E3C0302FDEA3E0521B982B42D89088A064F5441435F31', apdu_format='compact')
	@allure.step
	def card_audit(self):
		return self.ram('80F24002094F005C054F9F7053B500')




class SCP81:

	scp81_psk_tls_key           = Constants.config['ISDR']['scp81_psk_tls_key']
	scp81_psk_dek               = Constants.config['ISDR']['scp81_psk_dek']
	scp81_psk_id_diversified    = Constants.config['ISDR']['scp81_psk_id_diversified']
	
	tls_version            = 'TLS_SERVER_1_2'
	tls_ciphersuite        = ["TLS_PSK", "TLS_AES_128" or "TLS_CIPHER_NULL"]
	
	#def __init__(self, psk_tls_key='19BA73432C86E2BE476EB91CBEAA9C5D', psk_dek='F8195839102919DEAB908947198438FA', psk_id_diversified='383030313032383131303633363431363033313030303030393433333746303030303030303030303034344631304130303030303035353931303130464646464646464638393030303030313030383230313031383330313431', scp80=SCP80()):
	def __init__(self, psk_tls_key=scp81_psk_tls_key, psk_dek=scp81_psk_dek, psk_id_diversified=scp81_psk_id_diversified, scp80=SCP80()):    
		self.psk_tls_key            = psk_tls_key
		self.psk_dek                = psk_dek
		self.psk_id_diversified     = psk_id_diversified
		self.scp80                  = scp80

	@allure.step
	def establish_tls_session(self, sw, success=True, notifType = "", interrupt=False):
		####### TLS HANDSHAKE #############################
		pprint.h4("OKAY 3")
		tls_server.InitTlsSession(self.tls_version, self.tls_ciphersuite, self.psk_id_diversified, self.psk_tls_key)
		data, sw = device.get_status()
		pprint.h4("OKAY 4 {}/{}".format(data, sw))
		tls_client_hello, sw = bip_channel.RcvDataFromCardViaBip(sw)    
		pprint.h4("OKAY 5")
		if success:
			device.fetch_all(sw)
			server_hello = tls_server.ProcessHandshake(tls_client_hello)
			client_key_and_cipher_spec, sw = bip_channel.RcvDataFromCardViaBip(bip_channel.SendDataToCardViaBip(server_hello))
			device.fetch_all(sw)
			server_cipher_choice_and_finish = tls_server.ProcessHandshake(client_key_and_cipher_spec)
			####################################################
			card_first_request_ciphered, sw = bip_channel.RcvDataFromCardViaBip(bip_channel.SendDataToCardViaBip(server_cipher_choice_and_finish))
			device.fetch_all(sw)
			first_card_request_string = http_client.ParseRequest(tls_server.DecipherData(card_first_request_ciphered))
			LogInfo("first_card_request_string "+str(first_card_request_string))
			LogInfo("\n"+first_card_request_string)
			# euicc_notification_sequence_number = pprint.look_for_euicc_notification(first_card_request_string)
			euicc_notification_sequence_number = pprint.look_for_euicc_notification(first_card_request_string, notifType=notifType)
			if interrupt:
				data, sw = device.envelope_timer_expiration(id='biplink')
				device.fetch_all(sw)
			else:
				return euicc_notification_sequence_number
		else:
			# data, sw = Envelope('D60E190109020282813802810037013A')
			# data, sw = device.fetch_one(sw)
			# data, sw = Envelope('D60E190109020282813802810037013A')
			# device.fetch_all(sw)
			# data, sw = device.fetch_one(sw, 'setup event list')
			data, sw = device.fetch_one(sw)
			data, sw = device.fetch_one(sw)
			data, sw = Envelope('D60E190109020282813802810037013A')
			data, sw = device.fetch_one(sw)
			# data, sw = Fetch(sw[2:])
			# device.get_status()
			# sw = TerminalResponse('810301040002028281030100')

			# assert sw == '910B', 'expected to be 910B'
			# if sw == '910B':
			#     data, sw = device.fetch_one(sw)
			# data, sw = device.fetch_one(sw)
			# data, sw = device.fetch_one(sw)
			# data, sw = Envelope('D60E190109020282813802810037013A')
			# data, sw = device.fetch_one(sw, 'polling off')
			# data, sw = device.fetch_one(sw, 'receive data')
			# data, sw = device.fetch_one(sw)

	@allure.step
	def exchange_application_data(self, http_body='AA0F220D80F24002074F005C034F9F7000', http_header_next_uri="/server/adminagent?cmd=2", header_targeted_app='//aid/A000000559/1010FFFFFFFF8900000100', chunkedExpectedPor=False, content_type='RAM', chunk = '01', interrupt=False, pertrubation=None):
		
		if content_type == 'RAM' :
			list = [
						(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_CONTENT_TYPE, HttpAmdB.CONTENT_TYPE_RAM),
						(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_NEXT_URI, http_header_next_uri),
						(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_TARGETED_APPLI, header_targeted_app)
					]
			pprint.h2('EXCHANGE DATA RAM MODE')


		if content_type == 'RFM' :
			list = [
					(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_CONTENT_TYPE, HttpAmdB.CONTENT_TYPE_RFM),
					(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_NEXT_URI, http_header_next_uri),
					(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_TARGETED_APPLI, header_targeted_app)
					]
			pprint.h2('EXCHANGE DATA RFM MODE')

		
		if chunk == '00' : 	chunk = False
		else : 	chunk = True
		
		pprint.h4("CHUNK : {}".format(chunk))
		####### SERVER RESPONSE TO FIRST CARD POST REQUEST ###
		http_server.MakeHeader(HttpAmdB.TEST_MODE, list)
		server_first_response = http_server.CreateResponse(http_body, chunked=chunk)
		http_server.DisplayResponse(server_first_response[0])
		#commented on 20190219 due to added for new test script
		# server_first_response_ciphered = tls_server.CipherData(server_first_response[1])
		
		# second_card_request_ciphered, sw = bip_channel.RcvDataFromCardViaBip(bip_channel.SendDataToCardViaBip(server_first_response_ciphered))
		
		split_http_size=1000
		http_body_splits = [ server_first_response[1][x*split_http_size : x*split_http_size + split_http_size] for x in range(len(server_first_response[1])//split_http_size +1) ]
		i=0
		for http_body_split in http_body_splits:
			if pertrubation == None:
				pass					
			elif pertrubation == 'auto service' or pertrubation == 'auto applet':
				pprint.h4("Pertubation with Auto Service")
				if i==14 or i==19 :
					data, sw = SendAPDU('0070000001')
					data, sw = SendAPDU('01A4040010A000000077010C60110000FE00000400')
					data, sw = SendAPDU('8132000000')
					data, sw = SendAPDU('0070800100')	

					data, sw = SendAPDU('0070000001')
					data, sw = SendAPDU('01A4040010A000000077010C60110000FE00000400')
					data, sw = SendAPDU('8120000000')
					data, sw = SendAPDU('0070800100')	

					data, sw = SendAPDU('0070000001')
					data, sw = SendAPDU('01A4040010A000000077010C60110000FE00000400')
					data, sw = SendAPDU('8132010000')
					data, sw = SendAPDU('0070800100')	

					# data, sw = SendAPDU('80C2000005EE03EF0120')			
			
			elif 'psm' in pertrubation:
				if i==5:
					pprint.h4("Pertubation with timer expired {}".format(pertrubation))
					data, sw = device.envelope_timer_expiration(id=pertrubation)
					if sw != '9000':
						data, sw, last_proactive_cmd = device.fetch_all(sw, return_if='refresh')
			
			elif pertrubation == 'power-off' or pertrubation == 'reset':				
				if i==3:
					pprint.h4("Pertubation with Device Reset")
					PowerOff()
					PowerOn()
			
			elif pertrubation == 'bussy call':
				pprint.h4("Pertubation with Bussy Call")
				# if i > 0 :
				data_channel, sw = SendAPDU('00 70 00 00 01')
				# data, sw = SendAPDU(data_channel+'A404043BA0000000871002FFFFFFFF89030200006229820278218410A0000000871002FFFFFFFF89030200008A01058B032F060AC609900140830101830181')
				# data, sw = SendAPDU(data_channel+'A4040C10A0000000770100001D1000FE00000300')
				# data, sw = SendAPDU('8{}320100'.format(data_channel[1]))
				# data, sw = SendAPDU('8{}F2020C'.format(data_channel[1]))
				# data, sw = SendAPDU('8{}F2020C'.format(data_channel[1]))
				# data, sw = SendAPDU(data_channel+'A4044C10A0000000871002FFFFFFFF8903020000')
				# data, sw = SendAPDU(data_channel+'A40804')

				if data_channel != None:
					data, sw = SendAPDU(data_channel+'A4040010A000000077010C60110000FE00000400')
					data, sw = SendAPDU('8{}320000'.format(data_channel[1]))
					# data, sw = SendAPDU('8{}F2020C'.format(data_channel[1]))
					data, sw = SendAPDU('8{}320100'.format(data_channel[1]))
				data, sw = SendAPDU('00 70 80 '+data_channel)
			
			elif pertrubation == 'emergency profile':
				pprint.h4("Pertubation with Emergency Profile activation")
				capdu, sw = device.envelope_local_enable('emergency')
				if '91' in sw:
					device.fetch_all(sw,return_if='refresh')
			
			elif pertrubation == 'link drop':
				if i == 3:
					pprint.h4("Pertubation with Link Drop")
					data, sw = Envelope('D60B19010A0202828138020105')

					# data, sw = SendAPDU('0070800100')	

					# data, sw = SendAPDU('0070000001')
					# data, sw = SendAPDU('01A4040010A000000077010C60110000FE00000400')
					# data, sw = SendAPDU('8132000000')
					# data, sw = SendAPDU('8132010000')
					# data, sw = SendAPDU('0070800100')	
					if sw != '9000':
						device.fetch_all(sw)
					return None
			
			elif pertrubation == 'emergency profile':
				# pprint.h4("Pertubation with Emergency Profile")
				if i==14 or i==19 :
					pprint.h4("Pertubation with Emergency Profile")
					data, sw = device.envelope_local_enable(profile='emergency' )

			# elif pertrubation == 'power-off' or pertrubation == 'reset':
			# 	if i == 8:
			# 		pprint.h4("Pertubation with Device Reset")
			# 		PowerOff()
			# 		EUICC.init()
			
			elif pertrubation == 'negative event':				
				if i==5:
					pprint.h4("Pertubation with Negative Event")
			
			elif pertrubation == 'negative event':				
				if i==5:
					pprint.h4("Pertubation with Negative Event")
					data, sw = device.envelope_limited_service()
					data, sw = device.envelope_limited_service()
					data, sw = device.envelope_location_status(status='no service')

			server_first_response_ciphered = tls_server.CipherData(http_body_split)			
			timer_start = time()
			data_from_server = bip_channel.SendDataToCardViaBip(server_first_response_ciphered)
			timer_end = time()
			pprint.h4("timing data from server : {}".format(timer_end-timer_start))
			###################################################
			# # data_from_server = bip_channel.SendDataToCardViaBip(server_first_response_ciphered)
			# data_server = bip_channel.SendEnvDataAvailable(server_first_response_ciphered)
			# # data, sw = device.get_status()
			# while '91' in data_server:
			# 	data, sw = Fetch(data_server[2:4])
			# 	data_from_server = bip_channel.TRespRcvData(data=server_first_response_ciphered, dataLen=len(server_first_response_ciphered))
			# # pprint.h4("data from server"+data_from_server)
			###################################################
			i+=1

		if interrupt:
			pprint.h2('INTERRUPTION')
			data, sw = device.envelope_timer_expiration(id='biplink')

		second_card_request_ciphered, sw = bip_channel.RcvDataFromCardViaBip(data_from_server)
		device.fetch_all(sw)
		if not interrupt:
			second_card_request = tls_server.DecipherData(second_card_request_ciphered)
			second_card_request_string = http_client.ParseRequest(second_card_request, chunked=True)
			LogInfo(second_card_request_string)
			second_card_request_splits = second_card_request_string.split('\r\n')
			http_body_por = second_card_request_splits[-3]
	
			if http_body_por == 'AF80230269850000': LogInfo('6985, conditions of use not satisfied!')
			if http_body_por == 'AF8023026A880000': LogInfo('6A88, referenced data not found!')
			if http_body_por == 'AF80230269E10000': LogInfo('69E1, PCF-Rules do not allow this request') 
	
			assert http_body_por != 'AF802303009000230269820000', 'security status not satisfied'
			assert http_body_por != 'AF8023026A820000', 'application not found'
			assert http_body_por != 'AF8023026A800000', 'incorrect values in command data'
			assert http_body_por != 'AF80230265810000', 'memory failure'
			assert http_body_por != 'AF8023026A840000', 'not enough memory space'
			assert http_body_por != 'AF80230294840000', 'algorithm not supported'
			assert http_body_por != 'AF80230294850000', 'invalid key check value'
			assert http_body_por != 'AF8023026A810000', 'function not supported (e.g. card life cycle state is card_locked)'
		
			return http_body_por

	def send_204_and_keep_tls_session_alive(self, http_header_next_uri="/server/adminagent?cmd=2"):
		list = [
					(HttpAmdB.REMOVE_HEADER, HttpAmdB.HEADER_CONTENT_TYPE),
					(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_NEXT_URI, http_header_next_uri),
					(HttpAmdB.REMOVE_HEADER, HttpAmdB.HEADER_TARGETED_APPLI)
					# (HttpAmdB.REMOVE_HEADER, HttpAmdB.CONTENT_LENGTH)
				]

		http_server.MakeHeader(HttpAmdB.TEST_MODE, list)

		server_first_response = http_server.CreateResponse(apdu='',status='204 No Content')
		http_server.DisplayResponse(server_first_response[0])
		server_first_response_ciphered = tls_server.CipherData(server_first_response[1])
		tls_alert_ciphered, sw = bip_channel.RcvDataFromCardViaBip(bip_channel.SendDataToCardViaBip(server_first_response_ciphered))
		device.fetch_one(sw)
		# tls_server.CloseTlsConnection()
		# tls_server.CloseTlsSession()
		return sw
	
	@allure.step
	def close_tls_session(self):
		# if not fail_http:
		last_server_response = http_server.LastResponse()
		http_server.DisplayResponse(last_server_response[0])
		last_server_response_ciphered = tls_server.CipherData(last_server_response[1])
		tls_alert_ciphered, sw = bip_channel.RcvDataFromCardViaBip(bip_channel.SendDataToCardViaBip(last_server_response_ciphered))
		if sw!= '9000':
			data, sw, cmd_type_name = device.fetch_all(sw, 'close channel')
		tls_server.CloseTlsConnection()
		tls_server.CloseTlsSession()
		return sw
	
	@allure.step
	def remote_management(self, apdus, pull=False, header_targeted_app='//aid/A000000559/1010FFFFFFFF8900000100', no_length=False, apdu_format="indefinite", content_type='RAM',chunk='01', notificationTypeExp = "",pertrubation=None):
		assert pull != '9000', 'nothing to pull!'
		
		if not pull:
			pprint.h4("open channel remote management")
			script, rapdu, sw = self.scp80.push_sms()
			pprint.h4("OKAY {}/{}/{}".format(script, rapdu, sw))
			data, sw = device.fetch_one(sw, 'open channel')
			pprint.h4("OKAY2 {}/{}".format(data, sw))
			pull = sw
		
		pprint.h4("establish tls session")
		euicc_notification_sequence_number = self.establish_tls_session(pull, notifType = notificationTypeExp)
		# print(euicc_notification_sequence_number)
		LogInfo("euicc_notification_sequence_number: "+str(euicc_notification_sequence_number))
		
		if apdus == None: return None, None, self.close_tls_session(), euicc_notification_sequence_number
			
		if isinstance(apdus, str):
			# handle notification confirmation
			if euicc_notification_sequence_number:
				pprint.h3("check euicc notification sequence number")
				apdus = apdus.replace('XXXX', euicc_notification_sequence_number)
			# euicc_notification_sequence_number = "0002"
			apdus = [apdus]

		if not no_length:
			capdu_value = ''
			for apdu in apdus:
				capdu_value += '22' + berLv(apdu)
		else:
			capdu_value = apdus[0]

		if apdu_format == "compact" :
			capdu = ''
			for apdu in apdus:
				capdu += apdu
				print(capdu)
				LogInfo('Remote APDU format: Compact format')

		if apdu_format == "definite" :
			capdu = 'AA' + berLv(capdu_value)
			LogInfo('Remote APDU format: Expanded format, Definite Mode')

		if apdu_format == "indefinite" : 
			capdu = 'AE80' + capdu_value + '0000'
			LogInfo('Remote APDU format: Expanded format, Indefinite Mode')

		pprint.h4("exchange application data")
		rapdu = self.exchange_application_data(capdu, header_targeted_app=header_targeted_app, content_type=content_type, chunk = chunk,pertrubation=pertrubation)
		pprint.h4("close tls session")
		sw = self.close_tls_session()
		# if sw != '9000':
		# 	data, sw, last_proactive_cmd = device.fetch_all(sw)
		return capdu, rapdu, sw
   

if __name__ == '__main__':
	SetCurrentReader('DE620 Contact Reader')
	InitHandset()
	scp80 = SCP80(spi1=0x12, spi2=0x21, kic=0x10, kic_key=scp80_kic, kid=0x10, kid_key=scp80_kid, tar='000001', algo_cipher=const.hexa.ciphers['AES'], algo_crypto_verif=const.hexa.ciphers['AES'])

	pass