from model.gsma 								import Gsma
from model.gp 									import Gp
from model.device 								import Device
from model.ota									import SCP80, SCP81
from model.euicc 								import Euicc
from model.server 								import Smsr
from model.applet_local_management_plugin 		import LocalManagementApplet
from model.ingenico_reader 						import IngenicoReader
from model.applet_ota_poller			 		import OtaPoller
from model.applet_high_stress_memory			import HighStressMemory
from model.testlink_server						import TestlinkServer
from model.applet_location_plugin				import LocationPlugin
from model.applet_era_glonass					import EraGlonass
from model.applet_local_swap					import LocalSwap
from model.auto_service_applet import AutoService