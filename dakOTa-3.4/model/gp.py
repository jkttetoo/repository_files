
import allure
from Oberthur import *
from util import pprint
from util import hexastring
from terminaltables import AsciiTable

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

class Gp:

	''' GET STATUS '''
	
	@property
	@allure.step
	def audit_amdb_parameters(self):
		return '80CA008500'

	@property
	@allure.step
	def audit_applet_list(self):
		return '80F24000024F0000'

	# def put_key_single(self, test):
	#     return '80D80000'

	# def put_key_multiple(self, test):
	#     return '80D80080'
	
#     def pprint_audit_applet_list(self, data,scp,apdu_format):

#         life_cycle_state = {
#             '03': 'Installed',
#             '07': 'Selectable',
#             '0F': 'Personalized',
#             '87': 'Locked',
#             '05': 'Test'
			
#         }

#         privileges_short_names = {
#             'Contactless Self Activation': "CSA" , 
#             'Contactless Activation': "CA" , 
#             'Ciphered Load File Block' :"CLFDB"  ,
#             'Receipt Generation': "RG"  ,
#             'Global Service' : "GS" ,
#             'Final Application' : "FA" ,
#             'Global Registry' : "GR" ,
#             'Global Lock' : "GL" ,
#             'Global Delete' : "GD" ,
#             'Token Management' : "TM" ,
#             'Authorized Management' : "AM" ,
#             'Trusted Path' : "TP" ,
#             'Mandated DAP Verification' : "MDAPV"  ,
#             'CVM Management' : "CVMM" ,
#             'Card Reset' : "CR" ,
#             'Card Terminated' : "CT"  ,
#             'Card Lock' : "CL" ,
#             'Delegated Management' : "DM"  ,
#             'DAP Verification' : "DAPV"  ,
#             'Security Domain' : "SD"
#         }


#         privileges_byte_1_get_status = { # there is 3 bytes to describe privileges but get status returns only the 1st
#             'Security Domain': '1XXXXXXX',
#             'DAP Verification': '11XXXXX0',
#             'Delegated Management': '1X1XXXXX',
#             'Card Lock': 'XXX1XXXX',
#             'Card Terminate': 'XXXX1XXX',
#             'Card Reset': 'XXXXX1XX',
#             'CVM Management': 'XXXXXX1X',
#             'Mandated DAP Verification': '11XXXXX1'
#         }


#         list_aid                         = ['AID']
#         list_life_cycle_state             = ['Life Cycle State']
#         list_privileges                    = ['Privileges']

#         print("len(data): ",len(data))
# #        if apdu_format == "indefinite"  :
# #            if len(data) >= 127 :
# #                start_response = 'AF80238187'
# #            else:
# #                start_response = 'AF80230D'

#         if apdu_format == "indefinite"  :
#             if len(data) >= 256 :
#                 start_response = 'AF8023828187'
#             elif len(data) >= 127 :
#                 start_response = 'AF80238187'
#             else:
#                 start_response = 'AF80230D'
		
# #        if apdu_format == "definite" : 
# #            if len(data) >=127 : 
# #                start_response = 'AB818D800101238187'
# #            else :
# #                start_response = 'AB53800101234E'
				
#         if apdu_format == "definite" :
#             if len(data) >=256 :
#                 start_response = 'AB82818D80010123828187'  
#             if len(data) >=127 : 
#                 start_response = 'AB818D800101238187'
#             else :
#                 start_response = 'AB53800101234E' 
				
#         if apdu_format == "compact"  : start_response = '029000'
				
			

#         temp = data
#         data = data[len(start_response) :]
		
#         while(len(data)>8*2):
#             aid_len = data[0:2]
#             list_aid.append( data[2:2+int(aid_len,16)*2] )
#             list_life_cycle_state.append( '{} {}'.format(data[2+int(aid_len,16)*2:2+int(aid_len,16)*2+2], life_cycle_state[data[2+int(aid_len,16)*2:2+int(aid_len,16)*2+2]]))            
#             privileges_first_byte = data[2+int(aid_len,16)*2+2:2+int(aid_len,16)*2+4]
#             list_privileges.append('{} {}'.format(privileges_first_byte, [ privileges_short_names[privilege_name] for privilege_name, byte_pattern in privileges_byte_1_get_status.items() if hexastring.is_matching(byte_pattern, privileges_first_byte) ]) )            
#             data = data[2+int(aid_len,16)*2+4:]

#         pprint.table([list_aid, list_life_cycle_state, list_privileges])

#         return (temp)

	''' update fix error parsing on applet list 20180515'''
	@allure.step
	def pprint_audit_applet_list(self, data,scp,apdu_format):

		life_cycle_state = {
			'03': 'Installed',
			'07': 'Selectable',
			'0F': 'Personalized',
			'87': 'Locked',
			'05': 'Test'
			
		}

		privileges_short_names = {
			'Contactless Self Activation': "CSA" , 
			'Contactless Activation': "CA" , 
			'Ciphered Load File Block' :"CLFDB"  ,
			'Receipt Generation': "RG"  ,
			'Global Service' : "GS" ,
			'Final Application' : "FA" ,
			'Global Registry' : "GR" ,
			'Global Lock' : "GL" ,
			'Global Delete' : "GD" ,
			'Token Management' : "TM" ,
			'Authorized Management' : "AM" ,
			'Trusted Path' : "TP" ,
			'Mandated DAP Verification' : "MDAPV"  ,
			'CVM Management' : "CVMM" ,
			'Card Reset' : "CR" ,
			'Card Terminated' : "CT"  ,
			'Card Lock' : "CL" ,
			'Delegated Management' : "DM"  ,
			'DAP Verification' : "DAPV"  ,
			'Security Domain' : "SD"
		}


		privileges_byte_1_get_status = { # there is 3 bytes to describe privileges but get status returns only the 1st
			'Security Domain': '1XXXXXXX',
			'DAP Verification': '11XXXXX0',
			'Delegated Management': '1X1XXXXX',
			'Card Lock': 'XXX1XXXX',
			'Card Terminate': 'XXXX1XXX',
			'Card Reset': 'XXXXX1XX',
			'CVM Management': 'XXXXXX1X',
			'Mandated DAP Verification': '11XXXXX1'
		}


		list_aid                         = ['AID']
		list_life_cycle_state             = ['Life Cycle State']
		list_privileges                    = ['Privileges']

		print("len(data): ",len(data))

		if apdu_format == "indefinite"  :
			if len(data) >= 250 :
				start_response = 'AF8023810D'
			else:
				start_response = 'AF8023XX'
				
		if apdu_format == "definite"    : 
			if len(data) >=250 : 
				start_response = 'AB818D8001012381XX'
			else :
				start_response = 'AB5380010123XX'
				
		if apdu_format == "compact"  : start_response = '029000'
						 
		temp = data
		data = data[len(start_response) :]
		
		while(len(data)>8*2):
			aid_len = data[0:2]
			list_aid.append( data[2:2+int(aid_len,16)*2] )
			list_life_cycle_state.append( '{} {}'.format(data[2+int(aid_len,16)*2:2+int(aid_len,16)*2+2], life_cycle_state[data[2+int(aid_len,16)*2:2+int(aid_len,16)*2+2]]))            
			privileges_first_byte = data[2+int(aid_len,16)*2+2:2+int(aid_len,16)*2+4]
			list_privileges.append('{} {}'.format(privileges_first_byte, [ privileges_short_names[privilege_name] for privilege_name, byte_pattern in privileges_byte_1_get_status.items() if hexastring.is_matching(byte_pattern, privileges_first_byte) ]) )            
			data = data[2+int(aid_len,16)*2+4:]

		pprint.table([list_aid, list_life_cycle_state, list_privileges])

		return (temp)

	@property
	@allure.step
	def audit_sd_list(self):
		return '80F28000024F0000'

	@allure.step
	def pprint_audit_sd_list(self, data, scp=80,apdu_format="definite"):
		self.pprint_audit_applet_list(data,scp,apdu_format)
		return (data)