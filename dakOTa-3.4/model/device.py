from Mobile import *
from util.crypto import *
import collections
from util import hexastring
from util import pprint
from util.tools import *
from random import randint
import binascii
import allure
from time import time

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'


PROACTIVE_CMD_TYPES = {
	'01' : ('refresh', '810301010402028281030100'),
	'02' : 'more time', # This procedure is provided to allow the SIM Application Toolkit task in the SIM more time for processing, where the processing is so long that it is in danger of affecting normal GSM operation, and clock stop prevents processing to takeplace in the background.
	'03' : ('poll interval', '8103010300020282810301008402013E'),    # not sure // the maximum interval between two STATUS commands related to Proactive Polling.
	'04' : ('polling off', '810301040002028281030100'),      # not sure // This command disables the Proactive Polling
	'05' : ('setup event list', '810301050002028283030100'),
	'10' : 'setup call',
	'11' : 'send ss',
	'12' : 'send ussd',
	'13' : ('send sms', '810301130002028281830100'),
	'14' : 'send dtmf',
	'15' : 'launch browser',
	'20' : 'play tone',
	'21' : 'display text',
	'22' : 'get inkey',
	'23' : 'get input',
	'24' : 'select item',
	'25' : ('set up menu', '810301130002028281030100'), # appears after installation of jvi stk menu applet
	'26' : 'provide local info',
	# '27' : ('timer management', '810301270002028281030100240102'),
	'27' : ('timer management', '810301270002028281030100240101'),
	'28' : 'setup idle mode text',
	'30' : 'card apdu',
	'31' : 'power on card',
	'32' : 'power off card',
	'33' : 'get reader status',
	'34' : 'run at command',
	'35' : 'timer management',
	'40' : ('open channel', '81030140008202818203010038028100'),
	'41' : ('close channel', '81030141008202818203010038028100'),
	'42' : ('receive data', '810301420002028281030100'),
	# '43' : ('send data', '810301130002028281030100'),
	'43' : ('send data', '810301430182022181030100370100'),
	'44' : 'get channel status',

	'OK' : ('OK', '810301130002028281030100')
}

import Ot.GlobalPlatform as GP
from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain, IssuerSecurityDomain, AESKeyset
class Device:

	def get_proactive_cmd_type_from_data_fetched(self, data_fetched):
		half_tag_cmd_details    = '1'
		len_cmd_details         = '03'
		subval_cmd_number       = '01'

		index_cmd_type = data_fetched.find(half_tag_cmd_details+len_cmd_details+subval_cmd_number) + len(half_tag_cmd_details+len_cmd_details+subval_cmd_number)

		cmd_type_byte = data_fetched[index_cmd_type: index_cmd_type+2]

		cmd_type_name = PROACTIVE_CMD_TYPES[cmd_type_byte][0]

		data_terminal_reponse_successful = PROACTIVE_CMD_TYPES[cmd_type_byte][1]

		# !! fix to append timer_id if proactive command is timer mangement
		if cmd_type_name == 'timer management':
			# pprint.h3 ("timer management cmd_type_name")
			data_terminal_reponse_successful = data_terminal_reponse_successful[:-2] + data_fetched[-12:-10]
			# LogInfo("data_terminal_reponse_successful[:-2]: " + data_terminal_reponse_successful[:-2])
			# LogInfo("data_fetched[-12:-10]: " + data_fetched[-12:-10])

		return cmd_type_byte, cmd_type_name, data_terminal_reponse_successful



	def terminal_response(self, data_fetched, this_cmd_type=None, catch_first_notif=False, open_channel_failed=False, send_sms_failed=False, loci_change=False, refresh_failed=False, pli_failed=False, pli_result='ok', timer_management_failed=False, send_sms_2X=False):
		pprint.log("device", "terminal_response", "", Constants.debugMode)
		cmd_type_byte, cmd_type_name, data_terminal_reponse_successful = self.get_proactive_cmd_type_from_data_fetched(data_fetched)

		# LogInfo("cmd_type_name: "+str(cmd_type_name))
		# LogInfo("data_terminal_reponse_successful: "+str(data_terminal_reponse_successful))
		if cmd_type_name == 'set up menu':
			print('Set Up Menu triggered')
			return TerminalResponse('810301250082028281830100')
			
		if cmd_type_name == 'timer management':
			if '240101' in data_fetched:
				# return TerminalResponse('810301270002028281030100240101')
				if timer_management_failed:
					return TerminalResponse('810301270002028281030124240101')
				else:
					return TerminalResponse('810301270002028281030100240101')					
			elif '240102' in data_fetched:
				# return TerminalResponse('810301270002028281030100240102')
				if timer_management_failed:
					return TerminalResponse('810301270002028281030124240102')
				else:
					return TerminalResponse('810301270002028281030100240102')					
			elif '240103' in data_fetched:
				# return TerminalResponse('810301270002028281030100240103')
				if timer_management_failed:
					return TerminalResponse('810301270002028281030124240103')
				else:
					return TerminalResponse('810301270002028281030100240103')				
			elif '240104' in data_fetched:
				# return TerminalResponse('810301270002028281030100240104') 
				if timer_management_failed:
					return TerminalResponse('810301270002028281030124240104')
				else:
					return TerminalResponse('810301270002028281030100240104')				

		if cmd_type_name == 'p':
			if '03012601' in data_fetched:
				return TerminalResponse("8103012601820281828301001408 1111111122222212")
			elif '03012606' in data_fetched:
				return TerminalResponse('8103012606020282810301003F0108')
			else:
				if loci_change:
					if pli_failed:
						return TerminalResponse("81030126000202828103022004")
					else:
						if pli_result == 'ok':
							LogInfo ('loci_change activated')
							return TerminalResponse("8103012600820282818301001307" + self.randForLOCI())
						elif pli_result == 'limited service':
							return TerminalResponse("810301260002028281030106")
						elif pli_result == 'no service':
							return TerminalResponse("81030126000202828103022004")
				else:
					if pli_failed:
						return TerminalResponse("81030126000202828103022004")
					else:
						if pli_result == 'ok':
							LogInfo ('loci_change deactivated')
							#modify by hasbi removing duplicate tag and length (1307)
							return TerminalResponse("8103012600820282818301001307"+'12F89041235648')
						elif pli_result == 'limited service':
							return TerminalResponse("810301260002028281030106")
						elif pli_result == 'no service':
							return TerminalResponse("81030126000202828103022004")

		if cmd_type_name == 'provide local info':
			if '030126' in data_fetched:
				return TerminalResponse("8103012601820281828301001408 1111111122222212")
			else:
				return TerminalResponse("8103012600820282818301001307001F1032547698")

		if cmd_type_name == 'send sms':
			if send_sms_failed:
				return TerminalResponse('81030113000202828183013A')
			if send_sms_2X:
				return TerminalResponse('810301130082028281830221A6')
		
		if cmd_type_name == 'open channel':
			
			if open_channel_failed:
				data_terminal_reponse_successful = '81030140018202828183013A'
			else:
				tags = collections.OrderedDict()

				tags['buffer_size'] = '39'
				tags['network_access_name'] = '47'
				tags['sim_me_transport_layer_level'] ='3C' 
				tags['data_destination_addr'] = '3E'

				values = hexastring.parse_tlvs(data_fetched, tags, start='2028182') # consider data only after tlv device_ids

				transport_protocol = 'tcp' if values['sim_me_transport_layer_level'][0:2] == '02' else 'udp' # udp is 01
				port = int(values['sim_me_transport_layer_level'][2:6], 16)
				ipv4 = '{}.{}.{}.{}'.format(int(values['data_destination_addr'][2:4], 16), int(values['data_destination_addr'][4:6], 16), int(values['data_destination_addr'][6:8], 16), int(values['data_destination_addr'][8:10], 16))
				buffer_size = int(values['buffer_size'], 16)

				# transport_protocol = 'tcp' if data_fetched[-20:-18] == '02' else 'udp' # udp is 01
				# ipv4 = '{}.{}.{}.{}'.format(int(data_fetched[-8:-6], 16), int(data_fetched[-6:-4], 16), int(data_fetched[-4:-2], 16), int(data_fetched[-2:], 16))
				# port = int(data_fetched[-18:-14], 16)
				# buffer_size = int(data_fetched[-28:-24], 16)

				nan = hexastring.get_network_access_name(values['network_access_name'])

				LogInfo('bip channel {}: {}:{} | buffer {} bytes | {}'.format(transport_protocol, ipv4, port, buffer_size, nan))
				
				if transport_protocol == 'udp':
					pprint.h3("udp")
					LogInfo( 'Open Channel DNS request' )
					sw = TerminalResponse(data_terminal_reponse_successful)
					data, sw    = self.fetch_one(sw) # send data
					LogInfo('fetch send data dns query: {}'.format(data))

					# LogInfo("data before parsing tag 36: " + data)
					# LogInfo("sw before parsing tag 36: " + sw)

					#added new flow for BIPLink 10.7 DK3.4
					while True:
						#update aji 20180504 whats wrong with my parsing? O_o?					
						tags['tag_36'] = '36'
						values_tag_36 = getBytes(hexastring.parse_tlvs(data, tags, start='2028121')["tag_36"],1,2)
						# # LogInfo("values tag_36: " + values_tag_36["tag_36"])
						# LogInfo("values tag_36: " + values_tag_36)
						#update aji 20180504

						# LogInfo("data after parsing tag 36: " + data)
						# LogInfo("sw after parsing tag 36: " + sw)
						dns_data = pprint.look_for_dns_request(data)
		
						data, sw = self.fetch_one(sw) # timer
						data, sw = self.envelope_data_available()
						if sw == '9111' : data, sw = self.fetch_one(sw) # setup event list
						if sw == '9110' : data, sw = self.fetch_one(sw) # setup event list
						if sw == '910F' : data, sw = self.fetch_one(sw) # setup event list
						if sw == '910B' : data, sw = self.fetch_one(sw) # polling off
						data, sw = Fetch(sw[2:4]) # receive data
						# LogInfo( 'DNS server response: IP=....')
						#update aji 20180504
						# default
						# dns_response = '81030142008202218103010036 33 5378 8100 0001000200000000 06 6F74616C7465 06 74656C63656C 03 636F6D 0000010001 C00C000100010000825800 040AC96C67 370100'
						if "1C0001" in data:
							#dns_response_ipv6
							dns_response = '810301420002028281030100 36 81BA' + values_tag_36 + '81800001000100020004037777770866616365626F6F6B03636F6D00001C0001C00C00050001000007DF001109737461722D6D696E690463313072C010C0380002000100000BDD00070162026E73C038C0380002000100000BDD00040161C04DC05E0001000100000E04000445ABEF0BC05E001C000100000E0400102A032880FFFE000BFACEB00C00000099C04B0001000100000E04000445ABFF0BC04B001C000100000D1400102A032880FFFF000BFACEB00C00000099370100'
							first_query_char  = binascii.unhexlify('777777').decode("utf-8")
							# LogInfo("first_query_char: ", first_query_char)
							second_query_char = binascii.unhexlify('66616365626F6F6B').decode("utf-8")
							# LogInfo("second_query_char: ", second_query_char)
							third_query_char  = binascii.unhexlify('636F6D').decode("utf-8")
							# LogInfo("third_query_char: ", third_query_char)
							sw = TerminalResponse(dns_response)
						else:
							#dns_response_ipv4
							dns_response = '81030142008202218103010036 33'+values_tag_36 +'8100 0001000200000000 06 6F74616C7465 06 74656C63656C 03 636F6D 0000010001 C00C000100010000825800 040AC96C67 370100'

							# '''DLL value'''
							# dns_response = '81030142008202218103010036 33 8C4F 8100 0001000200000000 06 6F74616C7465 06 74656C63656C 03 636F6D 0000010001 C00C000100010000825800 040AC96C67 370100'
							#update aji 20180504
							sw = TerminalResponse(dns_response)

							first_query_char  = binascii.unhexlify('6F74616C7465').decode("utf-8")
							# LogInfo("first_query_char: ", first_query_char)
							second_query_char = binascii.unhexlify('74656C63656C').decode("utf-8")
							# LogInfo("second_query_char: ", second_query_char)
							third_query_char  = binascii.unhexlify('636F6D').decode("utf-8")
							# LogInfo("third_query_char: ", third_query_char)

						LogInfo("\n")
						LogInfo('#'*78)
						LogInfo('dns server response:')
						LogInfo('-----------------------------')
						LogInfo(dns_response[30:])
						#update aji 20180504
						# default
						# LogInfo("DNS transaction id     : 5378")

						LogInfo("DNS transaction id     : "+values_tag_36)
						# '''DLL value'''
						# LogInfo("DNS transaction id     : 8C4F")
						
						#update aji 20180504
						LogInfo("DNS flag               : 8100")
						LogInfo("DNS query              : {} {} {}".format(first_query_char, second_query_char, third_query_char))
						LogInfo("DNS data class         : 0000010001")
						LogInfo('#'*78)

						data, sw = self.fetch_one(sw) # If fetch SEND DATA it will continue loop for request IPv4
						if '030141' in data:
							break
						elif '030143' in data:
							dns_data = pprint.look_for_dns_request(data)

					# print("Close Channel")
					LogInfo("Close Channel")
					if '030141' in data:
						data, sw = Fetch(sw[2:4])

#		if cmd_type_name == 'refresh':
#			if refresh_failed:
#				return TerminalResponse("810301010402028281030120")


		if cmd_type_name == 'refresh':
			if refresh_failed:
				return TerminalResponse("81030101040202828103022002")

		if cmd_type_name == "d":
			return TerminalResponse("810301210082020281830100")
		
		if cmd_type_name == "display text":
			return TerminalResponse("810301210082020281830100")
							   
		if this_cmd_type:
			assert cmd_type_name == this_cmd_type, 'expected proactive command did not happen!'

		LogDebug('terminal response ' + cmd_type_name)

		# if open_channel_failed:
		# 	data_terminal_reponse_successful = '81030140018202828183013A'

		return TerminalResponse(data_terminal_reponse_successful)


	def envelope_location_status(self, status='normal service', i=1):
		if status == 'normal service':
			# data, sw = Envelope('D615190103020282811B0100130912F410CBA35992020A')
			data, sw = Envelope('D615190103820282811B0100130903022789081614406F')
		if status == 'no service':
			data, sw = Envelope('D60A190103020282811B0102')
		if status == 'link drop':
			data, sw = Envelope('D60B19010A0202828138020105')

		LogInfo('envelope location status: {} ({})'.format(status, sw))

		return data, sw

	def envelope_network_rejection(self):
		return Envelope('D617190112020283817D0562F01081893F010874010A75010F')

	def envelope_limited_service(self):
		return Envelope('D60A190103020282811B0101')


	def envelope_timer_expiration(self, id='psm' or 'biplink'):
		LogInfo('envelope timer expiration ('+id+')')
		# if(getSAAAAXCode() == "41594"):
		
		# added for DAKOTA 3.4		
		if id == 'psm2':
			timer_id    = '04'
			timer_val   = '000200' # 00h 02min 00s

		elif id == 'psm5':
			timer_id    = '05'
			timer_val   = '000200' # 00h 02min 00s

		elif id == 'psm4':
			timer_id    = '04'
			timer_val   = '000200' # 00h 02min 00s

		elif id == 'other':
			timer_id    = '04'
			timer_val   = '000200' # 00h 02min 00s

		elif id == 'psm':
			timer_id    = '03'
			timer_val   = '000200' # 00h 02min 00s

		elif id == 'ota poller':
			timer_id    = '02'
			timer_val   = '000200' # 00h 20min 00s

		elif id == 'biplink':
			timer_id    = '01'
			timer_val   = '000200' # 00h 02min 00s

		# added for DAKOTA 3.4 NG
		# if id == 'psm3':
		# 	timer_id    = '03'
		# 	timer_val   = '000200' # 00h 02min 00s

		# elif id == 'psm5':
		# 	timer_id    = '04'
		# 	timer_val   = '000200' # 00h 02min 00s

		# elif id == 'psm4':
		# 	timer_id    = '03'
		# 	timer_val   = '000200' # 00h 02min 00s

		# elif id == 'other':
		# 	timer_id    = '03'
		# 	timer_val   = '000200' # 00h 02min 00s

		# elif id == 'psm':
		# 	timer_id    = '02'
		# 	timer_val   = '000200' # 00h 02min 00s
					
		# elif id == 'psm2':
		# 	timer_id    = '02'
		# 	timer_val   = '000200' # 00h 02min 00s

		# elif id == 'biplink':
		# 	timer_id    = '01'
		# 	timer_val   = '000200' # 00h 02min 00s

		pprint.log("device", "envelope_timer_expiration", "envelope", Constants.debugMode)
		sendEnvelope = Envelope('D70C020282812401' + timer_id + '2503' +  timer_val)            
		 
		return sendEnvelope

	def envelope_data_available(self):
		''' channel id=1 / data length = 48'''
		LogInfo('envelope data available')
		return Envelope('D60E1901098202218138028100370148')

	def envelope_local_enable(self, profile='emergency' or 'test'):
		if profile == 'emergency':
			val = '00'
		elif profile == 'test':
			val = '02'
		LogInfo('envelope local enable ' + profile + ' profile')
		return Envelope('E401' + val)

	def envelope_local_disable(self, profile='emergency' or 'test'):
		if profile == 'emergency':
			val = '01'
		elif profile =='test':
			val = '03' 
		LogInfo('envelope local disable ' + profile + ' profile')
		return Envelope('E401' + val)


	def terminal_profile(self, tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008", mode="3G" or "2G"):
		SetMode(mode)
		LogDebug('terminal profile')
		# return TerminalProfile(inputData='FFFFFFFFFFFF1FFFFF0302FFFF9FFFEFDFFF0FFF0FFFFF0FFF03003F7FFF03')
		if(getSAAAAXCode() == "41594"):
			# tP = TerminalProfile(inputData='F71FE8FF1F9C00879600001F22600000C3C0000000014000500000000008')
			#use only for unsecure DLL byte 5 = no "location status", byte 25 = no "network rejection"
			tP = TerminalProfile(inputData= tp_data)
		else:
			# tP = TerminalProfile(inputData='F71FE8CE1F9C00879600001F22600000C3C0000000014000500000000008')
			#added by hasbi in Sprint 15
			# tP = TerminalProfile(inputData='F71FE8DE1F9C00C79600001FE2000000C3F0000700016000500000000008')
			return TerminalProfile(inputData='F71FE8FF1F9C00879600001F22600000C3C0000000014000500000000008')
		return tP

	def fetch_all(self, sw, return_if=[], catch_first_notif=False, loci_change=False, refresh_failed=False, open_channel_failed=False, send_sms_failed=False, timer_management_failed=False, pli_failed=False, pli_result='ok', send_sms_2X=False):
		# pprint.log("device - fetch_all", Constants.debugMode)
		pprint.log("device", "fetch_all", "", Constants.debugMode)
		assert sw[0:2] == '91', 'nothing to fetch!'
		if isinstance(return_if, str):
			return_if = [return_if]

		while '91' in sw:
			LogDebug('fetch')
			data, sw = Fetch(sw[2:4])				
			sw = self.terminal_response(data, catch_first_notif=False, loci_change=loci_change, refresh_failed=refresh_failed, open_channel_failed=open_channel_failed, send_sms_failed=send_sms_failed,timer_management_failed=timer_management_failed, pli_failed=pli_failed, pli_result=pli_result, send_sms_2X= send_sms_2X)		
			cmd_type_name = self.get_proactive_cmd_type_from_data_fetched(data)[1]			
			if cmd_type_name in return_if or sw[0:2] != '91':
				return data, sw, cmd_type_name
			
			# StepOn()
			# if '030142' in data or '030143' in data:
			# 	data_channel, sw = SendAPDU('00 70 00 00 01')
			# 	if data_channel != None:
			# 		SendAPDU(data_channel+'A4040010A000000077010C60110000FE00000400')
			# 		SendAPDU('8{}320000'.format(data_channel[1]))
			# 		# data, sw = SendAPDU('8{}F2020C'.format(data_channel[1]))
			# 		SendAPDU('8{}320100'.format(data_channel[1]))
			# 	SendAPDU('00 70 80 '+data_channel)
			
		LogInfo("########## FETCH ALL DONE ##########")
	# def fetch_one(self, sw, this_cmd_type=None, catch_first_notif=False, open_channel_failed=False):
	#     assert sw[0:2] == '91', 'nothing to fetch!'
	#     LogDebug('fetch')
	#     pprint.log("device", "fetch_one", "fetch", Constants.debugMode)
	#     data, sw = Fetch(sw[2:4])
	#     pprint.log("device", "fetch_one", "terminal response", Constants.debugMode)
	#     sw = self.terminal_response(data, this_cmd_type, catch_first_notif=False, open_channel_failed=open_channel_failed)
	#     return data, sw


	'''20180525: prastaji : added expected Data'''
	def fetch_one(self, sw, this_cmd_type=None, catch_first_notif=False, open_channel_failed=False, send_sms_failed=False, pli_failed=False, expectedData = None):
		assert sw[0:2] == '91', 'nothing to fetch!'
		LogDebug('fetch')
		data, sw = Fetch(sw[2:4], expectedData = expectedData)
		sw = self.terminal_response(data, this_cmd_type, catch_first_notif=False, open_channel_failed=open_channel_failed, send_sms_failed=send_sms_failed, pli_failed=pli_failed)
		return data, sw    

	# def get_por(self, data, spi2):

	#     # ton_npi                 = '81'
	#     # addr                    = '280102296117'
	#     tp_protocol_id          = '00'
	#     tp_data_coding_scheme   = 'F6'

	#     assert self.get_proactive_cmd_type_from_data_fetched(data)[1] == 'send sms'

	#     LogDebug('-'*20)
	#     LogDebug('DATA:     ' + data)
		
	#     tp_user_data_len_index  = data.find(tp_protocol_id + tp_data_coding_scheme) + len(tp_protocol_id + tp_data_coding_scheme)
	#     tp_user_data_len        = data[tp_user_data_len_index:tp_user_data_len_index+2]

	#     UDHL_index = tp_user_data_len_index+2
	#     UDHL = data[UDHL_index: UDHL_index+2]
	#     LogDebug('UDHL:     ' + UDHL)

	#     IEIDLa_index = UDHL_index+4
	#     IEIDLa = data[IEIDLa_index: IEIDLa_index+2]
	#     LogDebug('IEIDLa:   ' + IEIDLa)

	#     first_sms   = True 
	#     more_sms    = False

	#     if IEIDLa != '00':

	#         IEDa_index = IEIDLa_index+2
	#         IEDa_3bytes = data[IEDa_index: IEDa_index+6]
	#         LogDebug('nb_SMS:   ' + IEDa_3bytes[2:4])
	#         LogDebug('SMS#:     ' + IEDa_3bytes[4:6])

	#         if IEDa_3bytes[2:4] != IEDa_3bytes[4:6]:
	#             more_sms = True

	#         if int(IEDa_3bytes[4:6], 16) > 1:
	#             first_sms = False

	#     CPL_index = UDHL_index + 2 + int(UDHL, 16)*2
	#     CPL = data[CPL_index: CPL_index+4]
	#     LogDebug('CPL:      ' + CPL)

	#     CHL_index = CPL_index + 4
	#     CHL = data[CHL_index: CHL_index+2]
	#     LogDebug('CHL:      '+ CHL)


	#     if spi2 == 0x39:
	#         if first_sms:
	#             por_index = CHL_index + 2 + 3*2 #3*2 is the TAR length
	#         else:
	#             por_index = CPL_index

	#     else:
	#         por_index = CHL_index + 2 + int(CHL, 16)*2
	#     LogDebug('PoR:      '+ data[por_index:])
	#     LogDebug('-'*20)
	#     return data[por_index:], more_sms

	'''new update for mno list applet 20180515'''
	def get_por(self, data, spi2):

		# ton_npi                 = '81'
		# addr                    = '280102296117'
		tp_protocol_id          = '00'
		tp_data_coding_scheme   = 'F6'

		assert self.get_proactive_cmd_type_from_data_fetched(data)[1] == 'send sms'

		LogDebug('-'*20)
		LogDebug('DATA:     ' + data)
		
		tp_user_data_len_index  = data.find(tp_protocol_id + tp_data_coding_scheme) + len(tp_protocol_id + tp_data_coding_scheme)
		tp_user_data_len        = data[tp_user_data_len_index:tp_user_data_len_index+2]

		UDHL_index = tp_user_data_len_index+2
		UDHL = data[UDHL_index: UDHL_index+2]
		LogDebug('UDHL:     ' + UDHL)

		IEIDLa_index = UDHL_index+4
		IEIDLa = data[IEIDLa_index: IEIDLa_index+2]
		LogDebug('IEIDLa:   ' + IEIDLa)

		first_sms   = True 
		more_sms    = False

		if IEIDLa != '00':

			IEDa_index = IEIDLa_index+2
			IEDa_3bytes = data[IEDa_index: IEDa_index+6]
			LogDebug('nb_SMS:   ' + IEDa_3bytes[2:4])
			LogDebug('SMS#:     ' + IEDa_3bytes[4:6])

			if IEDa_3bytes[2:4] != IEDa_3bytes[4:6]:
				more_sms = True

			if int(IEDa_3bytes[4:6], 16) > 1:
				first_sms = False

		CPL_index = UDHL_index + 2 + int(UDHL, 16)*2
		CPL = data[CPL_index: CPL_index+4]
		LogDebug('CPL:      ' + CPL)

		CHL_index = CPL_index + 4
		CHL = data[CHL_index: CHL_index+2]
		LogDebug('CHL:      '+ CHL)


		if spi2 == 0x39:
			if first_sms:
				por_index = CHL_index + 2 + 3*2 #3*2 is the TAR length
			else:
				por_index = CPL_index

		else:
			if first_sms:
				por_index = CHL_index + 2 + int(CHL, 16)*2
			else:
				por_index = CPL_index
			

		LogDebug('PoR:      '+ data[por_index:])
		LogDebug('-'*20)
		return data[por_index:], more_sms    


	def fetch_por_sms(self, sw, scp80):
		assert sw[0:2] == '91', 'nothing to fetch!'
 
		data, sw, cmd_type_name = self.fetch_all(sw, return_if='send sms')
		por, more_sms = self.get_por(data, scp80.spi2)
	  
		while more_sms:
			data, sw = self.fetch_one(sw)
			new_por_fragment, more_sms = self.get_por(data, scp80.spi2)
			por += new_por_fragment

		deciphered_por, sw = decipher_por_aes_128(por) if scp80.spi2 == 0x39 else por, sw
		more_get_status = deciphered_por[-4:] == '6310'

		return deciphered_por, more_get_status, sw


	def look_for_refresh(self, max_nb_env=20):
		''' attempt to trigger fallback rollback mechanism by sending multiple envelope location status indicating no service '''
		
		for i in range(max_nb_env):
			data, sw = envelope_location_status('no service', i+1)
			if sw[0:2] == '91': return sw

		LogInfo('!! no fall/roll back despite {} envelopes location status no service !!'.format(max_nb_env))

		return sw

	def get_status(self, status=None):
		if status != None:
			return SendAPDU(status)
		else:
			return SendAPDU('80F2000C00')

	def randForLOCI(self):
		i = 0
		loci = ""
		for i in range (14):
			loci += str(randint(0,9))
		return loci

if __name__ == '__main__':
	SetCurrentReader('OMNIKEY CardMan 5x21 0') #'DE620 Contact Reader'
	displayAPDU(True)
