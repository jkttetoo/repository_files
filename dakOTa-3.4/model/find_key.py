from Oberthur import *
import binascii

def select_ecasd():
	return Send('00 A4 04 00 10A0000005591010FFFFFFFF8900000200')

def get_euicc_id():
	PowerOn()
	select_ecasd()
	data, sw = SendAPDU('80CA005A12')
	return str.encode(data[4:])

def get_sin():
	return SendAPDU('80 CA 00 42 0C')

def get_sdin():
	return SendAPDU('80 CA 00 45 08')


class Hsm:

    master_keys = {

        #'master_key_label': 'master_key_value'
        'MSK.EUICC.HARMAN.3DES.00000001'  : '9716FBD383C1AEDFEF46375D830226C7',
        'MSK.EUICC.HARMAN.AES.00000001'   : '15D39F7515FC53532DFDB8019FD9E578',
		'MSK.EUICC.OT.DES.00000001' 	  : '9716FBD383C1AEDFEF46375D830226C7',
		'MSK.EUICC.INGENICO.3DES.00000001': None

    }

class MCC_eUICC_Profile():
	def __init__(self, name, ctd, config, valuation, rc, isdr_scp81_master_key_label):
		name = name
		ctd = ctd
		config = config
		valuation = valuation
		rc = rc
		isdr_scp81_master_key_label = isdr_scp81_master_key_label

#euicc_id_daimler            = '6364160310000094337F000000000004'
#euicc_id_ingenico           = '89033024208100491100000000000248'

init_vector_null            = '00' * 8

HSM = Hsm()

SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('eSIM 3.1.rc11')

tetiana_rc7_dns_issue = MCC_eUICC_Profile(name='DNS_eUICC_IOT_DAKOTA_3.1_INGENICO_LM', ctd='eUICC DakOTa v3.1 RC5 IO222', config='200_DNS_GTO_ VF_RC7_newLM_HTTP_DAKOTA_3.1_78.47.26.178:65001', valuation="HTTP_CI_INGENICO2_VF_NOAP", rc=7, isdr_scp81_master_key_label='MSK.EUICC.INGENICO.3DES.00000001')


isdr_scp81_psk_id_diversified = "38303031303238313130" +get_euicc_id()+ "344631304130303030303035353931303130464646464646464638393030303030313030383230313031383330313431"
psk_tls_key_isdr_ingenico = DES3_CBC(get_euicc_id(), HSM.master_keys['MSK.EUICC.HARMAN.3DES.00000001'], init_vector_null)

isdp_scp03_kenk = AES_CBC_ENC_128('0F'+'01'+get_euicc_id(), HSM.master_keys['MSK.EUICC.HARMAN.AES.00000001'], init_vector_null)
isdp_scp03_kmac = AES_CBC_ENC_128('0F'+'02'+get_euicc_id(), HSM.master_keys['MSK.EUICC.HARMAN.AES.00000001'], init_vector_null)
isdp_scp03_kkek = AES_CBC_ENC_128('0F'+'03'+get_euicc_id(), HSM.master_keys['MSK.EUICC.HARMAN.AES.00000001'], init_vector_null)


print('eID', get_euicc_id())
print('isdp_scp03_kenk', isdp_scp03_kenk)
print('isdp_scp03_kmac', isdp_scp03_kmac)
print('isdp_scp03_kkek', isdp_scp03_kkek)
print('psk_tls_key_isdr', psk_tls_key_isdr_ingenico)
print('psk_id_diversified',isdr_scp81_psk_id_diversified)

