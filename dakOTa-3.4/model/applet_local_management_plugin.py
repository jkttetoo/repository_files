import allure
from Oberthur import *
from Mobile import *
from Ot.GlobalPlatform import *


class LocalManagementApplet:

	''' cf. Local_Management_CR_RnD Answer_V1.0_TA_draft in ../doc/ot/ '''

	# aid = 'A0000000770307601100FE0000300001'
	aid = Constants.config['Local_Management_Applet']['AID']
	cap_file = None
	version = '1.0'
	
	@allure.step
	def select(self, ch_number):
		capdu 		= ch_number + 'A40400' + lv(self.aid)
		data, sw 	= SendAPDU(capdu)
		LogInfo('ISO: select applet local management applet ({})'.format(sw))
		return capdu, data, sw

	@allure.step
	def enable_profile(self, aid_isdp):
		capdu 		= '81010000' + lv(aid_isdp)
		data, sw 	= SendAPDU(capdu)
		LogInfo('ISO: enable profile via local management applet in channel 1 ({})'.format(sw))
		return capdu, data, sw

	@allure.step
	def disable_profile(self, aid_isdp):
		LogInfo("Applet local management plugin - disable profile")
		capdu 		= '81020000' + lv(aid_isdp)
		data,sw 	= SendAPDU(capdu)
		LogInfo('ISO: disable profile via local management applet in channel 1 ({})'.format(sw))
		return capdu, data, sw

	@allure.step
	def delete_profile(self, aid_isdp):
		LogInfo("Applet local management plugin - delete profile")
		capdu 		= '81030000' + lv(aid_isdp)
		data,sw 	= SendAPDU(capdu)
		LogInfo('ISO: delete profile via local management applet in channel 1 ({})'.format(sw))
		return capdu, data, sw

	@allure.step
	def set_fallback_profile(self, aid_isdp):
		capdu		= '81040000' + lv(aid_isdp)
		data, sw	= SendAPDU(capdu)
		LogInfo('ISO: local set fallback profile via local management applet in channel 1 ({})'.format(sw))
		return capdu, data, sw

	@allure.step
	def get_eid(self):
		LogInfo('ISO: get eid information via local management applet')
		capdu 		=  '8016000012'
		data, sw 	= SendAPDU(capdu)
		return  capdu, data , sw

	@allure.step
	def get_local_management_config(self):
		LogInfo('ISO: get local management applet information')
		return SendAPDU('8024000002') 

	@allure.step
	def get_euicc_info(self):
		# current limitation : if more data available, it is not retrieved
		LogInfo('ISO: get euicc info in channel 1')
		capdu 		=  '8118000000'
		data, sw 	= SendAPDU(capdu)
		if sw[:2] == '6C':
			capdu		= '81180000'+sw[2:]
			data, sw	= SendAPDU(capdu)
		return  capdu, data , sw

	@allure.step
	def get_local_management_config(self):
		LogInfo('ISO: get local management applet information')
		capdu	 = '8024000002'
		data, sw = SendAPDU(capdu)
		return  capdu, data , sw


################# Allow/Disallow Local Management Command

	def allow_every_command(self):
		LogInfo('ISO: allow every local management command')
		return SendAPDU('8020000001 07')

	def disallow_every_command(self):
		LogInfo('ISO: disallow every local management command')
		return SendAPDU('8020000001 00')

	def only_allow_enable_command(self):
		LogInfo('ISO: allow enable command for local management applet')
		return SendAPDU('8020000001 01')

	def only_allow_disable_command(self):
		LogInfo('ISO: allow disable command for local management applet')
		return SendAPDU('8020000001 02')

	def only_allow_delete_command(self):
		LogInfo('ISO: allow delete command for local management applet')
		return SendAPDU('8020000001 04')

	def only_disallow_enable_command(self):
		LogInfo('ISO: disallow enable command')
		return SendAPDU('8020000001 06')

	def only_disallow_disable_command(self):
		LogInfo('ISO: disallow disable command')
		return SendAPDU('8020000001 05')

	def only_disallow_delete_command(self):
		LogInfo('ISO: disallow delete command')
		return SendAPDU('8020000001 03')


################# Set eUICC notification

	def activate_euicc_notification(self):
		capdu = '802200000101'
		data, sw = SendAPDU(capdu)
		LogInfo('ISO: activate euicc notification ({})'.format(sw))
		return capdu,  data, sw

	def deactivate_euicc_notification(self):
		capdu = '802200000100'
		data, sw = SendAPDU(capdu)
		LogInfo('ISO: deactivate euicc notification ({})'.format(sw))
		return capdu,  data, sw