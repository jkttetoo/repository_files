import allure
from Oberthur import *
from Mobile import *
from Ot.GlobalPlatform import *
import model
from util import pprint

__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'prastoto.aji@idemia.com'
'''
__created__ = 04 June 2018
__purpose__ = Auto Service applet API

'''

SERVER	= model.Smsr()		
EUICC = model.Euicc()
GSMA = model.Gsma()
DEVICE = model.Device()

class AutoService:

	aid = Constants.config['Auto_Service_Applet']['AID']
	cap_file = None
	version = '1.0'
	
	@allure.step
	def open_logical_channel(self, ch_number):
		pprint.h3('open logical channel 0'+ch_number)
		capdu	 = '00700000' + '0' + ch_number
		data, sw = SendAPDU(capdu)
		# return capdu, data, sw

	@allure.step
	def close_logical_channel(self, ch_number):
		pprint.h3('close logical channel 0'+ch_number)
		capdu	 = '007080' + '0' + ch_number + '00'
		data, sw = SendAPDU(capdu)
		if sw != "9000":
			LogInfo("Do Refresh")
			data, sw, cmd_type = DEVICE.fetch_all(sw, return_if="refresh")
			if cmd_type == "refresh":
				EUICC.init()

	# def selectApplet(self):
	@allure.step
	def selectApplet(self, channel = "1"):
		pprint.h3("iso: select auto service applet")
		# capdu = '00A4040010' + self.aid
		capdu = '0'+channel+'A4040010' + self.aid
		data,sw = SendAPDU(capdu)
		# LogInfo("data: "+str(data))
		# LogInfo('ISO: select applet Era Glonass ({})'.format(sw))
		return data, sw

	@allure.step
	def setEmergencyProfile(self, isdp, channel = "1"):
		# data,sw = self.selectApplet()
		# self.selectApplet()
		data, sw = self.selectApplet(channel=channel)
		pprint.h3("iso: auto service applet set emergency profile")
		capdu = "8"+channel +" 26 00 00 " + lv("ED"+lv(isdp))
		data, sw = SendAPDU(capdu)
		# LogInfo("data: "+str(data))
		return data, sw
	
	@allure.step
	def emergencyProfileActivation(self, channel = "1"):
		data, sw = self.selectApplet(channel=channel)
		pprint.h3("iso: auto service applet emergency profile activation")
		capdu = "8"+channel+" 20 00 00 00"
		data, sw = SendAPDU(capdu)
		return data,sw
	
	@allure.step
	def emergencyProfileDeactivation(self, channel = "1"):
		data, sw = self.selectApplet(channel=channel)
		pprint.h3("iso: auto service applet emergency profile deactivation")
		capdu = "8"+channel+" 24 00 00 00"
		data, sw = SendAPDU(capdu)
		return data,sw
	
	@allure.step
	def activateNotification(self, channel = "1"):
		data, sw = self.selectApplet(channel=channel)
		pprint.h3("iso: auto service applet active notification ")
		capdu = "8"+channel+" 28 00 00 00"
		data, sw = SendAPDU(capdu)
		return data,sw
	
	@allure.step
	def deactivateNotification(self, channel = "1"):
		data, sw = self.selectApplet(channel=channel)
		pprint.h3("iso: auto service applet deactivate notification")
		capdu = "8"+channel+" 28 01 00 00"
		data, sw = SendAPDU(capdu)
		return data, sw
	
	@allure.step
	def getEUICCInfo(self, channel = "1", le = "00", expectedData = None):
		data, sw = self.selectApplet(channel=channel)
		'''support le = 00, return all data as much as the buffer allowed'''
		'''limited to transmit 255 bytes (FF) profiles with le 240 bytes (F0)'''
		pprint.h3("iso: auto service applet get euicc info")
		capdu = "8"+channel+" 34 00 00 "+le
		data, sw = SendAPDU(capdu, expectedData = expectedData, expectedStatus = "9000")
		return data, sw

	@allure.step
	def mConnectLock(self, channel = "1", le = "00"):
		data, sw = self.selectApplet(channel=channel)
		pprint.h3("iso: M-Connect lock")
		capdu = "8"+channel+" 32 00 00 " + le
		data, sw = SendAPDU(capdu)
		return data, sw

	@allure.step
	def mConnectUnlock(self, channel = "1", le = "00"):
		data, sw = self.selectApplet(channel=channel)
		pprint.h3("iso: M-Connect unlock")
		capdu = "8"+channel+" 32 01 00 " + le
		data, sw = SendAPDU(capdu)
		return data, sw

		

	'''
	via toolkit
	cla 80 or A0
	'''
	# def setEmergencyProfileViaToolkit(self, aid, cla = "80"):
	# 	pprint.h3("envelope: auto service applet set emergency profile")
	# 	capdu = cla + "C20000" + lv("EE"+lv("EF 01 26" + "ED"+lv(aid)))
	# 	data, sw = SendAPDU(capdu)
	# 	return data, sw
	@allure.step
	def setEmergencyProfileViaToolkit(self, aid, cla = "80"):
		pprint.h3("envelope: auto service applet set emergency profile")		
		# SetMode("3G")
		output = Envelope("EE"+lv("EF 01 26" + "ED"+lv(aid)), expectedStatus = "9000")
		return output

	# def emergencyProfileActivationViaToolkit(self, cla = "80"):
	# 	pprint.h3("envelope: auto service applet emergency profile activation")
	# 	# capdu = cla + "C20000" + lv("EE"+lv("EF"+lv("20")))
	# 	# data, sw = SendAPDU(capdu)
	# 	# return data, sw	
	@allure.step
	def emergencyProfileActivationViaToolkit(self, expData='D0 09 81 03 01 01 04 82 02 81 82', dataTR='81 03 01 01 04 82 02 82 81 83 01 00', cla = "80"):
		pprint.h3("envelope: auto service applet emergency profile activation")
		# SetMode("3G")
		data, sw = Envelope("EE"+lv("EF"+lv("20")))
		if sw != '9000':
			data, sw, cmd_type = DEVICE.fetch_all(sw)
		
		#SPRINT 18
		#data, sw  = SendAPDU('A0C0000002')
		

		# data, sw = Fetch(sw[2:], expectedData = expData, expectedStatus = "9000" )
		# sw = TerminalResponse(dataTR, expectedStatus = "9000")
		return sw
	
	# def emergencyProfileDeactivationViaToolkit(self, cla = "80"):
	# 	pprint.h3("envelope: auto service applet emergency profile deactivation")
	# 	capdu = cla + "C20000" + lv("EE"+lv("EF"+lv("24")))
	# 	data, sw = SendAPDU(capdu)
	# 	return data, sw	
	@allure.step
	def emergencyProfileDeactivationViaToolkit(self, expData='D0 09 81 03 01 01 04 82 02 81 82', dataTR='81 03 01 01 04 82 02 82 81 83 01 00', cla = "80"):
		pprint.h3("envelope: auto service applet emergency profile deactivation")
		# SetMode("3G")
		data, sw = Envelope("EE"+lv("EF"+lv("24")), expectedStatus = "91XX")
		data, sw = Fetch(sw[2:], expectedData = expData, expectedStatus = "9000" )
		sw = TerminalResponse(dataTR, expectedStatus = "9000")
		return sw

	@allure.step
	def activateNotificationViaToolkit(self, cla = "80"):
		data,sw = self.selectApplet
		pprint.h3("envelope: auto service applet active notification ")
		capdu = cla + "C20000" + lv("EE"+lv("EF"+lv("28")+"EC"+lv("00")))
		data, sw = SendAPDU(capdu)
		return data, sw

	@allure.step
	def deactivateNotificationViaToolkit(self, cla = "80"):
		data,sw = self.selectApplet
		pprint.h3("envelope: auto service applet deactivate notification")
		capdu = cla + "C20000" + lv("EE"+lv("EF"+lv("28")+"EC"+lv("01")))
		data, sw = SendAPDU(capdu)
		return data, sw

	@allure.step
	def tearDownTest(self):
		pprint.h2("teardown")
		EUICC.init()
		capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list[0:28]+ '00C0000000', 80, apdu_format='compact')		
		data, sw = self.mConnectUnlock()
		data, sw = self.emergencyProfileDeactivation()
		capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list[0:28]+ '00C0000000', 80, apdu_format='compact')

		EUICC.init()
		pprint.h3("enable profile 11")
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11 ), 80, apdu_format='definite', chunk='01')
		if '9000' in rapdu: 
			sw = SERVER.euicc.refresh(sw, False)

		EUICC.init()
		pprint.h3("set emergency profile 10")
		data, sw = self.setEmergencyProfile(Constants.DEFAULT_ISDP10)
		pprint.h3("activate emergency profile 10")
		data, sw = self.emergencyProfileActivation()

		EUICC.init()
		pprint.h3("enable profile 11")
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11 ), 80, apdu_format='definite', chunk='01')
		if '9000' in rapdu: 
			sw = SERVER.euicc.refresh(sw, False)
		
		EUICC.init()
		capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list[0:28]+ '00C0000000', 80, apdu_format='compact')
		
		SERVER.disable_profile(Constants.AID_ISDP17, scp=80, network_service=False, apdu_format='indefinite')
		SERVER.delete_profile(Constants.AID_ISDP17, scp=80, apdu_format='definite')
		
		SERVER.disable_profile(Constants.AID_ISDP18, scp=80, network_service=False, apdu_format='indefinite')
		SERVER.delete_profile(Constants.AID_ISDP18, scp=80, apdu_format='indefinite')
		DEVICE.get_status()

		EUICC.init()
		self.mConnectUnlock()					
		self.emergencyProfileDeactivation()		
		self.setEmergencyProfile(Constants.DEFAULT_ISDP10)		

		#tambahan 20190304
		EUICC.init()
		capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
		LogInfo("rapdu: "+str(rapdu))



	# def select_naa(self):
	# 	capdu = '00A4040C07A0000000090001'
	# 	# capdu = '00A4040C0F A0000000 871002FF FFFFFF89 030100'
	# 	data, sw = SendAPDU(capdu)
	# 	LogInfo('ISO: select NAA ({})'.format(sw))

	# 	return data, sw
	

	# def glonass_profile_activation_via_applet_selection(self):
	# 	capdu = '8020000000'
	# 	data, sw = self.select
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def glonass_profile_activation_via_process_toolkit(self):
	# 	capdu = 'A0C2000005EE03EF0120'
	# 	''' [Need to find select NAA] '''
	# 	data, sw = self.select_naa()
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def glonass_profile_deactivation_via_applet_selection(self):
	# 	capdu = '8024000000'
	# 	data, sw = self.select
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def glonass_profile_deactivation_via_process_toolkit(self):
	# 	capdu = 'A0C2000005EE03EF0124'
	# 	''' [Need to find select NAA] '''
	# 	data, sw = self.select_naa()
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def set_glonass_profile_via_applet_selection(self, aid):
	# 	capdu = '80260000 12 ED 10' + aid
	# 	data, sw = self.select
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def set_glonass_profile_via_process_toolkit(self, aid):
	# 	capdu = 'A0C20000 17 EE15EF0126 ED 10 '+ aid
	# 	''' [Need to find select NAA] '''
	# 	data, sw = self.select_naa()
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw


	# def get_glonass_profile_via_applet_selection(self):
	# 	capdu = '8026010002'
	# 	data, sw = self.select
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def get_glonass_profile_via_process_toolkit(self):
	# 	capdu = 'A0C20000 05 EE03EF0126'
	# 	''' [Need to find select NAA] '''
	# 	data, sw = self.select_naa()
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def activate_notification_via_applet_selection(self):
	# 	capdu = '8028000000'
	# 	data, sw = self.select
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def activate_notification_via_process_toolkit(self):
	# 	capdu = 'A0C20000 08 EE06EF0128EC0100'
	# 	''' [Need to find select NAA] '''
	# 	data, sw = self.select_naa()
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def deactivate_notification_via_applet_selection(self):
	# 	capdu = '8028010000'
	# 	data, sw = self.select
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def deactivate_notification_via_process_toolkit(self):
	# 	capdu = 'A0C20000 08 EE06EF0128EC0101'
	# 	''' [Need to find select NAA] '''
	# 	data, sw = self.select_naa()
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw


	# def mconnect_lock_via_applet_selection(self):
	# 	capdu = '8032000000'
	# 	data, sw = self.select
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	# def mconnect_unlock_via_applet_selection(self):
	# 	capdu = '8032010000'
	# 	data, sw = self.select
	# 	data, sw = SendAPDU(capdu)

	# 	return data, sw

	

if __name__ == '__main__':
	autoservice = AutoService()
