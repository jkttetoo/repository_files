from Oberthur import *

class Hsm:

    master_keys = {

        #'master_key_label': 'master_key_value'
        'MSK.EUICC.HARMAN.3DES.00000001' 	: '9716FBD383C1AEDFEF46375D830226C7',
        'MSK.EUICC.HARMAN.AES.00000001'  	: '15D39F7515FC53532DFDB8019FD9E578',
        'MSK.EUICC.INGENICO.3DES.00000001'	: ''
    }

if __name__ == '__main__':

	euicc_id_daimler            = '6364160310000094337F000000000004'
	euicc_id_ingenico           = '89033024208100491100000000000248'

	init_vector_null            = '00' * 8

	HSM = Hsm()

	psk_tls_key_isdr_ingenico = DES3_CBC(euicc_id_ingenico, HSM.master_keys['MSK.EUICC.HARMAN.3DES.00000001'], init_vector_null)
	print('\npsk_tls_key_isdr_ingenico', psk_tls_key_isdr_ingenico)


	isdp_scp03_kenk = AES_CBC_ENC_128('0F'+'01'+euicc_id_daimler[4:], HSM.master_keys['MSK.EUICC.HARMAN.AES.00000001'], init_vector_null)
	isdp_scp03_kmac = AES_CBC_ENC_128('0F'+'02'+euicc_id_daimler[4:], HSM.master_keys['MSK.EUICC.HARMAN.AES.00000001'], init_vector_null)
	isdp_scp03_kkek = AES_CBC_ENC_128('0F'+'03'+euicc_id_daimler[4:], HSM.master_keys['MSK.EUICC.HARMAN.AES.00000001'], init_vector_null)


	print('\nisdp_scp03_kenk', isdp_scp03_kenk)
	print('isdp_scp03_kmac', isdp_scp03_kmac)
	print('isdp_scp03_kkek', isdp_scp03_kkek)