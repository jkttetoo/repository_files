import allure
from Oberthur import *
from Mobile import *
from Ot.GlobalPlatform import *
import model

SERVER 	= model.Smsr()
EUICC   = model.Euicc()

class LocalSwap:

	aid = Constants.config['Local_Swap_Applet']['AID']
	cap_file = None
	version = '1.0'
	
	@property	
	@allure.step
	def select(self):
		capdu = '00A4040010' + self.aid
		data, sw = SendAPDU(capdu)
		LogInfo('ISO: select applet Local Swap ({})'.format(sw))

		return data, sw

	@allure.step
	def change_to_factory_profile(self):
		capdu 		= '80C2000009' + 'D3070202018110017E'

		data, sw 	= self.select
		data, sw 	= SendAPDU(capdu)
		LogInfo('ISO: Change to SKT Factory profile ({})'.format(sw))
		return capdu, data, sw

	@allure.step
	def change_to_skt_profile(self):
		capdu 		= '80C2000009' + 'D3070202018110017F'

		data, sw 	= self.select
		data,sw 	= SendAPDU(capdu)
		LogInfo('ISO: Change to SKT #N profile ({})'.format(sw))
		return capdu, data, sw

	# def set_factory_profile(self, aid_isdp, scp=80, apdu_format='indefinite'):
	@allure.step
	def set_factory_profile(self, aid, scp=80, apdu_format='indefinite'):
		capdu 			 = "80 E2 88 00 " + lv("DF6D" + lv("30" + lv(aid)))
		LogInfo("open secure channel")
		EUICC.open_secure_channel_isdr()
		LogInfo("store data for set factory profile SKT module")
		data, sw = SendAPDU(capdu, expectedStatus = "9000")
		return capdu, data, sw

	@allure.step
	def set_factory_profile_w_expected(self, aid, scp=80, apdu_format='indefinite', expectedStatus= "9000" ):
		capdu 			 = "80 E2 88 00 " + lv("DF6D" + lv("30" + lv(aid)))
		print("open secure channel")
		EUICC.open_secure_channel_isdr()
		print("store data for set factory profile SKT module")
		data, sw = SendAPDU(capdu, expectedStatus = expectedStatus)
		return capdu, data, sw