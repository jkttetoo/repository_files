
import allure
from Oberthur import *
from Mobile import *
from Ot.GlobalPlatform import *
import model

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER	= model.Smsr()

DETECTION_CONDITION = {
	'mcc mnc' 				: ('E10102', 2),
	'mnc'	  				: ('E10101', 1),
	'mcc'	  				: ('E10100', 0)
}

TRIGGERING_CONDITION = {
	'power-on only' 		: ('E20101', 4),
	'detection only'		: ('E20102', 8),
	'power-on and detection': ('E20103', 12)
}

class LocationPlugin:

	aid = Constants.config['Location_Plugin_Applet']['AID']
	cap_file = None
	version = '1.0'
	
	@property	
	@allure.step
	def select(self):
		capdu = '00A4040010' + self.aid
		data, sw = SendAPDU(capdu)
		LogInfo('ISO: select applet Location Plugin ({})'.format(sw))

		return data, sw

	@allure.step
	def select_naa(self):
		capdu = '00A4040C0CA0000000871002FFFFFFFF89'
		# capdu = '00A4040C0F A0000000 871002FF FFFFFF89 030100'
		data, sw = SendAPDU(capdu)
		LogInfo('ISO: select NAA ({})'.format(sw))

		return data, sw

	@allure.step
	def send_via_update_smsr(self, capdu, scp, apdu_format):
		capdu, rapdu, sw = SERVER.ram(capdu, scp=scp, apdu_format=apdu_format)

		return capdu, rapdu, sw

	@allure.step
	def send_via_applet_selection(self, capdu):
		data, sw = self.select
		data, sw = SendAPDU(capdu)

		return data, sw

	@allure.step
	def location_plugin_activation(self, interface, scp=80, apdu_format='definite'):
		if interface == 'smsr':
			capdu = '80E288000F3A070CA307810505850282F23301FF'
			capdu, data, sw = self.send_via_update_smsr(capdu, scp=scp, apdu_format=apdu_format)

		elif interface == 'applet':
			capdu = '8040010000'
			data, sw = self.send_via_applet_selection(capdu)

		return data, sw

	@allure.step
	def location_plugin_deactivation(self, interface, scp=80, apdu_format='definite'):
		if interface == 'smsr':
			capdu = '80E288000F 3A070CA307810505850282F233017F'
			capdu, data, sw = self.send_via_update_smsr(capdu, scp=scp, apdu_format=apdu_format)

		elif interface == 'applet':
			capdu = '8040000000'
			data, sw = self.send_via_applet_selection(capdu)
			
		return data, sw

	@allure.step
	def location_plugin_update_configuration(self, interface, detection_condition='mcc mnc', triggering_condition='detection only', timer_interval=30, max_counter=16, scp=80, apdu_format='definite'):
		configuration = ''
		if interface == 'smsr':
			configuration += str(DETECTION_CONDITION[detection_condition][1] + TRIGGERING_CONDITION[triggering_condition][1])

			capdu = '80E288000F3A070CA307810505850282F23401{:02X}'.format(int(configuration))
			capdu, data, sw = self.send_via_update_smsr(capdu, scp=scp, apdu_format=apdu_format)

		elif interface == 'applet':
			configuration += DETECTION_CONDITION[detection_condition][0]
			configuration += TRIGGERING_CONDITION[triggering_condition][0]
			configuration += 'E302{:02X}'.format(int(timer_interval)) + '{:02X}'.format(int(max_counter))

			capdu = '80420000'+lv(configuration)
			data, sw = self.send_via_applet_selection(capdu)

		return data, sw

if __name__ == '__main__':
	locationplugin = LocationPlugin()
