import allure
from Oberthur import *
from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain
from Ot.GlobalPlatform import getVersion as GlobalPlatformModuleGetVersion
from util import pprint, aid, hexastring
from time import time
import model

__author__      = 'Joel Viellepeaenable_profileu'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

gsma    = model.Gsma()

class Smsr:

	def __init__(self, euicc=model.Euicc()):
		self.euicc = euicc

	@property
	@allure.step
	def audit(self):
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.audit_isdp_list, 80)
		timer_end = time()
		return gsma.pprint_audit_isdp_list(rapdu)

	def ram(self, capdus, scp=81 or 80, pull=False, header_targeted_app='//aid/A000000559/1010FFFFFFFF8900000100', no_length=False, apdu_format='indefinite',chunk = '01', tpoa='0681214365', interrupt=False, pertrubation=None):
		if scp == 81: 
			return self.euicc.isdr.scp81.remote_management(capdus, pull, header_targeted_app, no_length, apdu_format=apdu_format,content_type='RAM', chunk=chunk, pertrubation=pertrubation)
		if scp == 80: 
			return self.euicc.isdr.scp80.remote_management(capdus, apdu_format=apdu_format, tpoa=tpoa)

	def wrap_isdp_scp03(self,capdus):
		self.euicc.isdr.scp03(capdus)
		return       

	@allure.step
	def create_isdp_and_establish_keyset(self, aid_isdp, scp=81, apdu_format='definite'):
		timer_start = time()
		pprint.h3("create isdp and establish keyset "+str(aid_isdp))
		pprint.h4("install 4 install")
		capdu, rapdu, sw = self.ram(gsma.install4install(aid_isdp), scp, apdu_format=apdu_format)
		# assert '9000' in rapdu, 'install4install failed'
		pprint.h4("update smsr anna")
		capdu, rapdu, sw = self.ram(gsma.update_smsr_anna(aid_isdp), scp, apdu_format=apdu_format)
		pprint.h4("install 4 perso")
		capdu, rapdu, sw = self.ram([gsma.install4perso(aid_isdp), gsma.store_dp_certificate], scp, apdu_format=apdu_format)


		# assert '9000' in rapdu, 'install4perso + store_dp_certificate failed'
		random_challenge = rapdu[rapdu.find('8510')+4:rapdu.find('8510')+36]
		LogInfo("random challenge: {}".format(random_challenge))
		pprint.h4("store isdp keys")
		capdu, rapdu, sw = self.ram(gsma.store_isdp_keys(self.euicc.isdr.sd, random_challenge), scp, apdu_format=apdu_format)
		receipt = rapdu[ rapdu.find('8610') + 4 : rapdu.find('8610')+36 ]
		LogInfo("receipt: {}".format(receipt))
		timer_end = time()
		LogInfo ("duration : {}".format(timer_end - timer_start))
		
		### added by hasbi : to return the status of ISDP creation ###
		if gsma.verify_receipt(receipt):
			create_isdp_and_establish_keyset_status=True
		else:
			create_isdp_and_establish_keyset_status=False

		# self.store_data_for_master_delete(aid_isdp=aid_isdp)

		return create_isdp_and_establish_keyset_status
		# self.euicc.isdr.scp80.remote_management(gsma.store_sdin, scp=80, header_targeted_app='//aid/A000000151/000000', apdu_format=apdu_format, tpoa='0681214365')

		#############################################################

	@allure.step
	def enable_profile(self, aid_isdp, scp, network_service, apdu_format='definite', chunk='01',tpoa='0681214365'):
		# pprint.h3("enable profile "+str(aid_isdp))
		# LogInfo("server ram enable profile")
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.enable_profile(aid_isdp), scp, apdu_format=apdu_format, chunk=chunk)
		LogInfo("server ram enable profile after syntax self.ram(gsma.enable_profile)")
		if '9000' in rapdu: 
			LogInfo("refresh under server ram enable profile")
			sw = self.euicc.refresh(sw, network_service)
		timer_end = time()
		LogInfo ("duration : {}".format(timer_end - timer_start))
		return capdu, rapdu, sw

	@allure.step
	def disable_profile(self, aid_isdp, scp, network_service, apdu_format='definite', chunk='01', tpoa='0681214365'):
		# pprint.h3("disable profile "+str(aid_isdp))
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.disable_profile(aid_isdp), scp, apdu_format=apdu_format, chunk=chunk)
		if '9000' in rapdu: sw = self.euicc.refresh(sw, network_service)
		timer_end = time()
		LogInfo ("duration : {}".format(timer_end - timer_start))
		LogInfo('SW disable = {}'.format(sw))
		return capdu, rapdu, sw

	@allure.step
	def set_fallback_attribute(self, aid_isdp, scp, apdu_format='definite', chunk='01'):
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.set_fallback_attribute(aid_isdp), scp, apdu_format=apdu_format, chunk=chunk)
		timer_end = time()
		LogInfo ("duration : {}".format(timer_end - timer_start))
		return capdu, rapdu, sw

	@allure.step
	def set_fallback_enabled_profile_only(self, scp, apdu_format='definite', chunk='01'):
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.setfallback_enabled_profile_only, scp, apdu_format=apdu_format, chunk=chunk)
		timer_end = time()
		LogInfo ("duration : {}".format(timer_end - timer_start))
		return capdu, rapdu, sw

	@allure.step
	def set_fallback_enabled_disabled_profile(self, scp, apdu_format='definite', chunk='01'):
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.setfallback_enabled_disabled_profile, scp, apdu_format=apdu_format, chunk=chunk)
		timer_end = time()
		LogInfo ("duration : {}".format(timer_end - timer_start))
		return capdu, rapdu, sw

	@allure.step
	def set_fallback_attribute_via_store_data(self, aid_isdp, scp, apdu_format='definite', chunk='01'):
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.setfallback_profile_via_store_data(aid_isdp), scp, apdu_format=apdu_format, chunk=chunk)
		timer_end = time()
		LogInfo ("duration : {}".format(timer_end - timer_start))
		return capdu, rapdu, sw

	@allure.step
	def delete_profile(self, aid_isdp, scp, apdu_format='definite', chunk='01'):
		# pprint.h3("delete profile "+str(aid_isdp))
		return self.ram(gsma.delete_profile(aid_isdp), scp, apdu_format=apdu_format, chunk=chunk)

	@allure.step
	def audit_isdp_list(self, scp, apdu_format='definite'):
		# pprint.h3("audit isdp list")
		if apdu_format == 'compact' :
			timer_start = time()
			capdu, rapdu, sw = self.ram(gsma.audit_isdp_list[0:28]+ '00C0000000', scp, apdu_format= apdu_format)   
			timer_end = time()
			LogInfo ("duration : {}".format(timer_end - timer_start))
		else: 
			timer_start = time() 
			capdu, rapdu, sw = self.ram(gsma.audit_isdp_list, scp, apdu_format=apdu_format)
			timer_end = time()
			LogInfo ("duration : {}".format(timer_end - timer_start))
		# gsma.pprint_audit_isdp_list(rapdu, display=True)
		# return (timer_end - timer_start)
		return gsma.pprint_audit_isdp_list(rapdu, display=True)

	@allure.step
	def audit_isdp_enabled(self, scp, apdu_format='definite'):
		return self.ram(gsma.audit_isdp_enabled, scp)

	@allure.step
	def audit_isdp_disabled(self, scp, apdu_format='definite'):
		return self.ram(gsma.audit_isdp_disabled, scp)

	@allure.step
	def audit_profile_memory(self, aid_isdp, scp, apdu_format='definite'):
		capdu, rapdu, sw = self.ram(gsma.audit_profile_memory(aid_isdp), scp)
		LogInfo(rapdu)
		memory_info = rapdu[rapdu.find('9104')+4:rapdu.find('9104')+12]

		LogInfo("memory info :{} bytes".format(int(memory_info,16)))

		return 

	@allure.step
	def push_sms_test(self, scp, apdu_format='definite'):
		return self.ram("8100", scp,apdu_format=apdu_format)

	@allure.step
	def audit_resource_info(self, scp=80, apdu_format='definite'):
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.audit_resource_info, scp, apdu_format=apdu_format)
		timer_end = time()
		LogInfo("duration : {}".format(timer_end - timer_start))
		gsma.pprint_audit_resource_info(rapdu)
		return (timer_end - timer_start)

	# def audit_os_version(self, scp=80, apdu_format='definite'):
	# 	capdu, rapdu, sw = self.ram(gsma.audit_os_version, scp, apdu_format=apdu_format)
	# 	LogInfo("OS Version : {}".format(hexastring.get_network_access_name(rapdu[rapdu.find('56'):rapdu.find('56')+14])))

	@allure.step
	def audit_os_version(self, scp=80, apdu_format='definite'):
		capdu, rapdu, sw = self.ram(gsma.audit_os_version, scp, apdu_format=apdu_format)
		# LogInfo("OS Version : {}".format(hexastring.get_network_access_name(rapdu[rapdu.find('56'):rapdu.find('56')+14])))
		return capdu, rapdu, sw
	
	@allure.step
	def audit_psm_propietary(self, scp=80, apdu_format='definite'):
		capdu, rapdu, sw = self.ram(gsma.audit_propietary_psm, scp, apdu_format=apdu_format)
		# LogInfo("OS Version : {}".format(hexastring.get_network_access_name(rapdu[rapdu.find('56'):rapdu.find('56')+14])))
		return capdu, rapdu, sw
	
	@allure.step
	def audit_psm_connectivity(self, scp=80, apdu_format='definite'):
		capdu, rapdu, sw = self.ram(gsma.audit_connectivity_psm, scp, apdu_format=apdu_format)
		# LogInfo("OS Version : {}".format(hexastring.get_network_access_name(rapdu[rapdu.find('56'):rapdu.find('56')+14])))
		return capdu, rapdu, sw
	
	@allure.step
	def audit_ecasd_recognition_data(self, scp, apdu_format='definite'):
		return self.ram(gsma.audit_ecasd_recognition_data, scp, apdu_format=apdu_format)

	@allure.step
	def audit_ecasd_certificate_store(self, scp, apdu_format='definite'):
		return self.ram(gsma.audit_ecasd_certificate_store, scp, apdu_format=apdu_format)

	@allure.step
	def audit_ecasd_get_eid_data(self, scp=80, apdu_format='definite'):
		select_ecasd = '00A4040010 A0000005591010FFFFFFFF8900000200'  
		SendAPDU(select_ecasd)
		get_eid = '00CA005A00'
		rapdu, sw = SendAPDU(get_eid)
		LogInfo("ISO: 00A4040010A0000005591010FFFFFFFF8900000200, 00CA005A00")
		LogInfo("EID: {}".format(rapdu))

	@allure.step
	def install_ota_poller_applet(self, scp, apdu_format='definite', chunk='01'):
		apdus = ['80E602001D10A000000077010760061002000000000708A000000151000000000000', '80E8000020C4820E1D01001ADECAFFED010204000110A0000000770107600610020000000000', '80E80001200702001F001A001F0014006A013E003D0A880025012300000000000E0004000C00', '80E800022007010004006A07040107A0000000620101060110A0000000090005FFFFFFFF8900', '80E800032012000000050106A00000015100040110A0000000770100001D1000000000000100', '80E8000420000110A0000000090005FFFFFFFF8913000000000107A000000062000102021000', '80E8000520A0000000090003FFFFFFFF89107100010300140110A00000007701076006100200', '80E80006200100000007009506003D468003170109050C00000155FFFF01D90159017701B800', '80E800072001BB01C902960A6C0A810A848002008108010D820501088012010E8302040B0A00', '80E80008200C09801102100F070A8802081E800A082A00410891800C089F00410540188C0000', '80E8000920171804880018038801180488021804880318058904180389051803880618068900', '80E8000A200718048808180588091806880A1807880B1803880C1803890D1805900B870E1800', '80E8000B2005900B870F1805900B3D0303383D04033887101805900B3D03100F383D04023800', '80E8000C2087111806900B87121806048D003187131806900B87141806048D003187157A0500', '80E8000D20351D320329040329050329068F00183D181D1E8C001928071F181F250441413200', '80E8000E201F181F2504414132181F59030125290416047D001A6B1B181F8D001B81001C5900', '80E8000F200302181F5903012580001D1F29057008116A808D001E1507181D0441181D258B00', '80E8001020001F8D00208D00217D001C8E0200220615078D00208B0023160529061606160500', '80E8001120100A416D2C160616056A1B1606160504416A131606160508416A0B160616051000', '80E800122006416B081816068D002459060170CF048000251507181605100A8C00263B7A0100', '80E800132030187702601E7D001C6B04187701770320181D25600C181D25046A06181D033800', '80E80014207A052119088D001B11DF6D6B3519100725100D6B2D191008257C001D6B24191000', '80E8001520092510AA6B1C1819100B19100A251100FF538C0026311E1190006B040478037800', '80E8001620037800307A02218D00272D1A198E020028167A02118D00272C198D00208E020000', '80E800172028137A0522198B00292D188B002A60037A1A042532188B002A610D1F10A46B0800', '80E8001820116A828D001E1A032510F05310806A08116E008D001E1A052561071A0625600800', '80E8001920116A868D001E1A04257500700002FFAA000DFFAB0024198B002B3B181A081A0700', '80E8001A20251100FF538C00268D001E70541A0725100A6B3F1A03AE01381A04AE02381A0500', '80E8001B20AD140425381A06AD140525381A07AF055B381A08AE03381A1006AE0638AD120300', '80E8001C201A1007068D002C3B1903100A8B002D70101167008D001E7008116D008D001E7A00', '80E8001D2005278D002E2D03321D7501FA0004FFFF006B00010015000F00E200130156AD1500', '80E8001E200306038D002F3BAD130306038D002F3B188D0030871610158D0032600AAD160200', '80E8001F208E0200330A101E8D0032610818038800700618048800AE0204A301AAAE0104A300', '80E800202001A418078C003498019C181A03038C0035A801920329050329068D003628041500', '80E8002120048E0100370D7C001DA3017A15048E01003700290715041A0316078E0400370100', '80E80022203B160760451A03257500400001FFCC00091A04251100FF5329051605107F6F1E00', '80E800232016051100816B161A05251100FF53290506290616051100806D077A7A0529061800', '80E80024201A160616058C0035A8011B8D00362804188D00308716AE0204A3010AAE0104A300', '80E8002520010415048E0100370D10D6A300F81504101B048E030037029800EB1504038E0200', '80E8002620003704290716076137AD16100F8E0200330215041013048E030037026021150400', '80E800272003AD0F03058E050037063B18048C00349800B3181A03038C0035A800A97A7AAD00', '80E8002820168E0100330B32AE02046B4EAE01046B49AE0C604518AD1503AD14038C00382900', '80E8002920081608026B091FAD15038D003918AD1503AD14038C003829081608621D18058C00', '80E8002A200034600D181A03038C00351803880CAD150306038D002F3BAE06046B48AE010400', '80E8002B206B4318AD1303AD12038C003829081608026B091FAD13038D003918AD1303AD1200', '80E8002C20038C003829081608621918068C00346009181A03038C0035AD130306038D002F00', '80E8002D203B70027A03210331188D003087161D046A0BAE006007188C003A311E056B251D00', '80E8002E20076B0BAD16100F8E020033001D056B12183D850D0443890DAF0D61061803880C00', '80E8002F200378AE03046B3CAE006038188C003B056B311D076A061D046B131804880C18AF00', '80E800302005890DAF0D61061803880C1D056B12183D850D0443890DAF0D61061803880C0300', '80E800312078047805421F100A6A06116700788D003028041504048E02003300191E25046B00', '80E8003220151504028E0200330018191E5902012588017031191E2561291504028E02003300', '80E8003320021504100F8E02003303600B1504100F8E0200330218191E590201258801700500', '80E8003420590201191E256008191E25046B1A18191E590201258802AE026110AD1503060300', '80E80035208D002F3B7005590201191E7B003C04058D003D61157C0025604E7B003E03AD1400', '80E800362003068D002C3B7040191EAD1404058D002C3B18AD14037B003E038C0038046B0E00', '80E80037207B003E03AD1403068D002C3B18AD14037B003F038C0038026B0E7B003F03AD1400', '80E800382003068D002C3B590202AF052905191E7B003C03048D003D61137C00256018181900', '80E80039201E251100FF538905700C18191E251100FF5389051605AF0D43AF056C1518038800', '80E8003A200C1803890DAD150306038D002F3B700D18AF051605AF0D4343890D590201191E00', '80E8003B20256008191E25046B0D18191E5902012588037005590201191E256008191E250400', '80E8003C206B1A18191E590201258806AE066110AD130306038D002F3B7005590201191E7B00', '80E8003D20003C03068D003D61197C00256052038000257B004003AD1203068D002C3B704000', '80E8003E20191EAD1203068D002C3B18AD12037B0040038C0038046B0E7B004003AD12030600', '80E8003F208D002C3B18AD12037B003F038C0038026B0E7B003F03AD1203068D002C3B590200', '80E8004020010380002511900078064810072904032905032906032907011184008D00422800', '80E8004120088D00208D00212809150815091184000103038E0600430094000044280A1F9900', '80E800422000E98D0027280B150B10A11916041100FF8E05002821321F9800C35903FD191000', '80E800432008252907160763161916045904FF1081381910082506415B2906700B1910082500', '80E800442005415B29061916045904FF108438160663311916045904FF160638191604590400', '80E8004520FF1081381607107F6F0E191008251006415B2905701F1910082508415B29057000', '80E8004620141916045904FF1606381910082507415B29051916045904FF1083381605631600', '80E80047201916045904FF1605381916045904FF108138700B1916045904FF16053819160400', '80E80048201081380331160505415B321604651B19160419031F8D002C3B700F190310813800', '80E80049201904033805320331150A191E1F8E040044007004280B7A051303308D00452D8D00', '80E8004A2000462E1A10260310828E040047171A8E01004718301D10106D051D630405781B00', '80E8004B201013048E0300480260101B03AD0F03058E050048063B0478057807128D00492C00', '80E8004C2019113F008E02004A0719117F208E02004A0719116F078E02004A071904AD0E0300', '80E8004D20058E05004A093B70052D05781805AD0E03AD0E03058C004B1804AD0F03AD0F0300', '80E8004E20058C004BAD0E03AD1003058D003D600EAD0E03AD1103058D003D61040578AD0F00', '80E8004F2003AD0E03058D003D610404780578043303320329040329051C1100FF5329041C00', '80E800502010084F1100FF532905191E0541251100FF53160441321F1100FF6F1B191E054100', '80E80051201F110100435B38191E0441251100FF530441327013191E05411F5B38191E044100', '80E8005220251100FF53321F160541321F1100FF6F17191E04411F110100435B38191E3E2500', '80E800532004415B387009191E04411F5B387A06701D056B1B181A1F1504160516068C004C00', '80E800542015040315040325100F533870411D046B3D181A1F1504160516068C004C15041600', '80E800552005044115041605044125074F100F531504160525074D1100F053555B3815041600', '80E8005620051504160525074F100F53387A05520329060329051605066D2916066125181900', '80E80057201E160541251B1604160541258C004D290616066005160678160504415B29057000', '80E8005820D60378023203320329041D074F100F53321E074F100F5329041F16046F04047800', '80E80059201F16046D0402781D100F53301E100F53311D1E6F0404781D1E6D04027803780400', '80E8005A20620329060329071E2906160616056D2719160625074F100F5319160625074D1100', '80E8005B2000F053555B29071B160459040116073859060170D77A0210188B004E8D00208D00', '80E8005C2000217D001C8E020022007A00207A01200478080025000E0004000403000300FD00', '80E8005D2020030003FFFFFF03000300003C030003ED4E0000030003000D8805013E004F0200', '80E8005E2000000A020000000200000B0200000C0200000D0200000E0200000F020000100200', '80E8005F20000011020000120200001302000014020000150200001602000005020000060200', '80E8006020000008020000090200000402000007020000020200000302000001068003000100', '80E8006120000000060000110500000B06801004050000080500000D06800701038003020600', '80E800622080080306820309018204000300000B060001650500000A06000519068301010100', '80E800632083000003800A010380030303800A060680100203800A0806840100068010030600', '80E80064208110000680080D06810B02018109000600049A060006FF06810C00018103000600', '80E80065200009C3060008E40600082D06000870050000020680100005000000050000040500', '80E8006620000006018502000682030A018205000182090006810E0006811100018105000100', '80E800672081060006860100018600000600096406000A35060009F90300000C090123007E00', '80E80068201A0404040404040404040404040406060E0F06080608FFBB050507070606034100', '80E8006920090D09110602069802062D181E08050505030E07031C02090506030E07031A1400', '80E8006A2006110F0402060405150302020608040206332C190204230C091109110B1D0C0400', '80E8006B20030604020C040417190204270C09110911FF773111030903070309030B0300A100', '80E8006C200708066D0E13071F07030C0B0C03030304060332090A0C1F141915070705040800', '80E8006D20050B0D1010140E033C080808062109050709061D0A0C0804090D490606140F0B00', '80E8006E200F0A0E070A0B1A0E0A0B090C150E0A0B09080C0E112442090E18090B2F0B050500', '80E8006F2005080C08040608080406080D0505314A0B05050603080C08040608080406080800', '80E80070201A050310050910C3190D040B06140D0B0909090B130C0A0C0E981D528D03030300', '80E88071010400', '80E60C004D10A000000077010760061002000000000710A000000077010760061002010000000710A000000077010760060000FE00000700010016C903000000EF0FC90DB01088010112C0301010000003C00']
		return self.ram(apdus, scp, apdu_format=apdu_format)

	@allure.step
	def update_smsr_addressing_parameters(self, ipv4, tcp_port, scp, apdu_format='definite', chunk='01'):
		pprint.h3("update smsr addressing parameters")
		return self.ram(gsma.update_smsr_addressing_parameters(ipv4, tcp_port), scp, apdu_format=apdu_format, chunk = chunk)
		# return self.ram(gsma.update_jpatch_rc1(), scp, apdu_format=apdu_format, chunk = chunk)

	@allure.step
	def update_connectivity_sms(self, scp, apdu_format='definite'):
		pprint.h3("update connectivity sms")
		capdu, rapdu, sw = self.ram(gsma.update_connectivity_parameters_sms(), scp, apdu_format=apdu_format)
		return capdu, rapdu, sw

	@allure.step
	def frm_on(self, scp, apdu_format='definite', chunk='01' ):
		pprint.h3("set fallback rollback mechanism (frm) on")
		return self.ram(gsma.activate_frm, scp, apdu_format=apdu_format, chunk = chunk)

	@allure.step
	def frm_off(self, scp, apdu_format='definite', chunk='01'):
		pprint.h3("set fallback rollback mechanism (frm) off")
		return self.ram(gsma.deactivate_frm, scp, apdu_format=apdu_format, chunk = chunk)
	
	@allure.step
	def frm_trigger_on(self, scp=80, apdu_format='definite', chunk='01' ):
		pprint.h3("set fallback rollback mechanism (frm) trigger on")
		return self.ram(gsma.activate_frm_trigger, scp, apdu_format=apdu_format, chunk = chunk)
	
	@allure.step
	def frm_trigger_off(self, scp=80, apdu_format='definite', chunk='01' ):
		pprint.h3("set fallback rollback mechanism (frm) trigger on")
		return self.ram(gsma.deactivate_frm_trigger, scp, apdu_format=apdu_format, chunk = chunk)

	@allure.step
	def apf_on(self, scp, apdu_format='definite', chunk='01'):
		pprint.h3("set apf on")
		return self.ram(gsma.activate_apf, scp, apdu_format=apdu_format, chunk = chunk)

	@allure.step
	def apf_off(self, scp,  apdu_format='definite', chunk='01'):
		pprint.h3("set apf off")
		return self.ram(gsma.deactivate_apf, scp, apdu_format=apdu_format, chunk = chunk)
	
	@allure.step
	def apf_counter(self, scp=80, counter='0168', apdu_format='definite', chunk='01'):
		pprint.h3("set apf counter {}".format(counter))
		return self.ram(gsma.configure_apf_counter(counter=counter), scp, apdu_format=apdu_format, chunk = chunk)

	@allure.step
	def apf_interval_value(self, scp=80, interval='3BC4', apdu_format='definite', chunk='01'):
		pprint.h3("set apf internal value {}".format(interval))
		return self.ram(gsma.configure_apf_interval_value(interval=interval), scp, apdu_format=apdu_format, chunk = chunk)	
	@allure.step
	def audit_psm_propietary(self, scp=80, apdu_format='definite'):
		capdu, rapdu, sw = self.ram(gsma.audit_propietary_psm, scp, apdu_format=apdu_format)
		# LogInfo("OS Version : {}".format(hexastring.get_network_access_name(rapdu[rapdu.find('56'):rapdu.find('56')+14])))
		return capdu, rapdu, sw
	
	@allure.step
	def audit_psm_connectivity(self, scp=80, apdu_format='definite'):
		capdu, rapdu, sw = self.ram(gsma.audit_connectivity_psm, scp, apdu_format=apdu_format)
		# LogInfo("OS Version : {}".format(hexastring.get_network_access_name(rapdu[rapdu.find('56'):rapdu.find('56')+14])))
		return capdu, rapdu, sw

	#comment by PA 20181122
	# def download_apdu_profile(self, aid_isdp, perso_txt):
		
	#     LogInfo('install apdu profile to ISDP {} thru ISO/SCP03'.format(aid.short_isdp(aid_isdp)))
	#     SecurityDomain(aid_isdp, self.euicc.get_isdp_keyset_scp03t(aid_isdp)).openDefaultSecureChannel()
	#     timer_start = time()
	#     capdus = hexastring.perso_txt_to_capdus(perso_txt)
	#     for capdu in capdus:
	#         data, sw = SendAPDU(capdu)
	#     timer_end = time()
	#     LogInfo('apdu profile ({} commands) was installed successfully in {:04.1f} seconds'.format(len(capdus), timer_end - timer_start))
	#     # self.euicc.init()


	@allure.step
	def store_data_for_master_delete(self,aid_isdp):
		keyset_isdp = self.euicc.get_isdp_keyset_scp03t(aid_isdp)
		LogInfo('ISD-P SCP03 keyset value before operation: {}'.format(keyset_isdp))
		isdp = IssuerSecurityDomain_Profile(aid_isdp, keyset_isdp)
		apdu_open_scp03 = isdp.openDefaultSecureChannel(secureMessaging='33')
		profile_download_payload = apdu_open_scp03[0] + apdu_open_scp03[1]
		ram_targeted_app = aid.format_for_http_header_targeted_app(aid_isdp)
		LogInfo("profile download payload : HOST_CHALLENGE = {}, HOST_CRYPTOGRAM = {}".format(apdu_open_scp03[0][8:],apdu_open_scp03[1][6:]))
		payload = '80503000'+lv(apdu_open_scp03[0][8:])+'0084823300'+lv(apdu_open_scp03[1][6:])
		capdus = []

		capdus.append('80E28800'+lv('0070'+lv('45'+lv('6364160310000094337F'))))
		capdus.append('80E28800'+lv('0070'+lv('42'+lv('000000000004'))))
		capdus.append('80E28800'+lv('0070'+lv('5F20'+lv('00000000000000000000000000000000'))))
		i=0
		capdu=0
		commands_scp_counter_mac_chaining_value = []
		for capdu in range(2):
			secured_adpu = isdp.sendCommand(capdus[i], payload=True)[0]
			payload += secured_adpu
			commands_scp_counter_mac_chaining_value.append((isdp.gp_auth_manager.m_ICV_CounterForSCP_03, isdp.gp_auth_manager.m_MAC_ChainingValue))

		LogInfo("Content-payload : {}".format(payload))

		capdu, rapdu, sw = self.ram(payload, scp=80, apdu_format = 'definite', header_targeted_app=ram_targeted_app, no_length=True)

	@allure.step
	def master_delete(self, aid_isdp, scp, apdu_format='definite', chunk = '01'):
		LogInfo('master delete ISD-P {} thru SMS/SCP03'.format(aid.short_isdp(aid_isdp)))
		return self.ram(gsma.master_delete(aid_isdp=aid_isdp),scp, apdu_format=apdu_format, chunk = chunk)

	@allure.step
	def download_apdu_profile(self, aid_isdp, perso_txt):       
		LogInfo('install apdu profile to ISDP {} thru ISO/SCP03'.format(aid.short_isdp(aid_isdp)))
		SecurityDomain(aid_isdp, self.euicc.get_isdp_keyset_scp03t(aid_isdp)).openDefaultSecureChannel()
		timer_start = time()
		capdus, file_size = hexastring.perso_txt_to_capdus(perso_txt)
		euicc_response_sw = 0
		euicc_response_successfully = False
		pprint.h3("Sending APDU to download profile")
		for capdu in capdus:
			data, sw = SendAPDU(capdu)
			LogInfo("sw: "+str(sw))
			if sw != "9000":
				euicc_response_sw +=1

		if euicc_response_sw > 0:
			euicc_response_successfully = False
		else:
			euicc_response_successfully = True

		timer_end = time()
		download_time = timer_end-timer_start

		LogInfo('apdu profile ({} commands) was installed successfully in {:04.1f} seconds'.format(len(capdus), download_time))
		return download_time, euicc_response_successfully, file_size
		
	def set_test_profile_attribute(self, aid_isdp, scp, apdu_format='definite', chunk='01'):
		timer_start = time()
		capdu, rapdu, sw = self.ram(gsma.set_test_profile_attribute(aid_isdp), scp, apdu_format=apdu_format, chunk=chunk)
		timer_end = time()
		LogInfo ("duration : {}".format(timer_end - timer_start))
		return capdu, rapdu, sw

	@allure.step
	def download_profile(self, aid_isdp, saipv2_txt, apdu_format='indefinite', split_size = 1000, profile_type = "saip", source=None, interrupt=False, pertrubation=None):
		pprint.h3("download profile "+str(aid_isdp))
		# http payload = [init update + ext auth + saip splits wrapped in scp03t]
		LogInfo('download SAIPv2 to ISD-P {} thru SCP81/SCP03t'.format(aid.short_isdp(aid_isdp)))
		keyset_isdp = self.euicc.get_isdp_keyset_scp03t(aid_isdp)
		LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))
		isdp = IssuerSecurityDomain_Profile(aid_isdp, keyset_isdp)
		apdu_open_scp03t = isdp.openDefaultSecureChannel(secureMessaging='33')
		profile_download_payload = apdu_open_scp03t[0] + apdu_open_scp03t[1]
		ram_targeted_app = aid.format_for_http_header_targeted_app(aid_isdp)
		if profile_type == "saip":
			saipv2_hexastr, file_size = hexastring.saipv2_to_hexastr(saipv2_txt, source)			
			splitSizeChar = split_size*2
			split_size = splitSizeChar
			# split_size = 1000
			saip_splits = [ saipv2_hexastr[x*split_size : x*split_size + split_size] for x in range(len(saipv2_hexastr)//split_size +1) ]

			commands_scp_counter_mac_chaining_value = []
		else:
			saipv2_hexastr, file_size = hexastring.perso_txt_to_capdus(saipv2_txt)						
			saip_splits = saipv2_hexastr
			commands_scp_counter_mac_chaining_value = []
			# capdu="".join(saipv2_hexastr).replace(" ","")			
			# print("capdu: "+capdu)

		# splitSizeChar = split_size*2
		# split_size = splitSizeChar
		# # split_size = 1000
		# saip_splits = [ saipv2_hexastr[x*split_size : x*split_size + split_size] for x in range(len(saipv2_hexastr)//split_size +1) ]

		# commands_scp_counter_mac_chaining_value = []

		for saip_split in saip_splits:
			saip_split_ciphered_scp03t = isdp.sendData('86', saip_split, payload=True)[0]
			profile_download_payload += saip_split_ciphered_scp03t
			commands_scp_counter_mac_chaining_value.append((isdp.gp_auth_manager.m_ICV_CounterForSCP_03, isdp.gp_auth_manager.m_MAC_ChainingValue))

		
		LogInfo("saipv2 split size = {} bytes, {} splits in total".format(split_size//2, len(saipv2_hexastr)//split_size))
		
		timer_start = time()
		capdu, rapdu, sw = self.ram(profile_download_payload, header_targeted_app=ram_targeted_app, no_length=True, apdu_format=apdu_format,interrupt=interrupt, pertrubation=pertrubation)
		timer_end = time()
		tlvs = hexastring.hexastr_to_tlvs(rapdu[4:-4]) # not considering header and tail of indefinite format
		LogInfo('TLVs: {}'.format(tlvs))
		tlvs_unwrapped = []

		i = 0
		success = False
		euicc_responses_array = []

		euicc_responses = None
		
		for tlv in tlvs:
			if tlv[0:2] == "86":
				if tlv[2:4] not in ('00', '08'):
					isdp.gp_auth_manager.m_ICV_CounterForSCP_03 = commands_scp_counter_mac_chaining_value[i][0]
					isdp.gp_auth_manager.m_MAC_ChainingValue = commands_scp_counter_mac_chaining_value[i][1]
					euicc_responses = isdp.receiveData(tlv, payload=True)[0]
					euicc_responses_array.append( [ tlv, isdp.receiveData(tlv, payload=True)[0] ] )
					success = True

				i += 1


		# if tag 81 is present, status is not ok and its value indicates error origin according to the table below (82 is optionnal tag in any case)
		# euicc_response_der = '30XXA0XX30XX'+'80XXYY'+'81XXYY'+'8201YY'

		saip_installation_errors_tag80 = {
			'00': 'ok',
			'01': 'pe not supported',
			'02': 'memory failure',
			'03': 'bad values',
			'04': 'not enough memory',
			'05': 'invalid request format',
			'06': 'invalid parameter',
			'07': 'runtime not supported',
			'08': 'lib not supported',
			'09': 'template not supported',
			'0A': 'feature not supported',
			'0B': 'pin code missing',
			'1F': 'unsupported profile version'
		}

		download_time = timer_end - timer_start
		if success and len(tlvs)>2:
			LogInfo('saipv2 ({} bytes) was downloaded in {:04.1f} seconds'.format(len(saipv2_hexastr)//2, download_time)) # os.path.getsize(file_static_path) does not make sense if using txt file or divide by 2
			LogInfo('euicc responses: {} '.format(euicc_responses_array))
			LogInfo("{} : {} \n\n".format( euicc_responses[12:18],saip_installation_errors_tag80[euicc_responses[16:18]] ))
			LogInfo('Oberthur GlobalPlatform module version: {}'.format(GlobalPlatformModuleGetVersion()))
			LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))

				
		else:
			LogInfo('error, saipv2 has not been installed!')
			if len(tlvs) <= 2:
				LogInfo('Oberthur GlobalPlatform module version: {}'.format(GlobalPlatformModuleGetVersion()))
				LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))
			else:
				# LogInfo(saip_installation_errors_tag80)
				LogInfo('profile element #{} is incorrect'.format(i+1))
				LogInfo('euicc responses: {}'.format(euicc_responses))


		return download_time, euicc_responses, file_size   

if __name__ == '__main__':
	server = Smsr()
	# model.Euicc().init()
	# server.audit
	# model.Euicc().master_delete(aid_isdp='A0000005591010FFFFFFFF8900001100')
