import allure
from Oberthur import *
from Ot.GlobalPlatform import SCPKeyset, SecurityDomain, IssuerSecurityDomain_Profile
from util import pprint
from util.aid import *
from util.tools import *
import model
import const
from Mobile import *
from random import randint



__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

gp     = model.Gp()
gsma   = model.Gsma()
device = model.Device()
# server = model.Smsr()
# ota    = model.SCP80()

class Mno:
	aid         = 'A000000151000000'
	tar_rfm     = '000055'
	tar_ram     = 'B20100' or 'B00000' or '000000'
	tpoa        = '0681214365'


	def __init__(self, scp80, aid_isdp=None):
		self.scp80     = scp80
		self.aid_isdp  = aid_isdp

	@property
	def scp81(self):
		return model.SCP81(scp80=self.scp80)


	def ram(self, capdus, scp=81 or 80, pull=False, header_targeted_app='//aid/A000000151/000000', apdu_format='definite', no_length=False, tpoa=tpoa ):
		if scp == 81: return self.scp81.remote_management(capdus, pull, header_targeted_app, no_length, apdu_format= apdu_format)
		if scp == 80: return self.scp80.remote_management(capdus,apdu_format,tpoa=tpoa)

	def rfm(self, capdus, scp=81 or 80, pull=False, header_targeted_app='//aid/A000000087/1002FFFFFFFF89040300FF', apdu_format='definite',no_length=False, tpoa=tpoa, content_type='RFM'):
		if scp == 81:
			self.scp80.tar = 'B20100'
			return self.scp81.remote_management(capdus, pull, header_targeted_app, no_length, apdu_format, content_type='RFM')
		if scp == 80:  return self.scp80.remote_management(capdus,apdu_format,tpoa=tpoa)

	@allure.step
	def audit_applet_list(self, scp=80, apdu_format='definite', tpoa=tpoa):
		if apdu_format == 'compact' :
			capdu, rapdu, sw = self.ram(gp.audit_applet_list[0:14] + '00C0000000', scp, apdu_format= apdu_format, tpoa=tpoa)
		else :
			capdu, rapdu, sw = self.ram(gp.audit_applet_list, scp, apdu_format= apdu_format, tpoa=tpoa)
		return gp.pprint_audit_applet_list(rapdu,scp,apdu_format)

	@allure.step
	def audit_sd_list(self, scp=80, apdu_format="definite"):
		if apdu_format == 'compact' :
			capdu, rapdu, sw = self.ram(gp.audit_sd_list[0:14]+ '00C0000000', scp, apdu_format= apdu_format)
		else:
			capdu, rapdu, sw = self.ram(gp.audit_sd_list, scp, apdu_format= apdu_format)
		if rapdu == '026F00' :
			return LogInfo("Audit Failed !")

		return gp.pprint_audit_sd_list(rapdu,scp,apdu_format)

	@allure.step
	def audit_amdb_parameters(self, scp=80):
		capdu, rapdu, sw = self.ram(gp.audit_amdb_parameters, scp)
		return capdu, rapdu, sw

	@allure.step
	def audit_pol1(self, scp=80):
		self.scp80.spi1 = 0x16
		self.scp80.spi2 = 0x21
		self.scp80.tar = self.aid_isdp[-6:]
		return self.scp80.remote_management(gsma.audit_pol1_rules, scp)

	@allure.step
	def confirm_notification(self, scp=80, apdu_format='definite'):
		return self.ram(gsma.handle_notification_confirmation(), scp, apdu_format= apdu_format)

	@allure.step
	def push_sms(self):
		self.scp80.spi1 = 0x16
		self.scp80.spi2 = 0x21
		self.scp80.tar  ='B20100'
		return self.scp80.ram('8100', compact_mode=True)

	@allure.step
	def install_applet(self, scp=80, apdu_format='definite'):

		# commands_install_applet = [
		# 	'80E6020015100000000000F000000000000001234F1000000000',
		# 	'80E80000FFC482018701001ADECAFFED0102040001100000000000F000000000000001234F1002001F001A001F0014001E003A00180089001100150000000300020001000402010004001E02020107A0000000620101060210A0000000090003FFFFFFFF891071000203001401100000000000A000000000000001234F1000010600184380030300020702000000650068810101088100008002000700890005318F000D3D8C00032E1B181D0441181D258B00077A0911188C0008181007900B3D031041383D041050383D051050383D06104C383D071045383D081054383D100610313887018D000C2C18197B0002037B000292030303038B000A880019048B000B7A00',
		# 	'80E880018C207A06228D00062D8D00092E1D046B141A0310F6AD0103AD01928B00041A8B00053B7A080011000200010001030004424F4C540000000005003A000E0200000202000000050000000600001703810A1303810A1606810A000380030206800300068108000381090B038109090681090001000000090015000447171D03000D04040C072E0604080709041204',
		# 	'80E60C0051100000000000F000000000000001234F10100000000000A000000000000001234F101000000000000000000000000001234F1001001AEF16C8020001C7020001CA0C0100FF001001010000020100C90000',
		# 	'80F24000124F1000000000000000000000000001234F1000C0000000'
		# ]
		commands_install_applet = [
			'80E6020015100000000000F000000000000001234F1000000000',
			'80E80000FFC482018701001ADECAFFED0102040001100000000000F000000000000001234F1002001F001A001F0014001E003A00180089001100150000000300020001000402010004001E02020107A0000000620101060210A0000000090003FFFFFFFF891071000203001401100000000000A000000000000001234F1000010600184380030300020702000000650068810101088100008002000700890005318F000D3D8C00032E1B181D0441181D258B00077A0911188C0008181007900B3D031041383D041050383D051050383D06104C383D071045383D081054383D100610313887018D000C2C18197B0002037B000292030303038B000A880019048B000B7A00',
			'80E880018C207A06228D00062D8D00092E1D046B141A0310F6AD0103AD01928B00041A8B00053B7A080011000200010001030004424F4C540000000005003A000E0200000202000000050000000600001703810A1303810A1606810A000380030206800300068108000381090B038109090681090001000000090015000447171D03000D04040C072E0604080709041204',
			'80E60C0051100000000000F000000000000001234F10100000000000A000000000000001234F101000000000000000000000000001234F1001001AEF16C8020001C7020001CA0C0100FF001001010000020100C90000',
			'80F24000124F1000000000000000000000000001234F10'
		]

		return self.ram(commands_install_applet, scp, apdu_format=apdu_format)

	@allure.step
	def delete_applet(self, scp=80, apdu_format='definite'):
		if apdu_format=='compact':
			return self.ram('80E40080124F100000000000F000000000000001234F10'+'00C0000000',scp, apdu_format=apdu_format)
		else :
			return self.ram('80E40080124F100000000000F000000000000001234F1000',scp, apdu_format=apdu_format)

	@allure.step
	def lock_applet(self, scp=80, apdu_format='definite'):
		return self.ram('80F040FF1000000000000000000000000001234F10', scp, apdu_format=apdu_format)

	@allure.step
	def unlock_applet(self, scp=80, apdu_format='definite'):
		return self.ram('80F040071000000000000000000000000001234F10', scp, apdu_format=apdu_format)

	@allure.step
	def custom_RAM(self, scp=80, apdu_format='definite'):
		return self.ram(gp.audit_sd_list[0:14]+ '00C0000000', scp, apdu_format=apdu_format)

	@allure.step
	def test_rfm_3g_spn(self, scp=80, apdu_format='definite'):
		self.scp80.tar = 'B00001'
		# self.scp80.tar = '000005'

		update_SPN = [
		'00A40000023F00',
		'00A40000027F20',
		'00A40000026F46',
		'00D60000110159595858585858585858585858583132'
		]

		update_ADN = [
		'00A40000023F00',
		'00A40000027F10',
		'00A40000025F3A',
		'00A40000024F3A',
		'00DC0704261111111111111111111111111111111111111111111111111111111111111111111111999999'
		]

		return self.rfm(update_ADN,scp, header_targeted_app='//aid/A000000087/1002FFFFFFFF89040300FF', apdu_format=apdu_format, content_type='RFM')

	@allure.step
	def test_rfm_2g_spn(self, scp=80, apdu_format='definite'):
		self.scp80.tar = 'B00010'
		update_SPN = ['A0A40000023F00','A0A40000027F20','A0A40000026F46','A0D60000110159595858585858585858585858583132']
		return self.rfm(update_SPN,scp, header_targeted_app='//aid/A000000063/504B43532D3135', apdu_format=apdu_format, content_type='RFM')

	@allure.step
	def add_whitelist(self, scp=80, apdu_format='definite',tpoa=tpoa):
		self.scp80.tar = 'B00001'
		update_SPN = ['00A40000023F00','00A40000027FDE','00A40000025F14','00DC01040C0C91123456789123FFFFFFFFFF']
		return self.rfm(update_SPN,scp, header_targeted_app='//aid/A000000063/504B43532D3135', apdu_format=apdu_format,tpoa=tpoa)

	@allure.step
	def remove_whitelist(self, scp=80, apdu_format='definite',tpoa=tpoa):
		self.scp80.tar = 'B00001'
		update_SPN = ['00A40000023F00','00A40000027FDE','00A40000025F14','00DC0104260CFFFFFFFFFFFFFFFFFFFFFFFF']
		return self.rfm(update_SPN,scp, header_targeted_app='//aid/A000000063/504B43532D3135', apdu_format=apdu_format,tpoa=tpoa)

	@allure.step
	def update_pol1(self, aid_isdp, pol1_value, scp=80, apdu_format='definite'):
		return self.ram([gsma.install4perso(aid_isdp), '80E28800063A06038101{}'.format(pol1_value)],scp, apdu_format=apdu_format)

	@allure.step
	def update_connectivity(self, aid_isdp, connectivity_parameter, scp=80, apdu_format='definite'):
		update_connectivity = '80E28800' + lv('3A07' + lv(connectivity_parameter))
		print(update_connectivity)
		return self.ram([gsma.install4perso(aid_isdp), update_connectivity],scp, apdu_format=apdu_format)

		# '80E288000E 3A07 0B A0 09 06 0791135604993800

	def custom_apdu(self, apdu, scp=80, spi='16213939', tar='B20100', apdu_format='definite', custom_content_type='RFM'):
		apdus=[]
		for each in apdu.split(';'):
			apdus.append(each)

		if scp==80:
			if custom_content_type=='RAM':
				return self.ram(apdus,scp, apdu_format=apdu_format)
			elif custom_content_type=='RFM':
				self.scp80.spi1 = int(spi[0:2], base=16)
				self.scp80.spi2 = int(spi[2:4], base=16)
				self.scp80.kic  = int(spi[4:6], base=16)
				self.scp80.kid  = int(spi[6:8], base=16)

				self.scp80.tar  = tar
				return self.rfm(apdus,scp, apdu_format=apdu_format, content_type='RFM')
		elif scp==81:
			if custom_content_type=='RAM':
				return self.ram(apdus,scp, apdu_format=apdu_format)
			elif custom_content_type=='RFM':
				self.scp80.spi1 = int(spi[0:2], base=16)
				self.scp80.spi2 = int(spi[2:4], base=16)
				self.scp80.kic  = int(spi[4:6], base=16)
				self.scp80.kid  = int(spi[6:8], base=16)

				self.scp80.tar  = tar
				return self.rfm(apdus,scp, apdu_format=apdu_format, content_type='RFM')

class Isdr:

	scp03_kvn  = Constants.config['ISDR']['scp03_kvn']
	scp03_scp  = Constants.config['ISDR']['scp03_scp']
	scp03_sEnc = Constants.config['ISDR']['scp03_sEnc']
	scp03_sMac = Constants.config['ISDR']['scp03_sMac']
	scp03_sKek = Constants.config['ISDR']['scp03_sKek']

	# sd      = SecurityDomain('A0000005591010FFFFFFFF8900000100', SCPKeyset(kvn=scp03_kvn,scp=scp03_scp,sEnc=scp03_sEnc,sMac=scp03_sMac,sKek=scp03_sKek, sequence_number=None))
	scpKeyset = SCPKeyset(kvn=scp03_kvn,scp=scp03_scp,sEnc=scp03_sEnc,sMac=scp03_sMac,sKek=scp03_sKek, sequence_number=None)
	sd      = SecurityDomain('A0000005591010FFFFFFFF8900000100', keysets=scpKeyset)
	scp80   = model.SCP80(parent_aid=sd.getAID(), spi1=0x16, spi2=0x39, kic=0x12, kid=0x12, algo_crypto_verif=const.hexa.ciphers['AES'])
	@property
	def scp81(self):
		return model.SCP81(scp80=self.scp80)

	def scp03(self, sd, capdus='80F24002094F005C054F9F7053B500', payload=True):
		self.sd.openDefaultSecureChannel(keyset=self.scpKeyset, payload=payload)
		if isinstance(capdus, str):
			capdus = [capdus]
		for capdu in capdus:
			rapdu, sw = SendAPDU(capdu)
		return capdus, rapdu, sw

	@allure.step
	def audit_isdp_list(self, scp=80):
		if scp==80: radpu = self.scp80.remote_management(gsma.audit_isdp_list)[1]
		if scp==81: radpu = self.scp81.ram (gsma.audit_isdp_list)[1]
		if scp==3:  radpu = self.scp03     (gsma.audit_isdp_list)[1]
		return gsma.pprint_audit_isdp_list(radpu, display=True)

class Isdp:

	def __init__(self, sd, is_precreated=True):
		self.sd = sd
		self.is_precreated  = is_precreated

	def scp03(self, capdus):

		self.sd.openDefaultSecureChannel()
		if isinstance(capdus, str):
			capdus = [capdus]
		for capdu in capdus:
			data, sw = SendAPDU(capdu)
		return data, sw

class Euicc:

	eid = '6364160310000094337F000000000004'
	isdr = Isdr()
	# isdp = [
	# 		 Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001000',SCPKeyset(kvn='30',scp='0370',sEnc='6F19145916A2E906BCB019E02DCFFC52',sMac='D9D234EFE9637CCFE2856CCAA07BB704',sKek='35BDA4AA497838A3353353097B8F43BF',sequence_number='000000', kid='00'))),
	# 		 # Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001000',SCPKeyset(kvn='30',scp='0370',sEnc='C1ABB59A5C53CA787B1062580852EEB3',sMac='A9287EC31E6327E2960D2CA8D4CF9552',sKek='203003180B4F794E126E889316575A08',sequence_number='000000', kid='00'))),
	# 		 Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001100',SCPKeyset(kvn='30',scp='0370',sEnc='93B761298044C61969981448CFDDF35E',sMac='4961D619F80C82FC5708E2E356A61D31',sKek='96C606BCA73ABC91707220263986C9A0',sequence_number='000000', kid='00'))),
	# 		 Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001200',SCPKeyset(kvn='30',scp='0370',sEnc='6508682CBD08B840F0DAD61BF37B076A',sMac='61F695666B080B02BB6193FD0D735EEC',sKek='EA73C00D85D8959A31E2DADA5FA5A389',sequence_number='000000', kid='00'))),
	# 		 Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001300',SCPKeyset(kvn='30',scp='0370',sEnc='9411B1AF01E9CDB987F8CF12BAC079C8',sMac='769B1D517D6DB5327F9F4FFED24661A6',sKek='644E7A7D0ABDF7F6CF556DA939A19EBA',sequence_number='000000', kid='00'))),
	# 		 Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001400',SCPKeyset(kvn='30',scp='0370',sEnc='2FADD15DF7EA9B4CB080A42488F3EF52',sMac='1BD85E57552E70461592B43E8A3F027E',sKek='C8462934928A99A3979568E6B2BC3E62',sequence_number='000000', kid='00')))
	# 		 # Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001500',SCPKeyset(kvn='30',scp='0370',sEnc='',sMac='',sKek=''))), #try to reuse david script to find out !
	# 		]
	'''sprint 13'''
	isdp = [
			 Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001000',SCPKeyset(kvn='30',scp='0370',sEnc='6F19145916A2E906BCB019E02DCFFC52',sMac='D9D234EFE9637CCFE2856CCAA07BB704',sKek='35BDA4AA497838A3353353097B8F43BF',sequence_number='000000', kid='00')))
			 # Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001000',SCPKeyset(kvn='30',scp='0370',sEnc='C1ABB59A5C53CA787B1062580852EEB3',sMac='A9287EC31E6327E2960D2CA8D4CF9552',sKek='203003180B4F794E126E889316575A08',sequence_number='000000', kid='00'))),
			#  Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001100',SCPKeyset(kvn='30',scp='0370',sEnc='93B761298044C61969981448CFDDF35E',sMac='4961D619F80C82FC5708E2E356A61D31',sKek='96C606BCA73ABC91707220263986C9A0',sequence_number='000000', kid='00'))),
			#  Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001200',SCPKeyset(kvn='30',scp='0370',sEnc='6508682CBD08B840F0DAD61BF37B076A',sMac='61F695666B080B02BB6193FD0D735EEC',sKek='EA73C00D85D8959A31E2DADA5FA5A389',sequence_number='000000', kid='00'))),
			#  Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001300',SCPKeyset(kvn='30',scp='0370',sEnc='9411B1AF01E9CDB987F8CF12BAC079C8',sMac='769B1D517D6DB5327F9F4FFED24661A6',sKek='644E7A7D0ABDF7F6CF556DA939A19EBA',sequence_number='000000', kid='00'))),
			#  Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001400',SCPKeyset(kvn='30',scp='0370',sEnc='2FADD15DF7EA9B4CB080A42488F3EF52',sMac='1BD85E57552E70461592B43E8A3F027E',sKek='C8462934928A99A3979568E6B2BC3E62',sequence_number='000000', kid='00')))
			 # Isdp(SecurityDomain('A0000005591010FFFFFFFF8900001500',SCPKeyset(kvn='30',scp='0370',sEnc='',sMac='',sKek=''))), #try to reuse david script to find out !
			]	

	@property
	def precreated_isdps(self):
		return [isdp.sd.getAID() for isdp in self.isdp if isdp.is_precreated]

	@property
	def postcreated_isdp_keys(self):
		receipt_key     = '6518F0FCDD51E6C3E7BE55D1BE99C627'
		enc             = '60F76381159ED922DD8FCF1D21C7B69E'
		mac             = '4C26903C311128079C4D2146D870FDA7'
		dek             = 'AF539B65D4181E0412501DB20BBE1272'
		return enc, mac, dek

	def get_isdp_keyset_scp03t(self, aid_isdp):
		aid_isdp = is_short_aid(aid_isdp)
		if aid_isdp in self.precreated_isdps:
			precreated_isdp_kset = self.isdp[get_short_aid(aid_isdp)-10].sd.getDefault_SCPKeyset() ## ugly way to get precreated isdp info !!
			sEnc = precreated_isdp_kset.getSCPEncryptionKey()
			sMac = precreated_isdp_kset.getSCPMessageAuthenticationCodeKey()
			sKek = precreated_isdp_kset.getDataEncryptionKey()
			print(sEnc)
			print(sMac)
			print(sKek)
		else:
			sEnc, sMac, sKek = self.postcreated_isdp_keys

		#return SCPKeyset(kvn='30', scp='0370', sEnc=sEnc, sMac=sMac, sKek=sKek, sequence_number='000001', kid='00'). issue due to SPI update ?
		return SCPKeyset(kvn='30', scp='0370', sEnc=sEnc, sMac=sMac, sKek=sKek, sequence_number='000000', kid='00')

	def is_default_profile(self, aid_isdp):
		return aid_isdp == self.isdp[0].sd.getAID()

	@property
	@allure.step
	def audit_pol1_rules(self):
		profile_enabled = self.isdr.audit_isdp_list(scp=80)['aid_profile_enabled']
		isdp_keyset = self.get_isdp_keyset_scp03t(profile_enabled)
		isdp = Isdp(SecurityDomain(profile_enabled, isdp_keyset))
		data, sw = isdp.scp03(gsma.audit_pol1_rules)
		LogInfo('POL1 rules: {} {}'.format(data[0:2], gsma.POL1_CODING[data[0:2]]))
		return data[0:2], gsma.POL1_CODING[data[0:2]]


	@property
	def mno(self):
		pprint.h4("checking enabled profile")
		pprint.h4("audit isdp list")
		profile_enabled = self.isdr.audit_isdp_list(scp=80)['aid_profile_enabled']
		# capdu,rapdu,sw = model.Smsr.audit_isdp_enabled(scp=80, apdu_format='definite')
		# profile_enabled = rapdu[16:48]
		pprint.h4("sent back a configuration for SCP80/81 on MNO")
		if self.is_default_profile(profile_enabled):
			pprint.h4("precreated profile enable")
			return Mno(aid_isdp=profile_enabled, scp80=model.SCP80(parent_aid=profile_enabled, spi1=0x12, spi2=0x21, kic=0x19, kic_key='F8195839102919DEAB908947198438FA3489DEAE78BC45EC', kid=0x19, kid_key='F8195839102919DEAB908947198438FA3489DEAE78BC45EC', tar='B20100', algo_cipher=const.hexa.ciphers['3DES CBC 3 KEYS'], algo_crypto_verif=const.hexa.ciphers['3DES CBC 3 KEYS']))
			# return Mno(aid_isdp=profile_enabled, scp80=model.SCP80(spi1=0x16, spi2=0x21, kic=0x15, kic_key='F8195839102919DEAB908947198438FA', kid=0x15, kid_key='F8195839102919DEAB908947198438FA', tar='B20100', algo_cipher=const.hexa.ciphers['3DES CBC 2 KEYS'], algo_crypto_verif=const.hexa.ciphers['3DES CBC 2 KEYS']))
		else:
			pprint.h4("new created profile enable")
			return Mno(aid_isdp=profile_enabled, scp80=model.SCP80(parent_aid=profile_enabled, spi1=0x16, spi2=0x21, kic=0x15, kic_key='16'*16, kid=0x15, kid_key='17'*16, tar='B20100', algo_cipher=const.hexa.ciphers['3DES CBC 2 KEYS'], algo_crypto_verif=const.hexa.ciphers['3DES CBC 2 KEYS'])  )
			# return Mno(aid_isdp=profile_enabled, scp80=model.SCP80(parent_aid=profile_enabled, spi1=0x16, spi2=0x21, kic=0x12, kic_key='16'*16, kid=0x12, kid_key='17'*16, tar='B20100', algo_cipher=const.hexa.ciphers['AES'], algo_crypto_verif=const.hexa.ciphers['NONE'])  )

#            if(getSAAAAXCode() == "41594"):
#                return Mno(aid_isdp=profile_enabled, scp80=model.SCP80(parent_aid=profile_enabled, spi1=0x16, spi2=0x39, kic=0x15, kic_key='16'*16, kid=0x15, kid_key='17'*16, tar='B20100', algo_cipher=const.hexa.ciphers['3DES CBC 2 KEYS'], algo_crypto_verif=const.hexa.ciphers['3DES CBC 2 KEYS'])  )
#            else:
#                return Mno(aid_isdp=profile_enabled, scp80=model.SCP80(parent_aid=profile_enabled, spi1=0x16, spi2=0x21, kic=0x15, kic_key='16'*16, kid=0x15, kid_key='17'*16, tar='B20100', algo_cipher=const.hexa.ciphers['3DES CBC 2 KEYS'], algo_crypto_verif=const.hexa.ciphers['3DES CBC 2 KEYS'])  )




			# return Mno(aid_isdp=profile_enabled, scp80=model.SCP80(parent_aid=profile_enabled, spi1=0x12, spi2=0x02, kic=0x00, kic_key='16'*16, kid=0x15, kid_key='17'*16, tar='B20100', algo_cipher=const.hexa.ciphers['3DES CBC 2 KEYS'], algo_crypto_verif=const.hexa.ciphers['3DES CBC 2 KEYS'])  )

	@allure.step
	def open_secure_channel_isdr(self):
		# print (isdr)
		capdu,rapdu,sw = self.isdr.scp03()
		print ('capdu = {}'.format(capdu))
		print ('rapdu = {}'.format(rapdu))
		print ('sw = {}'.format(sw))

		# profile_enabled = self.isdr.audit_isdp_list(scp=3)['aid_profile_enabled']
		pass

	@allure.step
	def activate_local_fallback(self):
		Send('8032800000')

	@allure.step
	def deactivate_local_fallback(self):
		Send('8032000000')

	@allure.step
	def activate_era_glonass(self):
		Send('A0C2000005EE03EF0120')

	@allure.step
	def deactivate_era_glonass(self):
		Send('80C2000005EE03EF0124')

	@allure.step
	def update_counter_frm(self,counter=10):
		return Envelope('EE06EF0130EB01{:02X}'.format(int(counter)))
	
	@allure.step
	def reset(self):
		return "80E2880004DF6D0107"

	@property
	@allure.step
	def select_psm_applet(self):
		data, sw = Select("A0000000770107001C0000FE00000E00")
		return data, sw
	
	@allure.step
	def config_rollback_counter(self,counter='03'):
		Send('80C20000 08 EE 06 EF0130 EB01' + counter)

	@allure.step
	def notification_retry_mechanism_state(self, state='disable'):
		if state == 'enable'    : capdu = '80FE000003 3901 01'
		if state == 'disable'   : capdu = '80FE000003 3901 00'
		data, sw = self.select_psm_applet
		data, sw = SendAPDU(capdu)
		return  capdu, data, sw
	
	@allure.step
	def configure_retry_number(self, interface='applet', number_of_retry='03', scp=80, apdu_format='definite'):
		if interface == 'applet':
			capdu = '80FE000003 3801 {}'.format(number_of_retry)
			data, sw = self.select_psm_applet
			data, sw = SendAPDU(capdu)
		elif interface == 'store data':
			capdu = '80E2880006DF6D033801{}'.format(number_of_retry)
			if(getSAAAAXCode() == "41594"):
				server.ram(capdu, scp=80,  apdu_format='definite')
			else:
				self.ram(capdu, scp=80, apdu_format='definite')
		return  capdu, data, sw

	@allure.step
	def init(self, catch_first_notif=False, send_server_confirmation=False, loci_change=False, timer_management_failed=False, pli_failed=False, open_channel_failed=False, terminalProfile_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008"):
		pprint.h3("euicc init - restart handset")
		LogInfo('>> device initialization')
		PowerOn()
		sw = device.terminal_profile(tp_data = terminalProfile_data)
		data, sw, last_proactive_cmd = device.fetch_all(sw,return_if=['open channel', 'send sms'], catch_first_notif=catch_first_notif, loci_change=loci_change, timer_management_failed=timer_management_failed, pli_failed=pli_failed, open_channel_failed=open_channel_failed)

		### Added by hasbi ###
		# Uncomment scripts below when OTA Poller applet already added
		# sw = device.terminal_profile()
		# data, sw = Fetch(sw[2:4])
		# sw = TerminalResponse(data)
		# data, sw = device.envelope_location_status(status='normal service')
		# data, sw, last_proactive_cmd = device.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False)

		if catch_first_notif:
			# LogInfo("euicc.init - catch first notif")
			pprint.log("euicc", "init", "catch_first_notif", Constants.debugMode)
			# pprint.h3("First Notif / Network Attachment (?)")
			data, sw = device.envelope_location_status(status='normal service')
			while '90' in sw:
				data, sw = device.get_status()
			while '91' in sw:
				LogInfo(">>First Open Channel")
				data, sw, last_proactive_cmd = device.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False, loci_change=loci_change)
				# proactive command open_channel (http notification)
				if last_proactive_cmd == 'open channel':
					LogInfo('catch euicc first notification (http) {0}'.format('BUT no server confirmation' if not send_server_confirmation else '+ server confirmation'))
					if send_server_confirmation:
						return self.isdr.scp81.remote_management(gsma.handle_notification_confirmation(), sw)
					else:
						capdu, rapdu, sw, seq_counter = self.isdr.scp81.remote_management(None, sw)
						assert (capdu, rapdu) == (None, None)
						data, sw = device.envelope_timer_expiration(id='psm')
						LogInfo(">>Second Open Channel")
						data, sw, last_proactive_cmd = device.fetch_all(sw, return_if=['open channel'], catch_first_notif=False)
						LogInfo('catch 2nd euicc first notification (http) + server confirmation')
						return self.isdr.scp81.remote_management(gsma.handle_notification_confirmation(), sw)

				elif last_proactive_cmd == 'send sms':
					seq_number = pprint.pprint_sms_first_notif(data)
					data, sw, cmd_type = device.fetch_all(sw)

					if send_server_confirmation:
						LogInfo('server confirms reception of euicc notification')
						capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number))

					else:
						LogInfo('server DOES NOT confirm reception of euicc notification')
						data, sw = device.envelope_timer_expiration(id='psm')
						data, sw = device.fetch_one(sw, 'send sms')
						seq_number = pprint.pprint_sms_first_notif(data)
						data, sw, cmd_type = device.fetch_all(sw)
						LogInfo('server confirms reception of euicc notification (second one because first was not confirmed)')
						capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number))
		else:
			# LogInfo("euicc.init - NOT catch first notif")
			if last_proactive_cmd == 'open channel':
				capdu, rapdu, sw, seq_counter = self.isdr.scp81.remote_management(None, sw)
				# return capdu,rapdu,sw
				if sw!="9000":
					data, sw, cmd_type = device.fetch_all(sw)
				return sw
				# if("3C0301"in data):
				# 	data, sw, last_proactive_cmd = device.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False, loci_change=loci_change)											
			elif last_proactive_cmd == 'send sms':
				seq_number = pprint.pprint_sms_first_notif(data)
				data, sw, cmd_type = device.fetch_all(sw)
				LogInfo('server confirms reception of euicc notification')
				capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number))

	@allure.step
	def refresh(self, sw=None, no_service=False, server_confirm=True, fallback_only=True, unlimited=False, terminalProfile_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008"): ### ===> SGP 2 v3.2 new parameter => fallback_only=True
		if sw is not None:
			LogInfo('>>> fetch refresh \n')
			data, sw = device.fetch_one(sw, 'refresh')			
			if sw != '9000':
				sw = device.fetch_all(sw)

		# self.init()
		pprint.h3("euicc init - restart handset")
		LogInfo('>> device initialization')
		PowerOn()
		sw = device.terminal_profile(tp_data = terminalProfile_data)
		data, sw, last_proactive_cmd = device.fetch_all(sw,return_if=['timer management'])
		if last_proactive_cmd == 'timer management':
			if '240103' in data:
				timer_id = 'psm'
			elif '240104' in data:
				timer_id = 'psm4'
			elif '240105' in data:
				timer_id = 'psm5'
		
		if sw != '9000':
			data, sw, last_proactive_cmd = device.fetch_all(sw,return_if=['open channel', 'send sms'])

		if no_service:
			# if fallback_only: ### ===> SGP 2 v3.2 new condition check
			# 	### Default behavior ###
			# 	i = 0
			# 	if(getSAAAAXCode() != "41594"):
			# 		data, sw = device.envelope_location_status('no service', 1)
			# 		data, sw, last_proactive_cmd = device.fetch_all(sw)
			# 	while not i or sw == '9000':
			# 		i += 2
			# 		data, sw = device.envelope_location_status('no service', i)
			# 		# data, sw, last_proactive_cmd = device.fetch_all(sw)
			# 		assert i != 143, 'no fallback despite x143 env location status no service!'
			# 	self.refresh(sw, no_service=False)

			# ### SGP 2 v3.2  New Behavior ###
			# else:
			# 	i = 0
			# 	if(getSAAAAXCode() != "41594"):
			# 		data, sw = device.envelope_location_status('no service', 1)
			# 		data, sw, last_proactive_cmd = device.fetch_all(sw)
			# 	while not i or sw == '9000':
			# 		i += 2
			# 		data, sw = device.envelope_location_status('no service', i)
			# 		# data, sw, last_proactive_cmd = device.fetch_all(sw)
			# 		assert i != 143, 'no fallback despite x143 env location status no service!'
			# 	self.refresh(sw, no_service=False)
			# 	data, sw = device.envelope_timer_expiration(id='psm')
			# 	if sw != '9000' : self.refresh(no_service=False)
			#   ################################

			i = 0
			#remove this on sprint 13 vvv
			# if(getSAAAAXCode() != "41594"):
			# 	data, sw = device.envelope_location_status('no service', 1)
			# 	data, sw, last_proactive_cmd = device.fetch_all(sw)
			#remove this on sprint 13 ^^^
			while not i or sw == '9000':
				i += 2
				data, sw = device.envelope_location_status('no service', i)
				# data, sw, last_proactive_cmd = device.fetch_all(sw)
				# SPRINT 12
				if i == 1:
					if sw != '9000':
						data, sw = device.fetch_one(sw)				
				assert i != 143, 'no fallback despite x143 env location status no service!'
			self.refresh(sw, no_service=False, fallback_only=fallback_only, unlimited=False)

		else:
			data, sw = device.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = device.fetch_all(sw, return_if=['open channel', 'send sms'])
				assert sw[0:2] != 91, 'notification (sms or http) was expected!'

			if server_confirm:
				if last_proactive_cmd == 'open channel':

					'''Retry Mechanism'''
					# i = 1
					# while i <= 3:
					#     LogInfo("{} Open Channel".format(i))
					#     capu, rapdu, sw, seq_number = self.isdr.scp81.remote_management(None, pull=sw)
					#     assert (capu, rapdu) == (None, None)
					#     data, sw = device.envelope_timer_expiration(id='psm')
					#     if sw =="9000" :
					#         return self.isdr.scp81.remote_management(gsma.handle_notification_confirmation(seq_number=seq_number))
					#         # return self.mno.confirm_notification(scp=scp, apdu_format=apdu_format)

					#     LogInfo(">> Fetch Timer Management")
					#     data, sw, last_proactive_cmd = device.fetch_all(sw, return_if=['open channel'], catch_first_notif=False)
					#     i=i+1

					capdu,rapdu,sw = self.isdr.scp81.remote_management(gsma.handle_notification_confirmation(), sw)
					
					if sw != '9000':
						sw = device.fetch_all(sw)

					if not fallback_only: ### ===> SGP 2 v3.2 new condition check
						data, sw = device.envelope_timer_expiration(id=timer_id)
						if not unlimited:
							if sw == '910B' : self.refresh(sw, no_service=False)
						else:
							if sw == '910B' : self.refresh(sw, no_service=True)

					return capdu,rapdu,sw
					# return self.isdr.scp81.remote_management(gsma.handle_notification_confirmation(), sw)
				elif last_proactive_cmd == 'send sms':
					seq_number = pprint.pprint_sms_first_notif(data)
					data, sw, cmd_type = device.fetch_all(sw)
					LogInfo('server confirms reception of euicc notification')
					capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number)) 

					if sw != '9000':
						sw = device.fetch_all(sw)

					
					if not fallback_only: ### ===> SGP 2 v3.2 new condition check
						data, sw = device.envelope_timer_expiration(id='other')
						if not unlimited:
							if sw == '910B' : self.refresh(sw, no_service=False)
						else:
							if sw == '910B' : self.refresh(sw, no_service=True)
				# LogInfo('sw = {}'.format(sw))												
				# if sw != "9000":
				# 	data, sw, cmd_type = device.fetch_all(sw)

			else:
				if last_proactive_cmd == 'open channel':
					capdu, rapdu, sw, seq_counter = self.isdr.scp81.remote_management(None, sw)
					assert (capdu, rapdu) == (None, None)
					data, sw = device.envelope_timer_expiration(id='psm')
					LogInfo(">>Second Open Channel")
					data, sw, last_proactive_cmd = device.fetch_all(sw, return_if=['open channel'], catch_first_notif=False)
					data, sw = device.envelope_timer_expiration(id='psm')
					data, sw = device.fetch_one(sw)
					data, sw = device.envelope_timer_expiration(id='psm')
					data, sw, cmd_type = device.fetch_all(sw)
					data, sw = device.envelope_timer_expiration(id='psm')

				elif last_proactive_cmd == 'send sms':
					LogInfo('server DOES NOT confirm reception of euicc notification')
					data, sw, cmd_type = device.fetch_all(sw)
					i=1
					for i in range (2):
						data, sw = device.envelope_timer_expiration(id='psm')
						data, sw = device.fetch_one(sw, 'send sms')
						seq_number = pprint.pprint_sms_first_notif(data)
						data, sw, cmd_type = device.fetch_all(sw)
					data, sw = device.envelope_timer_expiration(id='psm')
					data, sw, cmd_type = device.fetch_all(sw)
					data, sw = device.envelope_timer_expiration(id='psm')
					data, sw = device.fetch_one(sw)
					data, sw = device.envelope_timer_expiration(id='psm')
					data, sw, cmd_type = device.fetch_all(sw)
					data, sw = device.envelope_timer_expiration(id='psm')

	def init_notif(self, catch_first_notif=False, send_server_confirmation=False):
		LogInfo('>> device initialization')
		PowerOn()
		sw = device.terminal_profile()
		device.fetch_all(sw)

		if catch_first_notif:
			data, sw = device.envelope_location_status(status='normal service')
			while '91' in sw:
				LogInfo(">>First Open Channel")
				data, sw, last_proactive_cmd = device.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False)
				# proactive command open_channel (http notification)
				if last_proactive_cmd == 'open channel':
					LogInfo('catch euicc first notification (http) {0}'.format('BUT no server confirmation' if not send_server_confirmation else '+ server confirmation'))
					if send_server_confirmation:
						return self.isdr.scp81.remote_management(gsma.handle_notification_confirmation(), sw)
					else:
						return self.isdr.scp81.remote_management(None, sw)

				# proactive command send_sms (sms notification)
				elif last_proactive_cmd == 'send sms':
					seq_number = pprint.pprint_sms_first_notif(data)
					data, sw, cmd_type = device.fetch_all(sw)

					if send_server_confirmation:
						LogInfo('server confirms reception of euicc notification')
						capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number))

					else:
						LogInfo('server DOES NOT confirm reception of euicc notification')
						data, sw = device.envelope_timer_expiration(id='psm')
						data, sw = device.fetch_one(sw, 'send sms')
						seq_number = pprint.pprint_sms_first_notif(data)
						data, sw, cmd_type = device.fetch_all(sw)
						LogInfo('server confirms reception of euicc notification (second one because first was not confirmed)')
						capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number))

	def init_loci_change(self):
		LogInfo('>> device initialization')
		PowerOn()
		sw = device.terminal_profile()
		data, sw = Fetch(sw[2:])
		sw = TerminalResponse('81030126018202818283010014 08 1111111122222212')
		data, sw = Fetch(sw[2:])
		sw = TerminalResponse('8103012600820282818301001307'+str(randint(0,99999999999999)))
		device.fetch_all(sw)




if __name__ == '__main__':
	SetLogLevel('debug')
	self.init()
