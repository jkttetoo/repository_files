import allure
from util import allow_short_aid_for_isdp, d2h, X9_63_KeyDerivationFunction, compute_receipt
from Oberthur import LogInfo, lv, berLv
from Ot.GlobalPlatform import *
import collections, const
from terminaltables import AsciiTable
from util import pprint

import Oberthur.OtCrypto as Cryptox

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

class Gsma:

	POL1_CODING = {
		'00': 'No POL1 rule active',
		'01': 'Disabling of this profile not allowed',
		'02': 'Deletion of this profile not allowed',
		'04': 'Profile deletion is mandatory when its state is changed to disabled'
	}
	DETECTION_CONDITION = {
		'mcc mnc' 				: ('E10102', 2),
		'mnc'	  				: ('E10101', 1),
		'mcc'	  				: ('E10100', 0)
	}

	TRIGGERING_CONDITION = {
		'power-on only' 		: ('E20101', 4),
		'detection only'		: ('E20102', 8),
		'power-on and detection': ('E20103', 12)
	}

	@allure.step
	@allow_short_aid_for_isdp
	def install4install(self, aid_isdp):
		# LogInfo('install4install')
		pprint.h3("install for install "+str(aid_isdp))
		return '80E60C003F10A0000005591010FFFFFFFF8900000D00' + '10' + 'A0000005591010FFFFFFFF8900000E00' + '10' + aid_isdp + '0380C00006C9048102037000'
		# return '80E60C00'+berLv(lv('A0000005591010FFFFFFFF8900000D00') + lv('A0000005591010FFFFFFFF8900000E00') + lv(aid_isdp) + lv('80C000')+lv('C9'+lv('81020370'))+'00')

	@allure.step
	@allow_short_aid_for_isdp
	def install4perso(self, aid_isdp):
		pprint.h3("install for perso "+str(aid_isdp))
		# LogInfo('install4perso')
		return '80E6200016000010' + aid_isdp + '00000000'

	@property
	@allure.step
	def store_dp_certificate(self):
		# LogInfo('store data: dp certificate')
		pprint.h3("store data dp certificate")
		smdp_certificate = collections.OrderedDict()
		smdp_certificate['certificate_serial_number'] = ['93', '00000000000000000000000000000000']
		smdp_certificate['ca_id'] = ['42', '00000000000000000000000000000000']
		smdp_certificate['subject_id'] = ['5F20', '00000000000000000000000000000000']
		smdp_certificate['key_usage'] = ['95', '80']
		smdp_certificate['effective_date'] = ['5F25', '20150421']
		smdp_certificate['expiration_date'] = ['5F24', '20250418']
		smdp_certificate['discretionary_data_smdp_certificate'] = ['73', 'C80101']
		smdp_certificate['smdp_public_key'] = ['7F49', 'B04104' + 'F98D938CA89E66D3E4977B367FE9CD6B45BF2F8677EBA9C630835124618F681CAFEE8D3EFDA71FC35BE2E0A97CE02D1A59A90EB7EE4FFDEA21FB2B37EA9039E2F00100']
		smdp_certificate['ci_signature'] = ['5F37', '85CC0B201AA886A0D333500F086E06937179928727F6F6984A2DF8A9EB69609E02B8C862618A1A3FD301B0C574D8BD0BD83099FF2C3F6D22769DC6FE95FA7C0D']
		return '80E20900' + lv( '3A01' + lv( '7F21' + berLv( d2h(smdp_certificate) ) ) ) + '00'

	def store_dp_certificate2(self, smdp_public_key, sign):
		# LogInfo('second store data: ')
		pprint.h3("store data dp certificate 2")
		smdp_certificate = collections.OrderedDict()
		smdp_certificate['scenario_identifier'] = ['90', '08']
		smdp_certificate['key_usage_qualifier'] = ['95', '10']
		smdp_certificate['key_access'] = ['96', '00']
		smdp_certificate['key_type'] = ['80', '88']
		smdp_certificate['key_length'] = ['81', '10']
		smdp_certificate['key_identifier'] = ['82', '01']
		smdp_certificate['key_version_number'] = ['83', '70']
		# smdp_certificate['init_sequence_counter'] = ['91']
		# smdp_certificate['security_domain_image_number'] = ['45', signature]
		# smdp_certificate['host_id'] = ['84', signature]
		temp = berLv( d2h(smdp_certificate) ) + '9100'
		# smdp_certificate['ePK'] = ['7F49', smdp_public_key]
		# smdp_certificate['signature'] = ['5F37', sign]
		ePK = '7F49' + lv('B04104'+smdp_public_key)
		signature = '5F37' + lv(sign)

		# return '80E28901' + lv( '3A02' + lv( 'A6' + berLv( d2h(smdp_certificate) ) ) ) + '00'
		return '80E28901' + lv( '3A02' + lv( 'A6' + temp + lv(ePK) + lv(signature)) ) + '00'

	@allure.step
	def store_isdp_keys(self, isdr, random_challenge):

		EC_PK_X_256_1       = '558ad2b8fa552c3a426105ee358798137a457bd875c959e04609c82541478f30'
		EC_PK_Y_256_1       = '8c4c325ee3dd2f7bf53d94515d752acb0033e88757f96366d240ef65e160b773'
		ePK_DP_ECKA         = EC_PK_X_256_1 + EC_PK_Y_256_1
		BERTLV_3A02         = '3A02'+'17A615'+'90020300'+'950110'+'800188'+'810110'+'820101'+'830130'+'9100'
		BERTLV_7F49 		= '7F49' + lv('04' + ePK_DP_ECKA)
		BERTLV_0085 		= '0085' + lv(random_challenge)
		Data_SIGNATURE 		= BERTLV_3A02 + BERTLV_7F49 + BERTLV_0085
		SK_DP_ECDSA        	= '8d6052dcb8e2c69e5ff1b8497785e55dd91fa8459189a10ea29f4d7784f52c4e'

		SIGN_ECDSA_ECC_KEY_PRIVATE     = lv('FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv(SK_DP_ECDSA)

		Ec_PrivateKey = Set_EC_PrivateKey(SIGN_ECDSA_ECC_KEY_PRIVATE, CoFactor=True)
		Data = SHA256(Data_SIGNATURE)
		ECDSA_Sign = Generate_EC_Signature_ECDSA(Data, Ec_PrivateKey)
		signature = ECDSA_Sign[0] + ECDSA_Sign[1]
		LogInfo("data: {}".format(Data_SIGNATURE))
		LogInfo("signature: {}".format(signature))

		store_keys =  BERTLV_3A02 + BERTLV_7F49 + '5F37' + lv(signature)
		storeDatacmd01 = isdr.storeData(store_keys, P1='09' , lastBlock=True, blockNumber='01', payload=True)

		return storeDatacmd01[0] + '00'

	def generates_keys(self, random_challenge):

		EC_PK_X_256_1       = '558ad2b8fa552c3a426105ee358798137a457bd875c959e04609c82541478f30'
		EC_PK_Y_256_1       = '8c4c325ee3dd2f7bf53d94515d752acb0033e88757f96366d240ef65e160b773'
		ePK_DP_ECKA         = EC_PK_X_256_1 + EC_PK_Y_256_1
		BERTLV_3A02         = '3A02'+'17A615'+'90020300'+'950110'+'800188'+'810110'+'820101'+'830130'+'9100'
		BERTLV_7F49 		= '7F49' + lv('04' + ePK_DP_ECKA)
		BERTLV_0085 		= '0085' + lv(random_challenge)
		Data_SIGNATURE 		= BERTLV_3A02 + BERTLV_7F49 + BERTLV_0085
		SK_DP_ECDSA        	= '8d6052dcb8e2c69e5ff1b8497785e55dd91fa8459189a10ea29f4d7784f52c4e'

		SIGN_ECDSA_ECC_KEY_PRIVATE     = lv('FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv('4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5')
		SIGN_ECDSA_ECC_KEY_PRIVATE    += lv(SK_DP_ECDSA)

		Ec_PrivateKey = Set_EC_PrivateKey(SIGN_ECDSA_ECC_KEY_PRIVATE, CoFactor=True)
		Data = SHA256(Data_SIGNATURE)
		ECDSA_Sign = Generate_EC_Signature_ECDSA(Data, Ec_PrivateKey)
		signature = ECDSA_Sign[0] + ECDSA_Sign[1]
		LogInfo("data: {}".format(Data_SIGNATURE))
		LogInfo("signature: {}".format(signature))

		return ePK_DP_ECKA, signature


	@allure.step
	def verify_receipt(self, euicc_receipt = '00'*16):

		# 256 values, standard curves NIST P-256
		EC_256_P    = 'FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF'
		EC_256_R    = 'FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551'
		EC_256_A    = 'FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC'
		EC_256_B    = '5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B'
		EC_256_X    = '6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296'
		EC_256_Y    = '4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5'
		EC_256_G    = '04' + EC_256_X  + EC_256_Y
		EC_256_N    = 'FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551'
		EC_COFACTOR = '0001'

		# 256 public and private keys
		EC_256_QX   = '18F124E0B4A6D964CEFC8453ED903D52F1B8C85258FE5B2459776C0630DC02FE'
		EC_256_QY   = 'F6D0B082D6D10B1EA728A50037AE69EBD7CCA10B1C73DA8AC3C287D2704BF325'
		EC_256_S    = 'DA4C12BA8C1BA9F2F75AFA3C20A760C230DCC8C084DE276A31E9F2B0A2BB9392'
		EC_256_PK   =  EC_256_QX + EC_256_QY

		# public key of the eCASD
		ePK_eCASD_ECKA = lv(EC_256_P)
		ePK_eCASD_ECKA += lv(EC_256_R)
		ePK_eCASD_ECKA += lv(EC_256_A)
		ePK_eCASD_ECKA += lv(EC_256_B)
		ePK_eCASD_ECKA += lv(EC_256_X)
		ePK_eCASD_ECKA += lv(EC_256_Y)
		ePK_eCASD_ECKA += lv(EC_256_QX)
		ePK_eCASD_ECKA += lv(EC_256_QY)

		# set EC GF(p) public key value
		EC_GF_p_public_key = Set_EC_PublicKey(ePK_eCASD_ECKA, CoFactor=True)

		# ephemeral Private Key of the AP
		eSK_AP_ECKA = lv(EC_256_P)
		eSK_AP_ECKA += lv(EC_256_R)
		eSK_AP_ECKA += lv(EC_256_A)
		eSK_AP_ECKA += lv(EC_256_B)
		eSK_AP_ECKA += lv(EC_256_X)
		eSK_AP_ECKA += lv(EC_256_Y)
		eSK_AP_ECKA += lv('DA4C12BA8C1BA9F2F75AFA3C20A760C230DCC8C084DE276A31E9F2B0A2BB9392')

		EC_PK_X_256_1 = '558ad2b8fa552c3a426105ee358798137a457bd875c959e04609c82541478f30'
		EC_PK_Y_256_1 = '8c4c325ee3dd2f7bf53d94515d752acb0033e88757f96366d240ef65e160b773'


		# set EC GF(p) private key value
		EC_GF_p_private_key = Set_EC_PrivateKey(eSK_AP_ECKA, CoFactor=True)

		# generates a shared secret for ECDH key agreement.
		# it uses an elliptic curve private key initialized and the elliptic curve public point.
		# compute standard Diffie-Hellman over GF(p)
		EC_Point = Set_EC_Point(EC_PK_X_256_1, EC_PK_Y_256_1)
		EC_Secret_ECDH = Generate_EC_Secret_ECDH(EC_GF_p_private_key, EC_Point, CoFactor=True)

		LogInfo("Shared Secret (ShS): " + EC_Secret_ECDH)

		LogInfo("Application Provider (AP) derives keyset from ShS") # (and DR if any)")
		ZAB  = EC_Secret_ECDH
		SharedInfo = '10' + '88' + '10' # + DR
		keyDerivation = X9_63_KeyDerivationFunction(ZAB, '10','10', SharedInfo)

		key_receipt = keyDerivation[:32]
		new_enc     = keyDerivation[32:64]
		new_mac     = keyDerivation[64:96]
		new_dek     = keyDerivation[96:128]

		BERTLV_3A02      = '3A0217A615900203009501108001888101108201018301309100'
		message          = BERTLV_3A02[6:]

		computed_receipt = compute_receipt(message, key_receipt)

		LogInfo(">> Comparing receipt received from euicc and receipt computed by AP...")
		assert computed_receipt == euicc_receipt, '... error, the two receipt have different value, end of program!'
		LogInfo("... OK. Establish ISDP Keyset opeartion is successful:")
		LogInfo("key_receipt: {}".format(key_receipt))
		LogInfo("sEnc: {}".format(new_enc))
		LogInfo("sMac: {}".format(new_mac))
		LogInfo("sDek: {}".format(new_dek))
		return True

	@allure.step
	@allow_short_aid_for_isdp
	def enable_profile(self, aid_isdp):
		pprint.h3("enable profile "+str(aid_isdp))
		return '80E28800153A03124F10' + aid_isdp

	@allure.step
	@allow_short_aid_for_isdp
	def disable_profile(self, aid_isdp):
		pprint.h3("disable profile "+str(aid_isdp))
		return '80E28800153A04124F10' + aid_isdp

	@allure.step
	@allow_short_aid_for_isdp
	def delete_profile(self, aid_isdp):
		pprint.h3("delete profile "+str(aid_isdp))
		return '80E40040124F10' + aid_isdp

	@allure.step
	@allow_short_aid_for_isdp
	def delete_keysets(self, kvn):
		return '80E4000005F203'+kvn+'010300'

	@property
	@allure.step
	def audit_isdp_list(self):
		pprint.h3("audit isdp list")
		return '80F24002094F005C054F9F7053B500'
	
	@allow_short_aid_for_isdp
	def set_emergency_profile(self, aid_isdp):
		pprint.h3("set emergency profile "+str(aid_isdp))
		return '80E28800153A09124F10' + aid_isdp
	

	@allure.step
	def pprint_audit_isdp_list(self, rapdu, display=True):

		life_cycle_state = {
			''   : 'unknown',
			'3F' : 'Enabled',
			'1F' : 'Disabled',
			'0F' : 'Personalized',
			'07' : 'Selectable',
			'03' : 'Installed'
		}

		result = {
			'aid_profile_enabled' : None,
			'aid_profile_has_fallback_attribute' : None,
			'list_aid' : ['AID ISD-P'],
			'list_life_cycle_state' : ['Life Cycle State'],
			'list_life_cycle_state_meaning' : ['Life Cycle State Meaning'],
			'list_has_fallback_attribute': ['Fallback'],
		}

		tag_start = '4F'

		rapdu = rapdu[4:] # drop header of rapdu expanded format

		while(rapdu.find(tag_start) != -1):

			isdp_aid_len_base_16 = rapdu[ rapdu.find(tag_start) +2 : rapdu.find(tag_start) +4 ]
			isdp_aid_val = rapdu[ rapdu.find(tag_start) +4 : rapdu.find(tag_start) +4+2*int(isdp_aid_len_base_16, 16)]
			rapdu = rapdu[rapdu.find(tag_start) +4+2*int(isdp_aid_len_base_16, 16):]
			isdp_life_cycle_state = rapdu[rapdu.find('9F7001')+6:rapdu.find('9F7001')+8]
			isdp_is_fallback = rapdu[rapdu.find('5301')+4:rapdu.find('5301')+6]

			result['list_aid'].append( isdp_aid_val )
			result['list_life_cycle_state'].append( '{} {}'.format( isdp_life_cycle_state, life_cycle_state[isdp_life_cycle_state] ))
			#result['list_life_cycle_state_meaning'].append( life_cycle_state[ isdp_life_cycle_state ] )
			result['list_has_fallback_attribute'].append( 'X' if isdp_is_fallback == '01' else '')

		result['aid_profile_enabled'] = result['list_aid'][ result['list_life_cycle_state'].index('3F Enabled') ]
		result['aid_profile_has_fallback_attribute'] = result['list_aid'][ result['list_has_fallback_attribute'].index('X') ]

		if display: pprint.table( [ result['list_aid'], result['list_life_cycle_state'], result['list_has_fallback_attribute'] ] )

		return result


	''' GET STATUS '''

	@property
	@allure.step
	def audit_isdp_enabled(self):
		return '80F240020B4F009F70013F5C034F9F7000'
	
	@property
	@allure.step
	def audit_isdp_disabled(self):
		return '80F240020B4F009F70011F5C034F9F7000'

	@allure.step
	@allow_short_aid_for_isdp
	def audit_profile_memory(self, aid_isdp='A0000005591010FFFFFFFF8900001000'):
		return '80F24002194F10' + aid_isdp + '5C054F9F708F91'+'00'


	''' GET DATA '''

	@property
	@allure.step
	def audit_pol1_rules(self):
		return '80CADF6D02'

	@property
	@allure.step
	def audit_resource_info(self):
		return '80CAFF210F'

	@property
	@allure.step
	def audit_os_version(self):
		return '80CADF6D00'
	
	@property
	@allure.step
	def get_data_E0(self):
		return '80CA00E000'
	@property
	@allure.step
	def get_data_certificate(self):
		return "80CABF30045C027F21"
	
	@property
	@allure.step
	def audit_propietary_psm(self):
		return "80CADF6D00"
	
	@property
	@allure.step
	def audit_connectivity_psm(self):
		return "80CA3A0700"


	@allure.step
	def pprint_audit_resource_info(self, rapdu):

		nb_app_index                = rapdu.find('81', rapdu.find('FF21'))
		non_volatile_memory_index   = rapdu.find('82', rapdu.find('FF21'))
		volatile_memory_index       = rapdu.find('83', rapdu.find('FF21'))


		nb_app_len              = rapdu[nb_app_index+2:nb_app_index+2+2]
		non_volatile_len        = rapdu[non_volatile_memory_index+2:non_volatile_memory_index+2+2]
		volatile_len            = rapdu[volatile_memory_index+2:volatile_memory_index+2+2]


		nb_app              = rapdu[nb_app_index+4 : nb_app_index+4 + 2*int(nb_app_len, 16)]
		non_volatile_memory = rapdu[non_volatile_memory_index+4 : non_volatile_memory_index+4 + 2*int(non_volatile_len, 16)]
		volatile_memory     = rapdu[volatile_memory_index+4 : volatile_memory_index+4 + 2*int(volatile_len, 16)]


		#pprint.table( [ ['Nb App', nb_app], ['Non Volatile Memory', non_volatile_memory], ['Volatile Memory', ] ] )

		pprint.table([
			['Resources','Nb App','Non Volatile Memory','Volatile Memory'],
			['hexadecimal (bytes)', nb_app, non_volatile_memory, volatile_memory],
			['decimal (bytes)', int(nb_app, 16), int(non_volatile_memory, 16), int(volatile_memory, 16)]
		])


	@property
	@allure.step
	def audit_ecasd_recognition_data(self):
		return '80CABF30035C0166'
	@property
	@allure.step
	def audit_ecasd_certificate_store(self):
		return '80CABF30045C027F21'

	@property
	@allure.step
	def get_eid_data(self):
		# get_eid =['00A4040010A0000005591010FFFFFFFF8900000200', '00CA005A00' ]
		return '00A4040010A0000005591010FFFFFFFF890000020000CA005A00'

	# def master_delete(self, aid_isdp):
	# 	LogInfo('Master Delete')
	# 	tag_B6 = collections.OrderedDict()
	# 	tag_B6['isdp_sin'] = ['42', '000000000004']
	# 	tag_B6['isdp_sdin'] = ['45', '6364160310000094337F']
	# 	tag_B6['isdp_prov_id'] = ['5F20', '00000000000000000000000000000000']
	# 	tag_B6['token_id'] = ['93', '22']
	# 	token = '00112233445566778899AABBCCDDEEFF'

	# 	return '80E40040' + lv( '4F10' + aid_isdp + 'B6' + berLv( d2h(tag_B6) ) + '9E10' + token ) + '00'

	#update by Aji
	# def master_delete(self, aid_isdp, token_aes_key, value_tag_42 = "000000000004", value_tag_45 = "6364160310000094337F", value_dgi_5f20 = "00000000000000000000000000000000", value_tag_93 = "01"):
	@allure.step
	def master_delete(self, aid_isdp, token_aes_key, value_tag_42 = "000000000004", value_tag_45 = "6364160310000094337F", value_dgi_5f20 = "A000000559", value_tag_93 = "01"):
		pprint.h3("master delete "+str(aid_isdp))
		'''Tag 4F: ISD-P to be deleted'''
		tag4f = "4F" + lv(aid_isdp)

		'''Tag 42: Identification Number ISD-P 
		(SIN: S? Identification Number )
		'''
		tag42 = "42" + lv(value_tag_42)
		'''Tag 45: Image Number ISD-P 
		(SDIN: SD? Image Number)'''
		tag45 = "45" + lv(value_tag_45)
		'''Tag 5F20: Application Provider identifier'''
		if(value_dgi_5f20.upper() == "NONE"):
			dgi5f20 = ""
		else:
			dgi5f20 = "5F20" + lv(value_dgi_5f20)
		# dgi5f20 = "5F20" + lv(value_dgi_5f20)
		'''Tag 93: Token identifier number'''
		tag93 = "93" + lv(value_tag_93)

		tagB6 = "B6" + lv(tag42 + tag45 + dgi5f20 + tag93)

		masterDelP1 = "00"
		masterDelP2 = "40"
		#master delete sw: 6982 because forgot to add "lv" dataForMac
		dataForMac = masterDelP1 + masterDelP2 + lv(tag4f + tagB6)
		print("dataForMac: ", dataForMac)
		print("token_aes_key: ", token_aes_key)
		mac = Cryptox.AES_CMAC_128(dataForMac, token_aes_key)
		print("mac: ", mac)

		'''Tag 9E: Delete Token'''
		tag9e = "9E" + lv(mac)
		print("tag9e: ", tag9e)
		masterDelData = tag4f + tagB6 + tag9e
		print("masterDelData: ", masterDelData)
		masterDelCmd = "80 E4" + masterDelP1 + masterDelP2 + lv(masterDelData) + "00"
		print("masterDelCmd: ", masterDelCmd)
		return masterDelCmd

	def eiucc_capability_audit(self):
		return

	@allure.step
	@allow_short_aid_for_isdp
	def set_fallback_attribute(self, aid_isp):
		return '80E28800153A05124F10' + aid_isp

	def establish_isdr_keyset(self):
		return

	def finalise_isdr_handover(self, keyset=None, keyrange=None):
		return "80E4000005F203" + keyset + keyrange

	def store_sdin(self):
		store_sdin
		return

	@allure.step
	def update_smsr_addressing_parameters(self, ipv4, tcp_port):
		ipv4_base_16 = '{:02X}{:02X}{:02X}{:02X}'.format(*map(int, ipv4.split('.')))
		tcp_port_base_16 = '{:04X}'.format(int(tcp_port))
		return '80E28800113A070EA40C3C0302'+ tcp_port_base_16 + '3E0521' + ipv4_base_16

	@allure.step
	def update_smsr_address_param_sms(self):
		return '80E288000C3A0709A307810505850282F2'

	@allure.step
	def update_jpatch_rc1(self):
		update_jpatch = [
		'80E602000C07C000000080504100000000',
		'80E8000060C48200BF010011DECAFFED010202000107C000000080504102001F0011001F00000001000200010001000A00040005000C00000000000000000004000100060001800700010008000A000000000000000000000A000501000000000500020000',
		'80E8000163830060B4BC60FAE8E769A4254F2414B24260A66EFE037EB2EB5F2FAF495ED517FE7980B0403BCD19F98FDA7AF140B2BF845C270AD937255AF20FFA2CB5139A651B855AFA85F19C2547D7C42E15B5ABB1DA8214A1A591580E91A962FB5C02A9D4B85197'
		]
		return update_jpatch

	@allure.step
	def update_jpatch_rc12(self):
		update_jpatch = [
		'80E602000C07C000000080504100000000',
		'80E8000060C48200BF010011DECAFFED010202000107C000000080504102001F0011001F00000001000200010001000A00040005000C00000000000000000004000100060001800700010008000A000000000000000000000A000501000000000500020000',
		'80E80001638300605EA3DFD343759E878C72650C235C13AE769FC1BE81C3617495E2A0A1EFE289DFC34E6E174DD7FEF17DB2D8A77AF92D50CEA77BA6FBEEE0F3D11AB3CE9CDB86EC675BB866E9E43A99CE51F9B31AD359D092E80469B43E8DA2C719B5F90691EDE0'
		]
		return update_jpatch

	@allure.step
	def update_smsr_anna(self, aid_isdp):
		return '80E288002F 3A07 2C A3 0B81090D91137900349174F1 E3 1D9109 0D91137900349174F10110' + aid_isdp
		# return '80E2880010 3A07 0D A3 0B81090D91137966649174F1'

	@allure.step
	def update_notification_timer(self, timer='112233'):
		return '80E2880008DF6D051803' + timer

	@allure.step
	def update_connectivity_param_dakot_962(self):
		return "80E288003B 3A07 38 A12B 3501034718077075626C6963340B6D326D696E7465726E657403636F6D0D05046E6F6E650D05046E6F6E65 A00060791135604993800"
		# return "80E288000B 3A07 2B A1 21 350103 470A066F72616E6765026672 0D07046F72616E6765 0D07046F72616E6765 A0 06 911011223344 "
		
	@allure.step
	def update_connectivity_parameters_sms(self):
		return '80E288000C 3A07 09 A3 07 81051122334455'

	@allure.step
	def update_connectivity_multiple_parameters_sms_http(self):
		return '80E288002F 3A07 2C A1 21 350103 470A066F72616E6765026672 0D07046F72616E6765 0D07046F72616E6765 A0 06 06911011223344 '

	@allure.step
	def update_connectivity_parameters_by_mno(self):
		return '80E2'

	@allure.step
	def handle_notification_confirmation(self, seq_number='XXXX', modification=False, location_plugin_state=True):
		basic_notif = '4E02' + seq_number
		notif = basic_notif
		if modification:
			if location_plugin_state:
				applet_location_plugin_state = '340181'
				notif += applet_location_plugin_state
			else:
				applet_location_plugin_state = '340180'
				notif += applet_location_plugin_state
		else:
			notif = basic_notif
		# 80E28900073A08044E02
		return '80E28900' + lv('3A08' + lv(notif) ) + '00'

	@allure.step
	def handle_default_notification(self):
		return self.handle_notification_confirmation('0001')

	@allure.step
	def update_http_retry_policy(self):
		return '80E280002D00702B852A841E0103014001020281820500350103390205DC3C0302EB123E05214E2F1AB286000700000000000000'
		# 80E28000 <?> 00 70 <?> \
		#  85 <?> \
		#     84 <%HTTP_CONNECTION_PARAM> %HTTP_CONNECTION_PARAM \
		#     85 <%HTTP_PSK_ID> %HTTP_PSK_ID \
		#     86 <%HTTP_RETRY_POLICY> %HTTP_RETRY_POLICY \
		#     89 <%HTTP_POST_PARAM> %HTTP_POST_PARAM \
		#     (9000)

	@allure.step
	def update_notification_with_imei(self, state):
		if state == 'activate':
			return '80E2880006DF6D03160101'
		else:
			return '80E2880006DF6D03160101'

	@property
	@allure.step
	def activate_frm(self):
		return '80E288000F3A070CA307810505850282F2'+'350101'

	@property
	@allure.step
	def deactivate_frm(self):
		return '80E288000F3A070CA307810505850282F2'+'350100'

	@property
	@allure.step
	def activate_apf(self):
		return '80E288000F3A070CA307810505850282F2'+'360101'

	@property
	@allure.step
	def deactivate_apf(self):
		return '80E288000F3A070CA307810505850282F2'+'360100'

	@allure.step
	def configure_apf_interval_value(self, interval='3BC4'):
		return '80E28800 09 3A07 06 37048102'+interval
	
	@allure.step
	def configure_apf_counter(self, counter='0168'):
		return '80E28800 09 3A07 06 37048202'+counter
	
	@allure.step
	def configure_status_triggering_notification(self, counter='0A'):
		return '80E28800 06 DF6D 03 4001 ' + counter

	''' PUT KEY '''
	# keyset = ['6F19145916A2E906BCB019E02DCFFC52','D9D234EFE9637CCFE2856CCAA07BB704','35BDA4AA497838A3353353097B8F43BF']
	@allure.step
	def put_key_scp(self, aid = 'A0000005591010FFFFFFFF8900001000', kvn='30' , scp='0370', sEnc='6F19145916A2E906BCB019E02DCFFC52', sMac='D9D234EFE9637CCFE2856CCAA07BB704', sKek='35BDA4AA497838A3353353097B8F43BF', sequence_number='000000', kid='00'):
		keyset = ['6F19145916A2E906BCB019E02DCFFC52','D9D234EFE9637CCFE2856CCAA07BB704','35BDA4AA497838A3353353097B8F43BF']
		SD = SecurityDomain(aid,SCPKeyset(kvn='30',scp='0370',sEnc='6F19145916A2E906BCB019E02DCFFC52',sMac='D9D234EFE9637CCFE2856CCAA07BB704',sKek='35BDA4AA497838A3353353097B8F43BF',sequence_number='000000', kid='00'))
		put_key = SD.load_KeySet(aKeyset=SCPKeyset(kvn='30',scp='0370',sEnc='6F19145916A2E906BCB019E02DCFFC52',sMac='D9D234EFE9637CCFE2856CCAA07BB704',sKek='35BDA4AA497838A3353353097B8F43BF',sequence_number='000000', kid='00'),replace=False, kvnToreplace = None, payload = True, nowrap = False)
		return put_key

	@allure.step
	def put_key_gp(self, aid='',  keyset_object_access='GP', kvn='30', keytype= const.hexa.GP_KEY_TYPE['3DES CBC'], keyValue=''):
		return None

	''' SET-FALLBACK'''

	# def setfallback_enabled_profile_only(self):
	# 	return '80E2880006DF6D03140100'

	# def setfallback_enabled_disabled_profile(self):
	# 	return '80E2880006DF6D03140101'

	''' New Hasbi '''
	@allure.step
	def setfallback_enabled_profile_only(self):
		return '80E2880006DF6D03140101'

	@allure.step
	def setfallback_enabled_disabled_profile(self):
		return '80E2880006DF6D03140100'

	''' New Hasbi '''
	@allure.step
	def setfallback_profile_via_store_data(self, aid_isdp):
		return '80E28800153A05124F10' + aid_isdp

	'''  '''


	''' SWITCHBACK PROFILE '''
	@allure.step
	def switchback_timer(self, seconds='00', minutes='00', hours='00'):
		pprint.h3("set switchback timer")
		return '80E2880008 DF6D 05 1503' + hours + minutes + seconds

	@allure.step
	def set_switchback_counter(self, counter='02'):
		pprint.h3("set switchback counter")
		return '80E28800 06 DF6D 03 1701' + counter

	@property
	@allure.step
	def activate_frm_trigger(self):
		return '80E2880006DF6D03'+'390101'
	
	@property
	@allure.step
	def audit_propietary_psm(self):
		return "80CADF6D00"
	
	@property
	@allure.step
	def audit_connectivity_psm(self):
		return "80CA3A0700"

	@property
	@allure.step
	def deactivate_frm_trigger(self):
		return '80E2880006DF6D03'+'390100' 		

	@allure.step
	@allow_short_aid_for_isdp
	def set_test_profile_attribute(self, aid_isp):
		return '80E28800 15 DF6D 12 1910' + aid_isp
		
	@allure.step
	def configure_poll_duration(self, interval="0030"):
		return '80E28800 07 DF6D 04 2002' + interval
	@property
	@allure.step
	def memory_reset(self):
		return '80E28800 04 DF6D 01 07'
	

	'''add by Sabarina'''
	''' Notification Counter Configuration '''
	@allure.step
	def set_notif_counter(self, counter='03'):
		return '80E28800 06 DF6D 03 3801' + counter

	'''Notification Retry Timer Configuration'''
	@allure.step
	def notif_retry_timer(self, seconds='00', minutes='01', hours='00'):
		#default = 10min
		return '80E28800 08 DF6D 05 1801' + hours + minutes + seconds


	''' Minimum Time Registration - MTR '''
	'''set MTR timer'''
	@allure.step
	def mtr_timer(self, seconds='00', minutes='00', hours='00'):
		return '80E28800 08 DF6D 05 2103' + hours + minutes + seconds


if __name__ == '__main__':
	GSMA = Gsma()
	new = GSMA.store_dp_certificate
