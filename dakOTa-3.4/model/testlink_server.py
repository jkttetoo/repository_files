import sys
from Oberthur import *
from testlink import TestlinkAPIClient
from util import file
import unittest, json, collections, datetime
import model

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'm.firmansyah@oberthur.com'

# Constants.testLinkTestCase = []
# Constants.testLinkCountRunAll = 0

class TestlinkServer:

	# def __init__(self, devkey = 'd2ec430274bb865271a34f0e4ceb4594', testlink_url = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'):
	def __init__(self, devkey = 'd2ec430274bb865271a34f0e4ceb4594', testlink_url = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'):
		self.devkey 		= devkey
		self.testlink_url 	= testlink_url
		self.tl_server 		= TestlinkAPIClient(self.testlink_url, self.devkey, verbose=False)

	def retrieve_log(self):
		if Constants.RunAll == True:
			# loggingFile = Oberthur.log.currentLoggingFile
			getLevelLogging = Oberthur.log.getLevel
			getLoggingLogging = Oberthur.log.getLogging
			logPathOberthur = Oberthur.LogPath + "\\"
			# loggingFileList = loggingFile.rsplit("\\")
			# loggingFilePrefix = loggingFile.replace(loggingFileList[len(loggingFileList)-1],"")
			loggingFileSuffix = str(str(Constants.testLinkTestCase[Constants.testLinkCountRunAll-1]).replace(".","\\"))+".otl"
			# logNewFile = loggingFilePrefix + loggingFileSuffix
			logNewFile = logPathOberthur + loggingFileSuffix
			with open(logNewFile, 'r') as f:
				file_string = f.read()
		else:
			with open(Oberthur.log.currentLoggingFile, 'r') as f:
				file_string = f.read()
		
		test_result = file_string.split('body part')

		return (test_result[1])

	def clear_log(self):
		if Constants.RunAll == False:
			with open(Oberthur.log.currentLoggingFile, 'w'): pass

	def create_build(self,testplan_id, build_name):
		build_id = None
		build = next( ( build for build in self.tl_server.getBuildsForTestPlan(testplan_id) if build['name'] == build_name ), None )
		if not build:
			build = self.tl_server.createBuild(testplan_id, build_name, '')[0]
			build_id = build['id']
		else:
			print('Build for "{}" already exist'.format(build_name))
		return (build_id)

	def get_project_id(self, project_name):
		return self.tl_server.getProjectIDByName(project_name)

	def get_testplan_id(self, project_id, testplan_name):
		test_plans 	  = self.tl_server.getProjectTestPlans(project_id)
		test_plan_id  = ''
		for test_plan in test_plans:
			if test_plan['name'] == testplan_name:
				# print (test_plan['id'], test_plan['name'])
				return (test_plan['id'])

	def get_build_id(self, testplan_id, build_name):
		build_id = None
		build = next( ( build for build in self.tl_server.getBuildsForTestPlan(testplan_id) if build['name'] == build_name ), None )
		if not build:
			build_id = self.create_build(testplan_id, build_name)
		else:
			Builds = self.tl_server.getBuildsForTestPlan(testplan_id)
			for build in Builds:
				if build['name'] == build_name:
					build_id = build['id']
		return (build_id)

	def get_testcase_id(self, target_id):
		testcase_id = None

		with open(file.db('testlink_db.txt')) as f:
			test_suite = json.load(f)

		testcase = next( (testcase for testcase in test_suite if testcase['ext_id'] == target_id),None)
		if not testcase:
			LogInfo('External ID not match with test dict.')
		
		return (testcase['ext_id'])

	def report(self, external_id, project_name, testplan_name, build_name, user, execution_status, platform_name, execduration):
		# LogInfo("%%%%%%%%%%%%%%%%% TESTLINK_SERVER.PY / REPORT %%%%%%%%%%%%%%%%%")
		# testcase_id = self.get_testcase_id(external_id)
		# testplan_id = self.get_testplan_id(project_id=self.get_project_id(project_name), testplan_name = testplan_name)
		project_id=self.get_project_id(project_name)
		# LogInfo("project_name: "+str(project_name))
		# LogInfo("project_id: "+str(project_id))
		testplan_id = self.get_testplan_id(project_id, testplan_name = testplan_name)
		build_id 	= self.get_build_id(testplan_id, build_name)

		# LogInfo("external_id: "+str(external_id))
		# LogInfo("testcase_id: "+str(testcase_id))
		# LogInfo("project_name: "+str(project_name))
		# LogInfo("project_id: "+str(project_id))
		# LogInfo("testplan_name: "+str(testplan_name))
		# LogInfo("testplan_id: "+str(testplan_id))
		# LogInfo("build_name: "+str(build_name))
		# LogInfo("build_id: "+str(build_id))
		# LogInfo("user: "+str(user))
		# LogInfo("execution_status: "+str(execution_status))
		# LogInfo("platform_name: "+str(platform_name))
		# LogInfo("execduration: "+str(execduration))
		
		
		timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		
		if execution_status == 'PASSED':
			self.tl_server.reportTCResult(testplanid=testplan_id, buildname=build_name, status='p', notes=self.retrieve_log(), guess=True, testcaseexternalid=external_id, build_id=build_id, user=user, platformname=platform_name,execduration=execduration, timestamp=timestamp)
			# self.clear_log()
		elif execution_status == 'FAILED':
			self.tl_server.reportTCResult(testplanid=testplan_id, buildname=build_name, status='f', notes=self.retrieve_log(), guess=True, testcaseexternalid=external_id, build_id=build_id, user=user, platformname=platform_name, execduration=execduration, timestamp=timestamp)
			# self.clear_log()


                