import allure
from Oberthur import *
from Mobile import *
from Ot.GlobalPlatform import *
import model
from util import pprint

__author__      = 'Muhammad Aditya Ridharrahman'
__maintainer__  = 'Muhammad Aditya Ridharrahman'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'm.ridharrahman@oberthur.com'


'''
High Stress Memory (HSM) V4
HSM v4 is designed to enhance the previous HSM file system endurance up to 16.5 M cycles. In order to achieve the intended performance, 
we only use 1 hot spot distribution area (128 kB) by merging data and counter into one HSM page file with 256 bytes size.

It is possible to audit the HSM counter, applets counters, NVM counter, EF update counter and also Reset Counter. 
In APDU or OTA mode it is done using a Standalone ELF APDU command. In application mode, auditing API is used.

This command to be delivered through external specifications for customers, partners as well as quality and manufacturing departments for industrialization purposes. 
This command is intended to monitoring and audit HSM & Counters on M2M products.
This Command returns the following information:
	Service 1:  HSM files counters.
	Service 2:  Applets counters or the highest applet counter.
	Service 3:  Number of writings in NVM (Global NVM counter).
	Service 4:  EF update counters
	Service 5:  RESET counter
	Service 6: Traceability informations


'''


SERVER 	= model.Smsr()
EUICC 	= model.Euicc()
DEVICE 	= model.Device()

class HighStressMemory:

	# aid = Constants.config['OTA_Poller_Applet']['AID']
	cap_file = None
	version = '4.0'
	
	# @property	
	# def select(self):
	# 	capdu = '00A4040010' + self.aid
	# 	data, sw = SendAPDU(capdu)
	# 	LogInfo('ISO: select applet OTA Poller ({})'.format(sw))
	# 	return data, sw

#	def audit_high_stress_memory_counter(self, option):
#		if option == 'all':
#			p2 = '00'
#		elif option == 'available':
#			p2 = 'FE'
#		elif option == 'highest':
#			p2 = 'FF'
#		else:
#			p2 = option
#
#		capdu    = 'A0DB01' + p2 + '00'
#		LogInfo(capdu)
#		data, sw = SendAPDU(capdu)
#		return data, sw
	'''vvv updated by Aji, added expected data vvv'''
	@allure.step
	def audit_high_stress_memory_counter(self, option, expData):
		pprint.h3("audit hsm memory counter")
		if option == 'all':
			p2 = '00'
		elif option == 'available':
			p2 = 'FE'
		elif option == 'highest':
			p2 = 'FF'
		else:
			p2 = option

		capdu    = 'A0DB01' + p2 + '00'
		LogInfo(capdu)
		data, sw = SendAPDU(capdu, expectedData = expData)
		return data, sw
	'''=== END ==='''
	@allure.step
	def audit_applet_counter(self, option):
		pprint.h3("audit applet counter")
		if option == 'all':
			p2 = '00'
		elif option == 'applet_lock_control':
			p2 = 'FE'
		elif option == 'highest':
			p2 = 'FF'
		else:
			p2 = option

		capdu    = 'A0DB 02' + p2 + '00'
		data, sw = SendAPDU(capdu)
		return data, sw
	@allure.step
	def audit_global_nvm_write_cycles_counter(self):
		capdu    = 'A0DB 03 00 00'
		data, sw = SendAPDU(capdu)
		return data, sw

	@allure.step
	def audit_global_ef_update_counter(self):
		capdu    = 'A0DB 04 00 00'
		data, sw = SendAPDU(capdu)
		return data, sw

	@allure.step
	def audit_card_reset_counter(self):
		capdu    = 'A0DB 05 00 00'
		data, sw = SendAPDU(capdu)
		return data, sw

	@allure.step
	def audit_traceability_informations(self, option):
		if option == 'component_name':
			p2 = '00'
		elif option == 'form_factor':
			p2 = '01'
		elif option == 'rnd_site_name':
			p2 = '02'
		elif option == 'os_information':
			p2 = '03'
		elif option == 'fallback_profile':
			p2 = '04'
		elif option == 'current_profile':
			p2 = '05'
		elif option == 'perso_id':
			p2 = '06'

		capdu    = 'A0DB 06' + p2 + '00'
		data, sw = SendAPDU(capdu)
		return data, sw

if __name__ == '__main__':
	otapoller = HighStressMemory()
