import sys, binascii
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import collections
import configparser
import model
import const


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
GSMA		= model.Gsma()
SERVER 		= model.Smsr()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('DakOTaV31SLE97PC41387XSecureRC7 Reader')
# EUICC.init()

# capdu, rapdu, sw = SERVER.ram(GSMA.update_http_retry_policy(), 80, apdu_format='indefinite', chunk='01')

EUICC.init()
OTA.spi1 = 0x16
OTA.spi2 = 0x00
OTA.kic  = 0x15
OTA.kid  = 0x15
OTA.tar	 = 'B20100'
OTA.kic_key = '16161616161616161616161616161616'
OTA.kid_key = '17171717171717171717171717171717'
OTA.algo_cipher=const.hexa.ciphers['3DES CBC 2 KEYS']
OTA.algo_crypto_verif=const.hexa.ciphers['3DES CBC 2 KEYS']
tags = collections.OrderedDict()

tags['buffer_size'] = '39'
tags['network_access_name'] = '47'
tags['sim_me_transport_layer_level'] ='3C' 
tags['data_destination_addr'] = '3E'

capdu, rapdu, sw = OTA.push_sms()
data, sw = Fetch(sw[2:4])
sw = TerminalResponse('81030140018202828183013A')

OTA.spi1 = 0x16
OTA.spi2 = 0x00
OTA.kic  = 0x15
OTA.kid  = 0x15
OTA.tar	 = 'B20100'
OTA.kic_key = '16161616161616161616161616161616'
OTA.kid_key = '17171717171717171717171717171717'
OTA.algo_cipher=const.hexa.ciphers['3DES CBC 2 KEYS']
OTA.algo_crypto_verif=const.hexa.ciphers['3DES CBC 2 KEYS']
capdu, rapdu, sw = OTA.push_sms(connection_param='4718077075626C6963340B6D326D696E7465726E657403636F6D0D0704534D534C4F470D0704534D53505744')
data, sw = Fetch(sw[2:4])



