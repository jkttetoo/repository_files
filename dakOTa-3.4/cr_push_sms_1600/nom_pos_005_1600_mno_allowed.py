import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model
import const


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE          = model.Device()
OTA             = model.SCP80()
EUICC           = model.Euicc()

displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('DE620 Contact Reader')

'''
'''

EUICC.init()
OTA.spi1 = 0x16
OTA.spi2 = 0x00
OTA.kic  = 0x15
OTA.kid  = 0x15
OTA.tar	 = 'B20100'
OTA.kic_key = '16161616161616161616161616161616'
OTA.kid_key = '17171717171717171717171717171717'
OTA.algo_cipher=const.hexa.ciphers['3DES CBC 2 KEYS']
OTA.algo_crypto_verif=const.hexa.ciphers['3DES CBC 2 KEYS']
capdu, rapdu, sw = OTA.push_sms()
DEVICE.fetch_one(sw)