import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Aditya Ridharrahman'
__maintainer__  = 'Muhammad Aditya Ridharrahman'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.ridharrahman@oberthur.com'

SERVER = model.Smsr()
EUICC = model.Euicc()
DEVICE = model.Device()
OTA = model.SCP80()
HSM = model.HighStressMemory()


displayAPDU(False)
SetLogLevel('info')
print(GetAllReaders())
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('eSIM 3.1.rc11')


EUICC.init()
h1('enable profile with HSM')
SERVER.enable_profile("A0000005591010FFFFFFFF8900001500", scp=81, network_service=False, apdu_format='definite')

h1('Audit audit_high_stress_memory_counter')

LogInfo('\n')
displayAPDU(True)
h1('Audit ALL HSM files')
HSM.audit_high_stress_memory_counter(option='all')

h1('Audit available HSM area')
HSM.audit_high_stress_memory_counter(option='available')

h1('Audit highest HSM file counter')
HSM.audit_high_stress_memory_counter(option='highest')

# capdu, rapdu, sw = SERVER.ram(HSM.audit_high_stress_memory_counter(option='all'), 80, apdu_format='indefinite', chunk='01')
displayAPDU(False)
h1('disable profile with HSM')
SERVER.disable_profile("A0000005591010FFFFFFFF8900001500", scp=80, network_service=False, apdu_format='indefinite')
