import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model



__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER = model.Smsr()
EUICC = model.Euicc()
DEVICE = model.Device()
OTA = model.SCP80()
GSMA = model.Gsma()


displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('DakOTaV31SLE97PC41387XSecureRC8 Reader')

'''
1. APF     		status ON
2. FRM 			status ON
3. Local Delete status DISABLED

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 12 state Pre-created
4. Profile 13 state Pre-created
5. Profile 14 state Pre-created
=====================================

'''

Constants.AID_ISDP 			= 'A0000005591010FFFFFFFF8900001500'
Constants.APDU_FORMAT 		= 'indefinite'
Constants.SAIP_TEST 		= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'



from test_case.sprint_1_contnts import nom_pos_001_audit_hsm_files




suite = unittest.TestSuite()

EUICC.init()

''' Audit HSM Files '''


unittest.TextTestRunner().run(suite)