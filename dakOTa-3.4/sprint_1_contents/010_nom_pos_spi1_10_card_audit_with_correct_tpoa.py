import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import const
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
GSMA 		= model.Gsma()
SERVER		= model.Smsr()

displayAPDU(True)
SetLogLevel('info')


class CRMultiTPOA(unittest.TestCase):
	audit_ef = [
		'00A40000023F00',
		'00A40000027F10',
		'00A40000025F3A',
		'00A40000024F3A'
        ]

	def setUp(self):
		EUICC.init()
		create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=81, apdu_format='definite')
		download_time, download_status, file_size = SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')

		EUICC.init()
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001800'), 80, apdu_format='indefinite', chunk='01')
		if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)

	def test_profile_allowed_for_spi_1600_(self):
		LogInfo('SPI1 10 - Card audit with correct TPOA')
		LogInfo('\n')
		
		LogInfo("Sent RFM - Audit EF SMS LOG")
		EUICC.init()
		OTA.spi1 = 0x16
		OTA.spi2 = 0x21
		OTA.kic  = 0x15
		OTA.kid  = 0x15
		OTA.kic_key = '16161616161616161616161616161616'
		OTA.kid_key = '17171717171717171717171717171717'
		OTA.tar  = 'B00001'
		OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
		OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
		capdu, por, sw_tpoa = OTA.remote_management(audit_ef,apdu_format='indefinite')

		LogInfo("Sent RAM - Card audit to check installed applet")
		EUICC.init()
		OTA.spi1 = 0x12
		OTA.spi2 = 0x21
		OTA.kic  = 0x15
		OTA.kid  = 0x15
		OTA.kic_key = '16161616161616161616161616161616'
		OTA.kid_key = '17171717171717171717171717171717'
		OTA.tar  = 'B20100'
		OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
		OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
		capdu, por, sw_tpoa = OTA.remote_management('80F24000024F0000',apdu_format='indefinite')

		LogInfo("POR : {}".format(sw_tpoa))

	def tearDown(self):
		LogInfo("Profile deletion")
		EUICC.init()
		OTA.spi1 = 0x16
		OTA.spi2 = 0x39
		OTA.kic  = 0x12
		OTA.kid  = 0x12
		EUICC.init()
		SERVER.disable_profile("A0000005591010FFFFFFFF8900001800", scp=80, network_service=False, apdu_format='indefinite')
		command_script, por, sw = SERVER.delete_profile("A0000005591010FFFFFFFF8900001800", scp=80, apdu_format='compact')
		SendAPDU('80F2 00 0C 00')
def suite():
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(CRMultiTPOA, 'test'))
	return suite

if __name__ == '__main__':
	unittest.main()