import sys, binascii
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Mobile import *
from util import *
import unittest
import collections
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
GSMA		= model.Gsma()
SERVER 		= model.Smsr()

displayAPDU(True)
SetLogLevel('info')


class CRPushSMS(unittest.TestCase):

	def setUp(self):
		pprint.h1('Send PUSH SMS 1639_push_after_1600_push_with_complete_conn_param')
		create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
		download_time, download_status, file_size = SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='compact', chunk='01')
		if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
		LogInfo('='*88)

	def test_profile_allowed_for_spi_1600_(self):
		LogInfo('Send PUSH SMS 1639_push_after_1600_push_with_complete_conn_param')
		LogInfo('\n')
		
		EUICC.init()
		OTA.spi1 = 0x16
		OTA.spi2 = 0x00
		OTA.kic  = 0x12
		OTA.kid  = 0x12
		tags = collections.OrderedDict()

		tags['buffer_size'] = '39'
		tags['network_access_name'] = '47'
		tags['sim_me_transport_layer_level'] ='3C' 
		tags['data_destination_addr'] = '3E'

		capdu, rapdu, sw = OTA.push_sms(connection_param='4718077075626C6963340B6D326D696E7465726E657403636F6D0D0704534D534C4F470D0704534D53505744')
		# capdu, rapdu, sw = OTA.push_sms()
		data, sw = Fetch(sw[2:4])
		apn_query = binascii.unhexlify('50555348534D53206672').decode("utf-8")
		login_query = binascii.unhexlify('534D534C4F47').decode("utf-8")
		passwd_query = binascii.unhexlify('534D53505744').decode("utf-8")
		LogInfo('\n')
		LogInfo('#'*78)
		LogInfo('first open channel will use following data from push sms:')
		LogInfo('-----------------------------')
		LogInfo(data)
		LogInfo("APN     	: {}".format(apn_query))
		LogInfo("Login 		: {}".format(login_query))
		LogInfo("Password	: {}".format(passwd_query))
		# LogInfo("DNS flag               : 8100")
		# LogInfo("DNS query              : {} {} {}".format(first_query_char, second_query_char, third_query_char)) 

		LogInfo('#'*78)
		sw = TerminalResponse('81030140018202828183013A')
		data, sw = Fetch(sw[2:4])
		TerminalResponse('810301270002028281030100240101')
		data, sw = Envelope('D70C020282812401' + '01' + '022503' +  '002000')
		data, sw = Fetch(sw[2:4])
		sw = TerminalResponse('81030140018202828183013A')
		data, sw = Fetch(sw[2:4])

		values = hexastring.parse_tlvs(data, tags, start='2028182') # consider data only after tlv device_ids
		transport_protocol = 'tcp' if values['sim_me_transport_layer_level'][0:2] == '02' else 'udp' # udp is 01
		LogInfo(transport_protocol)
		if transport_protocol=='udp':
			LogInfo( 'Open Channel DNS request' )
			sw = TerminalResponse('81030140008202818203010038028100')
			data, sw = Fetch(sw[2:4]) # send data
			sw= TerminalResponse('810301130002028281030100')
			LogInfo('fetch send data dns query: {}'.format(data))
		   
			Fetch(sw[2:4]) # timer
			TerminalResponse('810301270002028281030100240101')
			data, sw = Envelope('D60E1901098202218138028100370148')
			if sw == '910F' : 
				data = Fetch(sw[2:4]) # setup event list
				data, sw = TerminalResponse(data)
			if sw == '910B' : 
				data = Fetch(sw[2:4]) # polling off
				data, sw = TerminalResponse(data)
			Fetch(sw[2:4]) # receive data
			dns_response = '81030142008202218103010036 33 5378 8100 0001000100000000 06 6F74616C7465 06 74656C63656C 03 636F6D 0000010001 C00C000100010000825800 040AC96C68 370100'
			sw = TerminalResponse(dns_response)

			first_query_char  = binascii.unhexlify('6F74616C7465').decode("utf-8")
			second_query_char = binascii.unhexlify('74656C63656C').decode("utf-8")
			third_query_char  = binascii.unhexlify('636F6D').decode("utf-8")

			LogInfo("\n")
			LogInfo('#'*78)
			LogInfo('dns server response:')
			LogInfo('-----------------------------')
			LogInfo(dns_response[30:])
			LogInfo("DNS transaction id     : 5378")
			LogInfo("DNS flag               : 8100")
			LogInfo("DNS query              : {} {} {}".format(first_query_char, second_query_char, third_query_char)) 
			LogInfo('#'*78)

			LogInfo("Close Channel")
			data, sw = Fetch(sw[2:4]) # close channel
			sw = TerminalResponse('81030141008202818203010038028100')
			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030140018202828183013A')
			data, sw = Fetch(sw[2:4])
			TerminalResponse('810301270002028281030100240101')
			data, sw = Envelope('D70C020282812401' + '01' + '022503' +  '002000')
			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030140018202828183013A')

			OTA.spi1 = 0x16
			OTA.spi2 = 0x39
			OTA.kic  = 0x12
			OTA.kid  = 0x12
			capdu, rapdu, sw = OTA.push_sms()
			data, sw = Fetch(sw[2:4])

	def tearDown(self):
		EUICC.init()
		SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
		command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')
		SendAPDU('80F2 00 0C 00')

def suite():
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(CRPushSMS, 'test'))
	return suite

if __name__ == '__main__':
	unittest.main()