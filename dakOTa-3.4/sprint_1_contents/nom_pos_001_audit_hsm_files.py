import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Aditya Ridharrahman'
__maintainer__  = 'Muhammad Aditya Ridharrahman'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.ridharrahman@oberthur.com'

SERVER = model.Smsr()
EUICC = model.Euicc()
HSM = model.HighStressMemory()


displayAPDU(True)
SetLogLevel('info')


class AuditHSMFiles(unittest.TestCase):

	def setUp(self):
		pprint.h1('Create ISDP SCP80 definite mode')
		create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
		download_time, download_status, file_size = SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='compact', chunk='01')
		if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
		LogInfo('='*88)

	def test_create_isdp(self):
	
		EUICC.init()
		LogInfo('enable profile with HSM')
		SERVER.enable_profile(Constants.AID_ISDP, scp=81, network_service=False, apdu_format='definite')

		LogInfo('Audit audit_high_stress_memory_counter')
		LogInfo('\n')

		LogInfo('Audit ALL HSM files')
		HSM.audit_high_stress_memory_counter(option='all')

		LogInfo('Audit available HSM area')
		HSM.audit_high_stress_memory_counter(option='available')

		LogInfo('Audit highest HSM file counter')
		HSM.audit_high_stress_memory_counter(option='highest')

		LogInfo('disable profile with HSM')
		SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')

	def tearDown(self):
		EUICC.init()
		SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
		command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')
		SendAPDU('80F2 00 0C 00')

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(AuditHSMFiles, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()