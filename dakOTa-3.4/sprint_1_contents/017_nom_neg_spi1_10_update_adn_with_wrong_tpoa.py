import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
GSMA 		= model.Gsma()
SERVER		= model.Smsr()

displayAPDU(True)
SetLogLevel('debug')


class CRMultiTPOA(unittest.TestCase):
	audit_ef = [
			'00A40000023F00',
			'00A40000027F10',
			'00A40000025F3A',
			'00A40000024F3A'
			# '00DC0704261111111111111111111111111111111111111111111111111111111111111111111111999999'
			]
	commands_install_applet = [
			'80E6020015100000000000F000000000000001234F1000000000',
			'80E80000FFC482018701001ADECAFFED0102040001100000000000F000000000000001234F1002001F001A001F0014001E003A00180089001100150000000300020001000402010004001E02020107A0000000620101060210A0000000090003FFFFFFFF891071000203001401100000000000A000000000000001234F1000010600184380030300020702000000650068810101088100008002000700890005318F000D3D8C00032E1B181D0441181D258B00077A0911188C0008181007900B3D031041383D041050383D051050383D06104C383D071045383D081054383D100610313887018D000C2C18197B0002037B000292030303038B000A880019048B000B7A00',
			'80E880018C207A06228D00062D8D00092E1D046B141A0310F6AD0103AD01928B00041A8B00053B7A080011000200010001030004424F4C540000000005003A000E0200000202000000050000000600001703810A1303810A1606810A000380030206800300068108000381090B038109090681090001000000090015000447171D03000D04040C072E0604080709041204',
			'80E60C0051100000000000F000000000000001234F10100000000000A000000000000001234F101000000000000000000000000001234F1001001AEF16C8020001C7020001CA0C0100FF001001010000020100C90000',
			'80F24000124F1000000000000000000000000001234F1000C0000000'
			]

	def setUp(self):
		pprint.h1('SPI1 10 - Update ADN with wrong TPOA (Negative)')
		EUICC.init()
		create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=81, apdu_format='definite')
		download_time, download_status, file_size = SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')

		EUICC.init()
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001800'), 80, apdu_format='indefinite', chunk='01')
		if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
		LogInfo('='*88)

	def test_profile_allowed_for_spi_1600_(self):
		LogInfo("Sent RFM - Audit EF SMS LOG")
		EUICC.init()
		OTA.spi1 = 0x16
		OTA.spi2 = 0x21
		OTA.kic  = 0x15
		OTA.kid  = 0x15
		OTA.kic_key = '16161616161616161616161616161616'
		OTA.kid_key = '17171717171717171717171717171717'
		OTA.tar  = 'B00001'
		OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
		OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
		capdu, por, sw_tpoa = OTA.remote_management(audit_ef,apdu_format='indefinite')

		LogInfo("Sent RFM - Update EF ADN")
		EUICC.init()
		OTA.spi1 = 0x10
		OTA.spi2 = 0x21
		OTA.kic  = 0x15
		OTA.kid  = 0x15
		OTA.kic_key = '16161616161616161616161616161616'
		OTA.kid_key = '17171717171717171717171717171717'
		OTA.tar  = 'B00001'
		OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
		OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
		capdu, por, sw_tpoa = OTA.remote_management(update_ef,apdu_format='indefinite', tpoa='06812143FF')

		LogInfo("Sent RFM - Audit EF ADN")
		EUICC.init()
		OTA.spi1 = 0x16
		OTA.spi2 = 0x21
		OTA.kic  = 0x15
		OTA.kid  = 0x15
		OTA.kic_key = '16161616161616161616161616161616'
		OTA.kid_key = '17171717171717171717171717171717'
		OTA.tar  = 'B00001'
		OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
		OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
		capdu, por, sw_tpoa = OTA.remote_management(audit_ef,apdu_format='indefinite')

	def tearDown(self):
		EUICC.init()
		LogInfo("Profile deletion")
		OTA.spi1 = 0x16
		OTA.spi2 = 0x39
		OTA.kic  = 0x12
		OTA.kid  = 0x12
		EUICC.init()
		SERVER.disable_profile("A0000005591010FFFFFFFF8900001800", scp=80, network_service=False, apdu_format='indefinite')
		command_script, por, sw = SERVER.delete_profile("A0000005591010FFFFFFFF8900001800", scp=80, apdu_format='compact')
		SendAPDU('80F2 00 0C 00')

def suite():
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(CRMultiTPOA, 'test'))
	return suite

if __name__ == '__main__':
	unittest.main()

