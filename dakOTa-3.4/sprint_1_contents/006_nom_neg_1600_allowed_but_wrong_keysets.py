import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Mobile import *
from util import *
import unittest
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE          = model.Device()
OTA                     = model.SCP80()
EUICC           = model.Euicc()

displayAPDU(True)
SetLogLevel('debug')


class CRPushSMS(unittest.TestCase):

	def setUp(self):
		pprint.h1('Send PUSH SMS with SPI 1600 with wrong keysets (Negative)')
		create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
		download_time, download_status, file_size = SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='compact', chunk='01')
		if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
		LogInfo('='*88)

	def test_profile_allowed_for_spi_1600_(self):
		LogInfo('Send PUSH SMS with SPI 1600 with wrong keysets (Negative)')
		LogInfo('\n')		
		
		EUICC.init()
		OTA.spi1 = 0x16
		OTA.spi2 = 0x00
		OTA.kic  = 0x12
		OTA.kid  = 0x12
		OTA.kic_key = '16161616161616161616161616161616'
		OTA.kid_key = '17171717171717171717171717171717'
		capdu, rapdu, sw = OTA.push_sms()
		DEVICE.fetch_one(sw)

	def tearDown(self):
		EUICC.init()
		SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
		command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')
		SendAPDU('80F2 00 0C 00')

def suite():
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(CRPushSMS, 'test'))
	return suite

if __name__ == '__main__':
	unittest.main()