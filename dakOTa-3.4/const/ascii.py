__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'


CONTENT_TYPE            = 'Content-Type: application/vnd.globalplatform.card-content-mgt-response;version=1.0'
EUM_S_ID                = '10.11.12'
FAILED                  = 'Failed'
HOST                    = 'Host: localhost'
HTTP_CODE_200           = 'HTTP/1.1 200'
HTTP_CODE_204           = 'HTTP/1.1 204'
IMSI1                   = '234101943787656'
IMSI2                   = '234101943787657'
IMSI3                   = '234101943787658'
MNO1_S_ID               = '1.2.3'
MNO2_S_ID               = '11.22.33'
MSISDN1                 = '447112233445'
MSISDN2                 = '447112233446'
MSISDN3                 = '447112233447'
POST_URI                = 'POST /gsma/adminagent HTTP/1.1'
POST_URI_NOTIF          = 'POST /gsma/adminagent?msg=#NOTIF_PROFILE_CHANGE HTTP/1.1'
POST_URI_NOTIF2         = 'POST /gsma/adminagent?msg=#NOTIF_PROFILE_CHANGE2 HTTP/1.1'
PROFILE1_TYPE           = 'GENERIC PROFILE1 3G'
PROFILE2_TYPE           = 'GENERIC PROFILE2 3G'
PSK_ID                  = '8001028110' + EID + '4F10' + ISD_R_AID + '8201' + SCP81_KEY_ID + '8301' + SCP81_KVN
RC_ALREADY_REGISTER     = '1.3'
RC_ALREADY_USED         = '3.3'
RC_COND_PARAM           = '2.3'
RC_COND_USED            = '3'
RC_EXECUTION_ERROR      = '4.2'
RC_EXPIRED              = '6.3'
RC_ID_UNKNOWN           = '1.1'
RC_INACCESSIBLE         = '5.1'
RC_INVALID_DATA         = '1.5'
RC_INVALID_DEST         = '3.4'
RC_INVALID_SIGN         = '1.4'
RC_MEMORY               = '4.8'
RC_NOT_ALLOWED          = '1.2'
RC_REFUSED              = '3.8'
RC_UNKNOWN              = '3.9'
SC_ECASD                = '8.5.2'
SC_EID                  = '8.1.1'
SC_EIS                  = '8.6'
SC_EUICC                = '8.1'
SC_FUN_PROV             = '1.2'
SC_FUNCTION             = '1.6'
SC_SD_AID               = '8.3.1'
SC_ISDP                 = '8.3'
SC_ISDR                 = '8.4'
SC_POL1                 = '8.2.2'
SC_POL2                 = '8.2.3'
SC_PROFILE_ICCID        = '8.2.1'
SC_PROFILE              = '8.2'
SC_SM_SR                = '8.7'
SC_SR_CERTIF            = '8.5.3'
SC_SUB_ADDR             = '8.2.6'
SM_DP_S_ID              = '4.5.6'
SM_SR_S_ID              = '7.8.9'
SUCCESS                 = 'Executed-Success'
TRANSFER_ENCODING       = 'Transfer-Encoding: chunked'
UNKNOWN_SM_SR_ID        = '8888.9999.1111'
WARNING                 = 'Executed-WithWarning'
X_ADMIN_FROM_ISD_R      = 'X-Admin-From: //se-id/eid/#EID;//aa-id/aid/A000000559/1010FFFFFFFF8900000100'
X_ADMIN_FROM_MNO        = 'X-Admin-From: //se-id/eid/' + EID + '//aa-id/aid/'  + MNO_SD_AID
X_ADMIN_NEXT_URI        = 'X-Admin-Next-URI: /gsma/adminagent'
X_ADMIN_PROTOCOL        = 'X-Admin-Protocol: globalplatform-remote-admin/1.0'
X_ADMIN_STATUS_OK       = 'X-Admin-Script-Status: ok'