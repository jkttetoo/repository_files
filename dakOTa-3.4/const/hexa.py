__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'


EID = '6364160310000094337F000000000004'
MNO_SD_AID = 'A000000151000000'
ADMIN_HOST          = '6C6F63616C686F7374'
ADMIN_URI           = '2F67736D612F61646D696E6167656E74'
AGENT_ID            = '2F2F73652D69642F6569642F' + EID + '3B2F2F61612D69642F6169642F413030303030303535392F31303130464646464646464638393030303030313030'
BAD_SCP03_KVN       = '35'
BAD_SPI             = '1229'
BAD_TOKEN           = '010203'
BEARER_DESCRIPTION  = '02000003000002'
BUFFER_SIZE         = '0578'
CASD_AID            = 'A00000015153504341534400'
CAT_TP_PORT         = '0400'
DEST_ADDR           = '05850282F2'
DIALING_NUMBER      = '3386994211F0'
ECASD_AID           = 'A0000005591010FFFFFFFF8900000200'
ECASD_TAR           = '000002'
FIRST_SCRIPT        = '01'
HOST_ID             = '47534D415F484F53545F4944'
ICCID1              = '89019999000044777878'
ICCID2              = '89019999000044777879'
IP_VALUE            = '7F000001'
ISD_P_AID1          = 'A0000005591010FFFFFFFF8900001000'
ISD_P_AID2          = 'A0000005591010FFFFFFFF8900001100'
ISD_P_AID3          = 'A0000005591010FFFFFFFF8900001200'
ISD_P_ATTRIBUTE     = '53'
ISD_P_MOD_AID       = 'A0000005591010FFFFFFFF8900000E00'
ISD_P_PIX_PREFIX    = '1010FFFFFFFF89'
ISD_P_PKG_AID       = 'A0000005591010FFFFFFFF8900000D00'
ISD_P_PROV_ID       = '47534D41'
ISD_P_RID           = 'A000000559'
ISD_P_SDIN          = '495344505344494E'
ISD_P_SIN           = '49534450'
ISD_P_TAR1          = '000010'
ISD_R_AID           = 'A0000005591010FFFFFFFF8900000100'
ISD_R_TAR           = '000001'
KEY                 = '11223344556677889910111213141516'
KEY_USAGE           = '0080'
LAST_SCRIPT         = '03'
LOGIN               = '6C6F67696E'
MEMORY_QUOTA        = '00002000'
MNO_AGENT_ID        = '2F2F73652D69642F6569642F'  + EID + '3B2F2F61612D69642F6169642F' + MNO_SD_AID
NAN_VALUE           = '47534D416555494343'
PWD                 = '70617373776F7264'
RESERVED_ISD_P_AID  = 'A0000005591010FFFFFFFF8900000F00'
SC3_DR              = '0B'
SC3_DR_HOST         = '0F'
SC3_NO_DR           = '09'
SCP03_KVN           = '30'
SCP80_NEW_KVN       = '0E'
SPI_VALUE           = '1639'
SPI_NOTIF           = '0200'
SUB_SCRIPT          = '02'
TCP_PORT            = '1F41'
TOKEN_ID            = '01'
TON_NPI             = '91'
UDP_PORT            = '1F40'
VIRTUAL_EID         = '89001012012341234012345678901224'
VIRTUAL_EID2        = '89001567010203040506070809101152'
VIRTUAL_SDIN        = '000000000102030405060708'
VIRTUAL_SIN         = '01020304'

ciphers = {
   'NONE'               : '00',
   'DES CBC'            : '01',
   'AES'                : '02',
   'XOR'                : '03',
   '3DES CBC 2 KEYS'    : '05',
   '3DES CBC 3 KEYS'    : '09',
   'DES EBC'            : '0D'
}

CRYPTO_VERIF = {
    'NONE'              : '00',
    'RC'                : '01',
    'CC'                : '02',
    'DS'                : '03'
}

EUICC_NOTIFICATION_TYPES =  {
    '01'                : 'eUICC declaration, first network attachment',
    '02'                : 'Profile change succeeded',
    '03'                : 'Profile change failed and Roll-back',
    '04'                : 'Void',
    '05'                : 'Profile change after Fall-back',
    '06'                : 'Profile change after Local Disable Emergency profile',
    '07'                : 'Profile change after Local Disable Test profile',
    '08'                : 'RFU',
    'FC'                : 'Location change',
    'FD'                : 'START of the emergency call',
    'FE'                : 'END of the emergency call'

}

GP_KEY_TYPE = {
  'DES'                 : '80' ,
  '3DES CBC'            : '82' ,
  'DES ECB'             : '83' ,
  'DES CBC'             : '84' ,
  'PSK-TLS'             : '85' ,
  'AES'                 : '88' ,
  'HMAC_SHA1'           : '90' ,
  'HMAC_SHA1_160'       : '91' ,
  'RSA_Public_Key_Public_Exponent_Component'        : 'A0' ,
  'RSA_Public_Key_modulus_N_Component_Clear_Text'   : 'A1' ,
  'RSA_Public_Key_modulus_N_Component'              : 'A2' ,
  'RSA_Private_Key_Private_Exponent_Component'      : 'A3' ,
  'RSA_Private_Key_Chinese_Remainder_P_Component'   : 'A4' ,
  'RSA_Private_Key_Chinese_Remainder_Q_Component'   : 'A5' ,
  'RSA_Private_Key_Chinese_Remainder_PQ_Component'  : 'A6' ,
  'RSA_Private_Key_Chinese_Remainder_DP1_Component' : 'A7' ,
  'RSA_Private_Key_Chinese_Remainder_DQ1_Component' : 'A8' 
}
