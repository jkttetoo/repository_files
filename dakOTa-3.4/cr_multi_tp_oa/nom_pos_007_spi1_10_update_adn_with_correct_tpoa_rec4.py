import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model, const


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE          = model.Device()
OTA                     = model.SCP80()
EUICC           = model.Euicc()
GSMA            = model.Gsma()
SERVER          = model.Smsr()

displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('eSIM 3.1.rc9')
# SetCurrentReader('DE620 Contact Reader')


audit_tpoa = [
        '00A40000023F00',
        '00A40000027F10',
        '00A40000025F14',
        '00B201040C',
        '00B202040C',
        '00B203040C',
        '00B204040C',
        '00B205040C'
        ]

update_adn = [
        '00A40000023F00',
        '00A40000027F10',
        '00A40000025F3A',
        '00A40000024F3A',
        '00DC0704261111111111111111111111111111111111111111111111111111111111111111111111999999'
        ]

audit_adn = [
        '00A40000023F00',
        '00A40000027F10',
        '00A40000025F3A',
        '00A40000024F3A',
        '00B2070426'
        ]


# EUICC.init()
# create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=81, apdu_format='definite')
# download_time, download_status, file_size = SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')

# EUICC.init()
# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001800'), 80, apdu_format='indefinite', chunk='01')
# if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)

SetLogLevel('debug')

LogInfo("Sent RFM - Audit EF SMS LOG")
EUICC.init()
OTA.spi1 = 0x16
OTA.spi2 = 0x21
OTA.kic  = 0x15
OTA.kid  = 0x15
OTA.kic_key = '16161616161616161616161616161616'
OTA.kid_key = '17171717171717171717171717171717'
OTA.tar  = 'B00001'
OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
capdu, por, sw = OTA.remote_management(audit_tpoa,apdu_format='indefinite')

LogInfo("Sent RFM - Update EF ADN")
EUICC.init()
OTA.spi1 = 0x12 #<----- Change to 10
OTA.spi2 = 0x21
OTA.kic  = 0x15
OTA.kid  = 0x15
OTA.kic_key = '16161616161616161616161616161616'
OTA.kid_key = '17171717171717171717171717171717'
OTA.tar  = 'B00001'
OTA.tpoa = '0681214365'
OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
capdu, por_tpoa, sw = OTA.remote_management(update_adn,apdu_format='indefinite')

LogInfo("Sent RFM - Audit EF ADN")
EUICC.init()
OTA.spi1 = 0x16
OTA.spi2 = 0x21
OTA.kic  = 0x15
OTA.kid  = 0x15
OTA.kic_key = '16161616161616161616161616161616'
OTA.kid_key = '17171717171717171717171717171717'
OTA.tar  = 'B00001'
OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
capdu, por, sw = OTA.remote_management(audit_adn,apdu_format='indefinite')


# LogInfo("Profile deletion")
# OTA.spi1 = 0x16
# OTA.spi2 = 0x39
# OTA.kic  = 0x12
# OTA.kid  = 0x12
# EUICC.init()
# SERVER.disable_profile("A0000005591010FFFFFFFF8900001800", scp=80, network_service=False, apdu_format='indefinite')
# command_script, por, sw = SERVER.delete_profile("A0000005591010FFFFFFFF8900001800", scp=80, apdu_format='compact')
# SendAPDU('80F2 00 0C 00')

LogInfo("POR : {}".format(por_tpoa))
