import sys
from Oberthur import *
sys.path.insert(0, "lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Aditya Ridharrahman'
__maintainer__  = 'Muhammad Aditya Ridharrahman'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.ridharrahman@oberthur.com'

SERVER = model.Smsr()
EUICC = model.Euicc()
DEVICE = model.Device()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
SetCurrentReader('OMNIKEY CardMan 3x21 0')
PowerOn()
sw = DEVICE.terminal_profile()
LogInfo("Audit Failed !")
DEVICE.fetch_all(sw)
# EUICC.init_notif(catch_first_notif=True, send_server_confirmation=False)

EUICC.mno.add_whitelist(scp=80, apdu_format='compact', tpoa="0B 91 28 11 92 93 00 F9")

