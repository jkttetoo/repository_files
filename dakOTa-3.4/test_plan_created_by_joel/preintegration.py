# from collections import OrderedDict

# test_suite_functional = OrderedDict()

# test_suite_functional['GSMA 3.1'] = OrderedDict()
# test_suite_functional['GSMA 2'] = OrderedDict()



ts_functional = []

ts_gsma31 = [

	'download_saipv2_via_https',
	'rollback_mechanism'

]


ts_gsma2_positive = [

	'first_notification_with_one_retry',
	'enable_a_non_fallback_profile_via_https',
	'enable_the_fallback_profile_via_https',

]

ts_gsma2_negative = [

	'enable_the_enabled_profile',

]