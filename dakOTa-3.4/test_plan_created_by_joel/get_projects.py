#! /usr/bin/python
#
# Testlink API Sample Python 3.1.2 getProjects() - Client implementation
# 
import xmlrpc.client
import pprint

class TestlinkAPIClient:        
	# substitute your server URL Here
	SERVER_URL  = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'

	def __init__(self, devKey):
		self.server = xmlrpc.client.ServerProxy(self.SERVER_URL)
		self.devKey = devKey

	def getInfo(self):
		return self.server.tl.about()

	def getProjects(self):
		return self.server.tl.getProjects(dict(devKey=self.devKey))

	def getTestPlans(self):
		return self.server.tl.getProjectTestPlans(dict(devKey=self.devKey, testProjectId='15729'))

# substitute your Dev Key Here
client = TestlinkAPIClient("d2ec430274bb865271a34f0e4ceb4594")

# get info about the server
# print(client.getInfo())

# get info all project
# pprint.pprint(client.getProjects())
pprint.pprint(client.getProjects())

# get info all test plans
# print(client.getTestPlans())