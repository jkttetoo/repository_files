from testlink import TestlinkAPIClient
from Oberthur import *
import os
import datetime
import unittest
from test_case import *
import sys
from subprocess import check_output


SERVER_URL  = 'http://i2j6serv11.jakarta.oberthurcs.com:8081/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
DEVKEY      = '952fb029f0535bf82f477ee09b75baf7'

USER_NAME                       = 'j.viellepeau@oberthur.com'
PROJECT                         = 'DakOTa 3.1'
PROJECT_PREFIX                  = 'AUTO01'
TEST_PLAN_RAM                   = 'eUICC pre-integration'




TEST_SUITE_NOMINAL_POSITIVE     = 'Nominal positive'
TEST_SUITE_NOMINAL_NEGATIVE     = 'Nominal negative'


TEST_SUITE_SCP80     			= 'SMS'
TEST_SUITE_SCP81     			= 'HTTPS'


TEST_CASE_EUICC_AUDIT           = 'eUICC audit'
TEST_CASE_DELETE_ENABLED        = 'Delete enabled profile'

PLATFORM_ALM_COMPOSER           = 'Oberthur Testing tool 1.0, ALM 2.2.3 MR5, Java Composer 6.0.8.2'

MANUAL = 1
AUTOMATED = 2

this_file_dirname = os.path.dirname(__file__)
NEWATTACHMENT_PY  = os.path.join(this_file_dirname, 'hello.py')
NEWATTACHMENT_PNG = os.path.join(this_file_dirname, 'example.png')

LJUST = 20

################################################################

BUILD = 'DakOTa 3.1.rc2 / IO222' # increment if new execution
 
################################################################




LogInfo('\n\nDemo test e2e auto and save results in testlink'.upper())
LogInfo('-----------------------------------------------\n')


LogInfo('1. Creating TestLink components to save test execution results in database...\n')
server = TestlinkAPIClient(SERVER_URL, DEVKEY, verbose=False)

server.deleteTestProject(PROJECT_PREFIX)


project_id = server.createTestProject(PROJECT, PROJECT_PREFIX, notes='', active=1, public=1, options={'requirementsEnabled' : 0, 'testPriorityEnabled' : 1, 'automationEnabled' : 1, 'inventoryEnabled' : 0})[0]['id']
LogInfo( 'Project:'.ljust(LJUST) + '{}'.format(project_id).ljust(LJUST) + PROJECT)

test_plan = server.createTestPlan(TEST_PLAN_RAM, PROJECT, notes='Testing Remote Application Management Over-The-Air thru SCP80 and SCP81', active=1, public=1)[0]
LogInfo( 'Test plan:'.ljust(LJUST) + '{}'.format(test_plan['id']).ljust(LJUST) + TEST_PLAN_RAM)

platform = server.createPlatform(PROJECT, PLATFORM_ALM_COMPOSER, notes='Offline testing environment dedicated to ESF Jakarta Validation and Integration team for MNO projects')
assert server.addPlatformToTestPlan(test_plan['id'], PLATFORM_ALM_COMPOSER, notes='')['msg'] == 'link done'
LogInfo( 'Platform:'.ljust(LJUST) + '{}'.format(platform['id']).ljust(LJUST) + PLATFORM_ALM_COMPOSER)

build = server.createBuild(test_plan['id'], BUILD, '')[0]
LogInfo( 'Build:'.ljust(LJUST) + '{}'.format(build['id']).ljust(LJUST) + BUILD)


test_suite_functional = server.createTestSuite(project_id, 'Functional', '', parentid=None)
test_suite_gsma2 = server.createTestSuite(project_id, 'GSMA 2', '', parentid=test_suite_functional[0]['id'])
test_suite_gsma31 = server.createTestSuite(project_id, 'GSMA 3.1', '', parentid=test_suite_functional[0]['id'])
test_suite_gsma31_negative = server.createTestSuite(project_id, 'Negative', '', parentid=test_suite_gsma31[0]['id'])
test_suite_gsma31_positive = server.createTestSuite(project_id, 'Positive', '', parentid=test_suite_gsma31[0]['id'])

test_suite_gsma31_positive_eUICC_audit = server.createTestSuite(project_id, 'eUICC audit', '', parentid=test_suite_gsma31_positive[0]['id'])

test_script_name = 'Audit_ISD-R_via_SMS_to_list_all_ISD-P'
server.initStep("Send the get status command 80F24002094F005C054F9F7053B500 to ISD-R via SCP80", "Response APDU contains the list of ISD-P installed and their current states", AUTOMATED)
test_case_card_audit_https = server.createTestCase(test_script_name.replace('_',' '), test_suite_gsma31_positive_eUICC_audit[0]["id"], project_id, USER_NAME, "Send command Get Status to Issuer Security Domain Root to retrieve the list of Issuer Security Domain Profiles and their current states", preconditions='None')
result = check_output(['python', '../test_case/' + test_script_name +'.py'])

# print(sys.modules)
# module = sys.modules[test_script_name]
# suite = unittest.TestSuite()
# suite.addTest(module.suite())
# unittest.TextTestRunner().run(suite)

# from sprint_3.positive import _000_quick_audit
# _000_quick_audit.SCP = self.scp_value.get()
# suite = unittest.TestSuite()
# suite.addTest(_000_quick_audit.suite())
# unittest.TextTestRunner().run(suite)


# ###
# test_suite_gsma31_positive_create_isdp_and_establish_keyset = server.createTestSuite(project_id, 'Create ISD-P and establish keyset', '', parentid=test_suite_gsma31_positive[0]['id'])

# server.initStep("Send install for install command via SCP81 to create ISD-P with AID=A0000005591010FFFFFFFF8900001000", "Response APDU contains status word 9000", AUTOMATED)
# test_case_create_isdp = server.createTestCase('Create ISD-P via HTTPS', test_suite_gsma31_positive_create_isdp_and_establish_keyset[0]["id"], project_id, USER_NAME, 'Create a new Issuer Security Domain Profile via SCP81', preconditions='None')

# server.initStep("Send install for perso command and store DP certificate via SCP81", "Response APDU contains status word 9000", AUTOMATED)
# test_case_establish_keyset = server.createTestCase('Establish ISD-P keyset via HTTPS', test_suite_gsma31_positive_create_isdp_and_establish_keyset[0]["id"], project_id, USER_NAME, "Establish a SCP03 keyset for an Issuer Security Domain Profile via SCP81", preconditions='Targeted ISD-P is in personalized state')
# ###

# test_suite_gsma31_positive_download_profile = server.createTestSuite(project_id, 'Download SIMalliance profile', '', parentid=test_suite_gsma31_positive[0]['id'])
# server.initStep("Download SIMalliance profile named SAIPv2_http_notification_20160826.der", "eUICC response is XXXXXXX", AUTOMATED)
# test_case_download_saip = server.createTestCase('Download SIMalliance profile via SCP81/SCP03t', test_suite_gsma31_positive_download_profile[0]["id"], project_id, USER_NAME, "", preconditions='Targeted ISD-P is in pre-created state')