import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE = model.Device()
GSMA   = model.Gsma()
EUICC  = model.Euicc()
LPin   = model.LocationPlugin()


displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')
SetCurrentReader('eSIM 3.1.rc11')

h1('Applet triggered by mcc change')

EUICC.init(loci_change=True)
h2('Update applet config : Detect mcc change only + Triggered by location change only')
data, sw = LPin.location_plugin_update_configuration(interface='applet', detection_condition='mcc', triggering_condition='power-on only', timer_interval=30, max_counter=5)

EUICC.init(loci_change=True)
LPin.select_naa()
i=0
for i in range(20):
	data, sw = DEVICE.get_status()
data, sw = Fetch(sw[2:])
sw = TerminalResponse('810301260082028182830100130712489041235648')
data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'],loci_change=True)
assert sw[0:2] != 91, 'notification (sms or http) was expected!'
if last_proactive_cmd == 'open channel':
	EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
elif last_proactive_cmd == 'send sms':
	seq_number = pprint.pprint_sms_first_notif(data)
	data, sw, cmd_type = DEVICE.fetch_all(sw)
	LogInfo('server confirms reception of euicc notification')
	capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))

