import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER = model.Smsr()
EUICC  = model.Euicc()
LPin   = model.LocationPlugin()


displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')
SetCurrentReader('eSIM 3.1.rc11')

h1('Update applet configuration via applet selection')
EUICC.init()
data, sw = LPin.location_plugin_update_configuration(interface='applet', detection_condition='mcc mnc', triggering_condition='detection only', timer_interval=30, max_counter=16)
