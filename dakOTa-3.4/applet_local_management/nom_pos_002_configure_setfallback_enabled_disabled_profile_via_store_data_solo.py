import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER = model.Smsr()
GSMA   = model.Gsma()
EUICC  = model.Euicc()


displayAPDU(True)
SetLogLevel('info')
#print(GetAllReaders())
#SetCurrentReader('Dell Smart Card Reader Keyboard 0')
## SetCurrentReader('eSIM 3.1.rc11')

Options.counters={}

if Constants.displayAPDU != None :
    displayAPDU(Constants.displayAPDU)
            
try:
    if Constants.RunAll == True:
        SERVER                  = model.Smsr() 
        EUICC                   = model.Euicc()
        GSMA                    = model.Gsma()
        DEVICE                     = model.Device()
        TL_SERVER               = model.TestlinkServer()
        
        EXTERNAL_ID             = 'DAKO-4319'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None
    else:
        SERVER                  = model.Smsr()
        EUICC                   = model.Euicc()
        GSMA                    = model.Gsma()
        DEVICE                     = model.Device()
        TL_SERVER               = model.TestlinkServer()
        
        EXTERNAL_ID             = 'DAKO-4319'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None
        
        displayAPDU(True)
        SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')
        
        ''' TESTLINK VARIABLES '''
        Constants.TESTLINK_REPORT           = False
        Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
        Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
        Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
        Constants.TESTLINK_BUILD            = 'build_for_test'
        Constants.TESTLINK_PLATFORM         = 'IO222'
        Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
        Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'
        
        '''
        TEST VARIABLES
        
        To launch the test, prepare the following variables : 
        
        Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
        Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
        Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
        Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 
        
        '''
        Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
        Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
        Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
        Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
        Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
        Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
        Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
        Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
        Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
        Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
        Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
        Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
        Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
        Constants.ITERATIONS_PERFORMANCE    = 1
        Constants.ITERATIONS_STRESS         = 1
        Constants.RESULTS                   = {
                                                'test_name'         : ['Test name'],
                                                'test_result'        : ['Test result']
                                                }
        Constants.case                      = 0
except:
    SERVER                  = model.Smsr()
    EUICC                   = model.Euicc()
    GSMA                    = model.Gsma()
    DEVICE                     = model.Device()
    TL_SERVER               = model.TestlinkServer()
    
    EXTERNAL_ID             = 'DAKO-4319'
    EXECUTION_STATUS        = None
    EXECUTION_DURATION      = None
    
    displayAPDU(True)
    SetLogLevel('info')
    SetCurrentReader('OMNIKEY CardMan 3x21 0')
    
    ''' TESTLINK VARIABLES '''
    Constants.TESTLINK_REPORT           = False
    Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
    Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
    Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
    Constants.TESTLINK_BUILD            = 'build_for_test'
    Constants.TESTLINK_PLATFORM         = 'IO222'
    Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
    Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'
    
    '''
    TEST VARIABLES
    
    To launch the test, prepare the following variables : 
    
    Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
    Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
    Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
    Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 
    
    '''
    Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
    Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
    Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
    Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
    Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
    Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
    Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
    Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
    Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
    Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
    Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
    Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
    Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
    Constants.ITERATIONS_PERFORMANCE    = 1
    Constants.ITERATIONS_STRESS         = 1
    Constants.RESULTS                   = {
                                            'test_name'         : ['Test name'],
                                            'test_result'        : ['Test result']
                                            }
    Constants.case                      = 0

h1('Update applet configuration via store data')
EUICC.init()
capdu, data, sw = SERVER.ram(GSMA.setfallback_enabled_disabled_profile(),scp=80, apdu_format='definite')
assert data[14:] == '9000', 'Expected to be 9000'
capdu, rapdu, sw = SERVER.set_fallback_attribute('A0000005591010FFFFFFFF8900001000', scp=80, apdu_format='indefinite')
assert rapdu[8:12] == '9000', 'Expected to be 9000 - Setfallback can be done on enabled profile only'
capdu, rapdu, sw = SERVER.set_fallback_attribute('A0000005591010FFFFFFFF8900001100', scp=80, apdu_format='indefinite')
