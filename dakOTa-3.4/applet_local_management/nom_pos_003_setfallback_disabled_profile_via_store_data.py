import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER = model.Smsr()
GSMA   = model.Gsma()
EUICC  = model.Euicc()


displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('eSIM 3.1.rc11')

h1('Update applet configuration via applet selection')
EUICC.init()
capdu, data, sw = SERVER.ram(GSMA.setfallback_enabled_disabled_profile(),scp=80, apdu_format='definite')
assert data[14:] == '9000', 'Expected to be 9000'
capdu, rapdu, sw = SERVER.ram([GSMA.setfallback_profile_via_store_data('A0000005591010FFFFFFFF8900001600')], scp=80, apdu_format='indefinite')
assert rapdu[8:12] == '9000', 'Expected to be 9000 - Setfallback can be done on enabled profile only'


