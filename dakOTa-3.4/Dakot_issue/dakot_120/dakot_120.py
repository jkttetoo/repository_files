import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE = model.Device()
GSMA   = model.Gsma()
SERVER = model.Smsr()
EUICC  = model.Euicc()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('eSIM 3.1.rc11')



EUICC.init()
SERVER.ram('80E28800143A05124F10A0000005591010FFFFFFFF8900001000', scp=80, apdu_format='definite')