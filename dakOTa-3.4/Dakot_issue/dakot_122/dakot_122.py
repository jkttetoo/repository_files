import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import const
import configparser
import model, const


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE		= model.Device()
OTA_POLLER	= model.OtaPoller()
EUICC 		= model.Euicc()
GP 			= model.Gp()
AMDB		= model.SCP81()

displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('eSIM 3.1.rc9')
# SetCurrentReader('DE620 Contact Reader')

EUICC.init()
data, sw = OTA_POLLER.set_applet_config(applet_state=True)
assert sw == '9000', 'Expected to be 9000'

EUICC.init()
capdu, data, sw = OTA_POLLER.trigger_open_channel_via_unrecognized_envelope(data='817C837A8478350702020403051F02475F077075626C696334526D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E03636F6D0D05046E6F6E650D05046E6F6E65')
data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
# AMDB.remote_management(GP.audit_applet_list, pull=sw, apdu_format='definite')

print('='*88)
EUICC.init()
data, sw = Envelope('888180CC7E817C837A8478350702020403051F02475F077075626C696334526D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E7465726E65746D316D696E03636F6D0D05046E6F6E650D05046E6F6E65')
data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
# AMDB.remote_management(GP.audit_applet_list, pull=sw, apdu_format='definite')