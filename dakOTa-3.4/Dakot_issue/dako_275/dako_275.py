import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Ot.GlobalPlatform import *
from Mobile import *
from util import *
import unittest
import configparser
import model

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE = model.Device()
GSMA   = model.Gsma()
OTA    = model.SCP80()
AMDB   = model.SCP81()
SERVER = model.Smsr()
EUICC  = model.Euicc()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('eSIM 3.1.rc5')

'''
DAKO-275

- After open channel, the card send the client hello (first handshake), the card is waiting for server hello response.
- If no response from server after the client hello, the card will not close channel and will keep the session opened. So on next SMS, the card will not response, because the card is blocked on previous session.
- The only way to recover is to make a reboot.

steps:
1. Init handset
2. Send PUSH SMS (triggering open channel)
3. After OPEN CHANNEL wait for Client Handshake.
4. Make the server not to reply the Client Handshake.
'''

# i=0
# flag = True
# while flag:
# 	try:
# 		i+=1
# 		EUICC.init()
# 		pprint.h1('iteration #{}'.format(i))
# 		# SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001600', scp=81, apdu_format='definite')
# 		# SERVER.download_profile('A0000005591010FFFFFFFF8900001600', saipv2_txt='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
# 		if i%2==0:
# 			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001600' ), 80, apdu_format='definite', chunk='01')
# 		else:
# 			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001500' ), 80, apdu_format='definite', chunk='01')
# 		# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001600' ), 80, apdu_format='definite', chunk='01')
# 		DEVICE.fetch_one(sw)

# 		EUICC.init()
# 		data, sw = DEVICE.envelope_location_status()
# 		data, sw = DEVICE.fetch_one(sw)
# 		data, sw = DEVICE.fetch_one(sw)
# 		# data, sw = DEVICE.fetch_one(sw)
# 		SetLogLevel('debug')
# 		euicc_notification_sequence_number = AMDB.establish_tls_session(sw=sw, success=False)
# 		SetLogLevel('info')

# 		data, sw = Envelope('D70C020282812401' + '01' + '022503' + '002000')

# 		OTA.spi1 = 0x16
# 		OTA.spi2 = 0x00
# 		OTA.kic  = 0x12
# 		OTA.kid  = 0x12
# 		capdu, rapdu, sw = OTA.push_sms()
		
# 		if i == 2:
# 			flag=True
# 		else:
# 			flag=False
# 		try:
# 			data, sw = DEVICE.fetch_one(sw, 'open channel')
# 		except:
# 			print('open channel not processed')
# 	except:
# 		# flag = False
# 		print('polling off not triggered')

# flag=True
# while flag:
# 	try:
# 		EUICC.init()
# 		SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001500', scp=81, apdu_format='definite')
# 		SERVER.download_profile('A0000005591010FFFFFFFF8900001500', saipv2_txt='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
# 		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001500' ), 80, apdu_format='definite', chunk='01')
# 		DEVICE.fetch_one(sw)

# 		EUICC.init()
# 		data, sw = DEVICE.envelope_location_status()
# 		# data, sw = DEVICE.fetch_one(sw)
# 		# data, sw = DEVICE.fetch_one(sw)
# 		# data, sw = DEVICE.fetch_one(sw, 'send data')
# 		data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
# 		SetLogLevel('debug')
# 		euicc_notification_sequence_number = AMDB.establish_tls_session(sw=sw, success=False)
# 		SetLogLevel('info')
# 		data, sw = Envelope('D70C020282812401' + '01' + '022503' + '002000')
# 		print('condition passed')
# 		flag=False
# 	except:
# 		EUICC.init()
# 		SERVER.disable_profile('A0000005591010FFFFFFFF8900001500', scp=80, network_service=False, apdu_format='indefinite')
# 		SERVER.delete_profile('A0000005591010FFFFFFFF8900001500', scp=80, apdu_format='definite')
# 		DEVICE.get_status()
	

# flag=True
# i=0
# while flag:
# 	try:
		# i+=1
EUICC.init()
capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001500' ), 80, apdu_format='definite', chunk='01')
DEVICE.fetch_one(sw)

EUICC.init()
data, sw = DEVICE.envelope_network_rejection()
data, sw = DEVICE.envelope_limited_service()
data, sw = DEVICE.envelope_network_rejection()
data, sw = DEVICE.envelope_limited_service()
data, sw = DEVICE.envelope_location_status()

data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
SetLogLevel('debug')
euicc_notification_sequence_number = AMDB.establish_tls_session(sw=sw, success=False)
SetLogLevel('info')
data, sw = Envelope('D70C020282812401' + '01' + '022503' + '002000')
# data, sw = DEVICE.fetch_one(sw)
print('condition passed')
# flag=False
	# except:
	# 	EUICC.init()
	# 	SERVER.disable_profile('A0000005591010FFFFFFFF8900001500', scp=80, network_service=False, apdu_format='indefinite')
	# SERVER.delete_profile('A0000005591010FFFFFFFF8900001500', scp=80, apdu_format='definite')
	# DEVICE.get_status()