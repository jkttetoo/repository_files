import sys,os.path
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()
INGENICO	= model.IngenicoReader()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

# SetCurrentReader('eSIM.3.1.rc10')
SetCurrentReader('eSIM 3.1.rc11')

EUICC.init()
SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001200', scp=80, apdu_format='definite')
SERVER.download_profile('A0000005591010FFFFFFFF8900001200', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
SERVER.enable_profile('A0000005591010FFFFFFFF8900001200', scp=80, network_service=False, apdu_format='definite', chunk='01')

EUICC.init()
SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001300', scp=80, apdu_format='definite')
SERVER.download_profile('A0000005591010FFFFFFFF8900001300', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")

EUICC.init()
capdu, radpu, sw = SERVER.enable_profile('A0000005591010FFFFFFFF8900001100', scp=81, network_service=False, apdu_format='definite', chunk='01')

# Store Data - Update SMSR Address
EUICC.init()
capdu, rapdu, sw = SERVER.ram(['80E28800153A03124F10A0000005591010FFFFFFFF8900001300', GSMA.update_smsr_addressing_parameters('148.251.255.255','1919')], 81, apdu_format='definite',chunk = '01')
EUICC.refresh(sw, False)
SERVER.enable_profile('A0000005591010FFFFFFFF8900001100', scp=80, network_service=False, apdu_format='definite', chunk='01')
SERVER.enable_profile('A0000005591010FFFFFFFF8900001300', scp=80, network_service=False, apdu_format='definite', chunk='01')
SERVER.enable_profile('A0000005591010FFFFFFFF8900001100', scp=80, network_service=False, apdu_format='definite', chunk='01')

SERVER.audit_isdp_list(81, apdu_format='definite')

EUICC.init()
SERVER.delete_profile('A0000005591010FFFFFFFF8900001300', 80, apdu_format='definite', chunk='01')
EUICC.init()
SERVER.delete_profile('A0000005591010FFFFFFFF8900001200', 80, apdu_format='definite', chunk='01')



