import sys,os.path
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DE620 Contact Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

class con3_2140(unittest.TestCase):

    def setUp(self):
        pprint.h1('Continuous Download Enable Delete profile loop')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001900', scp=80, apdu_format='definite')
        SERVER.download_profile('A0000005591010FFFFFFFF8900001900', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")

    def test_body(self):
        i=0
        for i in range(7):
            # iteration+=1
            EUICC.init()
            SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
            SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")

            EUICC.init()
            SERVER.enable_profile('A0000005591010FFFFFFFF8900001800', scp=80, network_service=False, apdu_format='definite', chunk='01')

            EUICC.init()
            SERVER.delete_profile('A0000005591010FFFFFFFF8900001900', scp=80, apdu_format='definite', chunk='01')
            SendAPDU('80F2000C00')

            EUICC.init()
            SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001900', scp=80, apdu_format='definite')
            SERVER.download_profile('A0000005591010FFFFFFFF8900001900', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")

            EUICC.init()
            SERVER.enable_profile('A0000005591010FFFFFFFF8900001900', scp=80, network_service=False, apdu_format='definite', chunk='01')

            EUICC.init()
            SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
            SendAPDU('80F2000C00')
        
    def tearDown(self):
        EUICC.init()
        SERVER.disable_profile('A0000005591010FFFFFFFF8900001900', scp=80, network_service=False, apdu_format='definite', chunk='01')

        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001900', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(con3_2140, 'test'))

    return suite

if __name__ == '__main__':
    unittest.main()