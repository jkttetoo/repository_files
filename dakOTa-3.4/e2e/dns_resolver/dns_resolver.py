import sys,os.path,binascii
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Ot.PCOM32Manager import *
from Ot.GlobalPlatform import SCPKeyset, SecurityDomain, IssuerSecurityDomain_Profile
from Mobile import *
from util import *
import unittest, collections
import const
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DE620 Contact Reader')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')

scp03_kvn  = Constants.config['ISDR']['scp03_kvn'] 
scp03_scp  = Constants.config['ISDR']['scp03_scp'] 
scp03_sEnc = Constants.config['ISDR']['scp03_sEnc'] 
scp03_sMac = Constants.config['ISDR']['scp03_sMac'] 
scp03_sKek = Constants.config['ISDR']['scp03_sKek'] 

tags = collections.OrderedDict()

tags['buffer_size'] = '39'
tags['network_access_name'] = '47'
tags['sim_me_transport_layer_level'] ='3C' 
tags['data_destination_addr'] = '3E'

sd      = SecurityDomain('A0000005591010FFFFFFFF8900000100', SCPKeyset(kvn=scp03_kvn,scp=scp03_scp,sEnc=scp03_sEnc,sMac=scp03_sMac,sKek=scp03_sKek, sequence_number=None))
scp80   = model.SCP80(parent_aid=sd.getAID(), spi1=0x16, spi2=0x39, kic=0x12, kid=0x12, algo_crypto_verif=const.hexa.ciphers['AES'])
AMDB    = model.SCP81(scp80=scp80)
OTA     = model.SCP80()
OTA.spi1 = 0x16
OTA.spi2 = 0x39
OTA.kic  = 0x12
OTA.kid  = 0x12

class con3_1521(unittest.TestCase):

    def setUp(self):
        # pass
        pprint.h1('Enable Profile with power failure: SMS notifications')
        aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

        path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
        final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC11_HTTP_SMS_3.1_DNS_LM.pcom') 
        LogInfo(final_path)

        aManager.SetStopOnError(False)
        aManager.launchPCOM32(final_path)

    def test_body(self):
        EUICC.init()
        capdu, rapdu, sw = OTA.push_sms()
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        data, sw = Fetch(sw[2:4])
        TerminalResponse('810301270002028281030100240101')
        data, sw = Envelope('D70C020282812401' + '01' + '022503' +  '002000')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        data, sw = Fetch(sw[2:4])

        values = hexastring.parse_tlvs(data, tags, start='2028182') # consider data only after tlv device_ids
        transport_protocol = 'tcp' if values['sim_me_transport_layer_level'][0:2] == '02' else 'udp' # udp is 01
        LogInfo(transport_protocol)
        if transport_protocol=='udp':
            LogInfo( 'Open Channel DNS request' )
            sw = TerminalResponse('81030140008202818203010038028100')
            data, sw = Fetch(sw[2:4]) # send data
            sw= TerminalResponse('810301130002028281030100')
            LogInfo('fetch send data dns query: {}'.format(data))
           
            Fetch(sw[2:4]) # timer
            TerminalResponse('810301270002028281030100240101')
            data, sw = Envelope('D60E1901098202218138028100370148')
            if sw == '910F' : 
                data = Fetch(sw[2:4]) # setup event list
                data, sw = TerminalResponse(data)
            if sw == '910B' : 
                data = Fetch(sw[2:4]) # polling off
                data, sw = TerminalResponse(data)
            Fetch(sw[2:4]) # receive data
            dns_response = '81030142008202218103010036 33 5378 8100 0001000200000000 06 6F74616C7465 06 74656C63656C 03 636F6D 0000010001 C00C000100010000825800 040AC96C68 370100'
            sw = TerminalResponse(dns_response)

            first_query_char  = binascii.unhexlify('6F74616C7465').decode("utf-8")
            second_query_char = binascii.unhexlify('74656C63656C').decode("utf-8")
            third_query_char  = binascii.unhexlify('636F6D').decode("utf-8")

            LogInfo("\n")
            LogInfo('#'*78)
            LogInfo('dns server response:')
            LogInfo('-----------------------------')
            LogInfo(dns_response[30:])
            LogInfo("DNS transaction id     : 5378")
            LogInfo("DNS flag               : 8100")
            LogInfo("DNS query              : {} {} {}".format(first_query_char, second_query_char, third_query_char)) 
            LogInfo('#'*78)

            LogInfo("Close Channel")
            data, sw = Fetch(sw[2:4]) # close channel
            sw = TerminalResponse('81030141008202818203010038028100')
            data, sw = Fetch(sw[2:4])
            sw = TerminalResponse('81030140018202828183013A')
            sw = DEVICE.fetch_one(sw)
            data, sw = Envelope('D70C020282812401' + '01' + '022503' +  '002000')
            data, sw = Fetch(sw[2:4])
            sw = TerminalResponse('81030140018202828183013A')
            sw = DEVICE.fetch_one(sw)

        # LogI
                
    def tearDown(self):
        pass
        



def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(con3_714, 'test'))
    suite.addTest(unittest.makeSuite(con3_717, 'test'))
    suite.addTest(unittest.makeSuite(con3_4046, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()