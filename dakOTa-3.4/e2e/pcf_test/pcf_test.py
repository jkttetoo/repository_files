import sys,os.path, configparser
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
from Ot.PCOM32Manager import * 
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DE620 Contact Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

class con3_3986(unittest.TestCase):

    def setUp(self):
        pprint.h1('Disable fallback profile containing PCF (0x04 - delete when disable)')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der")
        EUICC.init()
        SERVER.enable_profile('A0000005591010FFFFFFFF8900001800', scp=80, network_service=False, apdu_format='definite', chunk='01')
        SERVER.set_fallback_attribute('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SERVER.audit_isdp_list(scp=80, apdu_format='definite')

    def test_body(self):
        EUICC.init()
        SERVER.enable_profile('A0000005591010FFFFFFFF8900001100', scp=80, network_service=False, apdu_format='definite', chunk='01')
        capdu, disabled_profile, sw = SERVER.audit_isdp_disabled(scp=80, apdu_format='definite')
        SERVER.audit_isdp_list(scp=80, apdu_format='definite')
        self.assertRegex(disabled_profile, 'A0000005591010FFFFFFFF8900001800')

    def tearDown(self):
        SERVER.set_fallback_attribute('A0000005591010FFFFFFFF8900001100', scp=80, apdu_format='definite', chunk='01')
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')

# class con3_3982(unittest.TestCase):
    
#     def setUp(self):
#         pprint.h1('Enable Profile loaded to pre-created ISD-P in HTTPs')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RCxx_HTTP_SMS_3.1_DNS_LM_precreated_isdp.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#         EUICC.init()
#         SERVER.download_profile('A0000005591010FFFFFFFF8900001200', saipv2_txt="saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der")

#     def test_body(self):
#         EUICC.init()
#         capdu, rapdu, sw = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=80, network_service=False, apdu_format='definite', chunk='01')
#         self.assertRegex(rapdu, '9000')
#         capdu, enabled_profile, sw = SERVER.audit_isdp_enabled(scp=80, apdu_format='definite')
#         self.assertRegex(enabled_profile, 'A0000005591010FFFFFFFF8900001200')

#         EUICC.init()
#         capdu, rapdu, sw = SERVER.disable_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=80, network_service=False, apdu_format='definite', chunk='01')
#         self.assertRegex(rapdu, '9000')
#         capdu, disabled_profile, sw = SERVER.audit_isdp_disabled(scp=80, apdu_format='definite')
#         self.assertRegex(disabled_profile, 'A0000005591010FFFFFFFF8900001200')

#         EUICC.init()
#         capdu, rapdu, sw = SERVER.delete_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=80, apdu_format='definite', chunk='01')
#         self.assertRegex(rapdu, '9000')
    #     capdu, isdp_list, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
    #     self.assertNotRegex(isdp_list, 'A0000005591010FFFFFFFF8900001200')

    # def tearDown(self):
    #     pprint.h1('Enable Profile loaded to pre-created ISD-P in HTTPs')
    #     aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

    #     path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
    #     final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC11_HTTP_SMS_3.1_DNS_LM.pcom') 
    #     LogInfo(final_path)

    #     aManager.SetStopOnError(False)
    #     aManager.launchPCOM32(final_path)


# class con3_3983(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('Enable Profile loaded to pre-created ISD-P in HTTPs')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RCxx_HTTP_SMS_3.1_DNS_LM_precreated_isdp.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#         EUICC.init()
#         SERVER.download_profile('A0000005591010FFFFFFFF8900001200', saipv2_txt="saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der")

#     def test_body(self):
#         EUICC.init()
#         SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=80, network_service=False, apdu_format='definite', chunk='01')

#         EUICC.init()
#         capdu, rapdu, sw = SERVER.disable_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=80, network_service=False, apdu_format='definite', chunk='01')
#         self.assertRegex(rapdu, '69E1')
#         capdu, disabled_profile, sw = SERVER.audit_isdp_disabled(scp=80, apdu_format='definite')
#         self.assertNotRegex(disabled_profile, 'A0000005591010FFFFFFFF8900001200')

#     def tearDown(self):
#         pass


# class con3_3984(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('Enable Profile loaded to pre-created ISD-P in HTTPs')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RCxx_HTTP_SMS_3.1_DNS_LM_precreated_isdp.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#         EUICC.init()
#         SERVER.download_profile('A0000005591010FFFFFFFF8900001200', saipv2_txt="saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der")

#     def test_body(self):
#         EUICC.init()
#         capdu, rapdu, sw = SERVER.delete_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=80, apdu_format='definite', chunk='01')
#         self.assertRegex(rapdu, '69E1')
#         capdu, isdp_list, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
#         self.assertRegex(isdp_list, 'A0000005591010FFFFFFFF8900001200')

#     def tearDown(self):
#         pass

# class con3_3985(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('Enable Profile loaded to pre-created ISD-P in HTTPs')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RCxx_HTTP_SMS_3.1_DNS_LM_precreated_isdp.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#         EUICC.init()
#         SERVER.download_profile('A0000005591010FFFFFFFF8900001200', saipv2_txt="saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der")

#     def test_body(self):
#         EUICC.init()
#         SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=80, network_service=False, apdu_format='definite', chunk='01')

#         EUICC.init()
#         capdu, rapdu, sw = SERVER.disable_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=80, network_service=False, apdu_format='definite', chunk='01')
#         self.assertRegex(rapdu, '9000')
#         capdu, isdp_list, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
#         self.assertNotRegex(isdp_list, 'A0000005591010FFFFFFFF8900001200')

#     def tearDown(self):
#         pass

class con3_4017(unittest.TestCase):

    def setUp(self):
        pprint.h1('Rollback to profile that is deleted due to PCF (0x04, delete after disabling)')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001900', scp=80, apdu_format='definite')
        SERVER.download_profile('A0000005591010FFFFFFFF8900001900', saipv2_txt="saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der")
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")

    def test_body(self):
        EUICC.init()
        SERVER.enable_profile('A0000005591010FFFFFFFF8900001900', scp=80, network_service=False, apdu_format='definite', chunk='01')
        SERVER.audit_isdp_list(scp=80, apdu_format='definite')
        EUICC.init()
        SERVER.enable_profile('A0000005591010FFFFFFFF8900001800', scp=80, network_service=True, apdu_format='definite', chunk='01')
        capdu, enabled_profile, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
        self.assertRegex(enabled_profile, '530101')

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(con3_3986, 'test'))
    suite.addTest(unittest.makeSuite(con3_3982, 'test'))
    suite.addTest(unittest.makeSuite(con3_3983, 'test'))
    suite.addTest(unittest.makeSuite(con3_3984, 'test'))
    suite.addTest(unittest.makeSuite(con3_3985, 'test'))
    suite.addTest(unittest.makeSuite(con3_4017, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()