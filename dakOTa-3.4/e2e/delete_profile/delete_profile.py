import sys,os.path
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DE620 Contact Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

class con3_735(unittest.TestCase):

    def setUp(self):
        pprint.h1('Delete Profile with non existing ISD-P')
        EUICC.init()

    def test_body(self):
        
        capdu, data, sw = SERVER.delete_profile('A0000005591010FFFFFFFF890000AA00', scp=80, apdu_format='definite', chunk='01')
        self.assertRegex(data, '6A88')
    
    def tearDown(self):
        pass

class con3_883(unittest.TestCase):

    def setUp(self):
        pprint.h1('Delete Profile failure on OPEN CHANNEL (push sms is received)')
        EUICC.init()

    def test_body(self):
        OTA.spi1 = 0x16
        OTA.spi2 = 0x39
        OTA.kic  = 0x12
        OTA.kid  = 0x12
        capdu, rapdu, sw = OTA.push_sms()
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        self.assertEqual(sw, '9113')
        # data, sw = Fetch(sw[2:4])
        # sw = TerminalResponse('810301270002028281030100240101')
        data, sw = Fetch(sw[2:4])
        data, sw = Envelope('D70C020282812401' + '01' + '022503' +  '000200')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        data, sw = Fetch(sw[2:4])
        try:
            SERVER.ram(GSMA.delete_profile('A0000005591010FFFFFFFF890000AA00'), 81, pull=True, apdu_format='indefinite', chunk='01')
        except:
            LogInfo("Expected to be failed due to unable make OPEN CHANNEL")

    
    def tearDown(self):
        EUICC.init()
        SERVER.audit_isdp_list(scp=80, apdu_format='definite')




def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(con3_735, 'test'))
    suite.addTest(unittest.makeSuite(con3_883, 'test'))

    return suite

if __name__ == '__main__':
    unittest.main()