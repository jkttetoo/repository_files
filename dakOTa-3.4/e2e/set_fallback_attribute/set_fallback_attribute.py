import sys,os.path
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DakOTaV31SLE97PC41387XSecureRC7 Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

class con3_808(unittest.TestCase):

    def setUp(self):
        pprint.h1('Set Fall-back Profile with non existing ISD-P')
        EUICC.init()

    def test_body(self):
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
        capdu, data, sw = SERVER.set_fallback_attribute('A0000005591010FFFFFFFF890000AA00', scp=81, apdu_format='definite', chunk='01')

    def tearDown(self):
        pass

class con3_1027(unittest.TestCase):

    def setUp(self):
        pprint.h1('Set a fallback profile when the device/eUICC is off')
        EUICC.init()
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

    def test_body(self):
        PowerOff()
        try:
            i=0
            for i in range(3):
                try:
                    print('iteration : {}'.format(i))
                    SERVER.set_fallback_attribute('A0000005591010FFFFFFFF8900001000', scp=81, apdu_format='definite', chunk='01')
                except :
                    LogInfo('test')
        except :
            LogInfo("Expected to be failed")

    def tearDown(self):
        EUICC.init()
        capdu, audit_response2, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(con3_808, 'test'))
    suite.addTest(unittest.makeSuite(con3_1027, 'test'))

    return suite

if __name__ == '__main__':
    unittest.main()