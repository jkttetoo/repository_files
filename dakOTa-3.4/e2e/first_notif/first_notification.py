import sys,os.path
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Ot.GlobalPlatform import SCPKeyset, SecurityDomain, IssuerSecurityDomain_Profile
from Ot.PCOM32Manager import *
from Mobile import *
from util import pprint
import const
import unittest
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DE620 Contact Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

scp03_kvn  = Constants.config['ISDR']['scp03_kvn'] 
scp03_scp  = Constants.config['ISDR']['scp03_scp'] 
scp03_sEnc = Constants.config['ISDR']['scp03_sEnc'] 
scp03_sMac = Constants.config['ISDR']['scp03_sMac'] 
scp03_sKek = Constants.config['ISDR']['scp03_sKek'] 

sd      = SecurityDomain('A0000005591010FFFFFFFF8900000100', SCPKeyset(kvn=scp03_kvn,scp=scp03_scp,sEnc=scp03_sEnc,sMac=scp03_sMac,sKek=scp03_sKek, sequence_number=None))
scp80   = model.SCP80(parent_aid=sd.getAID(), spi1=0x16, spi2=0x39, kic=0x12, kid=0x12, algo_crypto_verif=const.hexa.ciphers['AES'])
AMDB    = model.SCP81(scp80=scp80)
OTA     = model.SCP80()

# class con3_3795(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('First Notification PSM retry after timeout: HTTP')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_HTTP_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#     def test_body(self):
#         LogInfo('>> device initialization')
#         PowerOn()
#         sw = DEVICE.terminal_profile()
#         DEVICE.fetch_all(sw)

#         data, sw = DEVICE.envelope_location_status(status='normal service')
#         LogInfo(">>First Open Channel")
#         data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False)
#         capu, rapdu, sw, seq_counter = AMDB.remote_management(None, sw)
#         assert (capu, rapdu) == (None, None)
#         data, sw = DEVICE.envelope_timer_expiration(id='psm')
#         LogInfo(">>Second Open Channel")
#         data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel'], catch_first_notif=False)
#         LogInfo('catch 2nd euicc first notification (http) + server confirmation')
#         AMDB.remote_management(GSMA.handle_notification_confirmation(), sw)
                
#     def tearDown(self):
#         pass

# class con3_3796(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('First Notification PSM retry after power off: HTTP')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_HTTP_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#     def test_body(self):
#         LogInfo('>> device initialization')
#         PowerOn()
#         sw = DEVICE.terminal_profile()
#         DEVICE.fetch_all(sw)

#         data, sw = DEVICE.envelope_location_status(status='normal service')
#         LogInfo(">>First Open Channel")
#         data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False)
#         capu, rapdu, sw, seq_counter = AMDB.remote_management(None, sw)
#         PowerOff()
#         EUICC.init()
#         data, sw = DEVICE.envelope_timer_expiration(id='psm')
#         LogInfo(">>Second Open Channel")
#         data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel'], catch_first_notif=False)
#         LogInfo('catch 2nd euicc first notification (http) + server confirmation')
#         AMDB.remote_management(GSMA.handle_notification_confirmation(), sw)
                
#     def tearDown(self):
#         pass

# class con3_4038(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('First Notification PSM retry after timeout: SMS')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#     def test_body(self):
#         LogInfo('>> device initialization')
#         PowerOn()
#         sw = DEVICE.terminal_profile()
#         DEVICE.fetch_all(sw)

#         data, sw = DEVICE.envelope_location_status(status='normal service')
#         LogInfo(">>First Open Channel")
#         data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False)
#         seq_number = pprint.pprint_sms_first_notif(data)
#         data, sw, cmd_type = DEVICE.fetch_all(sw)
#         LogInfo('server DOES NOT confirm reception of euicc notification')
#         data, sw = DEVICE.envelope_timer_expiration(id='psm')
#         data, sw = DEVICE.fetch_one(sw, 'send sms')
#         seq_number = pprint.pprint_sms_first_notif(data)
#         data, sw, cmd_type = DEVICE.fetch_all(sw)
#         LogInfo('server confirms reception of euicc notification (second one because first was not confirmed)')
#         OTA.spi1 = 0x16
#         OTA.spi2 = 0x39
#         OTA.kic  = 0x12
#         OTA.kid  = 0x12
#         capdu, rapdu, sw = OTA.remote_management(GSMA.handle_notification_confirmation(seq_number))
                
#     def tearDown(self):
#         pass

# class con3_4039(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('First Notification PSM retry after power off: SMS')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#     def test_body(self):
#         LogInfo('>> device initialization')
#         PowerOn()
#         sw = DEVICE.terminal_profile()
#         DEVICE.fetch_all(sw)

#         data, sw = DEVICE.envelope_location_status(status='normal service')
#         LogInfo(">>First Open Channel")
#         data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False)
#         seq_number = pprint.pprint_sms_first_notif(data)
#         data, sw, cmd_type = DEVICE.fetch_all(sw)
#         LogInfo('server DOES NOT confirm reception of euicc notification')
#         PowerOff()
#         EUICC.init()
#         data, sw = DEVICE.envelope_timer_expiration(id='psm')
#         data, sw = DEVICE.fetch_one(sw, 'send sms')
#         seq_number = pprint.pprint_sms_first_notif(data)
#         data, sw, cmd_type = DEVICE.fetch_all(sw)
#         LogInfo('server confirms reception of euicc notification (second one because first was not confirmed)')
#         OTA.spi1 = 0x16
#         OTA.spi2 = 0x39
#         OTA.kic  = 0x12
#         OTA.kid  = 0x12
#         capdu, rapdu, sw = OTA.remote_management(GSMA.handle_notification_confirmation(seq_number))
                
#     def tearDown(self):
#         pass



def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(con3_3795, 'test'))

    return suite

if __name__ == '__main__':
    unittest.main()