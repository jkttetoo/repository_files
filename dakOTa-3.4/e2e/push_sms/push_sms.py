import sys,os.path
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DE620 Contact Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

class con3_4027(unittest.TestCase):

    def setUp(self):
        pass

    def test_body(self):
        EUICC.init()
        sw = ""
        try:
            capdu, rapdu, sw = OTA.push_sms()
        except:
            LogInfo("Card should block such message")
        
    def tearDown(self):
        pass




def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(con3_4027, 'test'))

    return suite

if __name__ == '__main__':
    unittest.main()