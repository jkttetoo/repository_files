import sys,os.path
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Ot.PCOM32Manager import *
from Ot.GlobalPlatform import SCPKeyset, SecurityDomain, IssuerSecurityDomain_Profile
from Mobile import *
from util import *
import unittest
import const
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DE620 Contact Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

scp03_kvn  = Constants.config['ISDR']['scp03_kvn'] 
scp03_scp  = Constants.config['ISDR']['scp03_scp'] 
scp03_sEnc = Constants.config['ISDR']['scp03_sEnc'] 
scp03_sMac = Constants.config['ISDR']['scp03_sMac'] 
scp03_sKek = Constants.config['ISDR']['scp03_sKek'] 

sd      = SecurityDomain('A0000005591010FFFFFFFF8900000100', SCPKeyset(kvn=scp03_kvn,scp=scp03_scp,sEnc=scp03_sEnc,sMac=scp03_sMac,sKek=scp03_sKek, sequence_number=None))
scp80   = model.SCP80(parent_aid=sd.getAID(), spi1=0x16, spi2=0x39, kic=0x12, kid=0x12, algo_crypto_verif=const.hexa.ciphers['AES'])
AMDB    = model.SCP81(scp80=scp80)
OTA     = model.SCP80()
OTA.spi1 = 0x16
OTA.spi2 = 0x39
OTA.kic  = 0x12
OTA.kid  = 0x12

# class con3_669(unittest.TestCase):

    # def setUp(self):
        # pprint.h1('Enable Profile loaded to pre-created ISD-P in HTTPs')
        # aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

        # path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
        # final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RCxx_HTTP_SMS_3.1_DNS_LM_precreated_isdp.pcom') 
        # LogInfo(final_path)

        # aManager.SetStopOnError(False)
        # aManager.launchPCOM32(final_path)

        # EUICC.init()
        # SERVER.download_profile('A0000005591010FFFFFFFF8900001200', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        # SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
        # EUICC.init()

    # def test_body(self):
    #     capdu, response_audit, sw = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=81, network_service=False, apdu_format='definite', chunk='01')
    #     LogInfo(response_audit)
    #     self.assertNotRegex(response_audit,'6A88', msg = 'Expected result not match')

    # def tearDown(self):
    #     pass
        # path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
        # final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC11_HTTP_SMS_3.1_DNS_LM.pcom') 
        # LogInfo(final_path)

        # aManager.SetStopOnError(False)
        # aManager.launchPCOM32(final_path)

# class con3_671(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('Enable Profile loaded to pre-created ISD-P in SMS')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RCxx_SMS_3.1_DNS_LM_precreated_isdp.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#         EUICC.init()
#         SERVER.download_profile('A0000005591010FFFFFFFF8900001200', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
#         SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
#         EUICC.init()

#     def test_body(self):
#         capdu, response_audit, sw = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001200', scp=81, network_service=False, apdu_format='definite', chunk='01')
#         LogInfo(response_audit)
#         # test_cases.assertRegex(response_audit,'6A88', msg = 'Expected result not match')

#     def tearDown(self):
#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC11_HTTP_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)



class con3_714(unittest.TestCase):

    def setUp(self):
        pprint.h1('Enable via HTTP profile with non existing ISD-P')
        EUICC.init()

    def test_body(self):
        capdu, response_audit, sw = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF890000AA00', scp=81, network_service=False, apdu_format='definite', chunk='01')
        LogInfo(response_audit)
        test_cases.assertRegex(response_audit,'6A88', msg = 'Expected result not match')

    def tearDown(self):
        pass

class con3_717(unittest.TestCase):

    def setUp(self):
        pprint.h1('Enable via SMS profile with non existing ISD-P')
        EUICC.init()

    def test_body(self):
        capdu, response_audit, sw = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF890000AA00', scp=80, network_service=False, apdu_format='definite', chunk='01')
        LogInfo(response_audit)
        test_cases.assertRegex(response_audit,'6A88', msg = 'Expected result not match')

    def tearDown(self):
        pass

class con3_822(unittest.TestCase):

    def setUp(self):
        pprint.h1('Enable Profile with power failure: SMS notifications')

    def test_body(self):
        EUICC.init()
        capdu, data, sw = OTA.push_sms()
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')

    def tearDown(self):
        pass

# class con3_1521(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('Enable Profile with power failure: SMS notifications')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#         EUICC.init()
#         SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
#         SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
#         SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
#         EUICC.init()
#         SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=81, network_service=False, apdu_format='definite', chunk='01')


#     def test_body(self):
#         EUICC.init()
#         capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001100'), 80, apdu_format='indefinite', chunk='01')
#         DEVICE.fetch_one(sw, 'refresh')
#         PowerOff()
#         EUICC.init()
#         data, sw = DEVICE.envelope_location_status()
#         data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
#         assert sw[0:2] != 91, 'notification (sms or http) was expected!'
#         seq_number = pprint.pprint_sms_first_notif(data)
#         data, sw, cmd_type = DEVICE.fetch_all(sw)
#         LogInfo('server confirms reception of euicc notification')
#         capdu, rapdu, sw = OTA.remote_management(GSMA.handle_notification_confirmation(seq_number))

#     def tearDown(self):
#         EUICC.init()
#         SERVER.disable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=80, network_service=False, apdu_format='definite', chunk='01')
#         EUICC.init()
#         SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
#         SendAPDU('80F2000C00')

# class con3_2175(unittest.TestCase):
#     def setUp(self):
#         pprint.h1('Enable Profile with http notification lost and audit success')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_HTTP_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)
#         EUICC.init()
#         SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001000', scp=80, network_service=False, apdu_format='definite', chunk='01')

#     def test_body(self):
#         EUICC.init()
#         capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001100'), 81, apdu_format='indefinite', chunk='01')
#         if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False, False)
#         SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

#     def tearDown(self):
#         pass

# class con3_2176(unittest.TestCase):
#     def setUp(self):
#         pprint.h1('Enable Profile with sms notification lost and audit success')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)
#         EUICC.init()
#         SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001000', scp=80, network_service=False, apdu_format='definite', chunk='01')

#     def test_body(self):
#         EUICC.init()
#         capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001100'), 80, apdu_format='indefinite', chunk='01')
#         if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False, False)
#         SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

#     def tearDown(self):
#         pass

# class con3_2183(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('Enable profile while first notification never confirmed')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_HTTP_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#     def test_body(self):
#         LogInfo('>> device initialization')
#         PowerOn()
#         sw = DEVICE.terminal_profile()
#         DEVICE.fetch_all(sw)

#         data, sw = DEVICE.envelope_location_status(status='normal service')
#         LogInfo(">>First Open Channel")
#         data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], catch_first_notif=False)
#         capu, rapdu, sw, seq_counter = AMDB.remote_management(None, sw)
#         PowerOff()
#         EUICC.init()

#         capdu, response_audit, sw = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001000', scp=80, network_service=False, apdu_format='definite', chunk='01')
#         LogInfo(response_audit)
#         test_cases.assertRegex(response_audit,'9000', msg = 'Expected result not match')

#     def tearDown(self):
#         pass

class con3_2273(unittest.TestCase):

    def setUp(self):
        pprint.h1('Enable Profile with power failure after sending notification')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

    def test_body(self):
        capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001800'), 80, apdu_format='indefinite', chunk='01')
        DEVICE.fetch_one(sw, 'refresh')
        EUICC.init()
        data, sw = DEVICE.envelope_location_status()
        data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
        assert sw[0:2] != 91, 'notification (sms or http) was expected!'
        data, sw = DEVICE.envelope_timer_expiration(id='psm')
        data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel'], catch_first_notif=False)

    def tearDown(self):
        SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
        EUICC.init()
        SERVER.disable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=80, network_service=False, apdu_format='definite', chunk='01')
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')

# class con3_4030(unittest.TestCase):

#     def setUp(self):
#         pprint.h1('Enable Profile with sms notification lost and audit success')
#         aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

#         path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
#         final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC9_SMS_3.1_DNS_LM.pcom') 
#         LogInfo(final_path)

#         aManager.SetStopOnError(False)
#         aManager.launchPCOM32(final_path)

#         EUICC.init()
#         SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001000', scp=80, network_service=False, apdu_format='definite', chunk='01')

#     def test_body(self):
#         EUICC.init()
#         capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001100'), 80, apdu_format='indefinite', chunk='01')
#         if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False, False)
#         SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

#     def tearDown(self):
#         pass

class con3_4046(unittest.TestCase):

    def setUp(self):
        pprint.h1('Enable profile check retry mechanism of prepare communication')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

    def test_body(self):
        PowerOff()
        # i=0
        # for i in range(3):
            # with self.assertRaises(Exception):
            #     SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=80, network_service=True, apdu_format='definite', chunk='01')
                   # test_cases.assertRaises(Exception, SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001800'), 80, apdu_format='definite', chunk='01'))
        try:
            i=0
            for i in range(3):
                try:
                    print('iteration : {}'.format(i))
                    capdu, response_audit, sw = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=80, network_service=True, apdu_format='definite', chunk='01')
                except :
                    LogInfo('test')
        except:
            LogInfo("Expected to be failed")

        EUICC.init()
        capdu, response_audit, sw = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=80, network_service=False, apdu_format='definite', chunk='01')
        test_cases.assertRegex(response_audit,'9000', msg = 'Expected result not match')

    def tearDown(self):
        EUICC.init()
        SERVER.disable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=80, network_service=False, apdu_format='definite', chunk='01')
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(con3_714, 'test'))
    suite.addTest(unittest.makeSuite(con3_717, 'test'))
    suite.addTest(unittest.makeSuite(con3_4046, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()