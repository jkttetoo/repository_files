import sys,os.path
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Ot.PCOM32Manager import *
from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain
from Ot.GlobalPlatform import getVersion as GlobalPlatformModuleGetVersion
from Oberthur import *
from Mobile import *
from util import pprint, aid, hexastring
import unittest, const
import configparser
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
OTA			= model.SCP80()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA		= model.Gsma()

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

test_cases = unittest.TestCase()

# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('DE620 Contact Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

class con3_693(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download Profile (with update SMSR address) to non existing ISD-P')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

    def test_body(self):
        EUICC.init()
        try:
            download_time, euicc_responses, file_size = SERVER.download_profile('A0000005591010FFFFFFFF8900001900', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        except :
            LogInfo('Error triggered')

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')

class con3_812(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download Profile when DUT has a power failure')
        pass

    def test_body(self):
        EUICC.init()
        SERVER.audit_resource_info(scp = 80, apdu_format = 'indefinite') 
        SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001800", scp=81, apdu_format='definite')
        keyset_isdp = EUICC.get_isdp_keyset_scp03t('A0000005591010FFFFFFFF8900001800')
        LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))
        isdp = IssuerSecurityDomain_Profile('A0000005591010FFFFFFFF8900001300', keyset_isdp)
        apdu_open_scp03t = isdp.openDefaultSecureChannel(secureMessaging='33')
        profile_download_payload = apdu_open_scp03t[0] + apdu_open_scp03t[1]
        ram_targeted_app = aid.format_for_http_header_targeted_app('A0000005591010FFFFFFFF8900001800')
        
        saipv2_hexastr, file_size = hexastring.saipv2_to_hexastr('saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
        split_size = 1000
        saip_splits = [ saipv2_hexastr[x*split_size : x*split_size + split_size] for x in range(len(saipv2_hexastr)//split_size +1) ]
        commands_scp_counter_mac_chaining_value = []
        
        for saip_split in saip_splits:
            saip_split_ciphered_scp03t = isdp.sendData('86', saip_split, payload=True)[0]
            profile_download_payload += saip_split_ciphered_scp03t
            commands_scp_counter_mac_chaining_value.append((isdp.gp_auth_manager.m_ICV_CounterForSCP_03, isdp.gp_auth_manager.m_MAC_ChainingValue))  
        LogInfo("saipv2 split size = {} bytes, {} splits in total".format(split_size//2, len(saipv2_hexastr)//split_size))

        PowerOff()
        EUICC.init()
        capdu, audit_response2, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
        self.assertRegex(audit_response2,'A0000005591010FFFFFFFF89000018009F70010F')

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')

class con3_816(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download profile failure when device/eUICC is off')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
    
    def test_body(self):
        PowerOff()
        try:
            i=0
            for i in range(3):
                try:
                    SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
                except :
                    LogInfo('Expected to be fail')
        except :
            LogInfo("Expected to be fail")
        finally:
            EUICC.init()
            capdu, audit_response2, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
            self.assertRegex(audit_response2,'A0000005591010FFFFFFFF89000018009F70010F')

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')

class con3_1830(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download Profile retry after power failure')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

    def test_body(self):
        PowerOff()
        try:
            SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        except:
            LogInfo("Expected to be fail")

        EUICC.init()
        SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')

# class con3_2138(unittest.TestCase):

# 	def setUp(self):
# 		pass
# 		# pprint.h1('Enable Profile loaded to pre-created ISD-P in HTTPs')
# 		# aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

# 		# path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
# 		# final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RCxx_HTTP_SMS_3.1_DNS_LM_precreated_isdp.pcom') 
# 		# LogInfo(final_path)

# 		# aManager.SetStopOnError(False)
# 		# aManager.launchPCOM32(final_path)

# 	def test_body(self):
# 		EUICC.init()
# 		LogInfo('download SAIPv2 to ISD-P {} thru SCP81/SCP03t'.format(aid.short_isdp('A0000005591010FFFFFFFF8900001400')))
# 		keyset_isdp = EUICC.get_isdp_keyset_scp03t('A0000005591010FFFFFFFF890000AA00')
# 		LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))
# 		isdp = IssuerSecurityDomain_Profile('A0000005591010FFFFFFFF8900001400', keyset_isdp)
# 		apdu_open_scp03t = isdp.openDefaultSecureChannel(secureMessaging='33')
# 		profile_download_payload = apdu_open_scp03t[0] + apdu_open_scp03t[1]
# 		ram_targeted_app = aid.format_for_http_header_targeted_app('A0000005591010FFFFFFFF8900001400')
# 		saipv2_hexastr, file_size = hexastring.saipv2_to_hexastr('saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
# 		split_size = 1000
# 		saip_splits = [ saipv2_hexastr[x*split_size : x*split_size + split_size] for x in range(len(saipv2_hexastr)//split_size +1) ]

# 		commands_scp_counter_mac_chaining_value = []

# 		for saip_split in saip_splits:
# 			saip_split_ciphered_scp03t = isdp.sendData('86', saip_split, payload=True)[0]
# 			profile_download_payload += saip_split_ciphered_scp03t
# 			commands_scp_counter_mac_chaining_value.append((isdp.gp_auth_manager.m_ICV_CounterForSCP_03, isdp.gp_auth_manager.m_MAC_ChainingValue))

        
# 		LogInfo("saipv2 split size = {} bytes, {} splits in total".format(split_size//2, len(saipv2_hexastr)//split_size))
        
# 		# timer_start = time()
# 		capdu, rapdu, sw = SERVER.ram(profile_download_payload, header_targeted_app=ram_targeted_app, no_length=True)
# 		# timer_end = time()
# 		tlvs = hexastring.hexastr_to_tlvs(rapdu[4:-4]) # not considering header and tail of indefinite format
# 		LogInfo('TLVs: {}'.format(tlvs))
# 		tlvs_unwrapped = []

# 		i = 0
# 		success = False
# 		euicc_responses_array = []

# 		euicc_responses = None
        
# 		for tlv in tlvs:
# 			if tlv[0:2] == "86":
# 				if tlv[2:4] not in ('00', '08'):
# 					isdp.gp_auth_manager.m_ICV_CounterForSCP_03 = commands_scp_counter_mac_chaining_value[i][0]
# 					isdp.gp_auth_manager.m_MAC_ChainingValue = commands_scp_counter_mac_chaining_value[i][1]
# 					euicc_responses = isdp.receiveData(tlv, payload=True)[0]
# 					euicc_responses_array.append( [ tlv, isdp.receiveData(tlv, payload=True)[0] ] )
# 					success = True

# 				i += 1


#         # if tag 81 is present, status is not ok and its value indicates error origin according to the table below (82 is optionnal tag in any case)
#         # euicc_response_der = '30XXA0XX30XX'+'80XXYY'+'81XXYY'+'8201YY'

# 		saip_installation_errors_tag80 = {
# 			'00': 'ok',
# 			'01': 'pe not supported',
# 			'02': 'memory failure',
# 			'03': 'bad values',
# 			'04': 'not enough memory',
# 			'05': 'invalid request format',
# 			'06': 'invalid parameter',
# 			'07': 'runtime not supported',
# 			'08': 'lib not supported',
# 			'09': 'template not supported',
# 			'10': 'feature not supported',
# 			'31': 'unsupported profile version',
# 			'0A': 'unidentified'
# 		}

# 		# download_time = timer_end - timer_start
# 		if success and len(tlvs)>2:
# 			LogInfo('saipv2 ({} bytes) was downloaded in {:04.1f} seconds'.format(len(saipv2_hexastr)//2, download_time)) # os.path.getsize(file_static_path) does not make sense if using txt file or divide by 2
# 			LogInfo('euicc responses: {} '.format(euicc_responses_array))
# 			LogInfo("{} : {} \n\n".format( euicc_responses[12:18],saip_installation_errors_tag80[euicc_responses[16:18]] ))
# 			LogInfo('Oberthur GlobalPlatform module version: {}'.format(GlobalPlatformModuleGetVersion()))
# 			LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))

                
# 		else:
# 			LogInfo('error, saipv2 has not been installed!')
# 			if len(tlvs) <= 2:
# 				LogInfo('Oberthur GlobalPlatform module version: {}'.format(GlobalPlatformModuleGetVersion()))
# 				LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))
# 			else:
#                 # LogInfo(saip_installation_errors_tag80)
# 				LogInfo('profile element #{} is incorrect'.format(i+1))
# 				LogInfo('euicc responses: {}'.format(euicc_responses))



	# def tearDown(self):
	# 	pass
	# 	# pprint.h1('Enable Profile loaded to pre-created ISD-P in HTTPs')
		# aManager = PCOM32Manager("C:/Program Files (x86)/Oberthur Technologies/PCOM32/PCOM32.exe")

		# path_db = os.path.dirname(os.path.realpath(__file__).replace("enable_profile","first_notif"))
		# final_path = os.path.join(path_db, 'eUICC_PROFILE_III_Daimler_Glonass_RC11_HTTP_SMS_3.1_DNS_LM.pcom') 
		# LogInfo(final_path)

		# aManager.SetStopOnError(False)
		# aManager.launchPCOM32(final_path)

class con3_2158(unittest.TestCase):

	def setUp(self):
		pprint.h1('Download Profile with incorrect SCP80 (Push SMS)')
		EUICC.init()
		OTA.spi1 = 0x16
		OTA.spi2 = 0x39
		OTA.kic  = 0x12
		OTA.kid  = 0x12

		OTA.kic_key = '16161616161616161616161616161616'
		OTA.kid_key = '17171717171717171717171717171717'
		OTA.tar  = '000001'
		OTA.algo_cipher        = const.hexa.ciphers['AES']
		OTA.algo_crypto_verif  = const.hexa.ciphers['AES']

	def test_body(self):
		sw = ''
		try:
			capdu, rapdu, sw = OTA.push_sms()
		except:
			LogInfo('Error triggered as expected')

	def tearDown(self):
		pass

class con3_4010(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download Profile retry after power failure')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

    def test_body(self):
        PowerOff()
        try:
            SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        except:
            LogInfo("Expected to be fail")

        EUICC.init()
        SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')

class con3_4011(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download profile failure when device/eUICC is off')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
    
    def test_body(self):
        PowerOff()
        try:
            i=0
            for i in range(3):
                try:
                    SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
                except :
                    LogInfo('Expected to be fail')
        except :
            LogInfo("Expected to be fail")
        finally:
            EUICC.init()
            capdu, audit_response2, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
            self.assertRegex(audit_response2,'A0000005591010FFFFFFFF89000018009F70010F')

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')

class con3_4012(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download Profile failure on OPEN CHANNEL (push sms is received)')
        EUICC.init()

    def test_body(self):
        OTA.spi1 = 0x16
        OTA.spi2 = 0x39
        OTA.kic  = 0x12
        OTA.kid  = 0x12
        capdu, rapdu, sw = OTA.push_sms()
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        self.assertEqual(sw, '9113')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('810301270002028281030100240101')
        data, sw = Fetch(sw[2:4])
        data, sw = Envelope('D70C020282812401' + '01' + '022503' +  '000200')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        data, sw = Fetch(sw[2:4])
        try:
            SERVER.ram(GSMA.delete_profile('A0000005591010FFFFFFFF890000AA00'), 81, pull=True, apdu_format='indefinite', chunk='01')
        except:
            LogInfo("Expected to be failed due to unable make OPEN CHANNEL")

    
    def tearDown(self):
        EUICC.init()
        SERVER.audit_isdp_list(scp=80, apdu_format='definite')

class con3_4012(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download Profile failure on OPEN CHANNEL (push sms is received)')
        EUICC.init()

    def test_body(self):
        OTA.spi1 = 0x16
        OTA.spi2 = 0x39
        OTA.kic  = 0x12
        OTA.kid  = 0x12
        capdu, rapdu, sw = OTA.push_sms()
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        self.assertEqual(sw, '9113')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('810301270002028281030100240101')
        data, sw = Fetch(sw[2:4])
        data, sw = Envelope('D70C020282812401' + '01' + '022503' +  '000200')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        data, sw = Fetch(sw[2:4])
        sw = TerminalResponse('81030140018202828183013A')
        data, sw = Fetch(sw[2:4])
        try:
            SERVER.ram(GSMA.delete_profile('A0000005591010FFFFFFFF890000AA00'), 81, pull=True, apdu_format='indefinite', chunk='01')
        except:
            LogInfo("Expected to be failed due to unable make OPEN CHANNEL")

    
    def tearDown(self):
        EUICC.init()
        SERVER.audit_isdp_list(scp=80, apdu_format='definite')

class con3_4028(unittest.TestCase):

    def setUp(self):
        pprint.h1('Download Profile when DUT has a power failure')
        pass

    def test_body(self):
        EUICC.init()
        SERVER.audit_resource_info(scp = 80, apdu_format = 'indefinite') 
        SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001800", scp=81, apdu_format='definite')
        keyset_isdp = EUICC.get_isdp_keyset_scp03t('A0000005591010FFFFFFFF8900001800')
        LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))
        isdp = IssuerSecurityDomain_Profile('A0000005591010FFFFFFFF8900001800', keyset_isdp)
        apdu_open_scp03t = isdp.openDefaultSecureChannel(secureMessaging='33')
        profile_download_payload = apdu_open_scp03t[0] + apdu_open_scp03t[1]
        ram_targeted_app = aid.format_for_http_header_targeted_app('A0000005591010FFFFFFFF8900001800')
        
        saipv2_hexastr, file_size = hexastring.saipv2_to_hexastr('saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
        split_size = 1000
        saip_splits = [ saipv2_hexastr[x*split_size : x*split_size + split_size] for x in range(len(saipv2_hexastr)//split_size +1) ]
        commands_scp_counter_mac_chaining_value = []
        
        for saip_split in saip_splits:
            saip_split_ciphered_scp03t = isdp.sendData('86', saip_split, payload=True)[0]
            profile_download_payload += saip_split_ciphered_scp03t
            commands_scp_counter_mac_chaining_value.append((isdp.gp_auth_manager.m_ICV_CounterForSCP_03, isdp.gp_auth_manager.m_MAC_ChainingValue))  
        LogInfo("saipv2 split size = {} bytes, {} splits in total".format(split_size//2, len(saipv2_hexastr)//split_size))

        PowerOff()
        EUICC.init()
        capdu, audit_response2, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
        self.assertRegex(audit_response2,'A0000005591010FFFFFFFF89000018009F70010F')

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')


def suite():
    suite = unittest.TestSuite()
    # suite.addTest(unittest.makeSuite(con3_693, 'test'))
    # suite.addTest(unittest.makeSuite(con3_4011, 'test'))
    # suite.addTest(unittest.makeSuite(con3_4028, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()