import sys,os.path, configparser
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Mobile import *
from util import *
import unittest
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'


EUICC 		= model.Euicc()
SERVER		= model.Smsr()


CAPDU = None
RAPDU = None
SW = None

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')

class DeleteProfile(unittest.TestCase):

    def setUp(self):
        pprint.h1('Delete Profile with non existing ISD-P')
        EUICC.init()

    def test_delete_profile_con3_735(self):
        global CAPDU, SW
        CAPDU, data, SW = SERVER.delete_profile('A0000005591010FFFFFFFF890000AA00', scp=80, apdu_format='definite', chunk='01')
        self.assertRegex(data, '6A88')
    
    def tearDown(self):
        pass

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(DeleteProfile, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()