import sys,os.path, configparser
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Ot.PCOM32Manager import *
from Mobile import *
from util import *
import unittest
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'

EUICC 		= model.Euicc()
SERVER		= model.Smsr()

CAPDU = None
SW = None

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')


class EnableProfile(unittest.TestCase):

    def setUp(self):
        pprint.h1('Enable via HTTP profile with non existing ISD-P')
        EUICC.init()

    def test_enable_profile_con3_714(self):
        global CAPDU, SW
        CAPDU, response_audit, SW = SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF890000AA00', scp=81, network_service=False, apdu_format='definite', chunk='01')
        
        self.assertRegex(response_audit,'6A88', msg = 'Expected result not match')

    def tearDown(self):
        pass

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(EnableProfile, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()