import sys,os.path, configparser
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Ot.PCOM32Manager import *
from Mobile import *
from util import *
import unittest
import model


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'


EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA        = model.Gsma()


CAPDU = None
RAPDU = None
SW = None

displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')

class DisableProfile(unittest.TestCase):

    def setUp(self):
        pprint.h1('Disable profile when the device/eUICC is off')
        EUICC.init()
        SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite')
        SERVER.download_profile('A0000005591010FFFFFFFF8900001800', saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")
        EUICC.init()
        SERVER.enable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=81, network_service=False, apdu_format='definite', chunk='01')
        capdu, audit_response, sw = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
    
    def test_disable_profile_con3_875(self):
        global CAPDU, SW
        PowerOff()
        try:
            i=0
            for i in range(3):
                try:
                    CAPDU, rapdu, SW = SERVER.ram(GSMA.disable_profile('A0000005591010FFFFFFFF8900001800'), 81, apdu_format='indefinite', chunk='01')
                except :
                    LogInfo('Expected to be fail')
        except :
            LogInfo("Expected to be fail")
        finally:
            EUICC.init()
            CAPDU, audit_response2, SW = SERVER.ram(GSMA.audit_isdp_list, scp=80, apdu_format='definite')
            self.assertRegex(audit_response2,'A0000005591010FFFFFFFF89000018009F70013F')

    def tearDown(self):
        EUICC.init()
        SERVER.disable_profile(aid_isdp='A0000005591010FFFFFFFF8900001800', scp=80, network_service=False, apdu_format='definite', chunk='01')
        EUICC.init()
        SERVER.delete_profile('A0000005591010FFFFFFFF8900001800', scp=80, apdu_format='definite', chunk='01')
        SendAPDU('80F2000C00')


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(DisableProfile, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()