import sys,os.path, configparser
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Mobile import *
from util import *
import unittest
import model

# from test_case.e2e.test_dir import main_execution_script


__author__		= 'Muhammad Hasbi Firmansyah'
__maintainer__	= 'Muhammad Hasbi Firmansyah'
__status__		= 'dev'
__version__		= '0.1.0'
__email__		= 'm.firmansyah@oberthur.com'


EUICC 		= model.Euicc()
SERVER		= model.Smsr()


CAPDU = None
RAPDU = None
SW = None


displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')

class DownloadProfile(unittest.TestCase):

    # def __init__(self, isdp='A0000005591010FFFFFFFF8900001900', saip_name='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'):
    #     self.isdp           = isdp
    #     self.saip_name      = saip_name 

    def setUp(self):
        pprint.h1('Download Profile (with update SMSR address) to non existing ISD-P {}'.format(Constants.ISDP))
        pass 
    def test_download_profile_con3_693(self):
        EUICC.init()
        try:
            download_time, euicc_responses, file_size = SERVER.download_profile(Constants.ISDP, Constants.SAIP_NAME)
        except :
            LogInfo('Error triggered as expected')

    def tearDown(self):
        pass

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(DownloadProfile, 'test'))
    return suite

if __name__ == '__main__':
    unittest.main()