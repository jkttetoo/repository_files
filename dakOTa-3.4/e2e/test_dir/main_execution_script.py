import sys,os.path, configparser
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Mobile import *
from util import *
import unittest
import model
from util.pprint import border
from util import pprint, aid, color


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Constants.ISDP = 'A0000005591010FFFFFFFF8900001900'
Constants.SAIP_NAME = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'


from test_case.e2e.test_dir import nom_neg_0693_download_profile_to_non_existing_isdp
# from test_case.e2e.test_dir import nom_neg_0714_enable_via_http_non_existing_isdp
# from test_case.e2e.test_dir import nom_neg_0735_delete_profile_to_non_existing_isdp
# from test_case.e2e.test_dir import nom_neg_0875_disable_profile_when_eUICC_off

suite = unittest.TestSuite()
suite.addTest(nom_neg_0693_download_profile_to_non_existing_isdp.suite())
# suite.addTest(nom_neg_0714_enable_via_http_non_existing_isdp.suite())
# suite.addTest(nom_neg_0735_delete_profile_to_non_existing_isdp.suite())
# suite.addTest(nom_neg_0875_disable_profile_when_eUICC_off.suite())
unittest.TextTestRunner().run(suite)