import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain
from Ot.GlobalPlatform import getVersion as GlobalPlatformModuleGetVersion
from util import pprint, aid, hexastring

from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model



__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER = model.Smsr()
EUICC = model.Euicc()
DEVICE = model.Device()
OTA = model.SCP80()


displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

'''


'''


EUICC.init()

# SERVER.download_profile("A0000005591010FFFFFFFF8900001500", saipv2_txt="saipv2_SD_NOPIN_SCP81_notoken_Expanded.der")

# LogInfo('download SAIPv2 to ISD-P {} thru SCP81/SCP03t'.format(aid.short_isdp(aid_isdp)))
keyset_isdp = EUICC.get_isdp_keyset_scp03t('A0000005591010FFFFFFFF8900001400')
# keyset_isdp.getSequenceNumber()
# keyset_isdp.incrementSequenceCounter()
keyset_isdp.incrementSequenceCounter(0x06)
LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))
# LogInfo('>> current sequence number {}'.format(keyset_isdp))
isdp = IssuerSecurityDomain_Profile('A0000005591010FFFFFFFF8900001400', keyset_isdp)
apdu_open_scp03t = isdp.openDefaultSecureChannel(secureMessaging='33')
profile_download_payload = apdu_open_scp03t[0] + apdu_open_scp03t[1]
ram_targeted_app = aid.format_for_http_header_targeted_app('A0000005591010FFFFFFFF8900001400')
saipv2_hexastr, file_size = hexastring.saipv2_to_hexastr('saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
split_size = 1000
saip_splits = [ saipv2_hexastr[x*split_size : x*split_size + split_size] for x in range(len(saipv2_hexastr)//split_size +1) ]

commands_scp_counter_mac_chaining_value = []

for saip_split in saip_splits:
	saip_split_ciphered_scp03t = isdp.sendData('86', saip_split, payload=True)[0]
	profile_download_payload += saip_split_ciphered_scp03t
	commands_scp_counter_mac_chaining_value.append((isdp.gp_auth_manager.m_ICV_CounterForSCP_03, isdp.gp_auth_manager.m_MAC_ChainingValue))

        
LogInfo("saipv2 split size = {} bytes, {} splits in total".format(split_size//2, len(saipv2_hexastr)//split_size))
        
timer_start = time()
capdu, rapdu, sw = SERVER.ram(profile_download_payload, header_targeted_app=ram_targeted_app, no_length=True)
timer_end = time()
tlvs = hexastring.hexastr_to_tlvs(rapdu[4:-4]) # not considering header and tail of indefinite format
LogInfo('TLVs: {}'.format(tlvs))
tlvs_unwrapped = []

i = 0
success = False
euicc_responses_array = []

euicc_responses = None
        
for tlv in tlvs:
	if tlv[0:2] == "86":
		if tlv[2:4] not in ('00', '08'):
			isdp.gp_auth_manager.m_ICV_CounterForSCP_03 = commands_scp_counter_mac_chaining_value[i][0]
			isdp.gp_auth_manager.m_MAC_ChainingValue = commands_scp_counter_mac_chaining_value[i][1]
			euicc_responses = isdp.receiveData(tlv, payload=True)[0]
			euicc_responses_array.append( [ tlv, isdp.receiveData(tlv, payload=True)[0] ] )
			success = True
		i += 1


# if tag 81 is present, status is not ok and its value indicates error origin according to the table below (82 is optionnal tag in any case)
# euicc_response_der = '30XXA0XX30XX'+'80XXYY'+'81XXYY'+'8201YY'

saip_installation_errors_tag80 = {
	'00': 'ok',
	'01': 'pe not supported',
	'02': 'memory failure',
	'03': 'bad values',
	'04': 'not enough memory',
	'05': 'invalid request format',
	'06': 'invalid parameter',
	'07': 'runtime not supported',
	'08': 'lib not supported',
	'09': 'template not supported',
	'0A': 'feature not supported',
	'0B': 'pin code missing',
	'1F': 'unsupported profile version'
    }

download_time = timer_end - timer_start
if success and len(tlvs)>2:
	LogInfo('saipv2 ({} bytes) was downloaded in {:04.1f} seconds'.format(len(saipv2_hexastr)//2, download_time)) # os.path.getsize(file_static_path) does not make sense if using txt file or divide by 2
	LogInfo('euicc responses: {} '.format(euicc_responses_array))
	LogInfo("{} : {} \n\n".format( euicc_responses[12:18],saip_installation_errors_tag80[euicc_responses[16:18]] ))

else:
	LogInfo('error, saipv2 has not been installed!')
	if len(tlvs) <= 2:
		LogInfo('Oberthur GlobalPlatform module version: {}'.format(GlobalPlatformModuleGetVersion()))
		LogInfo('ISD-P SCP03t keyset value before operation: {}'.format(keyset_isdp))
	else:
		LogInfo(saip_installation_errors_tag80)
		LogInfo('profile element #{} is incorrect'.format(i+1))
		LogInfo('euicc responses: {}'.format(euicc_responses))