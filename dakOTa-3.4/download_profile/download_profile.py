import sys
from Oberthur import *
sys.path.insert(0, "../../lib")
sys.path.insert(0, "../../")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER 		= model.Smsr()
EUICC 		= model.Euicc()
DEVICE 		= model.Device()
INGENICO 	= model.IngenicoReader()

displayAPDU(True)
SetLogLevel('info')
# print(GetAllReaders())
# SetCurrentReader('DakOTaV31SLE97PC413872SecureSprint1 Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('DakOTaV31SLE97PC41387XSecureRC8 Reader')
# SetCurrentReader('eSIM 3.1.rc9')
# SetCurrentReader('eSIM.3.1.rc10')
# SetCurrentReader('rc9_coba')


list_profile_from_david = (
	# 'saipv2_8900037037000000001_Fido_LTE_Dummy_v4.txt',
	# 'saipv2_applet_CU_profilev8_1_10_RFMUSIM_dummy_v4.txt',
	# 'saipv2_applet_OFR_ESv1_2_v1.8.txt',
	# 'saipv2_applet_rogers_eUICC_v6.txt',
	# 'saipv2_applet_SAIP_OPTUS_Postpaid_Sample_Dummy_v5.txt',
	'saipv2_applet_SprintSIMOTA-noFactoryReset_final_v3_rc10_[new].txt',
	# 'saipv2_applet_SprintUsimLite-eSIM_v3_edited.txt',
	# 'saipv2_applet_TransatelMCCmodif_DERTLV_04_NoAmdBwithDependancies_DER_3G_V3.txt',
	# 'saipv2_ClaroAr_Card01_SAIPv2_DERTLV_test02_Mconnect5.txt',
	# 'saipv2_createdWithoutApplets_DTAG_89490200001130195467_DERTLV_ManModif_v4.txt',
	# 'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_hash_no3G_v2.txt',
	# 'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_no_hash_no3G_v2.txt',
	# 'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_v10.txt',
	# 'saipv2_DAVID_EMT_eSIM_c081_c084_ver11.txt',
	# 'saipv2_e_gsm_sse_0900_d7g271_testep_samsung_odc_step2_multipass_v42-Edited_EDITED.txt',
	# 'saipv2_EMT_eSIM_c081_c084_ver2_MT_big150k_v2.txt',
	# 'saipv2_EMT_eSIM_c081_c084_ver2_MT_big300k_v2.txt',
	# 'saipv2_line_6_89883011659830093954_version3_derTLV_v9.txt',
	# 'saipv2_misft_ewa_dummy_v5.txt',
	# 'saipv2_MSFT_v1.0_BLA_2.4_89019900012340002096_v2.txt',
	'saipv2_ProfilePackage_m2m_Sequence_RemoveDFPhoneBook_v1.txt',
	# 'saipv2_ProfilePackageDescription_Basic_AES_v2_original_scp03keyset_DASE_corrected_v3.txt',
	# 'saipv2_rogers7_dummy_v2.txt',
	# 'saipv2_SprintUsimLite-eSIM_V4_edited.txt',
	# 'saipv2_TransatelMCCmodif_DERTLV_04_NoAmdBwithDependancies_DER_3G_v2_edited.txt',
	'saipv2_applet_profileOnly_15_HEXA_V1F_dummy_v3_rc10_edited (2).txt',
	'saipv2_ASN1_output_eSIM_project_test_card_V4_edited_[new].der',
	'saipv2_GD170130_KRSMSG021882V2.00_TestData.noTemplates.ups_der_RC10_edited_noStoreData.txt',
	'saipv2_GD170130_KRSMSG021882V2.00_TestData.Templates.ups_der_rc10_edited_noStoreData_[new].txt'
	# 'saipv2_EMT_eSIM_c081_c084_ver2_MT_big300k_DERTLVmanModv_v8.txt',
	)





all_profiles = list_profile_from_david 

result = {
	'list_profile' 	: ['Profile Name'],
	'repsonse' 		: ['Response'],
	'meaning'		: ['Meaning'],
	'download_time' : ['Time (s)'],
	'file_size'		: ['File Size']
		}


saip_installation_errors_tag80 = {
	'FF': 'unidentified',
	'00': 'ok',
	'01': 'pe not supported',
	'02': 'memory failure',
	'03': 'bad values',
	'04': 'not enough memory',
	'05': 'invalid request format',
	'06': 'invalid parameter',
	'07': 'runtime not supported',
	'08': 'lib not supported',
	'09': 'template not supported',
	'0A': 'feature not supported',
	'0B': 'pin code missing',
	'1F': 'unsupported profile version'
}



##### change this variable to select what group of profiles you want to download

list_profile = all_profiles

# ########################################################################################

for profile in list_profile:
	print(profile)

	EUICC.init()
	pprint.h1("Create ISDP {}".format(profile))
	SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001900", scp=80, apdu_format='definite')
	pprint.h1("Download Profile {}".format(profile))
	download_time, euicc_response, file_size = SERVER.download_profile("A0000005591010FFFFFFFF8900001900", saipv2_txt=profile)
	pprint.h1("Delete Profile {}".format(profile))
	SERVER.delete_profile("A0000005591010FFFFFFFF8900001900", scp=80, apdu_format='definite')

	SendAPDU('80F2 00 0C 00')

	if euicc_response==None:
		euicc_response = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'

	file_size = file_size/1000
	result['list_profile'].append(profile)
	result['repsonse'].append(euicc_response)
	result['meaning'].append(saip_installation_errors_tag80[euicc_response[16:18]])						
	result['download_time'].append('{:04.1f}'.format(download_time))
	result['file_size'].append('{} KB '.format(file_size))
	pprint.table( [result['list_profile'], result['repsonse'], result['meaning'], result['download_time'], result['file_size']] )

pprint.table( [result['list_profile'], result['repsonse'], result['meaning'], result['download_time'], result['file_size']] )

# profile = 'saipv2_ASN1_output_eSIM_project_test_card No1_89490200001130195475_v2VS_updateTextString.der'

# EUICC.init()

# pprint.h1("Delete Profile {}".format(profile))
# SERVER.delete_profile("A0000005591010FFFFFFFF8900001300", scp=80, apdu_format='definite')

# pprint.h1("Create ISDP {}".format(profile))
# SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001300", scp=80, apdu_format='definite')
# pprint.h1("Download Profile {}".format(profile))
# download_time, euicc_response, file_size = SERVER.download_profile("A0000005591010FFFFFFFF8900001300", saipv2_txt=profile)