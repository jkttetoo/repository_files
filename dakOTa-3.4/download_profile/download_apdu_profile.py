import sys
from Oberthur import *
sys.path.insert(0, "../../lib")
sys.path.insert(0, "../../")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER 		= model.Smsr()
EUICC 		= model.Euicc()
DEVICE 		= model.Device()
INGENICO 	= model.IngenicoReader()

displayAPDU(False)
SetLogLevel('info')
# print(GetAllReaders())
# SetCurrentReader('DakOTaV31SLE97PC413872SecureSprint1 Reader')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('DakOTaV31SLE97PC41387XSecureRC8 Reader')
# SetCurrentReader('eSIM 3.1.rc9')
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('rc9_coba')


list_profile_from_david = (
	'AF09_8910390000001168129F_HTTP_OTA.cmd',
	'AF09_8910390000001168129F_SMS_OTA.cmd'
	#'saipv2_EMT_eSIM_c081_c084_ver2_MT_big300k_DERTLVmanModv_v8.txt',
	)





all_profiles = list_profile_from_david 

result = {
	'list_profile' 	: ['Profile Name'],
	'repsonse' 		: ['Response'],
	'meaning'		: ['Meaning'],
	'download_time' : ['Time (s)'],
	'file_size'		: ['File Size']
		}


saip_installation_errors_tag80 = {
	'FF': 'unidentified',
	'00': 'ok',
	'01': 'pe not supported',
	'02': 'memory failure',
	'03': 'bad values',
	'04': 'not enough memory',
	'05': 'invalid request format',
	'06': 'invalid parameter',
	'07': 'runtime not supported',
	'08': 'lib not supported',
	'09': 'template not supported',
	'0A': 'feature not supported',
	'0B': 'pin code missing',
	'1F': 'unsupported profile version'
}



##### change this variable to select what group of profiles you want to download

list_profile = all_profiles

# ########################################################################################

# for profile in list_profile:
# 	print('current profile to be downloaded : {}'.format(profile))

	# EUICC.init()
	# pprint.h1("Create ISDP {}".format(profile))
	# SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001900", scp=80, apdu_format='definite')
	# pprint.h1("Download Profile {}".format(profile))
	# SERVER.download_apdu_profile("A0000005591010FFFFFFFF8900001900", perso_txt=profile)
# 	EUICC.init()
# 	pprint.h1("Delete Profile {}".format(profile))
# 	SERVER.delete_profile("A0000005591010FFFFFFFF8900001900", scp=80, apdu_format='definite')

# 	SendAPDU('80F2 00 0C 00')


EUICC.init()
# pprint.h1("Create ISDP {}".format(list_profile[0]))
# SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001300", scp=80, apdu_format='definite')
pprint.h1("Download Profile {}".format(list_profile[0]))
SERVER.download_apdu_profile("A0000005591010FFFFFFFF8900001300", perso_txt=list_profile[0])
# EUICC.init()
# pprint.h1("Delete Profile {}".format(list_profile[0]))
# SERVER.delete_profile("A0000005591010FFFFFFFF8900001300", scp=80, apdu_format='definite')

# SendAPDU('80F2 00 0C 00')

# EUICC.init()
# pprint.h1("Create ISDP {}".format(list_profile[1]))
# SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001600", scp=80, apdu_format='definite')
# pprint.h1("Download Profile {}".format(list_profile[1]))
# SERVER.download_apdu_profile("A0000005591010FFFFFFFF8900001600", perso_txt=list_profile[0])
# EUICC.init()
# pprint.h1("Delete Profile {}".format(list_profile[1]))
# SERVER.delete_profile("A0000005591010FFFFFFFF8900001600", scp=80, apdu_format='definite')

# SendAPDU('80F2 00 0C 00')