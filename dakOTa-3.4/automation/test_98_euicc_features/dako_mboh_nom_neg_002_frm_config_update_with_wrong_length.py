import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
from time import time
import unittest, json, collections
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER 	= model.Smsr()
EUICC 	= model.Euicc()
GSMA	= model.Gsma()

SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('eSIM 3.1.rc11')

EUICC.init()
SERVER.ram('80E2880005DF6D03160101', scp=80, apdu_format='definite')
