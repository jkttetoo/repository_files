from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest
import model
from time import time



__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()

		EXTERNAL_ID             = 'DK-4493'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()

		EXTERNAL_ID             = 'DK-4493'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None		

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'
		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4493'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = True
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Dako-4493 Audit OS Information"
# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)_debugging'
# expRapdu = "6E0956332E322E532E3136" #sprint 16
# expRapdu = "6E0956332E322E532E3137" #sprint 17
# expRapdu = "6E0956332E322E532E3138" #sprint 18
# expRapdu = "6E0A56332E322E312E532E31" #dakota 3.2.1 sprint 1
# expRapdu = "6E0A56332E322E312E532E32" #dakota 3.2.1 sprint 2
# expRapdu = "6E0856332E342E532E33" #dakota 3.2.1 sprint 2 
# expRapdu = "6E0956332E342E532E3135" #V3.4.S.15
# expRapdu = "6E0B56332E342E532E31352E31" #V3.4.S.15.1
# expRapdu = "6E0B444B3334503753322E7232" #DK34P7S2.r2
# expRapdu = "503353327236" #DK34P7S2.r2
expRapdu = "444B4E4711503453317243" #DKNG_P4S1rC

class AuditOs(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None	

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			# LogInfo("EXTERNAL_ID: "+str(EXTERNAL_ID))
			# LogInfo("Constants.TESTLINK_PROJECT_NAME: "+str(Constants.TESTLINK_PROJECT_NAME))
			# LogInfo("Constants.TESTLINK_TEST_PLAN: "+str(Constants.TESTLINK_TEST_PLAN))
			# LogInfo("Constants.TESTLINK_BUILD: "+str(Constants.TESTLINK_BUILD))
			# LogInfo("Constants.TESTLINK_TESTER: "+str(Constants.TESTLINK_TESTER))
			# LogInfo("execution_status: "+str(execution_status))
			# LogInfo("Constants.TESTLINK_PLATFORM: "+str(Constants.TESTLINK_PLATFORM))
			# LogInfo("execduration: "+str(execduration))			
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)          

	def setUp(self):
		pprint.h1(testName)
		h2("setup")

	def test_audit_os(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			EUICC.init()			
			capdu, rapdu, sw = SERVER.audit_os_version(scp=81, apdu_format='definite')
			LogInfo("rapdu: "+str(rapdu))
			if expRapdu in rapdu:
				LogInfo("right os version")
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				LogInfo("wrong os version")
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
		except:
			exceptionTraceback(position = 2)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start
#        self.assertEqual(Constants.RESULTS['test_result'][Constants.case], 'OK')
		# LogInfo('Constants.RESULTS table')
		# pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')


	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
		except:
			exceptionTraceback(position = 3)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(AuditOs)