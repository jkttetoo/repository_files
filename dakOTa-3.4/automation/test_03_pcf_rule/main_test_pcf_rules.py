import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


EUICC = model.Euicc()


displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())
# SetCurrentReader('eSIM 3.1.rc11')


'''
1. Set fallback attribut on profile 10
2. FRM status ON

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
=====================================

'''

Constants.AID_ISDP				= 'A0000005591010FFFFFFFF8900001500'
Constants.AID_DEFAULT			= 'A0000005591010FFFFFFFF8900001100'
Constants.SAIP_TEST				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_PCF00			= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01			= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02			= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04			= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
Constants.RESULTS 				= {
									'test_name' 		: ['Test name'],
									'test_result'		: ['Test result']
									}
Constants.TESTLINK_REPORT		= True
Constants.TESTLINK_DEVKEY		= 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL			= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME	= 'DakOTa v3.1'
Constants.TESTLINK_BUILD		= 'DakOTa v3.2 Build4Test4'
Constants.TESTLINK_PLATFORM		= 'IO222'
Constants.TESTLINK_TEST_PLAN	= 'eUICC Pre-integration'
Constants.TESTLINK_TESTER 		= 'm.firmansyah@oberthur.com'


from test_case.automation.test_03_pcf_rule import pcf00_enable_profile
from test_case.automation.test_03_pcf_rule import pcf00_disable_profile
from test_case.automation.test_03_pcf_rule import pcf00_delete_profile
from test_case.automation.test_03_pcf_rule import pcf00_network_attachment_loss

from test_case.automation.test_03_pcf_rule import pcf01_enable_profile
from test_case.automation.test_03_pcf_rule import pcf01_disable_profile
from test_case.automation.test_03_pcf_rule import pcf01_delete_profile
from test_case.automation.test_03_pcf_rule import pcf01_network_attachment_loss

from test_case.automation.test_03_pcf_rule import pcf02_enable_profile
from test_case.automation.test_03_pcf_rule import pcf02_disable_profile
from test_case.automation.test_03_pcf_rule import pcf02_delete_profile
from test_case.automation.test_03_pcf_rule import pcf02_network_attachment_loss

from test_case.automation.test_03_pcf_rule import pcf04_enable_profile
from test_case.automation.test_03_pcf_rule import pcf04_disable_profile
from test_case.automation.test_03_pcf_rule import pcf04_delete_profile
from test_case.automation.test_03_pcf_rule import pcf04_network_attachment_loss

suite = unittest.TestSuite()

EUICC.init()

''' PCF 00: no POL1 rule active  '''
suite.addTest(pcf00_enable_profile.suite())
# suite.addTest(pcf00_disable_profile.suite())
# suite.addTest(pcf00_delete_profile.suite())
# suite.addTest(pcf00_network_attachment_loss.suite())

# ''' PCF 01: Disabling of this profile not allowed  '''
# suite.addTest(pcf01_enable_profile.suite())
# suite.addTest(pcf01_disable_profile.suite())
# suite.addTest(pcf01_delete_profile.suite())
# suite.addTest(pcf01_network_attachment_loss.suite())

# ''' PCF 02: Disabling of this profile not allowed  '''
# suite.addTest(pcf02_enable_profile.suite())
# suite.addTest(pcf02_disable_profile.suite())
# suite.addTest(pcf02_delete_profile.suite())
# suite.addTest(pcf02_network_attachment_loss.suite())

# ''' PCF 04: Profile deletion is mandatory when its state is changed to disabled  '''
# suite.addTest(pcf04_enable_profile.suite())
# suite.addTest(pcf04_disable_profile.suite())
# suite.addTest(pcf04_delete_profile.suite())
# suite.addTest(pcf04_network_attachment_loss.suite())


unittest.TextTestRunner().run(suite)
pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )