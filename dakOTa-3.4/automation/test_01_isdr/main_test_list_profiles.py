import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


EUICC = model.Euicc()


displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
# SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')

'''
1. Set fallback attribut on profile 10
2. FRM status ON

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
=====================================

'''

Constants.AID_ISDP 				= 'A0000005591010FFFFFFFF8900001500'
Constants.DEFAULT_ISDP10		= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11		= 'A0000005591010FFFFFFFF8900001100'
Constants.SAIP_TEST 			= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.RESULTS 				= {
									'test_name' 		: ['Test name'],
									'test_result'		: ['Test result']
									}
Constants.TESTLINK_REPORT		= True
Constants.TESTLINK_DEVKEY		= 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL			= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME	= 'DakOTa v3.1'
Constants.TESTLINK_BUILD		= 'DakOTa v3.2 Build4Test4'
Constants.TESTLINK_PLATFORM		= 'IO222'
Constants.TESTLINK_TEST_PLAN	= 'eUICC Pre-integration'
Constants.TESTLINK_TESTER 		= 'm.firmansyah@oberthur.com'



from test_case.automation.test_01_isdr import scp80_list_profiles_compact
from test_case.automation.test_01_isdr import scp80_list_profiles_definite
from test_case.automation.test_01_isdr import scp80_list_profiles_indefinite
from test_case.automation.test_01_isdr import scp81_list_profiles_definite
from test_case.automation.test_01_isdr import scp81_list_profiles_indefinite

from test_case.automation.test_01_isdr import scp80_list_resources_compact
from test_case.automation.test_01_isdr import scp80_list_resources_definite
from test_case.automation.test_01_isdr import scp80_list_resources_indefinite
from test_case.automation.test_01_isdr import scp81_list_resources_definite
from test_case.automation.test_01_isdr import scp81_list_resources_indefinite

from test_case.automation.test_01_isdr import scp80_enable_profile_compact
from test_case.automation.test_01_isdr import scp80_enable_profile_definite
from test_case.automation.test_01_isdr import scp80_enable_profile_indefinite
from test_case.automation.test_01_isdr import scp81_enable_profile_definite
from test_case.automation.test_01_isdr import scp81_enable_profile_indefinite

from test_case.automation.test_01_isdr import scp80_disable_profile_compact
from test_case.automation.test_01_isdr import scp80_disable_profile_definite
from test_case.automation.test_01_isdr import scp80_disable_profile_indefinite
from test_case.automation.test_01_isdr import scp81_disable_profile_definite
from test_case.automation.test_01_isdr import scp81_disable_profile_indefinite

from test_case.automation.test_01_isdr import scp80_create_isdp_definite
from test_case.automation.test_01_isdr import scp80_create_isdp_indefinite
from test_case.automation.test_01_isdr import scp81_create_isdp_definite
from test_case.automation.test_01_isdr import scp81_create_isdp_indefinite

from test_case.automation.test_01_isdr import scp81_download_profile_definite
from test_case.automation.test_01_isdr import scp81_download_profile_indefinite

from test_case.automation.test_01_isdr import scp80_set_fallback_enable_profile_compact
from test_case.automation.test_01_isdr import scp80_set_fallback_enable_profile_definite
from test_case.automation.test_01_isdr import scp80_set_fallback_enable_profile_indefinite
from test_case.automation.test_01_isdr import scp81_set_fallback_enable_profile_definite
from test_case.automation.test_01_isdr import scp81_set_fallback_enable_profile_indefinite

from test_case.automation.test_01_isdr import scp80_set_fallback_disabled_profile_compact
from test_case.automation.test_01_isdr import scp80_set_fallback_disabled_profile_definite
from test_case.automation.test_01_isdr import scp80_set_fallback_disabled_profile_indefinite
from test_case.automation.test_01_isdr import scp81_set_fallback_disabled_profile_definite
from test_case.automation.test_01_isdr import scp81_set_fallback_disabled_profile_indefinite

from test_case.automation.test_01_isdr import scp80_delete_profile_compact
from test_case.automation.test_01_isdr import scp80_delete_profile_definite
from test_case.automation.test_01_isdr import scp80_delete_profile_indefinite
from test_case.automation.test_01_isdr import scp81_delete_profile_definite
from test_case.automation.test_01_isdr import scp81_delete_profile_indefinite

from test_case.automation.test_01_isdr import scp80_update_smsr_compact
from test_case.automation.test_01_isdr import scp80_update_smsr_definite
from test_case.automation.test_01_isdr import scp80_update_smsr_indefinite
from test_case.automation.test_01_isdr import scp81_update_smsr_definite
from test_case.automation.test_01_isdr import scp81_update_smsr_indefinite



suite = unittest.TestSuite()

''' List profiles '''
test1 = suite.addTest(scp80_list_profiles_compact.suite())
suite.addTest(scp80_list_profiles_definite.suite())
suite.addTest(scp80_list_profiles_indefinite.suite())
suite.addTest(scp81_list_profiles_definite.suite())
suite.addTest(scp81_list_profiles_indefinite.suite())

''' List resources '''
suite.addTest(scp80_list_resources_compact.suite())
suite.addTest(scp80_list_resources_definite.suite())
suite.addTest(scp80_list_resources_indefinite.suite())
suite.addTest(scp81_list_resources_definite.suite())
suite.addTest(scp81_list_resources_indefinite.suite())

''' Enable proflie '''
suite.addTest(scp80_enable_profile_compact.suite())
suite.addTest(scp80_enable_profile_definite.suite())
suite.addTest(scp80_enable_profile_indefinite.suite())
suite.addTest(scp81_enable_profile_definite.suite())
suite.addTest(scp81_enable_profile_indefinite.suite())

''' Disable proflie '''
suite.addTest(scp80_disable_profile_compact.suite())
suite.addTest(scp80_disable_profile_definite.suite())
suite.addTest(scp80_disable_profile_indefinite.suite())
suite.addTest(scp81_disable_profile_definite.suite())
suite.addTest(scp81_disable_profile_indefinite.suite())

''' Create ISD-P '''
suite.addTest(scp80_create_isdp_definite.suite())
suite.addTest(scp80_create_isdp_indefinite.suite())
suite.addTest(scp81_create_isdp_definite.suite())
suite.addTest(scp81_create_isdp_indefinite.suite())

''' Download profile '''
suite.addTest(scp81_download_profile_definite.suite())
suite.addTest(scp81_download_profile_indefinite.suite())

''' Set-fallback on enabled profile '''
suite.addTest(scp80_set_fallback_enable_profile_compact.suite())
suite.addTest(scp80_set_fallback_enable_profile_definite.suite())
suite.addTest(scp80_set_fallback_enable_profile_indefinite.suite())
suite.addTest(scp81_set_fallback_enable_profile_definite.suite())
suite.addTest(scp81_set_fallback_enable_profile_indefinite.suite())

''' Set-fallback on disabled profile '''
suite.addTest(scp80_set_fallback_disabled_profile_compact.suite())
suite.addTest(scp80_set_fallback_disabled_profile_definite.suite())
suite.addTest(scp80_set_fallback_disabled_profile_indefinite.suite())
suite.addTest(scp81_set_fallback_disabled_profile_definite.suite())
suite.addTest(scp81_set_fallback_disabled_profile_indefinite.suite())

''' Delete profile '''
suite.addTest(scp80_delete_profile_compact.suite())
suite.addTest(scp80_delete_profile_definite.suite())
suite.addTest(scp80_delete_profile_indefinite.suite())
suite.addTest(scp81_delete_profile_definite.suite())
suite.addTest(scp81_delete_profile_indefinite.suite())

''' Update SMSR '''
suite.addTest(scp80_update_smsr_compact.suite())
suite.addTest(scp80_update_smsr_definite.suite())
suite.addTest(scp80_update_smsr_indefinite.suite())
suite.addTest(scp81_update_smsr_definite.suite())
suite.addTest(scp81_update_smsr_indefinite.suite())

unittest.TextTestRunner().run(suite)
pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
