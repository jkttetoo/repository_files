import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)
			
try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr() 
		EUICC                   = model.Euicc()
		LOCAL_SWAP				= model.LocalSwap()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		
		EXTERNAL_ID             = 'DK-4428'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		LOCAL_SWAP				= model.LocalSwap()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		
		EXTERNAL_ID             = 'DK-4428'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
		
		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')
		
		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'
		
		'''
		TEST VARIABLES
		
		To launch the test, prepare the following variables : 
		
		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 
		
		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	LOCAL_SWAP				= model.LocalSwap()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	
	EXTERNAL_ID             = 'DK-4428'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None
	
	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')
	
	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'
	
	'''
	TEST VARIABLES
	
	To launch the test, prepare the following variables : 
	
	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 
	
	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

'''
Pre-conditions:
1. Create and Download ISDP 15
2. Set ISDP 15 as Factory profile

Test flow:
1. EUICC.init
2. Enable ISDP 15 via GSMA command
3. Enable should return 6985 (or any sw to indicate that process is not allowed)

Tear down:
1. Enable ISDP 11
2. Delete ISDP 15

'''

testName = "Factory Profile cant be enabled via GSMA"
class FactoryProfileCantBeEnabledViaGSMA(OtTestUnit):

	def report(self, execution_status, execduration):
		# LogInfo('Start reporting test result...')
		# TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		# LogInfo('Finished')
		pass

	def setUp(self):
		pprint.h1('Factory Profile can\'t be enabled via GSMA')
		pprint.h2("setup")
		EUICC.init()
		h3('Create new ISDP for Factory Profile')
		SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001500", scp=80, apdu_format='definite')
		SERVER.download_profile("A0000005591010FFFFFFFF8900001500", saipv2_txt='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
		# EUICC.init()
		h3('Set Factory Profile to ISDP 15')
		capdu, rapdu, sw = LOCAL_SWAP.set_factory_profile(aid='A0000005591010FFFFFFFF8900001500', scp=80, apdu_format='indefinite')
		print("capdu: ", capdu)
		print("rapdu: ", rapdu)
		print("sw: ", sw)
		# StepOn()
		assert sw == '9000', 'Expected result not match'

	def test_factory_profile_cant_be_enabled_via_gsma(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			h3('EUICC INIT')
			EUICC.init()
			h3('Enable Profile ISDP 15 via GSMA')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001500' ), 80, apdu_format='definite', chunk='01')
			print("capdu: ", capdu)
			print("rapdu: ", rapdu)
			print("sw: ", sw)
			# StepOn()
			# if '9000' in rapdu: 
			# 	sw = SERVER.euicc.refresh(sw, False)
			# 	EXECUTION_STATUS = 'FAILED'
			# else:
			# 	EXECUTION_STATUS = 'PASSED'
			if '9000' in rapdu: 
				sw = SERVER.euicc.refresh(sw, False)
				capdu, rapdu, sw = SERVER.apf_on(80)
				result, status = resultPassed(testName, True)
			else:
				result, status = resultPassed(testName, False)
		except:
			exceptionTraceback()
			result, status = resultPassed(testName, False)

		timer_end = time()
		EXECUTION_DURATION = timer_end - timer_start
		LogInfo('body part')		
		
		# if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		if status != "PASSED":raise ValueError("TEST FAILED")
			
	def tearDown(self):
		h2("teardown")
		EUICC.init()
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001100' ), 80, apdu_format='definite', chunk='01')
		if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
		SERVER.delete_profile("A0000005591010FFFFFFFF8900001500", scp=80, apdu_format='definite')

if __name__ == "__main__":
	
	from test import support
	Utilities.launchTest(FactoryProfileCantBeEnabledViaGSMA)
