import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time


__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4395'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4395'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
		Constants.debugMode = True
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	INGENICO                = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID             = 'DK-4395'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

expProfileEnable = "A0000005591010FFFFFFFF89000015009F70011F"
Constants.debugMode = True
class NetworkLoss(OtTestUnit):

	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		pprint.h1('PCF 00: no POL1 rule active - Network attachment loss')
		EUICC.init()
		create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
		download_time, download_status, file_size = SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='compact', chunk='01')
		print("capdu enable profile: ", capdu)
		print("rapdu enable profile: ", rapdu)
		print("sw enable profile: ", sw)
		# StepOn()
		if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
		capdu, rapdu, sw = SERVER.apf_on(80)

		capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
		print("capdu audit list: ", capdu)
		print("rapdu audit list: ", rapdu)
		print("sw audit list: ", sw)
		if(expProfileEnable in rapdu):
			print("PROFILE 15 DISABLE")
		else:
			print("PROFILE 15 ENABLE")
		# StepOn()

	def test_network_loss(self):
		LogInfo('body part')
		EUICC.init()
		timer_start = time()
		try:

			data, sw = DEVICE.envelope_location_status('normal service')
#            if(sprint3):
#                data, sw = DEVICE.fetch_all(sw)
			sw = DEVICE.fetch_all(sw)
			data, sw = DEVICE.envelope_location_status('no service')
			print("data: ", data)
			print("sw: ", sw)
			print("siap error")
			StepOn()
			print("timer management")
			data, sw = DEVICE.fetch_one(sw, 'timer management')
			StepOn()
			print("timer expiration")
			data, sw = DEVICE.envelope_timer_expiration(id='psm')
			StepOn()
			print("refresh")
			EUICC.refresh(sw)
			StepOn()
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
			print("audit list")
			print("capdu: ", capdu)
			print("rapdu: ", rapdu)
			print("sw: ", sw)
			if(expProfileEnable in rapdu):
				print("PROFILE 15 DISABLE")
				Constants.RESULTS['test_name'].append('PCF 00: no POL1 rule active - Network attachment loss')
				Constants.RESULTS['test_result'].append('OK')
				EXECUTION_STATUS = 'PASSED'
				Constants.case += 1
			else:
				print("PROFILE 15 ENABLE")
				Constants.RESULTS['test_name'].append('PCF 00: no POL1 rule active - Network attachment loss')
				Constants.RESULTS['test_result'].append('KO')
				EXECUTION_STATUS = 'FAILED'
				Constants.case += 1
			StepOn()
			isdr_audit = INGENICO.get_euicc
			i=0
			while isdr_audit['list_has_fallback_attribute'][i]!='X':
				i+=1
			timer_end = time()
			EXECUTION_DURATION = timer_end - timer_start
			# if isdr_audit['list_life_cycle_state'][i]=='3F Enabled':
			# 	Constants.RESULTS['test_name'].append('PCF 00: no POL1 rule active - Network attachment loss')
			# 	Constants.RESULTS['test_result'].append('OK')
			# 	EXECUTION_STATUS = 'PASSED'
			# 	Constants.case += 1
			# else:
			# 	Constants.RESULTS['test_name'].append('PCF 00: no POL1 rule active - Network attachment loss')
			# 	Constants.RESULTS['test_result'].append('KO')
			# 	EXECUTION_STATUS = 'FAILED'
			# 	Constants.case += 1
		except:
			print("exception body test")
			StepOn()
			Constants.RESULTS['test_name'].append('PCF 00: no POL1 rule active - Network attachment loss')
			Constants.RESULTS['test_result'].append('KO')
			EXECUTION_STATUS = 'FAILED'
			Constants.case += 1
			capdu, rapdu, sw = SERVER.ram(GSMA.disable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
		# LogInfo('Result table')
		# pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')
		# if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")


	def tearDown(self):
		EUICC.init()
		EUICC.init()
		SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')


if __name__ == "__main__":

	from test import support
	Utilities.launchTest(NetworkLoss)
