'''
PSM update connectivity http
'''
from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time



__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
			displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_SCP80 = model.SCP80()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_SCP80 = model.SCP80()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		# SetLogLevel('info')
		SetLogLevel('debug')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP_16                  = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4303'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

#???
dataBearer = "35"+lv("01 ")

#apn: 
# prastoto 
# vrastoadjie.blogspot.com
dataNAN = "47"+lv("70726173746F746F0D0A76726173746F61646A69652E626C6F6773706F742E636F6D")

#login n pass: prastoto
dataLogin = "0D" + lv("70726173746F746F")
dataPass = "0D" + lv("70726173746F746F")

dataConParamHttp = dataBearer + dataNAN + dataLogin + dataPass
dataTagA1 = "A1" +lv(dataConParamHttp)
dataUpdateCon ="3A07" + lv(dataTagA1)

installPersoDefISDP = "80 E6 20 00" + lv("0000"+lv(Constants.AID_ISDP)+"000000 00")
apduUpdate = [installPersoDefISDP ,"80 E2 88 00 " +berLv(dataUpdateCon)]


# expDataOpenChnnl =	  "D0 56 01 03 01 40 01 02 02 81 82 05 00 35 01 01\
# 					  39 02 05 DC 47 22 70 72 61 73 74 6F 74 6F 0D 0A\
# 					  76 72 61 73 74 6F 61 64 6A 69 65 2E 62 6C 6F 67\
# 					  73 70 6F 74 2E 63 6F 6D 0D 08 70 72 61 73 74 6F\
# 					  74 6F 0D 08 70 72 61 73 74 6F 74 6F 3C 03 02 FD\
# 					  EA 3E 05 21 0A C9 6C 67"

expDataOpenChnnl15 =  "XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX\
					  XX XX XX XX XX XX XX XX XX XX XX XX XX XX 0D 0A\
					  76 72 61 73 74 6F 61 64 6A 69 65 2E 62 6C 6F 67\
					  73 70 6F 74 2E 63 6F 6D 0D 08 70 72 61 73 74 6F\
					  74 6F 0D 08 70 72 61 73 74 6F 74 6F XX XX XX XX\
					  XX XX XX XX XX XX XX XX"

expDataOpenChnnl16 =  "XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX\
					XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX\
					XX XX XX XX XX XX XX XX XX XX XX XX XX XX 0D 05\
					04 6E 6F 6E 65 0D 05 04 6E 6F 6E 65 XX XX XX XX\
					XX XX XX XX XX XX XX XX"


expDataOpenChnnl16 = "D0 1E 01 03 01 40 01 02 02 81 82 05 00 35 01 03\
					  39 02 05 DC 3C 03 02 19 64 3E 05 21 94 FB DA E5"

# expDataopenChnnl = "76 72 61 73 74 6F 61 64 6A 69 65 2E 62 6C 6F 67\
# 					  73 70 6F 74 2E 63 6F 6D 0D 08 70 72 61 73 74 6F\
# 					  74 6F 0D 08 70 72 61 73 74 6F 74 6F"

expDataOpenChnnlStr = "76726173746F61646A69652E626C6F6773706F742E636F6D0D0870726173746F746F0D0870726173746F746F"					  


expDataOpenChnnl = [expDataOpenChnnl15,expDataOpenChnnl16,expDataOpenChnnl15]

isdpTest = [Constants.AID_ISDP_16, Constants.AID_ISDP, Constants.AID_ISDP_16]
titleTest = ["Check HTTP connectivity parameter isdp 15 then enable isdp 16", 
			"Check HTTP connectivity parameter isdp 16 then enable isdp 15", 
			"Check HTTP connectivity parameter isdp 15 then enable isdp 16"]
enableTitleTest = ["enable isdp 16", "enable isdp 15", "enable isdp 16"]



exp_seq_num = "XXXX"
exp_rc_cc_ds = "XXXXXXXXXXXXXXXX"
expDCS_PID = "48"
# expSMSCAddrs = "911011223344"
expSMSCAddrs = "110502100319"

expEnvDataCon = "D0698103011300820281838606" + expSMSCAddrs + "0B" +\
				"5641010D91137900349174F1 "+\
				expDCS_PID + expDCS_PID + "4802700000431502000012000001"+\
				"000000000000" + exp_rc_cc_ds + "E12B"+\
				"4C106364160310000094337F00000000"+\
				"00044D01024E02"+exp_seq_num+"2F10A000000559"+\
				"1010FFFFFFFF8900001500"



expTestResult = "D0698103011300820281838606110502"+\
					  "1003190B5641010D91137900349174F1"+\
					  "4848"

SCP80 = model.SCP80()
SCP81 = model.SCP81()
header_targeted_app='//aid/A000000559/1010FFFFFFFF8900000100'
# Constants.debugMode = True
testName = "GSMA SGP V32 Connectivity Param update Using SCP03 HTTP Protocol"
class PSMUpdateConnectivityHTTPS(OtTestUnit):
	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		pprint.h1('Check Update Connectivity Test Script')
		
		print("dataUpdateCon: ", dataUpdateCon)
		print("apduUpdate: ", apduUpdate)
		
		EUICC.init()
		pprint.h2("Create ISDP 15")
		SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
		pprint.h2("Download Profile")
		SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)

		'''cara pertama ini dicomment'''
		# pprint.h2("Create ISDP 16")
		# SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP_16, scp=81, apdu_format='definite')
		# pprint.h2("Download Profile")
		# SERVER.download_profile(Constants.AID_ISDP_16, saipv2_txt=Constants.SAIP_TEST)

		pprint.h2("Enable ISDP 15")
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
		LogInfo("capdu enable isdp 15: "+str(capdu))
		LogInfo("rapdu enable isdp 15: "+str(rapdu))
		LogInfo("sw    enable isdp 15: "+str(sw))
		if '9000' in rapdu:
			pprint.h2("Refresh ISDP 15")
			sw = SERVER.euicc.refresh(sw, not bool(1))
			capdu, rapdu, sw = SERVER.apf_on(80)
		'''untuk cara pertama ini dicomment'''
		# pprint.h2("Enable ISDP 16")
		# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP_16), 80, apdu_format='definite', chunk='01')
		# LogInfo("capdu enable isdp 16: "+str(capdu))
		# LogInfo("rapdu enable isdp 16: "+str(rapdu))
		# LogInfo("sw    enable isdp 16: "+str(sw))
		# if '9000' in rapdu:
		# 	pprint.h2("Refresh ISDP 16")
		# 	sw = SERVER.euicc.refresh(sw, not bool(1))		
			
		# pprint.h2("Enable ISDP 15")
		# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
		# LogInfo("capdu enable isdp 15: "+str(capdu))
		# LogInfo("rapdu enable isdp 15: "+str(rapdu))
		# LogInfo("sw    enable isdp 15: "+str(sw))
		# if '9000' in rapdu:
		# 	pprint.h2("Refresh ISDP 15")
		# 	sw = SERVER.euicc.refresh(sw, not bool(1))				

	def test_update_connectivity_https(self):
		LogInfo('body part')
		pprint.h2("testbody")
		timer_start = time()
		try:
			pprint.h3("Update Connectivity Parameters")
			LogInfo("apduUpdate update connectivity param: " + str(apduUpdate))
			command_script, por, sw = EUICC.mno.ram(apduUpdate, scp=81)

			# LogInfo("command_script update connectivity param: " + str(command_script))
			# LogInfo("por update connectivity param: " + str(por))
			# LogInfo("sw update connectivity param: " + str(sw))
			
			pprint.h3("euicc init")
			EUICC.init()
			'''cara pertama paling gk udah ngebuktiin kalo open channelnya udah berubah sesuai yang diupdate'''
			pprint.h3("push sms for open channel")
			script, rapdu, sw = SERVER.euicc.mno.scp80.push_sms()

			pprint.h3("open channel")
			data, sw = DEVICE.fetch_one(sw, 'open channel', expectedData = expDataOpenChnnl[0])
			dataOpenChannel = data
			pull = sw
			
			pprint.h3("establish tls session")
			euicc_notification_sequence_number = SCP81.establish_tls_session(pull)

			pprint.h3("close tls session")
			sw = EUICC.isdr.scp81.close_tls_session()

			if expDataOpenChnnlStr in dataOpenChannel:		
				result, status = resultPassed(testName, True)
			else:
				result, status = resultPassed(testName, False)


			'''cara kedua dicomment dulu soalnya gak jelas. harusnya ini yang (mungkin) bener'''
			# # pprint.h2("audit isdp list")
			# # capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 81, apdu_format='definite')
			# # LogInfo("capdu audit isdp list: "+str(capdu))
			# # LogInfo("rapdu audit isdp list: "+str(rapdu))
			# # LogInfo("sw    audit isdp list: "+str(sw))			

			
			# for i in range (3):
			# 	loopOn = " Loop - "+str((i+1))
			# 	pprint.h2(titleTest[i]+loopOn)			

			# 	pprint.h3("push sms for open channel")
			# 	# script, rapdu, sw = EUICC.isdr.scp80.push_sms()
			# 	script, rapdu, sw = SERVER.euicc.mno.scp80.push_sms()



			# 	# script, rapdu, sw = EUICC.mno.scp80.push_sms()

			# 	# script, rapdu, sw = EUICC.mno.push_sms()	
			# 	# command_script, por, sw = EUICC.mno.scp80.RAM.push_sms()
			# 	# command_script, por, sw = EUICC.mno.scp81.push_sms()
				
				
			# 	pprint.h3("open channel")
			# 	data, sw = DEVICE.fetch_one(sw, 'open channel', expectedData = expDataOpenChnnl[i])
			# 	# data, sw = DEVICE.fetch_one(sw, 'open channel')
				
			# 	dataOpenChannel = data
			# 	pull = sw
			# 	pprint.h3("establish tls session")
			# 	# euicc_notification_sequence_number = EUICC.isdr.scp81.establish_tls_session(pull)
			# 	euicc_notification_sequence_number = SCP81.establish_tls_session(pull)
			# 	# euicc_notification_sequence_number = EUICC.mno.scp80.scp81.establish_tls_session(pull)
			# 	# euicc_notification_sequence_number = EUICC.mno.scp81.scp80.establish_tls_session(pull)

			# 	# apdus = GSMA.enable_profile(isdpTest[i])

			# 	# if isinstance(apdus, str):
			# 	# 	# handle notification confirmation
			# 	# 	if euicc_notification_sequence_number:
			# 	# 		apdus = apdus.replace('XXXX', euicc_notification_sequence_number)
			# 	# 	# euicc_notification_sequence_number = "0002"
			# 	# 	apdus = [apdus]

			# 	# capdu_value = ''
			# 	# for apdu in apdus:
			# 	# 	capdu_value += '22' + berLv(apdu)			
			# 	# capdu = 'AA' + berLv(capdu_value)

			# 	# pprint.h3("exchange application data")
			# 	# # rapdu = EUICC.isdr.scp81.exchange_application_data(capdu, header_targeted_app=header_targeted_app, content_type='RAM',chunk='01')
			# 	# rapdu = SCP81.exchange_application_data(capdu, header_targeted_app=header_targeted_app, content_type='RAM',chunk='01')
			# 	pprint.h3("close tls session")
			# 	# sw = EUICC.isdr.scp81.close_tls_session()
			# 	sw = SCP81.close_tls_session()
							
			# 	# if '9000' in rapdu:
			# 	# 	pprint.h3("refresh")
			# 	# 	sw = SERVER.euicc.refresh(sw, not bool(1))		
			# 	pprint.h3(enableTitleTest[i]+loopOn)
			# 	capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(isdpTest[i]), 80, apdu_format='definite', chunk='01')
			# 	if '9000' in rapdu:
			# 		sw = SERVER.euicc.refresh(sw, False)	
			# 	LogInfo("StepOn() Loop: "+loopOn)
			# 	# StepOn()	
			# 	pprint.h3("euicc init")
			# 	EUICC.init()		

			# '''harusnya ini udah bisa'''
			# # pprint.h2("Enable ISDP 11")
			# # # capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 81, apdu_format='definite', chunk='01')
			# # # if '9000' in rapdu:
			# # # 	sw = SERVER.euicc.refresh(sw, False)				

			

			# # # pprint.h2("re-enable profile 15")
			# # pprint.h3("push sms for open channel")
			# # script, rapdu, sw = EUICC.isdr.scp80.push_sms()
			# # # script, rapdu, sw = EUICC.mno.scp80.push_sms()
			
			
			# # pprint.h3("open channel")
			# # data, sw = DEVICE.fetch_one(sw, 'open channel', expectedData = expDataOpenChnnl)
			# # # data, sw = DEVICE.fetch_one(sw, 'open channel')
			
			# # dataOpenChannel = data
			# # pull = sw
			# # pprint.h3("establish tls session")
			# # euicc_notification_sequence_number = EUICC.isdr.scp81.establish_tls_session(pull)

			# # apdus = GSMA.enable_profile(Constants.DEFAULT_ISDP11)

			# # if isinstance(apdus, str):
			# # 	# handle notification confirmation
			# # 	if euicc_notification_sequence_number:
			# # 		apdus = apdus.replace('XXXX', euicc_notification_sequence_number)
			# # 	# euicc_notification_sequence_number = "0002"
			# # 	apdus = [apdus]

			# # capdu_value = ''
			# # for apdu in apdus:
			# # 	capdu_value += '22' + berLv(apdu)			
			# # capdu = 'AA' + berLv(capdu_value)

			# # pprint.h3("exchange application data")
			# # rapdu = EUICC.isdr.scp81.exchange_application_data(capdu, header_targeted_app=header_targeted_app, content_type='RAM',chunk='01')
			# # pprint.h3("close tls session")
			# # sw = EUICC.isdr.scp81.close_tls_session()
			# # LogInfo("capdu: "+str(capdu))
			# # LogInfo("rapdu: "+str(rapdu))
			# # LogInfo("sw   : "+str(sw))

					
			# # if '9000' in rapdu:
			# # 	sw = SERVER.euicc.refresh(sw, not bool(1))
			

			# if dataOpenChannel == expDataOpenChnnl:						
			# 	Constants.RESULTS['test_name'].append('PSM Check Update Connectivity')
			# 	Constants.RESULTS['test_result'].append('OK')
			# 	EXECUTION_STATUS = 'PASSED'
			# 	Constants.case+=1
			# else:
			# 	Constants.RESULTS['test_name'].append('PSM Check Update Connectivity')
			# 	Constants.RESULTS['test_result'].append('KO')
			# 	EXECUTION_STATUS = 'FAILED'
			# 	Constants.case+=1

		except:
			exceptionTraceback()
			result, status = resultPassed(testName, False)
			
		timer_end = time()
		EXECUTION_DURATION = timer_end - timer_start
		LogInfo('body part')

		# if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		# if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")
		if(result!="OK"):raise ValueError("TEST FAILED")

	def tearDown(self):
		EUICC.init()
		pprint.h3("tearDown")
		pprint.h2("disable profile 15")
		SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
		pprint.h2("delete profile 15")
		SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
		DEVICE.get_status()

		'''cara pertama ini dicomment'''
		# pprint.h2("Enable ISDP 16")
		# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP_16), 80, apdu_format='definite', chunk='01')
		# LogInfo("capdu enable isdp 15: "+str(capdu))
		# LogInfo("rapdu enable isdp 15: "+str(rapdu))
		# LogInfo("sw    enable isdp 15: "+str(sw))
		# if '9000' in rapdu:
		# 	pprint.h2("Refresh ISDP 16")
		# 	sw = SERVER.euicc.refresh(sw, not bool(1))		

		# pprint.h2("disable profile 16")
		# SERVER.disable_profile(Constants.AID_ISDP_16, scp=80, network_service=False, apdu_format='indefinite')
		# pprint.h2("delete profile 16")
		# SERVER.delete_profile(Constants.AID_ISDP_16, scp=80, apdu_format='definite')
		# DEVICE.get_status()

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(PSMUpdateConnectivityHTTPS)