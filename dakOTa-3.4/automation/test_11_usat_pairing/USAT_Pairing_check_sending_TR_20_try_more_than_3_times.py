'''
Created on Jan 5, 2018

@author: prastaji

@purpose:
    Check USAT Pairing functionallity by sending Terminal Response
    but the Terminal Response must send error code of "20" trying more than 3 times
'''
from Ot.PyCom import *
import sys
from Oberthur import *
import os
sys.path.insert(0,os.getcwd()+"\site-packages")
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)

from util import *
from Mobile import *

import unittest
import model
from time import time

'''
additional import
'''
from const_USAT_Pairing import * #need create under(?)

__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'

Options.counters={}
underLoop = 10
tagParam = "CR"

if Constants.displayAPDU != None:
    displayAPDU(Constants.displayAPDU)

try:
    if Constants.RunAll == True:
        SERVER                  = model.Smsr()
        EUICC                   = model.Euicc()
        GSMA                    = model.Gsma()
        DEVICE                     = model.Device()
        TL_SERVER               = model.TestlinkServer()

        EXTERNAL_ID             = 'DK-4316'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None
    else:
        SERVER                  = model.Smsr()
        EUICC                   = model.Euicc()
        GSMA                    = model.Gsma()
        DEVICE                     = model.Device()
        TL_SERVER               = model.TestlinkServer()

        EXTERNAL_ID             = 'DK-4316'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None

        displayAPDU(True)
        SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

        ''' TESTLINK VARIABLES '''
        Constants.TESTLINK_REPORT           = False
        Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
        Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
        Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
        Constants.TESTLINK_BUILD            = 'build_for_test'
        Constants.TESTLINK_PLATFORM         = 'IO222'
        Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
        Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

        '''
        TEST VARIABLES

        To launch the test, prepare the following variables :

        Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
        Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
        Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
        Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

        '''
        Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
        Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
        Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
        Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
        Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
        Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
        Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
        Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
        Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
        Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
        Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
        Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
        Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
        Constants.ITERATIONS_PERFORMANCE    = 1
        Constants.ITERATIONS_STRESS         = 1
        Constants.RESULTS                   = {
                                                'test_name'         : ['Test name'],
                                                'test_result'        : ['Test result']
                                                }
        Constants.case                      = 0
except:
    SERVER                  = model.Smsr()
    EUICC                   = model.Euicc()
    GSMA                    = model.Gsma()
    DEVICE                     = model.Device()
    TL_SERVER               = model.TestlinkServer()

    EXTERNAL_ID             = 'DK-4316'
    EXECUTION_STATUS        = None
    EXECUTION_DURATION      = None

    displayAPDU(True)
    SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

    ''' TESTLINK VARIABLES '''
    Constants.TESTLINK_REPORT           = False
    Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
    Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
    Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
    Constants.TESTLINK_BUILD            = 'build_for_test'
    Constants.TESTLINK_PLATFORM         = 'IO222'
    Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
    Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

    '''
    TEST VARIABLES

    To launch the test, prepare the following variables :

    Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
    Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
    Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
    Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

    '''
    Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
    Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
    Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
    Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
    Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
    Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
    Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
    Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
    Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
    Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
    Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
    Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
    Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
    Constants.ITERATIONS_PERFORMANCE    = 1
    Constants.ITERATIONS_STRESS         = 1
    Constants.RESULTS                   = {
                                            'test_name'         : ['Test name'],
                                            'test_result'        : ['Test result']
                                            }
    Constants.case                      = 0

class USATPairingSendTR20more3(OtTestUnit):

    def report(self, execution_status, execduration):
        LogInfo('Start reporting test result...')
        TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
        LogInfo('Finished')

    def setUp(self):
        pprint.h1('USAT Pairing check sending Terminal Response code of 11')

        self.imeiAndImeiSvParam = setImeiAndImeiSvParameter(tagParam)
        self.underLoop = underLoop
        self.imeiAndImeiSvDataImeiTR = self.imeiAndImeiSvParam.get("imeiAndImeiSvDataImeiTR")
        self.imeiAndImeiSvStatusTR = self.imeiAndImeiSvParam.get("imeiAndImeiSvStatusTR")
        self.imeiAndImeiSvHeaderDataTR = self.imeiAndImeiSvParam.get("imeiAndImeiSvHeaderDataTR")
        self.imeiAndImeiSvExpectedIPS = self.imeiAndImeiSvParam.get("imeiAndImeiSvExpectedIPS")
        self.imeiAndImeiSvExpectedIPD = self.imeiAndImeiSvParam.get("imeiAndImeiSvExpectedIPD")
        self.IMEI_AND_IMEI_SV_ONLY_WHITE_LIST_DATA = self.imeiAndImeiSvParam.get("IMEI_AND_IMEI_SV_ONLY_WHITE_LIST_DATA")
        self.imeiAndImeiSvSWIPD = self.imeiAndImeiSvParam.get("imeiAndImeiSvSWIPD")
        self.imeiAndImeiSvSWAuth = self.imeiAndImeiSvParam.get("imeiAndImeiSvSWAuth")
        self.imeiAndImeiSvSWResp4Auth = self.imeiAndImeiSvParam.get("imeiAndImeiSvSWResp4Auth")
        self.imeiAndImeiSvExpDataResp4Auth = self.imeiAndImeiSvParam.get("imeiAndImeiSvExpDataResp4Auth")

        EUICC.init()
        SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
        SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
        capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')

        LogInfo("## Enable USAT Pairing profile")
        capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')

        PowerOn()
        LogInfo("## Choose mode for choose algo")
        Mobile.SetMode("2G")
        authAlgoChoosenAKA()

        PowerOn()
        LogInfo("## Choose mode for testing")
        Mobile.SetMode("3G")

        LogInfo("## Init Handset")
        self.tpData = setTerminalProfile ("IMEI_AND_IMEI_SV")
        self.sendTerminalResponse = self.imeiAndImeiSvHeaderDataTR[self.underLoop] + self.imeiAndImeiSvStatusTR[self.underLoop] + self.imeiAndImeiSvDataImeiTR[self.underLoop]
        pass

    def test_usat_pairing_sending_tr(self):
        LogInfo('body part')
        EUICC.init()
        PowerOn()
        timer_start = time()
        outputFetch = Mobile.TerminalProfile(self.tpData.get("tpData"), expectedStatus=OK_FETCH+"XX")
        Mobile.Fetch(outputFetch[2:], expectedData = self.tpData.get("tpFetchData"), expectedStatus=OK)

        LogInfo("## Send Terminal Response 1 time KO")
        outputFetch = Mobile.TerminalResponse(inputData = self.sendTerminalResponse, expectedStatus = "910B")
        Mobile.Fetch(outputFetch[2:], expectedData = self.tpData.get("tpFetchData"), expectedStatus=OK)

        LogInfo("## Send Terminal Response 2 time KO")
        outputFetch = Mobile.TerminalResponse(inputData = self.sendTerminalResponse, expectedStatus = "910B")
        Mobile.Fetch(outputFetch[2:], expectedData = self.tpData.get("tpFetchData"), expectedStatus=OK)

        LogInfo("## Send Terminal Response 3 time KO")
        outputFetch = Mobile.TerminalResponse(inputData = self.sendTerminalResponse, expectedStatus = "910B")
        Mobile.Fetch(outputFetch[2:], expectedData = self.tpData.get("tpFetchData"), expectedStatus=OK)

        LogInfo("## Send Terminal Response 4 time Done for KO")
        outputFetch = Mobile.TerminalResponse(inputData = self.sendTerminalResponse, expectedStatus = OK)

        LogInfo("## Finish send Terminal Response")

        LogInfo("## Verify PIN")
        Mobile.VerifyPin(ADM1_USIM_REF + "08" + ISC1, expectedStatus = OK)
        Mobile.Select(ADF_USIM, expectedStatus = OK)

        LogInfo("## Check IWL Data")
        Mobile.Select(EF_IMEI_SV_WHITE_LIST, expectedStatus = OK)
        for(i) in range (4): #how much record in this EF?
            Mobile.ReadRecord(intToHexString(i+1), "04", REC_LENGTH_EF_IWL, expectedData= self.IMEI_AND_IMEI_SV_ONLY_WHITE_LIST_DATA[i], expectedStatus=OK)

        LogInfo("## Check IPS Data")
        Mobile.Select(EF_IMEI_SV_PAIRING_STATUS, expectedStatus = OK)
        Mobile.ReadRecord(intToHexString(1), "04", REC_LENGTH_EF_IPS, expectedData = self.imeiAndImeiSvExpectedIPS[self.underLoop], expectedStatus=OK)

        LogInfo("## Check IPD Data")
        Mobile.Select(EF_IMEI_SV_PAIRING_DEVICE, expectedStatus = OK)
        Mobile.ReadRecord(intToHexString(1), "04", REC_LENGTH_EF_IPD, expectedData = self.imeiAndImeiSvExpectedIPD[self.underLoop],
                        expectedStatus=self.imeiAndImeiSvSWIPD[self.underLoop])

        LogInfo("### SEND AUTHENTICATION COMMAND ###")
        Mobile.SelectAIDUsim(AID_USIM, expectedStatus = OK)
        Mobile.VerifyPin(PIN_USIM_REF + "08" + PIN1_USIM  , expectedStatus = OK)
        Mobile.Select(ADF_USIM, expectedStatus = OK)
        Mobile.Select(DIRECTORY_FILE3, expectedStatus = OK)

        if(DEF("T1_PROTOCOL")):
            ''''''
            LogInfo("if(DEF(T1_PROTOCOL)):")
            Send(USIM_AUTHENTICATE + "$81"+"22"+ "10"+ str(rand)+ "10"+ GetConst("SQN_xor_AK")+str(amf)+GetConst("MAC")+"28",
                expectedData =  "DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                expectedStatus = self.imeiAndImeiSvSWAuth[self.underLoop] )
        else:
            ''''''
            LogInfo("else - if(DEF(T1_PROTOCOL))")
            Send(USIM_AUTHENTICATE + "$81"+"22"+ "10"+
            rand+ "10"+ GetConst("SQN_xor_AK")+str(amf)+GetConst("MAC"),
                expectedStatus = self.imeiAndImeiSvSWAuth[self.underLoop] )
            Mobile.GetResponse("28", expectedData = self.imeiAndImeiSvExpDataResp4Auth[self.underLoop],
                expectedStatus = self.imeiAndImeiSvSWResp4Auth[self.underLoop])
        timer_end = time()
        EXECUTION_DURATION = timer_end - timer_start

        LogInfo('body part')
        # if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

    def tearDown(self):
        EUICC.init()
        SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
        DEVICE.get_status()
        pass

if __name__ == "__main__":

    from test import support
    Utilities.launchTest(USATPairingSendTR20more3)