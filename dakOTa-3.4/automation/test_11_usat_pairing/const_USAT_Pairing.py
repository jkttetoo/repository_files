'''
@script: include_USAT_Pairing.py

@group: Specific

@author: AJI Prastoto

@date: Oct 17, 2016

$Revision:   1.0  $

@purpose:
    include file for USAT Pairing Test

@req:

@destructive: no
'''

from Oberthur.Reader import CardReader
from Ot.PyCom import *
from OtOTA2Wrapper import *

import include.include_OTA
import Utilities
import Mobile
import ADMCommands

ListOn()
Call("include.all_includes_gsm_cmd", ListOn = False)
Call("include.all_includes_usim_cmd", ListOn = False)
Call("include.card_manager_GP22_key_and_AID_cmd", ListOn = False)
Call("include.all_includes_CatTp_cmd", ListOn = False)

TERMINAL_PROFILE_DATA_USAT_Pairing       = "13ffff ff ff7f3f00dfff00001fe28a0d02 07 0900ffffffffffffffffff"
SET_UP_EVENT_LIST_PUSH_SMS_USAT_Pairing  = "D009 8103 012608" + DEVICE_ID_SIM_TO_ME
'''
012608
cmd no: = 01
type of cmnd = 26 => Provide local Info
cmd qualifier = 08 >>> bukannya 01???
'''

key = "75 61 5C 99 B1 B3 F7 A1 CA D0 64 23 1C C5 5A 4B"
rand = "BE F9 A8 95 F1 EF E9 E5 CB 47 1F A8 64 73 5E 63"
sqn = "3B 70 64 24 11 9E"
amf = "09 DC"

'''-------------------------------------------------IMEI PARAMETER------------------------------------------------------------'''
''' IMEI ONLY PARAMETER'''
imeiOnlyLogCASE = ["IMEI in LIST",
                    "Use IMEI SV",
                    "IMEI not in LIST",
                    "IMEI length MORE than record",
                    "IMEI length LESS than record",
                    "Terminal Response Result Tag - General Result = 10",
                    "Terminal Response Result Tag - General Result = 11",
                    "Terminal Response Result Tag - General Result = 20 retry less than 3 times",
                    "Terminal Response Result Tag - General Result = 20 retry more than 3 times",
                    "Terminal Response Result Tag - General Result = 30"]

def setImeiOnlyParameter(typeOfTag):
    if(typeOfTag == "CR"):
        tot = "94"
    else:
        tot = "14"
#    imeiOnlyDataImeiTR = [tot + "08" + "3F 25 91 01 97 54 46 59",
    imeiOnlyDataImeiTR = [tot + "08" + "3F 25 91 01 97 54 46 57",
    "62 08" + "53 15 10 50 61 26 08 54",
    tot + "08" + "3F 58 43 20 70 03 41 32",
    tot + "09" + "F3 52 19 10 79 45 64 48 48",
    tot + "07" + "F3 52 19 10 79 45 64",
    "",
    "",
    "",
    "",
    ""]

    imeiOnlyStatusTR = ["830100",
                        "830100",
                        "830100",
                        "830100",
                        "830100",
                        "830110",
                        "830111",
                        "83022000",
                        "83022000",
                        "830130"]

    imeiOnlyExpectedIPS = ["CF 25 01 00",
    "FF FF FF FF",
    "CB 27 01 00",
    "CB 27 01 00",
    "CB 27 01 00",
    "FF FF FF FF",
    "FF FF FF FF",
    "CF 25 01 00",
    "FF FF FF FF",
    "FF FF FF FF"]

#    imeiOnlyExpectedIPD = ["80 08 3F 25 91 01 97 54 46 59",
    imeiOnlyExpectedIPD = ["80 08 3F 25 91 01 97 54 46 57",
                           "",
                           "80 08 3F 58 43 20 70 03 41 32",
                           "80 09 F3 52 19 10 79 45 64 48",
                           "80 07 F3 52 19 10 79 45 64 FF",
                           "",
                           "",
                           "80 08 3F 25 91 01 97 54 46 59",
                           "",
                           ""]

    imeiOnlySWIPD = [OK,
              "6A83",
              OK,
              OK,
              OK,
              "6A83",
              "6A83",
              OK,
              "6A83",
              "6A83"]

    imeiOnlyHeaderDataTR = "8103 012601" + DEVICE_ID_ME_TO_SIM

    IMEI_ONLY_WHITE_LIST_DATA = ["80 10 3F259101975446503F25910197544659",
                      "80 10 3F550101156682103F55010115668290",
                      "80 10 3F584320700341303F58432070034130",
                      "80 10 3F544060121180203F54406012118020"]

    if(DEF("T1_PROTOCOL")):
        imeiOnlySWAuth = [OK,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          OK,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED
                          ]
    else:
        imeiOnlySWAuth = [GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED
                          ]

    imeiOnlySWResp4Auth = [OK,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           OK,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR]

    imeiOnlyExpDataResp4Auth = ["DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                "",
                                ""]

    returnOfImeiOnlyParameter = {"imeiOnlyDataImeiTR":imeiOnlyDataImeiTR,
                                 "imeiOnlyStatusTR":imeiOnlyStatusTR,
                                 "imeiOnlyExpectedIPS":imeiOnlyExpectedIPS,
                                 "imeiOnlyExpectedIPD":imeiOnlyExpectedIPD,
                                 "imeiOnlyHeaderDataTR":imeiOnlyHeaderDataTR,
                                 "IMEI_ONLY_WHITE_LIST_DATA":IMEI_ONLY_WHITE_LIST_DATA,
                                 "imeiOnlySWIPD":imeiOnlySWIPD,
                                 "imeiOnlySWAuth":imeiOnlySWAuth,
                                 "imeiOnlySWResp4Auth":imeiOnlySWResp4Auth,
                                 "imeiOnlyExpDataResp4Auth":imeiOnlyExpDataResp4Auth}

    return returnOfImeiOnlyParameter

'''IMEI ONLY PARAMETER'''

'''-------------------------------------------------IMEI SV PARAMETER------------------------------------------------------------'''
'''IMEI SV PARAMETER'''
imeiSvOnlyLogCASE = ["IMEI SV in LIST",
                    "Use IMEI",
                    "IMEI SV not in LIST",
                    "IMEI SV length MORE than record",
                    "IMEI SV length LESS than record",
                    "Terminal Response Result Tag - General Result = 10",
                    "Terminal Response Result Tag - General Result = 11",
                    "Terminal Response Result Tag - General Result = 20 retry less than 3 times",
                    "Terminal Response Result Tag - General Result = 20 retry more than 3 times",
                    "Terminal Response Result Tag - General Result = 30"]

def setImeiSvOnlyParameter(typeOfTag):
    if(typeOfTag == "CR"):
        tot = "E2"
    else:
        tot = "62"
    imeiSvOnlyDataImeiTR = [tot + "08" + "53 15 10 50 61 26 08 04",
                      "14 08" + "3F 25 91 01 97 54 46 49",
                      tot + "08" + "53 15 10 50 61 26 08 79",
                      tot + "09" + "53 15 10 50 61 36 08 01 53",
                      tot + "07" + "53 15 10 50 61 36 08",
                      "",
                      "",
                      "",
                      "",
                      ""]

    imeiSvOnlyStatusTR = ["830100",
                        "830100",
                        "830100",
                        "830100",
                        "830100",
                        "830110",
                        "830111",
                        "83022000",
                        "83022000",
                        "830130"]

    imeiSvOnlyExpectedIPS = ["CF 25 01 00",
                           "FF FF FF FF",
                           "CB 27 01 00",
                           "CB 27 01 00",
                           "CB 27 01 00",
                           "FF FF FF FF",
                           "FF FF FF FF",
                           "CF 25 01 00",
                           "FF FF FF FF",
                           "FF FF FF FF"]

    imeiSvOnlyExpectedIPD = ["81 08 53 15 10 50 61 26 08 04",
                           "",
                           "81 08 53 15 10 50 61 26 08 79",
                           "81 09 53 15 10 50 61 36 08 01",
                           "81 07 53 15 10 50 61 36 08 FF",
                           "",
                           "",
                           "81 08 53 15 10 50 61 26 08 04",
                           "",
                           ""]

    imeiSvOnlySWIPD = [OK,
              "6A83",
              OK,
              OK,
              OK,
              "6A83",
              "6A83",
              OK,
              "6A83",
              "6A83"]

    imeiSvOnlyHeaderDataTR = "8103 012608" + DEVICE_ID_ME_TO_SIM

    IMEI_SV_ONLY_WHITE_LIST_DATA = ["81 10 53151050612608045315105061260874",
                  "81 10 53151050613608015315105061360891",
                  "81 10 53151050612608745315105061260874",
                  "81 10 53151050613608015315105061360801"]

    if(DEF("T1_PROTOCOL")):
        imeiSvOnlySWAuth = [OK,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          OK,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED
                          ]
    else:
        imeiSvOnlySWAuth = [GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED
                          ]

    imeiSvOnlySWResp4Auth = [OK,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           OK,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR]

    imeiSvOnlyExpDataResp4Auth = ["DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                "",
                                ""]

    returnOfImeiSvOnlyParameter = {"imeiSvOnlyDataImeiTR":imeiSvOnlyDataImeiTR,
                                 "imeiSvOnlyStatusTR":imeiSvOnlyStatusTR,
                                 "imeiSvOnlyExpectedIPS":imeiSvOnlyExpectedIPS,
                                 "imeiSvOnlyExpectedIPD":imeiSvOnlyExpectedIPD,
                                 "imeiSvOnlyHeaderDataTR":imeiSvOnlyHeaderDataTR,
                                 "IMEI_SV_ONLY_WHITE_LIST_DATA":IMEI_SV_ONLY_WHITE_LIST_DATA,
                                 "imeiSvOnlySWIPD":imeiSvOnlySWIPD,
                                 "imeiSvOnlySWAuth":imeiSvOnlySWAuth,
                                 "imeiSvOnlySWResp4Auth":imeiSvOnlySWResp4Auth,
                                 "imeiSvOnlyExpDataResp4Auth":imeiSvOnlyExpDataResp4Auth}

    return returnOfImeiSvOnlyParameter
'''IMEI SV PARAMETER'''

'''-------------------------------------------------IMEI and IMEI SV PARAMETER------------------------------------------------------------'''
'''IMEI and IMEI SV PARAMETER'''
imeiAndImeiSvLogCASE = ["IMEI SV in LIST",
                    "IMEI in LIST",
                    "IMEI SV not in LIST",
                    "IMEI not in LIST",
                    "IMEI SV length MORE than record",
                    "IMEI SV length LESS than record",
                    "IMEI length MORE than record",
                    "IMEI length LESS than record",
                    "Terminal Response Result Tag - General Result = 10",
                    "Terminal Response Result Tag - General Result = 11",
                    "Terminal Response Result Tag - General Result = 20 retry less than 3 times",
                    "Terminal Response Result Tag - General Result = 20 retry more than 3 times",
                    "Terminal Response Result Tag - General Result = 30"]

def setImeiAndImeiSvParameter(typeOfTag):
    if(typeOfTag == "CR"):
        tot = "E2"
    else:
        tot = "62"
    imeiAndImeiSvDataImeiTR = [tot + "08" + "35 51 01 05 16 62 80 45",
                      "94 08" + "3F 25 91 01 97 54 46 45",
                      tot + "08" + "53 15 10 50 61 26 08 79",
                      "94 08" + "3F 25 91 01 97 54 56 99",
                      tot + "09" + "53 15 10 50 61 36 08 01 53",
                      tot + "07" + "53 15 10 50 61 36 08",
                      "94 09" + "3F 25 91 01 97 54 46 49 49",
                      "94 07" + "3F 25 91 01 97 54 46",
                      "",
                      "",
                      "",
                      "",
                      ""]

    imeiAndImeiSvStatusTR = ["830100",
                        "830100",
                        "830100",
                        "830100",
                        "830100",
                        "830100",
                        "830100",
                        "830100",
                        "830110",
                        "830111",
                        "83022000",
                        "83022000",
                        "830130"]

    imeiAndImeiSvExpectedIPS = ["CF 25 01 00",
                           "CF 25 01 00",
                           "CB 27 01 00",
                           "CB 27 01 00",
                           "CB 27 01 00",
                           "CB 27 01 00",
                           "CB 27 01 00",
                           "CB 27 01 00",
                           "FF FF FF FF",
                           "FF FF FF FF",
                           "CF 25 01 00",
                           "FF FF FF FF",
                           "FF FF FF FF"]

    imeiAndImeiSvExpectedIPD = ["81 08 35 51 01 05 16 62 80 45",
                           "80 08 3F 25 91 01 97 54 46 45",
                           "81 08 53 15 10 50 61 26 08 79",
                           "80 08 3F 25 91 01 97 54 56 99",
                           "81 09 53 15 10 50 61 36 08 01",
                           "81 07 53 15 10 50 61 36 08 FF",
                           "80 09 3F 25 91 01 97 54 46 49",
                           "80 07 3F 25 91 01 97 54 46 FF",
                           "",
                           "",
                           "81 08 35 51 01 05 16 62 80 45",
                           "",
                           ""]

    imeiAndImeiSvSWIPD = [OK,
              OK,
              OK,
              OK,
              OK,
              OK,
              OK,
              OK,
              "6A83",
              "6A83",
              OK,
              "6A83",
              "6A83"]

#    imeiAndImeiSvHeaderDataTR = "8103 012608" + DEVICE_ID_ME_TO_SIM
    imeiAndImeiSvHeaderDataTR = ["8103 012608" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012601" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012608" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012601" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012608" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012608" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012601" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012601" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012608" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012608" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012608" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012608" + DEVICE_ID_ME_TO_SIM,
                                 "8103 012608" + DEVICE_ID_ME_TO_SIM]

    IMEI_AND_IMEI_SV_ONLY_WHITE_LIST_DATA = ["80 10 3F259101975446403F25910197544649",
                                            "80 10 3F584320700341303F58432070034130",
                                            "81 10 35510105166280403551010516628047",
                                            "81 10 35510105166380103551010516638010"]

    if(DEF("T1_PROTOCOL")):
        imeiAndImeiSvSWAuth = [OK,
                          OK,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          OK,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED
                          ]
    else:
        imeiAndImeiSvSWAuth = [GET_RESPONSE_61 + "xx",
                          GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED
                          ]

    imeiAndImeiSvSWResp4Auth = [OK,
                           OK,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           OK,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR]

    imeiAndImeiSvExpDataResp4Auth = ["DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                    "DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                    "",
                                    ""]

    returnOfImeiAndImeiSvParameter = {"imeiAndImeiSvDataImeiTR":imeiAndImeiSvDataImeiTR,
                                 "imeiAndImeiSvStatusTR":imeiAndImeiSvStatusTR,
                                 "imeiAndImeiSvExpectedIPS":imeiAndImeiSvExpectedIPS,
                                 "imeiAndImeiSvExpectedIPD":imeiAndImeiSvExpectedIPD,
                                 "imeiAndImeiSvHeaderDataTR":imeiAndImeiSvHeaderDataTR,
                                 "IMEI_AND_IMEI_SV_ONLY_WHITE_LIST_DATA":IMEI_AND_IMEI_SV_ONLY_WHITE_LIST_DATA,
                                 "imeiAndImeiSvSWIPD":imeiAndImeiSvSWIPD,
                                 "imeiAndImeiSvSWAuth":imeiAndImeiSvSWAuth,
                                 "imeiAndImeiSvSWResp4Auth":imeiAndImeiSvSWResp4Auth,
                                 "imeiAndImeiSvExpDataResp4Auth":imeiAndImeiSvExpDataResp4Auth}

    return returnOfImeiAndImeiSvParameter
'''IMEI and IMEI SV PARAMETER'''



'''-------------------------------------------------IPD MORE THAN IPS REC PARAMETER------------------------------------------------------------'''
''' IPD MORE THAN IPS REC PARAMETER'''
IPDMoreThanIPSLogCASE = ["IMEI in LIST",
                    "IMEI in LIST",
                    "IMEI not in LIST",
                    "IMEI in LIST",
                    "IMEI not in LIST",
                    "IMEI in LIST",
                    "IMEI not in LIST"]

def setIPDMoreThanIPSParameter(typeOfTag):
    if(typeOfTag == "CR"):
        tot = "94"
    else:
        tot = "14"
    IPDMoreThanIPSDataImeiTR = [tot + "08" + "3F 25 91 01 97 54 46 59",
                      tot + "08" + "3F 55 01 01 15 66 82 20",
                      tot + "08" + "4F 55 01 01 15 66 82 10",
                      tot + "08" + "3F 58 43 20 70 03 41 30",
                      tot + "08" + "4F 58 43 20 70 03 41 30",
                      tot + "08" + "3F 25 91 01 97 54 46 59",
                      tot + "08" + "4F 25 91 01 97 54 46 59"]

#    IPDMoreThanIPSStatusTR = ["830100",
#                        "830100",
#                        "830100",
#                        "830100",
#                        "830100",
#                        "830110",
#                        "830111",
#                        "83022000",
#                        "83022000",
#                        "830130"]

    IPDMoreThanIPSStatusTR = "830100"

#    IPDMoreThanIPSExpectedIPS = ["CF 25 01 00",
#                           "CF 25 01 00",
#                           "CB 27 01 00",
#                           "CF 25 01 00",
#                           "CB 27 01 00",
#                           "CF 25 01 00",
#                           "CB 27 01 00"]

    IPDMoreThanIPSExpectedIPS = ["CF 25 01 00",
                           "CF 25 02 00",
                           "CB 27 03 00",
                           "CF 25 04 00",
                           "CB 27 05 00",
                           "CF 25 01 00",
                           "CB 27 06 00"]


    IPDMoreThanIPSExpectedIPD = ["80 08 3F 25 91 01 97 54 46 59",
                           "80 08 3F 55 01 01 15 66 82 20",
                           "80 08 4F 55 01 01 15 66 82 10",
                           "80 08 3F 58 43 20 70 03 41 30",
                           "80 08 4F 58 43 20 70 03 41 30",
                           "",
#                           "80 08 3F 25 91 01 97 54 46 59",
                           "80 08 4F 25 91 01 97 54 46 59"]

    IPDMoreThanIPSSWIPD = [OK,
              OK,
              OK,
              OK,
              OK,
              "6A83",
              OK]

#    IPDMoreThanIPSSWIPD = OK

    IPDMoreThanIPSHeaderDataTR = "8103 012601" + DEVICE_ID_ME_TO_SIM

    IMEI_ONLY_WHITE_LIST_DATA = ["80 10 3F259101975446093F25910197544689",
                      "80 10 3F550101156682103F55010115668290",
                      "80 10 3F584320700341303F58432070034130",
                      "80 10 3F544060121180203F54406012118020"]

    IPDMoreThanIPSCheckIPSPos = ["01",
                                      "02",
                                      "03",
                                      "04",
                                      "01",
                                      "02",
                                      "03"
                                      ]

    if(DEF("T1_PROTOCOL")):
        IPDMoreThanIPSSWAuth = [OK,
                          OK,
                          COND_USE_NOT_SATISFIED,
                          OK,
                          COND_USE_NOT_SATISFIED,
                          OK,
                          COND_USE_NOT_SATISFIED
                          ]
    else:
        IPDMoreThanIPSSWAuth = [GET_RESPONSE_61 + "xx",
                          GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          GET_RESPONSE_61 + "xx",
                          COND_USE_NOT_SATISFIED,
                          ]

    IPDMoreThanIPSSWResp4Auth = [OK,
                           OK,
                           TECHNICAL_ERROR,
                           OK,
                           TECHNICAL_ERROR,
                           OK,
                           TECHNICAL_ERROR]

    IPDMoreThanIPSExpDataResp4Auth = ["DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                "DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                "",
                                "DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                "",
                                "DB 04"+GetConst("RES") + "10" + GetConst("CK") +"10" + GetConst("IK"),
                                ""]

    returnOfImeiOnlyParameter = {"IPDMoreThanIPSDataImeiTR":IPDMoreThanIPSDataImeiTR,
                                 "IPDMoreThanIPSStatusTR":IPDMoreThanIPSStatusTR,
                                 "IPDMoreThanIPSExpectedIPS":IPDMoreThanIPSExpectedIPS,
                                 "IPDMoreThanIPSExpectedIPD":IPDMoreThanIPSExpectedIPD,
                                 "IPDMoreThanIPSHeaderDataTR":IPDMoreThanIPSHeaderDataTR,
                                 "IMEI_ONLY_WHITE_LIST_DATA":IMEI_ONLY_WHITE_LIST_DATA,
                                 "IPDMoreThanIPSSWIPD":IPDMoreThanIPSSWIPD,
                                 "IPDMoreThanIPSSWAuth":IPDMoreThanIPSSWAuth,
                                 "IPDMoreThanIPSSWResp4Auth":IPDMoreThanIPSSWResp4Auth,
                                 "IPDMoreThanIPSCheckIPSPos":IPDMoreThanIPSCheckIPSPos,
                                 "IPDMoreThanIPSExpDataResp4Auth":IPDMoreThanIPSExpDataResp4Auth
                                 }

    return returnOfImeiOnlyParameter

'''IPD MORE THAN IPS REC PARAMETER'''




'''-------------------------------------------------DELETED EFs MANDATORY------------------------------------------------------------'''
''' DELETED EFs MANDATORY PARAMETER'''
deletedEfsMandatoryLogCASE = ["DELETED EF_IPS",
                    "DELETED EF_IPD",
                    "DELETED EF_IWL"]

def setDeletedEfsMandatoryParameter(typeOfTag):
    if(typeOfTag == "CR"):
        tot = "94"
    else:
        tot = "14"
#    deletedEfsMandatoryDataImeiTR = [tot + "08" + "3F 25 91 01 97 54 46 59",
#                      tot + "08" + "3F 55 01 01 15 66 82 20",
#                      tot + "08" + "4F 55 01 01 15 66 82 10",
#                      tot + "08" + "3F 58 43 20 70 03 41 30",
#                      tot + "08" + "4F 58 43 20 70 03 41 30",
#                      tot + "08" + "3F 25 91 01 97 54 46 59",
#                      tot + "08" + "4F 25 91 01 97 54 46 59"]

    deletedEfsMandatoryDataImeiTR = tot + "08" + "3F 25 91 01 97 54 46 59"

    deletedEfsMandatoryStatusTR = "830100"

    deletedEfsMandatoryExpectedIPS = ["",
                           "FF FF FF FF",
                           "FF FF FF FF"]

    deletedEfsMandatoryExpectedIPD = ["80 08 3F 25 91 01 97 54 46 59",
                           "",
                           "80 08 3F 25 91 01 97 54 46 59"]


    deletedEfsMandatorySelectSWIPD = [OK,
                                      FILE_NOT_FOUND,
                                      OK
                                      ]
    deletedEfsMandatorySWIPD = [OK,
                                "6C04",#COMMAND_NOT_ALLOWED,
                                OK]

    deletedEfsMandatorySelectSWIPS = [FILE_NOT_FOUND,
                                OK,
                                OK]
    deletedEfsMandatorySWIPS = [COMMAND_NOT_ALLOWED,
                                OK,
                                OK]

    deletedEfsMandatoryHeaderDataTR = "8103 012601" + DEVICE_ID_ME_TO_SIM

    IMEI_ONLY_WHITE_LIST_DATA = ["80 10 3F259101975446093F25910197544689",
                      "80 10 3F550101156682103F55010115668290",
                      "80 10 3F584320700341303F58432070034130",
                      "80 10 3F544060121180203F54406012118020"]

    deletedEfsMandatoryCheckIPSPos = ["01",
                                      "02",
                                      "03"]

#    deletedEfsMandatoryCheckIPDPos = ["01",
#                                  "01",
#                                  "01"]
    deletedEfsMandatoryCheckIPDPos = "01"

    if(DEF("T1_PROTOCOL")):
        deletedEfsMandatorySWAuth = [COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED]
    else:
        deletedEfsMandatorySWAuth = [COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED,
                          COND_USE_NOT_SATISFIED]

    deletedEfsMandatorySWResp4Auth = [TECHNICAL_ERROR,
                           TECHNICAL_ERROR,
                           TECHNICAL_ERROR]

    deletedEfsMandatoryExpDataResp4Auth = ["",
                                            "",
                                            ""]

    returnOfImeiOnlyParameter = {"deletedEfsMandatoryDataImeiTR":deletedEfsMandatoryDataImeiTR,
                                 "deletedEfsMandatoryStatusTR":deletedEfsMandatoryStatusTR,
                                 "deletedEfsMandatoryExpectedIPS":deletedEfsMandatoryExpectedIPS,
                                 "deletedEfsMandatoryExpectedIPD":deletedEfsMandatoryExpectedIPD,
                                 "deletedEfsMandatoryHeaderDataTR":deletedEfsMandatoryHeaderDataTR,
                                 "IMEI_ONLY_WHITE_LIST_DATA":IMEI_ONLY_WHITE_LIST_DATA,
                                 "deletedEfsMandatorySWIPD":deletedEfsMandatorySWIPD,
                                 "deletedEfsMandatorySWAuth":deletedEfsMandatorySWAuth,
                                 "deletedEfsMandatorySWResp4Auth":deletedEfsMandatorySWResp4Auth,
                                 "deletedEfsMandatoryCheckIPSPos":deletedEfsMandatoryCheckIPSPos,
                                 "deletedEfsMandatoryExpDataResp4Auth":deletedEfsMandatoryExpDataResp4Auth,
                                 "deletedEfsMandatorySWIPS":deletedEfsMandatorySWIPS,
                                 "deletedEfsMandatorySelectSWIPS":deletedEfsMandatorySelectSWIPS,
                                 "deletedEfsMandatorySelectSWIPD":deletedEfsMandatorySelectSWIPD,
                                 "deletedEfsMandatoryCheckIPDPos":deletedEfsMandatoryCheckIPDPos
                                 }

    return returnOfImeiOnlyParameter

'''IPD MORE THAN IPS REC PARAMETER'''



def setTerminalProfile(kindIMEI):
    if(kindIMEI == "IMEI"):
        TERMINAL_PROFILE_DATA_USAT_Pairing       = "13ffff ff ff7f3f00dfff00001fe28a0d02 07 0900ffffffffffffffffff"
#        TERMINAL_PROFILE_DATA_USAT_Pairing       = "13ffff 40 ff7f3f00dfff00001fe28a0d02 07 0900ffffffffffffffffff"
        TERMINAL_PROFILE_FETCH_DATA = "D009 8103 012601" + DEVICE_ID_SIM_TO_ME
        TERMINAL_PROFILE_TR = "8103 012601" + DEVICE_ID_ME_TO_SIM + "14 10 333532313931303739343536343938FF"

        tpData = {"tpData":TERMINAL_PROFILE_DATA_USAT_Pairing,
        "tpFetchData":TERMINAL_PROFILE_FETCH_DATA,
        "tpTRData":TERMINAL_PROFILE_TR
        }

    elif(kindIMEI == "IMEI_SV"):
        TERMINAL_PROFILE_DATA_USAT_Pairing       = "13ffff BF ff7f3f00dfff00001fe28a0d02 47 0900ffffffffffffffffff"
#        TERMINAL_PROFILE_DATA_USAT_Pairing       = "13ffff 00 ff7f3f00dfff00001fe28a0d02 47 0900ffffffffffffffffff"
        TERMINAL_PROFILE_FETCH_DATA = "D009 8103 012608" + DEVICE_ID_SIM_TO_ME
        TERMINAL_PROFILE_TR = "8103 012608" + DEVICE_ID_ME_TO_SIM + "62 10 33353231393130373934353634393130"

        tpData = {"tpData":TERMINAL_PROFILE_DATA_USAT_Pairing,
        "tpFetchData":TERMINAL_PROFILE_FETCH_DATA,
        "tpTRData":TERMINAL_PROFILE_TR
        }

    elif(kindIMEI == "IMEI_AND_IMEI_SV"):
        TERMINAL_PROFILE_DATA_USAT_Pairing       = "13ffff ff ff7f3f00dfff00001fe28a0d02 47 0900ffffffffffffffffff"
#        TERMINAL_PROFILE_DATA_USAT_Pairing       = "13ffff 40 ff7f3f00dfff00001fe28a0d02 47 0900ffffffffffffffffff"
        TERMINAL_PROFILE_FETCH_DATA = "D009 8103 012608" + DEVICE_ID_SIM_TO_ME
        TERMINAL_PROFILE_TR = "8103 012608" + DEVICE_ID_ME_TO_SIM

        tpData = {"tpData":TERMINAL_PROFILE_DATA_USAT_Pairing,
        "tpFetchData":TERMINAL_PROFILE_FETCH_DATA,
        "tpTRData":TERMINAL_PROFILE_TR
        }
    else:
        TERMINAL_PROFILE_DATA_USAT_Pairing       = "13ffff ff ff7f3f00dfff00001fe28a0d02 47 0900ffffffffffffffffff"
        TERMINAL_PROFILE_FETCH_DATA = "D009 8103 012608" + DEVICE_ID_SIM_TO_ME
        TERMINAL_PROFILE_TR = "8103 012608" + DEVICE_ID_ME_TO_SIM

        tpData = {"tpData":TERMINAL_PROFILE_DATA_USAT_Pairing,
                "tpFetchData":TERMINAL_PROFILE_FETCH_DATA,
                "tpTRData":TERMINAL_PROFILE_TR
                }

    return tpData



def calculateCRC16(value):
    Buffer.I.SetBuffer(value)
    Call("cmd_tools.make_crc16_python_cmd")
    crc16CheckSum = Buffer.I.Get()
    LogInfo("Hasil crc16CheckSum= "+crc16CheckSum)
    return crc16CheckSum

def authAlgoChoosenAKA():
    LogInfo("def authAlgoChoosen:")
    '''F7 = TUAK'''
    '''F0 = Milenage'''

    LogInfo("''' VERIFY the ISC1 secret code '''")
    Mobile.VerifyCode(ISC1_REF + "08" + ISC1, expectedStatus = OK)

    LogInfo("'''Check before updated'''")
    Mobile.Select(EF_NATIVE_APPL, expectedStatus = "9FXX")
    Mobile.ReadRecord("03", "04", "19", expectedData=None, expectedStatus=OK)

    LogInfo("''' TUAK  algo chosen '''")
    DefineConst("ALGO_3G_TUAK", "00")
    DefineConst("ALGO_CHOSEN", ALGO_3G + ALGO_3G_TUAK)
    DefineConst("RES_LENGTH", "04")

    LogInfo("''' cmd_tools.change_algo_python_cmd '''")
    Call("cmd_tools.change_algo_python_cmd", ListOn = False)

    LogInfo("'''Check after updated'''")
    sw = Mobile.Select(EF_NATIVE_APPL, expectedStatus = "9FXX")
    Mobile.ReadRecord("03", "04", "19", expectedData=None, expectedStatus=OK)
    ADMCommands.writeLock("0018", "01"+"04", expectedData=None, expectedStatus=OK, sendApdu=True)


def authPersoAlgoTestAKA():
    Buffer.G.SetBuffer( rand + key )

    # ;'* Step 1: XDOUT[bits 0,1, . . .126,127]    =    K [bits 0,1, . . .126,127] XOR RAND[bits 0,1, . . .126,127]
    Dll.CALCUL.set_key(" G(17:24) "  )
    Dll.CALCUL.set_data("  G(1:8) "  )
    Dll.CALCUL.xor(" I(1:) 00 "  )

    Dll.CALCUL.set_key(" G(25:32) "  )
    Dll.CALCUL.set_data("  G(9:16) "  )
    Dll.CALCUL.xor(" I(9:) 00 "  )


    # ;'* Step 2: RES (test USIM), XRES (SS), CK, IK and AK are extracted from XDOUT this way:

    # ;'* RES[bits 0,1, . . .n-1,n]         =     f2(XDOUT,n)     =     XDOUT[bits 0,1, . . .n-1,n]   (with 30 < n < 128)
    # ;'* for this example n=31 (default value of the lock)
    DefineConst("RES", Buffer.I.Get(start = 1, length = 4))


    # ;'* CK[bits 0,1, . . .126,127]     =     f3(XDOUT)         =     XDOUT[bits 8,9, . . .126,127,0,1, . . .6,7]
    Buffer.J.SetBuffer( Buffer.I.Get(start = 2, end = 16) + Buffer.I.Get(start = 1, end = 1) )
    DefineConst("CK", Buffer.J.Get(start = 1, end = 16))

    # ;'* IK[bits 0,1, . . . 126,127]     =     f4(XDOUT)         =     XDOUT[bits 16,17, . . .126,127,0,1, . . .14,15]
    Buffer.K.SetBuffer( Buffer.I.Get(start = 3, end = 16) + Buffer.I.Get(start = 1, end = 2) )
    DefineConst("IK", Buffer.K.Get(start = 1, end = 16))

    # ;'* AK[bits 0,1, . . . 46,47]         =     f4(XDOUT)         =     XDOUT[bits 24,25, . . .70,71]
    DefineConst("AK", Buffer.I.Get(start = 4, end = 9))


    # ;'* Step 3: Concatenate SQN with AMF to obtain CDOUT like this:
    # ;'* CDOUT[bits 0,1,. . .62,63]    =    SQN[bits 0,1,. . .46,47] || AMF[bits 0,1,. . .14,15]


    # ;'* Step 4: XMAC (test USIM) and MAC (SS) are calculated from XDOUT and CDOUT this way:
    # ;'* XMAC[bits 0,1, . . .62, 63]     =     f1(XDOUT, CDOUT)      =    XDOUT[bits 0,1. . .62,63] XOR CDOUT[bits 0,1,. . .62,63]

    Dll.CALCUL.set_key("  " + sqn + amf  )
    Dll.CALCUL.set_data(" I(1;8) "  )
    Dll.CALCUL.xor(" N(1;8) 00  "  )

    DefineConst("MAC", Buffer.N.Get(start = 1, length = 8))

    # ;'* Step 5: AUTN[bits 0,1,..126,127]     =     SQN xor AK[bits 0,1,. . .46,47] || AMF[bits 0,1,. . .14,15] || MAC[bits 0,1, . . .62, 63]

    Dll.CALCUL.set_key(" I(4:9) 00 00 "  )
    Dll.CALCUL.set_data(sqn + "00 00"  )
    Dll.CALCUL.xor(" H(1:) 00 "  )

    DefineConst("SQN_xor_AK", Buffer.H.Get(start = 1, end = 6))

    # ;'* Conversion function definition

    # ;'* c2 definition
    # ;'* SRES = RES1 xor (RES2 xor (RES3 xor (RES4)))

    #DefineConst("SRES", RES)
    DefineConst("SRES", GetConst("RES"))


    # ;'* c3 definition
    # ;'* Kc = CK1 xor CK2 xor IK1 xor IK2
    Dll.CALCUL.set_key(" J(9;8) "  )
    Dll.CALCUL.set_data(" J(1;8) "  )
    Dll.CALCUL.xor(" N(1;8) 00 "  )

    Dll.CALCUL.set_key(" K(9;8) "  )
    Dll.CALCUL.set_data(" K(1;8) "  )
    Dll.CALCUL.xor(" N(9;8) 00 "  )

    Dll.CALCUL.set_key(" N(9;8) "  )
    Dll.CALCUL.set_data(" N(1;8) "  )
    Dll.CALCUL.xor(" M(1;8) 00 "  )

    DefineConst("Kc", Buffer.M.Get(start = 1, length = 8))