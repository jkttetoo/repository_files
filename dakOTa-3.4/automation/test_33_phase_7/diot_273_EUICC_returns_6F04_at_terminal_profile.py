from Ot.PyCom import *
import sys
from Oberthur import *
import os

cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)

import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
from model.euicc import Isdr as ISDR
import unittest
import model
from elevate import elevate
from Ot.PCOM32Manager import *
from time import time

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'hasbi.firmansyah@idemia.com'
'''
__date__ = 200180709
__purpose__= check delete profile 17 after emergency profile 17 activate

'''

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO_SERVICE = model.AutoService()
		ISDR					= ISDR()

		EXTERNAL_ID             = 'DK-4468'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
		#why its failed if I use SetLogLevel here?
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO_SERVICE = model.AutoService()
		GLONASS = model.EraGlonass()
		ISDR					= ISDR()

		EXTERNAL_ID             = 'DK-4468'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto4(DK34NG.P4S1RC)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'
		Constants.AID_ISDP17				= 'A0000005591010FFFFFFFF8900001700'
		Constants.AID_ISDP18				= 'A0000005591010FFFFFFFF8900001800'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_applet_profileOnly_15_HEXA_V1F_dummy_v3_V2_1.txt'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4468'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = True
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto4(DK34NG.P4S1RC)'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP17				= 'A0000005591010FFFFFFFF8900001700'
	Constants.AID_ISDP18				= 'A0000005591010FFFFFFFF8900001800'	
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_applet_profileOnly_15_HEXA_V1F_dummy_v3_V2_1.txt'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Auto Service Activate Emergency Profile 17 Delete Profile 17"

# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'

class SetEmergencyProfile(OtTestUnit):
	result_test = None
	status_test = None
	EXECUTION_DURATION = None
	flag=''

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 2)

	def setUp(self):
		pprint.h1(testName)
		pprint.h2("setup")
		try:

			pprint.h1('PERSO')
			aManager = PCOM32Manager("C:/Program Files (x86)/IDEMIA/PCOM32/PCOM32.exe")

			path_db = "c:\dakOTa-3.4\\perso\\"
			joined_path = os.path.join(path_db, "Erase_and_Do_Perso.pcom")
			LogInfo(joined_path)

			elevate(show_console=False, graphical=False)
			aManager.launchPCOM32(joined_path)

			PowerOff()
			PowerOn()
			EUICC.init()
				
		except:
			exceptionTraceback(position = 1)
			self.result_test, self.status_test = resultPassed(testName, False)

	def test_create_isdp(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			
			pprint.h3("Send EUCC RESET")
			capdu, rapdu, sw = ISDR.scp03(sd=ISDR.sd, capdus=EUICC.reset(), payload=False)
			if sw[:2] == '90':
				self.flag+='1'
			
			EUICC.init()
			pprint.h3("create isdp and establish keyset profile 16")
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP16, scp=80, apdu_format='definite')

			pprint.h3("download profile 16")
			SERVER.download_profile(Constants.AID_ISDP16, saipv2_txt=Constants.SAIP_TEST)

			pprint.h3("Set Emergency Profile to ISDP 16")
			capdu, rapdu, sw = SERVER.ram(GSMA.set_emergency_profile(Constants.AID_ISDP16), 80, apdu_format='indefinite')
			if '9000' in rapdu:
				self.flag+='2'

			pprint.h3('Set fallback ISDP 16')
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.AID_ISDP16, scp=80, apdu_format='definite')
			if '9000' in rapdu:
				self.flag+='3'
			
			pprint.h3("Send EUCC RESET")
			capdu, rapdu, sw = ISDR.scp03(sd=ISDR.sd, capdus=EUICC.reset(), payload=False)
			if sw[:2] == '90':
				self.flag+='4'

			pprint.h3('Device Reset')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			if sw[:2] != '6F':
				self.flag+='5'

			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			if self.flag == '12345':
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				pprint.h3('RESULT : {}'.format(self.flag))
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		except:			
			exceptionTraceback(position = 1)
			self.result_test, self.status_test = resultPassed(testName, False)

	def tearDown(self):
		h2("teardown")
		try:
			pprint.h1('PERSO')
			PowerOff()
			PowerOn()
			aManager = PCOM32Manager("C:/Program Files (x86)/IDEMIA/PCOM32/PCOM32.exe")

			path_db = "c:\dakOTa-3.4\\perso\\"
			joined_path = os.path.join(path_db, "Erase_and_Do_Perso.pcom")
			LogInfo(joined_path)

			elevate(show_console=False, graphical=False)
			aManager.launchPCOM32(joined_path)

			PowerOff()
			PowerOn()
		except:
			exceptionTraceback(position = 3)
			self.result_test, self.status_test = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.status_test, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(SetEmergencyProfile)