from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time


__author__ = 'Muhammad Hasbi Firmansyah'
__maintainer__ = 'Prastoto Aji'
__status__ = 'dev'
__version__ = '0.1.0'
__email__ = 'm.firmansyah@oberthur.com'

Options.counters = {}

if Constants.displayAPDU != None:
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER = model.Smsr()
		EUICC = model.Euicc()
		GSMA = model.Gsma()
		DEVICE = model.Device()
		LPin = model.LocationPlugin()
		TL_SERVER = model.TestlinkServer()
		INGENICO = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID = 'DAKO-4276'
		EXECUTION_STATUS = None
		EXECUTION_DURATION = None
	else:
		SERVER = model.Smsr()
		EUICC = model.Euicc()
		GSMA = model.Gsma()
		LPin = model.LocationPlugin()
		DEVICE = model.Device()
		TL_SERVER = model.TestlinkServer()
		INGENICO = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID = 'DAKO-4276'
		EXECUTION_STATUS = None
		EXECUTION_DURATION = None

		displayAPDU(True)
		SetLogLevel('debug')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		# Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_REPORT = False
		Constants.TESTLINK_DEVKEY = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM = 'IO222'
		Constants.TESTLINK_TEST_PLAN = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16 = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10 = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11 = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12 = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13 = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14 = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00 = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01 = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02 = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04 = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE = 10
		# Constants.ITERATIONS_STRESS         = 100 #max 9
		Constants.ITERATIONS_STRESS = 2  # max 9
		Constants.RESULTS = {
												'test_name': ['Test name'],
												'test_result': ['Test result']
												}
		Constants.case = 0
except:
	SERVER = model.Smsr()
	EUICC = model.Euicc()
	GSMA = model.Gsma()
	DEVICE = model.Device()
	TL_SERVER = model.TestlinkServer()
	INGENICO = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID = 'DAKO-4276'
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT = False
	Constants.TESTLINK_DEVKEY = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD = 'build_for_test'
	Constants.TESTLINK_PLATFORM = 'IO222'
	Constants.TESTLINK_TEST_PLAN = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP16 = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10 = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11 = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12 = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13 = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14 = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00 = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01 = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02 = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04 = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE = 1
	Constants.ITERATIONS_STRESS = 25
	Constants.RESULTS = {
											'test_name': ['Test name'],
											'test_result': ['Test result']
											}
	Constants.case = 0

testName = "Loop: {Enable profile, Fallback because network loss}"

# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'


class EnableFallbackNetworkLoss(OtTestUnit):
	EXECUTION_STATUS = None
	result = None
	EXECUTION_DURATION = None

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD,user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position=4)
			result, EXECUTION_STATUS = resultPassed(testName, False)

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			EUICC.init()
			LPin.location_plugin_deactivation(interface='applet', scp=80, apdu_format='definite')
			
			EUICC.init(catch_first_notif=True)
			pprint.h3('Configure FRM on')
			capdu, rapdu, sw = SERVER.frm_on(scp=80, apdu_format='definite', chunk='01')

			pprint.h3('Configure APF on')
			capdu, rapdu, sw = SERVER.apf_on(80)


			pprint.h3('Pre-condition #1 Block')
			EUICC.init()
			pprint.h3('Create ISDP 12')
			SERVER.create_isdp_and_establish_keyset(Constants.DEFAULT_ISDP12, scp=81, apdu_format='definite')
			SERVER.download_profile(Constants.DEFAULT_ISDP12, saipv2_txt=Constants.SAIP_TEST)

			pprint.h3('Create ISDP 13')
			SERVER.create_isdp_and_establish_keyset(Constants.DEFAULT_ISDP13, scp=81, apdu_format='definite')
			SERVER.download_profile(Constants.DEFAULT_ISDP13, saipv2_txt=Constants.SAIP_TEST)

			# pprint.h3('Enable ISDP 13')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP13), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))

			# pprint.h3('Enable ISDP 12')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP12), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))
			
			# pprint.h3('Disable ISDP 12')
			# capdu, rapdu, sw = SERVER.disable_profile(Constants.DEFAULT_ISDP12, scp=81, network_service=False, apdu_format='definite')

			# pprint.h3('Enable ISDP 12')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP12), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))
			
			# pprint.h3('Enable ISDP 11')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))
			
			# pprint.h3('Delete ISDP 12')
			# command_script, por, sw = SERVER.delete_profile(Constants.DEFAULT_ISDP12, scp=81, apdu_format='definite')
			
			pprint.h3('Create ISDP 15')
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)

			pprint.h3('Set Fallback into ISDP 15')
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.AID_ISDP, scp=81)			

			pprint.h3('Trigger Fallback')
			data, sw = DEVICE.envelope_location_status(status='no service')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			data, sw = DEVICE.envelope_timer_expiration(id='psm')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			
			pprint.h3('Pre-condition #2 Block')
			EUICC.init()
			data, sw = DEVICE.envelope_location_status()
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms', 'refresh'])
				if last_proactive_cmd == 'open channel':	
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
			
			# pprint.h3('Enable ISDP 13')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP13), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))

			# pprint.h3('Enable ISDP 11')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))
			
			# pprint.h3('Enable ISDP 15')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))
			
			# pprint.h3('Enable ISDP 11')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))
			
			# pprint.h3('Enable ISDP 15')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))

			# pprint.h3('Enable ISDP 13')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP13), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))

			# pprint.h3('Enable ISDP 11')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))

			# pprint.h3('Enable ISDP 15')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))
			
			# pprint.h3('Enable ISDP 13')
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP13), 81, apdu_format='indefinite', chunk='01')
			# if '9000' in rapdu :
			# 	sw = SERVER.euicc.refresh(sw, not bool(1))
			
			# pprint.h3('Trigger Fallback')
			# data, sw = DEVICE.envelope_location_status(status='no service')
			# if sw != '9000':
			# 	data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			# data, sw = DEVICE.envelope_timer_expiration(id='psm')
			# if sw != '9000':
			# 	data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			
			pprint.h3('Pre-condition #3 Block')
			EUICC.init()
			pprint.h3('Enable ISDP 13')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP13), 81, apdu_format='indefinite', chunk='01')
			if '9000' in rapdu :
				sw = SERVER.euicc.refresh(sw, not bool(1))

			pprint.h3('Enable ISDP 11')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 81, apdu_format='indefinite', chunk='01')
			if '9000' in rapdu :
				sw = SERVER.euicc.refresh(sw, not bool(1))
			
			pprint.h3('Set Fallback into ISDP 13')
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.DEFAULT_ISDP13, scp=81)

			pprint.h3('Trigger Fallback')
			data, sw = DEVICE.envelope_location_status(status='no service')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			# data, sw = DEVICE.envelope_timer_expiration(id='psm')
			# if sw != '9000':
			# 	data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
		
		except:
			exceptionTraceback(position = 1)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_enable_rollback_network_loss_disable_profile(self):
		h2("testbody")
		timer_start = time()
		try:			
			pprint.h3('eUICC Re-boot ')
			EUICC.init()

			pprint.h3('Trigger several location status event ')
			data, sw = DEVICE.envelope_location_status(status='no service')
			data, sw = DEVICE.get_status()
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)

			pprint.h3('Trigger Fallback ')
			data, sw = DEVICE.envelope_network_rejection()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)

			pprint.h3('Trigger several location status event')
			data, sw = DEVICE.envelope_location_status(status='no service')
			data, sw = DEVICE.get_status()
			data, sw = DEVICE.get_status()
			data, sw = DEVICE.envelope_network_rejection()
			data, sw = DEVICE.envelope_location_status(status='no service')
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			data, sw = DEVICE.get_status()
			data, sw = DEVICE.get_status()
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			data, sw = DEVICE.get_status()
			data, sw = DEVICE.get_status()
			data, sw = DEVICE.get_status()

			pprint.h3('Sent Audit enabled isdp')
			# capdu, rapdu, sw = SERVER.audit_isdp_enabled(81, apdu_format='indefinite')
			# capdu, rapdu, sw = SERVER.ram([GSMA.activate_apf],81, apdu_format='indefinite')
			# capdu, rapdu, sw = SERVER.set_fallback_attribute(aid_isdp=Constants.DEFAULT_ISDP11, scp=81)
			capdu, rapdu, sw = SERVER.set_fallback_attribute(aid_isdp=Constants.DEFAULT_ISDP11, scp=81)

			data, sw = DEVICE.get_status()
			data, sw = DEVICE.get_status()

			pprint.h3('Trigger location status event - deregister?')
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			pprint.h3('Enable Profile 15')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='indefinite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))
			
			pprint.h3('Trigger Fallback ')
			data, sw = DEVICE.envelope_location_status(status='no service')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			data, sw = DEVICE.envelope_location_status(status='no service')
			data, sw = DEVICE.envelope_location_status(status='no service')
		
			pprint.h3('Fallback occur after timer expired')
			data, sw = DEVICE.envelope_timer_expiration(id='psm')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')

			pprint.h3('device reset after fallback')
			EUICC.init()
			data, sw = DEVICE.envelope_location_status()
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms', 'refresh'])
				if last_proactive_cmd == 'open channel':	
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)

		
			if True :
				self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
		except:
			exceptionTraceback(position = 2)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')
		LogInfo('Result table')
		pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')

	def tearDown(self):
		h2("teardown")
		try:
			pass
		except:
			exceptionTraceback(position = 3)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(EnableFallbackNetworkLoss)
