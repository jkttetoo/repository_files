import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from elevate import elevate
from Ot.PCOM32Manager import *
from time import time



__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4767'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4767'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		# Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3(DK34NG.P3S2r10a)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 10
		# Constants.ITERATIONS_STRESS         = 100 #max 9
		Constants.ITERATIONS_STRESS         = 2 #max 9
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	INGENICO                = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID             = 'DK-4767'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = True
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS            = 25
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Loop: {Enable profile, Fallback because network loss}"

# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'

class EnableFallbackNetworkLoss(OtTestUnit):
	EXECUTION_STATUS = None
	result = None
	EXECUTION_DURATION = None
	flag=''

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)
			result, EXECUTION_STATUS = resultPassed(testName, False)

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			pprint.h1('PERSO')
			aManager = PCOM32Manager("C:/Program Files (x86)/IDEMIA/PCOM32/PCOM32.exe")

			path_db = "c:\dakOTa-3.4\\perso\\"
			joined_path = os.path.join(path_db, "Erase_and_Do_Perso.pcom")
			LogInfo(joined_path)

			elevate(show_console=False, graphical=False)
			aManager.launchPCOM32(joined_path)
			
			EUICC.init(catch_first_notif=True)
			# EUICC.init()
			pprint.h3('Create ISDP 15')
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')			
			pprint.h3('Download Test SAIP into ISDP 15')
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)

			pprint.h3('Create ISDP 16')
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP16, scp=81, apdu_format='definite')			
			pprint.h3('Download Test SAIP into ISDP 16')
			SERVER.download_profile(Constants.AID_ISDP16, saipv2_txt=Constants.SAIP_TEST)
			
			pprint.h3('Configure Switchback mechanism')
			SERVER.ram(GSMA.set_switchback_counter(counter='03'), scp=80)
			SERVER.ram(GSMA.switchback_timer(seconds='00', minutes='51', hours='00'), scp=80)		
			
			pprint.h3('Enable ISDP 15')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='indefinite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))
			
			pprint.h3('Configure FRM on')
			capdu, rapdu, sw = SERVER.frm_on(scp=80, apdu_format='definite', chunk='01')
			
			pprint.h3('Configure APF on')
			capdu, rapdu, sw = SERVER.apf_on(80)

		except:
			exceptionTraceback(position = 1)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_enable_rollback_network_loss_disable_profile(self):
		h2("testbody")
		timer_start = time()
		try:
			pprint.h3('Enable ISDP 16')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP16), 80, apdu_format='indefinite', chunk='01')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
				if last_proactive_cmd == 'refresh':
					self.flag+='1'
			
			pprint.h3('Active ISDP 16')
			EUICC.init()
			pprint.h3('Audit active profile - Expect ISDP 16')
			capdu, rapdu, sw = SERVER.audit_isdp_enabled(scp=80, apdu_format='definite')
			if '1600' in rapdu:
				self.flag+='2'
			pprint.h3('Trigger Normal Service')
			data, sw = DEVICE.envelope_location_status()
			pprint.h3('Exhaust All retry sent notif http')
			if sw!='9000':
				for i in range(3):
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
						for i in range(1):
							data, sw = DEVICE.envelope_timer_expiration(id='biplink')
							if sw != '9000':
								data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, open_channel_failed=True, send_sms_failed=True )
					data, sw = DEVICE.envelope_timer_expiration(id='psm')			
			
			pprint.h3('refresh')
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
				if last_proactive_cmd == 'refresh':
					self.flag+='3'
			
			pprint.h3('Active ISDP 15')
			EUICC.init()
			pprint.h3('Audit active profile - Expect ISDP 15')
			capdu, rapdu, sw = SERVER.audit_isdp_enabled(scp=80, apdu_format='definite')
			if '1500' in rapdu:
				self.flag+='4'

			pprint.h3('Trigger Normal Service')
			data, sw = DEVICE.envelope_location_status()
			pprint.h3('Exhaust All retry sent notif http')
			if sw!='9000':
				for i in range(3):
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
						for i in range(1):
							data, sw = DEVICE.envelope_timer_expiration(id='biplink')
							if sw != '9000':
								data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, open_channel_failed=True, send_sms_failed=True )
					data, sw = DEVICE.envelope_timer_expiration(id='psm')			
			
			pprint.h3('refresh')
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
				if last_proactive_cmd == 'refresh':
					self.flag+='5'
			
			pprint.h3('Active ISDP 11')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			data, sw, last_proactive_cmd_swb = DEVICE.fetch_all(sw,return_if=['timer management'])
			# # Used for DK 3.2
			if last_proactive_cmd_swb == 'timer management':
				if '240103' in data:
					timer_id = 'psm3'
				elif '240104' in data:
					timer_id = 'psm4'
				elif '240102' in data:
					timer_id = 'psm'
			
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			pprint.h3('Audit active profile - Expect ISDP 11')
			capdu, rapdu, sw = SERVER.audit_isdp_enabled(scp=80, apdu_format='definite')
			if '1100' in rapdu:
				self.flag+='6'
			pprint.h3('Exhaust All retry sent notif http')
			if sw!='9000':
				for i in range(3):
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
						for i in range(1):
							data, sw = DEVICE.envelope_timer_expiration(id='biplink')
							if sw != '9000':
								data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, open_channel_failed=True, send_sms_failed=True )
					data, sw = DEVICE.envelope_timer_expiration(id='psm')			
			
			pprint.h3('no refresh')
			if sw=='9000':
				self.flag+='7'
			
			pprint.h3('Expired SWB if any')
			if last_proactive_cmd_swb == 'timer management':
				data, sw = DEVICE.envelope_timer_expiration(id=timer_id)
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			else:
				self.flag+='8'
		  
						
			if self.flag == '12345678':
				self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				pprint.h3('RESULT: {}'.format(self.flag))
				self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
		except:
			exceptionTraceback(position = 2)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')
		LogInfo('Result table')
		pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')

	def tearDown(self):
		h2("teardown")
		try:
			pprint.h3('Setfallback ISDP 11')
			capdu, rapdu, sw = SERVER.set_fallback_attribute(aid_isdp=Constants.DEFAULT_ISDP11, scp=80)

			EUICC.init()
			capdu, rapdu, sw = SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='definite')
			capdu, rapdu, sw = SERVER.disable_profile(Constants.AID_ISDP16, scp=80, network_service=False, apdu_format='definite')
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='indefinite')
			command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')			
			command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP16, scp=80, apdu_format='definite')			
			DEVICE.get_status()
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='indefinite')
		except:
			exceptionTraceback(position = 3)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(EnableFallbackNetworkLoss)
