import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from elevate import elevate
from Ot.PCOM32Manager import *
from time import time



__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4776'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4776'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		# Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_2033					= '8934012031946000323.txt'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 10
		# Constants.ITERATIONS_STRESS         = 100 #max 9
		Constants.ITERATIONS_STRESS         = 2 #max 9
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	INGENICO                = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID             = 'DK-4776'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = True
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_2033					= '8934012031946000323.txt'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS            = 25
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Loop: {Enable profile, Fallback because network loss}"

# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'

class EnableFallbackNetworkLoss(OtTestUnit):
	EXECUTION_STATUS = None
	result = None
	EXECUTION_DURATION = None
	flag=''

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)
			result, EXECUTION_STATUS = resultPassed(testName, False)

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			EUICC.init()
		except:
			exceptionTraceback(position = 1)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_enable_rollback_network_loss_disable_profile(self):
		h2("testbody")
		timer_start = time()
		try:
			data, sw = DEVICE.envelope_location_status()
			data, sw = DEVICE.fetch_one(sw)
			data, sw = Fetch(sw[2:4])
			SendAPDU('80C2000020D51E820282818607914356060053F58604A12250F8130902F810630B25110102')
			sw = TerminalResponse('810301130002028281830100')
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'])
				if last_proactive_cmd == 'open channel':	
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw) 
			
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=80, apdu_format='definite')			
			
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_2033)
			# SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)

			EUICC.init()
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='indefinite', chunk='01')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')



			pprint.h3('After Enable Profile')
			EUICC.init()
			
			pprint.h3('LUS Normal Service')
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='timer management')
			
			pprint.h3('Sent SMS #1')
			data, sw_sms = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			data, sw = Envelope('D61399011282028381730183BF0103F40103F50107')
			data, sw = Envelope('D61099011282028381BF0103F40103F50107')
			data, sw = Envelope('D61099011282028381BF0103F40103F50107')
			sw = TerminalResponse('810301130002028281830120')
			
			pprint.h3('Sent SMS #2')
			data, sw_sms = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			data, sw = Envelope('D61399011282028381730183BF0103F40103F50107')
			sw = TerminalResponse('810301130002028281830120')

			pprint.h3('PLI 3x')
			for i in range(3):
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['p', 'provide local info'])

			pprint.h3('Open Channel - FAILED 3x')
			if sw!='9000':
				data, sw = Fetch(sw[2:4])
				data, sw = Envelope('D61099011282028381BF0103F40103F50107')
				sw = TerminalResponse('010301400102028281030120')
			for i in range(2):
				if sw!='9000':
					data, sw = Fetch(sw[2:4])
					sw = TerminalResponse('010301400102028281030120')
			
			pprint.h3('Fetch one - setup event list/poll')
			data, sw_fetch = DEVICE.fetch_one(sw)
			data, sw_fetch = DEVICE.fetch_one(sw_fetch)

			pprint.h3('Sent SMS #1 #2')
			data, sw = DEVICE.get_status()
			data, sw = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			sw = TerminalResponse('810301130002028281830120')
			data, sw = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			sw = TerminalResponse('810301130002028281830120')
			
			pprint.h3('Sent SMS #3 #4')
			data, sw = DEVICE.get_status()
			data, sw = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			sw = TerminalResponse('810301130002028281830120')
			data, sw = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			sw = TerminalResponse('810301130002028281830120')
			
			pprint.h3('Sent SMS #5 #6')
			data, sw = DEVICE.get_status()			
			data, sw = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			sw = TerminalResponse('810301130002028281830120')
			data, sw = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			sw = TerminalResponse('810301130002028281830120')
			
			pprint.h3('Timer expiration ID 1')
			data, sw = Envelope('D70C82028281A40101A503002000')
			if sw !='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			pprint.h3('Sent SMS #7 #8')
			data, sw = DEVICE.get_status()
			data, sw = Fetch(sw[2:4])
			if True:
				pprint.h3('Timer expiration ID 2')
				data, sw = DEVICE.envelope_timer_expiration(id='ota poller')				
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			sw = TerminalResponse('810301130002028281830120')
			data, sw = Fetch(sw[2:4])
			data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
			sw = TerminalResponse('810301130002028281830120')
			
			pprint.h3('Open Channel - FAILED 3x')
			for i in range(3):
				if sw!='9000':
					data, sw = Fetch(sw[2:4])
					sw = TerminalResponse('010301400102028281030120')
			
			pprint.h3('Sent SMS #9 #10')
			data, sw = DEVICE.get_status()
			data, sw = DEVICE.fetch_one(sw)
			data, sw = DEVICE.fetch_one(sw)
			if sw!='9000':
				data, sw = Fetch(sw[2:4])
				data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
				sw = TerminalResponse('810301130002028281830120')
				data, sw = Fetch(sw[2:4])
				data, sw = Envelope('D51E020282810607914356060013F10604812235F5130912F430045649A208E9')
				sw = TerminalResponse('810301130002028281830120')
			else:
				pprint.h3('Sent SMS not triggered')

			pprint.h3('Additional TimerExpired - SMS retry #1')
			data, sw = DEVICE.envelope_timer_expiration(id='ota poller')
			if sw != '9000':
				data, sw = Fetch(sw[2:4])
				sw = TerminalResponse('810301130002028281830120')
			if sw != '9000':
				data, sw = Fetch(sw[2:4])
				sw = TerminalResponse('810301130002028281830120')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			pprint.h3('Additional TimerExpired - SMS retry #2')
			data, sw = DEVICE.envelope_timer_expiration(id='ota poller')
			if sw != '9000':
				data, sw = Fetch(sw[2:4])
				sw = TerminalResponse('810301130002028281830120')
			if sw != '9000':
				data, sw = Fetch(sw[2:4])
				sw = TerminalResponse('810301130002028281830120')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			pprint.h3('Additional TimerExpired - refresh')
			data, sw = DEVICE.envelope_timer_expiration(id='ota poller')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			
			# pprint.h3('Polling off')
			# if sw!='9000':
			# 	data, sw = DEVICE.fetch_one(sw)
			
			# pprint.h3('status 3x')
			# for i in range(3):
			# 	data, sw = DEVICE.get_status()

			# pprint.h3('LUS normal service')
			# data, sw = DEVICE.envelope_location_status()
			# for i in range(3):
			# 	if sw!='9000':
			# 		data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['p','provide local info'])
			
			# pprint.h3('Status 10x')
			# for i in range(10):
			# 	data, sw = DEVICE.get_status()
			
			# if sw != '9000':
			# 	data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			# 	if last_proactive_cmd == 'refresh':
			# 		h3('THERE IS FRM PROCESS')

			if True:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				pprint.h3('RESULT: {}'.format(self.flag))
				self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
		except:
			exceptionTraceback(position = 2)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')
		LogInfo('Result table')
		pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')

	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()			
			capdu, rapdu, sw = SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='definite')
			command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
		except:
			exceptionTraceback(position = 3)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(EnableFallbackNetworkLoss)
