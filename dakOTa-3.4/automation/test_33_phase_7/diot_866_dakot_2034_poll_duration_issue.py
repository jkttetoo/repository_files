from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
from elevate import elevate
from Ot.PCOM32Manager import *
import model
from time import time

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()

		EXTERNAL_ID             = 'DK-4738'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()

		EXTERNAL_ID             = 'DK-4738'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4738'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

expectedAuditList = "A0000005591010FFFFFFFF89000015009F70013F"

testName = "mtr_003_MTRExpired_then_rollbackCounterElapsed_1"
# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'
class Test_mtr_003_MTRExpired_then_rollbackCounterElapsed_1(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None	
	flag =""
	status_counter = 0

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)          

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			pprint.h1('PERSO')
			aManager = PCOM32Manager("C:/Program Files (x86)/IDEMIA/PCOM32/PCOM32.exe")

			path_db = "c:\dakOTa-3.4\\perso\\"
			joined_path = os.path.join(path_db, "Erase_and_Do_Perso.pcom")
			LogInfo(joined_path)

			elevate(show_console=False, graphical=False)
			aManager.launchPCOM32(joined_path)


			EUICC.init()
			pprint.h3('Configure FRM on')
			capdu, rapdu, sw = SERVER.frm_on(80)
			
			pprint.h3('Configure APF on')
			capdu, rapdu, sw = SERVER.apf_on(80)

		except:
			exceptionTraceback(position = 1)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_mtr_003_MTRExpired_then_rollbackCounterElapsed_1(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			pprint.h3('Check Poll Interval on FNA')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['poll interval'])
			# # Used for DK 3.2
			if '840201' in data:
				self.flag+="1"
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)

			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'] )
				if last_proactive_cmd == 'open channel' or last_proactive_cmd == 'send sms':		
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)

			pprint.h3('Configure Poll Duration 5mins')
			data, rapdu, sw = SERVER.ram(GSMA.configure_poll_duration(interval='012C'), scp=80)
			if '9000' in rapdu:
				self.flag+="2"
				if sw != '9000':
					DEVICE.fetch_all(sw)
			
			pprint.h3('Create and enable ISDP 15')
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				if sw != '9000':
					self.flag+="3"
					DEVICE.fetch_all(sw)
			
			pprint.h3('Check Poll Interval After Enable')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['poll interval'])
			# # Used for DK 3.2
			if '84020005' in data:
				self.flag+="4"
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'] )
				if last_proactive_cmd == 'open channel' or last_proactive_cmd == 'send sms':		
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
			
			pprint.h3('Configure Poll Duration 6mins')
			data, rapdu, sw = SERVER.ram(GSMA.configure_poll_duration(interval='0168'), scp=80)
			if '9000' in rapdu:
				self.flag+="5"
				if sw != '9000':
					DEVICE.fetch_all(sw)
			
			pprint.h3('Disable ISDP 15')			
			capdu, rapdu, sw = SERVER.ram(GSMA.disable_profile(Constants.AID_ISDP), 81, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				if sw != '9000':
					DEVICE.fetch_all(sw)
		
			pprint.h3('Check Poll Interval After Disable')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['poll interval'])
			# # Used for DK 3.2
			if '84020006' in data:
				self.flag+="6"
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			pprint.h3('Configure Poll Duration 7mins')
			data, rapdu, sw = SERVER.ram(GSMA.configure_poll_duration(interval='01A4'), scp=80)
			if '9000' in rapdu:
				self.flag+="7"
				if sw != '9000':
					DEVICE.fetch_all(sw)
			
			pprint.h3('Exhaust All retry sent notif http')
			data, sw = DEVICE.envelope_location_status()
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'])
				for i in range(3):
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
						for i in range(1):
							data, sw = DEVICE.envelope_timer_expiration(id='biplink')
							if sw != '9000':
								data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, open_channel_failed=True, send_sms_failed=True )
					data, sw = DEVICE.envelope_timer_expiration(id='psm')
			
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			
			pprint.h3('Check Poll Interval After Rollback')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['poll interval'])
			# # Used for DK 3.2
			if '84020007' in data:
				self.flag+="8"
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'] )
				if last_proactive_cmd == 'open channel' or last_proactive_cmd == 'send sms':		
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
			
			pprint.h3('Configure Poll Duration 8mins')
			data, rapdu, sw = SERVER.ram(GSMA.configure_poll_duration(interval='01E0'), scp=80)
			if '9000' in rapdu:
				self.flag+="9"
				if sw != '9000':
					DEVICE.fetch_all(sw)
			
			########## APF v1 ##########
			# pprint.h3('Trigger Fallback')
			# data, sw = DEVICE.envelope_location_status(status='no service')
			# if sw!='9000':
			# 	data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='timer management')
			# 	if last_proactive_cmd == 'timer management':
			# 		data, sw = DEVICE.envelope_timer_expiration(id='psm')
			# 		if sw!='9000':
			# 			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			##############################
			
			########## APF v2 ##########			
			pprint.h3('Trigger APF - Limited Service')
			data, sw = DEVICE.envelope_limited_service()

			if sw != '9000': 				
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='poll interval')					
				if sw != '9000':
					DEVICE.fetch_all(sw)

			pprint.h3('Trigger STATUS 360x')
			while self.status_counter<=360:	
				self.status_counter+=1
				LogInfo('status {}'.format(self.status_counter))
				data, sw = DEVICE.get_status()

			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='p', pli_result='limited service', open_channel_failed=True, send_sms_failed=True )

			pprint.h3('Check Fallback')
			if sw != '9000':
				self.status_counter=0
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')						
			##############################
			
			pprint.h3('Check Poll Interval After Fallback')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['poll interval'])
			# # Used for DK 3.2
			if '84020008' in data:
				self.flag+="10"
			# if sw!='9000':
			# 	data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			# data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'] )
				if last_proactive_cmd == 'open channel' or last_proactive_cmd == 'send sms':		
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
				

			
			if self.flag == '12345678910':
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				pprint.h3('RESULT : {}'.format(self.flag))
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)


		except:
			exceptionTraceback(position = 2)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start
#        self.assertEqual(Constants.RESULTS['test_result'][Constants.case], 'OK')
		# LogInfo('Constants.RESULTS table')
		# pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')


	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
			SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
			SERVER.disable_profile(Constants.AID_ISDP16, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.delete_profile(Constants.AID_ISDP16, scp=80, apdu_format='definite')            
			# pprint.h3('Configure MTR')
			# SERVER.ram(GSMA.mtr_timer(seconds='00', minutes='00', hours='00'), scp=80)
			pprint.h3('Rollback Counter Configuration')
			EUICC.config_rollback_counter(counter='13')   
			DEVICE.get_status()
		except:
			exceptionTraceback(position = 3)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(Test_mtr_003_MTRExpired_then_rollbackCounterElapsed_1)