from Ot.PyCom import *
import sys
from Oberthur import *

import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)

from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model

import xmlrunner


__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'


EUICC 					= model.Euicc()
SERVER					= model.Smsr()
DEVICE 					= model.Device()
GSMA					= model.Gsma()
TL_SERVER			   	= model.TestlinkServer()
INGENICO				= model.IngenicoReader()
LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
GLONASS   				= model.EraGlonass()
HSM 					= model.HighStressMemory()

Constants.displayAPDU = True
displayAPDU(Constants.displayAPDU)
SetLogLevel('info')


''' TESTLINK VARIABLES '''
Constants.TESTLINK_REPORT			= False
#Constants.TESTLINK_REPORT			= True
Constants.TESTLINK_DEVKEY			= 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL				= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME		= 'DakOTa v3.1'
#Constants.TESTLINK_BUILD			= 'build_for_test'
Constants.TESTLINK_BUILD			= 'BUILD'
Constants.TESTLINK_PLATFORM			= 'IO222'
Constants.TESTLINK_TEST_PLAN		= 'eUICC Pre-integration'
Constants.TESTLINK_TESTER 			= 'm.firmansyah@oberthur.com'


'''
TEST VARIABLES

To launch the test, prepare the following variables :

Constants.AID_ISDP 			: It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
Constants.DEFAULT_ISDP10	: It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
Constants.DEFAULT_ISDP11	: It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
Constants.DEFAULT_ISDP12	: It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

'''
Constants.AID_ISDP 					= 'A0000005591010FFFFFFFF8900001500'
Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'
Constants.DEFAULT_ISDP10			= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11			= 'A0000005591010FFFFFFFF8900001100'
Constants.DEFAULT_ISDP12			= 'A0000005591010FFFFFFFF8900001200'
Constants.DEFAULT_ISDP13			= 'A0000005591010FFFFFFFF8900001300'
Constants.DEFAULT_ISDP14			= 'A0000005591010FFFFFFFF8900001400'
Constants.WRONG_ISDP				= 'A0000005591010FFFFFFFF8900001101'
Constants.SAIP_TEST 				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_HSM					= 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
Constants.SAIP_PCF00				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
#Constants.ITERATIONS_PERFORMANCE	= 5
#Constants.ITERATIONS_STRESS			= 100
Constants.ITERATIONS_PERFORMANCE	= 5
Constants.ITERATIONS_STRESS			= 100
Constants.RESULTS 					= {
										'test_name' 		: ['Test name'],
										'test_result'		: ['Test result']
										}

'''
PRE-CONDITION:

	1. Set fallback attribut on profile 11
	2. FRM status ON
	3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 12 state PERSONALIZED     --> Pre-created ISDP
4. Profile 13 state PERSONALIZED	 --> Pre-created ISDP
5. Profile 14 state PERSONALIZED	 --> Pre-created ISDP

=====================================

'''
Constants.case = 0
Constants.RunAll = True

class Preintegration_Test(unittest.TestCase):
	def test_USAT_Pairing_check_sending_IMEI_SV_registered_on_EF_IWL(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_IMEI_SV_registered_on_EF_IWL')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_sending_IMEI_registered_on_EF_IWL(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_IMEI_registered_on_EF_IWL')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_sending_IMEI_SV_not_registered_on_EF_IWL(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_IMEI_SV_not_registered_on_EF_IWL')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_sending_IMEI_not_registered_on_EF_IWL(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_IMEI_not_registered_on_EF_IWL')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_sending_TR_10(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_TR_10')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_sending_TR_11(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_TR_11')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_sending_TR_20_try_less_than_3_times(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_TR_20_try_less_than_3_times')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_sending_TR_20_try_more_than_3_times(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_TR_20_try_more_than_3_times')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_sending_TR_30(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_sending_TR_30')
		assert retValue == 0
		pass

	def test_USAT_Pairing_check_authentication_on_USAT_Pairing_and_SAIP_Profile(self):
		retValue = Run_ModuleMain('usat_pairing.USAT_Pairing_check_authentication_on_USAT_Pairing_and_SAIP_Profile')
		assert retValue == 0
		pass

if __name__ == '__main__':
    unittest.main(testRunner = xmlrunner.XMLTestRunner(output='C:\\ProjJenkin\\PreIntegration\\test-reports', verbosity=2))
