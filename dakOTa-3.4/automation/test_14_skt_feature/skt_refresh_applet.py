from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time



__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
            displayAPDU(Constants.displayAPDU)

try:
    if Constants.RunAll == True:
        SERVER                  = model.Smsr()
        EUICC                   = model.Euicc()
        GSMA                    = model.Gsma()
        DEVICE                  = model.Device()
        TL_SERVER               = model.TestlinkServer()

        EXTERNAL_ID             = 'DK-4303'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None
    else:
        SERVER                  = model.Smsr()
        EUICC                   = model.Euicc()
        GSMA                    = model.Gsma()
        DEVICE                  = model.Device()
        TL_SERVER               = model.TestlinkServer()

        EXTERNAL_ID             = 'DK-4303'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None

        displayAPDU(True)
        SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

        ''' TESTLINK VARIABLES '''
        Constants.TESTLINK_REPORT           = False
        Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
        Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
        Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
        Constants.TESTLINK_BUILD            = 'build_for_test'
        Constants.TESTLINK_PLATFORM         = 'IO222'
        Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
        Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

        '''
        TEST VARIABLES

        To launch the test, prepare the following variables :

        Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
        Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
        Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
        Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

        '''
        Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
        Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
        Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
        Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
        Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
        Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
        Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
        Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
        Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
        Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
        Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
        Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
        Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
        Constants.ITERATIONS_PERFORMANCE    = 1
        Constants.ITERATIONS_STRESS         = 1
        Constants.RESULTS                   = {
                                                'test_name'         : ['Test name'],
                                                'test_result'        : ['Test result']
                                                }
        Constants.case                      = 0
except:
    SERVER                  = model.Smsr()
    EUICC                   = model.Euicc()
    GSMA                    = model.Gsma()
    DEVICE                  = model.Device()
    TL_SERVER               = model.TestlinkServer()

    EXTERNAL_ID             = 'DK-4303'
    EXECUTION_STATUS        = None
    EXECUTION_DURATION      = None

    displayAPDU(True)
    SetLogLevel('info')
    SetCurrentReader('OMNIKEY CardMan 3x21 0')

    ''' TESTLINK VARIABLES '''
    Constants.TESTLINK_REPORT           = False
    Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
    Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
    Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
    Constants.TESTLINK_BUILD            = 'build_for_test'
    Constants.TESTLINK_PLATFORM         = 'IO222'
    Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
    Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

    '''
    TEST VARIABLES

    To launch the test, prepare the following variables :

    Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
    Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
    Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
    Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

    '''
    Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
    Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
    Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
    Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
    Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
    Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
    Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
    Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
    Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
    Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
    Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
    Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
    Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
    Constants.ITERATIONS_PERFORMANCE    = 1
    Constants.ITERATIONS_STRESS         = 1
    Constants.RESULTS                   = {
                                            'test_name'         : ['Test name'],
                                            'test_result'        : ['Test result']
                                            }
    Constants.case                      = 0


# update_adn = [
#         '00A40000023F00',
#         '00A40000027F10',
#         '00A40000025F3A',
#         '00A40000024F3A',
#         '00DC0704261111111111111111111111111111111111111111111111111111111111111111111111999999'
#         ]

# update_adn = [
#         '00A40000023F00',
#         '00A40000027F10',
#         '00A40000025F3A',
#         '00A40000024F3A'
#         ]

update_adn = [
        '0020000A083237333830343038',
        '00A4000C023F00',
        '00A4000C027F20',
        '00A4000C026F07',
        '00D60000080102030405060708'
        ]

# update_adn = [
#         '80A40000023F00',
#         '80A40000027F10',
#         '80A40000025F3A',
#         '80A40000024F3A'
#         ]

OTA		= model.SCP80()
Constants.SAIP_SKT = "[OT]_SKT_SAIP_V2_ASN1_(20180212)_from_mcc.der"
value_6f07 = "0849055069893945"
adm1 = "3237333830343038"
# def_ef_refresh = "BB 05 26 04 04 3F 00 2F 21 04 06 3F 00 7F 20 6F"+\
#                 "07 04 06 3F 00 7F FF 6F 07 04 06 3F 00 7F FF 6F"+\
#                 "61 04 06 3F 00 7F 20 6F 61"

def_ef_refresh = "BB 05 26"+\
                "04 04 3F 00 2F 21"+\
                "04 06 3F 00 7F 20 6F 07"+\
                "04 06 3F 00 7F FF 6F 07"+\
                "04 06 3F 00 7F FF 6F 61"+\
                "04 06 3F 00 7F 20 6F 61"

def_ef_refresh_config = "00 01 00 1E 05 25"+\
                        "00"+\
                        "00"+\
                        "10 A0 00 00 00 87 10 02 FF FF FF FF 89 04 03 00 FF"+\
                        "10 A0 00 00 00 87 10 02 FF FF FF FF 89 04 03 00 FF"+\
                        "00"

# scp03_sEnc="456E6372797074696F6E536563726574"
# scp03_sMac="506572736F6E616C69736174696F6E4B"
# scp03_sKek="4B6579456E6372797074696F6E4B6579"

scp03_kvn="30"
scp03_scp="0370"
scp03_sEnc="C1ABB59A5C53CA787B1062580852EEB3"
scp03_sMac="A9287EC31E6327E2960D2CA8D4CF9552"
scp03_sKek="203003180B4F794E126E889316575A08"

import const
from util.hexastring import subByte
import Utilities
# from all_function import *
import all_function

from Ot.GlobalPlatform import *

class SKTRefreshApplet(OtTestUnit):

    def report(self, execution_status, execduration):
        LogInfo('Start reporting test result...')
        TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
        LogInfo('Finished')

    # def setUp(self):
    #     pprint.h1('Enable profile SCP80 definite mode')
    #     EUICC.init()
    #     SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
    #     SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_SKT)
    #     capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')

    #     if '9000' in rapdu:
    #             sw = SERVER.euicc.refresh(sw, not bool(1))

    def setUp(self):
        pprint.h1('Install SKT Refresh Applet on ISDR')
        EUICC.init()
        LogInfo("Enable Profile ISDR")
        capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP10), 80, apdu_format='definite', chunk='01')
        if '9000' in rapdu:
                sw = SERVER.euicc.refresh(sw, not bool(1))
        # capdu, rapdu, sw = EUICC.mno.install_applet(scp=80, apdu_format='compact')

        # Create the SCP Keyset
        Send("00 A4 04 00 08 " + Constants.DEFAULT_ISDP10)
        ks = SCPKeyset(scp03_kvn, scp03_scp, scp03_sEnc, scp03_sMac, scp03_sKek)
        print("KS: ", ks)

        ## Create a Security Domain Object using its AID
        sd = IssuerSecurityDomain(Constants.DEFAULT_ISDP10, [ks])

        # Display ISD attibutes
        print("sd: ",sd)
        # PowerOn()
        # open Secure Channel using the keyset ks
        sd.openDefaultSecureChannel(keyset = ks, secureMessaging=CLEAR_LEVEL)
        # StepOn()


    def test_skt_refresh_applet(self):
        LogInfo('body part')
        timer_start = time()
        LogInfo("update APDU: "+ str(update_adn))
        # try:
        #         Constants.RESULTS['test_name'].append('Enable profile SCP80 definite mode')
        #         Constants.RESULTS['test_result'].append('OK')
        #         EXECUTION_STATUS = 'PASSED'
        #         Constants.case+=1
        #     else:
        #         Constants.RESULTS['test_name'].append('Enable profile SCP80 definite mode')
        #         Constants.RESULTS['test_result'].append('KO')
        #         EXECUTION_STATUS = 'FAILED'
        #         Constants.case+=1
        # except:
        #     Constants.RESULTS['test_name'].append('Enable profile SCP80 definite mode')
        #     Constants.RESULTS['test_result'].append('KO')
        #     EXECUTION_STATUS = 'FAILED'
        #     Constants.case+=1
        # timer_end = time()
        # EXECUTION_DURATION = timer_end - timer_start

        # update_and_read_1EF() #gak perlu

        EUICC.init()
        # StepOn()
        LogInfo("Read EF REFRESH")
        VerifyPin("0A"+lv(adm1), expectedStatus = "9000")
        Select("3F00", expectedStatus = "9000, 9XXX")
        Select("2F2F", expectedStatus = "9000, 9XXX")
        ReadBinary("00", "00", "29", expectedData = def_ef_refresh, expectedStatus="9000")
        # StepOn()

        LogInfo("Read EF REFRESH CONFIG")
        VerifyPin("0A"+lv(adm1), expectedStatus = "9000")
        Select("3F00", expectedStatus = "9000, 9XXX")
        Select("7FDF", expectedStatus = "9000, 9XXX")
        Select("17C3", expectedStatus = "9000, 9XXX")
        ReadBinary("00", "00", "2B",expectedData = def_ef_refresh_config, expectedStatus="9000")
        # StepOn()

        PowerOff()
        PowerOn()
        all_function.set_internal_flag("AA")
        # StepOn()
        # EUICC.init()
        # sw = TerminalProfile("FFFFFFFFFFFF1FFFFF03FEFFFF9FFFEF03FF0F000FFFE70F0F00003F7F03", expectedStatus="91XX")
        LogInfo("Send Terminal Profile: ")
        sw = TerminalProfile("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", expectedStatus="91XX")
        LogInfo("sw Terminal Profile: " + sw)
        LogInfo("fetch all terminal profile")
        while sw != "9000":
            print("Fetch")
            Fetch(sw[2:], expectedStatus = "9000")
            print("Terminal Response")
            sw = TerminalResponse("83 01 00", expectedStatus = "91XX, 9000")
        # StepOn()

        self.spi1 = 0x16
        self.spi2 = 0x21
        self.kic  = 0x15
        self.kid  = 0x15
        self.kic_key = '63044C5CD7A7DCDB5591C35193AAC51F'
        self.kid_key = '3A961E13148044902D0B9612CC5D63FC'
        self.tar  = "B00021"
        self.algo_cipher='05'
        self.algo_crypto_verif='05'
        self.tpoa = "0681214365"
        # LogInfo('SCP80: {}'.format(apdus))
        Dll.OTA2.init_env_0348('')
        Dll.OTA2.change_tar(self.tar)
        Dll.OTA2.change_counter(OTA.next_cntr())
        Dll.OTA2.change_cnt_chk(subByte(self.spi1,4,5))
        Dll.OTA2.change_cipher(subByte(self.spi1,3))
        Dll.OTA2.change_crypto_verif(subByte(self.spi1,1,2))
        Dll.OTA2.change_por_format(subByte(self.spi2,6))
        Dll.OTA2.change_por_cipher(subByte(self.spi2,5))
        Dll.OTA2.change_por_security(subByte(self.spi2,3,4))
        Dll.OTA2.change_por(subByte(self.spi2,1,2))
        if self.spi1 == 0x16: Dll.OTA2.change_algo_cipher(self.algo_cipher)
        Dll.OTA2.change_algo_crypto_verif(self.algo_crypto_verif)
        Dll.OTA2.change_kic(intToHexString(self.kic))
        Dll.OTA2.change_kid(intToHexString(self.kid))
        Dll.OTA2.set_dlkey_kic(self.kic_key)
        Dll.OTA2.set_dlkey_kid(self.kid_key)
        Dll.OTA2.change_tp_oa(self.tpoa)

        apdus = update_adn
        if isinstance(apdus, str):
            apdus = [apdus]

        command_script = ''

        for idx, apdu in enumerate(apdus):
            Dll.OTA2.append_script(apdu + "/EXPANDED")

        Dll.OTA2.end_message("GI")
        Dll.OTA2.get_message_nb("K")
        total_nb_of_sms = Buffer.K.Get()
        current_sms = '01'

        Dll.OTA2.display_message(Buffer.I.Get())

        LogInfo("### SEND SMS-OTA Update File 6F07")
        data, sw = Envelope(Buffer.I.Get(), "91XX,9000")

        LogInfo('{}/{} envelope sms-pp download ({})'.format(current_sms, total_nb_of_sms, sw))
        print("data: ", data)
        print("sw: ", sw)

        por, more_get_status, sw = DEVICE.fetch_por_sms(sw, self)
        # StepOn()

        if por == 'AB0780010123026985': LogInfo('6985, conditions of use not satisfied!')
        if por == 'AB07800101230269E1': LogInfo('PCF-Rules do not allow this request')

        print("por: ", por)
        print("sw: ", sw)
        LogInfo("Send Status Command: ")
        data = SendAPDU("80 F2 00 00 01", expectedStatus = "91XX")
        print("data: ", data)
        # StepOn()
        timer_end = time()
        EXECUTION_DURATION = timer_end - timer_start
        LogInfo('body part')

        LogInfo("Check updated value 3F00 - 7F20 - 6F07")
        VerifyPin("0A"+lv(adm1), expectedStatus = "9000")
        Select("3F00", expectedStatus = "9000, 9XXX")
        Select("7F20", expectedStatus = "9000, 9XXX")
        Select("6F07", expectedStatus = "9000, 9XXX")
        updatedData = ReadBinary("00", "00", Utilities.ComputeLengthHex(value_6f07), expectedData="0102030405060708", expectedStatus="9000")

        # StepOn()


        if por == "AB0780010523029000" and updatedData[1][0] == "0102030405060708":
            Constants.RESULTS['test_name'].append('Enable profile SCP80 definite mode')
            Constants.RESULTS['test_result'].append('OK')
            EXECUTION_STATUS = 'PASSED'
            Constants.case+=1
        else:
            Constants.RESULTS['test_name'].append('Enable profile SCP80 definite mode')
            Constants.RESULTS['test_result'].append('KO')
            EXECUTION_STATUS = 'FAILED'
            Constants.case+=1

        # if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
        if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

    def tearDown(self):
        EUICC.init()
        LogInfo("----- tearDown -----")
        LogInfo("### Enable Fallback Profile ")
        capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 80, apdu_format='definite', chunk='01')
        if '9000' in rapdu:
                sw = SERVER.euicc.refresh(sw, not bool(1))

        # SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
        # SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
        # DEVICE.get_status()

if __name__ == "__main__":

    from test import support
    Utilities.launchTest(SKTRefreshApplet)