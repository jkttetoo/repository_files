'''****************************************************************************************

@script    : all_function.py
@group     : Include_Files
@author    : Revina Febriani
@date      : Apr 23, 2014
@purpose   : all function needed for Softbank_Japan_USAT_OTA_Poller & Refresh testing
@destructive : no

*******************************************************************************************'''
from Oberthur import *
from Ot.PyCom import *
from Utilities import *
from Mobile import *
from HttpAmdB import *
from BipModule import *
from Tls import *
from Ot.GlobalPlatform import *
from OtOTA2Wrapper import *
import PushSms
import DDL

# from Include.config_ref import *
# from Include.variabel_setting import *
#from Include.include_OTA import *
global sd

DDL=DDL.DDL
bipChannel = BipClass()  
tlsServer =  TlsServer() 
httpClient = HttpClient()
httpserver = HttpServer()
httpmode = TEST_MODE      

value_6f07 = "0849055069893945"
adm1 = "3237333830343038"
'''******************************************FOR REFRESH APPLET************************************************************'''
def read_ef_biplink(SessionState):
    LogInfo("==============Read EF BIPLink==============")    
    ListOff()
    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("1570")
    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus=None)
    ListOn()
    if SessionState=="Open":
        Mobile.ReadBinary("00", "00", "01", expectedData='01', expectedStatus=None)     
    elif SessionState=="Close":
        Mobile.ReadBinary("00", "00", "01", expectedData='00', expectedStatus=None) 
    pass

def read_reset_loci(IsReset=None):
    LogInfo("==============Read EF LOCI in Reset==============")    
    ListOff()
    Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
    Mobile.Send('00A4000002'+'3F00')
    Mobile.Send('00A4000402'+'7FFF')
    Mobile.Send('00A4000002'+'6F7E')
    
    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus="9000 , 91XX")
    ListOn()
    if IsReset=='true':
        Mobile.ReadBinary("00", "00", "0B", expectedData='0000000000000000000000', expectedStatus="9000 , 91XX")  
    else:
        Mobile.ReadBinary("00", "00", "0B", expectedData='FFFFFFFF44F0020000FF01', expectedStatus="9000 , 91XX" )  
    pass

def read_reset_psloci(IsReset=None):
    LogInfo("==============Read EF PSLOCI in Reset==============")    
    ListOff()
    Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
    Mobile.Send('00A4000002'+'3F00')
    Mobile.Send('00A4000402'+'7FFF')
    Mobile.Send('00A4000002'+'6F73')
    
    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus="9000 , 91XX")
    ListOn()
    if IsReset=='true':
        Mobile.ReadBinary('00', '00', '0E', expectedData='1111111111111111111111111111', expectedStatus="9000 , 91XX")  
    else:
        Mobile.ReadBinary("00", "00", "0E", expectedData='FFFFFFFFFFFFFF44F0020000FF01', expectedStatus="9000 , 91XX")  
    pass

def read_reset_targeted_ef(IsEfRefresh="true",IsMsisdn="true",IsImsi="true",IsGid1="true",IsGid2="true",IsAcc="true",IsEcc="true",IsSpn="true",IsHrpdupp="true",IsAd=None):
    if IsEfRefresh=="true":
        LogInfo("==============read_reset_ef_refresh nilai 00==============")    
#        ListOff()
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7F8F')
        Mobile.Send('00A4000002'+'6FF3')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "06", "01", expectedData='00', expectedStatus='9000')  
    if IsMsisdn=="true":
        LogInfo("==============read_reset_ef_msisdn==============")    
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF1')
        Mobile.Send('00A4000002'+'6F40')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadRecord("01", "04", "27", expectedData='22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 ', expectedStatus='9000') 
        Mobile.ReadRecord("02", "04", "27", expectedData='22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 22 ', expectedStatus='9000') 
        Mobile.ReadRecord("03", "04", "27", expectedData='FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF ', expectedStatus='9000') 
    if IsImsi=="true": 
        LogInfo("==============read_reset_ef_Imsi==============")
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF1')
        Mobile.Send('00A4000002'+'6F07')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "09",  expectedData='08FFFFFFFFFFFFFFFF', expectedStatus='9000')

    if IsGid1=="true":
        LogInfo("==============read_reset_ef_gid1==============") 
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF1')
        Mobile.Send('00A4000002'+'6F3E')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "03", expectedData='111111', expectedStatus='9000')
    if IsGid2=="true": 
        LogInfo("==============read_reset_ef_gid2==============")
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF1')
        Mobile.Send('00A4000002'+'6F3F')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "03", expectedData='222222', expectedStatus='9000') 
    if IsAcc=="true": 
        LogInfo("==============read_reset_ef_acc==============")
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF1')
        Mobile.Send('00A4000002'+'6F78')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "02", expectedData='7777', expectedStatus='9000')
    if IsEcc=="true": 
        LogInfo("==============read_reset_ef_Ecc==============")
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF1')
        Mobile.Send('00A4000002'+'6FB7')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadRecord("01", "04", "1D", expectedData='0000000000000000000000000000000000000000000000000000000000', expectedStatus='9000') 
        Mobile.ReadRecord("02", "04", "1D", expectedData='1111111111111111111111111111111111111111111111111111111111', expectedStatus='9000') 
        Mobile.ReadRecord("03", "04", "1D", expectedData='3333333333333333333333333333333333333333333333333333333333', expectedStatus='9000') 
        Mobile.ReadRecord("04", "04", "1D", expectedData='3333333333333333333333333333333333333333333333333333333333', expectedStatus='9000') 
        Mobile.ReadRecord("05", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("06", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("07", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("08", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("09", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("0A", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
    if IsSpn=="true": 
        LogInfo("==============read_reset_ef_spn==============")
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF1')
        Mobile.Send('00A4000002'+'6F46')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "11", expectedData='8888888888888888888888888888888888', expectedStatus='9000') 
    if IsHrpdupp=="true": 
        LogInfo("==============read_reset_ef_Hrpdupp==============")
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 03 43 10 02 FF 81 FF 20 89 02 00 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF3')
        Mobile.Send('00A4000002'+'6F57')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "43", expectedData='FFFF 36343430323038313030303132333435406E61692E6570632E6D6E633032302E6D63633434302E336770706E6574776F726B2E6F726710FFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
    if IsAd=="true":
        LogInfo("==============read_reset_ef_ad==============")
#        ListOff()
#        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FF1')
        Mobile.Send('00A4000002'+'6FAD')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus= "9000")
        ListOn()
        Mobile.ReadBinary("00", "00", "04", expectedData='33333333', expectedStatus= "9000") 
    pass

def read_reset_targeted_add_ef_not_reset(IsEfRefresh=None,IsMsisdn="true",IsImsi="true",IsGid1="true",IsGid2="true",IsAcc="true",IsEcc="true",IsSpn="true",IsHrpdupp="true",IsAd=None):
    if IsEfRefresh=="true":
        LogInfo("==============read_reset_ef_refresh nilai 00==============")    
        ListOff()
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7F8F')
        Mobile.Send('00A4000002'+'6FF3')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "06", "01", expectedData='00', expectedStatus='9000')  
    if IsMsisdn=="true":
        LogInfo("==============read_reset_ef_msisdn==============")    
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6F40')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadRecord("01", "04", "27", expectedData='000000000000000000000000000000000000000000000000000000000000000000000000000000', expectedStatus='9000') 
        Mobile.ReadRecord("02", "04", "27", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("03", "04", "27", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
    if IsImsi=="true": 
        LogInfo("==============read_reset_ef_Imsi==============")
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6F07')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "09",  expectedData='084981041000200010', expectedStatus='9000')

    if IsGid1=="true":
        LogInfo("==============read_reset_ef_gid1==============") 
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6F3E')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "03", expectedData='00FFFF', expectedStatus='9000')
    if IsGid2=="true": 
        LogInfo("==============read_reset_ef_gid2==============")
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6F3F')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "03", expectedData='FFFFFF', expectedStatus='9000') 
    if IsAcc=="true": 
        LogInfo("==============read_reset_ef_acc==============")
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6F78')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "02", expectedData='0002', expectedStatus='9000')
    if IsEcc=="true": 
        LogInfo("==============read_reset_ef_Ecc==============")
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6FB7')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadRecord("01", "04", "1D", expectedData='11F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF01', expectedStatus='9000') 
        Mobile.ReadRecord("02", "04", "1D", expectedData='11F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF08', expectedStatus='9000') 
        Mobile.ReadRecord("03", "04", "1D", expectedData='11F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF06', expectedStatus='9000') 
        Mobile.ReadRecord("04", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("05", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("06", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("07", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("08", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("09", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
        Mobile.ReadRecord("0A", "04", "1D", expectedData='FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
    if IsSpn=="true": 
        LogInfo("==============read_reset_ef_spn==============")
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6F46')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "11", expectedData='02536F667442616E6BFFFFFFFFFFFFFFFF', expectedStatus='9000') 
    if IsHrpdupp=="true": 
        LogInfo("==============read_reset_ef_Hrpdupp==============")
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 03 43 10 02 FF 81 FF 20 89 02 00 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6F57')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "43", expectedData='383636343430323038313030303132333435406E61692E6570632E6D6E633032302E6D63633434302E336770706E6574776F726B2E6F726710FFFFFFFFFFFFFFFFFFFF', expectedStatus='9000') 
    if IsAd=="true":
        LogInfo("==============read_reset_ef_ad==============")
        ListOff()
        Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
        Mobile.Send('00A4000002'+'3F00')
        Mobile.Send('00A4000002'+'7FFF')
        Mobile.Send('00A4000002'+'6FAD')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus= "9000")
        ListOn()
        Mobile.ReadBinary("00", "00", "04", expectedData='00000002', expectedStatus= "9000") 
    pass

def read_EF_under_USIM(EF,value=None):
    Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
    Mobile.Send('00A4000002'+'3F00')
    Mobile.Send('00A4000002'+'7FFF')
    
    if EF == 'MSISDN':
        LogInfo("==============read_reset_ef_msisdn==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F40')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus="9000")
        ListOn()
        Mobile.ReadRecord("01", "04", "27", expectedData=value[0], expectedStatus='9000') 
        Mobile.ReadRecord("02", "04", "27", expectedData=value[1], expectedStatus='9000') 
        Mobile.ReadRecord("03", "04", "27", expectedData=value[2], expectedStatus='9000') 
    elif EF == 'IMSI':
        LogInfo("==============read_reset_ef_Imsi==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F07')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "09",  expectedData=value, expectedStatus='9000')
    elif EF == 'GID1':
        LogInfo("==============read_reset_ef_gid1==============") 
        ListOff()
        Mobile.Send('00A4000002'+'6F3E')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "03", expectedData=value, expectedStatus='9000')
    elif EF == 'GID2':
        LogInfo("==============read_reset_ef_gid2==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F3F')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "03", expectedData=value, expectedStatus='9000')
    elif EF == 'ACC':
        LogInfo("==============read_reset_ef_acc==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F78')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "02", expectedData=value, expectedStatus='9000')
    elif EF == 'ECC':
        LogInfo("==============read_reset_ef_Ecc==============")
        ListOff()
        Mobile.Send('00A4000002'+'6FB7')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadRecord("01", "04", "1D", expectedData=value[0], expectedStatus='9000') 
        Mobile.ReadRecord("02", "04", "1D", expectedData=value[1], expectedStatus='9000') 
        Mobile.ReadRecord("03", "04", "1D", expectedData=value[2], expectedStatus='9000') 
        Mobile.ReadRecord("04", "04", "1D", expectedData=value[3], expectedStatus='9000') 
        Mobile.ReadRecord("05", "04", "1D", expectedData=value[4], expectedStatus='9000') 
        Mobile.ReadRecord("06", "04", "1D", expectedData=value[5], expectedStatus='9000') 
        Mobile.ReadRecord("07", "04", "1D", expectedData=value[6], expectedStatus='9000') 
        Mobile.ReadRecord("08", "04", "1D", expectedData=value[7], expectedStatus='9000') 
        Mobile.ReadRecord("09", "04", "1D", expectedData=value[8], expectedStatus='9000') 
        Mobile.ReadRecord("0A", "04", "1D", expectedData=value[9], expectedStatus='9000') 
    elif EF == 'SPN':
        LogInfo("==============read_reset_ef_spn==============")
        ListOff()
        Mobile.Send('00A4000002'+'6FB7')
        Mobile.Send('00A4000002'+'6F46')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "11", expectedData=value, expectedStatus='9000') 
    elif EF == 'PLMNwACT':
        LogInfo("==============read_reset_ef_PLMNwACT==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F60')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "FA", expectedData=value, expectedStatus='9000') 
    elif EF == 'HPPLMN':
        LogInfo("==============read_reset_ef_HPPLMN ==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F31 ')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "01", expectedData=value, expectedStatus='9000') 
    elif EF == 'PUCT':
        LogInfo("==============read_reset_ef_PUCT==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F41')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "05", expectedData=value, expectedStatus='9000') 
    elif EF == 'FPLMN':
        LogInfo("==============read_reset_ef_FPLMN==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F7B')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "1E", expectedData=value, expectedStatus='9000') 
    elif EF == 'LOCI':
        LogInfo("==============read_reset_ef_LOCI==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F7E')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "0B", expectedData=value, expectedStatus='9000') 
    elif EF == 'AD':
        LogInfo("==============read_reset_ef_AD==============")
        ListOff()
        Mobile.Send('00A4000002'+'6FAD')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "04", expectedData=value, expectedStatus='9000') 
    elif EF == 'CBMID':
        LogInfo("==============read_reset_ef_CBMID==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F48')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "14", expectedData=value, expectedStatus='9000') 
    elif EF == 'CBMIR':
        LogInfo("==============read_reset_ef_CBMIR==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F50')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "28", expectedData=value, expectedStatus='9000') 
    elif EF == 'PSLOCI':
        LogInfo("==============read_reset_ef_PSLOCI==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F73')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "0E", expectedData=value, expectedStatus='9000') 
    elif EF == 'FDN':
        LogInfo("==============read_reset_ef_FDN==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F3B')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadRecord("01", "04", "2F", expectedData=value[0], expectedStatus='9000') 
        Mobile.ReadRecord("02", "04", "2F", expectedData=value[1], expectedStatus='9000') 
        Mobile.ReadRecord("03", "04", "2F", expectedData=value[2], expectedStatus='9000') 
        Mobile.ReadRecord("04", "04", "2F", expectedData=value[3], expectedStatus='9000') 
        Mobile.ReadRecord("05", "04", "2F", expectedData=value[4], expectedStatus='9000') 
        Mobile.ReadRecord("06", "04", "2F", expectedData=value[5], expectedStatus='9000') 
        Mobile.ReadRecord("07", "04", "2F", expectedData=value[6], expectedStatus='9000') 
        Mobile.ReadRecord("08", "04", "2F", expectedData=value[7], expectedStatus='9000') 
        Mobile.ReadRecord("09", "04", "2F", expectedData=value[8], expectedStatus='9000') 
        Mobile.ReadRecord("0A", "04", "2F", expectedData=value[9], expectedStatus='9000') 
    elif EF == 'EPRL':
        LogInfo("==============read_reset_ef_EPRL==============")
        ListOff()
        Mobile.Send('00A4000002'+'6F5A')
        Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
        ListOn()
        Mobile.ReadBinary("00", "00", "FA", expectedData=value, expectedStatus='9000') 
    else:
        print('EF not found')
    pass

def update_EF_under_USIM(EF,value=None):
    Mobile.Send("00A4040C10 A0 00 00 00 87 10 02 FF 81 FF 20 89 03 06 00 00")
    Mobile.Send('00A4000002'+'3F00')
    Mobile.Send('00A4000002'+'7FFF')
    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus=None)
    if EF == 'MSISDN':
        Mobile.Send('00A4000002'+'6F40')
        Mobile.ReadRecord("01", "04", "27", value, "9000")
    elif EF == 'IMSI':
        Mobile.Send('00A4000002'+'6F07')
        Mobile.ReadBinary("00", "00", "09", value, "9000")
    elif EF == 'GID1':
        Mobile.Send('00A4000002'+'6F3E')
        Mobile.ReadBinary("00", "00", "03", value, "9000")
    elif EF == 'GID2':
        Mobile.Send('00A4000002'+'6F3F')
        Mobile.ReadBinary("00", "00", "03", value, "9000")
    elif EF == 'ACC':
        Mobile.Send('00A4000002'+'6F78')
        Mobile.ReadBinary("00", "00", "02", value, "9000")
    elif EF == 'ECC':
        Mobile.Send('00A4000002'+'6FB7')
        Mobile.ReadRecord("01", "04", "1D", value, "9000")
    elif EF == 'SPN':
        Mobile.Send('00A4000002'+'6FB7')
        Mobile.ReadBinary("00", "00", "11", value, "9000")
    else:
        print('EF not found')
    pass

############################################################################################################
def read_targeted_ef():
    data= "00043F002F0000083F007F105F3C4F2000083F007F105F3C4F210Fxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx046F40"
#    LogInfo('----- Read value of EF Targeted_EF -----')
#    Mobile.Send('00A4000002'+'3F00', expectedStatus='6XXX')
#    Mobile.Send('00A4000002'+'7FDE', expectedStatus='6XXX')
#    Mobile.Send('00A4000002'+'xxxx', expectedStatus='6XXX')
#    Mobile.GetResponse(Buffer.SW.Get(start=2,length=1),expectedData=None,expectedStatus='9000')
#    P3= Buffer.R.Get(start=14,length=1)
#    Mobile.VerifyPin('0131323334FFFFFFFF', expectedStatus='9000')
#    data=Mobile.ReadBinary("00", "00", P3, expectedData=None, expectedStatus='9000')    
    i=0
    j=2
    targeted_ef=[]
    
    while i<data.__len__():
        j=i+2
        len=data[i:j]
        AID_len=(2*int(len,16))
        i=i+2     
        if AID_len != 0:
            AID=data[i:(i+AID_len)]
            i=i+AID_len
        else:
            AID="None"
        j=i+2
        len=data[i:j]
        Path_len=(2*int(len,16))
        i=i+2
        if Path_len != 0:
            Path=data[i:(i+Path_len)]   
            i=i+Path_len
        else:
            Path="None"
        targeted_ef.append([AID,Path])
    return targeted_ef

ListOn()
def OpenSC():
    sd,ks=init_openSC()
    
    # open Secure Channel using the keyset ks
    sd.openDefaultSecureChannel(keyset = ks, secureMessaging=CLEAR_LEVEL) 
    
def init_openSC():
        # Create the SCP Keyset
    ks = SCPKeyset(default_keyset['Kvn'],default_keyset['Enc_Key'],default_keyset['Mac_Key'],default_keyset['Kek_Key'])

    ## Create a Security Domain Object using its AID
    return (IssuerSecurityDomain("A000000151000000", [ks]),ks)

def check_ef(ef_path,AID=None):
    hasil="false"
    LogInfo('----- Check if updated EF is EF from target -----')
    targeted_ef=read_targeted_ef()
    if AID!=None:
        for i in range (0,targeted_ef.__len__()):
            if AID == targeted_ef[i][0] :
                hasil="AID_true"
            if ef_path == targeted_ef[i][1] :
                hasil=hasil + "&" + "Path_true"
        if hasil == "AID_true&Path_true":
            hasil="true"
        else:
            hasil="false"
    else:
        for i in range (0,targeted_ef.__len__()):
            if ef_path == targeted_ef[i][1] :
                hasil="true"
            pass
    return (hasil)    
    pass

#def set_refresh_indicator(value):
#    ListOff()
#    PowerOn()
#    Mobile.Send('00A4000002'+'3F00')
#    Mobile.Send('00A4000002'+'7F8F')
#    Mobile.Send('00A4000002'+'6FF3')
#    Mobile.GetResponse(Buffer.SW.Get(start=2,length=1),expectedData=None,expectedStatus='9000')
#    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
#    Mobile.UpdateBinary("00", "00", "01", value, "9000")
#    PowerOff()
#    ListOn()
#    pass

#def set_poll_interval_counter(value):
##    ListOff()
#    PowerOn()
#    Mobile.Send('00A4000002'+'3F00')
#    Mobile.Send('00A4000002'+'7F8F')
#    Mobile.Send('00A4000002'+'6FF3')
#    Mobile.GetResponse(Buffer.SW.Get(start=2,length=1),expectedData=None,expectedStatus='9000')
#    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
#    Mobile.UpdateBinary("00", "01", "01", value, "9000")
#    PowerOff()
#
#    pass
    
#ushi
#====================================================================================================
#update and read all value 
def read_ef_refresh_flag(mode,IsRefresh):
    LogInfo("==============Read EF Refresh for Internal Flag==============")    
    ListOff()
    Mobile.Select("3F00")
    Mobile.Select("2F2F")
    
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:    
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    
    if IsRefresh=="True":
        Mobile.ReadBinary("00", "00", "01", expectedData='BB', expectedStatus=None)     
    elif IsRefresh=="False":
        Mobile.ReadBinary("00", "00", "01", expectedData='AA', expectedStatus=None) 
    
    ListOn()
    pass

def update_and_read_1EF(mode):
    LogInfo("==============Update and Read EF IMSI with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:    
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 04063F007F206F07 04063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 04063F007F206F07 04063F007F4F6F07', expectedStatus="9000")

    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F07")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:  
        Mobile.Select("7F20")
        Mobile.Select("6F07")  
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000") 
    
    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        
    input = "02"+lv('00'+ lv('A0000000871002FFFFFFFF89'))
    Mobile.UpdateBinary("00", "04", GetLength(input),input , "9000")
    Mobile.ReadBinary("00", "04", GetLength(input), expectedData=input, expectedStatus="9000") 
    
    ListOn() 
    pass

def update_and_read_1EF_small(mode):
    LogInfo("==============Update and Read with default value==============")    
#    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:    
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "01", "01", "01", "9000")
    Mobile.UpdateBinary("00", "00", "09", "BB 01 06 04 04 3F007F01", "9000")
    Mobile.ReadBinary("00", "00", "09", expectedData='BB 01 06 0404 3F007F01', expectedStatus="9000")
    
    Mobile.Select("3F00")
    Mobile.Select("7F01")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")    
    
    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "04", "03", "01 01 00", "9000")
    Mobile.ReadBinary("00", "04", "03", expectedData='01 01 00', expectedStatus="9000") 
    
    ListOn() 
    pass

def update_and_read_1EF_underADF(mode):
    LogInfo("==============Update and Read EF IMSI with default value==============")    
#    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:    
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "0B", "BB 01 08 0406 3F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "0B", expectedData='BB 01 08 0406 3F007F4F6F07', expectedStatus="9000")

    Mobile.Select("7F4F")
    Mobile.Select("6F07")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:    
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")     
    
    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "04", "0F", "01 0D 0C A0000000871002FFFFFFFF89", "9000")
    Mobile.ReadBinary("00", "04", "0F", expectedData='01 0D 0C A0000000871002FFFFFFFF89', expectedStatus="9000") 
    
    ListOn() 
    pass

def update_and_read_3EF(mode,isUpdate):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    if isUpdate=="True":
        Mobile.Select('3F00')
        Mobile.Select('2F2F')
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        input = "BB 06" +lv('04 06 3F007F206F07 00 06 3F007F206F05 06 06 3F007F206F31 04 06 3F007F4F6F07 00 06 3F007F4F6F05 06 06 3F007F4F6F31')
        Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")

        Mobile.Select("3F00")
        Mobile.Select("7FDE")
        Mobile.Select("17C3")
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        input = "06" +lv('000000 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89')
        Mobile.UpdateBinary("00", "04", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "04", GetLength(input), expectedData=input, expectedStatus="9000")
        
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000") 
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
    elif isUpdate=="Success":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000") 
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='33', expectedStatus="9000")
    elif isUpdate=="False":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000") 
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")     
    pass

def update_and_read_4EF(mode,isUpdate):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    if isUpdate=="Success":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("6F30")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F30")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "18", expectedData='3333333333333333 3333333333333333 3333333333333333', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='44', expectedStatus="9000")
    elif isUpdate=="True":
        Mobile.Select('3F00')
        Mobile.Select('2F2F')
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            
        input = "BB 08" +lv('04 06 3F007F206F07 00 06 3F007F206F05 06 06 3F007F206F30 01 06 3F007F206F31 04 06 3F007F4F6F07 00 06 3F007F4F6F05 06 06 3F007F4F6F30 01 06 3F007F4F6F31')
        Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    
        Mobile.Select("3F00")
        Mobile.Select("7FDE")
        Mobile.Select("17C3")
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        
        input = "08" +lv('00000000 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89')
        Mobile.UpdateBinary("00", "04", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "04", GetLength(input), expectedData=input, expectedStatus="9000")
        
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("6F30")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F30")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.UpdateBinary("00", "00", "18", "FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF", "9000")
            Mobile.ReadBinary("00", "00", "18", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
        
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")
    elif isUpdate=="False":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("6F30")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F30")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "18", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000") 
        
    elif isUpdate=="Middle":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("6F30")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F30")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "18", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000") 
    ListOn()
    pass

def update_and_read_5EF(mode,isUpdate):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    if isUpdate=="True":
        Mobile.Select('3F00')
        Mobile.Select('2F2F')
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        input = "BB 0A" +lv('04 06 3F007F206F07 00 06 3F007F206F05 06 06 3F007F206F78 01 06 3F007F206F31 02 06 3F007F206F38 04 06 3F007F4F6F07 00 06 3F007F4F6F05 06 06 3F007F4F6F78 01 06 3F007F4F6F31 02 06 3F007F4F6F38')
        Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")

        Mobile.Select("3F00")
        Mobile.Select("7FDE")
        Mobile.Select("17C3")
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        input = "0A" +lv('0000000000 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89')
        Mobile.UpdateBinary("00", "04", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "04", GetLength(input), expectedData=input, expectedStatus="9000")

        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F78")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F78")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "02", "FFFF", "9000")
        Mobile.ReadBinary("00", "00", "02", expectedData='FFFF', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")  
        #5th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F38")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F38")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "02", "FFFF", "9000")
        Mobile.ReadBinary("00", "00", "02", expectedData='FFFF', expectedStatus="9000")  
    
    elif isUpdate=="False":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F78")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F78")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='FFFF', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")  
        #5th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F38")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F38")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='FFFF', expectedStatus="9000")
       
    elif isUpdate=="Success":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F78")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F78")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='3333', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='44', expectedStatus="9000")  
        #5th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F38")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F38")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='5555', expectedStatus="9000")
    ListOn()    
    pass

def read_5EF_mode_0504(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
#    ListOff()
    #1st EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F05")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F05")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F31")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F31")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='44', expectedStatus="9000")   
    
pass


def update_and_read_9EF(mode,isUpdate):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    if isUpdate=="True":
        Mobile.Select('3F00')
        Mobile.Select('2F2F')
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        input = "BB 12" +lv('04 06 3F007F206F07 00 06 3F007F206F05 06 06 3F007F206F65 01 06 3F007F206F31 02 06 3F007F206F38 05 06 3F007F206F74 03 06 3F007F206F78 07 06 3F007F206FB7 08 06 3F007F206FAD 04 06 3F007F4F6F07 00 06 3F007F4F6F05 06 06 3F007F4F6F65 01 06 3F007F4F6F31 02 06 3F007F4F6F38 05 06 3F007F4F6F74 03 06 3F007F4F6F78 07 06 3F007F4F6FB7 08 06 3F007F4F6FAD')
        Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")

        Mobile.Select("3F00")
        Mobile.Select("7FDE")
        Mobile.Select("17C3")
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        input = "12" +berLv('000000000000000000 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89')
        Mobile.UpdateBinary("00", "04", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "04", GetLength(input), expectedData=input, expectedStatus="9000")
        
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F65")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F65")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "02", "FFFF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")  
        #5th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F38")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F38")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "02", "FFFF", "9000")
        Mobile.ReadBinary("00", "00", "02", expectedData='FFFF', expectedStatus="9000")
        #6th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F74")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F74")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.UpdateBinary("00", "00", "10", "FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF", "9000")
            Mobile.ReadBinary("00", "00", "10", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
        #7th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F78")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F78")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "02", "FFFF", "9000")
        Mobile.ReadBinary("00", "00", "02", expectedData='FFFF', expectedStatus="9000")
        #8th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6FB7")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6FB7")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.UpdateBinary("00", "00", "03", "FFFFFF", "9000")
            Mobile.ReadBinary("00", "00", "03", expectedData='FFFFFF', expectedStatus="9000")
        #9th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6FAD")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6FAD")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "03", "FFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "03", expectedData='FFFFFF', expectedStatus="9000")
        
    elif isUpdate=="Success":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F65")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F65")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='3333', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='44', expectedStatus="9000")  
        #5th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F38")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F38")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='5555', expectedStatus="9000")
        #6th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F74")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F74")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "10", expectedData='6666666666666666 6666666666666666', expectedStatus="9000")
        #7th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F78")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F78")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='7777', expectedStatus="9000")
        #8th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6FB7")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6FB7")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "03", expectedData='888888', expectedStatus="9000")
        #9th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6FAD")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6FAD")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "03", expectedData='999999', expectedStatus="9000")
    
    pass

def update_and_read_10EF(mode,isUpdate):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    if isUpdate=="True":
        Mobile.Select('3F00')
        Mobile.Select('2F2F')
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        input = "BB 14" +lv('04 06 3F007F206F07 00 06 3F007F206F05 06 06 3F007F206F65 01 06 3F007F206F31 02 06 3F007F206F38 05 06 3F007F206F74 03 06 3F007F206F78 07 06 3F007F206FB7 08 06 3F007F206FAD FF 06 3F007F206F30 04 06 3F007F4F6F07 00 06 3F007F4F6F05 06 06 3F007F4F6F65 01 06 3F007F4F6F31 02 06 3F007F4F6F38 05 06 3F007F4F6F74 03 06 3F007F4F6F78 07 06 3F007F4F6FB7 08 06 3F007F4F6FAD FF 06 3F007F4F6F30')
        Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")

        Mobile.Select("3F00")
        Mobile.Select("7FDE")
        Mobile.Select("17C3")
        if(mode=='3G'):
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        input = "14" +lv('00000000000000000000 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89')
        Mobile.UpdateBinary("00", "04", GetLength(input), input, "9000")
        Mobile.ReadBinary("00", "04", GetLength(input), expectedData=input, expectedStatus="9000")

        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F65")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F65")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "02", "FFFF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")  
        #5th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F38")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F38")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "02", "FFFF", "9000")
        Mobile.ReadBinary("00", "00", "02", expectedData='FFFF', expectedStatus="9000")
        #6th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F74")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F74")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.UpdateBinary("00", "00", "10", "FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF", "9000")
            Mobile.ReadBinary("00", "00", "10", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
        #7th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F78")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F78")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "02", "FFFF", "9000")
        Mobile.ReadBinary("00", "00", "02", expectedData='FFFF', expectedStatus="9000")
        #8th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6FB7")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6FB7")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.UpdateBinary("00", "00", "03", "FFFFFF", "9000")
            Mobile.ReadBinary("00", "00", "03", expectedData='FFFFFF', expectedStatus="9000")
        #9th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6FAD")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6FAD")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "03", "FFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "03", expectedData='FFFFFF', expectedStatus="9000")
        #10th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
#            Mobile.Select("7F4F")
            Mobile.Select("6F30")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F30")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.UpdateBinary("00", "00", "18", "FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF" , "9000")
            Mobile.ReadBinary("00", "00", "18", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
        
    elif isUpdate=="Success":
        #1st EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F07")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F07")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000")     
        #2nd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F05")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F05")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")   
        #3rd EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F65")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F65")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='3333', expectedStatus="9000")
        #4th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F31")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F31")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='44', expectedStatus="9000")  
        #5th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F38")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F38")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "02", expectedData='5555', expectedStatus="9000")
        #6th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F74")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F74")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "10", expectedData='6666666666666666 6666666666666666', expectedStatus="9000")
        #7th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6F78")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F78")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "02", expectedData='7777', expectedStatus="9000")
        #8th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6FB7")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6FB7")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "03", expectedData='888888', expectedStatus="9000")
        #9th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
            Mobile.Select("7F4F")
            Mobile.Select("6FAD")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6FAD")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "03", expectedData='999999', expectedStatus="9000")
        #10th EF
        Mobile.Select("3F00")
        if(mode=='3G'):
#            Mobile.Select("7F4F")
            Mobile.Select("6F30")
            Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
        else:
            Mobile.Select("7F20")
            Mobile.Select("6F30")
            Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
            Mobile.ReadBinary("00", "00", "18", expectedData='0000000000000000 0000000000000000 0000000000000000', expectedStatus="9000")
    ListOn()
    pass
    
def update_and_read_1EF_mode_00(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '00'==============")    
#    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 00063F007F206F07 00063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 00063F007F206F07 00063F007F4F6F07', expectedStatus="9000")  
    ListOn()   
    pass

def update_and_read_1EF_mode_01(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '01'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 01063F007F206F07 01063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 01063F007F206F07 01063F007F4F6F07', expectedStatus="9000")
    ListOn()   
    pass

def update_and_read_1EF_mode_02(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '02'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 02063F007F206F07 02063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 02063F007F206F07 02063F007F4F6F07', expectedStatus="9000")
    ListOn()   
    pass

def update_and_read_1EF_mode_03(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '03'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 03063F007F206F07 03063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 03063F007F206F07 03063F007F4F6F07', expectedStatus="9000") 
    ListOn()   
    pass

def update_and_read_1EF_mode_04(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '04'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 04063F007F206F07 04063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 04063F007F206F07 04063F007F4F6F07', expectedStatus="9000")
    ListOn()   
    pass

def update_and_read_1EF_mode_05(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '05'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 05063F007F206F07 05063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 05063F007F206F07 05063F007F4F6F07', expectedStatus="9000")
    ListOn()   
    pass

def update_and_read_1EF_mode_06(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '06'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 06063F007F206F07 06063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 06063F007F206F07 06063F007F4F6F07', expectedStatus="9000")    
    ListOn()   
    pass

def update_and_read_1EF_mode_07(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '07'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 07063F007F206F07 07063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 07063F007F206F07 07063F007F4F6F07', expectedStatus="9000")
    ListOn()   
    pass

def update_and_read_1EF_mode_08(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '08'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 08063F007F206F07 08063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 08063F007F206F07 08063F007F4F6F07', expectedStatus="9000")    
    ListOn()   
    pass

def update_and_read_1EF_mode_09(mode):
    LogInfo("==============Update and Read EF IMSI with mode = '09'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 09063F007F206F07 09063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 09063F007F206F07 09063F007F4F6F07', expectedStatus="9000")   
    ListOn()   
    pass

def update_and_read_1EF_mode_FF(mode):
    LogInfo("==============Update and Read EF IMSI with mode = 'FF'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "13", "BB 02 10 FF063F007F206F07 FF063F007F4F6F07", "9000")
    Mobile.ReadBinary("00", "00", "13", expectedData='BB 02 10 FF063F007F206F07 FF063F007F4F6F07', expectedStatus="9000")    
    ListOn()   
    pass

def update_and_read_3EF_lowHi_prio(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 06" +lv('08 06 3F007F206F07 08 06 3F007F206F05 04 06 3F007F206F31 08 06 3F007F4F6F07 08 06 3F007F4F6F05 04 06 3F007F4F6F31')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
  
    ListOn()   
    pass

def update_and_read_3EF_same_path(mode):   
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 06"+lv('00 06 3F007F206F07 08 06 3F007F206F05 04 06 3F007F206F07 00 06 3F007F4F6F07 08 06 3F007F4F6F05 04 06 3F007F4F6F07')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    
    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "06"+lv('000000 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89')
    Mobile.UpdateBinary("00", "04", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "04", GetLength(input), expectedData=input, expectedStatus="9000")
    ListOn()   
    pass

def read_3EF_same_path(mode):   
#    ListOff()
    #1st EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F07")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F07")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "09", expectedData='08 3333333333333333', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F05")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F05")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")
    ListOn()   
    pass

def update_and_read_4EF_same_mode(mode):
    LogInfo("==============Update and Read 4 EF with same mode = '00'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 08" +lv('00 06 3F007F206F07 00 06 3F007F206F05 00 06 3F007F206F30 00 06 3F007F206F31 00 06 3F007F4F6F07 00 06 3F007F4F6F05 00 06 3F007F4F6F30 00 06 3F007F4F6F31')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")   
    ListOn() 
    pass

def update_and_read_4EF_both_same_mode(mode):
    LogInfo("==============Update and Read 4 EF with 2 of them have a same mode = '04', and others = '06'==============")    
#    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    
    input = "BB 08"+lv('06 06 3F007F206F07 06 06 3F007F206F05 04 06 3F007F206F30 04 06 3F007F206F31 06 06 3F007F4F6F07 06 06 3F007F4F6F05 04 06 3F007F4F6F30 04 06 3F007F4F6F31')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")  

    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        
    input = "08"+lv('00000000 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89')
    Mobile.UpdateBinary("00", "04", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "04", GetLength(input), expectedData=input, expectedStatus="9000") 
    
    ListOn() 
    pass

def read_4EF_fail1(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
#    ListOff()
    #1st EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F07")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F07")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F05")   
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F05")  
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")   
    #3rd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
#        Mobile.Select("7F4F")
        Mobile.Select("6F30")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F30")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "18", expectedData='3333333333333333 3333333333333333 3333333333333333', expectedStatus="9000")
    #4th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F31")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F31")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")
    
def read_4EF_fail2(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    #1st EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F07")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F07")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F05")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F05")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")   
    #3rd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
#        Mobile.Select("7F4F")
        Mobile.Select("6F30")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F30")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "18", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
    #4th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F31")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F31")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")

def update_and_read_5EF_diff_mode00(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
#    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 0A" +lv('08 06 3F007F206F07 03 06 3F007F206F05 00 06 3F007F206F78 01 06 3F007F206F31 02 06 3F007F206F38 08 06 3F007F4F6F07 03 06 3F007F4F6F05 00 06 3F007F4F6F78 01 06 3F007F4F6F31 02 06 3F007F4F6F38')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    ListOn() 
    pass

def update_and_read_5EF_diff_mode06(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 0A" +lv('06 06 3F007F206F07 03 06 3F007F206F05 00 06 3F007F206F78 01 06 3F007F206F31 02 06 3F007F206F38 06 06 3F007F4F6F07 03 06 3F007F4F6F05 00 06 3F007F4F6F78 01 06 3F007F4F6F31 02 06 3F007F4F6F38')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    ListOn() 
    pass

def update_and_read_5EF_mode_0504(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 0A" +lv('05 06 3F007F206F07 04 06 3F007F206FAD 05 06 3F007F206F78 04 06 3F007F206FB7 05 06 3F007F206F38 05 06 3F007F4F6F07 04 06 3F007F4F6FAD 05 06 3F007F4F6F78 04 06 3F007F4F6FB7 05 06 3F007F4F6F38')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    
    #1st EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F05")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F05")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
    Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F31")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F31")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
    Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000") 
    
    ListOn() 
    pass

def update_5EF_diff_list(mode):   
#    ListOff()
    #1st EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F74")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F74")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "10", "FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "10", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FAE")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FAE")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
        Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")
    #3rd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FAD")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FAD")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "03", "FFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "03", expectedData='FFFFFF', expectedStatus="9000")
    #4th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FB7")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FB7")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.UpdateBinary("00", "00", "03", "FFFFFF", "9000")
        Mobile.ReadBinary("00", "00", "03", expectedData='FFFFFF', expectedStatus="9000")
    #5th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F7B")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F7B")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "0C", "FFFFFFFFFFFFFFFF FFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "0C", expectedData='FFFFFFFFFFFFFFFF FFFFFFFF', expectedStatus="9000")
    ListOn()   
    pass

def read_5EF_diff_list(mode):   
#    ListOff()
    #1st EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F74")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F74")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "10", expectedData='1111111111111111 1111111111111111', expectedStatus="9000")    
    #2nd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FAE")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FAE")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")
    #3rd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FAD")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FAD")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "03", expectedData='333333', expectedStatus="9000")
    #4th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FB7")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FB7")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        Mobile.ReadBinary("00", "00", "03", expectedData='444444', expectedStatus="9000")
    #5th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F7B")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F7B")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "0C", expectedData='5555555555555555 55555555', expectedStatus="9000")
    ListOn()   
    pass

def read_5EF_2success(mode):   
#    ListOff()
    #1st EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F07")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F07")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "09", expectedData='08 1111111111111111', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F05")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F05")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='22', expectedStatus="9000")  
    #3rd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FAD")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FAD")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "03", expectedData='333333', expectedStatus="9000")
    #4th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F74")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F74")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "10", expectedData='4444444444444444 4444444444444444', expectedStatus="9000")
    #5th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FAE")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FAE")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='55', expectedStatus="9000")
    ListOn()   
    pass

def update_5EF_2success(mode):   
#    ListOff()
    #3rd EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FAD")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FAD")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "03", "FFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "03", expectedData='FFFFFF', expectedStatus="9000")
    #4th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F74")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6F74")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "10", "FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "10", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
    #5th EF
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6FAE")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.Select("7F20")
        Mobile.Select("6FAE")
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
    Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")
    ListOn()   
    pass

def update_and_read_9EF_mode_03(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 12" +lv('08 06 3F007F206F07 07 06 3F007F206F05 03 06 3F007F206F65 08 06 3F007F206F31 07 06 3F007F206F38 03 06 3F007F206F74 08 06 3F007F206F78 07 06 3F007F206FB7 03 06 3F007F206FAD 08 06 3F007F4F6F07 07 06 3F007F4F6F05 03 06 3F007F4F6F65 08 06 3F007F4F6F31 07 06 3F007F4F6F38 03 06 3F007F4F6F74 08 06 3F007F4F6F78 07 06 3F007F4F6FB7 03 06 3F007F4F6FAD')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    ListOn()   
    pass

def update_9EF_diff_list(mode):   
    ListOff()
    #1st EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6FAE")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
    Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F7B")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "0C", "FFFFFFFFFFFFFFFF FFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "0C", expectedData='FFFFFFFFFFFFFFFF FFFFFFFF', expectedStatus="9000")
    #3rd EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F20")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "09", "FFFFFFFFFFFFFFFF FF", "9000")
    Mobile.ReadBinary("00", "00", "09", expectedData='FFFFFFFFFFFFFFFF FF', expectedStatus="9000")
    #4th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6FAE")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "01", "FF", "9000")
    Mobile.ReadBinary("00", "00", "01", expectedData='FF', expectedStatus="9000")
    #5th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F54")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "20", "FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "20", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
    #6th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F52")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "09", "FFFFFFFFFFFFFFFF FF", "9000")
    Mobile.ReadBinary("00", "00", "09", expectedData='FFFFFFFFFFFFFFFF FF', expectedStatus="9000")
    #7th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F53")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "0E", "FFFFFFFFFFFFFFFF FFFFFFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "0E", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFF', expectedStatus="9000")
    #8th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F50")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "0C", "FFFFFFFFFFFFFFFF FFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "0C", expectedData='FFFFFFFFFFFFFFFF FFFFFFFF', expectedStatus="9000")
    #9th EF
    Mobile.Select("3F00")
    Mobile.Select("7F10")
    Mobile.Select("6F54")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "20", "FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "20", expectedData='FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF', expectedStatus="9000")
    ListOn()   
    pass

def read_9EF_diff_list(mode):   
    ListOff()
    #1st EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6FAE")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='44', expectedStatus="9000")     
    #2nd EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F7B")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "0C", expectedData='2222222222222222 22222222', expectedStatus="9000")
    #3rd EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F20")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "09", expectedData='3333333333333333 33', expectedStatus="9000")
    #4th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6FAE")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "01", expectedData='44', expectedStatus="9000")
    #5th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F54")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "20", expectedData='9999999999999999 9999999999999999 9999999999999999 9999999999999999', expectedStatus="9000")
    #6th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F52")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "09", expectedData='6666666666666666 66', expectedStatus="9000")
    #7th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F53")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "0E", expectedData='7777777777777777 777777777777', expectedStatus="9000")
    #8th EF
    Mobile.Select("3F00")
    Mobile.Select("7F20")
    Mobile.Select("6F50")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "0C", expectedData='8888888888888888 88888888', expectedStatus="9000")
    #9th EF
    Mobile.Select("3F00")
    Mobile.Select("7F10")
    Mobile.Select("6F54")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.ReadBinary("00", "00", "20", expectedData='9999999999999999 9999999999999999 9999999999999999 9999999999999999', expectedStatus="9000")
    ListOn()   
    pass

def update_and_read_10EF_diff_5_mode(mode):
    LogInfo("==============Update and Read 4 EF with same mode = '00'==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "01", "01", "0A", "9000")
    Mobile.UpdateBinary("00", "02", "50", "04 06 3F007F206F07 00 06 3F007F206F05 06 06 3F007F206F30 01 06 3F007F206F31 02 06 3F007F206F38 04 06 3F007F206F74 00 06 3F007F206F78 06 06 3F007F206FB7 01 06 3F007F206FAD 02 06 3F007F206FAE", "9000")     
    
    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "04", "83", "0A 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89", "9000")
    Mobile.ReadBinary("00", "04", "83", expectedData='0A 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89 0C A0000000871002FFFFFFFF89', expectedStatus="9000")
    ListOn() 
    pass

def update_and_read_10EF_diff_mode00(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
        
    input = "BB 14" +lv('08 06 3F007F206F07 00 06 3F007F206F05 08 06 3F007F206F65 01 06 3F007F206F31 02 06 3F007F206F38 05 06 3F007F206F74 03 06 3F007F206F78 07 06 3F007F206FB7 08 06 3F007F206FAD 08 06 3F007F206F30 08 06 3F007F4F6F07 00 06 3F007F4F6F05 08 06 3F007F4F6F65 01 06 3F007F4F6F31 02 06 3F007F4F6F38 05 06 3F007F4F6F74 03 06 3F007F4F6F78 07 06 3F007F4F6FB7 08 06 3F007F4F6FAD 08 06 3F007F206F30')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    ListOn() 
    pass

def update_and_read_10EF_diff_mode06(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 14" +lv('08 06 3F007F206F07 00 06 3F007F206F05 06 06 3F007F206F65 01 06 3F007F206F31 02 06 3F007F206F38 05 06 3F007F206F74 03 06 3F007F206F78 07 06 3F007F206FB7 08 06 3F007F206FAD 08 06 3F007F206F30 08 06 3F007F4F6F07 00 06 3F007F4F6F05 06 06 3F007F4F6F65 01 06 3F007F4F6F31 02 06 3F007F4F6F38 05 06 3F007F4F6F74 03 06 3F007F4F6F78 07 06 3F007F4F6FB7 08 06 3F007F4F6FAD 08 06 3F007F206F30')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    ListOn() 
    pass

def update_and_read_10EF_diff_mode01(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 14" +lv('08 06 3F007F206F07 08 06 3F007F206F05 08 06 3F007F206F65 01 06 3F007F206F31 08 06 3F007F206F38 05 06 3F007F206F74 03 06 3F007F206F78 07 06 3F007F206FB7 08 06 3F007F206FAD 08 06 3F007F206F30 08 06 3F007F4F6F07 08 06 3F007F4F6F05 08 06 3F007F4F6F65 01 06 3F007F4F6F31 08 06 3F007F4F6F38 05 06 3F007F4F6F74 03 06 3F007F4F6F78 07 06 3F007F4F6FB7 08 06 3F007F4F6FAD 08 06 3F007F206F30')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    ListOn() 
    pass

def update_and_read_10EF_diff_mode02(mode):
    LogInfo("==============Update and Read EF's with default value==============")    
    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    input = "BB 14" +lv('08 06 3F007F206F07 08 06 3F007F206F05 08 06 3F007F206F65 01 06 3F007F206F31 02 06 3F007F206F38 05 06 3F007F206F74 03 06 3F007F206F78 07 06 3F007F206FB7 08 06 3F007F206FAD 08 06 3F007F206F30 08 06 3F007F4F6F07 08 06 3F007F4F6F05 08 06 3F007F4F6F65 01 06 3F007F4F6F31 02 06 3F007F4F6F38 05 06 3F007F4F6F74 03 06 3F007F4F6F78 07 06 3F007F4F6FB7 08 06 3F007F4F6FAD 08 06 3F007F206F30')
    Mobile.UpdateBinary("00", "00", GetLength(input), input, "9000")
    Mobile.ReadBinary("00", "00", GetLength(input), expectedData=input, expectedStatus="9000")
    ListOn() 
    pass
   
def update_and_read_wrong_path(mode):    
#    ListOff()
    Mobile.Select('3F00')
    Mobile.Select('2F2F')
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
#    Mobile.UpdateBinary("00", "00", "11", "BB 02 0E 04 06 3F006F07 00 06 3F007F4F6F07", "9000")
#    Mobile.ReadBinary("00", "00", "11", expectedData='BB 02 0E 04 06 3F006F07 00 06 3F007F4F6F07', expectedStatus="9000")
    Mobile.UpdateBinary("00", "00", "09", "BB 01 06 04 04 3F006F07", "9000")
    Mobile.ReadBinary("00", "00", "09", expectedData='BB 01 06 04 04 3F006F07', expectedStatus="9000")
    
    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "04", "03", "01 01 00", "9000")
    Mobile.ReadBinary("00", "04", "03", expectedData='01 01 00', expectedStatus="9000")
    ListOn() 
    pass 

#====================================================================================================
#Set the default value for update EF
def set_no_EF_Refresh_list(mode):
    ListOff()
    Mobile.Select("3F00")
    Mobile.Select("2F2F")   
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "02", "BB 00", "9000")
    Mobile.ReadBinary("00", "00", "02", expectedData='BB 00', expectedStatus="9000")
    
    Mobile.Select("3F00")
    Mobile.Select("7FDE")
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "04", "01", "00", "9000")
    Mobile.ReadBinary("00", "04", "01", expectedData='00', expectedStatus="9000")
    
    Mobile.Select("3F00")
    if(mode=='3G'):
        Mobile.Select("7F4F")
        Mobile.Select("6F07")
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:  
        Mobile.Select("7F20")
        Mobile.Select("6F07")  
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "09", "08 FFFFFFFFFFFFFFFF", "9000")
    Mobile.ReadBinary("00", "00", "09", expectedData='08 FFFFFFFFFFFFFFFF', expectedStatus="9000")
    
    ListOn()
    pass

#OTA RFM
set_EFRefresh_list1 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 8888888888888888"
set_EFRefresh_list1_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 8888888888888888"

set_EFRefresh_list3 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F31 A0D6000001 33"
set_EFRefresh_list3_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 33"                    

set_EFRefresh_list4 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F30 A0D6000018 3333333333333333 3333333333333333 3333333333333333"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F31 A0D6000001 44"
set_EFRefresh_list4_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 44"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F30 00D6000018 3333333333333333 3333333333333333 3333333333333333"
                    
                    
set_EFRefresh_list5 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F78 A0D6000002 3333"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F31 A0D6000001 44"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F38 A0D6000002 5555"
set_EFRefresh_list5_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F78 00D6000002 3333"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 44"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F38 00D6000002 5555"

set_EFRefresh_list9 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F65 A0D6000002 3333"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F31 A0D6000001 44"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F38 A0D6000002 5555"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F74 A0D6000010 6666666666666666 6666666666666666"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F78 A0D6000002 7777"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026FB7 A0D6000003 888888"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026FAD A0D6000003 999999"
set_EFRefresh_list9_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F65 00D6000002 3333"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 44"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F38 00D6000002 5555"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F78 00D6000002 7777"\
                    + "00A40000023F00 00A40000027F4F 00A40000026FAD 00D6000003 999999"\
                    + "00A40000023F00 00A40000027F4F 00A40000026FB7 00D6000003 888888"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F74 00D6000010 6666666666666666 6666666666666666"                    
                    
set_EFRefresh_list10 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F65 A0D6000002 3333"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F31 A0D6000001 44"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F38 A0D6000002 5555"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F74 A0D6000010 6666666666666666 6666666666666666"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F78 A0D6000002 7777"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026FB7 A0D6000003 888888"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026FAD A0D6000003 999999"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F30 A0D6000018 0000000000000000 0000000000000000 0000000000000000"
set_EFRefresh_list10_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F65 00D6000002 3333"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 44"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F38 00D6000002 5555"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F74 00D6000010 6666666666666666 6666666666666666"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F78 00D6000002 7777"\
                    + "00A40000023F00 00A40000027F4F 00A40000026FB7 00D6000003 888888"\
                    + "00A40000023F00 00A40000027F4F 00A40000026FAD 00D6000003 999999"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F30 00D6000018 0000000000000000 0000000000000000 0000000000000000"

#others
set_EFRefresh_diff_list1 = "A0A4000C023F00 A0A4000C027F01 A0D6000009 08 8888888888888888"
set_EFRefresh_diff_list1_00 = "00A4000C023F00 00A4000C027F01 00D6000009 08 8888888888888888"

set_EFRefresh_list1_diff = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"
set_EFRefresh_list1_diff_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"

set_EFRefresh_list1_underADF = "A0A40000027F4F A0A40000026F07 A0D6000009 08 8888888888888888"
set_EFRefresh_list1_underADF_00 = "00A40000027F4F 00A40000026F07 00D6000009 08 8888888888888888"

set_EFRefresh_list1_small = "A0A40000023F00 A0A40000027F01 A0D6000004 08 888888"
set_EFRefresh_list1_small_00 = "00A40000023F00 00A40000027F01 00D6000004 08 888888"

set_EFRefresh_list1_big = "A0A40000023F00 A0A40000027F01 A0D600000A 08 8888888888888888 88"
set_EFRefresh_list1_big_00 = "00A40000023F00 00A40000027F01 00D600000A 08 8888888888888888 88"

set_EFRefresh_list3_diff = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 3333333333333333"
set_EFRefresh_list3_diff_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 3333333333333333" 

set_EFRefresh_list4_beg = "00A40000023F00 00A40000027F4F 00A40000026F30 00D6000018 3333333333333333 3333333333333333 3333333333333333"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 44"\
                    
set_EFRefresh_list4_mid = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                    +"00A40000023F00 00A40000027F4F 00A40000026F30 00D6000018 3333333333333333 3333333333333333 3333333333333333"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 44"

set_EFRefresh_diff_list5 = "A0A40000023F00 A0A40000027F20 A0A40000026F74 A0D6000010 1111111111111111 1111111111111111" \
                       + "A0A40000023F00 A0A40000027F20 A0A40000026FAE A0D6000001 22"\
                       + "A0A40000023F00 A0A40000027F20 A0A40000026FAD A0D6000003 333333"\
                       + "A0A40000023F00 A0A40000027F20 A0A40000026FB7 A0D6000003 444444"\
                       + "A0A40000023F00 A0A40000027F20 A0A40000026F7B A0D600000C 5555555555555555 55555555"
set_EFRefresh_diff_list5_00 = "00A40000023F00 00A40000027F4F 00A40000026FAD 00D6000003 333333"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F7B 00D600000C 5555555555555555 55555555"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F74 00D6000010 1111111111111111 1111111111111111" \
                        + "00A40000023F00 00A40000027F4F 00A40000026FAE 00D6000001 22"\
                        + "00A40000023F00 00A40000027F4F 00A40000026FB7 00D6000003 444444"
                       
set_EFRefresh_diff_list5_03 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026FAD A0D6000003 333333"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026F74 A0D6000010 4444444444444444 4444444444444444"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026FAE A0D6000001 55"
set_EFRefresh_diff_list5_03_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                        + "00A40000023F00 00A40000027F4F 00A40000026FAD 00D6000003 333333"\
                        + "00A40000023F00 00A40000027F4F 00A40000026FAE 00D6000001 55"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F74 00D6000010 4444444444444444 4444444444444444"

set_EFRefresh_diff_list9 = "A0A40000023F00 A0A40000027F20 A0A40000026FAE A0D6000001 11"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F7B A0D600000C 2222222222222222 22222222"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F20 A0D6000009 3333333333333333 33"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026FAE A0D6000001 44"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F54 A0D6000020 5555555555555555 5555555555555555 5555555555555555 5555555555555555"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F52 A0D6000009 6666666666666666 66"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F53 A0D600000E 7777777777777777 777777777777"\
                    + "A0A40000023F00 A0A40000027F20 A0A40000026F50 A0D600000C 8888888888888888 88888888"\
                    + "A0A40000023F00 A0A40000027F10 A0A40000026F54 A0D6000020 9999999999999999 9999999999999999 9999999999999999 9999999999999999"
                    
set_EFRefresh_list4_fail1 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026F30 A0D6000018 3333333333333333 3333333333333333 3333333333333333"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026F38 A0D6000002 5555"
set_EFRefresh_list4_fail1_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F30 00D6000018 3333333333333333 3333333333333333 3333333333333333"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F38 00D6000002 5555"
                        
set_EFRefresh_list4_fail2 = "A0A40000023F00 A0A40000027F20 A0A40000026F07 A0D6000009 08 1111111111111111"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026F05 A0D6000001 22"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026FAD A0D6000003 999999"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026F38 A0D6000002 5555"
set_EFRefresh_list4_fail2_00 = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
                        + "00A40000023F00 00A40000027F4F 00A40000026FAD 00D6000003 999999"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F38 00D6000002 5555"
                        
set_EFRefresh_list4_all_fail = "A0A40000023F00 A0A40000027F20 A0A40000026F78 A0D6000002 7777"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026FB7 A0D6000003 888888"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026FAD A0D6000003 999999"\
                        + "A0A40000023F00 A0A40000027F20 A0A40000026F38 A0D6000002 5555"
set_EFRefresh_list4_all_fail_00 = "00A40000023F00 00A40000027F4F 00A40000026F78 00D6000002 7777"\
                        + "00A40000023F00 00A40000027F4F 00A40000026FB7 00D6000003 888888"\
                        + "00A40000023F00 00A40000027F4F 00A40000026FAD 00D6000003 999999"\
                        + "00A40000023F00 00A40000027F4F 00A40000026F38 00D6000002 5555"

#HTTP
set_EFRefresh_list1_http = "A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 8888888888888888"
set_EFRefresh_list1_00_http = "00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 8888888888888888"

set_EFRefresh_list1_interup_http = "A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 0000000000000000"
set_EFRefresh_list1_interup_00_http = "00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 0000000000000000"

set_EFRefresh_list3_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F31", "A0D6000001 33"]
set_EFRefresh_list3_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F31", "00D6000001 33"]                    

set_EFRefresh_list4_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F30", "A0D6000018 3333333333333333 3333333333333333 3333333333333333",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F31", "A0D6000001 44"]
set_EFRefresh_list4_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F31", "00D6000001 44",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F30", "00D6000018 3333333333333333 3333333333333333 3333333333333333"]
                    
set_EFRefresh_list5_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F78", "A0D6000002 3333",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F31", "A0D6000001 44",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F38", "A0D6000002 5555"]
set_EFRefresh_list5_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F78", "00D6000002 3333",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F31", "00D6000001 44",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F38", "00D6000002 5555"]

set_EFRefresh_list9_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F65", "A0D6000002 3333",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F31", "A0D6000001 44",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F38", "A0D6000002 5555",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F74", "A0D6000010 6666666666666666 6666666666666666",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F78", "A0D6000002 7777",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026FB7", "A0D6000003 888888",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026FAD", "A0D6000003 999999"]
set_EFRefresh_list9_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F65", "00D6000002 3333",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F31", "00D6000001 44",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F38", "00D6000002 5555",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F78", "00D6000002 7777",
                     "00A40000023F00", "00A40000027F4F", "00A40000026FAD", "00D6000003 999999",
                     "00A40000023F00", "00A40000027F4F", "00A40000026FB7", "00D6000003 888888",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F74", "00D6000010 6666666666666666 6666666666666666"]                    
                    
set_EFRefresh_list10_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F65", "A0D6000002 3333",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F31", "A0D6000001 44",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F38", "A0D6000002 5555",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F74", "A0D6000010 6666666666666666 6666666666666666",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F78", "A0D6000002 7777",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026FB7", "A0D6000003 888888",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026FAD", "A0D6000003 999999",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F30", "A0D6000018 0000000000000000 0000000000000000 0000000000000000"]
set_EFRefresh_list10_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F65", "00D6000002 3333",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F31", "00D6000001 44",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F38", "00D6000002 5555",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F78", "00D6000002 7777",
                     "00A40000023F00", "00A40000027F4F", "00A40000026FAD", "00D6000003 999999",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F74", "00D6000010 6666666666666666 6666666666666666",
                     "00A40000023F00", "00A40000027F4F", "00A40000026FB7", "00D6000003 888888", 
                     "00A40000023F00", "00A40000027F4F", "00A40000026F30", "00D6000018 0000000000000000 0000000000000000 0000000000000000"]
                    
#others
set_EFRefresh_diff_list1_http = "A0A4000C023F00", "A0A4000C027F01", "A0D6000009 08 8888888888888888"
set_EFRefresh_diff_list1_00_http = "00A4000C023F00", "00A4000C027F01", "00D6000009 08 8888888888888888"

set_EFRefresh_list1_diff_http = "A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111"
set_EFRefresh_list1_diff_00_http = "00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111"

set_EFRefresh_list1_underADF_http = "A0A40000027F4F", "A0A40000026F07", "A0D6000009 08 8888888888888888"
set_EFRefresh_list1_underADF_00_http = "00A40000027F4F", "00A40000026F07", "00D6000009 08 8888888888888888"

set_EFRefresh_list1_small_http = "A0A40000023F00", "A0A40000027F01", "A0D6000004 08 888888"
set_EFRefresh_list1_small_00_http = "00A40000023F00", "00A40000027F01", "00D6000004 08 888888"

set_EFRefresh_list1_big_http = "A0A40000023F00", "A0A40000027F01", "A0D600000A 08 8888888888888888 88"
set_EFRefresh_list1_big_00_http = "00A40000023F00", "00A40000027F01", "00D600000A 08 8888888888888888 88"

set_EFRefresh_list3_diff_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 3333333333333333"]
set_EFRefresh_list3_diff_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                     "00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 3333333333333333"]

#set_EFRefresh_list4_beg_http = "00A40000023F00 00A40000027F4F 00A40000026F30 00D6000018 3333333333333333 3333333333333333 3333333333333333"\
#                    + "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
#                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
#                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 44"\
                    
#set_EFRefresh_list4_mid_http = "00A40000023F00 00A40000027F4F 00A40000026F07 00D6000009 08 1111111111111111"\
#                    +"00A40000023F00 00A40000027F4F 00A40000026F30 00D6000018 3333333333333333 3333333333333333 3333333333333333"\
#                    + "00A40000023F00 00A40000027F4F 00A40000026F05 00D6000001 22"\
#                    + "00A40000023F00 00A40000027F4F 00A40000026F31 00D6000001 44"

set_EFRefresh_diff_list5_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F74", "A0D6000010 1111111111111111 1111111111111111", 
                        "A0A40000023F00", "A0A40000027F20", "A0A40000026FAE", "A0D6000001 22",
                        "A0A40000023F00", "A0A40000027F20", "A0A40000026FAD", "A0D6000003 333333",
                        "A0A40000023F00", "A0A40000027F20", "A0A40000026FB7", "A0D6000003 444444",
                        "A0A40000023F00", "A0A40000027F20", "A0A40000026F7B", "A0D600000C 5555555555555555 55555555"]
set_EFRefresh_diff_list5_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026FAD", "00D6000003 333333",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F7B", "00D600000C 5555555555555555 55555555",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F74", "00D6000010 1111111111111111 1111111111111111" ,
                         "00A40000023F00", "00A40000027F4F", "00A40000026FAE", "00D6000001 22",
                         "00A40000023F00", "00A40000027F4F", "00A40000026FB7", "00D6000003 444444"]
                       
set_EFRefresh_diff_list5_03_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026FAD", "A0D6000003 333333",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026F74", "A0D6000010 4444444444444444 4444444444444444",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026FAE", "A0D6000001 55"]
set_EFRefresh_diff_list5_03_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                         "00A40000023F00", "00A40000027F4F", "00A40000026FAD", "00D6000003 333333",
                         "00A40000023F00", "00A40000027F4F", "00A40000026FAE", "00D6000001 55",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F74", "00D6000010 4444444444444444 4444444444444444"]

set_EFRefresh_diff_list9_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026FAE", "A0D6000001 11",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F7B", "A0D600000C 2222222222222222 22222222",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F20", "A0D6000009 3333333333333333 33",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026FAE", "A0D6000001 44",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F54", "A0D6000020 5555555555555555 5555555555555555 5555555555555555 5555555555555555",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F52", "A0D6000009 6666666666666666 66",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F53", "A0D600000E 7777777777777777 777777777777",
                     "A0A40000023F00", "A0A40000027F20", "A0A40000026F50", "A0D600000C 8888888888888888 88888888",
                     "A0A40000023F00", "A0A40000027F10", "A0A40000026F54", "A0D6000020 9999999999999999 9999999999999999 9999999999999999 9999999999999999"]
                    
set_EFRefresh_list4_fail1_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026F30", "A0D6000018 3333333333333333 3333333333333333 3333333333333333",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026F38", "A0D6000002 5555"]
set_EFRefresh_list4_fail1_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F30", "00D6000018 3333333333333333 3333333333333333 3333333333333333",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F38", "00D6000002 5555"]
                        
set_EFRefresh_list4_fail2_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F07", "A0D6000009 08 1111111111111111", 
                                  "A0A40000023F00", "A0A40000027F20", "A0A40000026F05", "A0D6000001 22", 
                                  "A0A40000023F00", "A0A40000027F20", "A0A40000026FAD", "A0D6000003 999999", 
                                  "A0A40000023F00", "A0A40000027F20", "A0A40000026F38", "A0D6000002 5555"]
set_EFRefresh_list4_fail2_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F07", "00D6000009 08 1111111111111111",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F05", "00D6000001 22",
                         "00A40000023F00", "00A40000027F4F", "00A40000026FAD", "00D6000003 999999",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F38", "00D6000002 5555"]
                        
set_EFRefresh_list4_all_fail_http = ["A0A40000023F00", "A0A40000027F20", "A0A40000026F78", "A0D6000002 7777",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026FB7", "A0D6000003 888888",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026FAD", "A0D6000003 999999",
                         "A0A40000023F00", "A0A40000027F20", "A0A40000026F38", "A0D6000002 5555"]
set_EFRefresh_list4_all_fail_00_http = ["00A40000023F00", "00A40000027F4F", "00A40000026F78", "00D6000002 7777",
                         "00A40000023F00", "00A40000027F4F", "00A40000026FB7", "00D6000003 888888",
                         "00A40000023F00", "00A40000027F4F", "00A40000026FAD", "00D6000003 999999",
                         "00A40000023F00", "00A40000027F4F", "00A40000026F38", "00D6000002 5555"]

#====================================================================================================
#Set the value for EF Config in SKT project
def set_retry_counter(mode,value):
    ListOff()
    Mobile.Select("3F00")
    Mobile.Select("7FDE")  
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "00", "01", value, "9000")
    Mobile.ReadBinary("00", "00", "01", expectedData=value, expectedStatus="9000")
    ListOn()
    pass

def set_poll_int_counter(mode,value):
    ListOff()
    Mobile.Select("3F00")
    Mobile.Select("7FDE")  
    Mobile.Select("17C3")    
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "01", "01", value, "9000")
    ListOn()
    pass

def set_poll_int_timer(mode,value):
    ListOff()
    PowerOn()
    Mobile.Select("3F00")
    Mobile.Select("7FDE")  
    Mobile.Select("17C3")
    if(mode=='3G'):
        Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
    else:
        Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
    Mobile.UpdateBinary("00", "02", "02", value, "9000")
    Mobile.ReadBinary("00", "02", "02", expectedData=value, expectedStatus="9000")
    PowerOff()
    ListOn()
    pass



def set_internal_flag(value):
    LogInfo("Update Internal Flag Refresh")
    Mobile.VerifyPin("0A"+lv(adm1), expectedStatus='9000')
    Mobile.Select("3F00")
    Mobile.Select("2F2F")
    Mobile.UpdateBinary("00", "00", "01", value, "9000")
    Mobile.ReadBinary("00", "00", "01", expectedData=value, expectedStatus="9000")

# def set_internal_flag(mode,value):
#     ListOff()
#     Mobile.Select("3F00")
#     Mobile.Select("2F2F")   
#     if(mode=='3G'):
#         Mobile.VerifyPin('0A 08 933F57845F706921', expectedStatus='9000')
#     else:
#         Mobile.VerifyCode("00 08 933F57845F706921", expectedStatus='9000')
#     Mobile.UpdateBinary("00", "00", "01", value, "9000")
#     Mobile.ReadBinary("00", "00", "01", expectedData=value, expectedStatus="9000")
#     ListOn()
#     pass


#def set_status_command(value):
#    ListOff()
#    PowerOn()
#    Mobile.Select("3F00")
#    Mobile.Select("2F2F")
#    Mobile.Select("6FF3")    
#    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
#    Mobile.UpdateBinary("00", "01", "01", value, "9000")
#    PowerOff()
#    ListOn()
#    pass
#=====================================================================================================


def set_StatusCmd (jml):
    for i in jml:
        cla_00.get_status("01")   
        outstat = Buffer.SW.Get(start=1,length=2)
        IsStatusOk("91XX",outstat)
    pass

'''**************************************FOR USAT OTA POLLER APPLET********************************************************'''
def getDataBin(P1,P2,P3):
    data=str(Mobile.ReadBinary(P1, P2, P3, expectedData=None, expectedStatus='9000'))
    data=data.split("'")
    data=data.__getitem__(1)
    return (data)
    pass

def OTA_init():
    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    tar="B00001"
    L = "00000000"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "00"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "00"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000000"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    
 #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"

    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("00")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000001") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 
    Dll.OTA2.end_message(" I K "  )
    Dll.OTA2.display_message(" K ")

def Update_EF_Config(P1,P2,P3,value_EF):
    ListOff()
    PowerOn()    
    LogInfo('----- Update value of EF_Config -----')
    Mobile.Send('00A4000C02'+'3F00')
    Mobile.Send('00A4000C02'+'7FDE')
    Mobile.Send('00A4000C02'+'1538')
#    Mobile.GetResponse(Buffer.SW.Get(start=2,length=1),expectedData=None,expectedStatus='9000')
    Mobile.Send('0020000A08933F57845F706921', expectedStatus='9000')
    Mobile.UpdateBinary(P1, P2, P3, value_EF, expectedStatus='9000')        
    Mobile.ReadBinary(P1, P2, P3,expectedData=value_EF,expectedStatus='9000')     
    PowerOff()
    ListOn()

def Update_EF_String(P1,P2,P3,Value_EF):
    PowerOn()
    Mobile.SetMode("2G")    
    LogInfo('----- Update value of EF_String -----')
    Mobile.Send('A0A4000002'+'3F00', expectedStatus='6XXX')
    Mobile.Send('A0A4000002'+'7FDE', expectedStatus='6XXX')
    Mobile.Send('A0AA4000002'+'1539', expectedStatus='6XXX')
    Mobile.GetResponse(Buffer.SW.Get(start=2,length=1),expectedData=None,expectedStatus='9000')
    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')
    Mobile.UpdateBinary(P1, P2, P3, value_EF, expectedStatus='9000')
        
    PowerOff()         

def HTTPAdminSessionProcessRFM_FailedResponse (dataRFM,output):

    OTA_init()
    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  

       
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000087/1002FF81FF208903060000"),
            ]

    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(Buffer.J.Get())

    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])

    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])

    LogInfo("##Send HTTP response to the card")
#    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")       
    Mobile.Send("80C2000010 D60E19010982022181380281003701FF ") 
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")
    Mobile.TerminalResponse("810301420082022181030130","91xx")
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")
    Mobile.TerminalResponse("810301420082022181030130","91xx")   

    LogInfo(''' **Close Channel** ''')
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")
    Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + "030100" + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)

def Failed_2():
    LogInfo('#---------------------------------------')
    LogInfo('# PROCESS HANDSHAKE With Bip Channel    ')
    LogInfo('#---------------------------------------')

    LogInfo ("## Init TLS session")
    tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_CIPHER_NULL"],"4F544132","F0C0FAAC 0EF1364A 3E5EB422 9CF797A3 752CD0C8 27784457 6B3E05D5 05A03F21 E0ED11BF EBE40B85 E2EC25AE 06B6FF60 D3D47FA1 26403743 C0AE5EAC F0AE71C4")
#    tlsServer.InitTlsSession( "TLS_SERVER_1_0",["TLS_PSK","TLS_CIPHER_NULL"],"4F544132","F0C0FAAC 0EF1364A 3E5EB422 9CF797A3 752CD0C8 27784457 6B3E05D5 05A03F21 E0ED11BF EBE40B85 E2EC25AE 06B6FF60 D3D47FA1 26403743 C0AE5EAC F0AE71C4")

    LogInfo(''' Open Channel Process ''')
#    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=None, expectedStatus='9000')
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=Open_Channel, expectedStatus='9000')
    output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_COMMAND_PERFORMED1 + CHANNEL_STATUS + CHANNEL_ID1 + "35 07 02 00 00 03 00 00 02" +"39 02 3A 98",expectedStatus = OK_FETCH)
    LogInfo("## TLS PSK PROCESS HANDSHAKE")
    LogInfo("#card should generate ClientHello")
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=None, expectedStatus='9000')
    output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + "030130" + CHANNEL_STATUS + CHANNEL_ID1 + "35 07 02 00 00 03 00 00 02" +"39 02 3A 98",expectedStatus = OK_FETCH)
    
#    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")
    ret=tlsServer.ProcessHandshake(output[0])
    LogInfo(''' Close Channel ''')
    tlsServer.CloseTlsConnection()
    tlsServer.CloseTlsSession() 
    Mobile.Fetch(output[1][2:], None , expectedStatus =OK)
    Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + "030100" + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)


def Retry_Session_2(isPollOff=None):
    LogInfo('#---------------------------------------')
    LogInfo('# OS RETRY MECHANISM                    ')
    LogInfo('#---------------------------------------')  
    a=1  
    for i in range (2):
        LogInfo('''**Timer Management**''')
        Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")  
        ##########################################
        LogInfo("==============Get Status==============")         
        cla_00.get_status("01")                                
        outstat = Buffer.SW.Get(start=1,length=2)
        IsStatusOk("9000",outstat)           
        
        ################################################### 
        
        Mobile.TerminalResponse("8103012700820282818301002401012503000010" )
        

        
        if isPollOff=="true":
             Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData= "D009810301040082028182", expectedStatus="9000")  
             Mobile.TerminalResponse("8103010400 82028281 030100 ")
             
             ##########################################
             LogInfo("==============Get Status==============")         
             cla_00.get_status("01")                                
             outstat = Buffer.SW.Get(start=1,length=2)
             IsStatusOk("9000",outstat)           
        
              ################################################### 
             
             
             isPollOff="false"
        if a==1:
              ##########################################
              LogInfo("==============Get Status==============")         
              cla_00.get_status("01")                                
              outstat = Buffer.SW.Get(start=1,length=2)
              IsStatusOk("911D",outstat)           
        
              ################################################### 
              LogInfo("==============Fetch Refresh==============")               
              Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")
              Mobile.TerminalResponse("8103010400 82028281 030100 ") 
        
              LogInfo("==============Fetch Poll Off==============") 
              Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData= "D009810301040082028182", expectedStatus="9000")  
              Mobile.TerminalResponse("8103010400 82028281 030100 ")
              a=0
        LogInfo('''**Envelope Timer Expiration**''')    
        Mobile.Envelope("D70C820282812401012503000010" ) 

        LogInfo('#---------------------------------------')  
        LogInfo("**Retry "+str(i+1)+"**")    
        ReqHTTPAdminSession_Failed_2()
        LogInfo('#---------------------------------------')  
    pass  

def Retry_Session_2a(isPollOff=None):
    LogInfo('#---------------------------------------')
    LogInfo('# OS RETRY MECHANISM                    ')
    LogInfo('#---------------------------------------')    
    for i in range (2):
        LogInfo('''**Timer Management**''')
        Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")  
        ##########################################
        LogInfo("==============Get Status==============")         
        cla_00.get_status("01")                                
        outstat = Buffer.SW.Get(start=1,length=2)
        IsStatusOk("9000",outstat)       
        ###################################################         
        Mobile.TerminalResponse("8103012700820282818301002401012503000010" )
        
        if isPollOff=="true":
             Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData= "D009810301040082028182", expectedStatus="9000")  
             Mobile.TerminalResponse("8103010400 82028281 030100 ")
             ##########################################
             LogInfo("==============Get Status==============")         
             cla_00.get_status("01")                                
             outstat = Buffer.SW.Get(start=1,length=2)
             IsStatusOk("9000",outstat)       
              ###################################################  
              
             isPollOff="false"
             
             ##########################################
             LogInfo("==============Get Status==============")         
             cla_00.get_status("01")                                
             outstat = Buffer.SW.Get(start=1,length=2)
             IsStatusOk("911D",outstat)           
              ################################################### 
             LogInfo("==============Fetch Refresh==============")               
             Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")
             Mobile.TerminalResponse("8103010400 82028281 030100 ") 
        
             LogInfo("==============Fetch Poll Off==============") 
             Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData= "D009810301040082028182", expectedStatus="9000")  
             Mobile.TerminalResponse("8103010400 82028281 030100 ")
              
              
        LogInfo('''**Envelope Timer Expiration**''')    
        Mobile.Envelope("D70C820282812401012503000010" ) 
        LogInfo('#---------------------------------------')  
        LogInfo("**Retry "+str(i+1)+"**")    
        ReqHTTPAdminSession_Failed()
        LogInfo('#---------------------------------------')  
    pass    

def ReqHTTPAdminSession_Failed():
    LogInfo('#---------------------------------------')
    LogInfo('# PROCESS HANDSHAKE With Bip Channel    ')
    LogInfo('#---------------------------------------')

    LogInfo ("## Init TLS session")
    tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_CIPHER_NULL"],"4F544132","F0C0FAAC 0EF1364A 3E5EB422 9CF797A3 752CD0C8 27784457 6B3E05D5 05A03F21 E0ED11BF EBE40B85 E2EC25AE 06B6FF60 D3D47FA1 26403743 C0AE5EAC F0AE71C4")
#    tlsServer.InitTlsSession( "TLS_SERVER_1_0",["TLS_PSK","TLS_CIPHER_NULL"],"4F544132","F0C0FAAC 0EF1364A 3E5EB422 9CF797A3 752CD0C8 27784457 6B3E05D5 05A03F21 E0ED11BF EBE40B85 E2EC25AE 06B6FF60 D3D47FA1 26403743 C0AE5EAC F0AE71C4")

    LogInfo(''' Open Channel Process ''')
#    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=None, expectedStatus='9000')
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=Open_Channel, expectedStatus='9000')
    output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_COMMAND_PERFORMED1 + CHANNEL_STATUS + CHANNEL_ID1 + "35 07 02 00 00 03 00 00 02" +"39 02 3A 98",expectedStatus = OK_FETCH)
    LogInfo("## TLS PSK PROCESS HANDSHAKE")
    LogInfo("#card should generate ClientHello")
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=None, expectedStatus='9000')
    output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + "030130" + CHANNEL_STATUS + CHANNEL_ID1 + "35 07 02 00 00 03 00 00 02" +"39 02 3A 98",expectedStatus = OK_FETCH)
    
#    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")
    ret=tlsServer.ProcessHandshake(output[0])
    LogInfo(''' Close Channel ''')
    tlsServer.CloseTlsConnection()
    tlsServer.CloseTlsSession() 
    Mobile.Fetch(output[1][2:], None , expectedStatus =OK)
    Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + "030100" + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)


def Retry_Session(isPollOff=None):
    LogInfo('#---------------------------------------')
    LogInfo('# OS RETRY MECHANISM                    ')
    LogInfo('#---------------------------------------')    
    for i in range (2):
        LogInfo('''**Timer Management**''')
        Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")  
        Mobile.TerminalResponse("8103012700820282818301002401012503000010" )
        if isPollOff=="true":
             Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData= "D009810301040082028182", expectedStatus="9000")  
             Mobile.TerminalResponse("8103010400 82028281 030100 ")
             isPollOff="false"
             
        LogInfo('''**Envelope Timer Expiration**''')    
        Mobile.Envelope("D70C820282812401012503000010" ) 
        LogInfo('#---------------------------------------')  
        LogInfo("**Retry "+str(i+1)+"**")    
        ReqHTTPAdminSession_Failed()
        LogInfo('#---------------------------------------')  
    pass  




def read_config():
    ListOff()
    PowerOn()    
    LogInfo('----- Read value of EF_Config -----')
    Mobile.SetMode("3G")
    Mobile.Send('00A4000C02'+'3F00')
    Mobile.Send('00A4000C02'+'7FDE')
    Mobile.Send('00A4000C02'+'1538')
    Mobile.VerifyPin('0A08933F57845F706921', expectedStatus='9000')      
    return Mobile.ReadBinary("00", "00", "09",expectedStatus='9000')  
    ListOn()      
    PowerOff()     
    
    
def read_iccid():
    listOff()
    PowerOn()
    Mobile.VerifyCode('0008933F57845F706921', expectedStatus='9000')
    Mobile.SetMode("2G")
    Mobile.Select("3F00")
    Mobile.Select("2FE2")
    Mobile.GetResponse(Buffer.SW.Get(start=2,length=1),expectedData=None,expectedStatus='9000')
    size= Buffer.R.Get(start=3,length=2)
    ICCID=Mobile.Send("A0B000000A")
    ICCID=ICCID[0]
    ICCID_ASCII=""
    for i in range (ICCID.__len__()):
        ICCID_ASCII=ICCID_ASCII + "3"+ICCID[i]
    pass
    print(ICCID_ASCII)
    listOn()    

def status_command1(set,duration,status):
    Loop=round(duration/30)
    if set==0:
        pass
    else:
        if duration>set:
            Loop=Loop
    for i in range (Loop):
        duration=duration-30
        if duration >=set:
           expectedStatus=status 
        else:
           if i>= (Loop-1):
               expectedStatus=status
           else:     
               expectedStatus="9000" 
        cla_00.get_status("01")  
        outstat = Buffer.SW.Get(start=1,length=2)
        IsStatusOk(expectedStatus,outstat)
        i+=1
    pass

def status_command(mode,set,duration,status):
    Loop=round(duration/30)
    if set==0:
        pass
    else:
        if duration>set:
            Loop=Loop
    for i in range (Loop):
        duration=duration-30
        if duration >=set:
           expectedStatus=status 
        else:
           if i>= (Loop-1):
               expectedStatus=status
           else:     
               expectedStatus="9000" 
        if (mode=="3G"):       
            cla_00.get_status("01")  
        else:
            cla_A0.get_status("01") 
        outstat = Buffer.SW.Get(start=1,length=2)
        IsStatusOk(expectedStatus,outstat)
        i+=1
    pass
# ubah CR000487 
def ReqHTTPAdminSession(IsPollOff=None):
    LogInfo('#---------------------------------------')
    LogInfo('# PROCESS HANDSHAKE With Bip Channel    ')
    LogInfo('#---------------------------------------')

    LogInfo ("## Init TLS session")
    
    #lidia
    #tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_CIPHER_NULL"],"4F544132","F8195839102919DEAB908947198438FA3489DEAE78BC45EC3489DEAE78BC45EC")
    #tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_AES_128"],"383030313032383131303633363431363033313030303030393433333746303030303030303030303031344631304130303030303035353931303130464646464646464638393030303031323030383230313031383330313431","F0C0FAAC0EF1364A3E5EB4229CF797A3752CD0C8277844576B3E05D505A03F21")
  
    #dakota 3.1 tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_AES_128"],"383030313032383131303633363431363033313030303030393433333746303030303030303030303031344631304130303030303035353931303130464646464646464638393030303031323030383230313031383330313431","F0C0FAAC0EF1364A3E5EB4229CF797A3752CD0C8277844576B3E05D505A03F21")
    #tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_CIPHER_NULL"],"383030313032383131303633363431363033313030303030393433333746303030303030303030303031344631304130303030303035353931303130464646464646464638393030303031323030383230313031383330313431,"F0C0FAAC 0EF1364A 3E5EB422 9CF797A3 752CD0C8 27784457 6B3E05D5 05A03F21 E0ED11BF EBE40B85 E2EC25AE 06B6FF60 D3D47FA1 26403743 C0AE5EAC F0AE71C4)
   
   #MCC_daimler
    #tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_AES_128"],"383030313032383131303633363431363033313030303030393433333746303030303030303030303034344631304130303030303035353931303130464646464646464638393030303030313030383230313034383330313430","F8195839102919DEAB908947198438FA")
    #tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_CIPHER_NULL"],"383030313032383131303633363431363033313030303030393433333746303030303030303030303034344631304130303030303035353931303130464646464646464638393030303030313030383230313034383330313430","F8195839102919DEAB908947198438FA")
    tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_AES_128"],"383030313032383131303633363431363033313030303030393433333746303030303030303030303034344631304130303030303035353931303130464646464646464638393030303030313030383230313031383330313431","F8195839102919DEAB908947198438FA")

   

    #bfore 
    #tlsServer.InitTlsSession( "TLS_SERVER_1_0",["TLS_PSK","TLS_CIPHER_NULL"],"4F544132","F0C0FAAC 0EF1364A 3E5EB422 9CF797A3 752CD0C8 27784457 6B3E05D5 05A03F21 E0ED11BF EBE40B85 E2EC25AE 06B6FF60 D3D47FA1 26403743 C0AE5EAC F0AE71C4")
   
    #lidia
   # Psk_ID                      = "4F544132"
    #SCP81_key                   = "F0C0FAAC0EF1364A3E5EB4229CF797A3" \
                              #"752CD0C8277844576B3E05D505A03F21" \
                              #"E0ED11BFEBE40B85E2EC25AE06B6FF60" \
                              #"D3D47FA126403743C0AE5EACF0AE71C4"

         
    #tlsServer.InitTlsSession("TLS_SERVER_1_2",["TLS_PSK","TLS_AES_128"],Psk_ID,SCP81_key)   
    LogInfo(''' Open Channel Process ''')
    #Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=None, expectedStatus='9000')
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=Open_Channel, expectedStatus='9000')
    #before - softbank output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_COMMAND_PERFORMED1 + CHANNEL_STATUS + CHANNEL_ID1 + "35 07 02 00 00 03 00 00 02" +"39 02 3A 98",expectedStatus = OK_FETCH)
    #output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_COMMAND_PERFORMED1 + CHANNEL_STATUS + CHANNEL_ID1 + "35 01 03" +"39 02 05 DC",expectedStatus = OK_FETCH)
    output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_COMMAND_PERFORMED1 + CHANNEL_STATUS + CHANNEL_ID1 + "35 01 03" +"39 08 00 ",expectedStatus = OK_FETCH)
    LogInfo("## TLS PSK PROCESS HANDSHAKE")
    LogInfo("#card should generate ClientHello")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    if IsPollOff=="true":
        output= bipChannel.RcvDataFromCardViaBip(output,
                                                 expectedStatus = "91xx")
        Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData="D009810301040082028182", expectedStatus="9000")
        Mobile.TerminalResponse("8103010400 82028281 030100" )
    else:
        output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91XX")     
    return output
    pass

def ReqHTTPAdminSession_(IsPollOff=None):
    LogInfo('#---------------------------------------')
    LogInfo('# PROCESS HANDSHAKE With Bip Channel    ')
    LogInfo('#---------------------------------------')

    LogInfo ("## Init TLS session")
    tlsServer.InitTlsSession( "TLS_SERVER_1_2",["TLS_PSK","TLS_CIPHER_NULL"],"4F544132","F0C0FAAC 0EF1364A 3E5EB422 9CF797A3 752CD0C8 27784457 6B3E05D5 05A03F21 E0ED11BF EBE40B85 E2EC25AE 06B6FF60 D3D47FA1 26403743 C0AE5EAC F0AE71C4")
#    tlsServer.InitTlsSession( "TLS_SERVER_1_0",["TLS_PSK","TLS_CIPHER_NULL"],"4F544132","F0C0FAAC 0EF1364A 3E5EB422 9CF797A3 752CD0C8 27784457 6B3E05D5 05A03F21 E0ED11BF EBE40B85 E2EC25AE 06B6FF60 D3D47FA1 26403743 C0AE5EAC F0AE71C4")
  
    LogInfo(''' Open Channel Process ''')
#    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=None, expectedStatus='9000')
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=Open_Channel, expectedStatus='9000')
    
    LogInfo("======================Envelope SMS EF_DIR========================")
    message = "00A4000C023F00 00A4000C022F00 00DC010424 611C4F10A0000000871002FF81FF2089030600005008536F667442616E6B00000000F789"                  
                    
    Envelope_OTA_Submit_002100(message,"9000",Ok_PoR)
    
    output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_COMMAND_PERFORMED1 + CHANNEL_STATUS + CHANNEL_ID1 + "35 07 02 00 00 03 00 00 02" +"39 02 3A 98",expectedStatus = OK_FETCH)
#    output = Mobile.TerminalResponse(OPEN_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_COMMAND_PERFORMED1 )
    
    LogInfo("## TLS PSK PROCESS HANDSHAKE")
    LogInfo("#card should generate ClientHello")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')

    if IsPollOff=="true":
        output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")
        Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData="D009810301040082028182", expectedStatus="9000")
        Mobile.TerminalResponse("8103010400 82028281 030100" )
    else:
        output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")     
    return output

    
    pass



def HTTPAdminSessionProcessRFM_CR (dataRFM,output,Poll=None,statComm=None,CSIM=None,NoAID=None,nosuccess=None):

    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    tar="B20100"
#    K = str(message)
    L = "01000000"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "02"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "02"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000000"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    
 #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("05")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000001") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 
      
    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
         
    if CSIM == "true":     
        LogInfo("##Encapsulate APDUs in HTTP frames")
        list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
                (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
                (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000343/1002FF81FF208902000000"),
                ]
        httpserver.MakeHeader(httpmode,list)
        postresponse = httpserver.CreateResponse(Buffer.J.Get())

        LogInfo("##Display HTTP Response")
        httpserver.DisplayResponse(postresponse[0])

        LogInfo("##Cipher HTTP post Response")
        output = tlsServer.CipherData(postresponse[1])

        LogInfo("##Send HTTP response to the card")
        output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
        LogInfo('#------------------------------------------------------------------------------')
        LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
        LogInfo('#------------------------------------------------------------------------------')
        
        LogInfo("##Receive HTTP Request from the card via BIP")
        output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")   
        
        LogInfo("## Decipher http post request")
        data = tlsServer.DecipherData(output[0])
        expectedPor = None
        chunkedExpectedPor = False  
               
        LogInfo("## Parse http post request ")
        #    Check_Error = "00"
        result_code_arrangement = "AF80"+"23029000"+"23"+lv("9000")+"0000"    
        
    elif NoAID == "true":        
        LogInfo("##Encapsulate APDUs in HTTP frames")
        list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
                (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
                (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000077/02076011101001000000D5"),
                ]
        httpserver.MakeHeader(httpmode,list)
        postresponse = httpserver.CreateResponse(Buffer.J.Get())

        LogInfo("##Display HTTP Response")
        httpserver.DisplayResponse(postresponse[0])

        LogInfo("##Cipher HTTP post Response")
        output = tlsServer.CipherData(postresponse[1])

        LogInfo("##Send HTTP response to the card")
        output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")      
        
        LogInfo('#------------------------------------------------------------------------------')
        LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
        LogInfo('#------------------------------------------------------------------------------')
    
        LogInfo("##Receive HTTP Request from the card via BIP")
        output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")  
        
        Mobile.Fetch(output[1][2:],expectedData =None ,expectedStatus = "9000" )   
        Mobile.Send ("801400000F 8103014301 82028121 030100 370100 ")
        Mobile.Fetch("0B",expectedData =None ,expectedStatus = "9000" )   
        Mobile.Send ("801400000F 8103014301 82028121 030100 370100 ")
    
        LogInfo("## Decipher http post request")
        data = tlsServer.DecipherData(output[0])
        expectedPor = None
        chunkedExpectedPor = False  
               
        LogInfo("## Parse http post request ")
        Check_Error = "00"
        result_code_arrangement = "AF80"+"23029000"+"23"+lv(Check_Error+"9000")+"0000"
   
    else:      
     
        LogInfo("##Encapsulate APDUs in HTTP frames")
        list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
                (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
                (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000087/1002FF81FF208903060000"),
                ]

        httpserver.MakeHeader(httpmode,list)
        postresponse = httpserver.CreateResponse(Buffer.J.Get())

        LogInfo("##Display HTTP Response")
        httpserver.DisplayResponse(postresponse[0])

        LogInfo("##Cipher HTTP post Response")
        output = tlsServer.CipherData(postresponse[1])

        LogInfo("##Send HTTP response to the card")
        output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
        LogInfo('#------------------------------------------------------------------------------')
        LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
        LogInfo('#------------------------------------------------------------------------------')
    
        if Poll == "true":
            LogInfo("##Receive HTTP Request from the card via BIP")
            output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")    
            #        LogInfo("##Send HTTP response to the card")
            #        Mobile.Send ("80C2000010 D60E19010982022181380281003701F2")
            LogInfo("===========Fetch Poll Interval=================")    
            Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData="D00D8103010300820281828402011E", expectedStatus='9000')
            Mobile.TerminalResponse('8103010300 82028281 830100 8402011E',expectedStatus='9000')
            LogInfo("================================================")  
            if statComm == "true":
                LogInfo("==============Get Status==============")         
                cla_00.get_status("01")                                
                outstat = Buffer.SW.Get(start=1,length=2)
                IsStatusOk("9000",outstat)
            else:    
                pass
        else:    
            LogInfo("##Receive HTTP Request from the card via BIP")
            output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")    
            LogInfo("##Send HTTP response to the card")            
        
   
            LogInfo("## Decipher http post request")
            data = tlsServer.DecipherData(output[0])
            expectedPor = None
            chunkedExpectedPor = False  
            
            if nosuccess == "true":
                LogInfo("## Parse http post request ")
                    #    Check_Error = "00"
                result_code_arrangement = "AF80"+"23029000"+"23"+lv("6700")+"0000" 
            else:  
                LogInfo("## Parse http post request ")
            #    Check_Error = "00"
                result_code_arrangement = "AF80"+"23029000"+"23"+lv("9000")+"0000"       
    
    LEN=str(ComputeLengthHex(result_code_arrangement))
   
    expectedPor = ByteToHex(LEN+ "\r\n")+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
               
    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
        
    LogInfo("---- Check Data Response from Server Here ----")
    LogInfo(DataTobeRead)
        
#    LogInfo("#Check Por")
#    if(expectedPor != None):
#            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
#            pass

def HTTPAdminSessionProcessRFM (dataRFM,output,CSIM=None,NoAID=None,nosuccess=None, mode="2G"):

    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    #tar="B20100"
    tar ="B00010"
#    K = str(message)
    L = "01000000"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "02"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "02"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000000"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    
 #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA" 
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA" 
    #lidia
    #DLKEY_KIC1          =   "1011121314151613"+"18191A1B1C1D1E1F"
    #DLKEY_KID1          =   "0111213141516172"+"8191A1B1C1D1E1F2"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    #Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
    #Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_crypto_verif("02")                  
    Dll.OTA2.change_cipher("00")
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

    #Dll.OTA2.change_kic("00")
    #Dll.OTA2.change_kid("50")
    #lidia
    Dll.OTA2.change_kic("10")
    Dll.OTA2.change_kid("10")
    Dll.OTA2.change_algo_cipher("00") 
    
    #softbank Dll.OTA2.change_algo_cipher("05")                          #* KiC value
    
    Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
    Dll.OTA2.change_algo_cipher("00")  #test profile beta
    Dll.OTA2.change_counter("0000000001") 
    
    #before and test kartu profile beta
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA") #AWAL
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") #AWAL
    #LIDIA
    #Dll.OTA2.set_dlkey_kic("1011121314151613"+"18191A1B1C1D1E1F")
    #Dll.OTA2.set_dlkey_kid("0111213141516172"+"8191A1B1C1D1E1F2")

    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
    
    #put timer       
    #Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData="D0118103012700820281822401012503000001", expectedStatus="9000")
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData="D0118103012700820281822401012503002000", expectedStatus="9000")
    #Mobile.Send("801400000F810301270082028281830100240101")
    Mobile.TerminalResponse("810301270082028281830100240101","9000")
                    
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
    
        
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    #output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
    
    #kluarin timer
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91XX" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
    #fetch timer 
    #Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData="D0118103012700820281822401012503000001", expectedStatus="9000")
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData="D0118103012700820281822401012503002000", expectedStatus="9000")
    Mobile.TerminalResponse("810301270082028281830100240101","9000")
       
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
    print(Buffer.J.Get())     
    if CSIM == "true":     
        LogInfo("##Encapsulate APDUs in HTTP frames")
        list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
                (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
                (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000343/1002FF81FF208902000000"),
                ]
   
        httpserver.MakeHeader(httpmode,list)
        postresponse = httpserver.CreateResponse(Buffer.J.Get())

        LogInfo("##Display HTTP Response")
        httpserver.DisplayResponse(postresponse[0])

        LogInfo("##Cipher HTTP post Response")
        output = tlsServer.CipherData(postresponse[1])

        LogInfo("##Send HTTP response to the card")
        output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
               
        LogInfo('#------------------------------------------------------------------------------')
        LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
        LogInfo('#------------------------------------------------------------------------------')
        
        LogInfo("##Receive HTTP Request from the card via BIP")
        output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")   
        
        LogInfo("## Decipher http post request")
        data = tlsServer.DecipherData(output[0])
        expectedPor = None
        chunkedExpectedPor = False  
               
        LogInfo("## Parse http post request ")
        #    Check_Error = "00"
        result_code_arrangement = "AF80"+"23029000"+"23"+lv("9000")+"0000"    

    elif NoAID == "true":        
        LogInfo("##Encapsulate APDUs in HTTP frames")
        list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
                (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
                (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000077/02076011101001000000D5"),
                ]
        httpserver.MakeHeader(httpmode,list)
        postresponse = httpserver.CreateResponse(Buffer.J.Get())

        LogInfo("##Display HTTP Response")
        httpserver.DisplayResponse(postresponse[0])

        LogInfo("##Cipher HTTP post Response")
        output = tlsServer.CipherData(postresponse[1])

        LogInfo("##Send HTTP response to the card")
        output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")      
    
        LogInfo('#------------------------------------------------------------------------------')
        LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
        LogInfo('#------------------------------------------------------------------------------')
    
        LogInfo("##Receive HTTP Request from the card via BIP")
        output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")  
        
        Mobile.Fetch(output[1][2:],expectedData =None ,expectedStatus = "9000" )   
        Mobile.Send ("801400000F 8103014301 82028121 030100 370100 ")
        Mobile.Fetch("0B",expectedData =None ,expectedStatus = "9000" )   
        Mobile.Send ("801400000F 8103014301 82028121 030100 370100 ")
    
        LogInfo("## Decipher http post request")
        data = tlsServer.DecipherData(output[0])
        expectedPor = None
        chunkedExpectedPor = False  
               
        LogInfo("## Parse http post request ")
        Check_Error = "00"
        result_code_arrangement = "AF80"+"23029000"+"23"+lv(Check_Error+"9000")+"0000"
   
    else:      
     
       # LogInfo("##Encapsulate APDUs in HTTP frames")
       # list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
       #         (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
       #         (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000087/1002FFFFFFFF89030100")]   #A000000343 1002FF81FF208902000000
#A000000087/1002FF81FF208903060000                      awal 1002FF81FF208903060000
        
        LogInfo("##Encapsulate APDUs in HTTP frames")
        if (mode == "2G"):
            list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
                    (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
                    (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000009/0001")]   #A000000151 000000A5049F6501FF //AID for GSM
        else:
            list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
                    (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
                    (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000087/1002FFFFFFFF89")] #//AID for USIM

        
        
        httpserver.MakeHeader(httpmode,list)
        postresponse = httpserver.CreateResponse(Buffer.J.Get())

        LogInfo("##Display HTTP Response")
        httpserver.DisplayResponse(postresponse[0])

        LogInfo("##Cipher HTTP post Response")
        output = tlsServer.CipherData(postresponse[1])

        LogInfo("##Send HTTP response to the card")
        output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
        LogInfo('#------------------------------------------------------------------------------')
        LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
        LogInfo('#------------------------------------------------------------------------------')
    
#        if Poll == "true":
#            LogInfo("##Receive HTTP Request from the card via BIP")
#            output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")    
#            #        LogInfo("##Send HTTP response to the card")
#            #        Mobile.Send ("80C2000010 D60E19010982022181380281003701F2")
#            LogInfo("===========Fetch Poll Interval=================")    
#            Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData="D00D8103010300820281828402011E", expectedStatus='9000')
#            Mobile.TerminalResponse('8103010300 82028281 830100 8402011E',expectedStatus='9000')
#            LogInfo("================================================")  
#            if statComm == "true":
#                LogInfo("==============Get Status==============")         
#                cla_00.get_status("01")                                
#                outstat = Buffer.SW.Get(start=1,length=2)
#                IsStatusOk("9000",outstat)
#            else:    
#                pass
#        else:    
#            LogInfo("##Receive HTTP Request from the card via BIP")
#            output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")    
#            LogInfo("##Send HTTP response to the card")            
        LogInfo("##Receive HTTP Request from the card via BIP")
        output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91XX")
        #fetch timer 
        #Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData="D0118103012700820281822401012503000001", expectedStatus="9000")
        Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData="D0118103012700820281822401012503002000", expectedStatus="9000")
        Mobile.TerminalResponse("810301270082028281830100240101","9000")
        
        LogInfo("##Send HTTP response to the card")  
   
        LogInfo("## Decipher http post request")
        data = tlsServer.DecipherData(output[0])
        expectedPor = None
        chunkedExpectedPor = False  
            
        if nosuccess == "true":
           LogInfo("## Parse http post request ")
            #    Check_Error = "00"
           result_code_arrangement = "AF80"+"23029000"+"23"+lv("6700")+"0000" 
        else:  
           LogInfo("## Parse http post request ")
           #    Check_Error = "00"
           result_code_arrangement = "AF80"+"23029000"+"23"+lv("9000")+"0000"       
    LEN=str(ComputeLengthHex(result_code_arrangement))
    expectedPor = ByteToHex(LEN+ "\r\n")+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
    LogInfo("---- Check Data Response from Server Here ----")
    LogInfo(DataTobeRead)
#    LogInfo("#Check Por")
#    if(expectedPor != None):
#            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
#            pass



def HTTPAdminSessionProcessRFMadd (dataRFM,output):

    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    tar="B20100"
#    K = str(message)
    L = "01000000"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "02"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "02"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000000"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    
 #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("05")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000001") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 


       
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000087/1002FF81FF208903060000"),
            ]

    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(Buffer.J.Get())

    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])

    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])

    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
    LogInfo('#------------------------------------------------------------------------------')

    
#    if NoPoll == "true":
    LogInfo("##Receive HTTP Request from the card via BIP")
    output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")    
    LogInfo("##Send HTTP response to the card")   
#    else:
#        LogInfo("##Receive HTTP Request from the card via BIP")
#        output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")    
#        LogInfo("##Send HTTP response to the card")
#        LogInfo("===========Fetch Poll Interval=================")    
#        Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData="D00D8103010300820281828402011E", expectedStatus='9000')
#        Mobile.TerminalResponse('8103010300 82028281 830100 8402011E',expectedStatus='9000')
#        LogInfo("================================================")                   
    
   
    LogInfo("## Decipher http post request")
    data = tlsServer.DecipherData(output[0])
    expectedPor = None
    chunkedExpectedPor = False  
               
    LogInfo("## Parse http post request ")
#    Check_Error = "00"
    result_code_arrangement = "AF80"+"23029000"+"23"+lv("9000")+"0000"       
    
    LEN=str(ComputeLengthHex(result_code_arrangement))
   
    expectedPor = ByteToHex(LEN+ "\r\n")+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
               
    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
        
    LogInfo("---- Check Data Response from Server Here ----")
    LogInfo(DataTobeRead)
        
#    LogInfo("#Check Por")
#    if(expectedPor != None):
#            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
#            pass



def ContinueSendStoreDataHTTP(PoRData,StoreData):
    LogInfo("-------- SEND STORE DATA AGAIN ----------") 
    headerTargetAppliValue = None 
    #Applets={'AID1':'A000000077020760110000FE0000C500'}
    ApplicationAID=Applets['AID1']
    nextURI = "/server/adminagent?cmd=1"
    RW_APDU = StoreData

    LogInfo("-------- Encapsulated RW_APDU Inside Stored Data using Expanded INDEFINITE Format ----------")  
    StoreDataAPDU="80E20100"+lv(RW_APDU)+"00"
    
    array2 = []        
    array2.append(StoreDataAPDU)  
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
        
        
    LogInfo ('-------------- HERE FOR DATA EXCHANGE is RAM MODEL ----------------------------')
    #Set Header
                
    headerNextURI = nextURI
    if(headerTargetAppliValue == None):
                    #No "X-Admin-Targeted-Application" in the HTTP POST response, 
                    #the targeted Security Domain is the one which provides the PSK TLS security of the communication channel
                    list = [(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_CONTENT_TYPE, HttpAmdB.CONTENT_TYPE_RAM),
                            (HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_NEXT_URI, headerNextURI),
                            (HttpAmdB.REMOVE_HEADER, HttpAmdB.HEADER_TARGETED_APPLI)
                    ]
    else:
                    #"X-Admin-Targeted-Application" header is present in the HTTP POST response, 
                    #the header value shall be read as the instance AID of the targeted Security Domain.
                    list = [(HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_CONTENT_TYPE, HttpAmdB.CONTENT_TYPE_RAM),
                            (HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_NEXT_URI, headerNextURI),
                            (HttpAmdB.CHANGE_VALUE, HttpAmdB.HEADER_TARGETED_APPLI, headerTargetAppliValue)
                    ]
                    pass                                                                                                
        
    LogInfo("##Construct buffer with C-apdus [Put APDU into Buffer for processed in HTTP Mode on Next Step]")
        
        
    ##Set body in Expanded Infinite format
    LogInfo("## Construct C-apdus with expanded format")
    arrayBody = array2   
           
    LogInfo ("##Construct install for perso command ")
    ##To construct 80E62000 16 0000  10 A0000000770207601100010000002200 000000
        
    body = "80E62000" + lv("0000"+ lv(ApplicationAID) + "000000")
    body = Utilities.AppendScript(body, "EXPANDED")
        
    for i in range(len(arrayBody)):
            body = body + Utilities.AppendScript(arrayBody[i], "EXPANDED")
        #    
    body = Utilities.EndMessage(body, "INDEFINITE")
        
    expectedPor = None
    chunkedExpectedPor = False
        
    LogInfo("## Encapsulate APDUs in HTTP frames")
    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(body)
        
    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])
        
    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])
#    *****   
    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip ("00112233", expectedStatus = "9000")
   
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
    LogInfo('#------------------------------------------------------------------------------')

    LogInfo("## Receive HTTP Request from the card via BIP")

    cektype=StoreData[0:2]

    if cektype=="02":
        store1 = StoreData[0:4]
        len_command = lv(StoreData[4:])
        
        len_command = len_command[:2]
        length = store1[2:]
        
        LogInfo(len_command)
    
        LogInfo(length)
        
        if len_command == length :
            stat = "91XX"
            output = bipChannel.RcvDataFromCardViaBip(output, expectedStatus = stat)
            
            LogInfo("Fetch SetUp Menu")
            IsStatusOk('91XX',Buffer.SW.Get(start=1,length=2))
            Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData=None, expectedStatus='9000')
 
            LogInfo(''' Send Terminal Response Set Up Menu ''')
            Mobile.TerminalResponse('810301250082028281830100',expectedStatus='9000')   
 
        else :
            output = bipChannel.RcvDataFromCardViaBip(output, expectedStatus = OK)
   
    else :
        output = bipChannel.RcvDataFromCardViaBip(output, expectedStatus = OK)
                  
    LogInfo("## Decipher http post request")
    data = tlsServer.DecipherData(output[0])
        
               
    LogInfo("## Parse http post request ")
    Check_Error = PoRData
    result_code_arrangement = "AF80"+"23029000"+"23"+lv(Check_Error+"9000")+"0000"       
    
    LEN=str(ComputeLengthHex(result_code_arrangement))
   
    expectedPor = ByteToHex(LEN+ "\r\n")+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
               
    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
        
    LogInfo("---- Check Data Response from Server Here ----")
    LogInfo(DataTobeRead)
        
    LogInfo("#Check Por")
    if(expectedPor != None):
            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
            pass
                
    pass

def CloseSession_dulu(TR_CloseSession = None) :        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# Close Session and Close Channel in the End ')
    LogInfo('#------------------------------------------------------------------------------')
        
    LogInfo("## Last Post response")
    lastResponse = httpserver.LastResponse()       
    httpserver.DisplayResponse(lastResponse[0])
               
    LogInfo("## Send HTTP response - 'NO Content' - to the card")
    output = tlsServer.CipherData(lastResponse[1])
    output = bipChannel.SendDataToCardViaBip(output, expectedStatus = OK_FETCH)
               
    LogInfo("## Receive data")
    output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = OK_FETCH)
        
    LogInfo("## Close Bip channel")
    Mobile.Fetch(output[1][2:], None , expectedStatus =OK)

    if TR_CloseSession == None :
        TR_CloseSession = TR_COMMAND_PERFORMED1
    else :
        TR_CloseSession = TR_CloseSession
    output = Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_CloseSession + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)
        
#    LogInfo("## Close Tls connection and session")
    tlsServer.CloseTlsConnection()
    tlsServer.CloseTlsSession() 

def CloseSession(TR_CloseSession = None,statComm=None,statComm2=None) :        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# Close Session and Close Channel in the End ')
    LogInfo('#------------------------------------------------------------------------------')
        
    LogInfo("## Last Post response")
    lastResponse = httpserver.LastResponse()       
    httpserver.DisplayResponse(lastResponse[0])
               
    LogInfo("## Send HTTP response - 'NO Content' - to the card")
    output = tlsServer.CipherData(lastResponse[1])
    output = bipChannel.SendDataToCardViaBip(output, expectedStatus = OK_FETCH)
               
    LogInfo("## Receive data")
    output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = OK_FETCH)
        
    LogInfo("## Close Bip channel")
    Mobile.Fetch(output[1][2:], None , expectedStatus =OK)
    if statComm == "true":
        LogInfo("==============Get Status==============")         
        cla_00.get_status("01")                                
        outstat = Buffer.SW.Get(start=1,length=2)
        IsStatusOk("9000",outstat)
    else:
        pass    

    if TR_CloseSession == None :
        TR_CloseSession = TR_COMMAND_PERFORMED1
        output = Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_CloseSession + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)
        tlsServer.CloseTlsConnection()
        tlsServer.CloseTlsSession()
    else :
        TR_CloseSession = TR_CloseSession
        output = Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_CloseSession + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)
        tlsServer.CloseTlsConnection()
        tlsServer.CloseTlsSession()
        if statComm2 == "true":
            Mobile.Fetch(output1[1][2:], None , expectedStatus =OK)
            LogInfo("==============Get Status==============")         
            cla_00.get_status("01")                                
            outstat = Buffer.SW.Get(start=1,length=2)
            IsStatusOk("9000",outstat)
            output = Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + "030100" + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)
            tlsServer.CloseTlsConnection()
            tlsServer.CloseTlsSession()    
        else:
            pass 

def HTTPAdminSessionProcessRFM_SMS (dataRFM,output):

    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    tar="B20100"
#    K = str(message)
    L = "01000000"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "02"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "02"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000000"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    
 #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("05")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000001") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 
      
    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
    
    LogInfo("======================Envelope SMS EF_MLPL========================")
    message = "00A4000C023F00 00A4000C027F10 00A4000C025F3C 00A4000C024F20 00D6000001 15"                  
    nilai = "15 FF FF FF FF FF FF FF FF " 
    Envelope_OTA_Submit_002100(message,"91XX",Ok_PoR)
        
    LogInfo(''' Fetch Send SM POR ''')            
    Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")
    Mobile.TerminalResponse('81 03 01 13 00 82 028381 830100',expectedStatus='9000')

        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000087/1002FF81FF208903060000"),
            ]

    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(Buffer.J.Get())

    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])

    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])

    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
    LogInfo('#------------------------------------------------------------------------------')
    

    LogInfo("##Receive HTTP Request from the card via BIP")
    output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")    
    LogInfo("##Send HTTP response to the card")   
   
    LogInfo("## Decipher http post request")
    data = tlsServer.DecipherData(output[0])
    expectedPor = None
    chunkedExpectedPor = False  
               
    LogInfo("## Parse http post request ")
#    Check_Error = "00"
    result_code_arrangement = "AF80"+"23029000"+"23"+lv("9000")+"0000"       
    
    LEN=str(ComputeLengthHex(result_code_arrangement))
   
    expectedPor = ByteToHex(LEN+ "\r\n")+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
               
    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
        
    LogInfo("---- Check Data Response from Server Here ----")
    LogInfo(DataTobeRead)
        
#    LogInfo("#Check Por")
#    if(expectedPor != None):
#            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
#            pass
           
def CloseSession_2(TR_CloseSession = None,statComm=None) :        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# Close Session and Close Channel in the End ')
    LogInfo('#------------------------------------------------------------------------------')
        
    LogInfo("## Last Post response")
    lastResponse = httpserver.LastResponse()       
    httpserver.DisplayResponse(lastResponse[0])
               
    LogInfo("## Send HTTP response - 'NO Content' - to the card")
    output = tlsServer.CipherData(lastResponse[1])
    output = bipChannel.SendDataToCardViaBip(output, expectedStatus = OK_FETCH)
               
    LogInfo("## Receive data")
    output1 = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = OK_FETCH)
        
        
    LogInfo("## Close Bip channel")
    Mobile.Fetch(output1[1][2:], None , expectedStatus =OK)
    ##########################################
    LogInfo("==============Get Status==============")         
    cla_00.get_status("01")                                
    outstat = Buffer.SW.Get(start=1,length=2)
    IsStatusOk("9000",outstat)
            
#    Mobile.Send("80C200000D D60B19010A82028281 38020105")
#    Mobile.Fetch(Buffer.SW.Get(start=2,length=1), expectedData=None, expectedStatus="9000")
        
        
        
    ################################################### 
    if TR_CloseSession == None :
        TR_CloseSession = TR_COMMAND_PERFORMED1
        output = Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_CloseSession + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)
        tlsServer.CloseTlsConnection()
        tlsServer.CloseTlsSession()
    else :
        TR_CloseSession = TR_CloseSession
        output = Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + TR_CloseSession + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)
        tlsServer.CloseTlsConnection()
        tlsServer.CloseTlsSession()
        
        Mobile.Fetch(output1[1][2:], None , expectedStatus =OK)
        LogInfo("==============Get Status==============")         
        cla_00.get_status("01")                                
        outstat = Buffer.SW.Get(start=1,length=2)
        IsStatusOk("9000",outstat)
        output = Mobile.TerminalResponse(CLOSE_CHANNEL_DETAILS + DEVICE_ID_SIM_TO_ME + "030100" + CHANNEL_STATUS + CHANNEL_ID1, expectedStatus = OK +"," + OK_FETCH)
        tlsServer.CloseTlsConnection()
        tlsServer.CloseTlsSession()    

#ushi
def Envelope_OTA_Submit_002100_concat(message,stat,data,tar=None,concatenated=False):
    LogInfo('.LOAD OTA2.dll')
    # Create OTA2 instance
    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    
    if tar == None :
#        tar = "B00010"
#        tar = "B00001" #//ori
         tar = "102345"
    else :
        tar = tar

    K = str(message)


    L = "01000100"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "00"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "00"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000006"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD


    #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    #DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA" #test kartu profile beta
    #DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA" #test kartu profile beta
    #DLKEY_KIC1           =   "456E637279707469" +"6F6E536563726574"
    #DLKEY_KID1           =   "4B6579456E637279" +"7074696F6E4B6579"
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
#    DLKEY_KIC1          =   "1011121314151613"+"18191A1B1C1D1E1F"
#    DLKEY_KID1          =   "0111213141516172"+"8191A1B1C1D1E1F2"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1        ori
#    Dll.OTA2.change_crypto_verif("02")                  #*bit 2 and bit 1, SPI1
#    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1     ori
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
#    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1    ori
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("01")                             #*bit 6, SPI2

#    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
#    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
#    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
#    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2


    Dll.OTA2.change_kic("00") #ori
    Dll.OTA2.change_kid("00") #ori
#    Dll.OTA2.change_kic("00")
#    Dll.OTA2.change_kid("50")
    
#    Dll.OTA2.change_algo_cipher("00")                          #* KiC value    ori
    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("00")                    #* KiD value    ori
#    Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000006") 
    
    #Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  #test kartu profile beta
    #Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") #test kartu profile beta
    #Dll.OTA2.set_dlkey_kic("456E637279707469" +"6F6E536563726574")
    #Dll.OTA2.set_dlkey_kic("4B6579456E637279" +"7074696F6E4B6579")
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA")
#    Dll.OTA2.set_dlkey_kic("1011121314151613"+"18191A1B1C1D1E1F")
#    Dll.OTA2.set_dlkey_kid("0111213141516172"+"8191A1B1C1D1E1F2")
    #;==================================================================
    Dll.OTA2.append_script(K) 
    Dll.OTA2.end_message(" M N ")
    Dll.OTA2.display_message(" N ")
    inhalt = str(N)[9:]

    if concatenated==False:
        dataEnv = inhalt
        LogInfo("## Send Envelope")
        output = Mobile.Envelope(dataEnv, expectedStatus = stat)
    else:   
        LogInfo("## Send Envelope")
        dataEnv = []
        dataEnv.append(inhalt)
        Dll.OTA2.get_message_nb(" O ")
        number_of_env = int(str(O)[9:])
        LogInfo('Number of envelope to send: ' + str(number_of_env))
        for i in range (number_of_env - 1):
            Dll.OTA2.get_next_message(" M N ")
            inhalt = str(N)[9:]
            dataEnv.append(inhalt)
        return dataEnv 

def Envelope_OTA_Submit_002100(message,stat,data,tar=None,concatenated=False):
    LogInfo('.LOAD OTA2.dll')
    # Create OTA2 instance
    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    
    if tar == None :
        tar = "B00010"
#        tar = "B00001" #//ori
#         tar = "102345"
    else :
        tar = tar

    K = str(message)


    L = "01000100"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "00"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "00"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000006"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD


    #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA" 
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA" 
    #EXISTING       DLKEY_KIC1           =   "456E637279707469" +"6F6E536563726574"
    #EXISTING       DLKEY_KID1           =   "4B6579456E637279" +"7074696F6E4B6579"
    #DLKEY_KIC1          =   "1011121314151613"+"18191A1B1C1D1E1F"
    #DLKEY_KID1          =   "0111213141516172"+"8191A1B1C1D1E1F2"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value 00
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif("02") 
    #before Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
#    Dll.OTA2.change_crypto_verif("02")                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher("00")                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("01")                             #*bit 6, SPI2

#    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
#    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
#    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
#    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

#before
    #Dll.OTA2.change_kic("00")
    #Dll.OTA2.change_kid("50")
#lidia for Dakota  3.1
#    Dll.OTA2.change_kic("00")
#    Dll.OTA2.change_kid("50")
    
#    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
    Dll.OTA2.change_kic("10")
    Dll.OTA2.change_kid("10")
    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
#before
    #Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
#lidia
    Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000006") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 
    #EXISTING      Dll.OTA2.set_dlkey_kic("456E637279707469" +"6F6E536563726574")
    #EXISTING      Dll.OTA2.set_dlkey_kic("4B6579456E637279" +"7074696F6E4B6579")
    #Dll.OTA2.set_dlkey_kic("1011121314151613"+"18191A1B1C1D1E1F")
    #Dll.OTA2.set_dlkey_kid("0111213141516172"+"8191A1B1C1D1E1F2")
    #;==================================================================
    Dll.OTA2.append_script(K) 
    Dll.OTA2.end_message(" M N ")
    Dll.OTA2.display_message(" N ")
    inhalt = str(N)[9:]

    if concatenated==False:
        dataEnv = inhalt
        LogInfo("## Send Envelope")
        output = Mobile.Envelope(dataEnv, expectedStatus = stat)
    else:   
        LogInfo("## Send Envelope")
        dataEnv = []
        dataEnv.append(inhalt)
        Dll.OTA2.get_message_nb(" O ")
        number_of_env = int(str(O)[9:])
        LogInfo('Number of envelope to send: ' + str(number_of_env))
        for i in range (number_of_env - 1):
            Dll.OTA2.get_next_message(" M N ")
            inhalt = str(N)[9:]
            dataEnv.append(inhalt)
        return dataEnv 
        

def Envelope_get_response_POR(message,stat,data,tar=None):
    LogInfo('.LOAD OTA2.dll')
    # Create OTA2 instance
    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    
    if tar == None :
#        tar = "B00010"
        tar = "B00001"
    else :
        tar = tar

    K = str(message)


    L = "01000000"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "00"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "00"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000006"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD


    #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
#    Dll.OTA2.change_crypto_verif("02")                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

#    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
#    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
#    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
#    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2


    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("00")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000006") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 

    #;==================================================================
    Dll.OTA2.append_script(K) 
    Dll.OTA2.end_message(" M N ")
    Dll.OTA2.display_message(" N ")

    LogInfo("## Send Envelope")
    #LogInfo(Buffer.M.Get())
    output = Mobile.Envelope(Buffer.N.Get(), expectedStatus = stat)


def Envelope_OTA_Submit_002100_noPOR(message,stat,tar=None): 
    LogInfo('.LOAD OTA2.dll')
    # Create OTA2 instance
    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    
    if tar == None :
        tar = "B00001"
    else :
        tar = tar

    K = str(message)


    L = "01000100"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "00"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "00"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000006"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD


    #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
#    Dll.OTA2.change_crypto_verif("02")                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("01")                             #*bit 6, SPI2

#    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
#    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
#    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
#    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2


    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("00")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000006") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 

    #;==================================================================
    Dll.OTA2.append_script(K) 
    Dll.OTA2.end_message(" M N ")
    Dll.OTA2.display_message(" N ")

    LogInfo("## Send Envelope")
    #LogInfo(Buffer.M.Get())
    output = Mobile.Envelope(Buffer.N.Get(), expectedStatus = stat)

def Envelope_OTA_MSL_not_00(message,stat,data,tar=None):
    LogInfo('.LOAD OTA2.dll')
    # Create OTA2 instance
    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    
    if tar == None :
        tar = "B00010"
#        tar = "B00001" #//ori
#         tar = "102345"
    else :
        tar = tar

    K = str(message)


    L = "01000100"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "00"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "00"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000006"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD


    #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif("02") 
    #before Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
#    Dll.OTA2.change_crypto_verif("02")                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher("00")                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("01")                             #*bit 6, SPI2

#    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
#    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
#    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
#    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

#before
    #Dll.OTA2.change_kic("00")
    #Dll.OTA2.change_kid("50")
#lidia for Dakota  3.1
#    Dll.OTA2.change_kic("00")
#    Dll.OTA2.change_kid("50")
    
#    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
    Dll.OTA2.change_kic("10")
    Dll.OTA2.change_kid("10")
    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
#before
    #Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
#lidia
    Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000006") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA")

    #;==================================================================
    Dll.OTA2.append_script(K) 
    Dll.OTA2.end_message(" M N ")
    Dll.OTA2.display_message(" N ")
    inhalt = str(N)[9:]

    LogInfo("## Send Envelope")
    #LogInfo(Buffer.M.Get())
#    output = Mobile.Envelope(Buffer.N.Get(), expectedStatus = stat)
    dataEnv = inhalt
    output = Mobile.Envelope(dataEnv, expectedStatus = stat)

def Envelope_OTA_Submit_001E00_noPOR(message,stat,tar=None): 
#    Mobile.SetMode("2G")
    LogInfo('.LOAD OTA2.dll')
    # Create OTA2 instance
    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    
    if tar == None :
#        tar = "102345"
#        tar = "B00001"  #ori
#        tar = "000000"
        tar = "B00010"
    else :
        tar = tar

    K = str(message)


    L = "01000100"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "00"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "00"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000006"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD


    #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
#    Dll.OTA2.change_crypto_verif("02")                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                               #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

#    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
#    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
#    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
#    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2


    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("00")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000006") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 

    #;==================================================================
    Dll.OTA2.append_script(K) 
    Dll.OTA2.end_message(" M N ")
    Dll.OTA2.display_message(" N ")

    LogInfo("## Send Envelope")
    #LogInfo(Buffer.M.Get())
    output = Mobile.Envelope(Buffer.N.Get(), expectedStatus = stat)
    
def Envelope_OTA_Submit_001E00_noPOR_concat(message,stat,tar=None,concatenated=False): 
#    Mobile.SetMode("2G")
    LogInfo('.LOAD OTA2.dll')
    # Create OTA2 instance
    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    
    if tar == None :
#        tar = "102345"
#        tar = "B00001"  #ori
#        tar = "000000"
        tar = "B00010"
    else :
        tar = tar

    K = str(message)


    L = "01000100"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "00"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "00"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000006"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD


    #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
#    Dll.OTA2.change_crypto_verif("02")                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                               #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

#    Dll.OTA2.change_por("01")                                    #*bit 2 and bit 1, SPI2
#    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
#    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
#    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2


    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("00")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("00")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000006") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 

    #;==================================================================
    Dll.OTA2.append_script(K) 
    Dll.OTA2.end_message(" M N ")
    Dll.OTA2.display_message(" N ")
    inhalt = str(N)[9:]
    
    if concatenated==False:
        dataEnv = inhalt
        LogInfo("## Send Envelope")
        output = Mobile.Envelope(dataEnv, expectedStatus = stat)
    else:   
        LogInfo("## Send Envelope")
        dataEnv = []
        dataEnv.append(inhalt)
        Dll.OTA2.get_message_nb(" O ")
        number_of_env = int(str(O)[9:])
        LogInfo('Number of envelope to send: ' + str(number_of_env))
        for i in range (number_of_env - 1):
            Dll.OTA2.get_next_message(" M N ")
            inhalt = str(N)[9:]
            dataEnv.append(inhalt)
        return dataEnv 


def Get_Response_Env(data):
    value = Buffer.SW.Get(start=2,length=1)
    Mobile.Send("00C00000"+value)
    
    output = Buffer.R.Get() 

    if  data == output :
        pass
    else :
        StepOn()

def ddl_process():
    #Create a DDL instance
    DDL = DDL.DDL()
    
    LogInfo("## choose protocol ")
    DDL.ChooseProtocol(protocol)
    
    LogInfo("## choose payload format ")
    DDL.SetPayloadFormat(format)
    
    LogInfo("## choose por type ")
    DDL.SetPOR(por)
    
    LogInfo("## Open bip Connection if not sms protocol")
    DDL.SetProtocolParam(TestParam)
    DDL.OpenConnection()
    DDL.GetOpenConnectionOutput()
    
    LogInfo("## Start data Download: Send payload")
    output = DDL.Send(myPayload)
    
    LogInfo("## Check the PoR ")
    DDL.GetPoR(output, expectedPoR)
    
    LogInfo("## Close bip Connection if not sms protocol")
    DDL.CloseConnection()

def HTTPAdminSessionProcessRFM_2 (dataRFM,output,NoPoll=None,PollOff=None):

    OTA2 = OtOTA2Wrapper("OTA2.dll")
    Dll.OTA2 = OTA2
    tar="B20100"
#    K = str(message)
    L = "01000000"            #(1)POR - bit 1 and 2, (2)POR Security - bit 4 and 3, (3)POR Cipher - bit 5, (4) POR Format - bit 6
    G = "02"                  #CHANGE_CNT_CHK      *bit 5 and bit 4, SPI1
    J = "02"                  #CHANGE_CRYPTO_VERIF *bit 2 and bit 1, SPI1
    Q = "00000000"            #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    H = "00"                  #CHANGE_CIPHER       *bit 3, SPI1
    I = tar+"0000000000"      #TAR, COUNTER
    Q = "0000"+"0000"         #(1)ALGO CIPHER - KiC value, (2)ALGO CRYPTO VERIF - KiD value, (3) CHANGE KiC, (4) CHANGE KiD
    
 #;========================== DEFINE =======================

    DEVICE_IDENTITIES1  =   "82028381"
    ADDRESS_TLV1        =   "060591261816F5"
    TP_OA1              =   "0C91261815464059"
    TP_PID1             =   "7F"        
    TP_DCS1             =   "F6"
    TP_SCTS1            =   "90707261658282"
    
    POR1                =   L[0:2]
    POR_SECURITY1       =   L[2:2]
    POR_CIPHER1         =   L[4:2]
    POR_FORMAT1         =   L[6:2]

    TAR1                =   I[0:6]
    COUNTER1            =   I[6:]
    KID1                =   Q[6:2]
    KIC1                =   Q[4:2]
    DLKEY_KIC1          =   "F8195839102919DE"+"AB908947198438FA"
    DLKEY_KID1          =   "F8195839102919DE"+"AB908947198438FA"
    
    ALGO_CIPHER1        =   Q[0:2]            #* KiC value
    ALGO_CRYPTO_VERIF1  =   Q[2:2]            #* KiD value
    CHANGE_CRYPTO_VERIF1=   J                 #*bit 2 and bit 1, SPI1
    CHANGE_CIPHER1      =   H                 #*bit 3, SPI1
    CHANGE_CNT_CHK1     =   G                 #*bit 5 and bit 4, SPI1

    #;============================ ENVELOPE ===========================
    LogInfo('.INIT_ENV_0348')
    Dll.OTA2.init_env_0348(" ")

    #;     .INIT_SMS_0348
    Dll.OTA2.change_device_identities_tlv(DEVICE_IDENTITIES1)            
    Dll.OTA2.change_address_tlv(ADDRESS_TLV1)                   
    Dll.OTA2.change_tp_oa(TP_OA1)                           
    Dll.OTA2.change_tp_scts(TP_SCTS1)                        
    Dll.OTA2.change_tp_pid(TP_PID1)                          
    Dll.OTA2.change_tar(TAR1)                             
    Dll.OTA2.change_tp_dcs(TP_DCS1)                         
        
    #* SPI1 = First Octet
    Dll.OTA2.change_crypto_verif(CHANGE_CRYPTO_VERIF1)                  #*bit 2 and bit 1, SPI1
    Dll.OTA2.change_cipher(CHANGE_CIPHER1)                              #*bit 3, SPI1
    Dll.OTA2.change_cnt_chk(CHANGE_CNT_CHK1)                            #*bit 5 and bit 4, SPI1
    
    #* SPI 2 = Second Octet
    Dll.OTA2.change_por("00")                                    #*bit 2 and bit 1, SPI2
    Dll.OTA2.change_por_security("00")                           #*bit 4 and bit 3, SPI2
    Dll.OTA2.change_por_cipher("00")                             #*bit 5, SPI2
    Dll.OTA2.change_por_format("00")                             #*bit 6, SPI2

    Dll.OTA2.change_kic("00")
    Dll.OTA2.change_kid("00")
    
    Dll.OTA2.change_algo_cipher("05")                          #* KiC value
    Dll.OTA2.change_algo_crypto_verif("05")                    #* KiD value
    
    Dll.OTA2.change_counter("0000000001") 
    
    Dll.OTA2.set_dlkey_kic("F8195839102919DE"+"AB908947198438FA")  
    Dll.OTA2.set_dlkey_kid("F8195839102919DE"+"AB908947198438FA") 
#    Dll.OTA2.end_message(" I J " )
#    Dll.OTA2.display_message(" J " )

    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000087/1002FF81FF208903060000"),
            ]

    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(Buffer.J.Get())

    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])

    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])

    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
    LogInfo('#------------------------------------------------------------------------------')
    

    
    if NoPoll == "true":
        LogInfo("##Receive HTTP Request from the card via BIP")
        output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")    
        LogInfo("##Send HTTP response to the card")   
    else:
        LogInfo("##Receive HTTP Request from the card via BIP")
        output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "91xx")    
        LogInfo("##Send HTTP response to the card")
#        LogInfo("===========Fetch Poll Interval=================")    
#        Mobile.Fetch(Buffer.SW.Get(start=2,length=1),expectedData="D00D8103010300820281828402011E", expectedStatus='9000')
#        Mobile.TerminalResponse('8103010300 82028281 830100 8402011E',expectedStatus='9000')
#        
#         ##########################################
        for i in range (100):
           LogInfo("==============Get Status==============")         
           cla_00.get_status("01")                                
           outstat = Buffer.SW.Get(start=1,length=2)
           IsStatusOk("9000",outstat)
           
        
        
        ################################################### 
        
        LogInfo("================================================")                   
    
   
    LogInfo("## Decipher http post request")
    data = tlsServer.DecipherData(output[0])
    expectedPor = None
    chunkedExpectedPor = False  
               
    LogInfo("## Parse http post request ")
#    Check_Error = "00"
    result_code_arrangement = "AF80"+"23029000"+"23"+lv("9000")+"0000"       
    
    LEN=str(ComputeLengthHex(result_code_arrangement))
   
    expectedPor = ByteToHex(LEN+ "\r\n")+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
               
    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
        
    LogInfo("---- Check Data Response from Server Here ----")
    LogInfo(DataTobeRead)
        
#    LogInfo("#Check Por")
#    if(expectedPor != None):
#            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
#            pass


def HTTPAdminSessionProcess_OTAPA (dataRFM,output,expectedPOR=None,TerminalProfile=None):
    OTA_init()
    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000343/1002FF81FF208902000000"),
            ]

    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(Buffer.J.Get())

    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])

    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])

    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
    LogInfo('#------------------------------------------------------------------------------')
    
    LogInfo("##Receive HTTP Request from the card via BIP")
    output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")
    
    if TerminalProfile == "true": 
        LogInfo("====================Send Terminal Profile====================")
        Mobile.TerminalProfile(TP_All, expectedStatus="91xx")
    else:    
        LogInfo("##Send HTTP response to the card")
        LogInfo("## Decipher http post request")
        data = tlsServer.DecipherData(output[0])
        if expectedPOR==None:
            result_code_arrangement = "AF80"
            for i in range (dataRFM.__len__()):
                result_code_arrangement = result_code_arrangement + "23029000"
            result_code_arrangement = result_code_arrangement + "0000"
            chunkedExpectedPor = False  
        else:
            result_code_arrangement=expectedPOR
            chunkedExpectedPor = False 
               
        LogInfo("## Parse http post request ") 
    
        LEN=str(ComputeLengthHex(result_code_arrangement))
#   
        expectedPor = ByteToHex(LEN+ "\r\n")+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
               
        PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
        DataTobeRead = httpClient.DisplayRequest(PostRequest)      
        
        LogInfo("---- Check Data Response from Server Here ----")
        LogInfo(DataTobeRead)
        
        LogInfo("#Check Por")
        if(expectedPor != None):
            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
            pass



def HTTPAdminSessionProcess_OTAPA_Restart (dataRFM,output,expectedPOR=None):
    OTA_init()
    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000343/1002FF81FF208902000000"),
            ]

    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(Buffer.J.Get())

    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])

    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])

    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
    LogInfo('#------------------------------------------------------------------------------')
    
    LogInfo("##Receive HTTP Request from the card via BIP")
    output = bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000")
    LogInfo("##Send HTTP response to the card")

    LogInfo("====================Send Terminal Profile====================")
    Mobile.TerminalProfile(TP_All, expectedStatus="91xx")   
    
#    LogInfo("## Decipher http post request")
#    data = tlsServer.DecipherData(output[0])
#    if expectedPOR==None:
#        result_code_arrangement = "AF80"
#        for i in range (dataRFM.__len__()):
#            result_code_arrangement = result_code_arrangement + "23029000"
#        result_code_arrangement = result_code_arrangement + "0000"
#        chunkedExpectedPor = False  
#    else:
#        result_code_arrangement=expectedPOR
#        chunkedExpectedPor = False 
#               
#    LogInfo("## Parse http post request ") 
#    
#    LEN=str(ComputeLengthHex(result_code_arrangement))
##   
#    expectedPor = ByteToHex(LEN+ "\r\n")+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
#               
#    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
#    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
#        
#    LogInfo("---- Check Data Response from Server Here ----")
#    LogInfo(DataTobeRead)
#        
#    LogInfo("#Check Por")
#    if(expectedPor != None):
#            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
#            pass
        


def HTTPAdminSessionProcess_OTAPA_3 (dataRFM,output,expectedPOR=None):
    OTA_init()
    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo("##Construct buffer with C-apdus")
    Dll.OTA2.init_sms_payload_only(" ")
    for i in range (dataRFM.__len__()):
        Dll.OTA2.append_script("/INDEFINITE_LENGTH_EXPANDED" + dataRFM[i])
    Dll.OTA2.end_message(" I J " )
    Dll.OTA2.display_message(" J " )
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RFM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000343/1002FF81FF208902000000"),
            ]

    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(Buffer.J.Get())

    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])

    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])

    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")        
   
#    LogInfo('#------------------------------------------------------------------------------')
#    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
#    LogInfo('#------------------------------------------------------------------------------')
#    
#    LogInfo("##Receive HTTP Request from the card via BIP")

#    Mobile.PowerOff()
#    Mobile.PowerOn()
    LogInfo("====================Send Terminal Profile====================")
    Mobile.TerminalProfile(TP_All, expectedStatus="91xx")
    ###################################################   
        
def HTTPAdminSessionProcessRAM (StoreData,output,expectedPor=None,):
    OTA_init()     
    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)      
    
        
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    Mobile.Send("0020000008933F57845F706921")     
#        
    LogInfo ('-------------- HERE FOR DATA EXCHANGE is RAM MODEL ----------------------------')
    #Set Header
                

    LogInfo("-------- Initialisation Parameter ----------") 
#    headerTargetAppliValue = None 
    nextURI = "/server/adminagent?cmd=1"
    RW_APDU = StoreData
    headerNextURI = nextURI
    LogInfo("-------- Encapsulated RW_APDU Inside Stored Data using Expanded INDEFINITE Format ----------")  
    StoreDataAPDU="80E28100"+lv(RW_APDU)+"00"
#    StoreDataAPDU="8010000000"
##    
    array2 = []        
    array2.append(StoreDataAPDU)  
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RAM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000151/000000"),
            ]                                                                                          
    
    LogInfo("##Construct buffer with C-apdus [Put APDU into Buffer for processed in HTTP Mode on Next Step]")
        
        
    ##Set body in Expanded Infinite format
    LogInfo("## Construct C-apdus with expanded format")
    arrayBody = array2   
           
    LogInfo ("##Construct install for perso command ")
    ##To construct 80E62000 16 0000  10 A0000000770207601100010000002200 000000
    
#    body = "80E62000" + lv("0000"+ lv("A000000151000000") + "000000")\
    body = ["80E62000" + lv("0000"+ lv("A000000077020760110000FE0000D600") + "000000")]
#    body = ["80E62000" + lv("0000"+ lv("A00000007702076011101000000000D6") + "000000")]
    body = Utilities.AppendScript(body, "EXPANDED")
    
    for i in range(len(arrayBody)):
        body = body + Utilities.AppendScript(arrayBody[i], "EXPANDED")
    #    
    body = Utilities.EndMessage(body, "INDEFINITE")
        

        
        
    LogInfo("## Encapsulate APDUs in HTTP frames")
    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(body)
        
    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])
        
    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])
        
    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")
   
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
    LogInfo('#------------------------------------------------------------------------------')

    LogInfo("## Receive HTTP Request from the card via BIP")

    output = bipChannel.RcvDataFromCardViaBip(output, expectedStatus = OK)
                  
    LogInfo("## Decipher http post request")
    data = tlsServer.DecipherData(output[0])
        
               
    if expectedPor==None:
        result_code_arrangement = "AF80"
        for i in range (2):
            result_code_arrangement = result_code_arrangement + "23029000"
        result_code_arrangement = result_code_arrangement + "0000"
        chunkedExpectedPor = False  
    else:
        result_code_arrangement=expectedPor+ "0000"
        chunkedExpectedPor = False 
               
    LogInfo("## Parse http post request ") 
    
    LEN=str(ComputeLengthHex(result_code_arrangement))
    expectedPor ="450D0A"+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
               
    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
        
    LogInfo("---- Check Data Response from Server Here ----")
    LogInfo(DataTobeRead)
        
    LogInfo("#Check Por")
    if(expectedPor != None):
            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
            pass

def HTTPAdminSessionProcessRAMadd (StoreData,expectedPor=None):
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST RESPONSE SENT BY SERVER via Bip Channel [Data begin exchange below here]') 
    LogInfo('#------------------------------------------------------------------------------')
    Mobile.Send("0020000008933F57845F706921")     
#        
    LogInfo ('-------------- HERE FOR DATA EXCHANGE is RAM MODEL ----------------------------')
    #Set Header
                

    LogInfo("-------- Initialisation Parameter ----------") 
#    headerTargetAppliValue = None 
    nextURI = "/server/adminagent?cmd=1"
    RW_APDU = StoreData
    headerNextURI = nextURI
    LogInfo("-------- Encapsulated RW_APDU Inside Stored Data using Expanded INDEFINITE Format ----------")  
    StoreDataAPDU="80E28100"+lv(RW_APDU)+"00"
#    StoreDataAPDU="8010000000"
##    
    array2 = []        
    array2.append(StoreDataAPDU)  
         
    LogInfo("##Encapsulate APDUs in HTTP frames")
    list = [(CHANGE_VALUE, HEADER_CONTENT_TYPE, CONTENT_TYPE_RAM),
            (CHANGE_VALUE, HEADER_NEXT_URI, "/server/adminagent?cmd=1"),
            (CHANGE_VALUE, HEADER_TARGETED_APPLI, "//aid/A000000151/000000"),
            ]                                                                                          
    
    LogInfo("##Construct buffer with C-apdus [Put APDU into Buffer for processed in HTTP Mode on Next Step]")
        
        
    ##Set body in Expanded Infinite format
    LogInfo("## Construct C-apdus with expanded format")
    arrayBody = array2   
           
    LogInfo ("##Construct install for perso command ")
    ##To construct 80E62000 16 0000  10 A0000000770207601100010000002200 000000
    
#    body = "80E62000" + lv("0000"+ lv("A000000151000000") + "000000")\
    body = ["80E62000" + lv("0000"+ lv("A000000077020760110000FE0000D600") + "000000")]
#    body = ["80E62000" + lv("0000"+ lv("A00000007702076011101000000000D6") + "000000")]
    body = Utilities.AppendScript(body, "EXPANDED")
    
    for i in range(len(arrayBody)):
        body = body + Utilities.AppendScript(arrayBody[i], "EXPANDED")
    #    
    body = Utilities.EndMessage(body, "INDEFINITE")
        

        
        
    LogInfo("## Encapsulate APDUs in HTTP frames")
    httpserver.MakeHeader(httpmode,list)
    postresponse = httpserver.CreateResponse(body)
        
    LogInfo("##Display HTTP Response")
    httpserver.DisplayResponse(postresponse[0])
        
    LogInfo("##Cipher HTTP post Response")
    output = tlsServer.CipherData(postresponse[1])
        
    LogInfo("##Send HTTP response to the card")
    output = bipChannel.SendDataToCardViaBip (output, expectedStatus = "91XX")
   
    LogInfo('#------------------------------------------------------------------------------')
    LogInfo('# POST REQUEST SENT BY CARD via Bip Channel ')
    LogInfo('#------------------------------------------------------------------------------')

    LogInfo("## Receive HTTP Request from the card via BIP")

    output = bipChannel.RcvDataFromCardViaBip(output, expectedStatus = OK)
                  
    LogInfo("## Decipher http post request")
    data = tlsServer.DecipherData(output[0])
        
               
    if expectedPor==None:
        result_code_arrangement = "AF80"
        for i in range (2):
            result_code_arrangement = result_code_arrangement + "23029000"
        result_code_arrangement = result_code_arrangement + "0000"
        chunkedExpectedPor = False  
    else:
        result_code_arrangement=expectedPor+ "0000"
        chunkedExpectedPor = False 
               
    LogInfo("## Parse http post request ") 
    
    LEN=str(ComputeLengthHex(result_code_arrangement))
    expectedPor ="450D0A"+result_code_arrangement+ByteToHex("\r\n"+"0"+"\r\n\r\n")
               
    PostRequest = httpClient.ParseRequest(data,expectedRequest=None,chunked=True)
    DataTobeRead = httpClient.DisplayRequest(PostRequest)      
        
    LogInfo("---- Check Data Response from Server Here ----")
    LogInfo(DataTobeRead)
        
    LogInfo("#Check Por")
    if(expectedPor != None):
            httpClient.CheckPor(data, expectedPor, chunkedExpectedPor)
            pass
        




def HTTPAdminSessionProcessRFM2 (dataRFM,output,CSIM=None,NoAID=None,nosuccess=None):
      
    LogInfo('#generate ServerHello+ServerHelloDone')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip(ret, expectedStatus = "91XX")

    LogInfo('#Check clientKeyExchange+CipherSpec+clientFinished messages')
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = GetConst("OK"))
                
    LogInfo('#generate change cipherspec + serverfinished')
    LogInfo('Envelope Data Download + Receive Data Process + Send Terminal Response Receive Data')
    ret=tlsServer.ProcessHandshake(output[0])
    output= bipChannel.SendDataToCardViaBip (ret, expectedStatus = "91XX")
       
    LogInfo('#------------------------------------------------------')
    LogInfo('# FIRST POST REQUEST FROM CLIENT (CARD) via Bip Channel')
    LogInfo('#------------------------------------------------------')
        
    LogInfo("## First post request sent by the card")
    LogInfo('Send Data Process + Fetch + Send Terminal Response Send Data')
    output= bipChannel.RcvDataFromCardViaBip(output,expectedStatus = "9000" )
        
    LogInfo("## Decipher post request") 
    data = tlsServer.DecipherData(output[0])   
        
    LogInfo("## Parse First Post Request")   
    firstRequest = httpClient.ParseRequest(data)
    httpClient.DisplayRequest(firstRequest)  
    
