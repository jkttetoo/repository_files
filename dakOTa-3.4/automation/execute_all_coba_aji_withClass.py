from Ot.PyCom import *
import sys
from Oberthur import *
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "esim_tool_configuration.ini")
Constants.config.read(fpath)

from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model

import xmlrunner


__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'


EUICC 					= model.Euicc()
SERVER					= model.Smsr()
DEVICE 					= model.Device()
GSMA					= model.Gsma()
TL_SERVER			   	= model.TestlinkServer()
INGENICO				= model.IngenicoReader()
LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
GLONASS   				= model.EraGlonass()
HSM 					= model.HighStressMemory()
LOCAL_SWAP				= model.LocalSwap()

Constants.displayAPDU = True
displayAPDU(Constants.displayAPDU)
SetLogLevel('info')

#SetCurrentReader('OMNIKEY CardMan 3x21 0')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')


''' TESTLINK VARIABLES '''
Constants.TESTLINK_REPORT           = True
# Constants.TESTLINK_REPORT           = False
Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
Constants.TESTLINK_BUILD            = 'build_test_preintegration'
Constants.TESTLINK_PLATFORM         = 'IO222'
Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'


'''
TEST VARIABLES

To launch the test, prepare the following variables :

Constants.AID_ISDP 			: It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
Constants.DEFAULT_ISDP10	: It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
Constants.DEFAULT_ISDP11	: It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
Constants.DEFAULT_ISDP12	: It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

'''
Constants.AID_ISDP 					= 'A0000005591010FFFFFFFF8900001500'
Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'

Constants.DEFAULT_ISDP10			= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11			= 'A0000005591010FFFFFFFF8900001100'
Constants.DEFAULT_ISDP12			= 'A0000005591010FFFFFFFF8900001200'
Constants.DEFAULT_ISDP13			= 'A0000005591010FFFFFFFF8900001300'
Constants.DEFAULT_ISDP14			= 'A0000005591010FFFFFFFF8900001400'
Constants.DEFAULT_ISDP16 = "A0000005591010FFFFFFFF8900001600"
Constants.WRONG_ISDP				= 'A0000005591010FFFFFFFF8900001101'
Constants.SAIP_TEST 				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_HSM					= 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
Constants.SAIP_PCF00				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
#Constants.ITERATIONS_PERFORMANCE	= 5
#Constants.ITERATIONS_STRESS			= 100
Constants.ITERATIONS_PERFORMANCE	= 5
Constants.ITERATIONS_STRESS			= 100
Constants.RESULTS 					= {
										'test_name' 		: ['Test name'],
										'test_result'		: ['Test result']
										}


#EXECUTE_TEST_ISDR					= True
#EXECUTE_TEST_MNOSD					= False
#EXECUTE_TEST_PCF_RULE				= False
#EXECUTE_TEST_LM_APPLET				= False
#EXECUTE_TEST_PERFORMANCE			= False
#EXECUTE_TEST_STRESS					= False
#EXECUTE_TEST_DOWNLOAD_PROFILE		= True
#EXECUTE_TEST_ERA_GLONASS_APPLET		= False
#EXECUTE_TEST_HSM_APPLET				= False
#EXECUTE_TEST_LOCATION_PLUGIN_APPLET	= False


'''
PRE-CONDITION:

	1. Set fallback attribut on profile 11
	2. FRM status ON
	3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 12 state PERSONALIZED     --> Pre-created ISDP
4. Profile 13 state PERSONALIZED	 --> Pre-created ISDP
5. Profile 14 state PERSONALIZED	 --> Pre-created ISDP

=====================================

'''
Constants.case = 0
Constants.RunAll = True

#class eSIM_Test(unittest.TestCase):
class Preintegration_Test(unittest.TestCase):
# 	# tags = ['prastaji', 'ISDR', 'SCP80']
	def test_01_scp80_create_isdp_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_create_isdp_definite')
		assert retValue == 0
		pass

	def test_01_scp80_create_isdp_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_create_isdp_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_delete_profile_compact(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_delete_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_delete_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_disable_profile_compact(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_disable_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_disable_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_enable_profile_compact(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_enable_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_enable_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_list_profiles_compact(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_compact')
		assert retValue == 0
		pass

	def test_01_scp80_list_profiles_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_definite')
		assert retValue == 0
		pass

	def test_01_scp80_list_profiles_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_list_resources_compact(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_compact')
		assert retValue == 0
		pass

	def test_01_scp80_list_resources_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_definite')
		assert retValue == 0
		pass

	def test_01_scp80_list_resources_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_disabled_profile_compact(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_disabled_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_disabled_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_enable_profile_compact(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_enable_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_enable_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_update_smsr_compact(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_compact')
		assert retValue == 0
		pass

	def test_01_scp80_update_smsr_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_definite')
		assert retValue == 0
		pass

	def test_01_scp80_update_smsr_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_indefinite')
		assert retValue == 0
		pass

#	tags = ['prastaji']
	def test_01_scp81_create_isdp_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_create_isdp_definite')
		assert retValue == 0
		pass

	def test_01_scp81_create_isdp_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_create_isdp_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_delete_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_delete_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_delete_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_delete_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_disable_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_disable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_disable_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_disable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_download_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_download_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_download_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_download_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_enable_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_enable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_enable_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_enable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_list_profiles_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_profiles_definite')
		assert retValue == 0
		pass

	def test_01_scp81_list_profiles_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_profiles_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_list_resources_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_resources_definite')
		assert retValue == 0
		pass

	def test_01_scp81_list_resources_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_resources_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_set_fallback_disabled_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_disabled_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_set_fallback_disabled_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_disabled_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_set_fallback_enable_profile_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_enable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_set_fallback_enable_profile_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_enable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_update_smsr_definite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_update_smsr_definite')
		assert retValue == 0
		pass

	def test_01_scp81_update_smsr_indefinite(self):
		retValue = Run_ModuleMain('test_01_isdr.scp81_update_smsr_indefinite')
		assert retValue == 0
		pass

	def test_02_scp80_install_applet_compact(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_compact')
		assert retValue == 0
		pass

	def test_02_scp80_install_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp80_install_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp80_list_mno_applet_compact(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_compact')
		assert retValue == 0
		pass

	def test_02_scp80_list_mno_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp80_list_mno_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp80_list_mnosd_compact(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_compact')
		assert retValue == 0
		pass

	def test_02_scp80_list_mnosd_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_definite')
		assert retValue == 0
		pass

	def test_02_scp80_list_mnosd_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_indefinite')
		assert retValue == 0
		pass

	def test_02_scp80_lock_applet_compact(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_compact')
		assert retValue == 0
		pass

	def test_02_scp80_lock_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp80_lock_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp80_uninstall_applet_compact(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_compact')
		assert retValue == 0
		pass

	def test_02_scp80_uninstall_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp80_uninstall_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp80_unlock_applet_compact(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_compact')
		assert retValue == 0
		pass

	def test_02_scp80_unlock_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp80_unlock_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp80_update_2g_spn_compact(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_compact')
		assert retValue == 0
		pass

	def test_02_scp80_update_2g_spn_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_definite')
		assert retValue == 0
		pass

	def test_02_scp80_update_2g_spn_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_indefinite')
		assert retValue == 0
		pass

	def test_02_scp80_update_3g_adn_compact(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_compact')
		assert retValue == 0
		pass

	def test_02_scp80_update_3g_adn_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_definite')
		assert retValue == 0
		pass

	def test_02_scp80_update_3g_adn_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_indefinite')
		assert retValue == 0
		pass

	def test_02_scp81_install_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_install_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp81_install_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_install_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp81_list_mno_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mno_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp81_list_mno_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mno_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp81_list_mnosd_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mnosd_definite')
		assert retValue == 0
		pass

	def test_02_scp81_list_mnosd_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mnosd_indefinite')
		assert retValue == 0
		pass

	def test_02_scp81_lock_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_lock_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp81_lock_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_lock_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp81_uninstall_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_uninstall_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp81_uninstall_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_uninstall_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp81_unlock_applet_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_unlock_applet_definite')
		assert retValue == 0
		pass

	def test_02_scp81_unlock_applet_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_unlock_applet_indefinite')
		assert retValue == 0
		pass

	def test_02_scp81_update_3g_adn_definite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_update_3g_adn_definite')
		assert retValue == 0
		pass

	def test_02_scp81_update_3g_adn_indefinite(self):
		retValue = Run_ModuleMain('test_02_mnosd.scp81_update_3g_adn_indefinite')
		assert retValue == 0
		pass

	tags=["prastaji", "pcf"]
	def test_03_pcf00_delete_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_delete_profile')
		assert retValue == 0
		pass

	def test_03_pcf00_disable_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_disable_profile')
		assert retValue == 0
		pass

	def test_03_pcf00_enable_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_enable_profile')
		assert retValue == 0
		pass

	def test_03_pcf00_network_attachment_loss(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_network_attachment_loss')
		assert retValue == 0
		pass

	def test_03_pcf01_delete_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_delete_profile')
		assert retValue == 0
		pass

	def test_03_pcf01_disable_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_disable_profile')
		assert retValue == 0
		pass

	def test_03_pcf01_enable_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_enable_profile')
		assert retValue == 0
		pass

	def test_03_pcf01_network_attachment_loss(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_network_attachment_loss')
		assert retValue == 0
		pass

	def test_03_pcf02_delete_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_delete_profile')
		assert retValue == 0
		pass

	def test_03_pcf02_disable_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_disable_profile')
		assert retValue == 0
		pass

	def test_03_pcf02_enable_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_enable_profile')
		assert retValue == 0
		pass

	def test_03_pcf02_network_attachment_loss(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_network_attachment_loss')
		assert retValue == 0
		pass

	def test_03_pcf04_delete_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_delete_profile')
		assert retValue == 0
		pass

	def test_03_pcf04_disable_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_disable_profile')
		assert retValue == 0
		pass

	def test_03_pcf04_enable_profile(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_enable_profile')
		assert retValue == 0
		pass

	def test_03_pcf04_network_attachment_loss(self):
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_network_attachment_loss')
		assert retValue == 0
		pass

#	tags = ["prastaji", "lm_applet"]
	def test_04_lm_applet_allow_delete_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_delete_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_allow_disable_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_disable_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_allow_enable_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_enable_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_delete_profile_with_notif(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_profile_with_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_delete_profile_without_notif(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_profile_without_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_delete_the_enabled_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_the_enabled_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_disable_notif(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_disable_profile_with_notif(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_profile_with_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_disable_profile_without_notif(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_profile_without_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_disable_the_disabled_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_the_disabled_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_disallow_delete_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_delete_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_disallow_disable_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_disable_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_disallow_enable_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_enable_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_enable_notif(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_enable_profile_with_notif(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_profile_with_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_enable_profile_without_notif(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_profile_without_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_enable_the_enabled_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_the_enabled_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_get_applet_config(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_applet_config')
		assert retValue == 0
		pass

	def test_04_lm_applet_get_eid(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_eid')
		assert retValue == 0
		pass

	def test_04_lm_applet_get_euicc_info(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_euicc_info')
		assert retValue == 0
		pass

	def test_04_lm_applet_set_fallback_disabled_profile(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_set_fallback_disabled_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_set_fallback(self):
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_set_fallback')
		assert retValue == 0
		pass

	def test_05_performance_scp80_create_isdp(self):
		retValue = Run_ModuleMain('test_05_performance.scp80_create_isdp')
		assert retValue == 0
		pass

	def test_05_performance_scp80_delete_profile(self):
		retValue = Run_ModuleMain('test_05_performance.scp80_delete_profile')
		assert retValue == 0
		pass

	def test_05_performance_scp80_disable_profile_http_notif(self):
		retValue = Run_ModuleMain('test_05_performance.scp80_disable_profile_http_notif')
		assert retValue == 0
		pass

	def test_05_performance_scp80_enable_profile_http_notif(self):
		retValue = Run_ModuleMain('test_05_performance.scp80_enable_profile_http_notif')
		assert retValue == 0
		pass

	def test_05_performance_scp80_list_profiles(self):
		retValue = Run_ModuleMain('test_05_performance.scp80_list_profiles')
		assert retValue == 0
		pass

	def test_05_performance_scp80_list_resources(self):
		retValue = Run_ModuleMain('test_05_performance.scp80_list_resources')
		assert retValue == 0
		pass

	def test_05_performance_scp80_set_fallback_profile(self):
		retValue = Run_ModuleMain('test_05_performance.scp80_set_fallback_profile')
		assert retValue == 0
		pass

	def test_05_performance_scp81_create_isdp(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_create_isdp')
		assert retValue == 0
		pass

	def test_05_performance_scp81_delete_profile(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_delete_profile')
		assert retValue == 0
		pass

	def test_05_performance_scp81_disable_profile_http_notif(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_disable_profile_http_notif')
		assert retValue == 0
		pass

	def test_05_performance_scp81_download_profile_138K(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_138K')
		assert retValue == 0
		pass

	def test_05_performance_scp81_download_profile_74K(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_74K')
		assert retValue == 0
		pass



	def test_05_performance_scp81_download_profile_9k(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_9k')
		assert retValue == 0
		pass

	def test_05_performance_scp81_enable_profile_http_notif(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_enable_profile_http_notif')
		assert retValue == 0
		pass

	def test_05_performance_scp81_list_profiles(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_list_profiles')
		assert retValue == 0
		pass

	def test_05_performance_scp81_list_resources(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_list_resources')
		assert retValue == 0
		pass

	def test_05_performance_scp81_set_fallback_profile(self):
		retValue = Run_ModuleMain('test_05_performance.scp81_set_fallback_profile')
		assert retValue == 0
		pass

	def test_06_stress_download_delete_status(self):
		retValue = Run_ModuleMain('test_06_stress.download_delete_status')
		assert retValue == 0
		pass

	def test_06_stress_download_delete_without_status(self):
		retValue = Run_ModuleMain('test_06_stress.download_delete_without_status')
		assert retValue == 0
		pass

	def test_06_stress_enable_profile_disable_profile(self):
		retValue = Run_ModuleMain('test_06_stress.enable_profile_disable_profile')
		assert retValue == 0
		pass

	def test_06_stress_enable_profile_fallback_network_loss(self):
		retValue = Run_ModuleMain('test_06_stress.enable_profile_fallback_network_loss')
		assert retValue == 0
		pass

	def test_06_stress_enable_rollback_network_loss_disable_profile(self):
		retValue = Run_ModuleMain('test_06_stress.enable_rollback_network_loss_disable_profile')
		assert retValue == 0
		pass

	def test_07_download_profile_comparison(self):
		retValue = Run_ModuleMain('test_07_download_profile.download_profile_comparison')
		assert retValue == 0
		pass

	def test_08_nom_neg_001_profile_activation_failed_due_to_activated(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_001_profile_activation_failed_due_to_activated')
		assert retValue == 0
		pass

	def test_08_nom_neg_002_profile_deactivation_failed_due_to_disabled_state(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_002_profile_deactivation_failed_due_to_disabled_state')
		assert retValue == 0
		pass

	def test_08_nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated')
		assert retValue == 0
		pass

	def test_08_nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state')
		assert retValue == 0
		pass

	def test_08_nom_neg_005_set_profile_failure_cause(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_005_set_profile_failure_cause')
		assert retValue == 0
		pass

	def test_08_nom_pos_001_profile_activation_via_applet_selection_without_notif(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_001_profile_activation_via_applet_selection_without_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_002_profile_deactivation_via_applet_selection_without_notif(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_002_profile_deactivation_via_applet_selection_without_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_003_profile_activation_via_process_toolkit_without_notif(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_003_profile_activation_via_process_toolkit_without_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_004_profile_deactivation_via_process_toolkit_without_notif(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_004_profile_deactivation_via_process_toolkit_without_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_005_lock_unlock_mconnect_command(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_005_lock_unlock_mconnect_command')
		assert retValue == 0
		pass

	def test_08_nom_pos_006_get_glonass_profile(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_006_get_glonass_profile')
		assert retValue == 0
		pass

	def test_08_nom_pos_007_profile_activation_via_applet_selection_with_notif(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_007_profile_activation_via_applet_selection_with_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_008_profile_deactivation_via_applet_selection_with_notif(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_008_profile_deactivation_via_applet_selection_with_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_009_profile_activation_via_process_toolkit_with_notif(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_009_profile_activation_via_process_toolkit_with_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_010_profile_deactivation_via_process_toolkit_with_notif(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_010_profile_deactivation_via_process_toolkit_with_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_011_profile_activation_via_local_management_applet(self):
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_011_profile_activation_via_local_management_applet')
		assert retValue == 0
		pass

	def test_09_audit_hsm_files(self):
		retValue = Run_ModuleMain('test_09_applet_hsm.audit_hsm_files')
		assert retValue == 0
		pass

	def test_10_nom_pos_001_update_applet_configuration_via_applet_selection(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_001_update_applet_configuration_via_applet_selection')
		assert retValue == 0
		pass

	def test_10_nom_pos_002_update_applet_configuration_via_update_smsr(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_002_update_applet_configuration_via_update_smsr')
		assert retValue == 0
		pass

	def test_10_nom_pos_003_applet_triggered_by_mcc_change(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_003_applet_triggered_by_mcc_change')
		assert retValue == 0
		pass

	def test_10_nom_pos_004_applet_triggered_by_power_on(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_004_applet_triggered_by_power_on')
		assert retValue == 0
		pass

	def test_10_nom_pos_005_applet_triggered_by_mnc_change(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_005_applet_triggered_by_mnc_change')
		assert retValue == 0
		pass

	def test_10_nom_pos_006_activate_location_plugin_notification_via_ota(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_006_activate_location_plugin_notification_via_ota')
		assert retValue == 0
		pass

	def test_10_nom_pos_007_deactivate_location_plugin_notification_via_ota(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_007_deactivate_location_plugin_notification_via_ota')
		assert retValue == 0
		pass

	def test_10_nom_pos_008_activate_location_plugin_notification_via_applet(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_008_activate_location_plugin_notification_via_applet')
		assert retValue == 0
		pass

	def test_10_nom_pos_009_deactivate_location_plugin_notification_via_applet(self):
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_009_deactivate_location_plugin_notification_via_applet')
		assert retValue == 0
		pass


## '''Sprint 4 new test script'''


# 	def test_11_USAT_Pairing_check_authentication_on_USAT_Pairing_and_SAIP_Profile(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_authentication_on_USAT_Pairing_and_SAIP_Profile')
# 		assert retValue == 0
# 		pass

# 	def test_11_USAT_Pairing_check_sending_IMEI_not_registered_on_EF_IWL(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_sending_IMEI_not_registered_on_EF_IWL')
# 		assert retValue == 0
# 		pass

# 	def test_11_USAT_Pairing_check_sending_IMEI_registered_on_EF_IWL(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_sending_IMEI_registered_on_EF_IWL')
# 		assert retValue == 0
# 		pass

# 	def test_11_USAT_Pairing_check_sending_IMEI_SV_not_registered_on_EF_IWL(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_sending_IMEI_SV_not_registered_on_EF_IWL')
# 		assert retValue == 0
# 		pass

# 	def test_11_USAT_Pairing_check_sending_IMEI_SV_registered_on_EF_IWL(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_sending_IMEI_SV_registered_on_EF_IWL')
# 		assert retValue == 0
# 		pass

# 	def test_11_USAT_Pairing_check_sending_TR_10(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_sending_TR_10')
# 		assert retValue == 0
# 		pass

# 	def test_11_USAT_Pairing_check_sending_TR_11(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_sending_TR_11')
# 		assert retValue == 0
# 		pass

# 	def test_11_USAT_Pairing_check_sending_TR_20_try_less_than_3_times(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_sending_TR_20_try_less_than_3_times')
# 		assert retValue == 0
# 		pass

# 	def test_11_USAT_Pairing_check_sending_TR_20_try_more_than_3_times(self):
# 		retValue = Run_ModuleMain('test_11_usat_pairing.USAT_Pairing_check_sending_TR_20_try_more_than_3_times')
# 		assert retValue == 0
#


	def test_12_skt_nom_neg_001_fallback_prohibited_when_switch_factory_profile_active(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_neg_001_fallback_prohibited_when_switch_factory_profile_active')
		assert retValue == 0
		pass

	def test_12_skt_nom_neg_002_rollback_prohibited_when_switch_factory_profile_active(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_neg_002_rollback_prohibited_when_switch_factory_profile_active')
		assert retValue == 0
		pass

	def test_12_skt_nom_neg_003_fallback_prohibited_when_switch_skt_profile_active(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_neg_003_fallback_prohibited_when_switch_skt_profile_active')
		assert retValue == 0
		pass

	def test_12_skt_nom_neg_004_rollback_prohibited_when_switch_skt_profile_active(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_neg_004_rollback_prohibited_when_switch_skt_profile_active')
		assert retValue == 0
		pass

	def test_12_skt_nom_neg_005_factory_profile_cant_be_enabled_via_gsma(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_neg_005_factory_profile_cant_be_enabled_via_gsma')
		assert retValue == 0
		pass

	def test_12_skt_nom_pos_001_set_factory_profile_from_enabled_ISDR_Profile_11(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_pos_001_set_factory_profile_from_enabled_ISDR_Profile_11')
		assert retValue == 0
		pass

	def test_12_skt_nom_pos_002_set_factory_profile_from_disabled_ISDR_Profile_11(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_pos_002_set_factory_profile_from_disabled_ISDR_Profile_11')
		assert retValue == 0
		pass

	def test_12_skt_nom_pos_003_change_to_factory_profile(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_pos_003_change_to_factory_profile')
		assert retValue == 0
		pass

	def test_12_skt_nom_pos_004_change_to_skt_profile(self):
		retValue = Run_ModuleMain('test_12_applet_local_swap.nom_pos_004_change_to_skt_profile')
		assert retValue == 0
		pass


if __name__ == '__main__':
    unittest.main(testRunner = xmlrunner.XMLTestRunner(output='C:\\ProjJenkin\\PreIntegration\\test-reports', verbosity=2))
