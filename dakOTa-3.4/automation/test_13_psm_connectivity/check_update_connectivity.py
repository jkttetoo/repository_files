from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time



__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
			displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_SCP80 = model.SCP80()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_SCP80 = model.SCP80()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_for_test'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4303'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

# smscAddrs = "8606 911011223344"
smscAddrs = "8606 110502100319"
# pid = "810111"
# dcs = "8201F6"
pid = "810148"
dcs = "820148"
dataUpdateCon ="3A07" + berLv("A0" + berLv(smscAddrs + pid + dcs))
# dataUpdateCon ="3A07" + berLv("A0" + berLv(pid + dcs))
installPersoDefISDP = "80 E6 20 00" + lv("0000"+lv(Constants.AID_ISDP)+"000000 00")
apduUpdate = [installPersoDefISDP ,"80 E2 88 00 " +berLv(dataUpdateCon)]

exp_rc_cc_ds = " XX XX XX XX XX XX XX XX "
expDCS_PID = "48"
# expSMSCAddrs = "911011223344"
expSMSCAddrs = "110502100319"

expEnvDataCon = " D0 69 81 03 01 13 00 82 02 81 83 86 06" + expSMSCAddrs + "0B" +\
				"56 41 01 0D 91 13 79 00 34 91 74 F1 "+\
				expDCS_PID + expDCS_PID + "48 02 70 00 00 43 15 02 00 00 12 00 00 01 "+\
				"00 00 00 00 00 00" + exp_rc_cc_ds + "E1 2B "+\
				"4C 10 63 64 16 03 10 00 00 94 33 7F 00 00 00 00 "+\
				"00 04 4D 01 02 4E 02 00 XX 2F 10 A0 00 00 05 59 "+\
				"10 10 FF FF FF FF 89 00 00 15"


expTestResult = " D0698103011300820281838606110502"+\
                      "1003190B5641010D91137900349174F1"+\
                      "4848"
class CheckUpdateConnectivity(OtTestUnit):

	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		pprint.h1('Check Update Connectivity Test Script')
		print("dataUpdateCon: ", dataUpdateCon)
		print("apduUpdate: ", apduUpdate)
		# StepOn()
		EUICC.init()
		pprint.h2("Create ISDP 15")
		SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
		pprint.h2("Download Profile")
		SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
		pprint.h2("Enable ISDP 15")
		capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
		if '9000' in rapdu:
			pprint.h2("Refresh ISDP 15")
			sw = SERVER.euicc.refresh(sw, not bool(1))

	def test_check_update_connectivity(self):
		LogInfo('body part')
		timer_start = time()
		try:
			pprint.h2("Update Connectivity Parameters")
			command_script, por, sw = EUICC.mno.ram(apduUpdate, scp=80)
			print("command_script: ", command_script)
			print("por: ", por)
			print("sw: ", sw)

			# StepOn()

			pprint.h2("Enable ISDP 11")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, False)

			pprint.h2("Enable ISDP 15")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				pprint.h2("Refresh")
				print("capdu: ",capdu)
				print("rapdu: ",rapdu)
				print("sw: ",sw)

				if sw is not None:
					DEVICE.fetch_one(sw, 'refresh')

				LogInfo('>>> fetch refresh \n')
				EUICC.init()
				pprint.h3("send envelope location status")
				data, sw = Envelope ("D615190103820282811B0100130903022789081614406F", expectedStatus = "91XX")
				print("data env: ", data)
				print("sw env: ", sw )
				LogInfo('envelope location status sw: {}'.format(sw))
				pprint.h3("Notification SMS")
				data = Fetch(sw[2:], expectedData = expEnvDataCon , expectedStatus = "9000")
				dataTr = TerminalResponse("81 03 01 13 00 02 02 82 81 83 01 00", expectedStatus = "91XX")
				# data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
				print(data[0][:32])
				if (data[0][:32] == expTestResult):
					print("TEST PASSED")
					Constants.RESULTS['test_name'].append('PSM Check Update Connectivity')
					Constants.RESULTS['test_result'].append('OK')
					EXECUTION_STATUS = 'PASSED'
					Constants.case+=1
				else:
					Constants.RESULTS['test_name'].append('PSM Check Update Connectivity')
					Constants.RESULTS['test_result'].append('KO')
					EXECUTION_STATUS = 'FAILED'
					Constants.case+=1

				# StepOn()
				print("data: ", data[0])
				# StepOn()
				seq_number = pprint.pprint_sms_first_notif(data[0])
				print("seq_number: ", seq_number)
				# data, sw, cmd_type = DEVICE.fetch_all(sw)
				data, sw, cmd_type = DEVICE.fetch_all(dataTr)
				print("data: ", data)
				print("sw: ", sw)
				print("cmd_type: ",cmd_type)
				pprint.h2('server confirms reception of euicc notification')
				command_script, por, sw = EUICC.mno.ram(GSMA.handle_notification_confirmation(seq_number), scp=80)
		except:
			pprint.h2("exception")
			Constants.RESULTS['test_name'].append('PSM Check Update Connectivity')
			Constants.RESULTS['test_result'].append('KO')
			EXECUTION_STATUS = 'FAILED'
			Constants.case+=1
		timer_end = time()
		EXECUTION_DURATION = timer_end - timer_start
#        self.assertEqual(Constants.RESULTS['test_result'][Constants.case], 'OK')
		# LogInfo('Constants.RESULTS table')
		# pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')
		# if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

    def tearDown(self):
        EUICC.init()
		pprint.h3("tearDown")
		SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
		SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
		DEVICE.get_status()

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(CheckUpdateConnectivity)