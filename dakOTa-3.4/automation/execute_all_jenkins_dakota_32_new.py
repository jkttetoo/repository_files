from Ot.PyCom import *
import sys
from Oberthur import *
import os
import pytest
import webbrowser
import smtplib
from email.mime.text import MIMEText

cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)

import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "esim_tool_configuration.ini")
Constants.config.read(fpath)

from Mobile import *
from util import *
from time import time
import unittest
import model

__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'


EUICC 					= model.Euicc()
SERVER					= model.Smsr()
DEVICE 					= model.Device()
GSMA					= model.Gsma()
TL_SERVER			   	= model.TestlinkServer()
INGENICO				= model.IngenicoReader()
LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
GLONASS   				= model.EraGlonass()
HSM 					= model.HighStressMemory()
LOCAL_SWAP				= model.LocalSwap()
AUTO_SERVICE = model.AutoService()

''' TESTLINK VARIABLES '''
Constants.TESTLINK_REPORT           = True
# Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
# Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME     = 'DakOTa 3.x'
Constants.TESTLINK_BUILD            = 'dk32_sprint19_release_22112019'
Constants.TESTLINK_PLATFORM         = 'IO222'
Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.2'
Constants.TESTLINK_TESTER           = 'muhamfir'

'''
TEST VARIABLES

To launch the test, prepare the following variables :

Constants.AID_ISDP 			: It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
Constants.DEFAULT_ISDP10	: It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
Constants.DEFAULT_ISDP11	: It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
Constants.DEFAULT_ISDP12	: It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

'''
Constants.AID_ISDR = "A0000005591010FFFFFFFF89000010"
Constants.AID_ISDP 					= 'A0000005591010FFFFFFFF8900001500'
Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'

Constants.AID_ISDP17				= 'A0000005591010FFFFFFFF8900001700'
Constants.AID_ISDP18				= 'A0000005591010FFFFFFFF8900001800'

Constants.DEFAULT_ISDP10			= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11			= 'A0000005591010FFFFFFFF8900001100'
Constants.DEFAULT_ISDP12			= 'A0000005591010FFFFFFFF8900001200'
Constants.DEFAULT_ISDP13			= 'A0000005591010FFFFFFFF8900001300'
Constants.DEFAULT_ISDP14			= 'A0000005591010FFFFFFFF8900001400'
Constants.DEFAULT_ISDP16 = "A0000005591010FFFFFFFF8900001600"
Constants.WRONG_ISDP				= 'A0000005591010FFFFFFFF8900001101'
Constants.SAIP_TEST 				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_HSM					= 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
Constants.SAIP_PCF00				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
Constants.SAIP_E2E 					= 'HTTPs_Aspider_SAIPv2.1_Dkt3.x_Elisa_new_re.der'
Constants.ITERATIONS_PERFORMANCE	= 10
Constants.ITERATIONS_STRESS			= 20
Constants.RESULTS 					= {
										'test_name' 		: ['Test name'],
										'test_result'		: ['Test result']
										}

'''
PRE-CONDITION:

	1. Set fallback attribut on profile 11
	2. FRM status ON
	3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 12 state PERSONALIZED     --> Pre-created ISDP
4. Profile 13 state PERSONALIZED	 --> Pre-created ISDP
5. Profile 14 state PERSONALIZED	 --> Pre-created ISDP
4
=====================================

'''
Constants.case = 0
Constants.RunAll = True
Constants.testLinkTestCase = []
Constants.testLinkCountRunAll = 0
Constants.debugMode = False
Constants.token_aes_key = ""

# if Constants.TESTLINK_REPORT == False:
# 	Constants.displayAPDU = True
# else:
# 	Constants.displayAPDU = False

Constants.displayAPDU = True
displayAPDU(Constants.displayAPDU)
SetLogLevel('info')

import Ot.GlobalPlatform as GP
from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain, IssuerSecurityDomain, AESKeyset

import automation.test_01_isdr as test_01_isdr
import automation.test_02_mnosd as test_02_mnosd
import automation.test_03_pcf_rule as test_03_pcf_rule
import automation.test_04_lm_applet as test_04_lm_applet
import automation.test_05_performance as test_05_performance
import automation.test_06_stress as test_06_stress
import automation.test_07_download_profile as test_07_download_profile
import automation.test_08_applet_era_glonass as test_08_applet_era_glonass
import automation.test_09_applet_hsm as test_09_applet_hsm
import automation.test_10_applet_location_plugin as test_10_applet_location_plugin
import automation.test_11_sprint_2 as test_11_sprint_2
import automation.test_11_sprint_3 as test_11_sprint_3
import automation.test_12_sprint_4 as test_12_sprint_4
import automation.test_13_sprint_5 as test_13_sprint_5
#sprint 6 NOT created test script
import automation.test_15_sprint_7 as test_15_sprint_7
#sprint 8 NOT created test script
import automation.test_17_sprint_9 as test_17_sprint_9
#sprint 10 NOT created test script
import automation.test_19_sprint_11 as test_19_sprint_11
import automation.test_20_sprint_12 as test_19_sprint_12
#sprint 13 NOT created test script
#sprint 14 NOT created test script
#sprint 15 NOT created test script
#sprint 16 NOT created test script
#sprint 17 NOT created test script
import automation.test_26_sprint_18 as test_26_sprint_18
import automation.test_27_phase_1 as test_27_phase_1
# import automation.test_28_phase_2 as test_28_phase_2
import automation.test_98_euicc_features as test_98_euicc_features
import automation.test_99_dako_issue as test_99_dako_issue
import automation.test_99_diot_issues as test_99_diot_issues

'''
test_08, test_10 dicomment di sprint 12 sampe nanti ntah kapan...
'''

class Preintegration_Test(unittest.TestCase):
	# tags = ['prastaji', 'ISDR', 'SCP80']
	def test_01_scp80_create_isdp_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_create_isdp_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_create_isdp_definite')
		assert retValue == 0
		

	def test_01_scp80_create_isdp_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_create_isdp_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_create_isdp_indefinite')
		assert retValue == 0
		

	def test_01_scp80_delete_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_delete_profile_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_compact')
		assert retValue == 0
		

	def test_01_scp80_delete_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_delete_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_definite')
		assert retValue == 0
		

	def test_01_scp80_delete_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_delete_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp80_disable_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_disable_profile_compact")
		
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_compact')
		
		assert retValue == 0
		

	def test_01_scp80_disable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_disable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_definite')
		assert retValue == 0
		

	def test_01_scp80_disable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_disable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp80_enable_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_enable_profile_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_compact')
		assert retValue == 0
		

	def test_01_scp80_enable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_enable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_definite')
		assert retValue == 0
		

	def test_01_scp80_enable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_enable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp80_list_profiles_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_profiles_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_compact')
		assert retValue == 0
		

	def test_01_scp80_list_profiles_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_profiles_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_definite')
		assert retValue == 0
		

	def test_01_scp80_list_profiles_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_profiles_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_indefinite')
		assert retValue == 0
		

	def test_01_scp80_list_resources_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_resources_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_compact')
		assert retValue == 0
		

	def test_01_scp80_list_resources_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_resources_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_definite')
		assert retValue == 0
		

	def test_01_scp80_list_resources_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_resources_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_indefinite')
		assert retValue == 0
		

	def test_01_scp80_set_fallback_disabled_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_disabled_profile_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_compact')
		assert retValue == 0
		

	def test_01_scp80_set_fallback_disabled_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_disabled_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_definite')
		assert retValue == 0
		

	def test_01_scp80_set_fallback_disabled_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_disabled_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp80_set_fallback_enable_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_enable_profile_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_compact')
		assert retValue == 0
		

	def test_01_scp80_set_fallback_enable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_enable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_definite')
		assert retValue == 0
		

	def test_01_scp80_set_fallback_enable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_enable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp80_update_smsr_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_update_smsr_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_compact')
		assert retValue == 0
		

	def test_01_scp80_update_smsr_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_update_smsr_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_definite')
		assert retValue == 0
		

	def test_01_scp80_update_smsr_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_update_smsr_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_indefinite')
		assert retValue == 0
		

#	tags = ['prastaji']
	def test_01_scp81_create_isdp_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_create_isdp_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_create_isdp_definite')
		assert retValue == 0
		

	def test_01_scp81_create_isdp_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_create_isdp_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_create_isdp_indefinite')
		assert retValue == 0
		

	def test_01_scp81_delete_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_delete_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_delete_profile_definite')
		assert retValue == 0
		

	def test_01_scp81_delete_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_delete_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_delete_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp81_disable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_disable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_disable_profile_definite')
		assert retValue == 0
		

	def test_01_scp81_disable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_disable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_disable_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp81_download_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_download_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_download_profile_definite')
		assert retValue == 0
		

	def test_01_scp81_download_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_download_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_download_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp81_enable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_enable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_enable_profile_definite')
		assert retValue == 0
		

	def test_01_scp81_enable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_enable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_enable_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp81_list_profiles_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_list_profiles_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_profiles_definite')
		assert retValue == 0
		

	def test_01_scp81_list_profiles_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_list_profiles_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_profiles_indefinite')
		assert retValue == 0
		

	def test_01_scp81_list_resources_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_list_resources_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_resources_definite')
		assert retValue == 0
		

	def test_01_scp81_list_resources_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_list_resources_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_resources_indefinite')
		assert retValue == 0
		

	def test_01_scp81_set_fallback_disabled_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_set_fallback_disabled_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_disabled_profile_definite')
		assert retValue == 0
		

	def test_01_scp81_set_fallback_disabled_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_set_fallback_disabled_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_disabled_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp81_set_fallback_enable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_set_fallback_enable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_enable_profile_definite')
		assert retValue == 0
		

	def test_01_scp81_set_fallback_enable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_set_fallback_enable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_enable_profile_indefinite')
		assert retValue == 0
		

	def test_01_scp81_update_smsr_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_update_smsr_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_update_smsr_definite')
		assert retValue == 0
		

	def test_01_scp81_update_smsr_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_update_smsr_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_update_smsr_indefinite')
		assert retValue == 0
		

	def test_02_scp80_install_applet_compact(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_install_applet_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_compact')
		assert retValue == 0
		

	def test_02_scp80_install_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_install_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_definite')
		assert retValue == 0
		

	def test_02_scp80_install_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_install_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp80_list_mno_applet_compact(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mno_applet_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_compact')
		assert retValue == 0
		

	def test_02_scp80_list_mno_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mno_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_definite')
		assert retValue == 0
		

	def test_02_scp80_list_mno_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mno_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp80_list_mnosd_compact(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mnosd_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_compact')
		assert retValue == 0
		

	def test_02_scp80_list_mnosd_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mnosd_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_definite')
		assert retValue == 0
		

	def test_02_scp80_list_mnosd_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mnosd_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_indefinite')
		assert retValue == 0
		

	def test_02_scp80_lock_applet_compact(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_lock_applet_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_compact')
		assert retValue == 0
		

	def test_02_scp80_lock_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_lock_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_definite')
		assert retValue == 0
		

	def test_02_scp80_lock_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_lock_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp80_uninstall_applet_compact(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_uninstall_applet_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_compact')
		assert retValue == 0
		

	def test_02_scp80_uninstall_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_uninstall_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_definite')
		assert retValue == 0
		

	def test_02_scp80_uninstall_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_uninstall_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp80_unlock_applet_compact(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_unlock_applet_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_compact')
		assert retValue == 0
		

	def test_02_scp80_unlock_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_unlock_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_definite')
		assert retValue == 0
		

	def test_02_scp80_unlock_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_unlock_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp80_update_2g_spn_compact(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_2g_spn_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_compact')
		assert retValue == 0
		

	def test_02_scp80_update_2g_spn_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_2g_spn_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_definite')
		assert retValue == 0
		

	def test_02_scp80_update_2g_spn_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_2g_spn_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_indefinite')
		assert retValue == 0
		

	def test_02_scp80_update_3g_adn_compact(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_3g_adn_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_compact')
		assert retValue == 0
		

	def test_02_scp80_update_3g_adn_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_3g_adn_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_definite')
		assert retValue == 0
		

	def test_02_scp80_update_3g_adn_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_3g_adn_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_indefinite')
		assert retValue == 0
		

	def test_02_scp81_install_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_install_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_install_applet_definite')
		assert retValue == 0
		

	def test_02_scp81_install_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_install_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_install_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp81_list_mno_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_list_mno_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mno_applet_definite')
		assert retValue == 0
		

	def test_02_scp81_list_mno_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_list_mno_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mno_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp81_list_mnosd_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_list_mnosd_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mnosd_definite')
		assert retValue == 0
		

	def test_02_scp81_list_mnosd_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_list_mnosd_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mnosd_indefinite')
		assert retValue == 0
		

	def test_02_scp81_lock_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_lock_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_lock_applet_definite')
		assert retValue == 0
		

	def test_02_scp81_lock_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_lock_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_lock_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp81_uninstall_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_uninstall_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_uninstall_applet_definite')
		assert retValue == 0
		

	def test_02_scp81_uninstall_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_uninstall_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_uninstall_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp81_unlock_applet_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_unlock_applet_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_unlock_applet_definite')
		assert retValue == 0
		

	def test_02_scp81_unlock_applet_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_unlock_applet_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_unlock_applet_indefinite')
		assert retValue == 0
		

	def test_02_scp81_update_3g_adn_definite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_update_3g_adn_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_update_3g_adn_definite')
		assert retValue == 0
		

	def test_02_scp81_update_3g_adn_indefinite(self):
		Constants.testLinkTestCase.append("test_02_mnosd.scp81_update_3g_adn_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_02_mnosd.scp81_update_3g_adn_indefinite')
		assert retValue == 0
		

# 	tags=["prastaji", "pcf"]
	def test_03_pcf00_delete_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf00_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_delete_profile')
		assert retValue == 0
		

	def test_03_pcf00_disable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf00_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_disable_profile')
		assert retValue == 0
		

	def test_03_pcf00_enable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf00_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_enable_profile')
		assert retValue == 0
		

	def test_03_pcf00_network_attachment_loss(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf00_network_attachment_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_network_attachment_loss')
		assert retValue == 0
		

	def test_03_pcf01_delete_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf01_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_delete_profile')
		assert retValue == 0
		

	def test_03_pcf01_disable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf01_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_disable_profile')
		assert retValue == 0
		

	def test_03_pcf01_enable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf01_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_enable_profile')
		assert retValue == 0
		

	# SGP02 v4 implementation
	def test_03_pcf01_network_attachment_loss(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf01_network_attachment_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_network_attachment_loss')
		assert retValue == 0
		

	def test_03_pcf02_delete_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf02_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_delete_profile')
		assert retValue == 0
		

	def test_03_pcf02_disable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf02_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_disable_profile')
		assert retValue == 0
		

	def test_03_pcf02_enable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf02_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_enable_profile')
		assert retValue == 0
		
	def test_03_pcf02_network_attachment_loss(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf02_network_attachment_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_network_attachment_loss')
		assert retValue == 0
		

	def test_03_pcf04_delete_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf04_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_delete_profile')
		assert retValue == 0
		

	def test_03_pcf04_disable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf04_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_disable_profile')
		assert retValue == 0
		

	def test_03_pcf04_enable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf04_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_enable_profile')
		assert retValue == 0
		
	# SGP02 v4 implementation
	def test_03_pcf04_network_attachment_loss(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf04_network_attachment_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_network_attachment_loss')
		assert retValue == 0
		

# #	tags = ["prastaji", "lm_applet"]
	def test_04_lm_applet_allow_delete_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_allow_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_delete_profile')
		assert retValue == 0
		

	def test_04_lm_applet_allow_disable_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_allow_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_disable_profile')
		assert retValue == 0
		

	def test_04_lm_applet_allow_enable_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_allow_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_enable_profile')
		assert retValue == 0
		

	def test_04_lm_applet_delete_profile_with_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_delete_profile_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_profile_with_notif')
		assert retValue == 0
		

	def test_04_lm_applet_delete_profile_without_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_delete_profile_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_profile_without_notif')
		assert retValue == 0
		

	def test_04_lm_applet_delete_the_enabled_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_delete_the_enabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_the_enabled_profile')
		assert retValue == 0
		

	def test_04_lm_applet_disable_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disable_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_notif')
		assert retValue == 0
		

	def test_04_lm_applet_disable_profile_with_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disable_profile_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_profile_with_notif')
		assert retValue == 0
		

	def test_04_lm_applet_disable_profile_without_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disable_profile_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_profile_without_notif')
		assert retValue == 0
		

	def test_04_lm_applet_disable_the_disabled_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disable_the_disabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_the_disabled_profile')
		assert retValue == 0
		

	def test_04_lm_applet_disallow_delete_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disallow_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_delete_profile')
		assert retValue == 0
		

	def test_04_lm_applet_disallow_disable_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disallow_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_disable_profile')
		assert retValue == 0
		

	def test_04_lm_applet_disallow_enable_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disallow_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_enable_profile')
		assert retValue == 0
		

	def test_04_lm_applet_enable_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_enable_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_notif')
		assert retValue == 0
		

	def test_04_lm_applet_enable_profile_with_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_enable_profile_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_profile_with_notif')
		assert retValue == 0
		

	def test_04_lm_applet_enable_profile_without_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_enable_profile_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_profile_without_notif')
		assert retValue == 0
		

	def test_04_lm_applet_enable_the_enabled_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_enable_the_enabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_the_enabled_profile')
		assert retValue == 0
		

	def test_04_lm_applet_get_applet_config(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_get_applet_config")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_applet_config')
		assert retValue == 0
		

	def test_04_lm_applet_get_eid(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_get_eid")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_eid')
		assert retValue == 0
		

	def test_04_lm_applet_get_euicc_info(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_get_euicc_info")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_euicc_info')
		assert retValue == 0
		

	def test_04_lm_applet_set_fallback_disabled_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_set_fallback_disabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_set_fallback_disabled_profile')
		assert retValue == 0
		

	def test_04_lm_applet_set_fallback(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_set_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_set_fallback')
		assert retValue == 0
		

	def test_05_performance_scp80_create_isdp(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_create_isdp")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_create_isdp')
		assert retValue == 0
		

	def test_05_performance_scp80_delete_profile(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_delete_profile')
		assert retValue == 0
		

	def test_05_performance_scp80_disable_profile_http_notif(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_disable_profile_http_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_disable_profile_http_notif')
		assert retValue == 0
		

	def test_05_performance_scp80_enable_profile_http_notif(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_enable_profile_http_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_enable_profile_http_notif')
		assert retValue == 0
		

	def test_05_performance_scp80_list_profiles(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_list_profiles")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_list_profiles')
		assert retValue == 0
		

	def test_05_performance_scp80_list_resources(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_list_resources")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_list_resources')
		assert retValue == 0
		

	def test_05_performance_scp80_set_fallback_profile(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_set_fallback_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_set_fallback_profile')
		assert retValue == 0
		

	def test_05_performance_scp81_create_isdp(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_create_isdp")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_create_isdp')
		assert retValue == 0
		

	def test_05_performance_scp81_delete_profile(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_delete_profile')
		assert retValue == 0
		

	def test_05_performance_scp81_disable_profile_http_notif(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_disable_profile_http_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_disable_profile_http_notif')
		assert retValue == 0
		

	def test_05_performance_scp81_download_profile_138K(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_download_profile_138K")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_138K')
		assert retValue == 0
		

	def test_05_performance_scp81_download_profile_74K(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_download_profile_74K")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_74K')
		assert retValue == 0
		

	def test_05_performance_scp81_download_profile_9k(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_download_profile_9k")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_9k')
		assert retValue == 0
		

	def test_05_performance_scp81_enable_profile_http_notif(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_enable_profile_http_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_enable_profile_http_notif')
		assert retValue == 0
		

	def test_05_performance_scp81_list_profiles(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_list_profiles")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_list_profiles')
		assert retValue == 0
		

	def test_05_performance_scp81_list_resources(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_list_resources")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_list_resources')
		assert retValue == 0
		

	def test_05_performance_scp81_set_fallback_profile(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_set_fallback_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_set_fallback_profile')
		assert retValue == 0
		

	def test_06_stress_download_delete_status(self):
		Constants.testLinkTestCase.append("test_06_stress.download_delete_status")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.download_delete_status')
		assert retValue == 0
		

	def test_06_stress_download_delete_without_status(self):
		Constants.testLinkTestCase.append("test_06_stress.download_delete_without_status")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.download_delete_without_status')
		assert retValue == 0
		

	def test_06_stress_enable_profile_disable_profile(self):
		Constants.testLinkTestCase.append("test_06_stress.enable_profile_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.enable_profile_disable_profile')
		assert retValue == 0
		

	def test_06_stress_enable_profile_fallback_network_loss(self):
		Constants.testLinkTestCase.append("test_06_stress.enable_profile_fallback_network_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.enable_profile_fallback_network_loss')
		assert retValue == 0
		

	def test_06_stress_enable_rollback_network_loss_disable_profile(self):
		Constants.testLinkTestCase.append("test_06_stress.enable_rollback_network_loss_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.enable_rollback_network_loss_disable_profile')
		assert retValue == 0
		

	def test_07_download_profile_comparison(self):
		Constants.testLinkTestCase.append("test_07_download_profile.download_profile_comparison")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_07_download_profile.download_profile_comparison')
		assert retValue == 0
		

# 	# # ###After Sprint 11, Glonass was not use for test
# 	# # # def test_08_nom_neg_001_profile_activation_failed_due_to_activated(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_001_profile_activation_failed_due_to_activated")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_001_profile_activation_failed_due_to_activated')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_neg_002_profile_deactivation_failed_due_to_disabled_state(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_002_profile_deactivation_failed_due_to_disabled_state")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_002_profile_deactivation_failed_due_to_disabled_state')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_neg_005_set_profile_failure_cause(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_005_set_profile_failure_cause")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_005_set_profile_failure_cause')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_001_profile_activation_via_applet_selection_without_notif(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_001_profile_activation_via_applet_selection_without_notif")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_001_profile_activation_via_applet_selection_without_notif')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_002_profile_deactivation_via_applet_selection_without_notif(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_002_profile_deactivation_via_applet_selection_without_notif")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_002_profile_deactivation_via_applet_selection_without_notif')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_003_profile_activation_via_process_toolkit_without_notif(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_003_profile_activation_via_process_toolkit_without_notif")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_003_profile_activation_via_process_toolkit_without_notif')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_004_profile_deactivation_via_process_toolkit_without_notif(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_004_profile_deactivation_via_process_toolkit_without_notif")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_004_profile_deactivation_via_process_toolkit_without_notif')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_005_lock_unlock_mconnect_command(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_005_lock_unlock_mconnect_command")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_005_lock_unlock_mconnect_command')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_006_get_glonass_profile(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_006_get_glonass_profile")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_006_get_glonass_profile')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_007_profile_activation_via_applet_selection_with_notif(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_007_profile_activation_via_applet_selection_with_notif")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_007_profile_activation_via_applet_selection_with_notif')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_008_profile_deactivation_via_applet_selection_with_notif(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_008_profile_deactivation_via_applet_selection_with_notif")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_008_profile_deactivation_via_applet_selection_with_notif')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_009_profile_activation_via_process_toolkit_with_notif(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_009_profile_activation_via_process_toolkit_with_notif")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_009_profile_activation_via_process_toolkit_with_notif')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_010_profile_deactivation_via_process_toolkit_with_notif(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_010_profile_deactivation_via_process_toolkit_with_notif")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_010_profile_deactivation_via_process_toolkit_with_notif')
# 	# # # 	assert retValue == 0
# 	# # # 	

# 	# # # def test_08_nom_pos_011_profile_activation_via_local_management_applet(self):
# 	# # # 	Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_011_profile_activation_via_local_management_applet")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_011_profile_activation_via_local_management_applet')
# 	# # # 	assert retValue == 0
# 	# # # 	

	def test_09_audit_hsm_files(self):
		Constants.testLinkTestCase.append("test_09_applet_hsm.audit_hsm_files")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_09_applet_hsm.audit_hsm_files')
		assert retValue == 0
		

	def test_10_nom_pos_001_update_applet_configuration_via_applet_selection(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_001_update_applet_configuration_via_applet_selection")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_001_update_applet_configuration_via_applet_selection')
		assert retValue == 0
		

	def test_10_nom_pos_002_update_applet_configuration_via_update_smsr(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_002_update_applet_configuration_via_update_smsr")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_002_update_applet_configuration_via_update_smsr')
		assert retValue == 0
		

	def test_10_nom_pos_003_applet_triggered_by_mcc_change(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_003_applet_triggered_by_mcc_change")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_003_applet_triggered_by_mcc_change')
		assert retValue == 0
		

	def test_10_nom_pos_004_applet_triggered_by_power_on(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_004_applet_triggered_by_power_on")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_004_applet_triggered_by_power_on')
		assert retValue == 0
		

	def test_10_nom_pos_005_applet_triggered_by_mnc_change(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_005_applet_triggered_by_mnc_change")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_005_applet_triggered_by_mnc_change')
		assert retValue == 0
		

	def test_10_nom_pos_006_activate_location_plugin_notification_via_ota(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_006_activate_location_plugin_notification_via_ota")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_006_activate_location_plugin_notification_via_ota')
		assert retValue == 0
		

	def test_10_nom_pos_007_deactivate_location_plugin_notification_via_ota(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_007_deactivate_location_plugin_notification_via_ota")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_007_deactivate_location_plugin_notification_via_ota')
		assert retValue == 0
		

	def test_10_nom_pos_008_activate_location_plugin_notification_via_applet(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_008_activate_location_plugin_notification_via_applet")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_008_activate_location_plugin_notification_via_applet')
		assert retValue == 0
		

	def test_10_nom_pos_009_deactivate_location_plugin_notification_via_applet(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_009_deactivate_location_plugin_notification_via_applet")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_009_deactivate_location_plugin_notification_via_applet')
		assert retValue == 0
		

	def test_10_nom_pos_cr_001_location_plugin_keep_sending_notif_when_power_on_eventhough_no_loci_change(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_cr_001_location_plugin_keep_sending_notif_when_power_on_eventhough_no_loci_change")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_cr_001_location_plugin_keep_sending_notif_when_power_on_eventhough_no_loci_change')
		assert retValue == 0
		
	def test_10_nom_pos_cr_002_deactivate_applet_from_smsr_notif_confirm(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_cr_002_deactivate_applet_from_smsr_notif_confirm")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_cr_002_deactivate_applet_from_smsr_notif_confirm')
		assert retValue == 0
		

# 	def test_11_sprint_2_nom_neg_002_ota_poller_unrecognized_envelope_triggering_while_applet_active_but_open_channel_failed(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_neg_002_ota_poller_unrecognized_envelope_triggering_while_applet_active_but_open_channel_failed")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_neg_002_ota_poller_unrecognized_envelope_triggering_while_applet_active_but_open_channel_failed')
# 		assert retValue == 0
		

# 	def test_11_sprint_2_nom_neg_003_ota_poller_terminal_profile_triggering_while_applet_inactive(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_neg_003_ota_poller_terminal_profile_triggering_while_applet_inactive")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_neg_003_ota_poller_terminal_profile_triggering_while_applet_inactive')
# 		assert retValue == 0
		

# 	def test_11_sprint_2_nom_neg_004_ota_poller_unrecognized_envelope_triggering_while_applet_inactive(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_neg_004_ota_poller_unrecognized_envelope_triggering_while_applet_inactive")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_neg_004_ota_poller_unrecognized_envelope_triggering_while_applet_inactive')
# 		assert retValue == 0
		

# 	def test_11_sprint_2_nom_pos_001_ota_poller_terminal_profile_triggering_while_applet_active(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_pos_001_ota_poller_terminal_profile_triggering_while_applet_active")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_pos_001_ota_poller_terminal_profile_triggering_while_applet_active')
# 		assert retValue == 0
		

# 	def test_11_sprint_2_nom_pos_002_ota_poller_unrecognized_envelope_triggering_while_applet_active(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_pos_002_ota_poller_unrecognized_envelope_triggering_while_applet_active")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_pos_002_ota_poller_unrecognized_envelope_triggering_while_applet_active')
# 		assert retValue == 0
		

# 	def test_11_sprint_2_nom_pos_003_ota_poller_timer_based_triggering_while_applet_active(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_pos_003_ota_poller_timer_based_triggering_while_applet_active")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_pos_003_ota_poller_timer_based_triggering_while_applet_active')
# 		assert retValue == 0
		

# 	def test_11_sprint_2_nom_pos_004_ota_poller_get_ota_poller_applet_config(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_pos_004_ota_poller_get_ota_poller_applet_config")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_pos_004_ota_poller_get_ota_poller_applet_config')
# 		assert retValue == 0
		

# 	def test_11_sprint_2_nom_pos_005_ota_poller_terminal_profile_triggering_while_applet_active_and_roaming_active(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_pos_005_ota_poller_terminal_profile_triggering_while_applet_active_and_roaming_active")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_pos_005_ota_poller_terminal_profile_triggering_while_applet_active_and_roaming_active')
# 		assert retValue == 0
		

# 	def test_11_sprint_2_nom_pos_006_ota_poller_terminal_profile_triggering_while_applet_active_and_roaming_inactive(self):
# 		Constants.testLinkTestCase.append("test_11_sprint_2.nom_pos_006_ota_poller_terminal_profile_triggering_while_applet_active_and_roaming_inactive")
# 		Constants.testLinkCountRunAll+=1
# 		retValue = Run_ModuleMain('test_11_sprint_2.nom_pos_006_ota_poller_terminal_profile_triggering_while_applet_active_and_roaming_inactive')
# 		assert retValue == 0
		


# # # # # # ## '''Sprint 3 new test script'''
# # # 	# def test_11_sprint_3_telefonica_CTD_PoR_silent_disable_whitelist_na_oa_na(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_CTD_PoR_silent_disable_whitelist_na_oa_na")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_CTD_PoR_silent_disable_whitelist_na_oa_na')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # # 	# def test_11_sprint_3_telefonica_dns_req_nom_neg_001_dns_resolution_with_no_dns_ip_and_no_ip_in_handset(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_dns_req_nom_neg_001_dns_resolution_with_no_dns_ip_and_no_ip_in_handset")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_dns_req_nom_neg_001_dns_resolution_with_no_dns_ip_and_no_ip_in_handset')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # # 	# def test_11_sprint_3_telefonica_dns_req_nom_pos_001_no_dns_ip_and_single_ip_in_handset(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_dns_req_nom_pos_001_no_dns_ip_and_single_ip_in_handset")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_dns_req_nom_pos_001_no_dns_ip_and_single_ip_in_handset')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # # 	# def test_11_sprint_3_telefonica_dns_req_nom_pos_002_no_dns_ip_and_two_ip_in_handset(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_dns_req_nom_pos_002_no_dns_ip_and_two_ip_in_handset")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_dns_req_nom_pos_002_no_dns_ip_and_two_ip_in_handset')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # # 	# def test_11_sprint_3_telefonica_dns_req_nom_pos_003_reentrance_psm_dns_request(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_dns_req_nom_pos_003_reentrance_psm_dns_request")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_dns_req_nom_pos_003_reentrance_psm_dns_request')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # # 	# def test_11_sprint_3_telefonica_PoR_silent_disable_whitelist_na_oa_na(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_PoR_silent_disable_whitelist_na_oa_na")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_PoR_silent_disable_whitelist_na_oa_na')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # # 	# def test_11_sprint_3_telefonica_PoR_silent_enable_whitelist_disable_oa_na(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_PoR_silent_enable_whitelist_disable_oa_na")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_PoR_silent_enable_whitelist_disable_oa_na')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # # 	# def test_11_sprint_3_telefonica_PoR_silent_enable_whitelist_enable_oa_in_whitelist(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_PoR_silent_enable_whitelist_enable_oa_in_whitelist")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_PoR_silent_enable_whitelist_enable_oa_in_whitelist')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # # 	# def test_11_sprint_3_telefonica_PoR_silent_enable_whitelist_enable_oa_not_in_whitelist(self):
# # # 	# 	Constants.testLinkTestCase.append("test_11_sprint_3.telefonica_PoR_silent_enable_whitelist_enable_oa_not_in_whitelist")
# # # 	# 	Constants.testLinkCountRunAll+=1
# # # 	# 	retValue = Run_ModuleMain('test_11_sprint_3.telefonica_PoR_silent_enable_whitelist_enable_oa_not_in_whitelist')
# # # 	# 	assert retValue == 0
# # # 	# 	

# # ## '''Sprint 4 new test script'''
	def test_12_sprint_4_skt_local_swap_nom_neg_001_fallback_prohibited_when_switch_factory_profile_active(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_neg_001_fallback_prohibited_when_switch_factory_profile_active")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_neg_001_fallback_prohibited_when_switch_factory_profile_active')
		assert retValue == 0
		

	def test_12_sprint_4_skt_local_swap_nom_neg_002_rollback_prohibited_when_switch_factory_profile_active(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_neg_002_rollback_prohibited_when_switch_factory_profile_active")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_neg_002_rollback_prohibited_when_switch_factory_profile_active')
		assert retValue == 0
		

	def test_12_sprint_4_skt_local_swap_nom_neg_003_fallback_prohibited_when_switch_skt_profile_active(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_neg_003_fallback_prohibited_when_switch_skt_profile_active")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_neg_003_fallback_prohibited_when_switch_skt_profile_active')
		assert retValue == 0
		

	def test_12_sprint_4_skt_local_swap_nom_neg_004_rollback_prohibited_when_switch_skt_profile_active(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_neg_004_rollback_prohibited_when_switch_skt_profile_active")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_neg_004_rollback_prohibited_when_switch_skt_profile_active')
		assert retValue == 0
		

	def test_12_sprint_4_skt_local_swap_nom_neg_005_factory_profile_cant_be_enabled_via_gsma(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_neg_005_factory_profile_cant_be_enabled_via_gsma")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_neg_005_factory_profile_cant_be_enabled_via_gsma')
		assert retValue == 0
		

	def test_12_sprint_4_skt_local_swap_nom_pos_001_set_factory_profile_from_enabled_ISDR_Profile_11(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_pos_001_set_factory_profile_from_enabled_ISDR_Profile_11")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_pos_001_set_factory_profile_from_enabled_ISDR_Profile_11')
		assert retValue == 0
		

	def test_12_sprint_4_skt_local_swap_nom_pos_002_set_factory_profile_from_disabled_ISDR_Profile_11(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_pos_002_set_factory_profile_from_disabled_ISDR_Profile_11")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_pos_002_set_factory_profile_from_disabled_ISDR_Profile_11')
		assert retValue == 0
		

	def test_12_sprint_4_skt_local_swap_nom_pos_003_change_to_factory_profile(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_pos_003_change_to_factory_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_pos_003_change_to_factory_profile')
		assert retValue == 0
		

	def test_12_sprint_4_skt_local_swap_nom_pos_004_change_to_skt_profile(self):
		Constants.testLinkTestCase.append("test_12_sprint_4.skt_local_swap_nom_pos_004_change_to_skt_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_12_sprint_4.skt_local_swap_nom_pos_004_change_to_skt_profile')
		assert retValue == 0
		

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_authentication_on_USAT_Pairing_and_SAIP_Profile(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_authentication_on_USAT_Pairing_and_SAIP_Profile")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_authentication_on_USAT_Pairing_and_SAIP_Profile')
	# # # 	assert retValue == 0
	# # # 	

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_sending_IMEI_not_registered_on_EF_IWL(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_sending_IMEI_not_registered_on_EF_IWL")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_sending_IMEI_not_registered_on_EF_IWL')
	# # # 	assert retValue == 0
	# # # 	

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_sending_IMEI_registered_on_EF_IWL(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_sending_IMEI_registered_on_EF_IWL")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_sending_IMEI_registered_on_EF_IWL')
	# # # 	assert retValue == 0
	# # # 	

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_sending_IMEI_SV_not_registered_on_EF_IWL(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_sending_IMEI_SV_not_registered_on_EF_IWL")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_sending_IMEI_SV_not_registered_on_EF_IWL')
	# # # 	assert retValue == 0
	# # # 	

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_sending_IMEI_SV_registered_on_EF_IWL(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_sending_IMEI_SV_registered_on_EF_IWL")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_sending_IMEI_SV_registered_on_EF_IWL')
	# # # 	assert retValue == 0
	# # # 	

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_sending_TR_10(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_sending_TR_10")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_sending_TR_10')
	# # # 	assert retValue == 0
	# # # 	

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_sending_TR_11(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_sending_TR_11")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_sending_TR_11')
	# # # 	assert retValue == 0
	# # # 	

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_sending_TR_20_try_less_than_3_times(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_sending_TR_20_try_less_than_3_times")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_sending_TR_20_try_less_than_3_times')
	# # # 	assert retValue == 0
	# # # 	

	# # # def test_12_sprint_4_tmus_USAT_Pairing_check_sending_TR_20_try_more_than_3_times(self):
	# # # 	Constants.testLinkTestCase.append("test_12_sprint_4.tmus_USAT_Pairing_check_sending_TR_20_try_more_than_3_times")
	# # # 	Constants.testLinkCountRunAll+=1
	# # # 	retValue = Run_ModuleMain('test_12_sprint_4.tmus_USAT_Pairing_check_sending_TR_20_try_more_than_3_times')
	# # # 	assert retValue == 0
	# # # 	

# # ## '''Sprint 5 new test script'''
	def test_13_sprint_5_gsma_sgp_v_3_2_scp81_switchback_profile_after_fallback(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.gsma_sgp_v_3_2_scp81_switchback_profile_after_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.gsma_sgp_v_3_2_scp81_switchback_profile_after_fallback')
		assert retValue == 0
		

	def test_13_sprint_5_gsma_sgp_v_3_2_switchback_profile_after_fallback(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.gsma_sgp_v_3_2_switchback_profile_after_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.gsma_sgp_v_3_2_switchback_profile_after_fallback')
		assert retValue == 0
		

# 	# # # def test_13_sprint_5_gsma_sgp_v_3_2_switchback_profile_after_rollback(self):
# 	# # # 	Constants.testLinkTestCase.append("test_13_sprint_5.gsma_sgp_v_3_2_switchback_profile_after_rollback")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_13_sprint_5.gsma_sgp_v_3_2_switchback_profile_after_rollback')
# 	# # # 	assert retValue == 0
# 	# # # 	
		

	def test_13_sprint_5_gsma_sgp_v_3_2_switchback_profile_unlimited_and_cancellation(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.gsma_sgp_v_3_2_switchback_profile_unlimited_and_cancellation")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.gsma_sgp_v_3_2_switchback_profile_unlimited_and_cancellation')
		assert retValue == 0
		

	# def test_13_sprint_5_gsma_sgp_v_3_2_connectivity_param_update_using_scp03_sms_protocol(self):
	# 	Constants.testLinkTestCase.append("test_13_sprint_5.gsma_sgp_v_3_2_connectivity_param_update_using_scp03_sms_protocol")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_13_sprint_5.gsma_sgp_v_3_2_connectivity_param_update_using_scp03_sms_protocol')
	# 	assert retValue == 0
			

	def test_13_sprint_5_psm_connectivity_set_APF_off_twice(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.psm_connectivity_set_APF_off_twice")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.psm_connectivity_set_APF_off_twice')
		assert retValue == 0
		

	def test_13_sprint_5_psm_connectivity_set_APF_on_twice(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.psm_connectivity_set_APF_on_twice")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.psm_connectivity_set_APF_on_twice')
		assert retValue == 0
		

	def test_13_sprint_5_psm_connectivity_set_FRM_off_APF_off(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.psm_connectivity_set_FRM_off_APF_off")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.psm_connectivity_set_FRM_off_APF_off')
		assert retValue == 0
		

	def test_13_sprint_5_psm_connectivity_set_FRM_off_APF_on(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.psm_connectivity_set_FRM_off_APF_on")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.psm_connectivity_set_FRM_off_APF_on')
		assert retValue == 0
		

	def test_13_sprint_5_psm_connectivity_set_FRM_off_twice(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.psm_connectivity_set_FRM_off_twice")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.psm_connectivity_set_FRM_off_twice')
		assert retValue == 0
		

	def test_13_sprint_5_psm_connectivity_set_FRM_on_APF_off(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.psm_connectivity_set_FRM_on_APF_off")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.psm_connectivity_set_FRM_on_APF_off')
		assert retValue == 0
		

	def test_13_sprint_5_psm_connectivity_set_FRM_on_APF_on(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.psm_connectivity_set_FRM_on_APF_on")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.psm_connectivity_set_FRM_on_APF_on')
		assert retValue == 0
		

	def test_13_sprint_5_psm_connectivity_set_FRM_on_twice(self):
		Constants.testLinkTestCase.append("test_13_sprint_5.psm_connectivity_set_FRM_on_twice")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_13_sprint_5.psm_connectivity_set_FRM_on_twice')
		assert retValue == 0
		

# 	# # # def test_13_sprint_5_skt_refresh_applet(self):
# 	# # # 	Constants.testLinkTestCase.append("test_13_sprint_5.skt_refresh_applet")
# 	# # # 	Constants.testLinkCountRunAll+=1
# 	# # # 	retValue = Run_ModuleMain('test_13_sprint_5.skt_refresh_applet')
# 	# # # 	assert retValue == 0
# 	# # # 	

# # # # ## '''Sprint 6 NO new test script'''

# # # ## '''Sprint 7 new test script'''
	def test_15_sprint_7_gsma_sgp_v_3_2_connectivity_param_update_using_scp03_http_protocol(self):
		Constants.testLinkTestCase.append("test_15_sprint_7.gsma_sgp_v_3_2_connectivity_param_update_using_scp03_http_protocol")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_15_sprint_7.gsma_sgp_v_3_2_connectivity_param_update_using_scp03_http_protocol')
		assert retValue == 0
		
	def test_15_sprint_7_gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback(self):
		Constants.testLinkTestCase.append("test_15_sprint_7.gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_15_sprint_7.gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback')
		assert retValue == 0
		

	def test_15_sprint_7_gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_not_as_fallback(self):
		Constants.testLinkTestCase.append("test_15_sprint_7.gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_not_as_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_15_sprint_7.gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_not_as_fallback')
		assert retValue == 0
		

	def test_15_sprint_7_gsma_sgp_v_3_2_master_delete_pcf02_enable_isdp_as_fallback(self):
		Constants.testLinkTestCase.append("test_15_sprint_7.gsma_sgp_v_3_2_master_delete_pcf02_enable_isdp_as_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_15_sprint_7.gsma_sgp_v_3_2_master_delete_pcf02_enable_isdp_as_fallback')
		assert retValue == 0
		

	def test_15_sprint_7_gsma_sgp_v_3_2_master_delete_pcf02_enable_isdp_not_as_fallback(self):
		Constants.testLinkTestCase.append("test_15_sprint_7.gsma_sgp_v_3_2_master_delete_pcf02_enable_isdp_not_as_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_15_sprint_7.gsma_sgp_v_3_2_master_delete_pcf02_enable_isdp_not_as_fallback')
		assert retValue == 0
		

	def test_15_sprint_7_gsma_sgp_v_3_2_smsr_addressing_param_update_dns_param(self):
		Constants.testLinkTestCase.append("test_15_sprint_7.gsma_sgp_v_3_2_smsr_addressing_param_update_dns_param")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_15_sprint_7.gsma_sgp_v_3_2_smsr_addressing_param_update_dns_param')
		assert retValue == 0
		

# # # # ## '''Sprint 8 NO new test script'''

# # # ## '''Sprint 9 new test script'''
	def test_17_sprint_9_gsma_sgp_v_3_2_handle_default_notification(self):
		Constants.testLinkTestCase.append("test_17_sprint_9.gsma_sgp_v_3_2_handle_default_notification")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_17_sprint_9.gsma_sgp_v_3_2_handle_default_notification')
		assert retValue == 0
		

# # # # ## '''Sprint 10 NO new test script'''		

# # # # ## '''Sprint 11 new test script'''		
	def test_19_sprint_11_gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback_empty_app_provider_5F20(self):
		Constants.testLinkTestCase.append("test_19_sprint_11.gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback_empty_app_provider_5F20")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_19_sprint_11.gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback_empty_app_provider_5F20')
		assert retValue == 0
				

	def test_19_sprint_11_gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback_incorrect_app_provider_5F20(self):
		Constants.testLinkTestCase.append("test_19_sprint_11.gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback_incorrect_app_provider_5F20")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_19_sprint_11.gsma_sgp_v_3_2_master_delete_pcf02_disable_isdp_as_fallback_incorrect_app_provider_5F20')
		assert retValue == 0
				

	def test_19_sprint_11_gsma_sgp_v_3_2_smsr_addressing_param_update_sms_param(self):
		Constants.testLinkTestCase.append("test_19_sprint_11.gsma_sgp_v_3_2_smsr_addressing_param_update_sms_param")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_19_sprint_11.gsma_sgp_v_3_2_smsr_addressing_param_update_sms_param')
		assert retValue == 0
			

	def test_19_sprint_11_handset_power_off_during_APF(self):
		Constants.testLinkTestCase.append("test_19_sprint_11.handset_power_off_during_APF")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_19_sprint_11.handset_power_off_during_APF')
		assert retValue == 0
			

# # ## '''Sprint 12 new test script'''		
	def test_20_sprint_12_auto_service_activate_emergency_profile_17_delete_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_activate_emergency_profile_17_delete_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_activate_emergency_profile_17_delete_profile_17')
		assert retValue == 0
												

	def test_20_sprint_12_auto_service_activate_emergency_profile_17_enable_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_activate_emergency_profile_17_enable_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_activate_emergency_profile_17_enable_profile_17')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_activate_emergency_profile_17_enable_profile_18(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_activate_emergency_profile_17_enable_profile_18")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_activate_emergency_profile_17_enable_profile_18')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_activate_emergency_profile_17_twice(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_activate_emergency_profile_17_twice")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_activate_emergency_profile_17_twice')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_activate_emergency_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_activate_emergency_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_activate_emergency_profile_17')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_activate_notification_emergency_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_activate_notification_emergency_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_activate_notification_emergency_profile_17')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_check_enabled_profile_after_set_emergency_profile(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_check_enabled_profile_after_set_emergency_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_check_enabled_profile_after_set_emergency_profile')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_deactivate_emergency_profile_17_enable_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_deactivate_emergency_profile_17_enable_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_deactivate_emergency_profile_17_enable_profile_17')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_deactivate_emergency_profile_17_enable_profile_18(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_deactivate_emergency_profile_17_enable_profile_18")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_deactivate_emergency_profile_17_enable_profile_18')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_deactivate_emergency_profile_twice(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_deactivate_emergency_profile_twice")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_deactivate_emergency_profile_twice')
		assert retValue == 0
		
	
	def test_20_sprint_12_auto_service_deactivate_notification_emergency_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_deactivate_notification_emergency_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_deactivate_notification_emergency_profile_17')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_get_euicc_info(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_get_euicc_info")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_get_euicc_info')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_m_connect_lock_activate_emergency_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_m_connect_lock_activate_emergency_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_m_connect_lock_activate_emergency_profile_17')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_m_connect_lock_deactivate_emergency_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_m_connect_lock_deactivate_emergency_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_m_connect_lock_deactivate_emergency_profile_17')
		assert retValue == 0
				

	def test_20_sprint_12_auto_service_set_emergency_profile_17_enable_profile_17(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_set_emergency_profile_17_enable_profile_17")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_set_emergency_profile_17_enable_profile_17')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_set_emergency_profile_17_enable_profile_18(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_set_emergency_profile_17_enable_profile_18")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_set_emergency_profile_17_enable_profile_18')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_set_emergency_profile_on_2_different_isdp(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_set_emergency_profile_on_2_different_isdp")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_set_emergency_profile_on_2_different_isdp')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_set_emergency_profile_twice(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_set_emergency_profile_twice")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_set_emergency_profile_twice')
		assert retValue == 0
		
	
	def test_20_sprint_12_auto_service_set_emergency_profile(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_set_emergency_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_set_emergency_profile')
		assert retValue == 0
		
	
	def test_20_sprint_12_auto_service_via_toolkit_set_activate_deactivate_emergency_profile(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_via_toolkit_set_activate_deactivate_emergency_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_via_toolkit_set_activate_deactivate_emergency_profile')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_via_toolkit_set_activate_emergency_profile(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_via_toolkit_set_activate_emergency_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_via_toolkit_set_activate_emergency_profile')
		assert retValue == 0
		

	def test_20_sprint_12_auto_service_via_toolkit_set_emergency_profile(self):
		Constants.testLinkTestCase.append("test_20_sprint_12.auto_service_via_toolkit_set_emergency_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_20_sprint_12.auto_service_via_toolkit_set_emergency_profile')
		assert retValue == 0
		
	# DK 3.2 will include all test_26_sprint_18 directory
	# DK 3.4 will exclude some test since does not contain Auto Service applet

	def test_26_sprint_18_cr_mconnect_lock_01_fallback_pending_due_mconnect_lock_on(self):
		Constants.testLinkTestCase.append("test_26_sprint_18.cr_mconnect_lock_01_fallback_pending_due_mconnect_lock_on")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_26_sprint_18.cr_mconnect_lock_01_fallback_pending_due_mconnect_lock_on')
		assert retValue == 0

	def test_26_sprint_18_cr_mconnect_lock_02_rollback_pending_due_mconnect_lock_on(self):
		Constants.testLinkTestCase.append("test_26_sprint_18.cr_mconnect_lock_02_rollback_pending_due_mconnect_lock_on")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_26_sprint_18.cr_mconnect_lock_02_rollback_pending_due_mconnect_lock_on')
		assert retValue == 0

	def test_26_sprint_18_cr_mconnect_lock_04_auto_service_combination(self):
		Constants.testLinkTestCase.append("test_26_sprint_18.cr_mconnect_lock_04_auto_service_combination")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_26_sprint_18.cr_mconnect_lock_04_auto_service_combination')
		assert retValue == 0

	def test_26_sprint_18_cr_mconnect_lock_05_switchback_pending_due_mconnect_lock_on(self):
		Constants.testLinkTestCase.append("test_26_sprint_18.cr_mconnect_lock_05_switchback_pending_due_mconnect_lock_on")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_26_sprint_18.cr_mconnect_lock_05_switchback_pending_due_mconnect_lock_on')
		assert retValue == 0

	def test_26_sprint_18_cr_mconnect_lock_06_notif_retry_when_mconnect_lock_on(self):
		Constants.testLinkTestCase.append("test_26_sprint_18.cr_mconnect_lock_06_notif_retry_when_mconnect_lock_on")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_26_sprint_18.cr_mconnect_lock_06_notif_retry_when_mconnect_lock_on')
		assert retValue == 0

	# DK 3.4 deactivate this, no AutoService applet
	def test_26_sprint_18_cr_notif_timer_01(self):
		Constants.testLinkTestCase.append("test_26_sprint_18.cr_notif_timer_01")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_26_sprint_18.cr_notif_timer_01')
		assert retValue == 0

	def test_26_sprint_18_cr_notif_timer_02(self):
		Constants.testLinkTestCase.append("test_26_sprint_18.cr_notif_timer_02")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_26_sprint_18.cr_notif_timer_02')
		assert retValue == 0

	def test_26_sprint_18_cr_synchro_without_apn_mno(self):
		Constants.testLinkTestCase.append("test_26_sprint_18.cr_synchro_without_apn_mno")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_26_sprint_18.cr_synchro_without_apn_mno')
		assert retValue == 0

# # # # ## '''Sprint 13 NO new test script'''
# # # # ## '''Sprint 14 NO new test script'''
# # # # ## '''Sprint 15 NO new test script'''
# # # # ## '''Sprint 16 NO new test script'''
	
	# def test_27_phase_1_gsma_v4_compl_during_fallback_profile_deletion_of_pol_02_shall_rejected_01(self):
	# 	Constants.testLinkTestCase.append("test_27_phase_1.gsma_v4_compl_during_fallback_profile_deletion_of_pol_02_shall_rejected_01")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_27_phase_1.gsma_v4_compl_during_fallback_profile_deletion_of_pol_02_shall_rejected_01')
	# 	assert retValue == 0

	# def test_27_phase_1_gsma_v4_compl_during_fallback_profile_deletion_of_pol_02_shall_rejected_02(self):
	# 	Constants.testLinkTestCase.append("test_27_phase_1.gsma_v4_compl_during_fallback_profile_deletion_of_pol_02_shall_rejected_02")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_27_phase_1.gsma_v4_compl_during_fallback_profile_deletion_of_pol_02_shall_rejected_02')
	# 	assert retValue == 0

	# def test_27_phase_1_gsma_v4_compl_prevent_set_fallback_during_fallback_02(self):
	# 	Constants.testLinkTestCase.append("test_27_phase_1.gsma_v4_compl_prevent_set_fallback_during_fallback_02")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_27_phase_1.gsma_v4_compl_prevent_set_fallback_during_fallback_02')
	# 	assert retValue == 0

	# def test_27_phase_1_gsma_v4_compl_prevent_set_fallback_during_fallback_03(self):
	# 	Constants.testLinkTestCase.append("test_27_phase_1.gsma_v4_compl_prevent_set_fallback_during_fallback_03")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_27_phase_1.gsma_v4_compl_prevent_set_fallback_during_fallback_03')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_01_emergency_profile_set_and_enable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_01_emergency_profile_set_and_enable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_01_emergency_profile_set_and_enable')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_02_emergency_profile_set_but_have_fallback(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_02_emergency_profile_set_but_have_fallback")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_02_emergency_profile_set_but_have_fallback')
	# 	assert retValue == 0	

	# def test_28_phase_2_gsma_v4_03_emergency_profile_set_twice_to_different_profile_and_enable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_03_emergency_profile_set_twice_to_different_profile_and_enable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_03_emergency_profile_set_twice_to_different_profile_and_enable')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_04_emergency_profile_set_on_enabled_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_04_emergency_profile_set_on_enabled_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_04_emergency_profile_set_on_enabled_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_05_emergency_profile_set_on_other_profile_while_emergency_active(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_05_emergency_profile_set_on_other_profile_while_emergency_active")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_05_emergency_profile_set_on_other_profile_while_emergency_active')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_06_emergency_profile_active_than_disabled(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_06_emergency_profile_active_than_disabled")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_06_emergency_profile_active_than_disabled')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_07_emergency_profile_deletion(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_07_emergency_profile_deletion")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_07_emergency_profile_deletion')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_08_emergency_profile_set_to_other_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_08_emergency_profile_set_to_other_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_08_emergency_profile_set_to_other_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_09_emergency_profile_set_twice(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_09_emergency_profile_set_twice")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_09_emergency_profile_set_twice')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_10_emergency_profile_suddenly_loss_connection(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_10_emergency_profile_suddenly_loss_connection")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_10_emergency_profile_suddenly_loss_connection')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_11_emergency_profile_rollback(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_11_emergency_profile_rollback")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_11_emergency_profile_rollback')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_12_emergency_profile_not_set_and_local_enable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_12_emergency_profile_not_set_and_local_enable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_12_emergency_profile_not_set_and_local_enable')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_13_emergency_profile_set_and_local_enable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_13_emergency_profile_set_and_local_enable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_13_emergency_profile_set_and_local_enable')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_14_emergency_profile_local_enable_twice(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_14_emergency_profile_local_enable_twice")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_14_emergency_profile_local_enable_twice')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_15_emergency_profile_local_disable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_15_emergency_profile_local_disable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_15_emergency_profile_local_disable')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_16_emergency_profile_local_disable_failed(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_16_emergency_profile_local_disable_failed")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_16_emergency_profile_local_disable_failed')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_17_emergency_profile_local_disable_twice(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_17_emergency_profile_local_disable_twice")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_17_emergency_profile_local_disable_twice')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_18_emergency_profile_gsma_enable_local_disable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_18_emergency_profile_gsma_enable_local_disable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_18_emergency_profile_gsma_enable_local_disable')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_19_emergency_profile_local_enable_loss_conn_to_fallback(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_19_emergency_profile_local_enable_loss_conn_to_fallback")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_19_emergency_profile_local_enable_loss_conn_to_fallback')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_20_emergency_profile_local_enable_loss_conn(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_20_emergency_profile_local_enable_loss_conn")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_20_emergency_profile_local_enable_loss_conn')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_21_emergency_profile_restart_after_local_enable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_21_emergency_profile_restart_after_local_enable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_21_emergency_profile_restart_after_local_enable')
	# 	assert retValue == 0

	# def test_28_phase_2_gsma_v4_22_emergency_profile_restart_after_enable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_22_emergency_profile_restart_after_enable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_22_emergency_profile_restart_after_enable')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_01_local_enable_but_not_exist(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_01_local_enable_but_not_exist")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_01_local_enable_but_not_exist')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_02_on_new_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_02_on_new_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_02_on_new_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_03_on_fallback_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_03_on_fallback_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_03_on_fallback_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_05_on_enabled_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_05_on_enabled_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_05_on_enabled_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_06_set_twice_on_diff_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_06_set_twice_on_diff_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_06_set_twice_on_diff_profile')
	# 	assert retValue == 0
	
	# def test_28_phase_2_test_profile_07_set_twice_on_same_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_07_set_twice_on_same_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_07_set_twice_on_same_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_08_local_enable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_08_local_enable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_08_local_enable')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_09_local_enable_on_enabled_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_09_local_enable_on_enabled_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_09_local_enable_on_enabled_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_10_local_enable_on_emergency_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_10_local_enable_on_emergency_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_10_local_enable_on_emergency_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_11_cannot_provide_connectivity(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_11_cannot_provide_connectivity")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_11_cannot_provide_connectivity')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_12_enable_and_loss_connection(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_12_enable_and_loss_connection")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_12_enable_and_loss_connection')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_13_local_disable(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_13_local_disable")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_13_local_disable')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_14_local_disable_twice(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_14_local_disable_twice")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_14_local_disable_twice')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_15_local_disable_failed_to_fallback(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_15_local_disable_failed_to_fallback")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_15_local_disable_failed_to_fallback')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_16_local_disable_failed_without_fallback(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_16_local_disable_failed_without_fallback")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_16_local_disable_failed_without_fallback')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_17_restart_device(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_17_restart_device")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_17_restart_device')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_18_enable_using_gsma(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_18_enable_using_gsma")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_18_enable_using_gsma')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_19_disable_using_gsma(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_19_disable_using_gsma")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_19_disable_using_gsma')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_20_delete_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_20_delete_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_20_delete_profile')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_21_fallback_mechanism(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_21_fallback_mechanism")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_21_fallback_mechanism')
	# 	assert retValue == 0

	# def test_28_phase_2_test_profile_04_on_emergency_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.test_profile_04_on_emergency_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.test_profile_04_on_emergency_profile')
	# 	assert retValue == 0
		
	# def test_28_phase_2_gsma_v4_23_emergency_profile_set_on_test_profile(self):
	# 	Constants.testLinkTestCase.append("test_28_phase_2.gsma_v4_23_emergency_profile_set_on_test_profile")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_28_phase_2.gsma_v4_23_emergency_profile_set_on_test_profile')
	# 	assert retValue == 0

	def test_98_euicc_features_dako_3800_enable_the_enabled_profile(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3800_enable_the_enabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3800_enable_the_enabled_profile')
		assert retValue == 0

	def test_98_euicc_features_dako_3802_disable_the_disabled_profile(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3802_disable_the_disabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3802_disable_the_disabled_profile')
		assert retValue == 0

	def test_98_euicc_features_dako_3805_fna_notif_and_retry(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3805_fna_notif_and_retry")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3805_fna_notif_and_retry')
		assert retValue == 0

	def test_98_euicc_features_dako_3959_rollback_after_enabling_profile_network_no_service(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3959_rollback_after_enabling_profile_network_no_service")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3959_rollback_after_enabling_profile_network_no_service')
		assert retValue == 0

	def test_98_euicc_features_dako_3960_fallback_after_enabling_profile_network_no_service(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3960_fallback_after_enabling_profile_network_no_service")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3960_fallback_after_enabling_profile_network_no_service')
		assert retValue == 0

	def test_98_euicc_features_dako_3961_fallback_after_network_connection_loss(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3961_fallback_after_network_connection_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3961_fallback_after_network_connection_loss')
		assert retValue == 0

	def test_98_euicc_features_dako_3962_download_apdu_profile_on_new_isdp(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3962_download_apdu_profile_on_new_isdp")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3962_download_apdu_profile_on_new_isdp')
		assert retValue == 0

	def test_98_euicc_features_dako_3967_download_saipv2_on_new_isdp(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3967_download_saipv2_on_new_isdp")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3967_download_saipv2_on_new_isdp')
		assert retValue == 0

	def test_98_euicc_features_dako_3976_activate_deactivate_frm(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3976_activate_deactivate_frm")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3976_activate_deactivate_frm')
		assert retValue == 0

	def test_98_euicc_features_dako_3977_activate_deactivate_apf(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_3977_activate_deactivate_apf")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_3977_activate_deactivate_apf')
		assert retValue == 0

	def test_98_euicc_features_dako_4165_allow_disallow_local_set_fallback(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4165_allow_disallow_local_set_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4165_allow_disallow_local_set_fallback')
		assert retValue == 0

	def test_98_euicc_features_dako_4437_nom_pos_001_era_glonass_local_swap(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4437_nom_pos_001_era_glonass_local_swap")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4437_nom_pos_001_era_glonass_local_swap')
		assert retValue == 0

	def test_98_euicc_features_dako_4438_nom_pos_004_fna_notif_with_imei(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4438_nom_pos_004_fna_notif_with_imei")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4438_nom_pos_004_fna_notif_with_imei')
		assert retValue == 0

	def test_98_euicc_features_dako_4439_nom_pos_005_profile_swap_notif_with_imei(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4439_nom_pos_005_profile_swap_notif_with_imei")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4439_nom_pos_005_profile_swap_notif_with_imei')
		assert retValue == 0

	def test_98_euicc_features_dako_4440_nom_neg_001_profile_swap_with_imei_handset_not_support_imei(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4440_nom_neg_001_profile_swap_with_imei_handset_not_support_imei")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4440_nom_neg_001_profile_swap_with_imei_handset_not_support_imei')
		assert retValue == 0

	def test_98_euicc_features_dako_4441_nom_pos_003_frm_config_update_counter_limit_via_unrecognized_envelope(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4441_nom_pos_003_frm_config_update_counter_limit_via_unrecognized_envelope")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4441_nom_pos_003_frm_config_update_counter_limit_via_unrecognized_envelope')
		assert retValue == 0

	def test_98_euicc_features_dako_4442_nom_pos_001_configure_setfallback_on_enabled_profile_only_via_store_data(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4442_nom_pos_001_configure_setfallback_on_enabled_profile_only_via_store_data")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4442_nom_pos_001_configure_setfallback_on_enabled_profile_only_via_store_data')
		assert retValue == 0

	def test_98_euicc_features_dako_4443_nom_pos_002_configure_setfallback_enabled_disabled_profile_via_store_data(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4443_nom_pos_002_configure_setfallback_enabled_disabled_profile_via_store_data")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4443_nom_pos_002_configure_setfallback_enabled_disabled_profile_via_store_data')
		assert retValue == 0

	def test_98_euicc_features_dako_4444_nom_pos_003_setfallback_disabled_profile_via_store_data(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4444_nom_pos_003_setfallback_disabled_profile_via_store_data")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4444_nom_pos_003_setfallback_disabled_profile_via_store_data')
		assert retValue == 0

	def test_98_euicc_features_dako_4493_audit_os_information(self):
		Constants.testLinkTestCase.append("test_98_euicc_features.dako_4493_audit_os_information")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_98_euicc_features.dako_4493_audit_os_information')
		assert retValue == 0

# 	# def test_98_euicc_features_(self):
# 	# 	Constants.testLinkTestCase.append("test_98_euicc_features.")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_98_euicc_features.')
# 	# 	assert retValue == 0

# 	# def test_98_euicc_features_(self):
# 	# 	Constants.testLinkTestCase.append("test_98_euicc_features.")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_98_euicc_features.')
# 	# 	assert retValue == 0

		
	def test_99_dako_issue_Dako_4462_Dakot_861(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.Dako_4462_Dakot_861")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.Dako_4462_Dakot_861')
		assert retValue == 0

	def test_99_dako_issue_Dako_4463_Dakot_851(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.Dako_4463_Dakot_851")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.Dako_4463_Dakot_851')
		assert retValue == 0

	def test_99_dako_issue_DAKOT_932(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.DAKOT_932")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.DAKOT_932')
		assert retValue == 0

	def test_99_dako_issue_dakot_962(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_962")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_962')
		assert retValue == 0		

	def test_99_dako_issue_DAKOT_968(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.DAKOT_968")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.DAKOT_968')
		assert retValue == 0

	def test_99_dako_issue_dakot_988_no_rollback_after_failed_enable_pcf01(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_988_no_rollback_after_failed_enable_pcf01")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_988_no_rollback_after_failed_enable_pcf01')
		assert retValue == 0		

	def test_99_dako_issue_Dakot_1035(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.Dakot_1035")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.Dakot_1035')
		assert retValue == 0

	def test_99_dako_issue_dakot_1046_location_plugin_send_notif_FC_after_LUS_normal_service(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1046_location_plugin_send_notif_FC_after_LUS_normal_service")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1046_location_plugin_send_notif_FC_after_LUS_normal_service')
		assert retValue == 0		
	
	def test_99_dako_issue_dakot_1052_fallback_instead_of_rollback(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1052_fallback_instead_of_rollback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1052_fallback_instead_of_rollback')
		assert retValue == 0	

	def test_99_dako_issue_dakot_1058_profile_switch_during_voice_call_refresh_rejected_no_retry(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1058_profile_switch_during_voice_call_refresh_rejected_no_retry")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1058_profile_switch_during_voice_call_refresh_rejected_no_retry')
		assert retValue == 0
		
	def test_99_dako_issue_DAKOT_1071(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.DAKOT_1071")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.DAKOT_1071')
		assert retValue == 0

# 	## commented on 20190110 since it's an OS limitation and not to be fixed until uncertentain time				
# 	# def test_99_dako_issue_Dakot_1077(self):
# 	# 	Constants.testLinkTestCase.append("test_99_dako_issue.Dakot_1077")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_99_dako_issue.Dakot_1077')
# 	# 	assert retValue == 0	
	
	def test_99_dako_issue_dakot_1080_trigger_auto_applet_via_toolkit_not_allowed_select_auto_applet(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1080_trigger_auto_applet_via_toolkit_not_allowed_select_auto_applet")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1080_trigger_auto_applet_via_toolkit_not_allowed_select_auto_applet')
		assert retValue == 0	

	# # Not valide on DK3.2 S17 due to remove OTA_POLLER applet
	# def test_99_dako_issue_dakot_1137_ongoing_proactive_command_no_refresh_after_disable_era_glonass(self):
	# 	Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1137_ongoing_proactive_command_no_refresh_after_disable_era_glonass")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_99_dako_issue.dakot_1080_trigger_auto_applet_via_toolkit_not_allowed_select_auto_applet')
	# 	assert retValue == 0	

# 	# # Not valide on DK3.2 S17 due to remove OTA_POLLER applet
# 	# def test_99_dako_issue_dakot_1137_ongoing_proactive_command_no_refresh_after_disable_era_glonass(self):
# 	# 	Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1137_ongoing_proactive_command_no_refresh_after_disable_era_glonass")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_99_dako_issue.dakot_1137_ongoing_proactive_command_no_refresh_after_disable_era_glonass')
# 	# 	assert retValue == 0	

	def test_99_dako_issue_dakot_1153_no_rollback_after_refresh(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1153_no_rollback_after_refresh")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1153_no_rollback_after_refresh')
		assert retValue == 0	

	def test_99_dako_issue_dakot_1202_status_counter_not_reset_after_handset_restart(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1202_status_counter_not_reset_after_handset_restart")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1202_status_counter_not_reset_after_handset_restart')
		assert retValue == 0	

	def test_99_dako_issue_dakot_1203_rollback_not_happened_if_fallback_profile_have_bad_connectivity(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1203_rollback_not_happened_if_fallback_profile_have_bad_connectivity")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1203_rollback_not_happened_if_fallback_profile_have_bad_connectivity')
		assert retValue == 0		

	def test_99_dako_issue_dakot_1203_v2_quectel_modem(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1203_v2_quectel_modem")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1203_v2_quectel_modem')
		assert retValue == 0

#     # # commented due to remove OTA POLLER on Dakota 3.2 S17
# 	# def test_99_dako_issue_dakot_1231_open_channel_via_unrecognized_env_http_param_fail(self):
# 	# 	Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1231_open_channel_via_unrecognized_env_http_param_fail")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_99_dako_issue.dakot_1231_open_channel_via_unrecognized_env_http_param_fail')
# 	# 	assert retValue == 0

	def test_99_dako_issue_dakot_1327_after_rollback_card_enable_saip_with_wrong_ki_by_itself(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1327_after_rollback_card_enable_saip_with_wrong_ki_by_itself")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1327_after_rollback_card_enable_saip_with_wrong_ki_by_itself')
		assert retValue == 0

# 	# # commented due to remove OTA POLLER on Dakota 3.2 S17
# 	# def test_99_dako_issue_dakot_1363_coverage_test_apf_off(self):
# 	# 	Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1363_coverage_test_apf_off")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_99_dako_issue.dakot_1363_coverage_test_apf_off')
# 	# 	assert retValue == 0	

# 	# #commented on 20190110 since it's passed on the coverege test with APF OFF
# 	# def test_99_dako_issue_dakot_1363(self):
# 	# 	Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1363")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_99_dako_issue.dakot_1363')
# 	# 	assert retValue == 0	

	def test_99_dako_issue_dakot_1399(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1399")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1399')
		assert retValue == 0	

	def test_99_dako_issue_dakot_1398_FC_notif_sent_3x(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1398_FC_notif_sent_3x")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1398_FC_notif_sent_3x')
		assert retValue == 0	

	def test_99_dako_issue_dakot_1401_auto_applet_not_triggered_by_envelope_if_applet_already_selected(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1401_auto_applet_not_triggered_by_envelope_if_applet_already_selected")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1401_auto_applet_not_triggered_by_envelope_if_applet_already_selected')
		assert retValue == 0	

	# def test_99_dako_issue_dakot_1403_destructive(self):
	# 	Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1403_destructive")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_99_dako_issue.dakot_1401_auto_applet_not_triggered_by_envelope_if_applet_already_selected')
	# 	assert retValue == 0	

# 	# def test_99_dako_issue_dakot_1403_destructive(self):
# 	# 	Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1403_destructive")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_99_dako_issue.dakot_1403_destructive')
# 	# 	assert retValue == 0	

	def test_99_dako_issue_dakot_1409(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1409")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1409')
		assert retValue == 0	
	
	def test_99_dako_issue_dakot_1410_does_fallback_when_lost_network_during_LM(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1410_does_fallback_when_lost_network_during_LM")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1410_does_fallback_when_lost_network_during_LM')
		assert retValue == 0	
	
	def test_99_dako_issue_dakot_1504_card_not_rollback_when_device_reboot_after_notif_retry(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_1504_card_not_rollback_when_device_reboot_after_notif_retry")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_1504_card_not_rollback_when_device_reboot_after_notif_retry')
		assert retValue == 0

	def test_99_diot_issues_diot_168_SCP81_rfm_targeting_instance_aid_rfm_fail(self):
		Constants.testLinkTestCase.append("test_99_diot_issues.diot_168")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_diot_issues.diot_168')
		assert retValue == 0

	def test_99_dakot_smsr_change(self):
		Constants.testLinkTestCase.append("test_99_dako_issue.dakot_smsr_change")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_99_dako_issue.dakot_smsr_change')
		assert retValue == 0

	def test_final_report_to_mail(self):
		# print('wkwk')
		# print('hahaha')
		with open(os.path.abspath("C:/Temp/execute_all_jenkins_dakota_34.otl"),'r') as f:
			# print(f.read())
			logs = f.read()
		split_logs = logs.split("==============================================================================")
		i=0
		error = 0
		normal = 0
		failed_test=[]
		for i in range(len(split_logs)):
			if "Error" in split_logs[i]:
				error+=1
				test_name = split_logs[i].split("------------------------------------------------------------------------------")
				failed_test.append(test_name[1].replace(" ",""))
			elif "NORMAL" in split_logs[i]:
				normal+=1
		
		# print ("Total failed test cases : {}".format(error))
		i=0
		failed_test_names = ""
		for i in range(error):
			failed_test_names = failed_test_names + failed_test[i].replace("\n","") + "\n"

		#generate  re-test
		re_test = ""
		for i in range(error):
			template = ("" +
			"def test_retest_{}(self):\n"+
			"\tConstants.testLinkTestCase.append('{}')\n" +
			"\tConstants.testLinkCountRunAll+=1\n" +
			"\tretValue = Run_ModuleMain('{}')\n" +
			"\tassert retValue == 0\n").format(i, failed_test[i].replace("\n",""), failed_test[i].replace("\n",""))

			re_test = re_test + template



		to_be_send = ("" + 
		"This is auto-generated email, please do not reply. \n\n"+
		"Pre-integration test summary:\n"+
		"Total executed test: {} \n"+
		"Total failed test: {} \n"+
		"Total passed test: {} \n\n"+
		"Summary of the failed test: \n{} \n\n" +
		"re-test : \n{}").format(normal+error,error,normal,failed_test_names, re_test)
		print(to_be_send)

		recipient = "tedy.putranggono@idemia.com"
		cc 		  = "novella.sitanggang@idemia.com"
		subject = Constants.TESTLINK_TEST_PLAN + " " + Constants.TESTLINK_BUILD

		# webbrowser.open_new('mailto:'+recipient+'?subject='+subject+'&cc='+cc+'&body='+to_be_send.replace("\n","%0D"))
		import smtplib
		from email.mime.text import MIMEText

		
		import base64
		# print (base64.b64encode("Bismillah#07")) #Your Password here
		# print (base64.b64decode("UEBzc3cwcmR3dms0ODk3"))

		mailserver = smtplib.SMTP('smtprelay1.emea.oberthurcs.com',25)
		mailserver.ehlo()
		mailserver.starttls()
		mailserver.login('muhamfir@oberthurcs.com', 'Bismillah#07')
		mailserver.sendmail('hasbi.firmansyah@idemia.com',['hasbif.firmansyah39@gmail.com'],to_be_send)
		mailserver.quit()

# 	# def test_99_dako_issue_(self):
# 	# 	Constants.testLinkTestCase.append("test_99_dako_issue.")
# 	# 	Constants.testLinkCountRunAll+=1
# 	# 	retValue = Run_ModuleMain('test_99_dako_issue.')
# 	# 	assert retValue == 0	

if __name__ == '__main__':
	unittest.main()
	