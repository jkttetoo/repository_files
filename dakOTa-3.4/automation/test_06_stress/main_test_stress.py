import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


EUICC 	= model.Euicc()
SERVER	= model.Smsr()


displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())
SetCurrentReader('Dell Smart Card Reader Keyboard 0')


'''
1. Set fallback attribut on profile 10
2. FRM status ON

Profile status
=====================================
1. Profile 11 state DISABLED(FALLBACK)
2. Profile 10 state ENABLED
=====================================

'''

Constants.AID_ISDP				= 'A0000005591010FFFFFFFF8900001500'
Constants.DEFAULT_ISDP10		= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11		= 'A0000005591010FFFFFFFF8900001100'
Constants.SAIP_TEST				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.ITERATIONS_STRESS		= 2
Constants.RESULTS 				= {
									'test_name' 		: ['Test name'],
									'test_result'		: ['Test result']
									}
Constants.TESTLINK_REPORT		= False
Constants.TESTLINK_DEVKEY		= 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL			= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME	= 'DakOTa v3.1'
Constants.TESTLINK_BUILD		= 'DakOTa v3.2 Sprint 2'
Constants.TESTLINK_PLATFORM		= 'IO222'
Constants.TESTLINK_TEST_PLAN	= 'eUICC Pre-integration'
Constants.TESTLINK_TESTER 		= 'm.firmansyah@oberthur.com'




from test_case.automation.test_06_stress import download_delete_status
from test_case.automation.test_06_stress import download_delete_without_status
from test_case.automation.test_06_stress import enable_profile_disable_profile
from test_case.automation.test_06_stress import enable_rollback_network_loss_disable_profile
from test_case.automation.test_06_stress import enable_profile_fallback_network_loss

suite = unittest.TestSuite()

EUICC.init()
# suite.addTest(download_delete_status.suite())
# suite.addTest(download_delete_without_status.suite())
# suite.addTest(enable_profile_disable_profile.suite())
suite.addTest(enable_rollback_network_loss_disable_profile.suite())
suite.addTest(enable_profile_fallback_network_loss.suite())


unittest.TextTestRunner().run(suite)
pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )