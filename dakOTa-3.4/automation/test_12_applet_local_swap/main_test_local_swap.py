import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


EUICC 		= model.Euicc()
SERVER		= model.Smsr()
TL_SERVER 	= model.TestlinkServer()


displayAPDU(True)
SetLogLevel('info')
SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')


''' TESTLINK VARIABLES '''
Constants.TESTLINK_REPORT			= False
Constants.TESTLINK_DEVKEY			= 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL				= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME		= 'DakOTa v3.1'
Constants.TESTLINK_BUILD			= 'DakOTa v3.2 Sprint 4'
Constants.TESTLINK_PLATFORM			= 'IO222'
Constants.TESTLINK_TEST_PLAN		= 'eUICC Pre-integration'
Constants.TESTLINK_TESTER 			= 'm.firmansyah@oberthur.com'


'''
TEST VARIABLES

To launch the test, prepare the following variables : 

Constants.AID_ISDP 			: It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
Constants.DEFAULT_ISDP10	: It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
Constants.DEFAULT_ISDP11	: It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
Constants.DEFAULT_ISDP12	: It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 

'''
Constants.AID_ISDP 					= 'A0000005591010FFFFFFFF8900001500'
Constants.DEFAULT_ISDP10			= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11			= 'A0000005591010FFFFFFFF8900001100'
Constants.DEFAULT_ISDP12			= 'A0000005591010FFFFFFFF8900001200'
Constants.DEFAULT_ISDP13			= 'A0000005591010FFFFFFFF8900001300'
Constants.DEFAULT_ISDP14			= 'A0000005591010FFFFFFFF8900001400'
Constants.WRONG_ISDP				= 'A0000005591010FFFFFFFF8900001101'
Constants.SAIP_TEST 				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_HSM					= 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
Constants.SAIP_PCF00				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
Constants.ITERATIONS_PERFORMANCE	= 1
Constants.ITERATIONS_STRESS			= 1
Constants.RESULTS 					= {
										'test_name' 		: ['Test name'],
										'test_result'		: ['Test result']
										}

'''
PRE-CONDITION:

	1. Set fallback attribut on profile 11
	2. FRM status ON
	3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 10 state PERSONALIZED     --> Pre-created ISDP
4. Profile 10 state PERSONALIZED	 --> Pre-created ISDP
5. Profile 10 state PERSONALIZED	 --> Pre-created ISDP

=====================================

'''

from test_case.automation.test_12_applet_local_swap import nom_pos_001_set_factory_profile_to_enabled_profile
from test_case.automation.test_12_applet_local_swap import nom_pos_002_set_factory_profile_to_disabled_profile
from test_case.automation.test_12_applet_local_swap import nom_pos_003_change_to_factory_profile
from test_case.automation.test_12_applet_local_swap import nom_pos_004_change_to_skt_profile
from test_case.automation.test_12_applet_local_swap import nom_neg_001_fallback_prohibited_when_switch_factory_profile_active
from test_case.automation.test_12_applet_local_swap import nom_neg_002_rollback_prohibited_when_switch_factory_profile_active
from test_case.automation.test_12_applet_local_swap import nom_neg_003_fallback_prohibited_when_switch_skt_profile_active
from test_case.automation.test_12_applet_local_swap import nom_neg_004_rollback_prohibited_when_switch_skt_profile_active
from test_case.automation.test_12_applet_local_swap import nom_neg_005_factory_profile_cant_be_enabled_via_gsma

suite = unittest.TestSuite()

suite.addTest(nom_pos_001_set_factory_profile_to_enabled_profile.suite())
suite.addTest(nom_pos_002_set_factory_profile_to_disabled_profile.suite())
suite.addTest(nom_pos_003_change_to_factory_profile.suite())
suite.addTest(nom_pos_004_change_to_skt_profile.suite())
suite.addTest(nom_neg_001_fallback_prohibited_when_switch_factory_profile_active.suite())
suite.addTest(nom_neg_002_rollback_prohibited_when_switch_factory_profile_active.suite())
suite.addTest(nom_neg_003_fallback_prohibited_when_switch_skt_profile_active.suite())
suite.addTest(nom_neg_004_rollback_prohibited_when_switch_skt_profile_active.suite())
suite.addTest(nom_neg_005_factory_profile_cant_be_enabled_via_gsma.suite())


unittest.TextTestRunner().run(suite)
pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )