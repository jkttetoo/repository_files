import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
    sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

SetCurrentReader('Dell Smart Card Reader Keyboard 0')

if Constants.displayAPDU != None :
            displayAPDU(Constants.displayAPDU)

try:
    if Constants.RunAll == True:
        SERVER                  = model.Smsr()
        LPin                    = model.LocationPlugin()
        EUICC                   = model.Euicc()
        GSMA                    = model.Gsma()
        DEVICE                  = model.Device()
        TL_SERVER               = model.TestlinkServer()

        EXTERNAL_ID             = 'DK-4315'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None
    else:
        SERVER                  = model.Smsr()
        LPin                    = model.LocationPlugin()
        EUICC                   = model.Euicc()
        GSMA                    = model.Gsma()
        DEVICE                  = model.Device()
        TL_SERVER               = model.TestlinkServer()

        EXTERNAL_ID             = 'DK-4315'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None

        displayAPDU(True)
        SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

        ''' TESTLINK VARIABLES '''
        Constants.TESTLINK_REPORT           = False
        Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
        Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
        Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
        Constants.TESTLINK_BUILD            = 'build_for_test'
        Constants.TESTLINK_PLATFORM         = 'IO222'
        Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
        Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

        '''
        TEST VARIABLES

        To launch the test, prepare the following variables :

        Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
        Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
        Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
        Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

        '''
        Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
        Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
        Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
        Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
        Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
        Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
        Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
        Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
        Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
        Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
        Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
        Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
        Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
        Constants.ITERATIONS_PERFORMANCE    = 1
        Constants.ITERATIONS_STRESS         = 1
        Constants.RESULTS                   = {
                                                'test_name'         : ['Test name'],
                                                'test_result'        : ['Test result']
                                                }
        Constants.case                      = 0
except:
    SERVER                  = model.Smsr()
    LPin                    = model.LocationPlugin()
    EUICC                   = model.Euicc()
    GSMA                    = model.Gsma()
    DEVICE                  = model.Device()
    # TL_SERVER               = model.TestlinkServer()

    EXTERNAL_ID             = 'DK-4315'
    EXECUTION_STATUS        = None
    EXECUTION_DURATION      = None

    displayAPDU(True)
    SetLogLevel('info')
    SetCurrentReader('Dell Smart Card Reader Keyboard 0')

    ''' TESTLINK VARIABLES '''
    Constants.TESTLINK_REPORT           = False
    Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
    Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
    Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
    Constants.TESTLINK_BUILD            = 'build_for_test'
    Constants.TESTLINK_PLATFORM         = 'IO222'
    Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
    Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

    '''
    TEST VARIABLES

    To launch the test, prepare the following variables :

    Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
    Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
    Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
    Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

    '''
    Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
    Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
    Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
    Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
    Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
    Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
    Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
    Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
    Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
    Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
    Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
    Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
    Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
    Constants.SAIP_1059                 = 'saip_dakot_1059.txt'
    Constants.ITERATIONS_PERFORMANCE    = 1
    Constants.ITERATIONS_STRESS         = 1
    Constants.RESULTS                   = {
                                            'test_name'         : ['Test name'],
                                            'test_result'        : ['Test result']
                                            }
    Constants.case                      = 0


class dakot1072(OtTestUnit):

    def report(self, execution_status, execduration):
        LogInfo('Start reporting test result...')
        TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
        LogInfo('Finished')

    def setUp(self):
        pprint.h1('dakot-1072')     
        # EUICC.init()
        # SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=80, apdu_format='indefinite')
        # SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
        # SERVER.enable_profile(Constants.AID_ISDP, scp=80, apdu_format='indefinite', network_service=False)
        # capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')

        # LogInfo('>>> fetch refresh \n')
        # pprint.log("euicc", "refresh", "DEVICE.fetch_one", Constants.debugMode)
        # data, sw = DEVICE.fetch_one(sw, 'refresh')
        # if sw != '9000':
        #     sw = DEVICE.fetch_all(sw)


        EUICC.init()
        data, sw = DEVICE.envelope_location_status()
        
        if sw!='9000':
            data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
            hahaha
            if last_proactive_cmd == 'open channel':
                capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
            elif last_proactive_cmd == 'send sms':
                seq_number = pprint.pprint_sms_first_notif(data)
                data, sw, cmd_type = DEVICE.fetch_all(sw)
                LogInfo('server confirms reception of euicc notification')
                capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))


        while '9000' in sw:
            data, sw = DEVICE.get_status()

        if sw == '910B':
            data, sw = DEVICE.fetch_one(sw)
            # LogInfo("loci_change")
            # data, sw = Fetch(sw[2:4])
            # sw = TerminalResponse('81 03 01 26 00 82 02 82 81 83 01 00 13 0712F89041235640')
        else:
            pass

        if sw!='9000':
            data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
            if last_proactive_cmd == 'open channel':
                capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
            elif last_proactive_cmd == 'send sms':
                seq_number = pprint.pprint_sms_first_notif(data)
                data, sw, cmd_type = DEVICE.fetch_all(sw)
                LogInfo('server confirms reception of euicc notification')


        LogInfo("============UPDATE by SM-SR ============")
        data, sw = LPin.location_plugin_update_configuration(interface='smsr', detection_condition='mcc', triggering_condition='detection only', scp=80, apdu_format='definite')
        # LogInfo("============UPDATE by applet selection ============")
        # data, sw = LPin.location_plugin_update_configuration(interface='applet', detection_condition='mcc', triggering_condition='detection only', scp=80, apdu_format='definite')
            
        if sw!='9000':
            data, sw = DEVICE.fetch_all(sw)



        for i in range(2):
            LogInfo("============INIT============")
            EUICC.init()
            data, sw = DEVICE.envelope_location_status()
            
            if sw!='9000':
                data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
                if last_proactive_cmd == 'open channel':
                    capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
                elif last_proactive_cmd == 'send sms':
                    seq_number = pprint.pprint_sms_first_notif(data)
                    data, sw, cmd_type = DEVICE.fetch_all(sw)
                    LogInfo('server confirms reception of euicc notification')
                    capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))


            while '9000' in sw:
                data, sw = DEVICE.get_status()

            data, sw = DEVICE.fetch_one(sw)

            if sw!='9000':
                data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
                if last_proactive_cmd == 'open channel':
                    capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
                elif last_proactive_cmd == 'send sms':
                    seq_number = pprint.pprint_sms_first_notif(data)
                    data, sw, cmd_type = DEVICE.fetch_all(sw)
                    LogInfo('server confirms reception of euicc notification')
        

    def test_dakot1072(self):
        # SetLogLevel('debug') 
        # h2("testbody")
        # LogInfo('body part')
        # timer_start = time()
        # EUICC.init()
        # capdu, rapdu, sw = EUICC.mno.update_pol1(aid_isdp=Constants.AID_ISDP, pol1_value="00", scp=80, apdu_format='indefinite')
        # timer_end = time()
        # EXECUTION_DURATION = timer_end - timer_start
        pass


    def tearDown(self):
        EUICC.init()
        SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
        SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='indefinite')
        DEVICE.get_status()

if __name__ == "__main__":

    from test import support
    Utilities.launchTest(dakot1072)

