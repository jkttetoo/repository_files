from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time



__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_POLLER				= model.OtaPoller()

		EXTERNAL_ID             = 'DK-'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_POLLER				= model.OtaPoller()

		EXTERNAL_ID             = 'DK-'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_for_test'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16                  = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	OTA_POLLER				= model.OtaPoller()

	EXTERNAL_ID             = 'DK-'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('Dell Smart Card Reader Keyboard 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Dakot 1363"
# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'
expectedAuditList = "A0000005591010FFFFFFFF89000016009F70013F"
class Dakot1363(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None	
	dakot1363Saip = "dakot_1035_saip.txt"
	# dakot1363Saip = "saipv2_SD_NOPIN_SCP81_notoken_Expanded.der"

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)          

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			EUICC.init()
			# capdu, rapdu, sw = SERVER.apf_off(80)
			SERVER.apf_on(80)
			SERVER.frm_on(80)
			h3("disable OTA poller applet")
			data, sw = OTA_POLLER.set_applet_config(interface='applet',applet_state=False, profile_download_event=True, retry_duration=60, retry_number=2)
			EUICC.init()
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=self.dakot1363Saip)
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP16, scp=81, apdu_format='definite')
			SERVER.download_profile(Constants.AID_ISDP16, saipv2_txt=Constants.SAIP_TEST)
			h3("enable profile 16")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP16), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))	
		except:
			exceptionTraceback(position = 1)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_dakot_1363(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			h3("audit list before enable profile")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 81, apdu_format='definite')
			LogInfo("capdu: "+str(capdu))
			LogInfo("rapdu: "+str(rapdu))
			LogInfo("sw   : "+str(sw))
			
			h3("euicc init")
			EUICC.init()

			h3("enable profile with no line saip")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='definite', chunk='01')
			LogInfo("capdu: "+str(capdu))
			LogInfo("rapdu: "+str(rapdu))
			LogInfo("sw   : "+str(sw))
			h3("enable profile done")
			if '9000' in rapdu:				
				h3("refresh")
				LogInfo('>>> fetch refresh \n')
				data, sw = DEVICE.fetch_one(sw, 'refresh')
				if sw != '9000':
					sw = DEVICE.fetch_all(sw)				
			
			h3("audit list after enable profile")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 81, apdu_format='definite')
			LogInfo("rapdu: "+str(rapdu))
			
			h3("euicc init")			
			PowerOn()
			sw = DEVICE.terminal_profile()
			
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['open channel', 'send sms'], catch_first_notif=False, loci_change=False)
			LogInfo("data: "+str(data))
			LogInfo("sw: "+str(sw))
			LogInfo("last_proactive_cmd: "+str(last_proactive_cmd))

			if sw!= "9000":
				DEVICE.fetch_all(sw)
			
			h3("get notification by sending get status 10 times")
			i = 0
			for i in range (10):
				LogInfo("get status: "+str(i+1))
				data, sw = DEVICE.get_status()

			h3("fetch & TR notification")
			if sw != '9000':
				# sw = DEVICE.fetch_all(sw)
				h3("fetch all")
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], open_channel_failed=True)
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['timer management'], open_channel_failed=True)
				data, sw = DEVICE.envelope_timer_expiration(id='psm')
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], open_channel_failed=True)
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['timer management'], open_channel_failed=True)
				data, sw = DEVICE.envelope_timer_expiration(id='psm')
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], open_channel_failed=True)
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['timer management'], open_channel_failed=True)
				data, sw = DEVICE.envelope_timer_expiration(id='psm')
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], open_channel_failed=True)
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['timer management'], open_channel_failed=True)

			h3("send lus no service 13x")			
			j = 0
			for j in range (13):
				LogInfo("lus no service: "+str(j+1))
				data, sw = DEVICE.envelope_location_status('no service')			

			if sw != "9000":							
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['refresh', 'timer management'])
				if last_proactive_cmd == 'refresh': #expected flow must refresh
					EUICC.init()
				elif last_proactive_cmd == "timer management": #wrong behaviour
					data, sw = DEVICE.envelope_timer_expiration(id='psm')
					data, sw = DEVICE.fetch_one(sw, 'refresh')
					if sw != '9000':
						sw = DEVICE.fetch_all(sw)	
					EUICC.init()			

			h3("audit list after negative event get status and no service 13x")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 81, apdu_format='definite')

			if expectedAuditList in rapdu:
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:	
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
				LogInfo("RESULT MUST BE ROLLBACK TO PROFILE 16 INSTEAD OF PROFILE 15 OR PROFILE 11 (FALLBACK PROFILE)")
			
		except:
			exceptionTraceback(position = 2)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start
#        self.assertEqual(Constants.RESULTS['test_result'][Constants.case], 'OK')
		# LogInfo('Constants.RESULTS table')
		# pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')


	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
			SERVER.apf_on(80)
			SERVER.frm_on(80)
			data, sw = OTA_POLLER.set_applet_config(interface='applet',applet_state=True, profile_download_event=True, retry_duration=60, retry_number=2)
			EUICC.init()
			h3("enable profile 11")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))	

			# SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
			SERVER.delete_profile(Constants.AID_ISDP16, scp=80, apdu_format='definite')
			h3("audit list")			
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 81, apdu_format='definite')
			DEVICE.get_status()
			EUICC.init()
			OTA_POLLER.set_default_config()
		except:
			exceptionTraceback(position = 3)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		# if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(Dakot1363)