from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		OTA						= model.SCP80()
		AMDB 					= model.SCP81()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
		OTA_POLLER				= model.OtaPoller()
		GP 			= model.Gp()

		EXTERNAL_ID             = 'DK-4404'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		OTA						= model.SCP80()
		AMDB 					= model.SCP81()
		DEVICE                  = model.Device()
		OTA_POLLER				= model.OtaPoller()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
		GP 			= model.Gp()

		EXTERNAL_ID             = 'DK-4404'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('debug')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		# Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_E2E 					= 'HTTPs_Aspider_SAIPv2.1_Dkt3.x_Elisa_new_re.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
		# Constants.debugMode = True
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	OTA						= model.SCP80()
	AMDB 					= model.SCP81()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	INGENICO                = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID             = 'DK-4404'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('debug')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0


testName = "SM-SR Change"
class SMSRChange(OtTestUnit):

	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		pprint.h1('DAKOT-1236')
		EUICC.init()
		pass
	
	def test_smsr_change(self):
		LogInfo('body part')
		EUICC.init()
		timer_start = time()
		try:
			# h1('Install4Install')		
			# capdu, rapdu, sw = SERVER.ram(GSMA.install4install(Constants.AID_ISDP), 80, apdu_format='indefinite')
			h1('store dp cert')	
			capdu, rapdu, sw = SERVER.ram([GSMA.install4perso("A0000005591010FFFFFFFF8900000100"), GSMA.store_dp_certificate], 81, apdu_format='indefinite')
			# capdu, rapdu, sw = SERVER.ram(GSMA.store_dp_certificate, 81, apdu_format='indefinite')
			random_challenge = rapdu[rapdu.find('8510')+4:rapdu.find('8510')+36]
			LogInfo("random challenge: {}".format(random_challenge))

			h1('Store new key')
			capdu, rapdu, sw = SERVER.ram(GSMA.store_isdp_keys(SERVER.euicc.isdr.sd, random_challenge), 81, apdu_format='indefinite')
			receipt = rapdu[ rapdu.find('8610') + 4 : rapdu.find('8610')+36 ]
			LogInfo("receipt: {}".format(receipt))

			if GSMA.verify_receipt(receipt):
				print('Receipt match')
			else:
				print('Receipt not match')

			h1('Finalise ISDR Handover')
			capdu, rapdu, sw = SERVER.ram(GSMA.finalise_isdr_handover(keyset='01', keyrange='0103'), 80, apdu_format='indefinite')

			h2('Update CASD')
			capdu, rapdu, sw = SERVER.ram("80E280006000B919B90F9501888001B0810141820105830174B9068001F08501000036410438A2F013183D2BFFA1C44CE5E9A9FFEAA55C90CC1980BE05DDB846EB4999E37EE01A2E1AB72DCF389E42B3E213ECA1E71C79B6E5DE4760FF69C59FAAAECC5C8A", 80, apdu_format='indefinite')
			

			h2('Putkey SCP81')
			capdu, rapdu, sw = SERVER.ram("80D800812E41851110A64EBFEDA5FB415F3F2C8193CB3066D503F21F3C88102F76B86A492CECAFB0492B72BDB79821030B172E", 80, apdu_format='indefinite')
			
			
			
			
			capdu, rapdu, sw = SERVER.ram(GSMA.put_key_scp(key='TLS-PSK', sEnc = '60F76381159ED922DD8FCF1D21C7B69E', sMac = '4C26903C311128079C4D2146D870FDA7', sKek = 'AF539B65D4181E0412501DB20BBE1272'), 80, apdu_format='indefinite')
			capdu, rapdu, sw = SERVER.ram("80D800813F4185212090D02E46103F6D6CD2BF176130CE99500CFA82FBEB647AA29596B760C38F04380303ED9F881110D6407970C213F1D2806F540FA5B60C99038D986900", 80, apdu_format='indefinite')
			# EUICC.init()
			# data, sw = OTA_POLLER.set_applet_config(applet_state=True)
			# EUICC.init()
			# capdu, data, sw = OTA_POLLER.trigger_open_channel_via_unrecognized_envelope()
			# data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
			# if last_proactive_cmd == 'open channel':
			# 	if '91' in sw:
			# 		AMDB.remote_management(GP.audit_applet_list, pull=sw, apdu_format='definite')



			if '9000' in rapdu:
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
			else:	
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP ), 81, apdu_format='definite', chunk='01')
			if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
			timer_end = time()
			EXECUTION_DURATION = timer_end - timer_start

		except:
			exceptionTraceback(position = 2)     
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		LogInfo('body part')

	def tearDown(self):
		EUICC.init()
		SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
		SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')


if __name__ == "__main__":

	from test import support
	Utilities.launchTest(SMSRChange)
