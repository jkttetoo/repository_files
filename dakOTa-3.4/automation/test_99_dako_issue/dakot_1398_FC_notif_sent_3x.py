import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LPin   					= model.LocationPlugin()

		EXTERNAL_ID             = 'DK-4404'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LPin   					= model.LocationPlugin()

		EXTERNAL_ID             = 'DK-4404'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		# Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_E2E 					= 'HTTPs_Aspider_SAIPv2.1_Dkt3.x_Elisa_new_re.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
		# Constants.debugMode = True
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	INGENICO                = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID             = 'DK-4404'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

Constants.download_status_assert = ""
testName = "Dakot 1327"
# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'
maxCounterNotifFC = 3
counterNotifFC = 0

class Dakot1398(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			# EUICC.init(catch_first_notif=True, send_server_confirmation=False, loci_change=True)
			EUICC.init()
			create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			download_time, download_status, file_size = SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_E2E)
			capdu, rapdu, sw = SERVER.apf_on(80)
			capdu, rapdu, sw = SERVER.frm_on(80)
			data, sw = LPin.location_plugin_activation(interface='applet', scp=80, apdu_format='definite')
			data, sw = LPin.location_plugin_update_configuration(interface='applet', detection_condition='mcc', triggering_condition='power-on and detection', timer_interval=30, max_counter=5)
		except:
			exceptionTraceback(position = 1)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_dakot_1398(self):
		h2("testbody")
		LogInfo('body part')
		# EUICC.init()
		timer_start = time()
		try:
			h3("euicc init")
			EUICC.init()
			timer_start = time()
			counterNotifFC = 0
			h3("get status 5 times")
			for i in range(5):
				DEVICE.get_status()

			h3("enable profile "+Constants.AID_ISDP)
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='chunk')

			h3("refresh")
			data, sw = DEVICE.fetch_one(sw, 'refresh')
			if sw != '9000':
				sw = DEVICE.fetch_all(sw)
			
			h3("euicc init")
			EUICC.init()
			###### original version ##############
			# data, sw = DEVICE.get_status()
			# while sw == '9000':
			# 	data, sw = DEVICE.get_status()
			# data, sw = DEVICE.fetch_one(sw) #Expect PLI LOCI
			##########################################

			######### another version #############
			h3("send envelope location status")
			data, sw = DEVICE.envelope_location_status()
			######################################'
			
			h3("open channel")
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='open channel')
			if last_proactive_cmd == 'open channel':							
				capdu,rapdu,sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)				
			# elif last_proactive_cmd == 'send sms':
			# 	seq_number = pprint.pprint_sms_first_notif(data)
			# 	data, sw, cmd_type = device.fetch_all(sw)
			# 	LogInfo('server confirms reception of euicc notification')
			# 	capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))
			# 	counterNotifFC+=1
			
			# data, sw = DEVICE.envelope_network_rejection()
			h3("envelope location no service")
			data, sw = DEVICE.envelope_location_status(status='no service')
			data, sw = DEVICE.fetch_one(sw) #Expect Timer Mgmt

			h3("get status 10 times")
			for i in range(10):
				data, sw = DEVICE.get_status()

			h3("envelope location status")
			data, sw = DEVICE.envelope_location_status()
			data, sw = DEVICE.fetch_one(sw) #Expect Timer Mgmt
			h3("open channel")
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='open channel')

			if last_proactive_cmd == 'open channel':
				LogInfo("FC Notif send 1 times")								
				euicc_notification_sequence_number = EUICC.isdr.scp81.establish_tls_session(sw, interrupt=True)
				counterNotifFC+=1
			# elif last_proactive_cmd == 'send sms':
			# 	seq_number = pprint.pprint_sms_first_notif(data)
			# 	data, sw, cmd_type = device.fetch_all(sw)
			# 	LogInfo('server confirms reception of euicc notification')
			# 	capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))
			# 	counterNotifFC+=1

			h3("get status 7 times")
			for i in range(7):
				data, sw = DEVICE.get_status()
			if sw!='9000':
				h3("fetch timer management")
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='timer management')

			h3("send timer expiration biplink")
			data, sw = DEVICE.envelope_timer_expiration(id='biplink')

			if sw!='9000':
				h3("fetch open channel")
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='open channel')
				LogInfo("last_proactive_cmd: "+last_proactive_cmd)
				if last_proactive_cmd == 'open channel':
					LogInfo("FC Notif send 2 times")									
					capdu,rapdu,sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
					counterNotifFC+=1
				# elif last_proactive_cmd == 'send sms':
				# 	seq_number = pprint.pprint_sms_first_notif(data)
				# 	data, sw, cmd_type = device.fetch_all(sw)
				# 	LogInfo('server confirms reception of euicc notification')
				# 	capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))
				# 	counterNotifFC+=1

			if sw!='9000':
				h3("fetch open channel second times")				
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='open channel')
				if last_proactive_cmd == 'open channel':
					LogInfo("FC Notif send 3 times")	
					capdu,rapdu,sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
					counterNotifFC+=1
				# elif last_proactive_cmd == 'send sms':
				# 	seq_number = pprint.pprint_sms_first_notif(data)
				# 	data, sw, cmd_type = device.fetch_all(sw)
				# 	LogInfo('server confirms reception of euicc notification')
				# 	capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))
				# 	# counterNotifFC+=1

			LogInfo("counterNotifFC: "+str(counterNotifFC))
			LogInfo("maxCounterNotifFC: "+str(maxCounterNotifFC))

			if(counterNotifFC<maxCounterNotifFC):
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)	
			else:
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
			timer_end = time()
			self.EXECUTION_DURATION = timer_end - timer_start
		except:
			exceptionTraceback(position = 2)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		LogInfo('body part')

	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.DEFAULT_ISDP11, scp=80, apdu_format='indefinite')
			SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')
			data, sw = LPin.location_plugin_deactivation(interface='applet', scp=80, apdu_format='definite')
		except:
			exceptionTraceback(position = 3)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		# if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(Dakot1398)
