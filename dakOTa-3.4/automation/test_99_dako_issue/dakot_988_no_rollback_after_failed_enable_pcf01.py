import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		LPin                       = model.LocationPlugin()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
		TL_SERVER               = model.TestlinkServer()

		EXTERNAL_ID             = 'DK-4430'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		LPin                       = model.LocationPlugin()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
		TL_SERVER               = model.TestlinkServer()

		EXTERNAL_ID             = 'DK-4430'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_1056_NO_POL          = 'ProfilePackageDescription_Basic_no_pin_5.txt'
		Constants.SAIP_976_POL_01           = '813_pol1_RC10_wrong_Ki_OPC_hex.txt'
		Constants.SAIP_1059_SMS             = 'saipv2_sms.der'
		Constants.SAIP_1071                 = 'HTTPs_Aspider_SAIPv2.1_Dkt3.x_Elisa_new_re.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	LPin                       = model.LocationPlugin()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4430'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('debug')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.SAIP_1056_NO_POL          = 'ProfilePackageDescription_Basic_no_pin_5.txt'
	Constants.SAIP_1059_SMS             = 'saipv2_sms.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Dakot 988"
# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'
class Dakot988(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None	

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			h3("get FNA")
			EUICC.init()    
			h3("create and download profile 15")
			create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			download_time, download_status, file_size = SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
			h3("create and download profile 16")
			create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP16, scp=81, apdu_format='definite')
			download_time, download_status, file_size = SERVER.download_profile(Constants.AID_ISDP16, saipv2_txt=Constants.SAIP_PCF01)
			h3('Enable Profile 15')
			capdu, rapdu, sw = SERVER.enable_profile(Constants.AID_ISDP, scp=81, network_service=False, apdu_format='definite')
			h3("set fallback attribute")
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.AID_ISDP, scp=80, apdu_format='indefinite')
		except:
			exceptionTraceback(position = 1)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_dakot_988(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			pprint.h1('Enable Profile 16')
			# capdu, rapdu, sw = SERVER.enable_profile(Constants.AID_ISDP16, scp=81, network_service=True, apdu_format='definite')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP16), 81, apdu_format='indefinite', chunk='01')
			if sw is not None:
				LogInfo('>>> fetch refresh \n')
				data, sw = DEVICE.fetch_one(sw, 'refresh')
				if sw != '9000':
					sw = DEVICE.fetch_all(sw)

			EUICC.init()
			for i in range(13):
				data, sw_loci = DEVICE.envelope_location_status('no service')
				if i == 0:
					if sw_loci != '9000':
						data, sw = DEVICE.fetch_one(sw)
						sw_loci='9000'              
			# self.refresh(sw_loci, no_service=False, unlimited=False)
			if sw_loci is not None:
				if sw_loci != '9000':
					LogInfo('>>> fetch refresh \n')
					data, sw = DEVICE.fetch_one(sw_loci, 'refresh')
					if sw != '9000':
						sw = DEVICE.fetch_all(sw)

			EUICC.init()
			h3("auid isdp enabled")
			capdu, rapdu, sw = SERVER.audit_isdp_enabled(scp=80)			
			LogInfo("rapdu: "+str(rapdu))
			if '1500' in rapdu:
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
		except:
			exceptionTraceback(position = 2)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')

	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.DEFAULT_ISDP11, scp=80, apdu_format='indefinite')

			EUICC.init()
			EUICC.mno.update_pol1(aid_isdp = Constants.AID_ISDP, pol1_value = '00', scp = 80, apdu_format = 'indefinite')
			
			SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.disable_profile(Constants.AID_ISDP16, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')
			SERVER.delete_profile(Constants.AID_ISDP16, scp=80, apdu_format='compact')
		except:
			exceptionTraceback(position = 3)          
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		# if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")
if __name__ == "__main__":

	from test import support
	Utilities.launchTest(Dakot988)