import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
    sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
    displayAPDU(Constants.displayAPDU)

try:
    if Constants.RunAll == True:
        SERVER                  = model.Smsr()
        EUICC                   = model.Euicc()
        LPin                       = model.LocationPlugin()
        GSMA                    = model.Gsma()
        DEVICE                  = model.Device()
        INGENICO                = model.IngenicoReader()
        LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
        TL_SERVER               = model.TestlinkServer()

        EXTERNAL_ID             = 'DK-4430'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None
    else:
        SERVER                  = model.Smsr()
        EUICC                   = model.Euicc()
        LPin                       = model.LocationPlugin()
        GSMA                    = model.Gsma()
        DEVICE                  = model.Device()
        INGENICO                = model.IngenicoReader()
        LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
        TL_SERVER               = model.TestlinkServer()

        EXTERNAL_ID             = 'DK-4430'
        EXECUTION_STATUS        = None
        EXECUTION_DURATION      = None

        displayAPDU(True)
        SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

        ''' TESTLINK VARIABLES '''
        Constants.TESTLINK_REPORT           = False
        Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
        Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
        Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
        Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
        Constants.TESTLINK_PLATFORM         = 'IO222'
        Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
        Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

        '''
        TEST VARIABLES

        To launch the test, prepare the following variables :

        Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
        Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
        Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
        Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

        '''
        Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
        Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
        Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
        Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
        Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
        Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
        Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
        Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
        Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
        Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
        Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
        Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
        Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
        Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
        Constants.SAIP_1056_NO_POL          = 'ProfilePackageDescription_Basic_no_pin_5.txt'
        Constants.SAIP_976_POL_01           = '813_pol1_RC10_wrong_Ki_OPC_hex.txt'
        Constants.SAIP_1059_SMS             = 'saipv2_sms.der'
        Constants.SAIP_1071                 = 'HTTPs_Aspider_SAIPv2.1_Dkt3.x_Elisa_new_re.der'
        Constants.ITERATIONS_PERFORMANCE    = 1
        Constants.ITERATIONS_STRESS         = 1
        Constants.RESULTS                   = {
                                                'test_name'         : ['Test name'],
                                                'test_result'        : ['Test result']
                                                }
        Constants.case                      = 0
except:
    SERVER                  = model.Smsr()
    EUICC                   = model.Euicc()
    LPin                       = model.LocationPlugin()
    GSMA                    = model.Gsma()
    DEVICE                  = model.Device()
    TL_SERVER               = model.TestlinkServer()

    EXTERNAL_ID             = 'DK-4430'
    EXECUTION_STATUS        = None
    EXECUTION_DURATION      = None

    displayAPDU(True)
    SetLogLevel('debug')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

    ''' TESTLINK VARIABLES '''
    Constants.TESTLINK_REPORT           = False
    Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
    Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
    Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
    Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
    Constants.TESTLINK_PLATFORM         = 'IO222'
    Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
    Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

    '''
    TEST VARIABLES

    To launch the test, prepare the following variables :

    Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
    Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
    Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
    Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

    '''
    Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
    Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
    Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
    Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
    Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
    Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
    Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
    Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
    Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
    Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
    Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
    Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
    Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
    Constants.SAIP_1056_NO_POL          = 'ProfilePackageDescription_Basic_no_pin_5.txt'
    Constants.SAIP_1059_SMS             = 'saipv2_sms.der'
    Constants.ITERATIONS_PERFORMANCE    = 1
    Constants.ITERATIONS_STRESS         = 1
    Constants.RESULTS                   = {
                                            'test_name'         : ['Test name'],
                                            'test_result'        : ['Test result']
                                            }
    Constants.case                      = 0

class AppletTriggered(OtTestUnit):

    def report(self, execution_status, execduration):
        LogInfo('Start reporting test result...')
        TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
        LogInfo('Finished')

    def setUp(self):
        pprint.h1('Get FNA')
        # EUICC.init(catch_first_notif=True, send_server_confirmation=False)    
        EUICC.init()
        create_isdp_and_establish_keyset_status = SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
        download_time, download_status, file_size = SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)

    def test_download_delete_without_status(self):
        LogInfo('body part')
        timer_start = time()
        i=0
        while i < 2:            
            pprint.h1('Enable Profile 15 iter #{}'.format(i+1))
            capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
            if '9000' in rapdu:
                if sw!='9000':
                    data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['refresh'])
            EUICC.init()
            j=0
            while j < 6:
                data, sw = DEVICE.get_status()
                if sw!='9000':
                    data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
                    if last_proactive_cmd == 'open channel':
                        EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
                    elif last_proactive_cmd == 'send sms':
                        seq_number = pprint.pprint_sms_first_notif(data)
                        data, sw, cmd_type = DEVICE.fetch_all(sw)
                        LogInfo('server confirms reception of euicc notification')
                        capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))
                j+=1

            data, sw = DEVICE.envelope_location_status()
            data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
            if last_proactive_cmd == 'open channel':
                EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
            elif last_proactive_cmd == 'send sms':
                seq_number = pprint.pprint_sms_first_notif(data)
                data, sw, cmd_type = DEVICE.fetch_all(sw)
                LogInfo('server confirms reception of euicc notification')
                capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))

            SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
            i+=1



        
        if j == 6:
            Constants.RESULTS['test_name'].append('DAKOT-1202')
            Constants.RESULTS['test_result'].append('OK')
            EXECUTION_STATUS = 'PASSED'
            Constants.case+=1
        else:
            Constants.RESULTS['test_name'].append('DAKOT-1202')
            Constants.RESULTS['test_result'].append('KO')
            EXECUTION_STATUS = 'FAILED'
            Constants.case+=1
        LogInfo('body part')
        
        # if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
        # if EXECUTION_STATUS != "PASSED":raise ValueError("TEST FAILED")

    def tearDown(self):
        displayAPDU(False)
        EUICC.init()
        SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')

if __name__ == "__main__":

    from test import support
    Utilities.launchTest(AppletTriggered)