import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

EUICC = model.Euicc()


displayAPDU(True)
SetLogLevel('info')
SetCurrentReader('eSIM 3.1.rc11')


'''
1. Set fallback attribut on profile 10
2. FRM status ON

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
=====================================

'''

Constants.AID_ISDP				= 'A0000005591010FFFFFFFF8900001500'
Constants.DEFAULT_ISDP11		= 'A0000005591010FFFFFFFF8900001100'
Constants.SAIP_TEST				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.RESULTS 				= {
									'test_name' 		: ['Test name'],
									'test_result'		: ['Test result']
									}
Constants.TESTLINK_REPORT		= False
Constants.TESTLINK_DEVKEY		= 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL			= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME	= 'DakOTa v3.1'
Constants.TESTLINK_BUILD		= 'DakOTa v3.2 Build4Test4'
Constants.TESTLINK_PLATFORM		= 'IO222'
Constants.TESTLINK_TEST_PLAN	= 'eUICC Pre-integration'
Constants.TESTLINK_TESTER 		= 'm.firmansyah@oberthur.com'



from test_case.automation.test_04_lm_applet import lm_applet_enable_profile_with_notif
from test_case.automation.test_04_lm_applet import lm_applet_disable_profile_with_notif
from test_case.automation.test_04_lm_applet import lm_applet_delete_profile_with_notif

from test_case.automation.test_04_lm_applet import lm_applet_enable_profile_without_notif
from test_case.automation.test_04_lm_applet import lm_applet_disable_profile_without_notif
from test_case.automation.test_04_lm_applet import lm_applet_delete_profile_without_notif

from test_case.automation.test_04_lm_applet import lm_applet_enable_the_enabled_profile
from test_case.automation.test_04_lm_applet import lm_applet_disable_the_disabled_profile
from test_case.automation.test_04_lm_applet import lm_applet_delete_the_enabled_profile

from test_case.automation.test_04_lm_applet import lm_applet_set_fallback
from test_case.automation.test_04_lm_applet import lm_applet_get_euicc_info
from test_case.automation.test_04_lm_applet import lm_applet_get_eid
from test_case.automation.test_04_lm_applet import lm_applet_get_applet_config
from test_case.automation.test_04_lm_applet import lm_applet_enable_notif
from test_case.automation.test_04_lm_applet import lm_applet_disable_notif
from test_case.automation.test_04_lm_applet import lm_applet_disallow_enable_profile
from test_case.automation.test_04_lm_applet import lm_applet_disallow_disable_profile
from test_case.automation.test_04_lm_applet import lm_applet_disallow_delete_profile
from test_case.automation.test_04_lm_applet import lm_applet_allow_enable_profile
from test_case.automation.test_04_lm_applet import lm_applet_allow_disable_profile
from test_case.automation.test_04_lm_applet import lm_applet_allow_delete_profile




suite = unittest.TestSuite()

EUICC.init()

# suite.addTest(lm_applet_enable_profile_with_notif.suite())
# suite.addTest(lm_applet_disable_profile_with_notif.suite())
# suite.addTest(lm_applet_delete_profile_with_notif.suite())

suite.addTest(lm_applet_enable_profile_without_notif.suite())
# suite.addTest(lm_applet_disable_profile_without_notif.suite())
# suite.addTest(lm_applet_delete_profile_without_notif.suite())

# suite.addTest(lm_applet_enable_the_enabled_profile.suite())
# suite.addTest(lm_applet_disable_the_disabled_profile.suite())
# suite.addTest(lm_applet_delete_the_enabled_profile.suite())

# suite.addTest(lm_applet_set_fallback.suite())
# suite.addTest(lm_applet_get_euicc_info.suite())
# suite.addTest(lm_applet_get_eid.suite())
# suite.addTest(lm_applet_get_applet_config.suite())
# suite.addTest(lm_applet_enable_notif.suite())
# suite.addTest(lm_applet_disable_notif.suite())
# suite.addTest(lm_applet_disallow_enable_profile.suite())
# suite.addTest(lm_applet_disallow_disable_profile.suite())
# suite.addTest(lm_applet_disallow_delete_profile.suite())
# suite.addTest(lm_applet_allow_enable_profile.suite())
# suite.addTest(lm_applet_allow_disable_profile.suite())
# suite.addTest(lm_applet_allow_delete_profile.suite())


unittest.TextTestRunner(verbosity=2).run(suite)
pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
