from Ot.PyCom import *
import sys
from Oberthur import *
import os

cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)

import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *

import unittest
import model
from time import time

__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'
'''
__date__ = 200180705
__purpose__= check set emergency profile on profile 15

'''

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO_SERVICE = model.AutoService()

		EXTERNAL_ID             = 'DK-4478'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
		#why its failed if I use SetLogLevel here?
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO_SERVICE = model.AutoService()
		GLONASS = model.EraGlonass()

		EXTERNAL_ID             = 'DK-4478'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
		Constants.AID_ISDP17                = 'A0000005591010FFFFFFFF8900001700'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4478'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

'''card'''
expData = "E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 11 00 9F 70 01 3F 53 01 01"+\
			"2C 0A 98 01 93 00 00 00 11 76 89 F0 EB 01 00"+\
			"E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 10 00 9F 70 01 1F 53 01 00"+\
			"2C 0A 98 12 31 08 00 17 57 01 00 71 EB 01 00"+\
			"E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 17 00 9F 70 01 1F 53 01 00"+\
			"2C 0A 98 01 93 00 00 00 11 86 10 F2 EB 01 01"

'''sprint 15'''
# expData = "E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 11 00 9F 70 01 3F 53 01 01"+\
# 			"2C 0A 98 01 93 00 00 00 11 76 89 F0 EB 01 00"+\
# 			"E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 10 00 9F 70 01 1F 53 01 00"+\
# 			"2C 0A 98 12 31 08 00 17 57 01 00 71 EB 01 01"+\
# 			"E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 17 00 9F 70 01 1F 53 01 00"+\
# 			"2C 0A 98 01 93 00 00 00 11 86 10 F2 EB 01 00"

'''sprint 13'''
# expData = "E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 11 00 9F 70 01 3F 53 01 01"+\
# 			"2C 0A 98 01 93 00 00 00 11 76 89 F0 EB 01 00"+\
# 			"E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 10 00 9F 70 01 1F 53 01 00"+\
# 			"2C 0A 98 12 31 08 00 17 57 01 00 71 EB 01 00"+\
# 			"E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 16 00 9F 70 01 1F 53 01 00"+\
# 			"2C 0A 98 01 93 00 00 00 11 86 10 F2 EB 01 00"+\
# 			"E3 28 4F 10 A0 00 00 05 59 10 10 FF FF FF FF 89 00 00 17 00 9F 70 01 1F 53 01 00"+\
# 			"2C 0A 98 01 93 00 00 00 11 86 10 F2 EB 01 01"
'''dll'''
testName = "auto service applet get euicc info"
# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'
class SetEmergencyProfile(OtTestUnit):
	result_test = None
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	
	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)	            

	def setUp(self):		
		pprint.h1('Create ISDP SCP80 definite mode')
		pprint.h2("setup")
		try:
			EUICC.init()
			pprint.h3("audit isdp list")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list[0:28]+ '00C0000000', 80, apdu_format='compact')
			if(Constants.AID_ISDP17 not in rapdu):
				pprint.h3("create isdp and establish keyset profile 17")
				SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP17, scp=80, apdu_format='definite')
				pprint.h3("download profile")
				SERVER.download_profile(Constants.AID_ISDP17, saipv2_txt=Constants.SAIP_TEST)
		except:
			exceptionTraceback(position = 1)	            
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_create_isdp(self):
		"""Auto service applet get euicc info.
		Desc: check set emergency profile on profile 15
		"""
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			EUICC.init()
			timer_start = time()
			pprint.h3("set emergency profile")
			data, sw = AUTO_SERVICE.setEmergencyProfile(Constants.AID_ISDP17)
			h3("get euicc info emergency profile")
			data, sw = AUTO_SERVICE.getEUICCInfo(expectedData=expData)
			if sw == "9000":
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
		except:
			exceptionTraceback(position = 2)	            
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)		

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start
#        self.assertEqual(Constants.RESULTS['test_result'][Constants.case], 'OK')
		# LogInfo('Constants.RESULTS table')
		# pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')		

	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
			pprint.h3("enable profile 11")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11 ), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu: 
				sw = SERVER.euicc.refresh(sw, False)		
				EUICC.init()

			SERVER.disable_profile(Constants.AID_ISDP17, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.delete_profile(Constants.AID_ISDP17, scp=80, apdu_format='definite')
			DEVICE.get_status()
		except:
			exceptionTraceback(position = 3)	            
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(SetEmergencyProfile)