'''
PSM Update SMSR SMS Parameters tag A2
'''

from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time



__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'

Options.counters={}

if Constants.displayAPDU != None :
			displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_SCP80 = model.SCP80()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_SCP80 = model.SCP80()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_for_test'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDR = "A0000005591010FFFFFFFF8900000100"
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4303'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDR 					= "A0000005591010FFFFFFFF89000010"
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0


from Ot.GlobalPlatform import *
import collections
from util import hexastring

'''GSMA SGP 3.2 p.104 - p.107 specification'''
# dataTagA5 = "A5" + lv(dnsParamTag81+dnsParamTagA2+dnsParamTagA3)
dnsParamTagA5 = "" #conditional proprietary parameters => DNS Param
bipParamTagA4 = "" #conditional proprietary parameters => BIP Param for CAT TP


'''ISDP identifier digits 15 to 20 (or byte 13 to 15) of PIX of ISDP: tag 81 under tag A2'''
'''A000000559 1010FFFFFFFF89 000015 00 => 000015'''
dataTag81UnderA2 = "000015"
# dataTag81UnderA2 = "001500"
smsParamTag81UnderA2 = "81" + lv(dataTag81UnderA2)
# smsParamTag81UnderA2 = "81" + lv("05 910280F0")

'''profile specific smsr platform destination address: tag 82 under tag A2'''
'''example: #102511193 => 23313032353131313933'''
dataTag82UnderA2 = "23313032353131313933"
# dataTag82UnderA2 = "05 910280F1"
smsParamTag82UnderA2 = "82" + lv(dataTag82UnderA2)

'''profile specific smsr platform destination addresses: tag A2'''
smsParamTagA2 = "A2" + lv(smsParamTag81UnderA2 + smsParamTag82UnderA2)

'''smsr platform destination address: tag 81'''
# '''example: 193#511#102 => 3139332335313123313032'''
'''example: #193511102 => 23313933353131313032'''
dataTag81 = "23313933353131313032"
# dataTag81 = "05 910280F0"
smsParamTag81 = "81" + lv(dataTag81)

'''sms parameters: tag A3'''
smsParamTagA3 = "A3" + lv(smsParamTag81 + smsParamTagA2) #conditional proprietary parameters => SMS Param
# smsParamTagA3 = "A3" + lv(smsParamTag81) #conditional proprietary parameters => SMS Param

'''smsr addressing parameters: tag 86'''
smsrAddressData = "91 0511891002" #from TON/NPI tag address
smsrAddressingParam = "86"+lv(smsrAddressData)

'''data field sent in the command msg: DGI 3A07'''
updateSmsRData = "3A07" + lv(smsrAddressingParam + smsParamTagA3)
# updateSmsRData = "3A07" + lv(smsParamTagA3)


# getDataDefaultSmsRParam = ""
# Constants.getDataDefault = ""
# expDataOpenChannel = "D0 46 01 03 01 40 01 02 02 81 82 05 00 35 01 03\
#                       39 02 02 00 47 18 07 70 75 62 6C 69 63 34 0B 6D\
#                       32 6D 69 6E 74 65 72 6E 65 74 03 63 6F 6D 0D 05\
#                       04 6E 6F 6E 65 0D 05 04 6E 6F 6E 65 3C 03 02 48\
#                       48 3E 05 21 10 02 19 03"

# expDataOpenChannel = "D0 46 01 03 01 40 01 02 02 81 82 05 00 35 01 03\
#                       39 02 02 00 47 18 07 70 75 62 6C 69 63 34 0B 6D\
#                       32 6D 69 6E 74 65 72 6E 65 74 03 63 6F 6D 0D 05\
#                       04 6E 6F 6E 65 0D 05 04 6E 6F 6E 65 3C 03 02" + dataTagA2Port + \
# 					  "3E 05" + dataTagA2Ip






smscAddrs = "8606 110502100319"

pid = "810148"
dcs = "820148"
dataUpdateCon ="3A07" + berLv("A0" + berLv(smscAddrs + pid + dcs))

installPersoDefISDP = "80 E6 20 00" + lv("0000"+lv(Constants.AID_ISDP)+"000000 00")
apduUpdate = [installPersoDefISDP ,"80 E2 88 00 " +berLv(dataUpdateCon)]

testName = "PSM Update SMSR DNS Parameter"

class UpdateSmsrDnsParam(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None		

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4) 

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			EUICC.init()

			pprint.h2("Create ISDP 15")
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			pprint.h2("Download Profile")
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)

			pprint.h2("Enable ISDP 15")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, False)
				capdu, rapdu, sw = SERVER.apf_on(80)

			'''update sms connectivity'''		
			pprint.h2("Update Connectivity Parameters")
			command_script, por, sw = EUICC.mno.ram(apduUpdate, scp=80)					

			pprint.h2("Enable ISDP 11")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, False)
				capdu, rapdu, sw = SERVER.apf_on(80)
		except:
			exceptionTraceback(position = 1) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_update_smsr_dns_param(self):
		h2("testbody")
		LogInfo("body part")
		# timer_start = time() #SAT KENAPA MENDADAK GAK ADA MODULE TIME() KAMPERT... KESEL RASANYA T*I!!!
		try:
			pprint.h3("open secure channel")
			EUICC.isdr.sd.openDefaultSecureChannel()

			LogInfo("Get Data:")
			# Constants.getDataDefault = EUICC.isdr.sd.getData("85") #tag 8D only
			Constants.getDataDefault8D = EUICC.isdr.sd.getData("A5 03 5C 01 8D") #tag 8D only
			Constants.getDataDefault93 = EUICC.isdr.sd.getData("A5 03 5C 01 93") #tag 93 only

			pprint.h3("Update SMS Parameter") #need update after have new perso for sprint 7
			EUICC.isdr.sd.storeData(updateSmsRData, P1 = "88") #p1 bakal berubah untuk update smsr krn pake DGI
			# StepOn()
			pprint.h3("get data after store")
			# getDataAfterStore = EUICC.isdr.sd.getData("85") #tag full
			getDataAfterStore8D = EUICC.isdr.sd.getData("A5 03 5C 01 8D") #tag 8D only
			getDataAfterStore93 = EUICC.isdr.sd.getData("A5 03 5C 01 93") #tag 93 only
			
			EUICC.init()
			# pprint.h2("Enable ISDP 15")
			# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
			# if '9000' in rapdu:
			# 	pprint.h2("Refresh")
			# 	if sw is not None:
			# 		DEVICE.fetch_one(sw, 'refresh')

			pprint.h2("Enable ISDP 15")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				pprint.h2("Refresh")
				if sw is not None:
					DEVICE.fetch_one(sw, 'refresh')

				LogInfo('>>> fetch refresh \n')
				EUICC.init()
				pprint.h3("send envelope location status")
				data, sw = Envelope ("D615190103820282811B0100130903022789081614406F", expectedStatus = "91XX")
				LogInfo('envelope location status sw: {}'.format(sw))
				pprint.h3("Notification SMS")
				# data = Fetch(sw[2:], expectedData = expEnvDataCon , expectedStatus = "9000")
				data = Fetch(sw[2:], expectedStatus = "9000")
				dataTr = TerminalResponse("81 03 01 13 00 02 02 82 81 83 01 00", expectedStatus = "91XX")

				dataNotifSms = data[0][:68]
				# if (dataNotifSms == expTestResult):
				# 	Constants.RESULTS['test_name'].append('PSM Check Update Connectivity')
				# 	Constants.RESULTS['test_result'].append('OK')
				# 	EXECUTION_STATUS = 'PASSED'
				# 	Constants.case+=1
				# else:
				# 	Constants.RESULTS['test_name'].append('PSM Check Update Connectivity')
				# 	Constants.RESULTS['test_result'].append('KO')
				# 	EXECUTION_STATUS = 'FAILED'
				# 	Constants.case+=1

				seq_number = pprint.pprint_sms_first_notif(data[0])
				data, sw, cmd_type = DEVICE.fetch_all(dataTr)
				pprint.h2('server confirms reception of euicc notification')
				capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))























			# LogInfo('>>> fetch refresh \n')
			# EUICC.init()
			# pprint.h3("send envelope location status")
			# data, sw = Envelope ("D615190103820282811B0100130903022789081614406F", expectedStatus = "91XX")
			# LogInfo('envelope location status sw: {}'.format(sw))

			# pprint.h3("Notification HTTPS")
			# data = Fetch(sw[2:], expectedStatus = "9000")
			# dataTr = TerminalResponse("8103012700 82028281 830100", expectedStatus = "91XX")

			# # Spec Open channel flow IP: 
			# # 1. IP Connection (Tag 3E under tag 84) failed or not exist 
			# # 2. IP OTA (tag 91) failed or not exist
			# # 3. IP DNS (tag 8E) failed or not exist
			# # 4. DNS Req to Handset

			# LogInfo("Fetch 1 - 1- Open Channel - Failed")
			# # data = Fetch(dataTr[2:], expectedData = expDataOpenChannel, expectedStatus = "9000")
			# data = Fetch(dataTr[2:], expectedStatus = "9000")
			# dataTr = TerminalResponse("01 03 01 40 01 02 02 82 81 03 01 30", expectedStatus = "91XX, 9000")
			
			# LogInfo("Fetch 1 - 2 - Open Channel - Failed")
			# data = Fetch(dataTr[2:], expectedStatus = "9000")
			# dataTr = TerminalResponse("01 03 01 40 01 02 02 82 81 03 01 30", expectedStatus = "91XX, 9000")
			
			# LogInfo("Fetch 1 - 3 - Open Channel - Failed")
			# data = Fetch(dataTr[2:], expectedStatus = "9000")
			# dataTr = TerminalResponse("01 03 01 40 01 02 02 82 81 03 01 30", expectedStatus = "91XX, 9000")
			
			# LogInfo("Fetch 1 - 4 - Open Channel - Success")
			# data = Fetch(dataTr[2:], expectedStatus = "9000")
			# dataTr = TerminalResponse("01 03 01 40 01 02 02 82 81 0D 05 04 6E 6F 6E 65 0D 05 04 6E 6F 6E 65 03 01 00", expectedStatus = "91XX, 9000")
			
			# LogInfo("Fetch 2 - Poll Interval")
			# data = Fetch(dataTr[2:], expectedStatus = "9000")
			# dataTr = TerminalResponse("81 03 01 05 00 82 02 82 81 84 02 01 1E 83 01 00 ", expectedStatus = "91XX, 9000")

			self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
		except:
			exceptionTraceback(position = 2) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
		# timer_end = time()
		# EXECUTION_DURATION = timer_end - timer_start
		LogInfo('body part')

		

	def tearDown(self):
		pprint.h2("tearDown")
		try:
			EUICC.init()			
			pprint.h3("Open Secure Channel")
			EUICC.isdr.sd.openDefaultSecureChannel()
		
			pprint.h3("REMOVE TAG 93")
			if (Constants.getDataDefault93 == None) or (Constants.getDataDefault93 == ""):			
				EUICC.isdr.sd.storeData("A5"+berLv("93"+berLv("")), P1 = "91")
			else:
				EUICC.isdr.sd.storeData(Constants.getDataDefault93, P1 = "91")

			pprint.h3("store data tag 8d to default")
			EUICC.isdr.sd.storeData(Constants.getDataDefault8D, P1 = "91")

			pprint.h3("get data")
			getDataTearDown = EUICC.isdr.sd.getData("85") #get data full

			if(getDataTearDown == Constants.getDataDefault):
				LogInfo("UPDATE TAG 8D TO DEFAULT VALUE: SUCCEED")
			else:
				LogInfo("UPDATE TAG 8D TO DEFAULT VALUE: FAILED")

			EUICC.init()

			pprint.h3("Enable ISDP 11")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, False)

			pprint.h3("delete isdp 15")
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
			DEVICE.get_status()
		except:
			exceptionTraceback(position = 3) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
		
		# if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		if(self.result!="OK"):raise ValueError("TEST FAILED")
if __name__ == "__main__":

	from test import support
	Utilities.launchTest(UpdateSmsrDnsParam)