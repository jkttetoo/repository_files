import sys
from Oberthur import *
from testlink import TestlinkAPIClient
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import unittest, json, collections, datetime
from util import file

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'm.firmansyah@oberthur.com'

target_id="DAKO-4309"
with open(file.db('testlink_db.txt')) as f:
	test_suite = json.load(f)

testcase = next( (testcase for testcase in test_suite if testcase['ext_id'] == target_id),None)
if testcase['ext_id'] == target_id:
	print(testcase['ext_id'])
	testcase_id = testcase['ext_id']
else:
	LogInfo('External ID not match with test dict.')
# print(testcase)
print (testcase_id)