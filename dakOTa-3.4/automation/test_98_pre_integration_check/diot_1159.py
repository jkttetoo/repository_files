import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
import const
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'
__created__		= '05222020'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		SMS                     = model.SCP80()
		HTTP                    = model.SCP81()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LPin                    = model.LocationPlugin()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4663'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		SMS                     = model.SCP80()
		HTTP                    = model.SCP81()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LPin                    = model.LocationPlugin()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4663'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('debug')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		# Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk34_NG_proto2'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.DIOT_913                  = '8315_Telstra_DEFAULT_template.hex.txt'        
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 10
		# Constants.ITERATIONS_STRESS         = 100 #max 9
		Constants.ITERATIONS_STRESS         = 2 #max 9
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	LPin                    = model.LocationPlugin()
	INGENICO                = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID             = 'DK-4663'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('debug')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = True
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.DIOT_913                  = '8315_Telstra_DEFAULT_template.hex.txt'        
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS            = 25
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "dakot_1770_01_disable_FNA_trig_FC_notif"
test_summary = '''
test_description:
Test behavior of the sent FC notification followed with several normal operation/usages

pre-condition:
create + download ISDP 16
configure APF ON, FRM OFF
Configure swb retry 3, timer 15mins

Steps:
1. eUICC init
2. Trigger Sent FC by status + Notif confirm
3. Trigger no service on Fallback profile + Refresh should not occur
4. Enable Profile 16 + Sent Notif 02 + Notif confirm
5. Trigger Fallback + Refresh
6. Fallback notif sent + notif confirm
7. Switchback + Sent notif 02 + notif confirm



'''

# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'

class EnableFallbackNetworkLoss(OtTestUnit):
	EXECUTION_STATUS = None
	result = None
	EXECUTION_DURATION = None
	flag = ''

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)
			result, EXECUTION_STATUS = resultPassed(testName, False)

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			EUICC.init()
			
		except:
			exceptionTraceback(position = 1)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_enable_rollback_network_loss_disable_profile(self):
		h2("testbody")
		timer_start = time()
		try:

			data, sw = Envelope('D61B190103820282819B0100930FD100632B103401B1D3F88C088D336B')
			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030126010202828103010014083A25790109009006')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030126080202828103010062099309004378107200F0')				
				
			HTTP.scp81_psk_id_diversified = '383030313032383131303839303333303234303633343030343337373030303030303030303032383433344631304130303030303035353931303130464646464646464638393030303030313030383230313031383330313431'
			HTTP.scp81_psk_tls_key = '2532179A6F632F8E4B0C39D8EF25B315'
			GTO_INSTALL_FOR_PERSO = ['80E620001300000DA0000000308000000001F0000100000000','80E290000E030C180513003938180502151324','80E620001500000FA0000000185308000000000044323500000000','80E290000480020001','80E620001300000DA0000000308000000001EF000100000000','80E2900003100100']
			
			data, sw = DEVICE.fetch_one(sw)
			capdu, rapdu, sw = HTTP.remote_management(GTO_INSTALL_FOR_PERSO, pull=sw, notification=False, header_targeted_app=None)

			if sw != '9000':
				DEVICE.fetch_all(sw)
			
			pprint.h3('http session IMEI tracking pcom')
			EUICC.init()
			
			data, sw = DEVICE.get_status()
			data, sw = Envelope('D60A19010B020282813F0108')
			data, sw = Envelope('D615190103020282811B0100130913018433010C80D02F')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030126010202828103010014083A85270907006100')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('810301020002028281030100')

			counter = 0
			while sw!='9000':
				counter = counter+1
				pprint.h3('OPEN CHANNEL FAIL #{}'.format(counter))
				data, sw = Fetch(sw[2:4])
				sw = TerminalResponse('81030140010202828103023A003501033902058E')
				data, sw = Fetch(sw[2:4])
				sw = TerminalResponse('810301270002028281030100240101')
				data, sw = DEVICE.get_status()
				data,sw = Envelope('D70C020282812401012503000034')

			pprint.h3('http session IMEI tracking pcom - Reset and test DNS')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8CE1F9C00A79E00001FE2600000C3D0000700116000500000000008")
			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030126010202828103010014083A85270907006100')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('010301050002028281030100')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('8103010300020282810301000402011E')

			data, sw = DEVICE.get_status()
			data, sw = Envelope('D60A19010B020282813F0108')
			data, sw = Envelope('D615190103020282811B0100130913018433010C80D02F')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030126010202828103010014083A85270907006100')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('810301020002028281030100')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030140010202828103023A003501033902058E')
			data, sw = DEVICE.fetch_one(sw)

			data,sw = Envelope('D70C020282812401012503000034')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030140010202828103023A003501033902058E')

			pprint.h3('DNS SESSION')
			# data, sw = DEVICE.fetch_one(sw)
			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('8103014003020282810301003802810035010339020200')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('8103014301020282810301003701FF')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('810301270002028281030100240101')

			data, sw = Envelope('D60E1901098202218138028100370184')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030142000202828103010036775378858000010000000100000973696D6F74616D2D3207767A773367707003636F6D00001C0001C0160006000100001C2000440A6E6A62626461646E7331056C6F63616C000B767A772E6E65742E646E730F766572697A6F6E776972656C657373C01E781BFD640000A8C000001C200012750000002A30370100')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('8103014301020282810301003701FF')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('810301270002028281030100240101')

			data, sw = Envelope('D60E1901090202828138028100370147')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('81030142000202828103010036475378858000010002000000000973696D6F74616D2D3207767A773367707003636F6D0000010001C00C0001000100001C200004454E5FCDC00C0001000100001C200004454EEB3C370100')

			data, sw = Fetch(sw[2:4])
			sw = TerminalResponse('810301410082028281830100')
			
			if sw != '9000':
				data, sw = DEVICE.fetch_one(sw)
				INSTALL_FOR_PERSO = ['80e620001500000fa0000000185308000000000044323500000000', '80e291000409020008000000']
				capdu, rapdu, sw = HTTP.remote_management(INSTALL_FOR_PERSO, pull=sw, notification=False, http_header_next_uri='/CEH/otaip/ImeiTracking?pt=RumImeiTracking&trSeqNum=0008&trBl=0&msisdn=19083231341&step=1&Worker=(m11)', header_targeted_app=None)
				# data, sw = DEVICE.fetch_one(sw)
			
			pprint.h3('STATUS 7x')
			for i in range(7):
				data, sw = DEVICE.get_status()
			data, sw = Envelope('D7 0C 02 02 82 81 24 01 01 25 03 00 00 34')

			data, sw = DEVICE.get_status()
			data, sw = DEVICE.fetch_one(sw)
			data, sw = DEVICE.fetch_one(sw)			
			capdu, rapdu, sw = HTTP.remote_management(INSTALL_FOR_PERSO, pull=sw, notification=False, http_header_next_uri='/CEH/otaip/ImeiTracking?pt=RumImeiTracking&trSeqNum=0008&trBl=0&msisdn=19083231341&step=1&Worker=(m11)', header_targeted_app=None)
			# data, sw = DEVICE.fetch_one(sw)

			

			pprint.h3('STATUS 30x')
			for i in range(30):
				data, sw = DEVICE.get_status()

			pprint.h3('PUSH SMS')
			data, sw = Envelope('D14F020283810607919130391214F30B40440C911108212222017FF6026081413523692D02700000281516391010000001174C1C3EC0967C9C38A93E552A5AFB55F5195C011288B0CCD69BA788E45D53FA')

			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='open channel')
				if last_proactive_cmd == 'open channel':
					capdu, rapdu, sw = HTTP.remote_management(GSMA.audit_isdp_enabled, pull=sw, notification=False)
			
			# PowerOff()
			# PowerOn()
			# sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			# data, sw = DEVICE.fetch_one(sw)
			# data, sw = DEVICE.fetch_one(sw)
			# data, sw = DEVICE.fetch_one(sw)
			# data, sw = Envelope('D6 0A 19 01 0B 02 02 82 81 3F 01 08 ')
			# data, sw = Envelope('D6 15 19 01 03 02 02 82 81 1B 01 00 13 09 13 01 84 33 01 0C 80 D0 2F ')
			
			# data, sw = DEVICE.fetch_one(sw)
			# data, sw = DEVICE.fetch_one(sw)
			
			# data, sw = Fetch(sw[2:4])
			# sw = TerminalResponse('8103014003020282810301003802810035010339020200')
			# data, sw = Fetch(sw[2:4])
			# sw = TerminalResponse('')

			# EUICC.init()
			# SMS.spi1 = 0x16
			# SMS.spi2 = 0x39
			# SMS.kic = 0x10
			# SMS.kic_key = '5CBD565D61E00B8190FF22F09B3096D8'
			# SMS.kid = 0x10
			# SMS.kid_key = '5BC23789CA4894696DF95866251F2CCA'
			# SMS.tar = '000000'
			# SMS.algo_cipher = const.hexa.ciphers['AES']
			# SMS.algo_crypto_verif = const.hexa.ciphers['AES']
			# capdu, rapdu, sw = SMS.remote_management(['8110830E840C3E05215BF0484B3C0302241B'],apdu_format='definite',tpoa='0681214365')

			# if sw != '9000':
			# 	capdu, rapdu, sw = HTTP.remote_management(GSMA.audit_isdp_enabled, pull=sw)

			# # pprint.h3('Delete ISDP 16 - Should return 69E1')
			# # command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP16, scp=81, apdu_format='definite')
		
			# if '9000' in rapdu:
			# 	self.flag+='1'
			

			# pprint.h3('Flag : {}'.format(self.flag))
			# if self.flag == '1' :
			# 	self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
			# else:
			# 	pprint.h3("TEST FAIL {}".format(self.flag))
			# 	self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
		except:
			# exceptionTraceback(position = 2)
			# self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
			for i in range(7):
				data, sw = DEVICE.get_status()
			data, sw = Envelope('D7 0C 02 02 82 81 24 01 01 25 03 00 00 34')

			data, sw = DEVICE.get_status()
			data, sw = DEVICE.fetch_one(sw)
			data, sw = DEVICE.fetch_one(sw)
			INSTALL_FOR_PERSO = ['80e620001500000fa0000000185308000000000044323500000000', '80e291000409020008000000']
			capdu, rapdu, sw = HTTP.remote_management(INSTALL_FOR_PERSO, pull=sw, notification=False, http_header_next_uri='/CEH/otaip/ImeiTracking?pt=RumImeiTracking&trSeqNum=0008&trBl=0&msisdn=19083231341&step=1&Worker=(m11)', header_targeted_app=None)
			# data, sw = DEVICE.fetch_one(sw)
		finally:
			for i in range(100):
				data, sw = DEVICE.get_status()
			
			if sw == '9000':
				for i in range(100):
					data, sw = DEVICE.get_status()
			
			if sw != '9000':
				DEVICE.fetch_all(sw)

			pprint.h3('PUSH SMS')
			data, sw = Envelope('D14F020283810607919130391214F30B40440C911108212222017FF6026081413523692D02700000281516391010000001174C1C3EC0967C9C38A93E552A5AFB55F5195C011288B0CCD69BA788E45D53FA')

			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='open channel')
				if last_proactive_cmd == 'open channel':
					capdu, rapdu, sw = HTTP.remote_management(GSMA.audit_isdp_enabled, pull=sw, notification=False)


		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')
		LogInfo('Result table')
		pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')

	def tearDown(self):
		h2("teardown")
		try:

			EUICC.init()
			# capdu, rapdu, sw = SERVER.disable_profile(Constants.AID_ISDP16, scp=81, network_service=False, apdu_format='definite')
			# command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP16, scp=81, apdu_format='definite')
			# DEVICE.get_status()
			# EUICC.init()
			# capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.DEFAULT_ISDP11, scp=80, apdu_format='compact')
			# DEVICE.get_status()
		except:
			exceptionTraceback(position = 3)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(EnableFallbackNetworkLoss)
