from Ot.PyCom import *
import sys
from Oberthur import *
import os

cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)

import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *

import unittest
import model
from time import time

__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'
'''
__date__ = 200180709
__purpose__= check delete profile 17 after emergency profile 17 activate

'''

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO_SERVICE = model.AutoService()

		EXTERNAL_ID             = 'DK-4739'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
		#why its failed if I use SetLogLevel here?
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO_SERVICE = model.AutoService()
		GLONASS = model.EraGlonass()

		EXTERNAL_ID             = 'DK-4739'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL 				= 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk34_phase_7'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'
		Constants.AID_ISDP17				= 'A0000005591010FFFFFFFF8900001700'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4739'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL 				= 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'dk34_phase_7'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'
	Constants.AID_ISDP17				= 'A0000005591010FFFFFFFF8900001700'	
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "diot_1127_APF_not_started_if_network_loss_during_prof_download"

# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'

class diot_1127(OtTestUnit):
	result_test = None
	status_test = None
	EXECUTION_DURATION = None
	flag = ''

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 2)

	def setUp(self):
		pprint.h1(testName)
		pprint.h2("setup")
		try:
			EUICC.init()
			pprint.h3("establish keyset isdp 15")
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			pprint.h3("download profile 15")
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
			
			capdu, rapdu, sw = SERVER.apf_on(80)
			capdu, rapdu, sw = SERVER.frm_off(80)	

			pprint.h3('Enable ISDP 15')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='indefinite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))		
				
		except:
			exceptionTraceback(position = 1)
			self.result_test, self.status_test = resultPassed(testName, False)


	def test_diot_1127_APF_not_started_if_network_loss_during_prof_download(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			EUICC.init()
			data, sw = DEVICE.envelope_location_status()
			pprint.h3("create isdp and establish keyset profile 16")
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP16, scp=80, apdu_format='definite')
			pprint.h3("download profile 16")
			capdu, rapdu, sw = SERVER.download_profile(Constants.AID_ISDP16, saipv2_txt=Constants.SAIP_TEST,pertrubation='negative event')


			#==================================================================================
			data, sw = DEVICE.envelope_timer_expiration(id='psm')
			if sw != '9000':
				sw = DEVICE.fetch_all(sw)


			EUICC.init()
			pprint.h3('Audit ISDP')
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
			split_data = rapdu.split('E31C4F10')
			if '1100' in split_data[1]:
				if '70013F' in split_data[1]:
					self.flag+='1'

			data, sw = DEVICE.envelope_location_status()
						
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'] )
				if last_proactive_cmd == 'open channel' or last_proactive_cmd == 'send sms':
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
					self.flag += '2'
				else:
					pprint.h3('!!! No notification !!!')
			#==================================================================================


			pprint.h3('RESULT : {}'.format(self.flag))
			if self.flag == '12':
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				pprint.h3('RESULT : {}'.format(self.flag))
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		except:
			data, sw = DEVICE.envelope_timer_expiration(id='psm')
			if sw != '9000':
				sw = DEVICE.fetch_all(sw)


			EUICC.init()
			pprint.h3('Audit ISDP')
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
			split_data = rapdu.split('E31C4F10')
			if '1100' in split_data[1]:
				if '70013F' in split_data[1]:
					self.flag+='1'

			data, sw = DEVICE.envelope_location_status()
						
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'] )
				if last_proactive_cmd == 'open channel' or last_proactive_cmd == 'send sms':
					capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
					self.flag += '2'
				else:
					pprint.h3('!!! No notification !!!')


			pprint.h3('RESULT : {}'.format(self.flag))
			if self.flag == '12':
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				pprint.h3('RESULT : {}'.format(self.flag))
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
			
			
			pprint.h3('RESULT : {}'.format(self.flag))
			if self.flag == '12':
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
			exceptionTraceback(position = 1)
			# self.result_test, self.status_test = resultPassed(testName, False)


	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
			pprint.h3("disable isdp 15")
			SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
			pprint.h3("delete profile 15")
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')

			pprint.h3("delete profile 16")
			SERVER.delete_profile(Constants.AID_ISDP16, scp=80, apdu_format='compact')

		except:
			exceptionTraceback(position = 3)
			self.result_test, self.status_test = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.status_test, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(diot_1127)