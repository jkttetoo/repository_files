from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
from model.euicc import Isdr as ISDR
import unittest
import model
from elevate import elevate
from Ot.PCOM32Manager import *
from time import time

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'
'''
__date-creation__ = 20190418
'''

Options.counters={}

if Constants.displayAPDU != None :
			displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO 					= model.AutoService()
		ISDR					= ISDR()
		
		EXTERNAL_ID             = 'DK-4548'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO 					= model.AutoService()
		ISDR					= ISDR()

		EXTERNAL_ID             = 'DK-4548'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
		# Constants.debugMode = False
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	AUTO 					= model.AutoService()
	ISDR					= ISDR()

	EXTERNAL_ID             = 'DK-4548'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	# SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = True
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

# import Ot.GlobalPlatform as GP
# from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain, IssuerSecurityDomain, AESKeyset

# kvn70Key = "10029119039605111002911903960511"
# kvn70Data = "70" + "01" + lv("88" + lv(kvn70Key) + "00")
# kvn70Putkey = "80 D8" + kvn70Data

# Constants.token_aes_key = ""

testName = "diot_255_phase2_send_memory_reset"
class diot255(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None	
	flag_ok = 0
	location_status = True

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4) 			

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			EUICC.init()
			pprint.h3("establish keyset isdp 12")
			SERVER.create_isdp_and_establish_keyset(Constants.DEFAULT_ISDP12, scp=81, apdu_format='definite')
			pprint.h3("download profile 12")
			SERVER.download_profile(Constants.DEFAULT_ISDP12, saipv2_txt=Constants.SAIP_TEST)
			
			capdu, rapdu, sw = SERVER.apf_on(80)
			capdu, rapdu, sw = SERVER.frm_off(80)

		except:
			exceptionTraceback(position = 1) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

	def test_master_delete(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			pprint.h3("Set Emergency Profile to ISDP 10")
			capdu, rapdu, sw = SERVER.ram(GSMA.set_emergency_profile(Constants.DEFAULT_ISDP10), 80, apdu_format='indefinite')
			if '9000' in rapdu:
				self.flag_ok+=1

			pprint.h3("Audit ISDP list")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
			split_data = rapdu.split('E31C4F10')
			if '1000' in split_data[2]:
				if '530102' in split_data[2]:
					self.flag_ok+=1

			pprint.h3("Send EUCC RESET")
			capdu, rapdu, sw = ISDR.scp03(sd=ISDR.sd, capdus=EUICC.reset(), payload=False)
			if sw[:2] == '90':
				self.flag+=1

			pprint.h3("Audit ISDP list")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
			split_data = rapdu.split('E31C4F10')
			if Constants.DEFAULT_ISDP12 not in split_data:
				self.flag_ok+=1


			if self.flag_ok == 4:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		except:
			exceptionTraceback(position = 2) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')


	def tearDown(self):
		pprint.h2("teardown")
		try:
			pprint.h3("enable isdp 11 (pre-condition)")
			EUICC.init()			
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 80, apdu_format='compact', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))

			pprint.h3("set fallback isdp 11 (pre-condition)")
			EUICC.init()
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.DEFAULT_ISDP11, scp=80, apdu_format='definite')
			
			pprint.h3("set emergency profile isdp 10 (pre-condition)")
			EUICC.init()
			capdu, rapdu, sw = SERVER.ram(GSMA.set_emergency_profile(Constants.DEFAULT_ISDP10), 81, apdu_format='indefinite')
			
			pprint.h3("disable isdp 12")
			EUICC.init()
			SERVER.disable_profile(Constants.DEFAULT_ISDP12, scp=80, network_service=False, apdu_format='definite')

			pprint.h3("delete profile 12")
			SERVER.delete_profile(Constants.DEFAULT_ISDP12, scp=80, apdu_format='compact')
		except:
			exceptionTraceback(position = 3) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

		if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		# if(self.result!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(diot255)