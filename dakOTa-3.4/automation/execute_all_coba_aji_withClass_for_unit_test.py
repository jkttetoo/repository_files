from Ot.PyCom import *
import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


EUICC 	= model.Euicc()
SERVER	= model.Smsr()

Constants.displayAPDU = True
displayAPDU(Constants.displayAPDU)
SetLogLevel('info')

SetCurrentReader('OMNIKEY CardMan 3x21 0')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')


''' TESTLINK VARIABLES '''
Constants.TESTLINK_REPORT			= False
Constants.TESTLINK_DEVKEY			= 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL				= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME		= 'DakOTa v3.1'
Constants.TESTLINK_BUILD			= 'build_for_test'
Constants.TESTLINK_PLATFORM			= 'IO222'
Constants.TESTLINK_TEST_PLAN		= 'eUICC Pre-integration'
Constants.TESTLINK_TESTER 			= 'm.firmansyah@oberthur.com'


'''
TEST VARIABLES

To launch the test, prepare the following variables : 

Constants.AID_ISDP 			: It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
Constants.DEFAULT_ISDP10	: It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
Constants.DEFAULT_ISDP11	: It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
Constants.DEFAULT_ISDP12	: It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 

'''
Constants.AID_ISDP 					= 'A0000005591010FFFFFFFF8900001500'
Constants.DEFAULT_ISDP10			= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11			= 'A0000005591010FFFFFFFF8900001100'
Constants.DEFAULT_ISDP12			= 'A0000005591010FFFFFFFF8900001200'
Constants.DEFAULT_ISDP13			= 'A0000005591010FFFFFFFF8900001300'
Constants.DEFAULT_ISDP14			= 'A0000005591010FFFFFFFF8900001400'
Constants.WRONG_ISDP				= 'A0000005591010FFFFFFFF8900001101'
Constants.SAIP_TEST 				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_HSM					= 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
Constants.SAIP_PCF00				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
Constants.ITERATIONS_PERFORMANCE	= 1
Constants.ITERATIONS_STRESS			= 1
Constants.RESULTS 					= {
										'test_name' 		: ['Test name'],
										'test_result'		: ['Test result']
										}


EXECUTE_TEST_ISDR					= True
EXECUTE_TEST_MNOSD					= False
EXECUTE_TEST_PCF_RULE				= False
EXECUTE_TEST_LM_APPLET				= False
EXECUTE_TEST_PERFORMANCE			= False
EXECUTE_TEST_STRESS					= False
EXECUTE_TEST_DOWNLOAD_PROFILE		= True
EXECUTE_TEST_ERA_GLONASS_APPLET		= False
EXECUTE_TEST_HSM_APPLET				= False
EXECUTE_TEST_LOCATION_PLUGIN_APPLET	= False


'''
PRE-CONDITION:

	1. Set fallback attribut on profile 11
	2. FRM status ON
	3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 10 state PERSONALIZED     --> Pre-created ISDP
4. Profile 10 state PERSONALIZED	 --> Pre-created ISDP
5. Profile 10 state PERSONALIZED	 --> Pre-created ISDP

=====================================

'''
Constants.case = 0
Constants.RunAll = True


from test_case.automation.test_01_isdr import scp80_list_profiles_compact_Aji
''' test_01_isdr '''
from test_case.automation.test_01_isdr import scp80_list_profiles_indefinite
from test_case.automation.test_01_isdr import scp80_list_profiles_definite

suite = unittest.TestSuite()
AAAAA = suite._tests

class eSIM_Test(unittest.TestCase):
#	if EXECUTE_TEST_ISDR:
#		EUICC.init()
#		
#		'''Uji Coba Aji'''
#		suite.addTest(scp80_list_profiles_compact_Aji.suite())
#					
#		''' List profiles '''
#		suite.addTest(scp80_list_profiles_compact.suite())
#		suite.addTest(scp80_list_profiles_definite.suite())

	tags = ['prastaji', 'ISDR']
	EUICC.init()
	
	'''Uji Coba Aji'''
	suite.addTest(scp80_list_profiles_compact_Aji.suite())
				
	''' List profiles '''
#	suite.addTest(scp80_list_profiles_compact.suite())
	suite.addTest(scp80_list_profiles_definite.suite())
	suite.addTest(scp80_list_profiles_indefinite.suite())

	'''Uji coba tambahan'''
	suite.addTest(scp80_list_profiles_definite.suite())
	suite.addTest(scp80_list_profiles_compact_Aji.suite())
	suite.addTest(scp80_list_profiles_indefinite.suite())
	
#	
#	def test_Buat3(self):
#		Constants.case += 1
##		retValue = unittest.TextTestRunner().run(AAAAA.__getitem__(Constants.case - 1))
##		result = unittestGetResultFailed(str(retValue))
##		assert result == 0
#		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_indefinite')
#		assert retValue == 0
#		pass	
#	
#	def test_Buat1(self):
#		Constants.case += 1 
#		retValue = unittest.TextTestRunner().run(AAAAA.__getitem__(Constants.case - 1))
#		result = unittestGetResultFailed(str(retValue))
#		assert result == 0
#	
#	def test_Buat2(self):
#		Constants.case += 1	
#		retValue = unittest.TextTestRunner().run(AAAAA.__getitem__(Constants.case - 1))
#		result = unittestGetResultFailed(str(retValue))
#		assert result == 0
#	
#	def test_Buat3(self):
#		Constants.case += 1
#		retValue = unittest.TextTestRunner().run(AAAAA.__getitem__(Constants.case - 1))
#		result = unittestGetResultFailed(str(retValue))
#		assert result == 0
#
#	def test_Buat4(self):
#		Constants.case += 1
#		retValue = unittest.TextTestRunner().run(AAAAA.__getitem__(Constants.case - 1))
#		result = unittestGetResultFailed(str(retValue))
#		assert result == 0	
#
#	def test_Buat5(self):
#		Constants.case += 1
#		retValue = unittest.TextTestRunner().run(AAAAA.__getitem__(Constants.case - 1))
#		result = unittestGetResultFailed(str(retValue))
#		assert result == 0
#
#	def test_Buat6(self):
#		Constants.case += 1
#		retValue = unittest.TextTestRunner().run(AAAAA.__getitem__(Constants.case - 1))
#		result = unittestGetResultFailed(str(retValue))
#		assert result == 0










#	def test_Buat6(self):
#		Constants.case += 1
#		retValue = unittest.TextTestRunner().run(AAAAA.__getitem__(Constants.case - 1))
#		print("Unittest Result: ",retValue)
#		assert Constants.RESULTS['test_result'][Constants.case] == "OK"
##		assert unittest.TextTestRunner().run(AAAAA.__getitem__(2)) == "OK"



if __name__ == '__main__':
    unittest.main()