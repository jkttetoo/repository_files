import sys
from Oberthur import *
sys.path.insert(0, "../")
sys.path.insert(0, "../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
from time import time
import unittest
import configparser
import model
import automation


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


EUICC 	= model.Euicc()
SERVER	= model.Smsr()


displayAPDU(True)
SetLogLevel('info')
SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')

'''
PRE-CONDITION:

	1. Set fallback attribut on profile 11
	2. FRM status ON
	3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 10 state PERSONALIZED     --> Pre-created ISDP
4. Profile 10 state PERSONALIZED	 --> Pre-created ISDP
5. Profile 10 state PERSONALIZED	 --> Pre-created ISDP

=====================================

'''


''' test_01_isdr '''
from test_case.automation.test_01_isdr import scp80_list_profiles_compact
from test_case.automation.test_01_isdr import scp80_list_profiles_definite
from test_case.automation.test_01_isdr import scp80_list_profiles_indefinite
from test_case.automation.test_01_isdr import scp81_list_profiles_definite
from test_case.automation.test_01_isdr import scp81_list_profiles_indefinite
from test_case.automation.test_01_isdr import scp80_list_resources_compact
from test_case.automation.test_01_isdr import scp80_list_resources_definite
from test_case.automation.test_01_isdr import scp80_list_resources_indefinite
from test_case.automation.test_01_isdr import scp81_list_resources_definite
from test_case.automation.test_01_isdr import scp81_list_resources_indefinite
from test_case.automation.test_01_isdr import scp80_enable_profile_compact
from test_case.automation.test_01_isdr import scp80_enable_profile_definite
from test_case.automation.test_01_isdr import scp80_enable_profile_indefinite
from test_case.automation.test_01_isdr import scp81_enable_profile_definite
from test_case.automation.test_01_isdr import scp81_enable_profile_indefinite
from test_case.automation.test_01_isdr import scp80_disable_profile_compact
from test_case.automation.test_01_isdr import scp80_disable_profile_definite
from test_case.automation.test_01_isdr import scp80_disable_profile_indefinite
from test_case.automation.test_01_isdr import scp81_disable_profile_definite
from test_case.automation.test_01_isdr import scp81_disable_profile_indefinite
from test_case.automation.test_01_isdr import scp80_create_isdp_definite
from test_case.automation.test_01_isdr import scp80_create_isdp_indefinite
from test_case.automation.test_01_isdr import scp81_create_isdp_definite
from test_case.automation.test_01_isdr import scp81_create_isdp_indefinite
from test_case.automation.test_01_isdr import scp81_download_profile_definite
from test_case.automation.test_01_isdr import scp81_download_profile_indefinite
from test_case.automation.test_01_isdr import scp80_set_fallback_enable_profile_compact
from test_case.automation.test_01_isdr import scp80_set_fallback_enable_profile_definite
from test_case.automation.test_01_isdr import scp80_set_fallback_enable_profile_indefinite
from test_case.automation.test_01_isdr import scp81_set_fallback_enable_profile_definite
from test_case.automation.test_01_isdr import scp81_set_fallback_enable_profile_indefinite
from test_case.automation.test_01_isdr import scp80_set_fallback_disabled_profile_compact
from test_case.automation.test_01_isdr import scp80_set_fallback_disabled_profile_definite
from test_case.automation.test_01_isdr import scp80_set_fallback_disabled_profile_indefinite
from test_case.automation.test_01_isdr import scp81_set_fallback_disabled_profile_definite
from test_case.automation.test_01_isdr import scp81_set_fallback_disabled_profile_indefinite
from test_case.automation.test_01_isdr import scp80_delete_profile_compact
from test_case.automation.test_01_isdr import scp80_delete_profile_definite
from test_case.automation.test_01_isdr import scp80_delete_profile_indefinite
from test_case.automation.test_01_isdr import scp81_delete_profile_definite
from test_case.automation.test_01_isdr import scp81_delete_profile_indefinite
from test_case.automation.test_01_isdr import scp80_update_smsr_compact
from test_case.automation.test_01_isdr import scp80_update_smsr_definite
from test_case.automation.test_01_isdr import scp80_update_smsr_indefinite
from test_case.automation.test_01_isdr import scp81_update_smsr_definite
from test_case.automation.test_01_isdr import scp81_update_smsr_indefinite

''' test_02_mnosd '''
from test_case.automation.test_02_mnosd import scp80_list_mnosd_compact
from test_case.automation.test_02_mnosd import scp80_list_mnosd_definite
from test_case.automation.test_02_mnosd import scp80_list_mnosd_indefinite
from test_case.automation.test_02_mnosd import scp81_list_mnosd_definite
from test_case.automation.test_02_mnosd import scp81_list_mnosd_indefinite
from test_case.automation.test_02_mnosd import scp80_list_mno_applet_compact
from test_case.automation.test_02_mnosd import scp80_list_mno_applet_definite
from test_case.automation.test_02_mnosd import scp80_list_mno_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_list_mno_applet_definite
from test_case.automation.test_02_mnosd import scp81_list_mno_applet_indefinite
from test_case.automation.test_02_mnosd import scp80_install_applet_compact
from test_case.automation.test_02_mnosd import scp80_install_applet_definite
from test_case.automation.test_02_mnosd import scp80_install_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_install_applet_definite
from test_case.automation.test_02_mnosd import scp81_install_applet_indefinite
from test_case.automation.test_02_mnosd import scp80_lock_applet_compact
from test_case.automation.test_02_mnosd import scp80_lock_applet_definite
from test_case.automation.test_02_mnosd import scp80_lock_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_lock_applet_definite
from test_case.automation.test_02_mnosd import scp81_lock_applet_indefinite
from test_case.automation.test_02_mnosd import scp80_unlock_applet_compact
from test_case.automation.test_02_mnosd import scp80_unlock_applet_definite
from test_case.automation.test_02_mnosd import scp80_unlock_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_unlock_applet_definite
from test_case.automation.test_02_mnosd import scp81_unlock_applet_indefinite
from test_case.automation.test_02_mnosd import scp80_uninstall_applet_compact
from test_case.automation.test_02_mnosd import scp80_uninstall_applet_definite
from test_case.automation.test_02_mnosd import scp80_uninstall_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_uninstall_applet_definite
from test_case.automation.test_02_mnosd import scp81_uninstall_applet_indefinite
from test_case.automation.test_02_mnosd import scp80_update_3g_adn_compact
from test_case.automation.test_02_mnosd import scp80_update_3g_adn_definite
from test_case.automation.test_02_mnosd import scp80_update_3g_adn_indefinite
from test_case.automation.test_02_mnosd import scp81_update_3g_adn_definite
from test_case.automation.test_02_mnosd import scp81_update_3g_adn_indefinite
from test_case.automation.test_02_mnosd import scp80_update_2g_spn_compact
from test_case.automation.test_02_mnosd import scp80_update_2g_spn_definite
from test_case.automation.test_02_mnosd import scp80_update_2g_spn_indefinite

''' test_03_pcf_rule ''' 
from test_case.automation.test_03_pcf_rule import pcf00_enable_profile
from test_case.automation.test_03_pcf_rule import pcf00_disable_profile
from test_case.automation.test_03_pcf_rule import pcf00_delete_profile
from test_case.automation.test_03_pcf_rule import pcf00_network_attachment_loss
from test_case.automation.test_03_pcf_rule import pcf01_enable_profile
from test_case.automation.test_03_pcf_rule import pcf01_disable_profile
from test_case.automation.test_03_pcf_rule import pcf01_delete_profile
from test_case.automation.test_03_pcf_rule import pcf01_network_attachment_loss
from test_case.automation.test_03_pcf_rule import pcf02_enable_profile
from test_case.automation.test_03_pcf_rule import pcf02_disable_profile
from test_case.automation.test_03_pcf_rule import pcf02_delete_profile
from test_case.automation.test_03_pcf_rule import pcf02_network_attachment_loss
from test_case.automation.test_03_pcf_rule import pcf04_enable_profile
from test_case.automation.test_03_pcf_rule import pcf04_disable_profile
from test_case.automation.test_03_pcf_rule import pcf04_delete_profile
from test_case.automation.test_03_pcf_rule import pcf04_network_attachment_loss

''' test_04_lm_applet '''
from test_case.automation.test_04_lm_applet import lm_applet_enable_profile_with_notif
from test_case.automation.test_04_lm_applet import lm_applet_disable_profile_with_notif
from test_case.automation.test_04_lm_applet import lm_applet_delete_profile_with_notif

from test_case.automation.test_04_lm_applet import lm_applet_enable_profile_without_notif
from test_case.automation.test_04_lm_applet import lm_applet_disable_profile_without_notif
from test_case.automation.test_04_lm_applet import lm_applet_delete_profile_without_notif

from test_case.automation.test_04_lm_applet import lm_applet_enable_the_enabled_profile
from test_case.automation.test_04_lm_applet import lm_applet_disable_the_disabled_profile
from test_case.automation.test_04_lm_applet import lm_applet_delete_the_enabled_profile

from test_case.automation.test_04_lm_applet import lm_applet_set_fallback
from test_case.automation.test_04_lm_applet import lm_applet_set_fallback_disabled_profile
from test_case.automation.test_04_lm_applet import lm_applet_get_euicc_info
from test_case.automation.test_04_lm_applet import lm_applet_get_eid
from test_case.automation.test_04_lm_applet import lm_applet_get_applet_config
from test_case.automation.test_04_lm_applet import lm_applet_enable_notif
from test_case.automation.test_04_lm_applet import lm_applet_disable_notif
from test_case.automation.test_04_lm_applet import lm_applet_disallow_enable_profile
from test_case.automation.test_04_lm_applet import lm_applet_disallow_disable_profile
from test_case.automation.test_04_lm_applet import lm_applet_disallow_delete_profile
from test_case.automation.test_04_lm_applet import lm_applet_allow_enable_profile
from test_case.automation.test_04_lm_applet import lm_applet_allow_disable_profile
from test_case.automation.test_04_lm_applet import lm_applet_allow_delete_profile


''' test_05_performance '''
from test_case.automation.test_05_performance import scp80_list_profiles
from test_case.automation.test_05_performance import scp80_list_resources
from test_case.automation.test_05_performance import scp80_enable_profile_http_notif
from test_case.automation.test_05_performance import scp80_disable_profile_http_notif
from test_case.automation.test_05_performance import scp80_create_isdp
from test_case.automation.test_05_performance import scp80_set_fallback_profile
from test_case.automation.test_05_performance import scp80_delete_profile
from test_case.automation.test_05_performance import scp81_list_profiles
from test_case.automation.test_05_performance import scp81_list_resources
from test_case.automation.test_05_performance import scp81_enable_profile_http_notif
from test_case.automation.test_05_performance import scp81_disable_profile_http_notif
from test_case.automation.test_05_performance import scp81_create_isdp
from test_case.automation.test_05_performance import scp81_set_fallback_profile
from test_case.automation.test_05_performance import scp81_delete_profile
from test_case.automation.test_05_performance import scp81_download_profile_9k
from test_case.automation.test_05_performance import scp81_download_profile_74K
from test_case.automation.test_05_performance import scp81_download_profile_138K

''' test_06_stress '''
from test_case.automation.test_06_stress import download_delete_status
from test_case.automation.test_06_stress import download_delete_without_status
from test_case.automation.test_06_stress import enable_profile_disable_profile
from test_case.automation.test_06_stress import enable_rollback_network_loss_disable_profile
from test_case.automation.test_06_stress import enable_profile_fallback_network_loss

''' test_07_download_profile '''
from test_case.automation.test_07_download_profile import download_profile_comparison

''' test_08_applet_era_glonass '''
from test_case.automation.test_08_applet_era_glonass import nom_neg_001_profile_activation_failed_due_to_activated
from test_case.automation.test_08_applet_era_glonass import nom_neg_002_profile_deactivation_failed_due_to_disabled_state
from test_case.automation.test_08_applet_era_glonass import nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated
from test_case.automation.test_08_applet_era_glonass import nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state
from test_case.automation.test_08_applet_era_glonass import nom_neg_005_set_profile_failure_cause
from test_case.automation.test_08_applet_era_glonass import nom_pos_001_profile_activation_via_applet_selection_without_notif
from test_case.automation.test_08_applet_era_glonass import nom_pos_002_profile_deactivation_via_applet_selection_without_notif
from test_case.automation.test_08_applet_era_glonass import nom_pos_003_profile_activation_via_process_toolkit_without_notif
from test_case.automation.test_08_applet_era_glonass import nom_pos_004_profile_deactivation_via_process_toolkit_without_notif
from test_case.automation.test_08_applet_era_glonass import nom_pos_005_lock_unlock_mconnect_command
from test_case.automation.test_08_applet_era_glonass import nom_pos_006_get_glonass_profile
from test_case.automation.test_08_applet_era_glonass import nom_pos_007_profile_activation_via_applet_selection_with_notif
from test_case.automation.test_08_applet_era_glonass import nom_pos_008_profile_deactivation_via_applet_selection_with_notif
from test_case.automation.test_08_applet_era_glonass import nom_pos_009_profile_activation_via_process_toolkit_with_notif
from test_case.automation.test_08_applet_era_glonass import nom_pos_010_profile_deactivation_via_process_toolkit_with_notif

''' test_09_applet_HSM '''
from test_case.automation.test_09_applet_hsm import audit_hsm_files

''' test_10_applet_location plugin '''
from test_case.automation.test_10_applet_location_plugin import nom_pos_001_update_applet_configuration_via_applet_selection
from test_case.automation.test_10_applet_location_plugin import nom_pos_002_update_applet_configuration_via_update_smsr
from test_case.automation.test_10_applet_location_plugin import nom_pos_003_applet_triggered_by_mcc_change
from test_case.automation.test_10_applet_location_plugin import nom_pos_004_applet_triggered_by_power_on
from test_case.automation.test_10_applet_location_plugin import nom_pos_005_applet_triggered_by_mnc_change
from test_case.automation.test_10_applet_location_plugin import nom_pos_006_activate_location_plugin_notification_via_ota
from test_case.automation.test_10_applet_location_plugin import nom_pos_007_deactivate_location_plugin_notification_via_ota
from test_case.automation.test_10_applet_location_plugin import nom_pos_008_activate_location_plugin_notification_via_applet
from test_case.automation.test_10_applet_location_plugin import nom_pos_009_deactivate_location_plugin_notification_via_applet

class ExecuteAll():

	def execute(self, REPORT= False, TEST_ISDR = False, TEST_MNOSD = False,  TEST_PCF_RULE =False, TEST_LM_APPLET = False, TEST_PERFORMANCE = False, TEST_STRESS = False, TEST_DOWNLOAD_PROFILE = False, TEST_ERA_GLONASS_APPLET = False,  TEST_HSM_APPLET = False, TEST_LOCATION_PLUGIN_APPLET = False):

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT			= REPORT
		Constants.TESTLINK_DEVKEY			= 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL				= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME		= 'DakOTa v3.1'
		Constants.TESTLINK_BUILD			= 'build_for_test'
		Constants.TESTLINK_PLATFORM			= 'IO222'
		Constants.TESTLINK_TEST_PLAN		= 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER 			= 'm.firmansyah@oberthur.com'


		'''
		TEST VARIABLES

		To launch the test, prepare the following variables : 

		Constants.AID_ISDP 			: It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10	: It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11	: It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12	: It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 

		'''
		Constants.AID_ISDP 					= 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10			= 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11			= 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12			= 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13			= 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14			= 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP				= 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST 				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM					= 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE	= 5
		Constants.ITERATIONS_STRESS			= 100
		Constants.RESULTS 					= {
												'test_name' 		: ['Test name'],
												'test_result'		: ['Test result']
												}

		suite = unittest.TestSuite()

		if TEST_ISDR:
			EUICC.init()
			''' List profiles '''
			suite.addTest(scp80_list_profiles_compact.suite())
			suite.addTest(scp80_list_profiles_definite.suite())
			suite.addTest(scp80_list_profiles_indefinite.suite())
			suite.addTest(scp81_list_profiles_definite.suite())
			suite.addTest(scp81_list_profiles_indefinite.suite())
			''' List resources '''
			suite.addTest(scp80_list_resources_compact.suite())
			suite.addTest(scp80_list_resources_definite.suite())
			suite.addTest(scp80_list_resources_indefinite.suite())
			suite.addTest(scp81_list_resources_definite.suite())
			suite.addTest(scp81_list_resources_indefinite.suite())
			''' Enable proflie '''
			suite.addTest(scp80_enable_profile_compact.suite())
			suite.addTest(scp80_enable_profile_definite.suite())
			suite.addTest(scp80_enable_profile_indefinite.suite())
			suite.addTest(scp81_enable_profile_definite.suite())
			suite.addTest(scp81_enable_profile_indefinite.suite())
			''' Disable proflie '''
			suite.addTest(scp80_disable_profile_compact.suite())
			suite.addTest(scp80_disable_profile_definite.suite())
			suite.addTest(scp80_disable_profile_indefinite.suite())
			suite.addTest(scp81_disable_profile_definite.suite())
			suite.addTest(scp81_disable_profile_indefinite.suite())
			''' Create ISD-P '''
			suite.addTest(scp80_create_isdp_definite.suite())
			suite.addTest(scp80_create_isdp_indefinite.suite())
			suite.addTest(scp81_create_isdp_definite.suite())
			suite.addTest(scp81_create_isdp_indefinite.suite())
			''' Download profile '''
			suite.addTest(scp81_download_profile_definite.suite())
			suite.addTest(scp81_download_profile_indefinite.suite())
			''' Set-fallback on enabled profile '''
			suite.addTest(scp80_set_fallback_enable_profile_compact.suite())
			suite.addTest(scp80_set_fallback_enable_profile_definite.suite())
			suite.addTest(scp80_set_fallback_enable_profile_indefinite.suite())
			suite.addTest(scp81_set_fallback_enable_profile_definite.suite())
			suite.addTest(scp81_set_fallback_enable_profile_indefinite.suite())
			''' Set-fallback on disabled profile '''
			suite.addTest(scp80_set_fallback_disabled_profile_compact.suite())
			suite.addTest(scp80_set_fallback_disabled_profile_definite.suite())
			suite.addTest(scp80_set_fallback_disabled_profile_indefinite.suite())
			suite.addTest(scp81_set_fallback_disabled_profile_definite.suite())
			suite.addTest(scp81_set_fallback_disabled_profile_indefinite.suite())
			''' Delete profile '''
			suite.addTest(scp80_delete_profile_compact.suite())
			suite.addTest(scp80_delete_profile_definite.suite())
			suite.addTest(scp80_delete_profile_indefinite.suite())
			suite.addTest(scp81_delete_profile_definite.suite())
			suite.addTest(scp81_delete_profile_indefinite.suite())
			''' Update SMSR '''
			suite.addTest(scp80_update_smsr_compact.suite())
			suite.addTest(scp80_update_smsr_definite.suite())
			suite.addTest(scp80_update_smsr_indefinite.suite())
			suite.addTest(scp81_update_smsr_definite.suite())
			suite.addTest(scp81_update_smsr_indefinite.suite())

		if TEST_MNOSD:
			EUICC.init()
			SERVER.download_profile(Constants.DEFAULT_ISDP12, saipv2_txt=Constants.SAIP_TEST)
			
			EUICC.init()
			''' List profiles '''
			suite.addTest(scp80_list_mnosd_compact.suite())
			suite.addTest(scp80_list_mnosd_definite.suite())
			suite.addTest(scp80_list_mnosd_indefinite.suite())
			suite.addTest(scp81_list_mnosd_definite.suite())
			suite.addTest(scp81_list_mnosd_indefinite.suite())
			''' List applets '''
			suite.addTest(scp80_list_mno_applet_compact.suite())
			suite.addTest(scp80_list_mno_applet_definite.suite())
			suite.addTest(scp80_list_mno_applet_indefinite.suite())
			suite.addTest(scp81_list_mno_applet_definite.suite())
			suite.addTest(scp81_list_mno_applet_indefinite.suite())
			''' Install applet '''
			suite.addTest(scp80_install_applet_compact.suite())
			suite.addTest(scp80_install_applet_definite.suite())
			suite.addTest(scp80_install_applet_indefinite.suite())
			suite.addTest(scp81_install_applet_definite.suite())
			suite.addTest(scp81_install_applet_indefinite.suite())
			''' Lock applet '''
			suite.addTest(scp80_lock_applet_compact.suite())
			suite.addTest(scp80_lock_applet_definite.suite())
			suite.addTest(scp80_lock_applet_indefinite.suite())
			suite.addTest(scp81_lock_applet_definite.suite())
			suite.addTest(scp81_lock_applet_indefinite.suite())
			''' Unlock applet '''
			suite.addTest(scp80_unlock_applet_compact.suite())
			suite.addTest(scp80_unlock_applet_definite.suite())
			suite.addTest(scp80_unlock_applet_indefinite.suite())
			suite.addTest(scp81_unlock_applet_definite.suite())
			suite.addTest(scp81_unlock_applet_indefinite.suite())
			''' Uninstall applet '''
			suite.addTest(scp80_uninstall_applet_compact.suite())
			suite.addTest(scp80_uninstall_applet_definite.suite())
			suite.addTest(scp80_uninstall_applet_indefinite.suite())
			suite.addTest(scp81_uninstall_applet_definite.suite())
			suite.addTest(scp81_uninstall_applet_indefinite.suite())
			''' Update 3G ADN '''
			suite.addTest(scp80_update_3g_adn_compact.suite())
			suite.addTest(scp80_update_3g_adn_definite.suite())
			suite.addTest(scp80_update_3g_adn_indefinite.suite())
			suite.addTest(scp81_update_3g_adn_definite.suite())
			suite.addTest(scp81_update_3g_adn_indefinite.suite())
			''' Update 2G ADN '''
			suite.addTest(scp80_update_2g_spn_compact.suite())
			suite.addTest(scp80_update_2g_spn_definite.suite())
			suite.addTest(scp80_update_2g_spn_indefinite.suite())

		if TEST_PCF_RULE:
			EUICC.init()
			''' PCF 00: no POL1 rule active  '''
			suite.addTest(pcf00_enable_profile.suite())
			suite.addTest(pcf00_disable_profile.suite())
			suite.addTest(pcf00_delete_profile.suite())
			suite.addTest(pcf00_network_attachment_loss.suite())
			''' PCF 01: Disabling of this profile not allowed  '''
			suite.addTest(pcf01_enable_profile.suite())
			suite.addTest(pcf01_disable_profile.suite())
			suite.addTest(pcf01_delete_profile.suite())
			suite.addTest(pcf01_network_attachment_loss.suite())
			''' PCF 02: Disabling of this profile not allowed  '''
			suite.addTest(pcf02_enable_profile.suite())
			suite.addTest(pcf02_disable_profile.suite())
			suite.addTest(pcf02_delete_profile.suite())
			suite.addTest(pcf02_network_attachment_loss.suite())
			''' PCF 04: Profile deletion is mandatory when its state is changed to disabled  '''
			suite.addTest(pcf04_enable_profile.suite())
			suite.addTest(pcf04_disable_profile.suite())
			suite.addTest(pcf04_delete_profile.suite())
			suite.addTest(pcf04_network_attachment_loss.suite())

		if TEST_LM_APPLET:
			EUICC.init()
			suite.addTest(lm_applet_enable_profile_with_notif.suite())
			suite.addTest(lm_applet_disable_profile_with_notif.suite())
			suite.addTest(lm_applet_delete_profile_with_notif.suite())

			suite.addTest(lm_applet_enable_profile_without_notif.suite())
			suite.addTest(lm_applet_disable_profile_without_notif.suite())
			suite.addTest(lm_applet_delete_profile_without_notif.suite())

			suite.addTest(lm_applet_enable_the_enabled_profile.suite())
			suite.addTest(lm_applet_disable_the_disabled_profile.suite())
			suite.addTest(lm_applet_delete_the_enabled_profile.suite())

			suite.addTest(lm_applet_set_fallback_disabled_profile.suite())
			suite.addTest(lm_applet_set_fallback.suite())
			suite.addTest(lm_applet_get_euicc_info.suite())
			suite.addTest(lm_applet_get_eid.suite())
			suite.addTest(lm_applet_get_applet_config.suite())
			suite.addTest(lm_applet_enable_notif.suite())
			suite.addTest(lm_applet_disable_notif.suite())
			suite.addTest(lm_applet_disallow_enable_profile.suite())
			suite.addTest(lm_applet_disallow_disable_profile.suite())
			suite.addTest(lm_applet_disallow_delete_profile.suite())
			suite.addTest(lm_applet_allow_enable_profile.suite())
			suite.addTest(lm_applet_allow_disable_profile.suite())
			suite.addTest(lm_applet_allow_delete_profile.suite())

		if TEST_PERFORMANCE:
			EUICC.init()
			''' scp80 '''
			suite.addTest(scp80_list_profiles.suite())
			suite.addTest(scp80_list_resources.suite())
			suite.addTest(scp80_enable_profile_http_notif.suite())
			suite.addTest(scp80_disable_profile_http_notif.suite())
			suite.addTest(scp80_create_isdp.suite())
			suite.addTest(scp80_set_fallback_profile.suite())
			suite.addTest(scp80_delete_profile.suite())
			''' scp81 '''
			suite.addTest(scp81_list_profiles.suite())
			suite.addTest(scp81_list_resources.suite())
			suite.addTest(scp81_enable_profile_http_notif.suite())
			suite.addTest(scp81_disable_profile_http_notif.suite())
			suite.addTest(scp81_create_isdp.suite())
			suite.addTest(scp81_set_fallback_profile.suite())
			suite.addTest(scp81_delete_profile.suite())
			suite.addTest(scp81_download_profile_9k.suite())
			suite.addTest(scp81_download_profile_74K.suite())
			suite.addTest(scp81_download_profile_138K.suite())

		if TEST_STRESS:
			suite.addTest(download_delete_status.suite())
			suite.addTest(download_delete_without_status.suite())
			suite.addTest(enable_profile_disable_profile.suite())
			suite.addTest(enable_rollback_network_loss_disable_profile.suite())
			suite.addTest(enable_profile_fallback_network_loss.suite())

		if TEST_DOWNLOAD_PROFILE:
			suite.addTest(download_profile_comparison.suite())

		if TEST_ERA_GLONASS_APPLET:
			suite.addTest(nom_neg_001_profile_activation_failed_due_to_activated.suite())
			suite.addTest(nom_neg_002_profile_deactivation_failed_due_to_disabled_state.suite())
			suite.addTest(nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated.suite())
			suite.addTest(nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state.suite())
			suite.addTest(nom_neg_005_set_profile_failure_cause.suite())
			suite.addTest(nom_pos_001_profile_activation_via_applet_selection_without_notif.suite())
			suite.addTest(nom_pos_002_profile_deactivation_via_applet_selection_without_notif.suite())
			suite.addTest(nom_pos_003_profile_activation_via_process_toolkit_without_notif.suite())
			suite.addTest(nom_pos_004_profile_deactivation_via_process_toolkit_without_notif.suite())
			suite.addTest(nom_pos_005_lock_unlock_mconnect_command.suite())
			suite.addTest(nom_pos_006_get_glonass_profile.suite())
			suite.addTest(nom_pos_007_profile_activation_via_applet_selection_with_notif.suite())
			suite.addTest(nom_pos_008_profile_deactivation_via_applet_selection_with_notif.suite())
			suite.addTest(nom_pos_009_profile_activation_via_process_toolkit_with_notif.suite())
			suite.addTest(nom_pos_010_profile_deactivation_via_process_toolkit_with_notif.suite())

		if TEST_HSM_APPLET:
			suite.addTest(audit_hsm_files.suite())

		if TEST_LOCATION_PLUGIN_APPLET:
			suite.addTest(nom_pos_001_update_applet_configuration_via_applet_selection.suite())
			suite.addTest(nom_pos_002_update_applet_configuration_via_update_smsr.suite())
			suite.addTest(nom_pos_003_applet_triggered_by_mcc_change.suite())
			suite.addTest(nom_pos_004_applet_triggered_by_power_on.suite())
			suite.addTest(nom_pos_005_applet_triggered_by_mnc_change.suite())
			suite.addTest(nom_pos_006_activate_location_plugin_notification_via_ota.suite())
			suite.addTest(nom_pos_007_deactivate_location_plugin_notification_via_ota.suite())
			suite.addTest(nom_pos_008_activate_location_plugin_notification_via_applet.suite())
			suite.addTest(nom_pos_009_deactivate_location_plugin_notification_via_applet.suite())

		unittest.TextTestRunner().run(suite)
		pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )

if __name__ == '__main__':
    automated = automation.ExecuteAll()
    # automated.execute(TEST_LM_APPLET=True)