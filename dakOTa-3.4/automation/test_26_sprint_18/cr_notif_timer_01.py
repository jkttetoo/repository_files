from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'
'''
__date-creation__ = 20180409
'''

Options.counters={}

if Constants.displayAPDU != None :
			displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO 					= model.AutoService()
		
		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO 					= model.AutoService()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_for_test'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
		# Constants.debugMode = False
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	AUTO 					= model.AutoService()

	EXTERNAL_ID             = 'DK-4303'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

# import Ot.GlobalPlatform as GP
# from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain, IssuerSecurityDomain, AESKeyset

# kvn70Key = "10029119039605111002911903960511"
# kvn70Data = "70" + "01" + lv("88" + lv(kvn70Key) + "00")
# kvn70Putkey = "80 D8" + kvn70Data

# Constants.token_aes_key = ""

testName = "Update Notification timer when Fallback going to happen"
class MasterDelete(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None	
	flag_ok = 0
	location_status = True

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4) 			

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			EUICC.init()
			pprint.h3("establish keyset isdp 15")
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			pprint.h3("download profile 15")
			# SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_MASTER_DELETE_PCF02)
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)			
			capdu, rapdu, sw = SERVER.apf_on(80)
			capdu, rapdu, sw = SERVER.frm_off(80)
			pprint.h3("enable profile 15")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['refresh'])
				EUICC.init()

		except:
			exceptionTraceback(position = 1) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

	def test_master_delete(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:


			pprint.h3("MConnect-lock on")
			AUTO.open_logical_channel(ch_number='1')
			AUTO.mConnectLock(channel = "1")

			pprint.h3('Update Notification Timer into 112233')
			capdu, rapdu, sw = SERVER.ram(GSMA.update_notification_timer(), 80, apdu_format='definite')

			pprint.h3("Trigger no service")
			data, sw = DEVICE.envelope_location_status(status='no service')
			pprint.h3('Trigger normal service + send notification')


			data, sw = DEVICE.envelope_location_status(status='normal service')
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])

				if sw != '9000':
					if last_proactive_cmd == 'open channel':
						capdu,rapdu,sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
					elif last_proactive_cmd == 'send sms':
						seq_number = pprint.pprint_sms_first_notif(data)
						data, sw, cmd_type = DEVICE.fetch_all(sw)
						LogInfo('server confirms reception of euicc notification')
						capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number))
						if sw != '9000':
							sw = DEVICE.fetch_all(sw)
			pprint.h3("MConnect-lock off")
			AUTO.open_logical_channel(ch_number='1')
			data, sw = AUTO.mConnectUnlock(channel = "1")

			if sw != '9000':
				data, sw, cmd_type = DEVICE.fetch_all(sw)
	
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			pprint.h3('Update Notification Timer into 112233')
			capdu, rapdu, sw = SERVER.ram(GSMA.update_notification_timer(), 80, apdu_format='definite')

			if self.location_status:
				pprint.h3("Trigger status 16x")
				for i in range(16):
					data, sw = DEVICE.get_status()
	
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['timer management'])
					if '112233' in data:
						if '112233' in data:
							self.flag_ok += 1
							print('WOW 112233 muncul')
						else:
							print('WOW ngga 112233 muncul')
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
					if last_proactive_cmd == 'open channel':
						capdu,rapdu,sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
					elif last_proactive_cmd == 'send sms':
						seq_number = pprint.pprint_sms_first_notif(data)
						data, sw, cmd_type = DEVICE.fetch_all(sw)
						LogInfo('server confirms reception of euicc notification')
						capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number))
	
						if sw != '9000':
							sw = DEVICE.fetch_all(sw)

			pprint.h3("Trigger no service + fallback")
			data, sw = DEVICE.envelope_location_status(status='no service')
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['timer management'])
			if last_proactive_cmd == 'timer management':
				data, sw = DEVICE.envelope_timer_expiration(id='psm')
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['refresh'])

			if last_proactive_cmd == 'refresh':
				self.flag_ok += 1
			else:
				pass

			EUICC.init()
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['timer management'])
				if '112233' in data:
					if '112233' in data:
						print('WOW 112233 muncul')
					else:
						print('WOW ngga 112233 muncul')
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'])
				if last_proactive_cmd == 'open channel':
					capdu,rapdu,sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
				elif last_proactive_cmd == 'send sms':
					seq_number = pprint.pprint_sms_first_notif(data)
					data, sw, cmd_type = DEVICE.fetch_all(sw)
					LogInfo('server confirms reception of euicc notification')
					capdu, rapdu, sw = self.isdr.scp80.remote_management(gsma.handle_notification_confirmation(seq_number))

					if sw != '9000':
						sw = DEVICE.fetch_all(sw)

			if self.flag_ok == 2:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		except:
			exceptionTraceback(position = 2) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')


	def tearDown(self):
		pprint.h2("teardown")
		try:
			EUICC.init()
			pprint.h3("set fallback isdp 11 (pre-condition)")
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.DEFAULT_ISDP11, scp=80, apdu_format='definite')

			pprint.h3('Update Notification Timer into 112233')
			capdu, rapdu, sw = SERVER.ram(GSMA.update_notification_timer(timer='000200'), 80, apdu_format='definite')

			SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='definite')
			pprint.h3("delete profile 15")
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')
		except:
			exceptionTraceback(position = 3) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

		# if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		if(self.result!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(MasterDelete)