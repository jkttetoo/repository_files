import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		HSM                     = model.HighStressMemory()

		EXTERNAL_ID             = 'DK-4427'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		HSM                     = model.HighStressMemory()

		EXTERNAL_ID             = 'DK-4427'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		# Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_HSM_NEW              = 'SAIP_ATT_89011704278250638402_v5_diot_11.der'
		Constants.SAIP_HSM_NEW_NG			= 'MyTestDecode_http_Jumbo_M2M_12_files.der.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	HSM                     = model.HighStressMemory()

	EXTERNAL_ID             = 'DK-4427'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM_NEW              = 'SAIP_ATT_89011704278250638402_v5_diot_11.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_HSM_NEW_NG			= 'MyTestDecode_http_Jumbo_M2M_12_files.der.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Audit HSM Files"
# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'

class AuditHSMFiles(OtTestUnit):

	EXECUTION_STATUS = None
	setupNotExcept = True
	testbodyNotExcept = True
	teardownNotExcept = True
	EXECUTION_DURATION = None
	result = None 

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)          
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
			

	def setUp(self):
		pprint.h1('Audit HSM Files ')
		pprint.h2("setup")
		try:
			EUICC.init()
			pprint.h3("create isdp and establish keyset")
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			
			# DK 3.2
			# pprint.h3("download profile")
			# SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_HSM)

			# # DK 3.4
			# pprint.h3("download profile")
			# SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_HSM_NEW)

			
			# DK 3.4 NG Proto 3
			pprint.h3("download profile")
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_HSM_NEW_NG)

			pprint.h3("enable profile")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP ), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
			capdu, rapdu, sw = SERVER.apf_on(80)
		except:
			exceptionTraceback(position = 1)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			
			self.setupNotExcept = False		

	def test_download_delete_without_status(self):
		LogInfo('body part')
		pprint.h2("bodytest")
		timer_start = time()
		try:
			EUICC.init
			timer_start = time()
			h3('Audit ALL HSM files')
			# DK 3.2
			data, sw = HSM.audit_high_stress_memory_counter(option='all', expData = "6FAA000000XX6FAB000000XX6FAC000000XX6FAD000000XX6FAE000000XX")
			# '''sprint 6 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='all', expData = "6FAA0000XXXX6FAB0000XXXX6FAC000000XX6FAD000000XX6FAE000000XX")
			# '''sprint 9 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='all', expData = "6F7E0000XXXX6F730000XXXX6FC4000000XX6F08000000XX6F09000000XX")
			# '''sprint 12 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='all', expData = "6FAA000000XX6FAB000000XX6FAC000000XX6FAD000000XX6FAE000000XX")

			# DK 3.4
			# '''Phase 1 perso'''
			data, sw = HSM.audit_high_stress_memory_counter(option='all', expData = "6F7E0000XXXX6F730000XXXX6FC4000000XX6F08000000XX6F09000000XX6F5B000000XX4F20000000XX4F52000000XX6F7E000000XX6F20000000XX")

			h3('Audit available HSM area')
			# DK 3.2
			# '''sprint 6 perso'''
			data, sw = HSM.audit_high_stress_memory_counter(option='available', expData = "0B")
			# '''sprint 9 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='available', expData = "00")
			# '''sprint 12 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='available', expData = "05")
			# '''sprint 18 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='available', expData = "0B")

			# DK 3.4
			# h3('Audit available HSM area')

#           
			h3('Audit highest HSM file counter')
			# data, sw = HSM.audit_high_stress_memory_counter(option='highest', expData = "6FAA000000XX")
			# '''sprint 6 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='highest', expData = "6FAA0000XXXX")
			# '''sprint 9 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='highest', expData = "6F7E0000XXXX")
			# '''sprint 12 perso'''
			# data, sw = HSM.audit_high_stress_memory_counter(option='highest', expData = "6FAA0000XXXX")

			# DK 3.4
			# '''Phase 1 perso'''
			data, sw = HSM.audit_high_stress_memory_counter(option='highest', expData = "6F7E0000XXXX")

			self.result, self.EXECUTION_STATUS = resultPassed(testName, True)

		except:
			exceptionTraceback(position = 2)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
			self.testbodyNotExcept = False
			
		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start
		# LogInfo("EXECUTION_DURATION: "+EXECUTION_DURATION)

		LogInfo('body part')
		

	def tearDown(self):		
		pprint.h2("teardown")		
		try:
			SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
			DEVICE.get_status()
		except:
			exceptionTraceback(position = 3)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			
			self.teardownNotExcept = False

		# reportTestlinkStatus = _testlinkReportChecker(self.setupNotExcept, self.testbodyNotExcept, self.teardownNotExcept)
		if Constants.TESTLINK_REPORT: self.report(execution_status=reportTestlinkStatus, execduration=self.EXECUTION_DURATION)
		# if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(AuditHSMFiles)