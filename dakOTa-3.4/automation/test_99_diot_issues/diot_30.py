from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		OTA						= model.SCP80()
		AMDB 					= model.SCP81()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
		OTA_POLLER				= model.OtaPoller()
		GP 			= model.Gp()

		EXTERNAL_ID             = 'DK-4404'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		OTA						= model.SCP80()
		AMDB 					= model.SCP81()
		DEVICE                  = model.Device()
		OTA_POLLER				= model.OtaPoller()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
		GP 			= model.Gp()

		EXTERNAL_ID             = 'DK-4404'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('debug')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		# Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_E2E 					= 'HTTPs_Aspider_SAIPv2.1_Dkt3.x_Elisa_new_re.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
		# Constants.debugMode = True
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	OTA						= model.SCP80()
	AMDB 					= model.SCP81()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	INGENICO                = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID             = 'DK-4404'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('debug')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0


testName = "DIOT-30"
class DIOT_30(OtTestUnit):

	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		pprint.h1('DIOT-30 aka DAKO-277')
		EUICC.init()
		pass
	
	def test_smsr_change(self):
		LogInfo('body part')
		EUICC.init()
		timer_start = time()
		try:
			
			data, sw  = DEVICE.envelope_location_status(status='normal service')
			# data, sw = DEVICE.envelope_timer_expiration(id='psm')
			for i in range(13):
				data, sw = DEVICE.get_status()
			while '91' in sw:
				data, sw, last_proactice_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], loci_change=False)
				if last_proactice_cmd == 'open channel':
					data, sw, last_proactice_cmd = DEVICE.fetch_all(sw)
					data, sw = DEVICE.envelope_timer_expiration(id='biplink')
					if sw != '9000':
						print('follow data come after timer expired')
						data, sw, last_proactice_cmd = DEVICE.fetch_all(sw)
						data, sw = DEVICE.envelope_timer_expiration(id='biplink')
						data, sw, last_proactice_cmd = DEVICE.fetch_all(sw, return_if=['open channel', 'send sms'], loci_change=False)
						if last_proactice_cmd == 'open channel':
							EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
					else:
						print('No more data to be fetch')
					# return self.isdr.scp81.remote_management(gsma.handle_notification_confirmation(), sw)




			if '9000' in rapdu:
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)
			else:	
				self.result_test, self.EXECUTION_STATUS = resultPassed(testName, True)
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP ), 81, apdu_format='definite', chunk='01')
			if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
			timer_end = time()
			EXECUTION_DURATION = timer_end - timer_start

		except:
			exceptionTraceback(position = 2)     
			self.result_test, self.EXECUTION_STATUS = resultPassed(testName, False)

		LogInfo('body part')

	def tearDown(self):
		# EUICC.init()
		# SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
		# SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')
		pass


if __name__ == "__main__":

	from test import support
	Utilities.launchTest(DIOT_30)
