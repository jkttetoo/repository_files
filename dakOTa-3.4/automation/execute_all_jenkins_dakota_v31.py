from Ot.PyCom import *
import sys
from Oberthur import *
import os
import pytest

cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)

import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "esim_tool_configuration.ini")
Constants.config.read(fpath)

from Mobile import *
from util import *
from time import time
import unittest
import model

# import xmlrunner

__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'


EUICC 					= model.Euicc()
SERVER					= model.Smsr()
DEVICE 					= model.Device()
GSMA					= model.Gsma()
TL_SERVER			   	= model.TestlinkServer()
INGENICO				= model.IngenicoReader()
LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
GLONASS   				= model.EraGlonass()
HSM 					= model.HighStressMemory()
LOCAL_SWAP				= model.LocalSwap()
AUTO_SERVICE = model.AutoService()

''' TESTLINK VARIABLES '''
# Constants.TESTLINK_REPORT           = True
Constants.TESTLINK_REPORT           = False
Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
Constants.TESTLINK_BUILD            = 'build_test_preintegration_jenkins'
Constants.TESTLINK_PLATFORM         = 'IO222'
Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

'''
TEST VARIABLES

To launch the test, prepare the following variables :

Constants.AID_ISDP 			: It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
Constants.DEFAULT_ISDP10	: It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
Constants.DEFAULT_ISDP11	: It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
Constants.DEFAULT_ISDP12	: It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

'''
Constants.AID_ISDR = "A0000005591010FFFFFFFF89000010"
Constants.AID_ISDP 					= 'A0000005591010FFFFFFFF8900001500'
Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'

Constants.AID_ISDP17				= 'A0000005591010FFFFFFFF8900001700'
Constants.AID_ISDP18				= 'A0000005591010FFFFFFFF8900001800'

Constants.DEFAULT_ISDP10			= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11			= 'A0000005591010FFFFFFFF8900001100'
Constants.DEFAULT_ISDP12			= 'A0000005591010FFFFFFFF8900001200'
Constants.DEFAULT_ISDP13			= 'A0000005591010FFFFFFFF8900001300'
Constants.DEFAULT_ISDP14			= 'A0000005591010FFFFFFFF8900001400'
Constants.DEFAULT_ISDP16 = "A0000005591010FFFFFFFF8900001600"
Constants.WRONG_ISDP				= 'A0000005591010FFFFFFFF8900001101'
Constants.SAIP_TEST 				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_HSM					= 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
Constants.SAIP_PCF00				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
Constants.ITERATIONS_PERFORMANCE	= 5
Constants.ITERATIONS_STRESS			= 100
Constants.RESULTS 					= {
										'test_name' 		: ['Test name'],
										'test_result'		: ['Test result']
										}

#EXECUTE_TEST_ISDR					= True
#EXECUTE_TEST_MNOSD					= False
#EXECUTE_TEST_PCF_RULE				= False
#EXECUTE_TEST_LM_APPLET				= False
#EXECUTE_TEST_PERFORMANCE			= False
#EXECUTE_TEST_STRESS					= False
#EXECUTE_TEST_DOWNLOAD_PROFILE		= True
#EXECUTE_TEST_ERA_GLONASS_APPLET		= False
#EXECUTE_TEST_HSM_APPLET				= False
#EXECUTE_TEST_LOCATION_PLUGIN_APPLET	= False


'''
PRE-CONDITION:

	1. Set fallback attribut on profile 11
	2. FRM status ON
	3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 12 state PERSONALIZED     --> Pre-created ISDP
4. Profile 13 state PERSONALIZED	 --> Pre-created ISDP
5. Profile 14 state PERSONALIZED	 --> Pre-created ISDP
4
=====================================

'''
Constants.case = 0
Constants.RunAll = True
Constants.testLinkTestCase = []
Constants.testLinkCountRunAll = 0
Constants.debugMode = False
Constants.token_aes_key = ""

if Constants.TESTLINK_REPORT == False:
	Constants.displayAPDU = True
else:
	Constants.displayAPDU = False

displayAPDU(Constants.displayAPDU)
SetLogLevel('info')

import Ot.GlobalPlatform as GP
from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain, IssuerSecurityDomain, AESKeyset

import automation.test_01_isdr as test_01_isdr
import automation.test_02_mnosd as test_02_mnosd
import automation.test_03_pcf_rule as test_03_pcf_rule
import automation.test_04_lm_applet as test_04_lm_applet
import automation.test_05_performance as test_05_performance
import automation.test_06_stress as test_06_stress
import automation.test_07_download_profile as test_07_download_profile
import automation.test_08_applet_era_glonass as test_08_applet_era_glonass
import automation.test_09_applet_hsm as test_09_applet_hsm
import automation.test_10_applet_location_plugin as test_10_applet_location_plugin


'''
test_08 test_10 dicomment di sprint 12 sampe nanti ntah kapan...
'''

class Preintegration_Test(unittest.TestCase):
	# tags = ['prastaji', 'ISDR', 'SCP80']
	def test_01_scp80_create_isdp_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_create_isdp_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_create_isdp_definite')
		assert retValue == 0
		pass

	def test_01_scp80_create_isdp_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_create_isdp_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_create_isdp_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_delete_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_delete_profile_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_delete_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_delete_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_delete_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_delete_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_delete_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_disable_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_disable_profile_compact")
		
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_compact')
		
		assert retValue == 0
		pass

	def test_01_scp80_disable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_disable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_disable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_disable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_disable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_enable_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_enable_profile_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_enable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_enable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_enable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_enable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_enable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_list_profiles_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_profiles_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_compact')
		assert retValue == 0
		pass

	def test_01_scp80_list_profiles_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_profiles_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_definite')
		assert retValue == 0
		pass

	def test_01_scp80_list_profiles_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_profiles_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_profiles_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_list_resources_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_resources_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_compact')
		assert retValue == 0
		pass

	def test_01_scp80_list_resources_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_resources_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_definite')
		assert retValue == 0
		pass

	def test_01_scp80_list_resources_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_list_resources_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_list_resources_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_disabled_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_disabled_profile_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_disabled_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_disabled_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_disabled_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_disabled_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_disabled_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_enable_profile_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_enable_profile_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_compact')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_enable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_enable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp80_set_fallback_enable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_set_fallback_enable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_set_fallback_enable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp80_update_smsr_compact(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_update_smsr_compact")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_compact')
		assert retValue == 0
		pass

	def test_01_scp80_update_smsr_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_update_smsr_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_definite')
		assert retValue == 0
		pass

	def test_01_scp80_update_smsr_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp80_update_smsr_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp80_update_smsr_indefinite')
		assert retValue == 0
		pass

#	tags = ['prastaji']
	def test_01_scp81_create_isdp_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_create_isdp_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_create_isdp_definite')
		assert retValue == 0
		pass

	def test_01_scp81_create_isdp_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_create_isdp_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_create_isdp_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_delete_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_delete_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_delete_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_delete_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_delete_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_delete_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_disable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_disable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_disable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_disable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_disable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_disable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_download_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_download_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_download_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_download_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_download_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_download_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_enable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_enable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_enable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_enable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_enable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_enable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_list_profiles_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_list_profiles_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_profiles_definite')
		assert retValue == 0
		pass

	def test_01_scp81_list_profiles_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_list_profiles_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_profiles_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_list_resources_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_list_resources_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_resources_definite')
		assert retValue == 0
		pass

	def test_01_scp81_list_resources_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_list_resources_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_list_resources_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_set_fallback_disabled_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_set_fallback_disabled_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_disabled_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_set_fallback_disabled_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_set_fallback_disabled_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_disabled_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_set_fallback_enable_profile_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_set_fallback_enable_profile_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_enable_profile_definite')
		assert retValue == 0
		pass

	def test_01_scp81_set_fallback_enable_profile_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_set_fallback_enable_profile_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_set_fallback_enable_profile_indefinite')
		assert retValue == 0
		pass

	def test_01_scp81_update_smsr_definite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_update_smsr_definite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_update_smsr_definite')
		assert retValue == 0
		pass

	def test_01_scp81_update_smsr_indefinite(self):
		Constants.testLinkTestCase.append("test_01_isdr.scp81_update_smsr_indefinite")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_01_isdr.scp81_update_smsr_indefinite')
		assert retValue == 0
		pass

	# def test_02_scp80_install_applet_compact(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_install_applet_compact")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_compact')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_install_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_install_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_install_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_install_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_install_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_list_mno_applet_compact(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mno_applet_compact")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_compact')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_list_mno_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mno_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_list_mno_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mno_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mno_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_list_mnosd_compact(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mnosd_compact")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_compact')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_list_mnosd_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mnosd_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_list_mnosd_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_list_mnosd_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_list_mnosd_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_lock_applet_compact(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_lock_applet_compact")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_compact')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_lock_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_lock_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_lock_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_lock_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_lock_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_uninstall_applet_compact(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_uninstall_applet_compact")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_compact')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_uninstall_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_uninstall_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_uninstall_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_uninstall_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_uninstall_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_unlock_applet_compact(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_unlock_applet_compact")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_compact')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_unlock_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_unlock_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_unlock_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_unlock_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_unlock_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_update_2g_spn_compact(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_2g_spn_compact")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_compact')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_update_2g_spn_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_2g_spn_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_update_2g_spn_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_2g_spn_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_update_2g_spn_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_update_3g_adn_compact(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_3g_adn_compact")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_compact')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_update_3g_adn_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_3g_adn_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp80_update_3g_adn_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp80_update_3g_adn_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp80_update_3g_adn_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_install_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_install_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_install_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_install_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_install_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_install_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_list_mno_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_list_mno_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mno_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_list_mno_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_list_mno_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mno_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_list_mnosd_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_list_mnosd_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mnosd_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_list_mnosd_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_list_mnosd_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_list_mnosd_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_lock_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_lock_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_lock_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_lock_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_lock_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_lock_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_uninstall_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_uninstall_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_uninstall_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_uninstall_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_uninstall_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_uninstall_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_unlock_applet_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_unlock_applet_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_unlock_applet_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_unlock_applet_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_unlock_applet_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_unlock_applet_indefinite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_update_3g_adn_definite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_update_3g_adn_definite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_update_3g_adn_definite')
	# 	assert retValue == 0
	# 	pass

	# def test_02_scp81_update_3g_adn_indefinite(self):
	# 	Constants.testLinkTestCase.append("test_02_mnosd.scp81_update_3g_adn_indefinite")
	# 	Constants.testLinkCountRunAll+=1
	# 	retValue = Run_ModuleMain('test_02_mnosd.scp81_update_3g_adn_indefinite')
	# 	assert retValue == 0
	# 	pass

# 	tags=["prastaji", "pcf"]
	def test_03_pcf00_delete_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf00_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_delete_profile')
		assert retValue == 0
		pass

	def test_03_pcf00_disable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf00_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_disable_profile')
		assert retValue == 0
		pass

	def test_03_pcf00_enable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf00_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_enable_profile')
		assert retValue == 0
		pass

	def test_03_pcf00_network_attachment_loss(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf00_network_attachment_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf00_network_attachment_loss')
		assert retValue == 0
		pass

	def test_03_pcf01_delete_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf01_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_delete_profile')
		assert retValue == 0
		pass

	def test_03_pcf01_disable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf01_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_disable_profile')
		assert retValue == 0
		pass

	def test_03_pcf01_enable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf01_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_enable_profile')
		assert retValue == 0
		pass

	def test_03_pcf01_network_attachment_loss(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf01_network_attachment_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf01_network_attachment_loss')
		assert retValue == 0
		pass

	def test_03_pcf02_delete_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf02_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_delete_profile')
		assert retValue == 0
		pass

	def test_03_pcf02_disable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf02_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_disable_profile')
		assert retValue == 0
		pass

	def test_03_pcf02_enable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf02_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_enable_profile')
		assert retValue == 0
		pass

	def test_03_pcf02_network_attachment_loss(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf02_network_attachment_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf02_network_attachment_loss')
		assert retValue == 0
		pass

	def test_03_pcf04_delete_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf04_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_delete_profile')
		assert retValue == 0
		pass

	def test_03_pcf04_disable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf04_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_disable_profile')
		assert retValue == 0
		pass

	def test_03_pcf04_enable_profile(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf04_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_enable_profile')
		assert retValue == 0
		pass

	def test_03_pcf04_network_attachment_loss(self):
		Constants.testLinkTestCase.append("test_03_pcf_rule.pcf04_network_attachment_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_03_pcf_rule.pcf04_network_attachment_loss')
		assert retValue == 0
		pass

# #	tags = ["prastaji", "lm_applet"]
	def test_04_lm_applet_allow_delete_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_allow_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_delete_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_allow_disable_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_allow_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_disable_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_allow_enable_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_allow_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_allow_enable_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_delete_profile_with_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_delete_profile_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_profile_with_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_delete_profile_without_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_delete_profile_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_profile_without_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_delete_the_enabled_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_delete_the_enabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_delete_the_enabled_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_disable_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disable_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_disable_profile_with_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disable_profile_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_profile_with_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_disable_profile_without_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disable_profile_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_profile_without_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_disable_the_disabled_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disable_the_disabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disable_the_disabled_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_disallow_delete_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disallow_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_delete_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_disallow_disable_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disallow_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_disable_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_disallow_enable_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_disallow_enable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_disallow_enable_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_enable_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_enable_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_enable_profile_with_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_enable_profile_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_profile_with_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_enable_profile_without_notif(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_enable_profile_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_profile_without_notif')
		assert retValue == 0
		pass

	def test_04_lm_applet_enable_the_enabled_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_enable_the_enabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_enable_the_enabled_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_get_applet_config(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_get_applet_config")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_applet_config')
		assert retValue == 0
		pass

	def test_04_lm_applet_get_eid(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_get_eid")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_eid')
		assert retValue == 0
		pass

	def test_04_lm_applet_get_euicc_info(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_get_euicc_info")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_get_euicc_info')
		assert retValue == 0
		pass

	def test_04_lm_applet_set_fallback_disabled_profile(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_set_fallback_disabled_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_set_fallback_disabled_profile')
		assert retValue == 0
		pass

	def test_04_lm_applet_set_fallback(self):
		Constants.testLinkTestCase.append("test_04_lm_applet.lm_applet_set_fallback")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_04_lm_applet.lm_applet_set_fallback')
		assert retValue == 0
		pass

	def test_05_performance_scp80_create_isdp(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_create_isdp")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_create_isdp')
		assert retValue == 0
		pass

	def test_05_performance_scp80_delete_profile(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_delete_profile')
		assert retValue == 0
		pass

	def test_05_performance_scp80_disable_profile_http_notif(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_disable_profile_http_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_disable_profile_http_notif')
		assert retValue == 0
		pass

	def test_05_performance_scp80_enable_profile_http_notif(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_enable_profile_http_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_enable_profile_http_notif')
		assert retValue == 0
		pass

	def test_05_performance_scp80_list_profiles(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_list_profiles")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_list_profiles')
		assert retValue == 0
		pass

	def test_05_performance_scp80_list_resources(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_list_resources")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_list_resources')
		assert retValue == 0
		pass

	def test_05_performance_scp80_set_fallback_profile(self):
		Constants.testLinkTestCase.append("test_05_performance.scp80_set_fallback_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp80_set_fallback_profile')
		assert retValue == 0
		pass

	def test_05_performance_scp81_create_isdp(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_create_isdp")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_create_isdp')
		assert retValue == 0
		pass

	def test_05_performance_scp81_delete_profile(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_delete_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_delete_profile')
		assert retValue == 0
		pass

	def test_05_performance_scp81_disable_profile_http_notif(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_disable_profile_http_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_disable_profile_http_notif')
		assert retValue == 0
		pass

	def test_05_performance_scp81_download_profile_138K(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_download_profile_138K")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_138K')
		assert retValue == 0
		pass

	def test_05_performance_scp81_download_profile_74K(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_download_profile_74K")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_74K')
		assert retValue == 0
		pass

	def test_05_performance_scp81_download_profile_9k(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_download_profile_9k")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_download_profile_9k')
		assert retValue == 0
		pass

	def test_05_performance_scp81_enable_profile_http_notif(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_enable_profile_http_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_enable_profile_http_notif')
		assert retValue == 0
		pass

	def test_05_performance_scp81_list_profiles(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_list_profiles")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_list_profiles')
		assert retValue == 0
		pass

	def test_05_performance_scp81_list_resources(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_list_resources")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_list_resources')
		assert retValue == 0
		pass

	def test_05_performance_scp81_set_fallback_profile(self):
		Constants.testLinkTestCase.append("test_05_performance.scp81_set_fallback_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_05_performance.scp81_set_fallback_profile')
		assert retValue == 0
		pass

	def test_06_stress_download_delete_status(self):
		Constants.testLinkTestCase.append("test_06_stress.download_delete_status")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.download_delete_status')
		assert retValue == 0
		pass

	def test_06_stress_download_delete_without_status(self):
		Constants.testLinkTestCase.append("test_06_stress.download_delete_without_status")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.download_delete_without_status')
		assert retValue == 0
		pass

	def test_06_stress_enable_profile_disable_profile(self):
		Constants.testLinkTestCase.append("test_06_stress.enable_profile_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.enable_profile_disable_profile')
		assert retValue == 0
		pass

	def test_06_stress_enable_profile_fallback_network_loss(self):
		Constants.testLinkTestCase.append("test_06_stress.enable_profile_fallback_network_loss")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.enable_profile_fallback_network_loss')
		assert retValue == 0
		pass

	def test_06_stress_enable_rollback_network_loss_disable_profile(self):
		Constants.testLinkTestCase.append("test_06_stress.enable_rollback_network_loss_disable_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_06_stress.enable_rollback_network_loss_disable_profile')
		assert retValue == 0
		pass

	def test_07_download_profile_comparison(self):
		Constants.testLinkTestCase.append("test_07_download_profile.download_profile_comparison")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_07_download_profile.download_profile_comparison')
		assert retValue == 0
		pass

	def test_08_nom_neg_001_profile_activation_failed_due_to_activated(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_001_profile_activation_failed_due_to_activated")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_001_profile_activation_failed_due_to_activated')
		assert retValue == 0
		pass

	def test_08_nom_neg_002_profile_deactivation_failed_due_to_disabled_state(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_002_profile_deactivation_failed_due_to_disabled_state")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_002_profile_deactivation_failed_due_to_disabled_state')
		assert retValue == 0
		pass

	def test_08_nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_003_profile_activation_via_process_toolkit_failed_due_to_activated')
		assert retValue == 0
		pass

	def test_08_nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_004_profile_deactivation_via_process_toolkit_failed_due_to_disabled_state')
		assert retValue == 0
		pass

	def test_08_nom_neg_005_set_profile_failure_cause(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_neg_005_set_profile_failure_cause")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_neg_005_set_profile_failure_cause')
		assert retValue == 0
		pass

	def test_08_nom_pos_001_profile_activation_via_applet_selection_without_notif(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_001_profile_activation_via_applet_selection_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_001_profile_activation_via_applet_selection_without_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_002_profile_deactivation_via_applet_selection_without_notif(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_002_profile_deactivation_via_applet_selection_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_002_profile_deactivation_via_applet_selection_without_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_003_profile_activation_via_process_toolkit_without_notif(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_003_profile_activation_via_process_toolkit_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_003_profile_activation_via_process_toolkit_without_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_004_profile_deactivation_via_process_toolkit_without_notif(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_004_profile_deactivation_via_process_toolkit_without_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_004_profile_deactivation_via_process_toolkit_without_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_005_lock_unlock_mconnect_command(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_005_lock_unlock_mconnect_command")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_005_lock_unlock_mconnect_command')
		assert retValue == 0
		pass

	def test_08_nom_pos_006_get_glonass_profile(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_006_get_glonass_profile")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_006_get_glonass_profile')
		assert retValue == 0
		pass

	def test_08_nom_pos_007_profile_activation_via_applet_selection_with_notif(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_007_profile_activation_via_applet_selection_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_007_profile_activation_via_applet_selection_with_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_008_profile_deactivation_via_applet_selection_with_notif(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_008_profile_deactivation_via_applet_selection_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_008_profile_deactivation_via_applet_selection_with_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_009_profile_activation_via_process_toolkit_with_notif(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_009_profile_activation_via_process_toolkit_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_009_profile_activation_via_process_toolkit_with_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_010_profile_deactivation_via_process_toolkit_with_notif(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_010_profile_deactivation_via_process_toolkit_with_notif")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_010_profile_deactivation_via_process_toolkit_with_notif')
		assert retValue == 0
		pass

	def test_08_nom_pos_011_profile_activation_via_local_management_applet(self):
		Constants.testLinkTestCase.append("test_08_applet_era_glonass.nom_pos_011_profile_activation_via_local_management_applet")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_08_applet_era_glonass.nom_pos_011_profile_activation_via_local_management_applet')
		assert retValue == 0
		pass

	def test_09_audit_hsm_files(self):
		Constants.testLinkTestCase.append("test_09_applet_hsm.audit_hsm_files")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_09_applet_hsm.audit_hsm_files')
		assert retValue == 0
		pass

	def test_10_nom_pos_001_update_applet_configuration_via_applet_selection(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_001_update_applet_configuration_via_applet_selection")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_001_update_applet_configuration_via_applet_selection')
		assert retValue == 0
		pass

	def test_10_nom_pos_002_update_applet_configuration_via_update_smsr(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_002_update_applet_configuration_via_update_smsr")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_002_update_applet_configuration_via_update_smsr')
		assert retValue == 0
		pass

	def test_10_nom_pos_003_applet_triggered_by_mcc_change(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_003_applet_triggered_by_mcc_change")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_003_applet_triggered_by_mcc_change')
		assert retValue == 0
		pass

	def test_10_nom_pos_004_applet_triggered_by_power_on(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_004_applet_triggered_by_power_on")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_004_applet_triggered_by_power_on')
		assert retValue == 0
		pass

	def test_10_nom_pos_005_applet_triggered_by_mnc_change(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_005_applet_triggered_by_mnc_change")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_005_applet_triggered_by_mnc_change')
		assert retValue == 0
		pass

	def test_10_nom_pos_006_activate_location_plugin_notification_via_ota(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_006_activate_location_plugin_notification_via_ota")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_006_activate_location_plugin_notification_via_ota')
		assert retValue == 0
		pass

	def test_10_nom_pos_007_deactivate_location_plugin_notification_via_ota(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_007_deactivate_location_plugin_notification_via_ota")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_007_deactivate_location_plugin_notification_via_ota')
		assert retValue == 0
		pass

	def test_10_nom_pos_008_activate_location_plugin_notification_via_applet(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_008_activate_location_plugin_notification_via_applet")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_008_activate_location_plugin_notification_via_applet')
		assert retValue == 0
		pass

	def test_10_nom_pos_009_deactivate_location_plugin_notification_via_applet(self):
		Constants.testLinkTestCase.append("test_10_applet_location_plugin.nom_pos_009_deactivate_location_plugin_notification_via_applet")
		Constants.testLinkCountRunAll+=1
		retValue = Run_ModuleMain('test_10_applet_location_plugin.nom_pos_009_deactivate_location_plugin_notification_via_applet')
		assert retValue == 0
		pass

if __name__ == '__main__':
	unittest.main()