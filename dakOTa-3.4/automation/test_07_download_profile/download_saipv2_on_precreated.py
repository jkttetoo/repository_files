import sys
from Oberthur import *
import os
sys.path.insert(0,os.getcwd()+"\site-packages")
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


SERVER 				= model.Smsr()
EUICC  				= model.Euicc()
DEVICE 				= model.Device()
TL_SERVER 			= model.TestlinkServer()

EXTERNAL_ID			= 'DK-3971'
EXECUTION_STATUS	= None
EXECUTION_DURATION	= None


displayAPDU(True)
SetLogLevel('info')


class DownloadProfile(unittest.TestCase):

	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		pprint.h1('Download profile to pre-created ISDP')
		pass

	def test_download_profile(self):
		LogInfo('body part')
		EUICC.init()
		timer_start = time()
		try:
			download_time, download_status, file_size = SERVER.download_profile(Constants.DEFAULT_ISDP14, saipv2_txt=Constants.SAIP_TEST, apdu_format='indefinite')
			if download_status[16:18] == '00':
				Constants.RESULTS['test_name'].append('Download profile to pre-created ISDP')
				Constants.RESULTS['test_result'].append('OK')
				EXECUTION_STATUS = 'PASSED'
			else:
				Constants.RESULTS['test_name'].append('Download profile to pre-created ISDP')
				Constants.RESULTS['test_result'].append('KO')
				EXECUTION_STATUS = 'FAILED'
		except:
			EXECUTION_STATUS = 'FAILED'

		timer_end = time()
		EXECUTION_DURATION = timer_end - timer_start
		self.assertEqual(Constants.RESULTS['test_result'][1], 'OK')
		# LogInfo('Constants.RESULTS table')
		# pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')
		if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)

	def tearDown(self):
		EUICC.init()
		SERVER.delete_profile(Constants.DEFAULT_ISDP14, scp=80, apdu_format='compact')
		DEVICE.get_status()

def suite():
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(DownloadProfile, 'test'))
	return suite

if __name__ == '__main__':
	unittest.main()
