import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

LIST_PROFILES 		= (
	'saipv2_8900037037000000001_Fido_LTE_Dummy_v4.txt',
	'saipv2_applet_CU_profilev8_1_10_RFMUSIM_dummy_v4.txt',
	'saipv2_applet_OFR_ESv1_2_v1.8.txt',
	'saipv2_applet_rogers_eUICC_v6.txt',
	'saipv2_applet_SAIP_OPTUS_Postpaid_Sample_Dummy_v5.der',
	'saipv2_applet_SprintSIMOTA-noFactoryReset_final_v3_rc10_[new].txt',
	'saipv2_applet_SprintUsimLite-eSIM_v3_edited.txt',
	'saipv2_applet_TransatelMCCmodif_DERTLV_04_NoAmdBwithDependancies_DER_3G_V3.txt',
	'saipv2_ClaroAr_Card01_SAIPv2_DERTLV_test02_Mconnect5.der',
	'saipv2_createdWithoutApplets_DTAG_89490200001130195467_DERTLV_ManModif_v4.txt',
	'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_hash_no3G_v2.txt',
	'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_no_hash_no3G_v2.txt',
	'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_v10.txt',
	'saipv2_DAVID_EMT_eSIM_c081_c084_ver11.txt',
	'saipv2_e_gsm_sse_0900_d7g271_testep_samsung_odc_step2_multipass_v42-Edited_EDITED.txt',
	'saipv2_EMT_eSIM_c081_c084_ver2_MT_big150k_v2.txt',
	'saipv2_EMT_eSIM_c081_c084_ver2_MT_big300k_v2.txt',
	'saipv2_line_6_89883011659830093954_version3_derTLV_v9.txt',
	'saipv2_misft_ewa_dummy_v5.txt',
	'saipv2_MSFT_v1.0_BLA_2.4_89019900012340002096_v2.txt',
	'saipv2_ProfilePackage_m2m_Sequence_RemoveDFPhoneBook_v1.txt',
	'saipv2_ProfilePackageDescription_Basic_AES_v2_original_scp03keyset_DASE_corrected_v3.txt',
	'saipv2_rogers7_dummy_v2.txt',
	'saipv2_SprintUsimLite-eSIM_V4_edited.txt',
	'saipv2_TransatelMCCmodif_DERTLV_04_NoAmdBwithDependancies_DER_3G_v2_edited.txt',
	'saipv2_applet_profileOnly_15_HEXA_V1F_dummy_v3_rc10_edited (2).txt',
	'saipv2_ASN1_output_eSIM_project_test_card_V4_edited_[new].der',
	'saipv2_GD170130_KRSMSG021882V2.00_TestData.noTemplates.ups_der_RC10_edited_noStoreData.txt',
	'saipv2_GD170130_KRSMSG021882V2.00_TestData.Templates.ups_der_rc10_edited_noStoreData_[new].txt'
	# 'saipv2_EMT_eSIM_c081_c084_ver2_MT_big300k_DERTLVmanModv_v8.txt',
	)



# LIST_PROFILES 		= (
# 	'saipv2_8900037037000000001_Fido_LTE_Dummy_v4.txt',
# 	'saipv2_applet_CU_profilev8_1_10_RFMUSIM_dummy_v4.txt',
# 	'saipv2_GD170130_KRSMSG021882V2.00_TestData.Templates.ups_der_rc10_edited_noStoreData_[new].txt'
# 	# 'saipv2_EMT_eSIM_c081_c084_ver2_MT_big300k_DERTLVmanModv_v8.txt',
# 	)




# LIST_PROFILES 		= (
# 	'saipv2_8900037037000000001_Fido_LTE_Dummy_v4.der',
# 	'saipv2_applet_CU_profilev8_1_10_RFMUSIM_dummy_v4.der',
# 	'saipv2_applet_OFR_ESv1_2_v1.8.der',
# 	'saipv2_applet_rogers_eUICC_v6.der',
# 	'saipv2_applet_SAIP_OPTUS_Postpaid_Sample_Dummy_v5.der',
# 	'saipv2_applet_SprintSIMOTA-noFactoryReset_final_v3_rc10_[new].der',
# 	'saipv2_applet_SprintUsimLite-eSIM_v3_edited.der',
# 	'saipv2_applet_TransatelMCCmodif_DERTLV_04_NoAmdBwithDependancies_DER_3G_V3.der',
# 	'saipv2_ClaroAr_Card01_SAIPv2_DERTLV_test02_Mconnect5.txt',
# 	'saipv2_createdWithoutApplets_DTAG_89490200001130195467_DERTLV_ManModif_v4.der',
# 	'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_hash_no3G_v2.der',
# 	'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_no_hash_no3G_v2.der',
# 	'saipv2_DAVID_EMT_eSIM_c081_c084_ver2_MT_DummyApplet_v10.der',
# 	'saipv2_DAVID_EMT_eSIM_c081_c084_ver11.der',
# 	'saipv2_e_gsm_sse_0900_d7g271_testep_samsung_odc_step2_multipass_v42-Edited_EDITED.der',
# 	'saipv2_EMT_eSIM_c081_c084_ver2_MT_big150k_v2.der',
# 	'saipv2_EMT_eSIM_c081_c084_ver2_MT_big300k_v2.der',
# 	'saipv2_line_6_89883011659830093954_version3_derTLV_v9.der',
# 	'saipv2_misft_ewa_dummy_v5.der',
# 	'saipv2_MSFT_v1.0_BLA_2.4_89019900012340002096_v2.der',
# 	'saipv2_ProfilePackage_m2m_Sequence_RemoveDFPhoneBook_v1.txt',
# 	'saipv2_ProfilePackageDescription_Basic_AES_v2_original_scp03keyset_DASE_corrected_v3.der',
# 	'saipv2_rogers7_dummy_v2.der',
# 	'saipv2_SprintUsimLite-eSIM_V4_edited.der',
# 	'saipv2_TransatelMCCmodif_DERTLV_04_NoAmdBwithDependancies_DER_3G_v2_edited.der',
# 	'saipv2_applet_profileOnly_15_HEXA_V1F_dummy_v3_rc10_edited (2).der',
# 	'saipv2_ASN1_output_eSIM_project_test_card_V4_edited_[new].der',
# 	'saipv2_GD170130_KRSMSG021882V2.00_TestData.noTemplates.ups_der_RC10_edited_noStoreData.txt',
# 	# 'saipv2_GD170130_KRSMSG021882V2.00_TestData.Templates.ups_der_rc10_edited_noStoreData_[new].txt'
# 	# 'saipv2_EMT_eSIM_c081_c084_ver2_MT_big300k_DERTLVmanModv_v8.der',
# 	)

RESULTS = {
	'list_profile' 	: ['Profile Name'],
	'repsonse' 		: ['Response'],
	'meaning'		: ['Meaning'],
	'download_time' : ['Time (s)'],
	'file_size'		: ['File Size']
		}

EUICC_RESPONSE = {
	'FF': 'unidentified',
	'00': 'ok',
	'01': 'pe not supported',
	'02': 'memory failure',
	'03': 'bad values',
	'04': 'not enough memory',
	'05': 'invalid request format',
	'06': 'invalid parameter',
	'07': 'runtime not supported',
	'08': 'lib not supported',
	'09': 'template not supported',
	'0A': 'feature not supported',
	'0B': 'pin code missing',
	'1F': 'unsupported profile version'
}



Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()

		EXTERNAL_ID             = 'DK-4277'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()

		EXTERNAL_ID             = 'DK-4277'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		# Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'DakOTa v3.2.1 Sprint 1 Nominal Test Case Generic'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4277'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Download Profile Comparison"
# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'

class DownloadComparison(OtTestUnit):
	EXECUTION_STATUS = None
	result = None
	EXECUTION_DURATION = None
	euicc_response = None
	file_size = 0
	download_time = 0

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

	def setUp(self):
		pprint.h1(testName)
		h2("setup")		

	def test_download_profile_comparison(self):
		h2("testbody")
		counter_failed=0
		timer_start = time()
		# try:
		for profile in LIST_PROFILES:
			try:
				EUICC.init()
				pprint.h1("Download Profile {}".format(profile))
				SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=80, apdu_format='definite')
				download_time, self.euicc_response, file_size = SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=profile)
				self.file_size += file_size
				self.download_time += download_time
				EUICC.init()
				SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
				DEVICE.get_status()
				LogInfo("euicc_response: "+self.euicc_response)
			except:			
				# exceptionTraceback(position = 2)
				# self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
				EUICC.init()
				SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
				DEVICE.get_status()
				counter_failed+=1
			
			if self.euicc_response==None:
				self.euicc_response = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'
			# if euicc_response !='ok':
			# 	counter_failed+=1
			if EUICC_RESPONSE[self.euicc_response[16:18]] != "ok":
				counter_failed+=1
			self.file_size = self.file_size/1000
			RESULTS['list_profile'].append(profile)
			RESULTS['repsonse'].append(self.euicc_response)
			RESULTS['meaning'].append(EUICC_RESPONSE[self.euicc_response[16:18]])
			RESULTS['download_time'].append('{:04.1f}'.format(self.download_time))
			RESULTS['file_size'].append('{} KB '.format(self.file_size))
			LogInfo("counter_failed: "+str(counter_failed))
		# except:			
		# 	exceptionTraceback(position = 2)
		# 	self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
		# 	counter_failed+=1

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start
		# Constants.RESULTS['test_name'].append('Download Profile Comparison')
		if counter_failed == 0:
			self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
		else:
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		LogInfo('body part')
		LogInfo('Result table')
		pprint.table( [RESULTS['list_profile'], RESULTS['repsonse'], RESULTS['meaning'], RESULTS['download_time'], RESULTS['file_size']] )
		LogInfo('body part')	

	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
		except:
			exceptionTraceback(position = 3)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		LogInfo("self.result: "+self.result)
		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(self.result!="OK"):raise ValueError("TEST FAILED")


if __name__ == "__main__":

	from test import support
	Utilities.launchTest(DownloadComparison)