import sys
from Oberthur import *
import os
sys.path.insert(0,os.getcwd()+"\site-packages")
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


EUICC               = model.Euicc()
SERVER              = model.Smsr()
DEVICE              = model.Device()
TL_SERVER           = model.TestlinkServer()

EXTERNAL_ID         = 'DK-3970'
EXECUTION_STATUS    = None
EXECUTION_DURATION  = None

LIST_PROFILES = (
	'AF09_8910390000001168129F_HTTP_OTA.cmd',
	'AF09_8910390000001168129F_SMS_OTA.cmd'
	)

RESULTS = {
	'list_profile'  : ['Profile Name'],
	'repsonse'      : ['Response'],
	'meaning'       : ['Meaning'],
	'download_time' : ['Time (s)'],
	'file_size'     : ['File Size']
		}

EUICC_RESPONSE = {
	'FF': 'unidentified',
	'00': 'ok',
	'01': 'pe not supported',
	'02': 'memory failure',
	'03': 'bad values',
	'04': 'not enough memory',
	'05': 'invalid request format',
	'06': 'invalid parameter',
	'07': 'runtime not supported',
	'08': 'lib not supported',
	'09': 'template not supported',
	'0A': 'feature not supported',
	'0B': 'pin code missing',
	'1F': 'unsupported profile version'
}

displayAPDU(True)
SetLogLevel('info')


class DownloadApduProfile(unittest.TestCase):

	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		pprint.h1('Download Profile on Pre-created ISDP ')
		pass

	def test_download_apdu_profile(self):
		counter_failed=0
		timer_start = time()
		LogInfo('body part')
		EUICC.init()
		try:
			pprint.h1("Download Profile {}".format(list_profile[0]))
			download_time, euicc_response, file_size = SERVER.download_apdu_profile(Constants.DEFAULT_ISDP13, perso_txt=list_profile[0])
			EUICC.init()
			SERVER.delete_profile(Constants.DEFAULT_ISDP13, scp=80, apdu_format='definite')
			DEVICE.get_status()
		except:
			EXECUTION_STATUS = 'FAILED'
		if euicc_response==None:
			euicc_response = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'

		Constants.RESULTS['test_name'].append('Download Profile on Pre-created ISDP')
		if euicc_response =='ok':
			EXECUTION_STATUS = 'PASSED'
			Constants.RESULTS['test_result'].append('OK')
		else:
			EXECUTION_STATUS = 'FAILED'
			Constants.RESULTS['test_result'].append('KO')

		file_size = file_size/1000
		RESULTS['list_profile'].append(profile)
		RESULTS['repsonse'].append(euicc_response)
		RESULTS['meaning'].append(EUICC_RESPONSE[euicc_response[16:18]])
		RESULTS['download_time'].append('{:04.1f}'.format(download_time))
		RESULTS['file_size'].append('{} KB '.format(file_size))
		timer_end = time()
		EXECUTION_DURATION = timer_end - timer_start
		LogInfo('body part')
		LogInfo('Result table')
		pprint.table( [RESULTS['list_profile'], RESULTS['repsonse'], RESULTS['meaning'], RESULTS['download_time'], RESULTS['file_size']] )
		if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)

	def tearDown(self):
		EUICC.init()
		pass

def suite():
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(DownloadApduProfile, 'test'))
	return suite

if __name__ == '__main__':
	unittest.main()