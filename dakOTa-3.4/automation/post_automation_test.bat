@ECHO OFF

ECHO ============================
ECHO GENERATE MAIL REPORT
ECHO ============================

python C:\dakOTa-3.2\automation\test_report.py

ECHO ============================
ECHO GENERATE ALLURE REPORT
ECHO ============================

allure generate C:\temp\allure_report\Sprint_20_Unofficial -c -o C:\Apache24\htdocs


