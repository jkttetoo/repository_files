import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from testlink import TestlinkAPIClient
from Mobile import *
from util import *
import unittest, json, collections
import model
from elevate import elevate
from Ot.PCOM32Manager import *
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4560'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		INGENICO                = model.IngenicoReader()
		LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

		EXTERNAL_ID             = 'DK-4560'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		# Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 10
		# Constants.ITERATIONS_STRESS         = 100 #max 9
		Constants.ITERATIONS_STRESS         = 2 #max 9
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	INGENICO                = model.IngenicoReader()
	LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()

	EXTERNAL_ID             = 'DK-4560'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = True
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3 (DK34NG.P3S2r6)'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP16                = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS            = 25
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

testName = "Loop: {Enable profile, Fallback because network loss}"
test_summary = '''
check the swb retry counter when switchback profile could not attach to a network.

pre-condition:
create + download ISDP 15
configure APF ON, FRM OFF
Configure swb retry 3, timer 15mins
Enable ISDP 15

Steps:
1. Trigger Fallback to ISDP 11
2. Fallback to ISDP 11
3. Timer swb shall start after reset
4. Expire timer swb
5. Switchback to ISDP 15 and could not attach to a network
6. Repeat step #2-#5 3 times


Expected result:
After 3 times, on 4th Fallback to ISDP 11 (step #2) timer swb shall not start
'''

# Constants.TESTLINK_REPORT = True
# Constants.TESTLINK_BUILD = 'build_test_preintegration_jenkins_debugging'

class EnableFallbackNetworkLoss(OtTestUnit):
	EXECUTION_STATUS = None
	result = None
	EXECUTION_DURATION = None
	status_counter=0

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4)
			result, EXECUTION_STATUS = resultPassed(testName, False)

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			# EUICC.init()
			# SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP16, scp=81, apdu_format='definite')
			# SERVER.download_profile(Constants.AID_ISDP16, saipv2_txt=Constants.SAIP_TEST)
			# SERVER.enable_profile(Constants.AID_ISDP16, scp=81, network_service=False, apdu_format='definite')

			EUICC.init()
			# asdasdas
			pprint.h3('Create ISDP 15')
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			
			pprint.h3('Download Test SAIP into ISDP 15')
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
			
			pprint.h3('Configure Switchback mechanism')
			SERVER.ram(GSMA.set_switchback_counter(counter='00'), scp=80)
			SERVER.ram(GSMA.switchback_timer(seconds='00', minutes='51', hours='00'), scp=80)
			
			pprint.h3('Enable ISDP 15')
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='indefinite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))
			
			pprint.h3('Configure FRM off')
			capdu, rapdu, sw = SERVER.frm_off(scp=80, apdu_format='definite', chunk='01')
			
			pprint.h3('Configure APF on')
			capdu, rapdu, sw = SERVER.apf_on(80)
		except:
			exceptionTraceback(position = 1)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

	def test_enable_rollback_network_loss_disable_profile(self):
		h2("testbody")
		timer_start = time()
		try:
			
			EUICC.init()
			data, sw = DEVICE.envelope_location_status()
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			
			########## APF v1 ##########
			# pprint.h3('trigger no service')
			# data, sw = DEVICE.envelope_location_status(status='no service')
			# data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
			# data, sw = DEVICE.envelope_timer_expiration(id='psm')
			# if sw != '9000':
			# 	sw = DEVICE.fetch_all(sw, return_if='refresh')
			##############################

			########## APF v2 ##########
			data, sw = DEVICE.envelope_location_status() 
			if sw[0:2] == '91':
				DEVICE.fetch_all(sw)
				
			pprint.h3('Trigger APF - Limited Service')
			data, sw = DEVICE.envelope_limited_service()

			if sw != '9000': 
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='poll interval')					
				if sw != '9000':
					DEVICE.fetch_all(sw)

			pprint.h3('Trigger STATUS 360x')
			while self.status_counter<=360:	
				self.status_counter+=1
				LogInfo('status {}'.format(self.status_counter))
				data, sw = DEVICE.get_status()

			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='p', pli_result='limited service', open_channel_failed=True, send_sms_failed=True )

			pprint.h3('Check Fallback')
			if sw != '9000':
				self.status_counter=0
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			########################################

			pprint.h3('Fallback to ISDP 11')
			PowerOff()
			PowerOn()
			sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['timer management'])
			# # Used for DK 3.4
			if '240103' in data:
				timer_id = 'psm'
			elif '240104' in data:
				timer_id = 'psm2'
			elif '240105' in data:
				timer_id = 'psm5'

			# # Used for DK 3.4 NG
			# if '240102' in data:
			# 	timer_id = 'psm'
			# elif '240103' in data:
			# 	timer_id = 'psm4'
			# elif '240104' in data:
			# 	timer_id = 'psm5'
			
			if sw != '9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)

			pprint.h3('Audit ISDP')
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')

			pprint.h3('trigger sent notif using status')
			while sw == '9000':
				data, sw = DEVICE.get_status()
			
			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['timer management'])
			# # Used for DK 3.4
			if '240103' in data:
				timer_psm = 'psm'
			elif '240104' in data:
				timer_psm = 'psm2'
			elif '240105' in data:
				timer_psm = 'psm5'

			# # Used for DK 3.4 NG
			# if '240102' in data:
			# 	timer_psm = 'psm'
			# elif '240103' in data:
			# 	timer_psm = 'psm4'
			# elif '240104' in data:
			# 	timer_psm = 'psm5'
						
			pprint.h3('Exhaust All retry sent notif http')
			if sw!='9000':
				for i in range(2):
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
						for i in range(2):
							data, sw = DEVICE.envelope_timer_expiration(id='biplink')
							if sw != '9000':
								data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
						if sw != '9000':
							data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
						if sw != '9000':
							data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					data, sw = DEVICE.envelope_timer_expiration(id=timer_psm)
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					for i in range(2):
						data, sw = DEVICE.envelope_timer_expiration(id='biplink')
						if sw != '9000':
							data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
					if sw != '9000':
						data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
			
			# pprint.h3('Exhaust All retry sent notif sms')
			# if sw!='9000':
			# 	for i in range(2):
			# 		data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
			# 		if sw != '9000':
			# 			data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'], open_channel_failed=True, send_sms_failed=True )
			# 		data, sw = DEVICE.envelope_timer_expiration(id='psm5')

			# for i in range (2):
			for i in range(3):
				pprint.h3('Expired switchback timer')
				data, sw = DEVICE.envelope_timer_expiration(id=timer_id)
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')

				pprint.h3('Switchback to ISDP 15')
				if last_proactive_cmd == 'refresh':
					EUICC.init()
					pprint.h3('Trigger no service')
					for i in range(13):
						data, sw = DEVICE.envelope_location_status(status='no service')
					pprint.h3('Expect to have refresh')
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)

			# for i in range(60):
				pprint.h3('Fallback to ISDP 11')
				PowerOff()
				PowerOn()
				sw = DEVICE.terminal_profile(tp_data = "F71FE8FF 0F 9C00 87 96 00 00 1F 22 60 00 00 C3C0000000014000 40 0000000008")
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw,return_if=['timer management'])
				# # Used for DK 3.4
				if '240103' in data:
					timer_id = 'psm'
				elif '240104' in data:
					timer_id = 'psm2'
				elif '240105' in data:
					timer_id = 'psm5'

				# # Used for DK 3.4 NG
				# if '240102' in data:
				# 	timer_id = 'psm'
				# elif '240103' in data:
				# 	timer_id = 'psm4'
				# elif '240104' in data:
				# 	timer_id = 'psm5'
								
				if sw != '9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)

				pprint.h3('Audit ISDP')
				capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')

				pprint.h3('trigger sent notif using status')
				i=0
				while sw == '9000':
					data, sw = DEVICE.get_status()
					i+=1
					if i == 12:
						break
						
				if sw!='9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'] )
					if last_proactive_cmd == 'open channel' or last_proactive_cmd == 'send sms':
						capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
					else:
						pprint.h3('!!! No notification !!!')

				# pprint.h3('Expired switchback timer shall return 9000')
				# data, sw = DEVICE.envelope_timer_expiration(id=timer_id)
				# if sw != '9000':
				# 	data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if='refresh')
			# i=0
			if last_proactive_cmd != 'refresh':
				self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, False)
		except:
			exceptionTraceback(position = 2)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')
		LogInfo('Result table')
		pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )
		LogInfo('body part')

	def tearDown(self):
		h2("teardown")
		try:
			EUICC.init()
			# command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP, scp=81, apdu_format='definite')
			# DEVICE.get_status()
			EUICC.init()
			capdu, rapdu, sw = SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='definite')
			command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
			DEVICE.get_status()
		except:
			exceptionTraceback(position = 3)
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=self.EXECUTION_DURATION)
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(EnableFallbackNetworkLoss)
