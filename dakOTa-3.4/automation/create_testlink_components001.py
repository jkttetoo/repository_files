from Oberthur import *
from testlink import TestlinkAPIClient
import os, pprint, xmlrpc.client
import datetime

SERVER_URL  = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
DEVKEY      = 'd2ec430274bb865271a34f0e4ceb4594'

USER_NAME                       = 'j.viellepeau@oberthur.com'
PROJECT                         = 'Tests auto from ALM'
PROJECT_PREFIX                  = 'AUTO01'
TEST_PLAN_RAM                   = 'Remote Application Management (RAM)'
TEST_SUITE_HTTPS                = 'HTTPS'
TEST_SUITE_SMS                  = 'SMS'
TEST_CASE_CARD_AUDIT            = 'Card Audit'
PLATFORM_ALM_COMPOSER           = 'ALM 2.2.3 MR5, Java Composer 6.0.8.2'

MANUAL = 1
AUTOMATED = 2

this_file_dirname = os.path.dirname(__file__)
NEWATTACHMENT_PY  = os.path.join(this_file_dirname, 'hello.py')
NEWATTACHMENT_PNG = os.path.join(this_file_dirname, 'example.png')

LJUST = 20

################################################################

BUILD = 'fireFly rev I X.Y.1' # increment if new execution
 
################################################################




LogInfo('\n\nDemo test e2e auto and save results in testlink'.upper())
LogInfo('-----------------------------------------------\n')


LogInfo('1. Preparing TestLink server to store the results...\n')
server = TestlinkAPIClient(SERVER_URL, DEVKEY, verbose=False)

# server.deleteTestProject(PROJECT_PREFIX)

# new = False
# project_id = server.getProjectIDByName(PROJECT)
# if project_id == -1:
#     new = True
#     #LogInfo('Creation of the TestLink project {}:{}'.format(PROJECT_PREFIX, PROJECT))
#     project_id = server.createTestProject(PROJECT, PROJECT_PREFIX, notes='', active=1, public=1, options={'requirementsEnabled' : 0, 'testPriorityEnabled' : 1, 'automationEnabled' : 1, 'inventoryEnabled' : 0})[0]['id']
# LogInfo( 'Project:'.ljust(LJUST) + '{}'.format(project_id).ljust(LJUST) + PROJECT + ' [NEW]' if new else '') 

# new = False
# test_plan = next( ( test_plan for test_plan in server.getProjectTestPlans(project_id) if test_plan['name'] == TEST_PLAN_RAM ), None )
# if not test_plan:
#     new = True
#     #LogInfo('Creation of the test plan {}'.format(TEST_PLAN_RAM))
#     test_plan = server.createTestPlan(TEST_PLAN_RAM, PROJECT, notes='Testing Remote Application Management Over-The-Air thru SCP80 and SCP81', active=1, public=1)[0]
# LogInfo( 'Test plan:'.ljust(LJUST) + '{}'.format(test_plan['id']).ljust(LJUST) + TEST_PLAN_RAM + ' [NEW]' if new else '')

# new = False
# platform = next( ( platform for platform in server.getTestPlanPlatforms(test_plan['id']) if platform['name'] == PLATFORM_ALM_COMPOSER ), None )
# if not platform:
#     new = True
#     #LogInfo('Creation of the platform {}'.format(PLATFORM_ALM_COMPOSER))
#     platform = server.createPlatform(PROJECT, PLATFORM_ALM_COMPOSER, notes='Offline testing environment dedicated to ESF Jakarta Validation and Integration team for MNO projects')
#     assert server.addPlatformToTestPlan(test_plan['id'], PLATFORM_ALM_COMPOSER, notes='')['msg'] == 'link done'
#     #resp = server.addPlatformToTestPlan(test_plan['id'], PLATFORM_ALM_COMPOSER, notes='')
#     #LogInfo(resp)
# LogInfo( 'Platform:'.ljust(LJUST) + '{}'.format(platform['id']).ljust(LJUST) + PLATFORM_ALM_COMPOSER + ' [NEW]' if new else '')

# new = False
# build = next( ( build for build in server.getBuildsForTestPlan(test_plan['id']) if build['name'] == BUILD ), None )
# if not build:
#     new = True
#     build = server.createBuild(test_plan['id'], BUILD, '')[0]
# build_id = build['id']
# LogInfo( 'Build:'.ljust(LJUST) + '{}'.format(build_id).ljust(LJUST) + BUILD + ' [NEW]' if new else '')

# #>>>> TRY EXCEPT IS NECESSRY

# test_suite_https = server.createTestSuite(project_id, TEST_SUITE_HTTPS, 'Bearer SCP81', parentid=None)
# LogInfo(server.getFirstLevelTestSuitesForTestProject(project_id))


# test_suite_https =  next( ( test_suite for test_suite in server.getFirstLevelTestSuitesForTestProject(project_id) if test_suite['name'] == TEST_SUITE_HTTPS ), None )
# if not test_suite_https:
#     test_suite_https = server.createTestSuite(project_id, TEST_SUITE_HTTPS, 'Bearer SCP81', parentid=None)
# LogInfo( 'Test suite:'.ljust(LJUST) + '{}'.format(test_suite_https['id'].ljust(LJUST) + TEST_SUITE_HTTPS) )

# LogInfo('End of test_suite_https')

# tc_version=1
# test_case_card_audit_https = server.getTestCasesForTestPlan(test_plan['id'], platformid=platform['id'])
# LogInfo(test_case_card_audit_https)
# if not test_case_card_audit_https:
#     LogInfo('Create new test case')
#     server.initStep("Open ALM session", "MD connected", AUTOMATED)
#     server.appendStep("Send CardAudit request to ALM for +6281807774546", "ALM ACK", AUTOMATED)
#     server.appendStep("Receive CardAudit Result (async ALM notification)", "List of AID", AUTOMATED)
#     test_case_card_audit_https = server.createTestCase(TEST_CASE_CARD_AUDIT, test_suite_https["id"], project_id, USER_NAME, "Server is requesting a card audit thru SCP81", preconditions='Java Composer is connected to 172.16.220.35:6015')
#     LogInfo(test_case_card_audit_https)
#     test_case_card_audit_https_full_ext_id = server.getTestCase(test_case_card_audit_https[0]["id"])[0]['full_tc_external_id']
#     server.addTestCaseToTestPlan(project_id, test_plan['id'], test_case_card_audit_https_full_ext_id, tc_version, platformid=platform['id'])
# else:
#     test_case_card_audit_https_full_ext_id = server.getTestCase(list( test_case_card_audit_https.keys() )[0])[0]['full_tc_external_id']

#test_case_card_audit_https_full_ext_id = server.getTestCase(test_case_card_audit_https[0]["id"])[0]['full_tc_external_id']
# LogInfo( 'Test case:'.ljust(LJUST) + '{}'.format( list( test_case_card_audit_https.keys() )[0] ).ljust(LJUST) + TEST_CASE_CARD_AUDIT )



#### Known bugs, if zero, it seems to return [{'id': -1}]
#### LogInfo(server.getTestCaseBugs(test_plan['id'], testcaseexternalid=test_case_card_audit_https_full_ext_id))

# timestamp = datetime.datetime.now().strsftime("%Y-%m-%d %H:%M:%S")

# LogInfo(timestamp)
# server.reportTCResult(None, test_plan['id'], None, 'f', '', guess=True, testcaseexternalid=test_case_card_audit_https_full_ext_id, platformname=platform['name'], execduration=3.9, timestamp=timestamp)
# server.reportTCResult(None, '33605', None, 'p', '', guess=True, testcaseexternalid='AUTO01-1', platformname='ALM 2.2.3 MR5, Java Composer 6.0.8.2', execduration=3.9, timestamp=timestamp)


#
## TC AA should be tested with platforms 'Big Birds'+'Small Birds'
#tc_aa_full_ext_id = myTestLink.getTestCase(newTestCaseID_AA)[0]['full_tc_external_id']
#response = myTestLink.addTestCaseToTestPlan(newProjectID, newTestPlanID_A, 
#                    tc_aa_full_ext_id, tc_version, platformid=newPlatFormID_A)
#
#
#test_suite_https_linked_to_test_plan = next( ( test_suite for test_suite in server.getTestSuitesForTestPlan(test_plan['id']) if test_suite['name'] == TEST_SUITE_HTTPS ), None )
#if test_suite_https_linked_to_test_plan:
#    
#
#LogInfo(server.getFirstLevelTestSuitesForTestProject(project_id))
#
#test_suite_https =  next( ( test_suite for test_suite in server.getTestSuitesForTestPlan(test_plan['id']) if test_suite['name'] == TEST_SUITE_HTTPS ), None )
#if not test_suite_https:
#    LogInfo('Creation of the test suite HTTPS')
#    
#    
#
#test_case = server.createTestCase(TEST_CASE_CARD_AUDIT, test_suite_https["id"], project_id, USER_NAME, "Server is requesting a card audit thru SCP81", preconditions='Java Composer is connected to 172.16.220.35:6015')
#    
#LogInfo(test_case)
#    
    

#for test_plans in server.getProjectTestPlans(project_id):
    
    

#if TEST_PLAN_RAM not in server.getProjectTestPlans(project_id):
#    test_plan_id = server.createTestPlan(TEST_PLAN_RAM, PROJECT, notes='Testing Remote Application Management Over-The-Air thru SCP80 and SCP81', active=1, public=1)[0]['id']
#
#
#if PLATFORM_ALM_COMPOSER not in server.getTestPlanPlatforms(test_plan_id):
#    platform_id = server.createPlatform(PROJECT, PLATFORM_ALM_COMPOSER, notes='')['id']
#    server.addPlatformToTestPlan(test_plan_id, PLATFORM_ALM_COMPOSER, notes='')



# print (server.whatArgs('reportTCResult'))


#Get Test Case
# pprint.pprint(server.getTestCase(testcaseexternalid='DAKO-3805', version=1, devKey=DEVKEY)) #<------ Get Test Case

# # Get project_id by name
# project_id = server.getProjectIDByName('Tests auto from ALM')
# print ('Project ID\t\t\t: %s' %project_id)
# #Get TestPlan based on project_id
# projects=server.getProjects()
# for project in projects:
#     if project['id']== project_id:
#         ret = server.getProjectTestPlans(project['id'])
#         testplan_id = ret[0]['name']
#         print ('Testplan ID\t\t\t: %s' %testplan_id)



# #Get platfrom from test_plan_id
# TestPlans = server.getProjectTestPlans(project_id)
# for TestPlan in TestPlans:
#     platforms = server.getTestPlanPlatforms('33605')
#     print ('Test Platform\t\t: %s' %platforms[0]['name'])



# # Get project_id by name
project_id = server.getProjectIDByName('DakOTa v3.1')
print ('Project ID\t\t\t: %s' %project_id)
#Get TestPlan based on project_id
   
# projects=server.getProjects()
# for project in projects:
#     if project['id']== project_id:
#         test_plan = server.getProjectTestPlans(project['id'])
#         testplan_name = test_plan[0]['name']
#         print ('Testplan ID\t\t\t: %s' %testplan_name)
#         #Get platfrom from test_plan_id
#         try:
#             platforms = server.getTestPlanPlatforms(test_plan[0]['id'])
#             platforms = platforms[0]['name']
#         except:
#             platforms = '-'
#         print ('Test Platform\t\t: %s' %platforms)
#         #Get test suites
#         TestSuites = server.getTestSuitesForTestPlan(test_plan[0]['id'])
#         for testsuite in TestSuites:
#             # pprint.pprint(testsuite)
#             print ('Test Suite : {}'.format(testsuite['name']))
#             TestCases = server.getTestCasesForTestSuite(testsuite['id'],'true','full')
#             for testcase in TestCases:
#                 print ('\t',testcase['external_id'], testcase['name'])



# # Get project_id by name
# project_id = server.getProjectIDByName('DakOTa v3.1')
# print ('Project ID\t\t\t: %s' %project_id)
# #Get TestPlan based on project_id
   
# projects=server.getProjects()
# for project in projects:
#     if project['id']== project_id:
#         test_plan = server.getProjectTestPlans(project['id'])
#         testplan_name = test_plan[0]['name']
#         print ('Testplan ID\t\t\t: %s' %testplan_name)
#         #Get platfrom from test_plan_id
#         try:
#             platforms = server.getTestPlanPlatforms(test_plan[0]['id'])
#             platforms = platforms[0]['name']
#         except:
#             platforms = '-'
#         print ('Test Platform\t\t: %s' %platforms)
#         #Get test suites
#         TestSuites = server.getTestSuitesForTestPlan(test_plan[0]['id'])
#         pprint.pprint(TestSuites)
        # for testsuite in TestSuites:
        #     if test
            # pprint.pprint(testsuite)
#             # pprint.pprint (testsuite)
#             # TestCases = server.getTestCasesForTestSuite(testsuite['id'],'true','full')
#             # for testcase in TestCases:
#             #     print ('\t',testcase['external_id'], testcase['name'])



# # Get project_id by name
# project_id = server.getProjectIDByName('DakOTa v3.1')
# print ('Project ID\t\t\t: %s' %project_id)
#Get TestPlan based on project_id

# path = "C:/Users/muhamfir/Desktop/eSIM_for_3.2/db/testlink_db.txt"

# with open (path, 'r') as f:
#     file_string = f.read()

# index_start = file_string.find('=')
# if file_string[79:83] == 'Date':
#     index_start = file_string.find('=', 100)
# file_string = file_string[index_start:]

# print (file_string)

# with open(Oberthur.log.currentLoggingFile, 'w') as f:
#     f.write(file_string)
temp="[\n"

TestCases = server.getTestCasesForTestSuite('52858','true','full')
for testcase in TestCases:
    # pprint.pprint(testcase)
    temp = temp + '{'
    temp = temp + '\n\t"ext_id"\t: "'+testcase['external_id']+'",\n'
    temp = temp + '\t"id"\t\t: "'  +testcase['id']+'",\n'
    temp = temp + '\t"name"\t\t: "'  +testcase['name'] +'",\n'
    temp = temp + '\t"suite"\t\t: "eUICC features "\n'
    temp = temp + '},\n'
    # print(testcase['external_id'],testcase['id'],testcase['name'])
    # print (temp)
temp=temp+']'

print (temp)
# # with open(path, 'w') as f:
#     f.write(temp)