import sys
from Oberthur import *
import os
sys.path.insert(0,os.getcwd()+"\site-packages")
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)

from Mobile import *
from util import *
from time import time
import unittest, json
import model
from time import time


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
			displayAPDU(Constants.displayAPDU)

try:
	if(Constants.RunAll == True):
		SERVER                 = model.Smsr()
		EUICC                  = model.Euicc()
		GSMA                   = model.Gsma()
		TL_SERVER             = model.TestlinkServer()
		DEVICE                  = model.Device()

		EXTERNAL_ID            = 'DK-4271'
		EXECUTION_STATUS    = None
		EXECUTION_DURATION    = None
	else:
		SERVER                 = model.Smsr()
		EUICC                  = model.Euicc()
		GSMA                   = model.Gsma()
		TL_SERVER             = model.TestlinkServer()
		DEVICE                  = model.Device()

		EXTERNAL_ID            = 'DK-4271'
		EXECUTION_STATUS    = None
		EXECUTION_DURATION    = None


		displayAPDU(True)
		SetLogLevel('info')
		#if(Constants.RunAll == True):
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')
		# SetCurrentReader('Dell Smart Card Reader Keyboard 0')


		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT            = False
		Constants.TESTLINK_DEVKEY            = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL                = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME        = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_for_test'
		Constants.TESTLINK_PLATFORM            = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER             = 'm.firmansyah@oberthur.com'


		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                     = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                    = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS            = 1
		Constants.RESULTS                     = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                        = 0
except:
	SERVER                 = model.Smsr()
	EUICC                  = model.Euicc()
	GSMA                   = model.Gsma()
	TL_SERVER             = model.TestlinkServer()
	DEVICE                  = model.Device()

	EXTERNAL_ID            = 'DK-4271'
	EXECUTION_STATUS    = None
	EXECUTION_DURATION    = None


	displayAPDU(True)
	SetLogLevel('info')
	#if(Constants.RunAll == True):
#    SetCurrentReader('OMNIKEY CardMan 3x21 0')
	# SetCurrentReader('Dell Smart Card Reader Keyboard 0')


	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT            = False
	Constants.TESTLINK_DEVKEY            = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL                = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME        = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM            = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER             = 'm.firmansyah@oberthur.com'


	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                     = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                    = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS            = 1
	Constants.RESULTS                     = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                        = 0

class DNSResolution(OtTestUnit):

	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		EUICC.init()
		pprint.h1('nom_pos_002_no_dns_ip_and_two_ip_in_handset')
		
	def test_download_profile(self):

		timer_temp=0
		timer_process_start = time()
		try:
			EUICC.init()
			OTA.spi1 = 0x16
			OTA.spi2 = 0x39
			OTA.kic  = 0x12
			OTA.kid  = 0x12
			capdu, rapdu, sw = OTA.push_sms()
			i=0
			print('=========> #{} attempt'.format(i+1))
			data, sw = Fetch(sw[2:])
			sw = TerminalResponse('8103014000820281820302200038020000')
			for i in range(3):
				data, sw = Fetch(sw[2:])
				TerminalResponse('810301270002028281030100240101')
				data, sw = Envelope('D70C020282812401' + '01' + '2503' +  '001000')
				data, sw = Fetch(sw[2:])
				sw = TerminalResponse('8103014000820281820302200038020000')
				print('=========> #{} attempt'.format(i+2))
				if i>=2:
					data, sw = Fetch(sw[2:])
					TerminalResponse('810301270002028281030100240101')
					data, sw = Envelope('D70C020282812401' + '01' + '2503' +  '001000')
				data, sw = Fetch(sw[2:])
				sw = TerminalResponse('8103014000820281820302200038020000')

			print('=========> Last attempt using previous address')
			data, sw = Fetch(sw[2:])
			TerminalResponse('810301270002028281030100240101')
			data, sw = Envelope('D70C020282812401' + '01' + '2503' +  '001000')
			data, sw = Fetch(sw[2:])
			sw = TerminalResponse('810301400082028182 03022000 38020000')
			print('=========> open channel with command qualifier 09')
			data, sw = Fetch(sw[2:])
			sw = TerminalResponse('81030140098202818203010038028100400A210AE91807210AE91808')
			data, sw = Fetch(sw[2:])
			sw = TerminalResponse('810301400082028182030100')
			i = 0
			for i in range(6):
				data, sw = Fetch(sw[2:])
				sw = TerminalResponse('8103014000820281820302200038020000')
				data, sw = Fetch(sw[2:])
				sw = TerminalResponse('810301270002028281030100240101')
				data, sw = Envelope('D70C020282812401' + '01' + '2503' +  '001000')

			data, sw = Fetch(sw[2:])
			sw = TerminalResponse('81030140008202818203010038028100')
			sw = DEVICE.fetch_all(sw)
			AMDB.remote_management(GP.audit_applet_list, pull=sw, apdu_format='definite')

			EXECUTION_STATUS = 'PASSED'
			Constants.case+=1
		except:
			EXECUTION_STATUS = 'FAILED'
			Constants.case+=1
		LogInfo('body part')
		LogInfo('Download pass with {} s'.format(timer_temp/Constants.ITERATIONS_PERFORMANCE))
		LogInfo('body part')
		timer_process_end = time()
		EXECUTION_DURATION = timer_process_end - timer_process_start
		Constants.RESULTS['test_name'].append('nom_pos_002_no_dns_ip_and_two_ip_in_handset')
		Constants.RESULTS['test_result'].append(timer_temp/Constants.ITERATIONS_PERFORMANCE)
		# LogInfo('Result table')
		# pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )

		if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		if EXECUTION_STATUS != "PASSED":raise ValueError("TEST FAILED")

	def tearDown(self):
		EUICC.init()
		command_script, por, sw = SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='compact')
		DEVICE.get_status()

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(DNSResolution)
