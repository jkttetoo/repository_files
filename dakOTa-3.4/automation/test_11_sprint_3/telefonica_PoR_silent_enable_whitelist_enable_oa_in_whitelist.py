'''
Created on Nov 14, 2017

@author: prastaji

@purpose:
	Test PoR silent = Enable, Whitelist = Enable and OA = in Whitelist, with SPI1 = 16 and 12.
	Download profile SAIP on A0000005591010FFFFFFFF8900001500 and 
	Telefonica on A0000005591010FFFFFFFF8900001600.
	Case 1: check PoR with PoR Silent Enable on Telefonica Profile
	Case 2: check PoR on SAIP Profile with Telefonica OA
	Case 3: check PoR with PoR Silent Enable on Telefonica Profile After Enabling SAIP Profile
	Case 4: check PoR on SAIP Profile with Telefonica OA after delete Telefonica Profile
'''
from Ot.PyCom import *
import sys
from Oberthur import *
#sys.path.insert(0, "../../")
#sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Mobile import *
from util import *
import unittest
import model, const
from time import time
import ADMCommands

__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev/tester'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@IDEMIA.com'

Options.counters={}

if Constants.displayAPDU != None :
	displayAPDU(Constants.displayAPDU)
	
valueError = ["Enable Profile Error",
			  "Different PoR on OTA",
			  "Delete Profile Error"]

flagCaseResult = []

apduSend = [
#        '00000000000000',
		'00A40000023F00',
		'00A40000027F10',
		]

expRSC = {"saip"            :["null", "null"],
		  "telefonica"      :["00", "00"]}
expPoR = {"saip"            :["null", "null"],
		  "telefonica"      :["AF8023029000230290000000", "AF8023029000230290000000"]}

#expCounter = {"saip"        :["", ""], #counter ini yang digunakan
#              "telefonica"  :["0000000000", ""]}

expCounter = {"saip"        :["", ""],
			  "telefonica"  :["", ""]}

isc1=""   
spi1 = [0x16, 0x12]
			
try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr() 
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		OTA                     = model.SCP80()
		TL_SERVER               = model.TestlinkServer()
		
		EXTERNAL_ID             = 'DK-4192'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		OTA                     = model.SCP80()
		TL_SERVER               = model.TestlinkServer()
		
		EXTERNAL_ID             = 'DK-4192'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
		
		displayAPDU(True)
		SetLogLevel('debug')
		
		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = False
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_for_test'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'
		
		'''
		TEST VARIABLES
		
		To launch the test, prepare the following variables : 
		
		Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 
		
		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISD_MSL10             = 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_MSL10                = "saipv2_SD_NOPIN_SCP81_notoken_Expanded_MSL10.der"
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	
	EXTERNAL_ID             = 'DK-4192'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None
	
	displayAPDU(True)
	SetLogLevel('info')
	
	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'
	
	'''
	TEST VARIABLES
	
	To launch the test, prepare the following variables : 
	
	Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 
	
	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISD_MSL10             = 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

class PoRSilent(OtTestUnit):

	def report(self, execution_status, execduration):
		LogInfo('Start reporting test result...')
		TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
		LogInfo('Finished')

	def setUp(self):
		pprint.h1('PoR Silent = Disable, Whitelist = N/A, OA = N/A')
		PowerOn()
		EUICC.init()
		LogInfo("Download Profile SAIP")
		SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
		SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
		
		LogInfo("Download Profile Telefonica")    
		SERVER.create_isdp_and_establish_keyset(Constants.AID_ISD_MSL10, scp=81, apdu_format='definite')
		SERVER.download_profile(Constants.AID_ISD_MSL10, saipv2_txt=Constants.SAIP_MSL10)
		
	def test_por_silent(self):
		LogInfo('body part')
		timer_start = time()
		
		try:
			LogInfo("Enable Profile SAIP")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='definite', chunk='01')
			if "9000" in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))
				isc1 = Constants.config['Key_Value']['ISC1_ISDP']
			else:
				raise ValueError(valueError[0])

			LogInfo("Enable Profile TELEFONICA")
			capdu, rapdu, sw = SERVER.enable_profile(Constants.AID_ISD_MSL10, scp=80, network_service=False, apdu_format='definite', chunk='01',tpoa='0681214365')
			if "9000" in rapdu:
				isc1 = Constants.config['Key_Value']['ISC1_ISDP']
			else:    
				raise ValueError(valueError[0])
			
		except:
			raise ValueError(valueError[0])        
		
		LogInfo("Case 1: check PoR with PoR Silent Enable on Telefonica Profile")
		LogInfo("### Verify PoR Silent Mechanism by verifying lock")
		PowerOn()

		LogInfo("Read Lock")
		ADMCommands.readLock("001D", "01", expectedData = "00")

#        self.checkEFWhitelist("01", "0B812250839AECD2384F0011", "FFFFFFFFFFFFFFFFFFFFFFFF", "9FXX")
		
		EUICC.init()
		
		LogInfo("### TEST SEND OTA ###")
		for i in range (2):
			LogInfo("Case 1 - OTA: "+str(i+1))
			OTA.spi1 = spi1[i]
			OTA.spi2 = 0x21
			OTA.kic  = 0x15
			OTA.kid  = 0x15
			OTA.kic_key = '16161616161616161616161616161616'
			OTA.kid_key = '17171717171717171717171717171717'
			OTA.tar  = 'B00001'
#            OTA.tpoa = '0B812250839AECD2384F0012'            
			OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
			OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
			capdu, por, sw = OTA.remote_management(apduSend,apdu_format='indefinite')
			
			if expRSC.get("telefonica")[i] in Constants.RSC:
				if expPoR.get("telefonica")[i] == por: 
					if expCounter.get("telefonica")[i] in Constants.CntChck: 
						flagCaseResult.append("PASSED")
					else:
						flagCaseResult.append("FAILED")
				else:
					flagCaseResult.append("FAILED")
			else:
				flagCaseResult.append("FAILED")
		
		LogInfo("Case 2: check PoR on SAIP Profile with Telefonica OA")
		try:
			LogInfo("Enable Profile SAIP")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='definite', chunk='01')
			if "9000" in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))
				isc1 = Constants.config['Key_Value']['ISC1_ISDP']
			else:
				raise ValueError(valueError[0])
		except:
			raise ValueError(valueError[0])   
		
		LogInfo("### Verify PoR Silent Mechanism by verifying lock")
		PowerOn()

		LogInfo("Read Lock")
		ADMCommands.readLock("001D", "01", expectedData = "00")
		
#        self.checkEFWhitelist("01", "0B812250839AECD2384F0011", "FFFFFFFFFFFFFFFFFFFFFFFF", "9FXX") #must be error on EF Whitelist IF the ID is different        
		
		EUICC.init()
		
		LogInfo("### TEST SEND OTA ###")
		for i in range (2):
			LogInfo("Case 2 - OTA: "+str(i+1))
			OTA.spi1 = spi1[i]
			OTA.spi2 = 0x21
			OTA.kic  = 0x15
			OTA.kid  = 0x15
			OTA.kic_key = '16161616161616161616161616161616'
			OTA.kid_key = '17171717171717171717171717171717'
			OTA.tar  = 'B00001'
			OTA.tpoa = '0B812250839AECD2384F0012'
			OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
			OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
			capdu, por, sw = OTA.remote_management(apduSend,apdu_format='indefinite')
			
#            if Constants.swEnv == "9000": #Constants.swEnv under ota.remote_management
#                por = "null"
#                Constants.RSC = "null"
				
			if expRSC.get("saip")[i] in Constants.RSC:
				if expPoR.get("saip")[i] == por:  
					if expCounter.get("saip")[i] in Constants.CntChck: 
						flagCaseResult.append("PASSED")
					else: flagCaseResult.append("FAILED")
				else: flagCaseResult.append("FAILED")
			else: flagCaseResult.append("FAILED")                            

		LogInfo("Case 3: check PoR with PoR Silent Enable on Telefonica Profile After Enabling SAIP Profile")
		try:
			LogInfo("### Enable Profile TELEFONICA ###")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISD_MSL10), 81, apdu_format='definite', chunk='01')
			if "9000" in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))
				isc1 = Constants.config['Key_Value']['ISC1_ISDP']
			else:
				raise ValueError(valueError[0])
		except:
			raise ValueError(valueError[0])
		
		LogInfo("### Verify PoR Silent Mechanism by verifying lock")
		PowerOn()

		LogInfo("Read Lock")
		ADMCommands.readLock("001D", "01", expectedData = "00")
		
#        self.checkEFWhitelist("01", "FFFFFFFFFFFFFFFFFFFFFFFF", "FFFFFFFFFFFFFFFFFFFFFFFF", "9FXX")        

		EUICC.init()
		
		LogInfo("### TEST SEND OTA ###")
		for i in range (2):
			LogInfo("Case 3 - OTA: "+str(i+1))
			OTA.spi1 = spi1[i]
			OTA.spi2 = 0x21
			OTA.kic  = 0x15
			OTA.kid  = 0x15
			OTA.kic_key = '16161616161616161616161616161616'
			OTA.kid_key = '17171717171717171717171717171717'
			OTA.tar  = 'B00001'
#            OTA.tpoa = '0B812250839AECD2384F0012'
			OTA.tpoa = '0681214365'
			OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
			OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
			capdu, por, sw = OTA.remote_management(apduSend,apdu_format='indefinite')
			
			if expRSC.get("telefonica")[i] in Constants.RSC:
				if expPoR.get("telefonica")[i] == por:  
					if expCounter.get("telefonica")[i] in Constants.CntChck: 
						flagCaseResult.append("PASSED")
					else: flagCaseResult.append("FAILED") 
				else: flagCaseResult.append("FAILED")
			else: flagCaseResult.append("FAILED")
				
		LogInfo("Case 4: Check PoR on SAIP Profile with Telefonica OA after delete Telefonica Profile")     
		PowerOn()
		
		LogInfo("Read Lock")
		ADMCommands.readLock("001D", "01", expectedData = "00")
		
		EUICC.init()

		try:
			LogInfo("### Disable Profile TELEFONICA ###")
			SERVER.disable_profile(Constants.AID_ISD_MSL10, scp=80, network_service=False, apdu_format='indefinite')
			LogInfo("### Deleted Profile TELEFONICA ###")
			SERVER.delete_profile(Constants.AID_ISD_MSL10, scp=80, apdu_format='definite')
		except:
			raise ValueError(valueError[2])
		
		
		try:
			LogInfo("### Enable Profile SAIP ###")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 81, apdu_format='definite', chunk='01')
			if "9000" in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))
				isc1 = Constants.config['Key_Value']['ISC1_ISDP']
			else:
				raise ValueError(valueError[0])
		except:
			raise ValueError(valueError[0])   
		
		LogInfo("### TEST SEND OTA ###")
		for i in range (2):
			LogInfo("Case 4 - OTA: "+str(i+1))
			OTA.spi1 = spi1[i]
			OTA.spi2 = 0x21
			OTA.kic  = 0x15
			OTA.kid  = 0x15
			OTA.kic_key = '16161616161616161616161616161616'
			OTA.kid_key = '17171717171717171717171717171717'
			OTA.tar  = 'B00001'
			OTA.tpoa = '0B812250839AECD2384F0012'
			OTA.algo_cipher = const.hexa.ciphers['3DES CBC 2 KEYS']
			OTA.algo_crypto_verif = const.hexa.ciphers['3DES CBC 2 KEYS']
			capdu, por, sw = OTA.remote_management(apduSend,apdu_format='indefinite')
			
#            if Constants.swEnv == "9000": #Constants.swEnv under ota.remote_management
#                por = "Null"
#                Constants.RSC = "Null"            
			
			if expRSC.get("saip")[i] in Constants.RSC:
				if expPoR.get("saip")[i] == por:  
					if expCounter.get("saip")[i] in Constants.CntChck: 
						flagCaseResult.append("PASSED")
					else: flagCaseResult.append("FAILED")
				else: flagCaseResult.append("FAILED")
			else: flagCaseResult.append("FAILED")

		timer_end = time()
		EXECUTION_DURATION = timer_end - timer_start
		
		PowerOn()
		LogInfo("Read Lock")
		ADMCommands.readLock("001D", "01", expectedData = "00")
		
		LogInfo("Checking the error result...")
		if "FAILED" in flagCaseResult:
			Constants.RESULTS['test_name'].append('Enable profile SCP81 definite mode')
			Constants.RESULTS['test_result'].append('KO')
			EXECUTION_STATUS = 'FAILED'
			Constants.case+=1  
			LogInfo("FAILED")
		else:
			Constants.RESULTS['test_name'].append('Enable profile SCP81 definite mode')
			Constants.RESULTS['test_result'].append('OK')
			EXECUTION_STATUS = 'PASSED'
			Constants.case+=1
			LogInfo("PASSED")            
			
			
		if Constants.TESTLINK_REPORT: self.report(execution_status=EXECUTION_STATUS, execduration=EXECUTION_DURATION)
								
		if(Constants.RESULTS['test_result'][Constants.case]!="OK"):raise ValueError("TEST FAILED")        
		
		
	def tearDown(self):
		EUICC.init()
		LogInfo("### RESET TO INITIAL CONDITION AFTER PERSO ###")
		
		EUICC.init()
		try:               
			LogInfo("### DISABLE PROFILE AID_ISDP on tearDown ###") 
			SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
			LogInfo("### DELETE PROFILE AID_ISDP on tearDown ###")
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
		except:
			raise ValueError(valueError[2])
		LogInfo("### GET STATUS on tearDown ###") 
		DEVICE.get_status()
		
	def checkEFWhitelist(self):
		LogInfo("Check record EF Whitelist")
		Mobile.Select("3F00", expectedStatus = "9FXX")
		Mobile.Select("7F10", expectedStatus = "9FXX")
		Mobile.Select("8F0A", expectedStatus = "9FXX")
		Mobile.ReadRecord("01", "04", "0C", expectedData = "0B812250839AECD2384F0012")
		Mobile.ReadRecord("02", "04", "0C", expectedData = "0B812250839AECD2384F0012")
		Mobile.ReadRecord("03", "04", "0C", expectedData = "0B812250839AECD2384F0013")

if __name__ == "__main__":
	
	from test import support
	Utilities.launchTest(PoRSilent)
