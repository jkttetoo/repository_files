'''
Created on Nov 17, 2017

@author: prastaji

@purpose: run all Telefonica Features
'''
#from Ot.PyCom import *
#import sys
#from Oberthur import *
#sys.path.insert(0, "../../")
#sys.path.insert(0, "../../lib")
#import configparser
#Constants.config = configparser.ConfigParser()
#Constants.config.read('./esim_tool_configuration.ini')
#
#from Oberthur import *
#from Mobile import *
#from util import *
#from time import time
#import unittest
#import configparser
#import model
#
#import xmlrunner

from Ot.PyCom import *
import sys
from Oberthur import *
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Mobile import *
from util import *
import unittest
import model, const
from time import time
import ADMCommands
import xmlrunner


__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev/tester'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@IDEMIA.com'

SERVER                  = model.Smsr() 
EUICC                   = model.Euicc()
GSMA                    = model.Gsma()
DEVICE                  = model.Device()
OTA                     = model.SCP80()
TL_SERVER               = model.TestlinkServer()

Constants.displayAPDU = True
displayAPDU(Constants.displayAPDU)
SetLogLevel('info')

''' TESTLINK VARIABLES '''
Constants.TESTLINK_REPORT            = False
Constants.TESTLINK_DEVKEY            = 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL               = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME      = 'DakOTa v3.1'
Constants.TESTLINK_BUILD             = 'build_for_test'
Constants.TESTLINK_PLATFORM          = 'IO222'
Constants.TESTLINK_TEST_PLAN         = 'eUICC Pre-integration'
Constants.TESTLINK_TESTER            = 'm.firmansyah@oberthur.com'


'''
TEST VARIABLES

To launch the test, prepare the following variables : 

Constants.AID_ISDP          : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue. 

'''
Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
Constants.AID_ISD_MSL10             = 'A0000005591010FFFFFFFF8900001600'
Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
Constants.SAIP_MSL10                = "saipv2_SD_NOPIN_SCP81_notoken_Expanded_MSL10.der"
Constants.ITERATIONS_PERFORMANCE    = 1
Constants.ITERATIONS_STRESS         = 1
Constants.RESULTS                   = {
                                        'test_name'         : ['Test name'],
                                        'test_result'        : ['Test result']
                                        }
Constants.case                      = 0

'''
PRE-CONDITION:

    1. Set fallback attribut on profile 11
    2. FRM status ON
    3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 12 state PERSONALIZED     --> Pre-created ISDP
4. Profile 13 state PERSONALIZED     --> Pre-created ISDP
5. Profile 14 state PERSONALIZED     --> Pre-created ISDP

=====================================

'''
Constants.case = 0
Constants.RunAll = True

class eSIM_Test(unittest.TestCase):
#    tags = ['prastaji', 'telefonica', 'por_silent']
    def test_PoRSilentWhitelist_1(self):
        print("PoR_silent_disable_whitelist_na_oa_na")
        retValue = Run_ModuleMain('PoR_silent_disable_whitelist_na_oa_na')
        assert retValue == 0
        pass    
        
    def test_PoRSilentWhitelist_2(self):
        print("PoR_silent_enable_whitelist_disable_oa_na")
        retValue = Run_ModuleMain('PoR_silent_enable_whitelist_disable_oa_na')
        assert retValue == 0
        pass         

    def test_PoRSilentWhitelist_3(self):
        print("PoR_silent_enable_whitelist_enable_oa_in_whitelist")
        retValue = Run_ModuleMain('PoR_silent_enable_whitelist_enable_oa_in_whitelist')
        assert retValue == 0
        pass        
    
    def test_PoRSilentWhitelist_4(self):
        print("PoR_silent_enable_whitelist_enable_oa_not_in_whitelist")
        retValue = Run_ModuleMain('PoR_silent_enable_whitelist_enable_oa_not_in_whitelist')
        assert retValue == 0
        pass    
    
if __name__ == '__main__':
    unittest.main(testRunner = xmlrunner.XMLTestRunner(output='C:\\Project\\20. dakOTa 3.2\\Log\\', verbosity=3))
