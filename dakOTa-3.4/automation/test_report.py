from Ot.PyCom import *
import sys
from Oberthur import *
import os
import pytest
import webbrowser

cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)

import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "esim_tool_configuration.ini")
Constants.config.read(fpath)

from Mobile import *
from util import *
from time import time
import unittest
import model

__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'prastoto.aji@idemia.com'


EUICC 					= model.Euicc()
SERVER					= model.Smsr()
DEVICE 					= model.Device()
GSMA					= model.Gsma()
TL_SERVER			   	= model.TestlinkServer()
INGENICO				= model.IngenicoReader()
LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
GLONASS   				= model.EraGlonass()
HSM 					= model.HighStressMemory()
LOCAL_SWAP				= model.LocalSwap()
AUTO_SERVICE = model.AutoService()

''' TESTLINK VARIABLES '''
Constants.TESTLINK_REPORT           = False
Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
Constants.TESTLINK_BUILD            = 'dakota_34_phase_1_http_20190709'
Constants.TESTLINK_PLATFORM         = 'IO222'
Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

'''
TEST VARIABLES

To launch the test, prepare the following variables :

Constants.AID_ISDP 			: It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
Constants.DEFAULT_ISDP10	: It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
Constants.DEFAULT_ISDP11	: It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
Constants.DEFAULT_ISDP12	: It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

'''
Constants.AID_ISDR = "A0000005591010FFFFFFFF89000010"
Constants.AID_ISDP 					= 'A0000005591010FFFFFFFF8900001500'
Constants.AID_ISDP16				= 'A0000005591010FFFFFFFF8900001600'

Constants.AID_ISDP17				= 'A0000005591010FFFFFFFF8900001700'
Constants.AID_ISDP18				= 'A0000005591010FFFFFFFF8900001800'

Constants.DEFAULT_ISDP10			= 'A0000005591010FFFFFFFF8900001000'
Constants.DEFAULT_ISDP11			= 'A0000005591010FFFFFFFF8900001100'
Constants.DEFAULT_ISDP12			= 'A0000005591010FFFFFFFF8900001200'
Constants.DEFAULT_ISDP13			= 'A0000005591010FFFFFFFF8900001300'
Constants.DEFAULT_ISDP14			= 'A0000005591010FFFFFFFF8900001400'
Constants.DEFAULT_ISDP16 = "A0000005591010FFFFFFFF8900001600"
Constants.WRONG_ISDP				= 'A0000005591010FFFFFFFF8900001101'
Constants.SAIP_TEST 				= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.SAIP_HSM					= 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
Constants.SAIP_PCF00				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
Constants.SAIP_PCF01				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
Constants.SAIP_PCF02				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
Constants.SAIP_PCF04				= 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
Constants.SAIP_E2E 					= 'HTTPs_Aspider_SAIPv2.1_Dkt3.x_Elisa_new_re.der'
Constants.ITERATIONS_PERFORMANCE	= 5
Constants.ITERATIONS_STRESS			= 10
Constants.RESULTS 					= {
										'test_name' 		: ['Test name'],
										'test_result'		: ['Test result']
										}

'''
PRE-CONDITION:

	1. Set fallback attribut on profile 11
	2. FRM status ON
	3. Profile status should like follow:

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
3. Profile 12 state PERSONALIZED     --> Pre-created ISDP
4. Profile 13 state PERSONALIZED	 --> Pre-created ISDP
5. Profile 14 state PERSONALIZED	 --> Pre-created ISDP
4
=====================================

'''
Constants.case = 0
Constants.RunAll = True
Constants.testLinkTestCase = []
Constants.testLinkCountRunAll = 0
Constants.debugMode = False
Constants.token_aes_key = ""

# if Constants.TESTLINK_REPORT == False:
# 	Constants.displayAPDU = True
# else:
# 	Constants.displayAPDU = False

Constants.displayAPDU = True
displayAPDU(Constants.displayAPDU)
SetLogLevel('info')

import Ot.GlobalPlatform as GP
from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain, IssuerSecurityDomain, AESKeyset

import automation.test_01_isdr as test_01_isdr
import automation.test_02_mnosd as test_02_mnosd
import automation.test_03_pcf_rule as test_03_pcf_rule
import automation.test_04_lm_applet as test_04_lm_applet
import automation.test_05_performance as test_05_performance
import automation.test_06_stress as test_06_stress
import automation.test_07_download_profile as test_07_download_profile
import automation.test_08_applet_era_glonass as test_08_applet_era_glonass
import automation.test_09_applet_hsm as test_09_applet_hsm
import automation.test_10_applet_location_plugin as test_10_applet_location_plugin
import automation.test_11_sprint_2 as test_11_sprint_2
import automation.test_11_sprint_3 as test_11_sprint_3
import automation.test_12_sprint_4 as test_12_sprint_4
import automation.test_13_sprint_5 as test_13_sprint_5
#sprint 6 NOT created test script
import automation.test_15_sprint_7 as test_15_sprint_7
#sprint 8 NOT created test script
import automation.test_17_sprint_9 as test_17_sprint_9
#sprint 10 NOT created test script
import automation.test_19_sprint_11 as test_19_sprint_11
import automation.test_20_sprint_12 as test_19_sprint_12
#sprint 13 NOT created test script
#sprint 14 NOT created test script
#sprint 15 NOT created test script
#sprint 16 NOT created test script
#sprint 17 NOT created test script
import automation.test_26_sprint_18 as test_26_sprint_18
import automation.test_27_phase_1 as test_27_phase_1
import automation.test_98_euicc_features as test_98_euicc_features
import automation.test_99_dako_issue as test_99_dako_issue

'''
test_08, test_10 dicomment di sprint 12 sampe nanti ntah kapan...
'''

class Preintegration_Test(unittest.TestCase):
	
	def test_get_psm(self):
		PowerOn()
		data, sw = Select('A0000000770107001C0000FE00000E00')
		data, sw = SendAPDU('80CADF6D00')
		data, sw = SendAPDU('80CA3A0700')

	# def test_final_report_to_mail(self):
	# 	# print('wkwk')
	# 	# print('hahaha')
	# 	with open(os.path.abspath("C:/Temp/execute_all_jenkins_dakota_32_new.otl"),'r') as f:
	# 		# print(f.read())
	# 		logs = f.read()
	# 	split_logs = logs.split("==============================================================================")
	# 	i=0
	# 	error = 0
	# 	normal = 0
	# 	failed_test=[]
	# 	for i in range(len(split_logs)):
	# 		if "Error" in split_logs[i]:
	# 			error+=1
	# 			test_name = split_logs[i].split("------------------------------------------------------------------------------")
	# 			failed_test.append(test_name[1].replace(" ",""))
	# 		elif "NORMAL" in split_logs[i]:
	# 			normal+=1
		
	# 	# print ("Total failed test cases : {}".format(error))
	# 	i=0
	# 	failed_test_names = ""
	# 	for i in range(error):
	# 		failed_test_names = failed_test_names + failed_test[i].replace("\n","") + "\n"
	# 	to_be_send = ("" + 
	# 	"This is auto-generated email, please do not reply. \n\n"+
	# 	"Pre-integration test summary:\n"+
	# 	"Total executed test: {} \n"+
	# 	"Total failed test: {} \n"+
	# 	"Total passed test: {} \n\n"+
	# 	"Summary of the failed test: \n{} ").format(normal+error,error,normal,failed_test_names)
	# 	print(to_be_send)

	# 	recipient = ["tedy.putranggono@idemia.com","novella.sitanggang@idemia.com"]
	# 	subject = Constants.TESTLINK_TEST_PLAN + " " + Constants.TESTLINK_BUILD

	# 	# webbrowser.open_new('mailto:'+recipient+'?subject='+subject+'&cc='+cc+'&body='+to_be_send.replace("\n","%0D"))
	# 	import smtplib
	# 	from email.mime.text import MIMEText

		
	# 	# import base64
	# 	# print (base64.b64encode("Bismillah#07")) #Your Password here
	# 	# print (base64.b64decode("UEBzc3cwcmR3dms0ODk3"))

	# 	mailserver = smtplib.SMTP('smtprelay1.emea.oberthurcs.com',25)
	# 	mailserver.ehlo()
	# 	mailserver.starttls()
	# 	# mailserver.login('muhamfir@oberthurcs.com', 'Bismillah#07')
	# 	mailserver.sendmail('hasbi.firmansyah@idemia.com',['hasbi.firmansyah@idemia.com','hasbif.firmansyah39@gmail.com'],to_be_send)
	# 	mailserver.quit()







if __name__ == '__main__':
	unittest.main()
	# print('hahahahaha')
	