'''
    
    @script: checkVersionMPLPackage.py

    @group: SMS_RAM_ImmediateAction 
        
    @author: KHODOR Naim
    
    @date: Octobre 2012

    @purpose: 
            - Case 0 : Creation Key set 5x or 4x over OTA on SD 
            - Case 1 : 
            - Case 
            ------ : Clean all and verify the memory space   
        
    @req: ETSI TS 102226 v11.0.0

    @destructive:  No

'''
from Oberthur.Reader import CardReader
from Ot.PyCom import *



import HttpAmdB
import BipModule
import Tls
import PushSms
import Utilities
import Mobile
import DDL
import DDL_Perturbation
import DDL_HTTP
import DDL_CATTP

#Class instance
#HttpAmdB = HttpAmdB.HttpAmdB()
#BipModule = BipModule.BipModule()
#Tls = Tls.Tls()
#PushSms = PushSms.PushSms()
#Utilities = Utilities.Utilities()
#Mobile = Mobile.Mobile()
#DDL = DDL.DDL()
#DDL_Perturbation = DDL_Perturbation.DDL_Perturbation()
#DDL_HTTP = DDL_HTTP.DDL_HTTP()
#DDL_CATTP = DDL_CATTP()

class checkVersionMPLPackage(OtTestUnit):  
    
    def __init__(self):
        
        self.HttpAmdB = HttpAmdB.__version__
        self.BipModule = BipModule.__version__
        self.Tls = Tls.__version__
        self.PushSms = PushSms.__version__
        self.Utilities = Utilities.__version__
        self.Mobile = Mobile.__version__
        self.DDL = DDL.__version__
        self.DDL_Perturbation = DDL_Perturbation.__version__
        self.DDL_HTTP = DDL_HTTP.__version__
        self.DDL_CATTP = DDL_CATTP.__version__
        
        '''
        Class constructor of the test
        '''
        
        OtTestUnit.__init__(self,methodName='testBody')
        
    def testBody(self): 
        LogInfo("testBody")
        LogInfo("self.HttpAmdB: "+self.HttpAmdB)
        LogInfo("self.BipModule: "+self.BipModule)
        LogInfo("self.Tls: "+self.Tls)
        LogInfo("self.PushSms: "+self.PushSms)
        LogInfo("self.Utilities: "+self.Utilities)
        LogInfo("self.Mobile: "+self.Mobile)
        LogInfo("self.DDL: "+self.DDL)
        LogInfo("self.DDL_Perturbation: "+self.DDL_Perturbation)
        LogInfo("self.DDL_HTTP: "+self.DDL_HTTP)
        LogInfo("self.DDL_CATTP: "+self.DDL_CATTP)
        
#        
    def tearDown(self):
        ''' This method is automatically executed at the end of the test '''

if __name__ == "__main__" :
    
    from test import support
        
    LogInfo("## Creation Key set 5x or 4x over OTA on SD ")
    Utilities.launchTest(checkVersionMPLPackage())