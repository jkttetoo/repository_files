import sys
from Oberthur import *
sys.path.insert(0, "../../../")
sys.path.insert(0, "../../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())
# SetCurrentReader('eSIM 3.1.rc11')


'''
1. Set fallback attribut on profile 10
2. FRM status ON

Profile status
=====================================
1. Profile 11 state ENABLED(FALLBACK)
2. Profile 10 state DISABLED
=====================================

'''


EUICC = model.Euicc()


Constants.DEFAULT_ISDP12		= 'A0000005591010FFFFFFFF8900001200'
Constants.SAIP_TEST 			= 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
Constants.RESULTS 				= {
									'test_name' 		: ['Test name'],
									'test_result'		: ['Test result']
									}
Constants.TESTLINK_REPORT		= False
Constants.TESTLINK_DEVKEY		= 'd2ec430274bb865271a34f0e4ceb4594'
Constants.TESTLINK_URL			= 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
Constants.TESTLINK_PROJECT_NAME	= 'DakOTa v3.1'
Constants.TESTLINK_BUILD		= 'DakOTa v3.2 Build4Test1'
Constants.TESTLINK_PLATFORM		= 'IO222'
Constants.TESTLINK_TEST_PLAN	= 'eUICC Pre-integration'
Constants.TESTLINK_TESTER 		= 'm.firmansyah@oberthur.com'


from test_case.automation.test_02_mnosd import scp80_list_mnosd_compact
from test_case.automation.test_02_mnosd import scp80_list_mnosd_definite
from test_case.automation.test_02_mnosd import scp80_list_mnosd_indefinite
from test_case.automation.test_02_mnosd import scp81_list_mnosd_definite
from test_case.automation.test_02_mnosd import scp81_list_mnosd_indefinite

from test_case.automation.test_02_mnosd import scp80_list_mno_applet_compact
from test_case.automation.test_02_mnosd import scp80_list_mno_applet_definite
from test_case.automation.test_02_mnosd import scp80_list_mno_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_list_mno_applet_definite
from test_case.automation.test_02_mnosd import scp81_list_mno_applet_indefinite

from test_case.automation.test_02_mnosd import scp80_install_applet_compact
from test_case.automation.test_02_mnosd import scp80_install_applet_definite
from test_case.automation.test_02_mnosd import scp80_install_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_install_applet_definite
from test_case.automation.test_02_mnosd import scp81_install_applet_indefinite

from test_case.automation.test_02_mnosd import scp80_lock_applet_compact
from test_case.automation.test_02_mnosd import scp80_lock_applet_definite
from test_case.automation.test_02_mnosd import scp80_lock_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_lock_applet_definite
from test_case.automation.test_02_mnosd import scp81_lock_applet_indefinite

from test_case.automation.test_02_mnosd import scp80_unlock_applet_compact
from test_case.automation.test_02_mnosd import scp80_unlock_applet_definite
from test_case.automation.test_02_mnosd import scp80_unlock_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_unlock_applet_definite
from test_case.automation.test_02_mnosd import scp81_unlock_applet_indefinite

from test_case.automation.test_02_mnosd import scp80_uninstall_applet_compact
from test_case.automation.test_02_mnosd import scp80_uninstall_applet_definite
from test_case.automation.test_02_mnosd import scp80_uninstall_applet_indefinite
from test_case.automation.test_02_mnosd import scp81_uninstall_applet_definite
from test_case.automation.test_02_mnosd import scp81_uninstall_applet_indefinite

from test_case.automation.test_02_mnosd import scp80_update_3g_adn_compact
from test_case.automation.test_02_mnosd import scp80_update_3g_adn_definite
from test_case.automation.test_02_mnosd import scp80_update_3g_adn_indefinite
from test_case.automation.test_02_mnosd import scp81_update_3g_adn_definite
from test_case.automation.test_02_mnosd import scp81_update_3g_adn_indefinite

from test_case.automation.test_02_mnosd import scp80_update_2g_spn_compact
from test_case.automation.test_02_mnosd import scp80_update_2g_spn_definite
from test_case.automation.test_02_mnosd import scp80_update_2g_spn_indefinite





suite = unittest.TestSuite()

# EUICC.init()
# SERVER.download_profile(Constants.DEFAULT_ISDP12, saipv2_txt=Constants.SAIP_TEST)
# capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP12), 80, apdu_format='indefinite', chunk='01')
# if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)

EUICC.init()

''' List profiles '''
suite.addTest(scp80_list_mnosd_compact.suite())
suite.addTest(scp80_list_mnosd_definite.suite())
suite.addTest(scp80_list_mnosd_indefinite.suite())
suite.addTest(scp81_list_mnosd_definite.suite())
suite.addTest(scp81_list_mnosd_indefinite.suite())

''' List applets '''
suite.addTest(scp80_list_mno_applet_compact.suite())
suite.addTest(scp80_list_mno_applet_definite.suite())
suite.addTest(scp80_list_mno_applet_indefinite.suite())
suite.addTest(scp81_list_mno_applet_definite.suite())
suite.addTest(scp81_list_mno_applet_indefinite.suite())

''' Install applet '''
suite.addTest(scp80_install_applet_compact.suite())
suite.addTest(scp80_install_applet_definite.suite())
suite.addTest(scp80_install_applet_indefinite.suite())
suite.addTest(scp81_install_applet_definite.suite())
suite.addTest(scp81_install_applet_indefinite.suite())

''' Lock applet '''
suite.addTest(scp80_lock_applet_compact.suite())
suite.addTest(scp80_lock_applet_definite.suite())
suite.addTest(scp80_lock_applet_indefinite.suite())
suite.addTest(scp81_lock_applet_definite.suite())
suite.addTest(scp81_lock_applet_indefinite.suite())

''' Unlock applet '''
suite.addTest(scp80_unlock_applet_compact.suite())
suite.addTest(scp80_unlock_applet_definite.suite())
suite.addTest(scp80_unlock_applet_indefinite.suite())
suite.addTest(scp81_unlock_applet_definite.suite())
suite.addTest(scp81_unlock_applet_indefinite.suite())

''' Uninstall applet '''
suite.addTest(scp80_uninstall_applet_compact.suite())
suite.addTest(scp80_uninstall_applet_definite.suite())
suite.addTest(scp80_uninstall_applet_indefinite.suite())
suite.addTest(scp81_uninstall_applet_definite.suite())
suite.addTest(scp81_uninstall_applet_indefinite.suite())

''' Update 3G ADN '''

suite.addTest(scp80_update_3g_adn_compact.suite())
suite.addTest(scp80_update_3g_adn_definite.suite())
suite.addTest(scp80_update_3g_adn_indefinite.suite())
suite.addTest(scp81_update_3g_adn_definite.suite())
suite.addTest(scp81_update_3g_adn_indefinite.suite())

''' Update 2G ADN '''
suite.addTest(scp80_update_2g_spn_compact.suite())
suite.addTest(scp80_update_2g_spn_definite.suite())
suite.addTest(scp80_update_2g_spn_indefinite.suite())


unittest.TextTestRunner().run(suite)
pprint.table( [Constants.RESULTS['test_name'], Constants.RESULTS['test_result']] )