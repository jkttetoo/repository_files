'''
PSM connectvity update connectivity sms
'''
from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time



__author__      = 'Prastoto Aji'
__maintainer__  = 'Prastoto Aji'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

Options.counters={}

if Constants.displayAPDU != None :
			displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_POLLER				= model.OtaPoller()
		OTA_SCP80 = model.SCP80()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None
	else:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		OTA_POLLER				= model.OtaPoller()
		OTA_SCP80 = model.SCP80()

		EXTERNAL_ID             = 'DK-4303'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
		Constants.TESTLINK_BUILD            = 'build_test_preintegration'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
		Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()

	EXTERNAL_ID             = 'DK-4303'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = False
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv2v0002.in.idemia.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = 'DakOTa v3.1'
	Constants.TESTLINK_BUILD            = 'build_for_test'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration'
	Constants.TESTLINK_TESTER           = 'm.firmansyah@oberthur.com'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'saipv2_SD_NOPIN_SCP81_notoken_Expanded.der'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

'''
Check updated SMS Connectivity parameters
'''
# smscAddrs = "8606 911011223344"
smscAddrs = "8606 110502100319"
# pid = "810111"
# dcs = "8201F6"
pid = "810148"
dcs = "820148"
dataUpdateCon ="3A07" + berLv("A0" + berLv(smscAddrs + pid + dcs))
# dataUpdateCon ="3A07" + berLv("A0" + berLv(pid + dcs))
installPersoDefISDP = "80 E6 20 00" + lv("0000"+lv(Constants.AID_ISDP)+"000000 00")
apduUpdate = [installPersoDefISDP ,"80 E2 88 00 " +berLv(dataUpdateCon)]

exp_seq_num = "XXXX"
exp_rc_cc_ds = "XXXXXXXXXXXXXXXX"
expDCS_PID = "48"
# expSMSCAddrs = "911011223344"
expSMSCAddrs = "110502100319"

expEnvDataCon = "D0698103011300820281838606" + expSMSCAddrs + "0B" +\
				"5641010D91137900349174F1 "+\
				expDCS_PID + expDCS_PID + "4802700000431502000012000001"+\
				"000000000000" + exp_rc_cc_ds + "E12B"+\
				"4C106364160310000094337F00000000"+\
				"00044D01024E02"+exp_seq_num+"2F10A000000559"+\
				"1010FFFFFFFF8900001500"


expTestResult = "D0698103011300820281838606110502"+\
					  "1003190B5641010D91137900349174F1"+\
					  "4848"

expDataNotifSMS = ["8606110502100319", "4848"]
testName = "PSM Check Update Connectivity"
class CheckUpdateConnectivity(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None	
	
	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4) 			

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			print("dataUpdateCon: ", dataUpdateCon)
			print("apduUpdate: ", apduUpdate)
			EUICC.init()
			h3("disable OTA poller applet")
			data, sw = OTA_POLLER.set_applet_config(interface='applet',applet_state=False, profile_download_event=True, retry_duration=60, retry_number=2)
			EUICC.init()
			pprint.h2("Create ISDP 15")
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP, scp=81, apdu_format='definite')
			pprint.h2("Download Profile")
			SERVER.download_profile(Constants.AID_ISDP, saipv2_txt=Constants.SAIP_TEST)
			pprint.h2("Enable ISDP 15")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				pprint.h2("Refresh ISDP 15")
				sw = SERVER.euicc.refresh(sw, not bool(1))
			capdu, rapdu, sw = SERVER.apf_on(80)
		except:
			exceptionTraceback(position = 1) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

	def test_check_update_connectivity(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			pprint.h2("Update Connectivity Parameters")
			command_script, por, sw = EUICC.mno.ram(apduUpdate, scp=80)

			pprint.h2("Enable ISDP 11")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, False)
			capdu, rapdu, sw = SERVER.apf_on(80)

			pprint.h2("Enable ISDP 15")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				pprint.h2("Refresh")
				if sw is not None:
					DEVICE.fetch_one(sw, 'refresh')
				capdu, rapdu, sw = SERVER.apf_on(80)

				LogInfo('>>> fetch refresh \n')
				EUICC.init()
				pprint.h3("send envelope location status")
				data, sw = Envelope ("D615190103820282811B0100130903022789081614406F", expectedStatus = "91XX")
				LogInfo('envelope location status sw: {}'.format(sw))

				# pprint.h3("Notification SMS")
				# data = Fetch(sw[2:], expectedData = expEnvDataCon , expectedStatus = "9000")
				# dataTr = TerminalResponse("81 03 01 13 00 02 02 82 81 83 01 00", expectedStatus = "91XX")

				# dataNotifSms = data[0][:68]
				# LogInfo("dataNotifSms: "+str(dataNotifSms))
				# LogInfo("expTestResult: "+str(expTestResult))
				# if (dataNotifSms == expTestResult):
				# 	self.result, self.EXECUTION_STATUS = resultPassed(testName, True)	
				# else:
				# 	self.result, self.EXECUTION_STATUS = resultPassed(testName, False)	

				#new checked on sprint 16E
				pprint.h3("Notification SMS")
				data = Fetch(sw[2:], expectedStatus = "9000")
				dataTr = TerminalResponse("81 03 01 13 00 02 02 82 81 83 01 00", expectedStatus = "91XX")

				# LogInfo("data: "+str(data))
				# LogInfo("expDataNotifSMS: "+str(expDataNotifSMS))
				# LogInfo("expDataNotifSMS[0]: "+str(expDataNotifSMS[0]))
				# LogInfo("expDataNotifSMS[1]: "+str(expDataNotifSMS[1]))
				if (expDataNotifSMS[0] in data[0]) and (expDataNotifSMS[1] in data[0]):
					self.result, self.EXECUTION_STATUS = resultPassed(testName, True)	
				else:
					self.result, self.EXECUTION_STATUS = resultPassed(testName, False)	

				seq_number = pprint.pprint_sms_first_notif(data[0])
				data, sw, cmd_type = DEVICE.fetch_all(dataTr)
				pprint.h2('server confirms reception of euicc notification')
				capdu, rapdu, sw = EUICC.isdr.scp80.remote_management(GSMA.handle_notification_confirmation(seq_number))
		except:
			exceptionTraceback(position = 2) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start
		LogInfo('body part')

	def tearDown(self):
		pprint.h3("tearDown")
		try:
			EUICC.init()		
			SERVER.disable_profile(Constants.AID_ISDP, scp=80, network_service=False, apdu_format='indefinite')
			SERVER.delete_profile(Constants.AID_ISDP, scp=80, apdu_format='definite')
			DEVICE.get_status()
			EUICC.init()			
			h3('enable ota poller')
			data, sw = OTA_POLLER.set_applet_config(interface='applet',applet_state=True, profile_download_event=True, retry_duration=60, retry_number=2)
		except:
			exceptionTraceback(position = 3) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

		# if Constants.TESTLINK_REPORT: self.report(execution_status=status, execduration=EXECUTION_DURATION)		
		if(self.result!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(CheckUpdateConnectivity)