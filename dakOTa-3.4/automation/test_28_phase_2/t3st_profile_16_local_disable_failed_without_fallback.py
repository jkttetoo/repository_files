from Ot.PyCom import *
import sys
from Oberthur import *
import os
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../")))
if cmd_subfolder not in sys.path:
	sys.path.insert(0, cmd_subfolder)
import configparser
Constants.config = configparser.ConfigParser()
fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
Constants.config.read(fpath)
from Mobile import *
from util import *
import unittest
import model
from time import time

__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'
'''
__date-creation__ = 20190418
'''

Options.counters={}

if Constants.displayAPDU != None :
			displayAPDU(Constants.displayAPDU)

try:
	if Constants.RunAll == None:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO 					= model.AutoService()

		EXTERNAL_ID             = 'DK-4540'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3(DK34NG.P3S2r10a)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP_AA               = 'A0000005591010FFFFFFFF890000AA00'
		Constants.AID_ISDP_BB               = 'A0000005591010FFFFFFFF890000BB00'
		Constants.AID_ISDP16           		= 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'ProfilePackageDescription_Basic_Test_EWA_profile_Owner_notif_http.txt'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
	elif Constants.RunAll == True:
		SERVER                  = model.Smsr()
		EUICC                   = model.Euicc()
		GSMA                    = model.Gsma()
		DEVICE                  = model.Device()
		TL_SERVER               = model.TestlinkServer()
		AUTO 					= model.AutoService()

		EXTERNAL_ID             = 'DK-4540'
		EXECUTION_STATUS        = None
		EXECUTION_DURATION      = None

		displayAPDU(True)
		SetLogLevel('info')
#        SetCurrentReader('OMNIKEY CardMan 3x21 0')

		''' TESTLINK VARIABLES '''
		Constants.TESTLINK_REPORT           = True
		Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
		Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
		Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
		Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3(DK34NG.P3S2r10a)'
		Constants.TESTLINK_PLATFORM         = 'IO222'
		Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
		Constants.TESTLINK_TESTER           = 'muhamfir'

		'''
		TEST VARIABLES

		To launch the test, prepare the following variables :

		Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
		Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
		Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
		Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

		'''
		Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
		Constants.AID_ISDP_AA               = 'A0000005591010FFFFFFFF890000AA00'
		Constants.AID_ISDP_BB               = 'A0000005591010FFFFFFFF890000BB00'
		Constants.AID_ISDP16           		= 'A0000005591010FFFFFFFF8900001600'
		Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
		Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
		Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
		Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
		Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
		Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
		Constants.SAIP_TEST                 = 'ProfilePackageDescription_Basic_Test_EWA_profile_Owner_notif_http.txt'
		Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
		Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
		Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
		Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
		Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
		Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
		Constants.ITERATIONS_PERFORMANCE    = 1
		Constants.ITERATIONS_STRESS         = 1
		Constants.RESULTS                   = {
												'test_name'         : ['Test name'],
												'test_result'        : ['Test result']
												}
		Constants.case                      = 0
		# Constants.debugMode = False
except:
	SERVER                  = model.Smsr()
	EUICC                   = model.Euicc()
	GSMA                    = model.Gsma()
	DEVICE                  = model.Device()
	TL_SERVER               = model.TestlinkServer()
	AUTO 					= model.AutoService()

	EXTERNAL_ID             = 'DK-4540'
	EXECUTION_STATUS        = None
	EXECUTION_DURATION      = None

	displayAPDU(True)
	SetLogLevel('info')
	SetCurrentReader('OMNIKEY CardMan 3x21 0')

	''' TESTLINK VARIABLES '''
	Constants.TESTLINK_REPORT           = True
	Constants.TESTLINK_DEVKEY           = 'd2ec430274bb865271a34f0e4ceb4594'
	Constants.TESTLINK_URL              = 'http://i2j6serv1v0004:8183/lib/api/xmlrpc/v1/xmlrpc.php'
	Constants.TESTLINK_PROJECT_NAME     = '[CTD] DakOTa 3.x'
	Constants.TESTLINK_BUILD            = 'dk_34_NG_Proto3(DK34NG.P3S2r10a)'
	Constants.TESTLINK_PLATFORM         = 'IO222'
	Constants.TESTLINK_TEST_PLAN        = 'eUICC Pre-integration DK 3.4 NG'
	Constants.TESTLINK_TESTER           = 'muhamfir'

	'''
	TEST VARIABLES

	To launch the test, prepare the following variables :

	Constants.AID_ISDP             : It value should greater than A0000005591010FFFFFFFF8900001400 (A0000005591010FFFFFFFF8900001500), to avoid duplicate ISDP creations.
	Constants.DEFAULT_ISDP10    : It value should refer to the Default ISDP 10 from perso (in This case I use A0000005591010FFFFFFFF8900001000)
	Constants.DEFAULT_ISDP11    : It value should refer to the Default ISDP 11 from perso (in This case I use A0000005591010FFFFFFFF8900001100)
	Constants.DEFAULT_ISDP12    : It value should refer A0000005591010FFFFFFFF8900001200, this AID mean to avoid the tool's internal code issue.

	'''
	Constants.AID_ISDP                  = 'A0000005591010FFFFFFFF8900001500'
	Constants.AID_ISDP_AA               = 'A0000005591010FFFFFFFF890000AA00'
	Constants.AID_ISDP_BB               = 'A0000005591010FFFFFFFF890000BB00'
	Constants.AID_ISDP16           		= 'A0000005591010FFFFFFFF8900001600'
	Constants.DEFAULT_ISDP10            = 'A0000005591010FFFFFFFF8900001000'
	Constants.DEFAULT_ISDP11            = 'A0000005591010FFFFFFFF8900001100'
	Constants.DEFAULT_ISDP12            = 'A0000005591010FFFFFFFF8900001200'
	Constants.DEFAULT_ISDP13            = 'A0000005591010FFFFFFFF8900001300'
	Constants.DEFAULT_ISDP14            = 'A0000005591010FFFFFFFF8900001400'
	Constants.WRONG_ISDP                = 'A0000005591010FFFFFFFF8900001101'
	Constants.SAIP_TEST                 = 'ProfilePackageDescription_Basic_Test_EWA_profile_Owner_notif_http.txt'
	Constants.SAIP_HSM                  = 'saipv2_MyTestDecode_http_Small_M2M_edited.txt'
	Constants.SAIP_PCF00                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol00.der'
	Constants.SAIP_PCF01                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol01.der'
	Constants.SAIP_PCF02                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02.der'
	Constants.SAIP_PCF04                = 'saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol04.der'
	Constants.SAIP_MASTER_DELETE_PCF02 = "saipv2_myscript_testline_testserveur_SAIPV2_SD_NOPIN_pol02_der_add_kvn70.der"
	Constants.ITERATIONS_PERFORMANCE    = 1
	Constants.ITERATIONS_STRESS         = 1
	Constants.RESULTS                   = {
											'test_name'         : ['Test name'],
											'test_result'        : ['Test result']
											}
	Constants.case                      = 0

# import Ot.GlobalPlatform as GP
# from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain, IssuerSecurityDomain, AESKeyset

# kvn70Key = "10029119039605111002911903960511"
# kvn70Data = "70" + "01" + lv("88" + lv(kvn70Key) + "00")
# kvn70Putkey = "80 D8" + kvn70Data

# Constants.token_aes_key = ""

testName = "test_profile_16"
class GSMA4Compliancy(OtTestUnit):
	EXECUTION_STATUS = None
	EXECUTION_DURATION = None
	result_test = None	
	flag_ok = 0
	location_status = True

	def report(self, execution_status, execduration):
		try:
			LogInfo('Start reporting test result...')
			TL_SERVER.report(external_id=EXTERNAL_ID, project_name=Constants.TESTLINK_PROJECT_NAME, testplan_name=Constants.TESTLINK_TEST_PLAN, build_name=Constants.TESTLINK_BUILD, user=Constants.TESTLINK_TESTER, execution_status=execution_status, platform_name=Constants.TESTLINK_PLATFORM, execduration=execduration)
			LogInfo('Finished')
		except:
			exceptionTraceback(position = 4) 			

	def setUp(self):
		pprint.h1(testName)
		h2("setup")
		try:
			EUICC.init()
			pprint.h3("Checking profile AA availability")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='indefinite')
			if 'AA00' in rapdu:
				pass
			else:
				pprint.h3("establish keyset isdp AA")
				SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP_AA, scp=81, apdu_format='definite')
				pprint.h3("download profile AA")
				SERVER.download_profile(Constants.AID_ISDP_AA, saipv2_txt=Constants.SAIP_TEST)

			pprint.h3("Checking profile BB availability")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='indefinite')
			if 'BB00' in rapdu:
				pass
			else:
				pprint.h3("establish keyset isdp BB")
				SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP_BB, scp=81, apdu_format='definite')
				pprint.h3("download profile BB")
				SERVER.download_profile(Constants.AID_ISDP_BB, saipv2_txt=Constants.SAIP_TEST)
			pprint.h3("establish keyset isdp 16")
			SERVER.create_isdp_and_establish_keyset(Constants.AID_ISDP16, scp=81, apdu_format='definite')
			pprint.h3("download profile 16")
			SERVER.download_profile(Constants.AID_ISDP16, saipv2_txt=Constants.SAIP_TEST)
			
			capdu, rapdu, sw = SERVER.apf_on(80)
			capdu, rapdu, sw = SERVER.frm_off(80)

		except:
			exceptionTraceback(position = 1) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

	def test_master_delete(self):
		h2("testbody")
		LogInfo('body part')
		timer_start = time()
		try:
			pprint.h3("Enable Profile ISDP BB")
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.AID_ISDP_BB), 80, apdu_format='definite', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))
				self.flag_ok+=1

			pprint.h3("Audit ISDP list")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
			split_data = rapdu.split('E31C4F10')
			if 'BB00' in split_data[4]:
				if '70013F' in split_data[4]:
					self.flag_ok+=1


			pprint.h3("Set Test Profile to ISDP 16")
			capdu, rapdu, sw = SERVER.ram(GSMA.set_test_profile_attribute(Constants.AID_ISDP16), 80, apdu_format='definite')
			if '9000' in rapdu:
				self.flag_ok+=1

			pprint.h3("Audit ISDP list")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='indefinite')
			split_data = rapdu.split('E31C4F10')
			if '1600' in split_data[5]:
				if '530104' in split_data[5]:
					self.flag_ok+=1


			pprint.h3("Local Enable Test Profile ISDP 16")
			capdu, sw = DEVICE.envelope_local_enable(profile='test')
			if sw!='9000':
				LogInfo('>>> fetch refresh \n')
				data, sw = DEVICE.fetch_one(sw, 'refresh')			
				if sw != '9000':
					sw = DEVICE.fetch_all(sw)
				self.flag_ok+=1
				

			EUICC.init()

			pprint.h3("Audit ISDP list")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='indefinite')
			split_data = rapdu.split('E31C4F10')
			if '1600' in split_data[5]:
				if '70013F' in split_data[5] and '530104' in split_data[5]:
					self.flag_ok+=1


			pprint.h3("Local Disable Test Profile")
			capdu, sw = DEVICE.envelope_local_disable('test')
			if sw!='9000':
				data, sw, last_proactive_cmd = DEVICE.fetch_all(sw)
				EUICC.init()
				data, sw = DEVICE.envelope_location_status(status='no service')
				while sw=='9000':
					data, sw = DEVICE.get_status()

				if sw!='9000':
					data, sw, last_proactive_cmd = DEVICE.fetch_all(sw, return_if=['open channel','send sms'])
					if last_proactive_cmd == 'open channel':	
						capdu, rapdu, sw = EUICC.isdr.scp81.remote_management(GSMA.handle_notification_confirmation(), sw)
						self.flag_ok+=1
						# euicc_notification_sequence_number = EUICC.isdr.scp81.establish_tls_session(sw, notifType = "")
						# if '4D0107' in euicc_notification_sequence_number:
						# 	self.flag_ok+=1
						# notif = 'AE80' + GSMA.handle_notification_confirmation(seq_number=pprint.look_for_euicc_notification(euicc_notification_sequence_number)) + '0000'
						# rapdu = EUICC.isdr.scp81.exchange_application_data(capdu, header_targeted_app=header_targeted_app, content_type=content_type, chunk = chunk)
						# pprint.h4("close tls session")
						# sw = EUICC.isdr.scp81.close_tls_session()

			pprint.h3("Audit ISDP list")
			capdu, rapdu, sw = SERVER.ram(GSMA.audit_isdp_list, 80, apdu_format='definite')
			split_data = rapdu.split('E31C4F10')
			if 'BB00' in split_data[4]:
				if '70013F' in split_data[4]:
					self.flag_ok+=1


			if self.flag_ok == 8:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, True)
			else:
				self.result, self.EXECUTION_STATUS = resultPassed(testName, False)

		except:
			exceptionTraceback(position = 4) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

		timer_end = time()
		self.EXECUTION_DURATION = timer_end - timer_start

		LogInfo('body part')


	def tearDown(self):
		pprint.h2("teardown")
		try:
			pprint.h3("enable isdp 11 (pre-condition)")
			EUICC.init()			
			capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile(Constants.DEFAULT_ISDP11), 80, apdu_format='compact', chunk='01')
			if '9000' in rapdu:
				sw = SERVER.euicc.refresh(sw, not bool(1))

			pprint.h3("set fallback isdp 11 (pre-condition)")
			EUICC.init()
			capdu, rapdu, sw = SERVER.set_fallback_attribute(Constants.DEFAULT_ISDP11, scp=80, apdu_format='definite')

			pprint.h3("set test profile isdp AA (pre-condition)")
			EUICC.init()
			capdu, rapdu, sw = SERVER.ram(GSMA.set_test_profile_attribute(Constants.AID_ISDP_AA), 80, apdu_format='indefinite')

			pprint.h3("disable isdp BB")
			EUICC.init()
			SERVER.disable_profile(Constants.AID_ISDP_BB, scp=80, network_service=False, apdu_format='definite')

			pprint.h3("disable isdp 16")
			EUICC.init()
			SERVER.disable_profile(Constants.AID_ISDP16, scp=80, network_service=False, apdu_format='definite')

			pprint.h3("delete profile BB")
			SERVER.delete_profile(Constants.AID_ISDP_BB, scp=80, apdu_format='compact')

			pprint.h3("delete profile 16")
			SERVER.delete_profile(Constants.AID_ISDP16, scp=80, apdu_format='compact')
		except:
			exceptionTraceback(position = 3) 
			self.result, self.EXECUTION_STATUS = resultPassed(testName, False)			

		if Constants.TESTLINK_REPORT: self.report(execution_status=self.EXECUTION_STATUS, execduration=EXECUTION_DURATION)
		if(self.result!="OK"):raise ValueError("TEST FAILED")

if __name__ == "__main__":

	from test import support
	Utilities.launchTest(GSMA4Compliancy)