from Oberthur import AES_CBC_DEC_128, LogDebug

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellpeau@oberthur.com'

def decipher_por_aes_128(message, key='F8195839102919DEAB908947198438FA'):
	
	PoR = AES_CBC_DEC_128(message, key)

	LogDebug('\n')
	LogDebug('='*20)
	LogDebug('POR CIPHERED: '+message)
	LogDebug('-'*20)
	LogDebug('>> DECIPHERING with AES CBC 128 and KEY={}'.format(key))
	LogDebug('-'*20)
	LogDebug('POR CLEAR: '+ PoR)
	LogDebug('-'*20)
	LogDebug('CNTR: 		'+ PoR[0:10])
	pcntr = PoR[10:12]
	LogDebug('PCNTR:		'+ PoR[10:12])
	LogDebug('RSC: 		'    + PoR[12:14])
	LogDebug('CC:			'+ PoR[14:30])
	LogDebug('USER-DATA: 	'+ PoR[30:])
	LogDebug('='*20)

	return PoR[30:-2*int(pcntr, 16)] if int(pcntr, 16) else PoR[30:]