from Oberthur import *

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

def X9_63_KeyDerivationFunction(X9_63_PARAMETER_ZAB , X9_63_KEY_LENGTH, X9_63_KEY_USAGE , X9_63_SHARED_INFO):
    
    if X9_63_KEY_LENGTH == "10":
        # k is the bit length of the keying data to be generated
        k = 8*16 #  128 bits 
        k_hex = hex(k) 
    #Hash s length ( Amendment E  states : 4.8 Confidential Setup of Secure Channel Keys using ECKA see P 26/35 ) 
    # SHA-256 shall be used for the key derivation to calculate KeyData of sufficient length,which is then assigned to keys as defined below.
    l = 256 
    l_hex = hex(l) 
    # Compute the number of round 
    if X9_63_KEY_USAGE == '10':
        keysNumber = 4 
    elif X9_63_KEY_USAGE == '5C':
        keysNumber = 2 
    
    j = int((keysNumber * k) / l) 
    counter = 1
    Hi = ''
    for i in range(1 , j):        
        temp = X9_63_PARAMETER_ZAB +  '%08x' % counter  + X9_63_SHARED_INFO
        H = SHA256(temp)
        Hi =  H + Hi
        counter +=1
    temp = X9_63_PARAMETER_ZAB +  '%08x' % counter  + X9_63_SHARED_INFO
    Hl = SHA256(temp)
    KeyData = Hi + Hl
    return KeyData

def compute_receipt(message, receipt_key):
    return AES_CMAC_128(message, receipt_key)