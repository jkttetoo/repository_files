__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

def is_short_aid(aid_isdp):
	if isinstance(aid_isdp, int):
		return 'A0000005591010FFFFFFFF890000'+str(aid_isdp)+'00'
	else:
		return aid_isdp

def get_short_aid(aid_isdp):
	# for server1.py

	# if aid_isdp == 'A0000005591010FFFFFFFF890000'+ 'EE' +'00': # so ugly, to hangle not existing isdp !! change that
	# 	return 'EE'

	if isinstance(aid_isdp, int):
		return aid_isdp
	
	else:
		try:
			return int(aid_isdp[-4:-2])  #### ugly !!!!
		except(TypeError, ValueError):
			return aid_isdp[-4:-2]
    
def format_for_http_header_targeted_app(aid_isdp):
	aid_isdp = is_short_aid(aid_isdp)
	return '//aid/A000000559/1010FFFFFFFF' +  aid_isdp[-10:]


def short_isdp(aid_isdp):
	if isinstance(aid_isdp, int):
		return aid_isdp
	else:
		return aid_isdp[-4:-2]