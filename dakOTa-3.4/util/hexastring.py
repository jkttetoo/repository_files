from Oberthur import *
import collections
import string
import os
from util import file

from pathlib import Path

__author__	  = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__	  = 'dev'
__version__	 = '1.0.0'
__email__	   = 'j.viellepeau@oberthur.com'


def parse_tlvs(data, tags_oredered_dict, start):

	data = data[data.index(start) + len(start):]

	result = collections.OrderedDict()

	for name, tag in tags_oredered_dict.items():
		try:
			l = data[data.index(tag)+len(tag): data.index(tag)+len(tag)+2]
			v = data[data.index(tag)+len(tag)+2: data.index(tag)+len(tag)+2+int(l,16)*2]
		except ValueError:
			v = None
		finally:
			result[name] = v
	
	return result 

def get_network_access_name(hexastring):
	result = ''
	# LogInfo("get_network_access_name - hexastring: "+hexastring)
	while(hexastring): 
		length = hexastring[0:2]
		hexaname = hexastring[2: 2+int(hexastring[0:2], 16)*2]
		result += toASCIIString(toByteArray(hexaname))
		hexastring = hexastring[2+len(hexaname):]
		if hexastring: result += '.'
	return result

def subByte(byte, start, end=None):
	if not end: end = start
	return intToHexString(int("{0:08b}".format(byte)[::-1][start-1:end][::-1],2))

def is_matching(pattern, octet):
	
	if len(pattern) > len(octet):
		octet = bin( int(octet, 16) )[2:]

	if octet == '0': octet = '0000'*2

	assert len(octet) == 2 or len(octet) == 8 
	
	result = True

	for i in range( len(pattern) ):
		if pattern[i] is not 'X' and pattern[i] != octet[i]:
			result = False

	return result



def der_to_payloads(saipv2_der_file_name, returnRichPayload=False, withStatus=True, withData=True, defaultExpectedStatus="9000"):
		
	module_profile_static_path  = os.path.dirname(os.path.realpath(__file__)).replace('util', 'profile')
	file_static_path = os.path.join(module_profile_static_path, saipv2_der_file_name)

	payloadList = []
	richPayloadList = []
	persoFile = open(file_static_path, "r")
	lines = persoFile.readlines()
	lineIndex = 1
	# parse perso file: remove spaces then for each line add all char till we meet a non hexa char
	for line in lines :
		#if the line is not empty check it
		addLine = False
		#remove all spaces in the line
		line = "".join(line.split()) 
		# the line still contains End of line chars ("\n") ->min length is: 3 EOL + 2 hex char
		if len(line) > 2 :
			index = 0
			command = ""
			expectedData = ""
			expectedStatus = ""
			catchCommand = True
			catchExpectedData = False
			catchExpectedStatus = False
			for c in line :
				if c in string.hexdigits :
					if (catchCommand == True):
						command += c
				if (catchExpectedData == True):
					expectedData += c
				if (catchExpectedStatus == True):
					expectedStatus += c
				elif c == '[' :
					# data expected start to fill expected data for current line
					if (withData == True) :
						catchCommand = False
						catchExpectedData = True 
						catchExpectedStatus = False
					else : 
						break
				elif c == ']' :
						catchCommand = False
						catchExpectedData = False 
						catchExpectedStatus = False
				# status expected
				elif c == '(' :
					if (withStatus == True) :
						catchCommand = False
						catchExpectedData = False 
						catchExpectedStatus = True
					else : 
						break
					# data expected
				elif c == ')' :
						catchCommand = False
						catchExpectedData = False 
						catchExpectedStatus = False
				else :
					# if string is not hex break loop for c in line
					break
			# end of for loop
			
			if len(command) > 0 :
				addLine = True
			else :
				addLine = False
			#if line seems ok check if it has a possible length
			if (addLine == True) :
				# 1) check command 
				#for each line without comments check if it is hexa
				if (len(command) % 2 == 0) :
					isValidCommand = True
				else :
					isValidCommand = False
					raise ValueError("parsing Error in file " + saipv2_der_file_name + " at line " + str(lineIndex) + " : not a valid Command")
				
				if (len(expectedData) % 2 == 0) :
					isValidExpectedData = True
				else :
					isValidExpectedData = False
					raise ValueError("parsing Error in file " + saipv2_der_file_name + " at line " + str(lineIndex) + " : expected data is not valid")
				   
				if (len(expectedStatus) != 4) or (len(expectedStatus) != 0) :
					isValidExpectedStatus = True
				else :
					isValidExpectedStatus = False
					raise ValueError("parsing Error in file " + saipv2_der_file_name + " at line " + str(lineIndex) + " : expected Status is not valid")

		if addLine :
			payloadList.append(command)
			if not expectedStatus :
				expectedStatus = defaultExpectedStatus
			if (withStatus == True) and (withData == True):
				richPayloadList.append([command, expectedStatus, expectedData])
			else :
				richPayloadList.append([command, expectedStatus])
		lineIndex += 1
		#end for each line
	if returnRichPayload == True :
		return richPayloadList 
	else :
		return payloadList

#def saipv2_to_hexastr(sapiv2_txt):
#
#	module_profile_static_path  = os.path.dirname(os.path.realpath(__file__)).replace('util', 'profile')
#	file_static_path = os.path.join(module_profile_static_path, sapiv2_txt)
#	file_size = os.path.getsize(file_static_path)
#
#	if sapiv2_txt.endswith('.txt'):
#		with open(file_static_path) as f:
#			saipv2_hexastr_raw = f.read()
#			saipv2_hexastr = saipv2_hexastr_raw.replace(' ','').replace('\n','')
##			print(saipv2_hexastr)
#			print("open saipv2 txt")
#	
#	elif sapiv2_txt.endswith('.der'):
#		saipv2_hexastr = StringFromFile(file_static_path)
##		print(saipv2_hexastr)
#		print("open saipv2 der")
#		# saipv2_hexastr = der_to_payloads(sapiv2_txt)
#		# LogInfo('>>> HERE <<<')
#		# LogInfo(saipv2_hexastr)
#
#	return saipv2_hexastr, file_size


def saipv2_to_hexastr(sapiv2_txt, source=None):

	cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../")))		
	if source == None:
		module_profile_static_path  = cmd_subfolder + "\profile"
	elif source =='GTS':
		module_profile_static_path  = cmd_subfolder + "\saip-for-dakota-v4.x"
	print(module_profile_static_path)
	file_static_path = os.path.join(module_profile_static_path, sapiv2_txt)
	print(file_static_path)
	file_size = os.path.getsize(file_static_path)

	if sapiv2_txt.endswith('.txt'):
		with open(file_static_path) as f:
			saipv2_hexastr_raw = f.read()
			saipv2_hexastr = saipv2_hexastr_raw.replace(' ','').replace('\n','')
			print("open saipv2 txt")
	elif sapiv2_txt.endswith('.hex'):
		with open(file_static_path) as f:
			saipv2_hexastr_raw = f.read()
			saipv2_hexastr = saipv2_hexastr_raw.replace(' ','').replace('\n','')
			print("open saipv2 hex")	
	elif sapiv2_txt.endswith('.der'):
		saipv2_hexastr = StringFromFile(file_static_path)
		print("open saipv2 der")
	return saipv2_hexastr, file_size


# def perso_txt_to_capdus(perso_txt):
# 	module_profile_static_path  = os.path.dirname(os.path.realpath(__file__)).replace('util', 'profile')
# 	file_static_path = os.path.join(module_profile_static_path, perso_txt)
# 	with open(file_static_path) as f:
# 		capdus = [line.split('(')[0] for line in f.readlines()]
# 	return capdus

#update by P.A
def perso_txt_to_capdus(perso_txt):
	cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../")))		
	module_profile_static_path  = cmd_subfolder + "\profile"
	file_static_path = os.path.join(module_profile_static_path, perso_txt)
	file_size = os.path.getsize(file_static_path)
	# module_profile_static_path  = os.path.dirname(os.path.realpath(__file__)).replace('util', 'profile')
	# file_static_path = os.path.join(module_profile_static_path, perso_txt)
	with open(file_static_path) as f:
		capdus = [line.split('(')[0] for line in f.readlines()]
	return capdus, file_size

def applet_cmd_to_apdus(applet_cmd):
	cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../")))		
	module_profile_static_path  = cmd_subfolder + "\\"+"applet"	
	file_static_path = os.path.join(module_profile_static_path, applet_cmd)	
	file_size = os.path.getsize(file_static_path)
	
	with open(file_static_path) as f:
		capdus = [line.split('(')[0] for line in f.readlines()]
	
	return capdus, file_size	


def hexastr_to_tlvs(TLVstring, mode=1):
	tmpList = TLVstring
	res = []
	# mode = 0, list of tuples (Tag, Value) 
	if mode == 0:
		while len(tmpList) != 0:
			_OffsetEndCurrTag = (int(tmpList[2:4], 16)+2)*2
			_CurrValue = tmpList[4: _OffsetEndCurrTag ]
			res.append((tmpList[0:2],_CurrValue))
			tmpList = tmpList[_OffsetEndCurrTag:]
	# mode = 1 list de str "TLV"
	if mode == 1:
		while len(tmpList) != 0:
			_OffsetEndCurrTag = (int(tmpList[2:4], 16)+2)*2
			_CurrValue = tmpList[: _OffsetEndCurrTag ]
			res.append(_CurrValue)
			tmpList = tmpList[_OffsetEndCurrTag:]
	return res


def d2h(ordered_dict):
	hexastring = ''
	for tag, val in list(ordered_dict.values()):
		hexastring += tag + lv(val)
	return hexastring


def ip_port_from_base_10_to_16(ip_and_port_str_base_10):
	ip_str_base_10, port_str_base_10 = ip_and_port_str_base_10.split(':')
	ip_str_base_10_four_splits = ip_str_base_10.split('.')
	ip_str_base_16 = ''

	if len(ip_str_base_10_four_splits) != 4:
		return LogError('Expected IPv4 format is X.X.X.X')


	for split_ip_str_base_10 in ip_str_base_10_four_splits:

		try: 
			split_int = int(split_ip_str_base_10)
			split_ip_str_base_16 = intToHexString(split_int) 
		except:
			return LogError('Expected IPv4 format is X.X.X.X with 0 <= X <=255')
	
		ip_str_base_16 += split_ip_str_base_16

	if len(port_str_base_10) > 4: # port can be 5 digits, is not it?
		return LogError('0 <= TCP port <= 9999')

	try:
		port_str_base_16 = intToHexString(int(port_str_base_10),2)
	except:
		return LogError('0 <= TCP port <= 9999 !')

	return ip_str_base_16, port_str_base_16

if __name__ == '__main__':
	is_matching('1XXXX1XX', '82')