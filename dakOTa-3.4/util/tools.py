__author__      = 'AJI Prastoto'
__maintainer__  = 'AJI Prastoto'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'prastoto.aji@idemia.com'

from Oberthur import Options
from Mobile import *
from util import pprint
# from Utilities import *
import Utilities
from Ot.PyCom import *
import os
def readPathEsimToolConfig():
    Constants.config = configparser.ConfigParser()
    fpath = os.path.join(os.path.dirname(__file__), "..\\" + "esim_tool_configuration.ini")
    Constants.config.read(fpath)

def unittestGetResultFailed(unittestResult):
    findFails = "failures=0"
    findErrors = "errors=0"
    if(unittestResult.find(findFails)==-1 or unittestResult.find(findErrors)==-1):
        errorCode = 1 #Find = -1 => String Not found.
    else:
        errorCode = 0
    return errorCode

def getSAAAARCode():
    dataIfdef = Options.default_IFDEF.split(";")
#    for i in range (len(dataIfdef)):
#        print(dataIfdef[i])
    return dataIfdef[0]

def getSAAAAXCode():
    SAAAAXCode = getSAAAARCode().lstrip().replace(" ","")[:5]
    return SAAAAXCode

def exceptionTraceback(position=0):
    if(position == 0):
        pprint.h3("exception")
    elif(position == 1):
        pprint.h3("exception setup")
    elif(position == 2):
        pprint.h3("exception testbody")
    elif(position == 3):
        pprint.h3("exception teardown")
    elif(position == 4):
        pprint.h3("exception report testlink")
    else:
        pprint.h3("exception")
        
    
    tb = traceback.format_exc()
    LogInfo("traceback exception: " + str(tb))

# def resultPassed(testName, testPassed):
#     Constants.RESULTS['test_name'].append(testName)
#     result = "KO"
#     status = "FAILED"

#     if testPassed == True:
#         result = "OK"
#         status = "PASSED"
#     else:
#         result = "KO"
#         status = "FAILED"

#     Constants.RESULTS['test_result'].append(result)
#     EXECUTION_STATUS = status
#     Constants.case += 1
#     return result, status

def resultPassed(testName, isTestPassed, addedResult = True):
    result = "KO"
    status = "FAILED"

    if isTestPassed == True:
        result = "OK"
        status = "PASSED"
    else:
        result = "KO"
        status = "FAILED"

    LogInfo("RESULT: "+status)

    if addedResult == True:
        Constants.RESULTS['test_name'].append(testName)
        Constants.RESULTS['test_result'].append(result)        
        Constants.case += 1
    return result, status

def _testlinkReportChecker(setupNotExcept, testNotExcept, teardownNotExcept):
    setupNotExceptFlag = setupNotExcept
    testNotExceptFlag = testNotExcept
    teardownNotExceptFlag = teardownNotExcept
    reportedPassed = "FAILED"
    if setupNotExceptFlag == True and testNotExceptFlag == True and teardownNotExceptFlag == True:
        reportedPassed = "PASSED"
    
    return reportedPassed

def persoTheCard(sprint = "SPRINT_16;"):
    pathWorkspace = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../")))
    LogInfo("pathWorkspace = "+str(pathWorkspace))
    Utilities.LaunchPerso(pathWorkspace+"/perso/Erase_and_Do_Perso.cmd"+sprint)