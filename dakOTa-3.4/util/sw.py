__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

SW_PoR = {  
       '00': 'PoR OK',
       '01': 'RC/CC/DS failed',
       '02': 'CNTR low',
       '03': 'CNTR high',
       '04': 'CNTR blocked',
       '05': 'Ciphering error',
       '06': 'Unidentified security error. This code is for the case where the Receiving Entity cannot correctly interpret the Command Header and the Response Packet is sent unciphered with no RC/CC/DS',
       '07': 'Insufficient memory to process incoming message',
       '08': 'This status code "more time" should be used if the Receiving Entity/Application needs more time to process the Command Packet due to timing constraints. In this case a later Response Packet should be returned to the Sending Entity once processing has been completed',
       '09': 'TAR Unknown',
       '0A': 'Insufficient security level'
       }

SW_GP221 = {  
       '6200': 'logical channel already closed (warning)'
       '6400': 'no specific diagnosis',
       '6700': 'wrong length in lc',
       '6881': 'logical channel not supported or is not active',
       '6982': 'security status not satisfied',
       '6985': 'conditions of use not satisfied',
       '6A86': 'incorrect p1 p2',
       '6D00': 'invalid instruction',
       '6E00': 'invalid class',
       '6A80': 'incorrect values in command data',
       '6A84': 'not enough memory space',
       '6A88': 'referenced data not found'
       }


SW_ISO_7816_4 = {
       '67XX': 'Wrong length',
       '68XX': 'Functions in CLA not supported',
       '69XX': 'Command not allowed',
       '6AXX': 'Wrong parameters P1-P2',
       '6BXX': 'Wrong parameters P1-P2',
       '6CXX': 'Wrong Le field;',
       '6D00': 'Instruction code not supported or invalid',
       '6E00': 'Class not supported',
       '6F00': 'Command aborted, no precise diagnosis'
}

SW_DAKOTA30_SPECIFIC_OT = {
       '9000': 'Normal ending',
       '6A80': 'Incorrect parameters in the data field',
       '6A88': 'Reference Data not found: pID not found',
       '6985': 'Conditions of use not satisfied: Profile is currently not in a state allowing this request OR Profile is in state Pre-Created OR PiD and ISD-P AID not equal in install command',
       '69E1': 'PCF-Rules do not allow this request',
       '6FD1': 'Bad Cryptogram or no cryptogram (if cryptogram was required)',
       '6200': 'Profile is already in the requested state (blank operation)',
       '6D00': 'Command allowed only for ISD-R',
       '6400': 'Command refused because M-Connect lock is set'
}