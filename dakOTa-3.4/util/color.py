__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

blue 		= '#%02x%02x%02x' % (70, 130, 180)
ot_blue 	= '#%02x%02x%02x' % (35, 136, 207)