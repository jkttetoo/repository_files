import binascii

class TestPython:
	def test_func(self):
		self.data = 'Ω'
		bytes = str.encode(self.data)
		hex_value=binascii.hexlify(bytes)
		hex_string=hex_value.decode(encoding='utf-8')
		return hex_string

if __name__ == "__main__":
	test = TestPython()
	print (test.test_func())