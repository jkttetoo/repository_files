from tkinter import Tk
#import pyperclip when py35!! pyperclip.copy(str) instead of tkinter to access clipboard
import base64

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

folder_name = 'C:/Users/joelviel/Desktop/'
file_name = '2016-08-15 15_21_47-eSIM v0.1'

with open(folder_name+file_name+'.png', 'rb') as f:
	img_b64 = base64.b64encode(f.read())

header = 'data:image/png;base64,'

print(header+img_b64.decode('ascii'))

t = Tk()
t.withdraw()
t.clipboard_clear()
t.clipboard_append(header+img_b64.decode('ascii'))
t.destroy()