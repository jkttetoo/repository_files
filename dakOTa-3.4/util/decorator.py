from Oberthur import intToHexString

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

def allow_short_aid_for_isdp(es5_fn):
    def _es5_fn_decorator(self, aid_isdp, *args):
        if isinstance(aid_isdp, int):
            aid_isdp = 'A0000005591010FFFFFFFF890000'+ intToHexString(aid_isdp) +'00'
        return es5_fn(self, aid_isdp, *args)
    return _es5_fn_decorator