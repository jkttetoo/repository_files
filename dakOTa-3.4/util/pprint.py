from Oberthur import *
from functools import wraps
from const.hexa import *
from terminaltables import AsciiTable
import binascii

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

width = 88

def table(matrix):
    matrix.insert(0, ['#'] + [ i+1 for i in range( len(matrix[0]) ) ] )
    table_data = [list(i) for i in zip( *matrix )]
    table = AsciiTable(table_data)
    LogInfo(table.table)
    
def tableWithPrint(matrix):
    matrix.insert(0, ['#'] + [ i+1 for i in range( len(matrix[0]) ) ] )
    table_data = [list(i) for i in zip( *matrix )]
    table = AsciiTable(table_data)
    print(table.table)    

def dble_line():
    LogInfo(width*'=')

def line():
    LogInfo(width*'-')

def max_width(label, value, every=width):
    lines = []
    
    if len(label) + 1 + len(value) > every:
        
        index_end_first_line = every - len(label) -1
        


        lines.append(value[0:index_end_first_line])

        value = value[index_end_first_line:]

        for i in range(0, len(value), every):
            lines.append(value[i:i+every])
        value = '\n'.join(lines)
    
    LogInfo('{0} {1}'.format(label, value))

def h1(title):
    title = (' ' + title + ' ').replace('_', ' ').upper()
    LogInfo("\n")
    LogInfo(width*'=')
    LogInfo(title.center(width, '='))
    LogInfo(width*'=')

def h2(title):
    title = (' ' + title + ' ').replace('_', ' ').upper()
    LogInfo('')
    LogInfo(title.center(width, '='))
    LogInfo('')
    
def h3(title):
    title = (' ' + title + ' ').replace('_', ' ').upper()
    LogInfo(title.center(width, '-'))
    LogInfo('')

def h4(title):
    title = (' ' + title + ' ').replace('_', ' ').upper()
    LogInfo(title.center(10, '.'))
    LogInfo('')    
    
def pprint_audit(rapdu, display=True):
    
    line()

    isdp_lifecycle = {
                    ''   : 'unknown',
                    '3F' : 'enabled',
                    '1F' : 'disabled',
                    '0F' : 'personalized',
                    '07' : 'selectable',
                    '03' : 'installed'
                  }
    
    isdp_infos = []
    
    tag_start = '4F'

    isdp_enabled = None
    isdp_fallback = None

    rapdu = rapdu[4:] # drop minimum infinite length header

    while(rapdu.find(tag_start) != -1):
        
        isdp_aid_len_base_16    = rapdu[ rapdu.find(tag_start) +2 : rapdu.find(tag_start) +4 ]
        isdp_aid_val            = rapdu[ rapdu.find(tag_start) +4 : rapdu.find(tag_start) +4+2*int(isdp_aid_len_base_16, 16)]

        rapdu = rapdu[rapdu.find(tag_start) +4+2*int(isdp_aid_len_base_16, 16):]
        
        isdp_lifecycle_state    = rapdu[rapdu.find('9F7001')+6:rapdu.find('9F7001')+8]

        if isdp_lifecycle_state == '3F':
            isdp_enabled = isdp_aid_val
        
        isdp_is_fallback        = rapdu[rapdu.find('5301')+4:rapdu.find('5301')+6]

        if isdp_is_fallback=='01':
            isdp_fallback = isdp_aid_val

        isdp_infos.append((isdp_aid_val, isdp_lifecycle_state, 'X' if isdp_is_fallback=='01' else ' '))

    matrix_result = []

    for isdp_info in isdp_infos:
        if display:
            LogInfo('{0} {1} {2} {3}'.format(isdp_info[0], isdp_info[2], isdp_info[1], isdp_lifecycle[isdp_info[1]]))
        matrix_result.append([isdp_info[0], isdp_info[2], isdp_info[1], isdp_lifecycle[isdp_info[1]]])

    return isdp_enabled, isdp_fallback, matrix_result



def pprint_rsc_info(rapdu):
    line()
    
    nb_app_index                = rapdu.find('81', rapdu.find('FF21'))
    non_volatile_memory_index   = rapdu.find('82', rapdu.find('FF21'))
    volatile_memory_index       = rapdu.find('83', rapdu.find('FF21'))
    
    nb_app_len              = rapdu[nb_app_index+2:nb_app_index+2+2]
    non_volatile_len        = rapdu[non_volatile_memory_index+2:non_volatile_memory_index+2+2]
    volatile_len            = rapdu[volatile_memory_index+2:volatile_memory_index+2+2]

    nb_app              = rapdu[nb_app_index+4 : nb_app_index+4 + 2*int(nb_app_len, 16)]
    non_volatile_memory = rapdu[non_volatile_memory_index+4 : non_volatile_memory_index+4 + 2*int(non_volatile_len, 16)]
    volatile_memory     = rapdu[volatile_memory_index+4 : volatile_memory_index+4 + 2*int(volatile_len, 16)]

    LogInfo('NB APP                 : {0}     ({1})'.format(nb_app, int(nb_app, 16)))
    LogInfo('NON VOLATILE MEMORY    : {0} ({1})'.format(non_volatile_memory, int(non_volatile_memory, 16)))
    LogInfo('VOLATILE MEMORY        : {0}   ({1})'.format(volatile_memory, int(volatile_memory, 16)))


def pprint_sms_first_notif(data):
    line()
    LogInfo('sms notification from euicc:')
    LogInfo(data)
    eid_index = data.find('4C10')
    LogInfo('EID \t{0}'.format(data[eid_index+4:eid_index+4+16*2]))
    type_index = data.find('4D01')
    type_hexastring = data[type_index+4:type_index+4+1*2]
    LogInfo('TYPE\t{0} {1}'.format(type_hexastring, EUICC_NOTIFICATION_TYPES[type_hexastring]))
    sequence_number_index = data.find('4E02')
    sequence_number_hexastring = data[sequence_number_index+4:sequence_number_index+4+2*2]
    LogInfo('SEQ#\t{0}'.format(sequence_number_hexastring))
    aid_isdp_index = data.find('2F10')
    LogInfo('ISDP\t{0}'.format(data[aid_isdp_index+4:aid_isdp_index+4+16*2]))
    imei_index = data.find('1408')
    LogInfo('IMEI\t{0}'.format(data[imei_index+4:imei_index+4+8*2]))
    line()
    return sequence_number_hexastring


def border(func):
    fname = func.__name__
    def _decorator(*args, **kwargs):
        print()
        h1(fname)
        response = func(*args, **kwargs)
        dble_line()
        print()
        return response
    return wraps(func)(_decorator)

def string_to_hex(text,apn=False):
    converted_string = ''
    if apn:
        for each in text.split('.'):
            sub_apn_hexa = convert_to_hex(each)
            converted_string += lv(sub_apn_hexa)
    else:
        apn = convert_to_hex(text)
        converted_string += lv(apn)

    LogInfo(converted_string)
    return (converted_string)

def convert_to_hex(string_text):
    bytes = str.encode(string_text)
    hex_value=binascii.hexlify(bytes)
    hex_string=hex_value.decode(encoding='utf-8')
    return hex_string



def look_for_euicc_notification(first_card_request_string, notifType = ""):
    euicc_notification_sequence_number = None    
    if 'msg=' in first_card_request_string:
        first_card_request_splits = first_card_request_string.split('\r\n')
        http_header_cmd = first_card_request_splits[0].split('msg=')[1].split(' HTTP')[0]
        LogInfo('#'*78)
        LogInfo('http notification from euicc:')
        LogInfo('-----------------------------')
        LogInfo(http_header_cmd)
        eid_index = http_header_cmd.find('4C10')
        LogInfo('EID \t{0}'.format(http_header_cmd[eid_index+4:eid_index+4+16*2]))
        type_index = http_header_cmd.find('4D01')
        type_hexastring = http_header_cmd[type_index+4:type_index+4+1*2]
        LogInfo("type_hexastring: "+str(type_hexastring))
        if(notifType!=""):
            assert type_hexastring == notifType
        LogInfo('TYPE\t{0} {1}'.format(type_hexastring, EUICC_NOTIFICATION_TYPES[type_hexastring]))
        sequence_number_index = http_header_cmd.find('4E02')
        sequence_number_hexastring = http_header_cmd[sequence_number_index+4:sequence_number_index+4+2*2]
        LogInfo('SEQ#\t{0}'.format(sequence_number_hexastring))
        euicc_notification_sequence_number = sequence_number_hexastring
        aid_isdp_index = http_header_cmd.find('2F10')
        LogInfo('ISDP\t{0}'.format(http_header_cmd[aid_isdp_index+4:aid_isdp_index+4+16*2]))
        imei_index = http_header_cmd.find('1408')
        if imei_index >= 1:
            LogInfo('IMEI\t{0}'.format(http_header_cmd[imei_index+4:imei_index+4+8*2]))
        else:
            LogInfo('IMEI\tNo IMEI' )
        LogInfo('#'*78)    
    # if 'smsr-uri?msg=' in first_card_request_string:
    #     first_card_request_splits = first_card_request_string.split('\r\n')
    #     http_header_cmd = first_card_request_splits[0].split('smsr-uri?msg=')[1].split(' HTTP')[0]
    #     LogInfo('#'*78)
    #     LogInfo('http notification from euicc:')
    #     LogInfo('-----------------------------')
    #     LogInfo(http_header_cmd)
    #     eid_index = http_header_cmd.find('4C10')
    #     LogInfo('EID \t{0}'.format(http_header_cmd[eid_index+4:eid_index+4+16*2]))
    #     type_index = http_header_cmd.find('4D01')
    #     type_hexastring = http_header_cmd[type_index+4:type_index+4+1*2]
    #     LogInfo('TYPE\t{0} {1}'.format(type_hexastring, EUICC_NOTIFICATION_TYPES[type_hexastring]))
    #     sequence_number_index = http_header_cmd.find('4E02')
    #     sequence_number_hexastring = http_header_cmd[sequence_number_index+4:sequence_number_index+4+2*2]
    #     LogInfo('SEQ#\t{0}'.format(sequence_number_hexastring))
    #     euicc_notification_sequence_number = sequence_number_hexastring
    #     aid_isdp_index = http_header_cmd.find('2F10')
    #     LogInfo('ISDP\t{0}'.format(http_header_cmd[aid_isdp_index+4:aid_isdp_index+4+16*2]))
    #     imei_index = http_header_cmd.find('1408')
    #     if imei_index >= 1:
    #         LogInfo('IMEI\t{0}'.format(http_header_cmd[imei_index+4:imei_index+4+8*2]))
    #     else:
    #         LogInfo('IMEI\tNo IMEI' )
    #     LogInfo('#'*78)
    return euicc_notification_sequence_number

def look_for_dns_request(dns_data_raw):
    data_dns        = dns_data_raw[26:]
    transaction_id  = data_dns[:4]
    dns_flag        = data_dns[4:8]
    dns_header      = data_dns[:24]
    dns_query       = data_dns[24:-10]
    first_length    = dns_query[:2]
    first_limit     = 2+int(first_length,16)*2
    first_query     = dns_query[2:first_limit]

    second_length   = dns_query[first_limit:first_limit+2]
    second_limit    = first_limit + 2 + (int(second_length,16)*2)
    second_query    = dns_query[first_limit+2:second_limit]

    third_length    = dns_query[second_limit:second_limit+2]
    third_limit     = second_limit + 2 + (int(second_length,16)*2)
    third_query     = dns_query[second_limit+2:third_limit]

    data_class      = data_dns[-10:]

    first_query_char  = binascii.unhexlify(first_query).decode("utf-8")
    second_query_char = binascii.unhexlify(second_query).decode("utf-8")
    third_query_char  = binascii.unhexlify(third_query).decode("utf-8")

    LogInfo("\n")
    LogInfo('#'*78)
    LogInfo('dns request from euicc:')
    LogInfo('-----------------------------')
    LogInfo(data_dns)
    LogInfo("DNS transaction id     : {}".format(transaction_id))
    LogInfo("DNS flag               : {}".format(dns_flag))
    LogInfo("DNS query              : {} {} {}".format(first_query_char, second_query_char, third_query_char))
    LogInfo("DNS data class         : {}".format(data_class))
    LogInfo('#'*78)

#add by aji
def log(className, methodName, funcName, debugMode):
    if debugMode:
        # title = (' ' + title + ' ').replace('_', ' ').upper()
        title = " " + className + " || " + methodName + " || " + funcName + " "
        LogInfo(title.center(width, '#'))
        LogInfo('')