import os
from pathlib import Path

__author__      = 'Joel Viellepeau'
__maintainer__  = 'Joel Viellepeau'
__status__      = 'dev'
__version__     = '1.0.0'
__email__       = 'j.viellepeau@oberthur.com'

def db(file_name):
#	path_db_os_path = os.path.dirname(os.path.realpath(__file__)).replace('util', 'db')
#	path_db = os.dirname(os.dirname(os.getcwd()))
#	path_db = str(Path(os.getcwd().replace('test_case','db')).parents[1])
#	path_db = Path(os.getcwd())
	path_db = "C:\ProgramData\Oberthur Technologies\Oberthur_Python_API\db_dakota3"
	joined_path = os.path.join(path_db, file_name)
#	path_db = Path(os.getcwd()).parent.parent.
	return joined_path
#	return os.path.join(path_db, file_name)

def dll(file_name):
	path_db = os.path.dirname(os.path.realpath(__file__)).replace('util', 'dll')
	return os.path.join(path_db, file_name)

def profile(file_name):
	path_db = os.path.dirname(os.path.realpath(__file__)).replace('util', 'profile')
	return os.path.join(path_db, file_name)

def config(file_name):
	path_db = os.path.dirname(os.path.realpath(__file__)).replace('util', 'config')
	return os.path.join(path_db, file_name)

def log_path(file_name):
	path_db = os.path.dirname(os.path.realpath(__file__)).replace('util', 'log')
	return os.path.join(path_db, file_name)