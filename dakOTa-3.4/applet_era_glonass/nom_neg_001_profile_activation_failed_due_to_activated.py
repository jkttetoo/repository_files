import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')

from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model
from Oberthur import OtTestUnit


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER 		= model.Smsr()
EUICC  		= model.Euicc()
GLONASS   	= model.EraGlonass()
DEVICE		= model.Device()


displayAPDU(True)
SetLogLevel('info')
print(GetAllReaders())
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')
SetCurrentReader('eSIM 3.1.rc11')

EUICC.init()
data, sw = GLONASS.set_glonass_profile_via_applet_selection('A0000005591010FFFFFFFF8900001000')
data, sw = GLONASS.glonass_profile_activation_via_applet_selection()
assert sw == '9000', 'Expected to be 9000 - Normal execution'

data, sw = GLONASS.set_glonass_profile_via_applet_selection('A0000005591010FFFFFFFF8900001000')
data, sw = GLONASS.glonass_profile_activation_via_applet_selection()
assert sw == '9020', 'Expected to be 9020 - Glonass profile already active'

EUICC.init()
capdu, data, sw = SERVER.audit_isdp_enabled(scp=80)
assert data[16:48] == 'A0000005591010FFFFFFFF8900001000', 'Profile 10 Expceted to be Enabled'
