import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from Mobile import *
from util import *
from time import time
import unittest, json, collections
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER 					= model.Smsr()
EUICC 					= model.Euicc()
INGENICO    			= model.IngenicoReader()
LOCAL_MANAGEMENT_APPLET = model.LocalManagementApplet()
TL_SERVER 				= model.TestlinkServer()

displayAPDU(False)
SetCurrentReader('Dell Smart Card Reader Keyboard 0')

EUICC.init()
capdu, data, sw = INGENICO.select_applet_local_management(ch_number='00')
capdu, data, sw_activate = LOCAL_MANAGEMENT_APPLET.activate_euicc_notification()
EUICC.init()
timer_start = time()
capdu, data, sw = INGENICO.open_logical_channel(ch_number='01')
capdu, data, sw = INGENICO.select_applet_local_management(ch_number='01')
capdu, data, sw_enable = LOCAL_MANAGEMENT_APPLET.enable_profile('A0000005591010FFFFFFFF8900001000')
capdu, data, sw_closech = INGENICO.close_logical_channel(ch_number='01')
EUICC.refresh(sw=sw_closech)
timer_end = time()
EXECUTION_DURATION = timer_end - timer_start

EUICC.init()
SERVER.disable_profile('A0000005591010FFFFFFFF8900001000', scp=80, network_service=False, apdu_format='indefinite')
print('Era Glonass local swap : {}s'.format(EXECUTION_DURATION))