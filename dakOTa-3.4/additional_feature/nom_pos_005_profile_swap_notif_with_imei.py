import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from Mobile import *
from util import *
from time import time
import unittest, json, collections
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

SERVER 	= model.Smsr()
EUICC 	= model.Euicc()
GSMA	= model.Gsma()

displayAPDU(False)
SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('eSIM 3.1.rc11')

EUICC.init()
SERVER.ram(GSMA.update_notification_with_imei(state='activate'), scp=80, apdu_format='definite')

EUICC.init()
# SERVER.create_isdp_and_establish_keyset('A0000005591010FFFFFFFF8900001500', scp=81, apdu_format='definite')
# SERVER.download_profile('A0000005591010FFFFFFFF8900001500', saipv2_txt='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
capdu, rapdu, sw = SERVER.ram(GSMA.enable_profile('A0000005591010FFFFFFFF8900001500' ), 80, apdu_format='definite', chunk='01')
if '9000' in rapdu: sw = SERVER.euicc.refresh(sw, False)
