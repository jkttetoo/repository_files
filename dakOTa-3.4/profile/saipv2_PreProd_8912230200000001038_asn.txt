rec1value ProfileElement ::= header : {
  major-version 2,
  minor-version 0,
  profileType "Telus SAIP_consumer",
  iccid '8912230200000001038F'H,
  eUICC-Mandatory-services {
    usim NULL,
    milenage NULL,
    javacard NULL
  },
  eUICC-Mandatory-GFSTEList {
    { 2 23 143 1 2 1 }
  }
}
rec2value ProfileElement ::= mf : {
  mf-header {
    mandated NULL,
    identification 0
  },
  templateID { 2 23 143 1 2 1 },
  mf {
    fileDescriptor : {
      fileDescriptor '7821'H,
      fileID '3F00'H,
      lcsi '05'H,
      securityAttributesReferenced '02'H,
      pinStatusTemplateDO '01'H,
      shortEFID ''H
    }
  },
  ef-pl {
    fileDescriptor : {
      fileDescriptor '4121'H,
      fileID '2F05'H,
      lcsi '05'H,
      securityAttributesReferenced '0C'H,
      efFileSize '08'H,
      shortEFID '28'H
    },
    fillFileContent : '656E6672'H
  },
  ef-iccid {
    fileDescriptor : {
      fileDescriptor '4121'H,
      fileID '2FE2'H,
      lcsi '05'H,
      securityAttributesReferenced '0A'H,
      efFileSize '0A'H,
      shortEFID '10'H
    },
    fillFileContent : '982132200000001030F8'H
  },
  ef-dir {
    fileDescriptor : {
      fileDescriptor '42210026'H,
      fileID '2F00'H,
      lcsi '05'H,
      securityAttributesReferenced '0B'H,
      efFileSize '26'H,
      shortEFID 'F0'H
    },
    fillFileContent : '61184F10A0000000871002F110FFFF89B00140FF50045553494D'H
  },
  ef-arr {
    fileDescriptor : {
      fileDescriptor '42210046'H,
      fileID '2F06'H,
      lcsi '05'H,
      securityAttributesReferenced '01'H,
      efFileSize '0348'H,
      shortEFID '30'H
    },
    fillFileContent : '8401D4A40683010A950108800101900080011EA40683010A950108'H,
    fillFileOffset : 43,
    fillFileContent : '8C02A0E89000800107A40683010A950108'H,
    fillFileOffset : 53,
    fillFileContent : '8401D4A40683010A95010880011B9700800104A40683010A950108'H,
    fillFileOffset : 43,
    fillFileContent : '8401D4A40683010A95010880011B9700800104A40683010A950108'H,
    fillFileOffset : 43,
    fillFileContent : '8001019000'H,
    fillFileOffset : 65,
    fillFileContent : '8401D4A40683010A950108800101900080011EA40683010A950108'H,
    fillFileOffset : 43,
    fillFileContent : '8401D4A40683010A95010880011B9700800104A40683010A950108'H,
    fillFileOffset : 43,
    fillFileContent : '80011FA40683010A950108'H,
    fillFileOffset : 59,
    fillFileContent : '8401D4A40683010A950108800107A40683010A9501088001189700'H,
    fillFileOffset : 43,
    fillFileContent : '8401D4A40683010A9501088001019000800102970080015CA40683010A950108'H,
    fillFileOffset : 38,
    fillFileContent : '8401D4A40683010A950108800101900080015EA40683010A950108'H,
    fillFileOffset : 43,
    fillFileContent : '8401D4A40683010A9501088001019000800102A40683010195010880015CA40683010A950108'H
  }
}
rec3value ProfileElement ::= pinCodes : {
  pin-Header {
    mandated NULL,
    identification 1
  },
  pinCodes pinconfig : {
    {
      keyReference pinAppl1,
      pinValue '31323334FFFFFFFF'H,
      unblockingPINReference pukAppl1,
      pinAttributes 6,
      maxNumOfAttemps-retryNumLeft 51
    },
    {
      keyReference adm1,
      pinValue 'AD3F91D2E546CF65'H,
      pinAttributes 3,
      maxNumOfAttemps-retryNumLeft 170
    }
  }
}
rec4value ProfileElement ::= pukCodes : {
  puk-Header {
    mandated NULL,
    identification 2
  },
  pukCodes {
    {
      keyReference pukAppl1,
      pukValue '3730373536373137'H,
      maxNumOfAttemps-retryNumLeft 170
    },
    {
      keyReference secondPUKAppl1,
      pukValue '3535323331313030'H,
      maxNumOfAttemps-retryNumLeft 170
    }
  }
}
rec5value ProfileElement ::= genericFileManagement : {
  gfm-header {
    mandated NULL,
    identification 3
  },
  fileManagementCMD {
    {
      createFCP : {
        fileDescriptor '7821'H,
        fileID '7FDE'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        pinStatusTemplateDO '01'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '1780'H,
        lcsi '05'H,
        securityAttributesReferenced '2F0608'H,
        efFileSize '01'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '42210064'H,
        fileID '1781'H,
        lcsi '05'H,
        securityAttributesReferenced '2F0608'H,
        efFileSize '03E8'H,
        shortEFID ''H
      },
      fillFileContent : '00F1FF71038116F1720802914130961970F47103816A11720802914130961970F47103816B11720802914130961970F47103812A72720802914130961970F67103812B72720802914130961970F67104817B26F6720802914130961970F4'H,
      fillFileOffset : 6,
      fillFileContent : '03022271038116F1720802914130961970F87103816A11720802914130961970F87103816B11720802914130961970F87103812A72720802914130961970F97103812B72720802914130961970F97104817B26F6720802914130961970F4'H,
      fillFileOffset : 6,
      fillFileContent : '03028871038116F1720802914130961970F87103816A11720802914130961970F87103816B11720802914130961970F87103812A72720802914130961970F97103812B72720802914130961970F97104817B26F6720802914130961970F4'H,
      fillFileOffset : 6,
      fillFileContent : '03F2FF71038116F1720802914130961970F87103816A11720802914130961970F87103816B11720802914130961970F87103812A72720802914130961970F97103812B72720802914130961970F97104817B26F6720802914130961970F4'H,
      fillFileOffset : 6,
      fillFileContent : '13F0FF71038116F1720802914130961970F47103816A11720802914130961970F47103816B11720802914130961970F47103812A72720802914130961970F97103812B72720802914130961970F97104817B26F6720802914130961970F4'H,
      fillFileOffset : 6,
      fillFileContent : '13F1FF71038116F1720802914130961970F47103816A11720802914130961970F47103816B11720802914130961970F47103812A72720802914130961970F97103812B72720802914130961970F97104817B26F6720802914130961970F4'H
    }
  }
}
rec6value ProfileElement ::= genericFileManagement : {
  gfm-header {
    mandated NULL,
    identification 4
  },
  fileManagementCMD {
    {
      createFCP : {
        fileDescriptor '7821'H,
        fileID '7FF1'H,
        dfName 'A0000000871002F110FFFF89B00140FF'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        pinStatusTemplateDO '0181'H,
        shortEFID ''H
      }
    }
  }
}
rec7value ProfileElement ::= akaParameter : {
  aka-header {
    mandated NULL,
    identification 5
  },
  algoConfiguration algoParameter : {
    algorithmID milenage,
    algorithmOptions '01'H,
    key '51622F650285C03B27A224CECBED4C77'H,
    opc '0EB09AF2DF02947E46BD3991BF4AC18A'H,
    rotationConstants '4000204060'H,
    xoringConstants '0000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000020000000000000000000000000000000400000000000000000000000000000008'H
  },
  sqnOptions '02'H,
  sqnDelta '000010000000'H,
  sqnAgeLimit '000000040000'H,
  sqnInit {
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H,
    '000000000000'H
  }
}
rec8value ProfileElement ::= pinCodes : {
  pin-Header {
    mandated NULL,
    identification 6
  },
  pinCodes pinconfig : {
    {
      keyReference secondPINAppl1,
      pinValue '31323334FFFFFFFF'H,
      unblockingPINReference secondPUKAppl1,
      pinAttributes 3,
      maxNumOfAttemps-retryNumLeft 51
    }
  }
}
rec9value ProfileElement ::= genericFileManagement : {
  gfm-header {
    mandated NULL,
    identification 7
  },
  fileManagementCMD {
    {
      filePath : '7FF1'H,
      createFCP : {
        fileDescriptor '4221004B'H,
        fileID '6F06'H,
        lcsi '05'H,
        securityAttributesReferenced '01'H,
        efFileSize '0339'H,
        shortEFID 'B8'H
      },
      fillFileContent : '8401D4A40683010A950108800101900080011EA40683010A950108'H,
      fillFileOffset : 48,
      fillFileContent : '8401D4A40683010A950108800101A40683010195010880015EA40683010A950108'H,
      fillFileOffset : 42,
      fillFileContent : '8401D4A40683010A950108800103A40683010195010880015CA40683010A950108'H,
      fillFileOffset : 42,
      fillFileContent : '8401D4A40683010A950108800101900080015EA40683010A950108'H,
      fillFileOffset : 48,
      fillFileContent : '8401D4A40683010A9501088001019000800102A40683010195010880015CA40683010A950108'H,
      fillFileOffset : 37,
      fillFileContent : '800101A40683010195010880011EA40683010A950108'H,
      fillFileOffset : 53,
      fillFileContent : '8C02A0E89000800107A40683010A950108'H,
      fillFileOffset : 58,
      fillFileContent : '8401D4A40683010A950108800101A406830101950108800102A40683018195010880015CA40683010A950108'H,
      fillFileOffset : 31,
      fillFileContent : '8401D4A40683010A95010880011B9700800144A40683010A950108'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F07'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '09'H,
        shortEFID '38'H
      },
      fillFileContent : '083920220083081030'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F7E'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '0B'H,
        shortEFID '58'H
      },
      fillFileOffset : 4,
      fillFileContent : '0302220000FF01'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F73'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '0E'H,
        shortEFID '60'H
      },
      fillFileOffset : 7,
      fillFileContent : '0302220000FF01'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6FAD'H,
        lcsi '05'H,
        securityAttributesReferenced '04'H,
        efFileSize '04'H,
        shortEFID '18'H
      },
      fillFileContent : '00000003'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F78'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '02'H,
        shortEFID '30'H
      },
      fillFileContent : '0008'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6FC4'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize 'DA'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F05'H,
        lcsi '05'H,
        securityAttributesReferenced '05'H,
        efFileSize '08'H,
        shortEFID '10'H
      },
      fillFileContent : '656E6672'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F38'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '0E'H,
        shortEFID '20'H
      },
      fillFileContent : '184A172C27CE0500400010000000'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F08'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '21'H,
        shortEFID '40'H
      },
      fillFileContent : '07'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F09'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '21'H,
        shortEFID '48'H
      },
      fillFileContent : '07'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F5B'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '06'H,
        shortEFID '78'H
      },
      fillFileContent : 'F00000F00000'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F5C'H,
        lcsi '05'H,
        securityAttributesReferenced '06'H,
        efFileSize '03'H,
        shortEFID '80'H
      }
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F3E'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '02'H,
        shortEFID ''H
      },
      fillFileContent : '5455'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F3F'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '02'H,
        shortEFID ''H
      },
      fillFileContent : 'FFFF'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F7B'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '0C'H,
        shortEFID '68'H
      }
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F61'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '0140'H,
        shortEFID '88'H,
        proprietaryEFInfo {
          specialFileInformation '00'H,
          repeatPattern 'FFFFFF0000'H
        }
      },
      fillFileContent : '030222C000030288C000030222C000030288C000030222C000030288C000030222C000030288C000030222C000030288C000030227C000'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F62'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '32'H,
        shortEFID '98'H
      },
      fillFileContent : '03022240000302228000FFFFFF0000FFFFFF0000FFFFFF0000FFFFFF0000FFFFFF0000FFFFFF0000FFFFFF0000FFFFFF0000'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F31'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '01'H,
        shortEFID '90'H
      },
      fillFileContent : '05'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F46'H,
        lcsi '05'H,
        securityAttributesReferenced '04'H,
        efFileSize '11'H,
        shortEFID ''H
      },
      fillFileContent : '03'H
    },
    {
      createFCP : {
        fileDescriptor '42210004'H,
        fileID '6FB7'H,
        lcsi '05'H,
        securityAttributesReferenced '04'H,
        efFileSize '14'H,
        shortEFID '08'H
      },
      fillFileContent : '19F1FF00'H,
      fillFileContent : '11F2FF00'H
    },
    {
      createFCP : {
        fileDescriptor '42210022'H,
        fileID '6F40'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '66'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '42210020'H,
        fileID '6F49'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize 'A0'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '4221000D'H,
        fileID '6F4C'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '0D'H,
        shortEFID ''H
      },
      fillFileContent : '00'H
    },
    {
      createFCP : {
        fileDescriptor '422100B0'H,
        fileID '6F3C'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '06E0'H,
        shortEFID ''H,
        proprietaryEFInfo {
          specialFileInformation '00'H,
          fillPattern '00FF'H
        }
      }
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F43'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '02'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '4221002B'H,
        fileID '6F42'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '81'H,
        shortEFID ''H
      },
      fillFileContent : '534D532043656E747265FFFFFFFFFFEDFFFFFFFFFFFFFFFFFFFFFFFF07916174850071F2FFFFFFFFFFFFAA'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F45'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '14'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6FE3'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '12'H,
        shortEFID 'F0'H
      },
      fillFileOffset : 12,
      fillFileContent : '030222000001'H
    },
    {
      createFCP : {
        fileDescriptor '42210036'H,
        fileID '6FE4'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '36'H,
        shortEFID 'C0'H
      }
    },
    {
      createFCP : {
        fileDescriptor '4221001E'H,
        fileID '6FC7'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '78'H,
        shortEFID ''H
      },
      fillFileContent : '566F6963656D61696CFFFFFFFFFFFFFF07919120854000F1'H
    },
    {
      createFCP : {
        fileDescriptor '4221000D'H,
        fileID '6FC8'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '34'H,
        shortEFID ''H
      },
      fillFileContent : '00'H,
      fillFileOffset : 12,
      fillFileContent : '00'H,
      fillFileOffset : 12,
      fillFileContent : '00'H,
      fillFileOffset : 12,
      fillFileContent : '00'H
    },
    {
      createFCP : {
        fileDescriptor '42210004'H,
        fileID '6FC9'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '10'H,
        shortEFID ''H,
        proprietaryEFInfo {
          specialFileInformation '00'H,
          fillPattern '00'H
        }
      },
      fillFileContent : '01'H
    },
    {
      createFCP : {
        fileDescriptor '42210005'H,
        fileID '6FCA'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '14'H,
        shortEFID ''H,
        proprietaryEFInfo {
          specialFileInformation '00'H,
          fillPattern '00'H
        }
      }
    },
    {
      createFCP : {
        fileDescriptor '42210010'H,
        fileID '6FCB'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '40'H,
        shortEFID ''H
      },
      fillFileContent : '0100'H,
      fillFileOffset : 14,
      fillFileContent : '0200'H,
      fillFileOffset : 14,
      fillFileContent : '0300'H,
      fillFileOffset : 14,
      fillFileContent : '0400'H
    },
    {
      createFCP : {
        fileDescriptor '7821'H,
        fileID '5F3B'H,
        lcsi '05'H,
        securityAttributesReferenced '07'H,
        pinStatusTemplateDO '0181'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '4F20'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '09'H,
        shortEFID '08'H
      },
      fillFileOffset : 8,
      fillFileContent : '07'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '4F52'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '09'H,
        shortEFID '10'H
      },
      fillFileOffset : 8,
      fillFileContent : '07'H
    },
    {
      filePath : '7FF1'H,
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6FD9'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '3C'H,
        shortEFID 'E8'H
      },
      fillFileContent : '030222030288'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F56'H,
        lcsi '05'H,
        securityAttributesReferenced '08'H,
        efFileSize '02'H,
        shortEFID '28'H
      },
      fillFileContent : '0000'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6F57'H,
        lcsi '05'H,
        securityAttributesReferenced '08'H,
        efFileSize '64'H,
        shortEFID ''H
      },
      fillFileContent : '00'H
    },
    {
      createFCP : {
        fileDescriptor '4121'H,
        fileID '6FCD'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        efFileSize '13'H,
        shortEFID 'D8'H
      },
      fillFileContent : 'A311800F'H
    },
    {
      createFCP : {
        fileDescriptor '4221000D'H,
        fileID '6F4E'H,
        lcsi '05'H,
        securityAttributesReferenced '03'H,
        efFileSize '41'H,
        shortEFID ''H
      },
      fillFileContent : '00'H,
      fillFileOffset : 12,
      fillFileContent : '00'H,
      fillFileOffset : 12,
      fillFileContent : '00'H,
      fillFileOffset : 12,
      fillFileContent : '00'H,
      fillFileOffset : 12,
      fillFileContent : '00'H
    },
    {
      createFCP : {
        fileDescriptor '42210018'H,
        fileID '6FC5'H,
        lcsi '05'H,
        securityAttributesReferenced '04'H,
        efFileSize '01E0'H,
        shortEFID 'C8'H
      },
      fillFileContent : '430685D422B33A05'H,
      fillFileOffset : 16,
      fillFileContent : '430483452C15'H
    },
    {
      createFCP : {
        fileDescriptor '42210008'H,
        fileID '6FC6'H,
        lcsi '05'H,
        securityAttributesReferenced '04'H,
        efFileSize '0190'H,
        shortEFID 'D0'H
      },
      fillFileContent : '0302220000FFFE01'H,
      fillFileContent : '0302880000FFFE01'H,
      fillFileContent : '0302270000FFFE02'H
    },
    {
      filePath : ''H,
      createFCP : {
        fileDescriptor '7821'H,
        fileID '7F10'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        pinStatusTemplateDO '01'H,
        shortEFID ''H
      }
    },
    {
      createFCP : {
        fileDescriptor '4221004B'H,
        fileID '6F06'H,
        lcsi '05'H,
        securityAttributesReferenced '01'H,
        efFileSize '02EE'H,
        shortEFID ''H
      },
      fillFileContent : '8401D4A40683010A950108800101900080011EA40683010A950108'H
    },
    {
      filePath : ''H,
      createFCP : {
        fileDescriptor '7821'H,
        fileID '7F20'H,
        lcsi '05'H,
        securityAttributesReferenced '02'H,
        pinStatusTemplateDO '01'H,
        shortEFID ''H
      }
    }
  }
}
rec10value ProfileElement ::= securityDomain : {
  sd-Header {
    mandated NULL,
    identification 8
  },
  instance {
    applicationLoadPackageAID 'A0000001515350'H,
    classAID 'A000000151535041'H,
    instanceAID 'A000000151000000'H,
    extraditeSecurityDomainAID 'A000000151000000'H,
    applicationPrivileges '82FC80'H,
    lifeCycleState '0F'H,
    applicationSpecificParametersC9 '81020255810280008201F0'H,
    applicationParameters {
      uiccToolkitApplicationSpecificParametersField '010010000002011203B2010000'H
    }
  },
  keyList {
    {
      keyUsageQualifier '38'H,
      keyAccess '00'H,
      keyIdentifier '01'H,
      keyVersionNumber '01'H,
      keyCompontents {
        {
          keyType '80'H,
          keyData '2A3E22E7B17D71003106CCC025049D66'H,
          macLength 8
        }
      }
    },
    {
      keyUsageQualifier '34'H,
      keyAccess '00'H,
      keyIdentifier '02'H,
      keyVersionNumber '01'H,
      keyCompontents {
        {
          keyType '80'H,
          keyData 'DCC755045CF1E140F0ED36BCD2E41EAD'H,
          macLength 8
        }
      }
    },
    {
      keyUsageQualifier 'C8'H,
      keyAccess '00'H,
      keyIdentifier '03'H,
      keyVersionNumber '01'H,
      keyCompontents {
        {
          keyType '80'H,
          keyData '728F012A07A4FBABB1CEB29AFA5CE00C'H,
          macLength 8
        }
      }
    },
    {
      keyUsageQualifier '38'H,
      keyAccess '00'H,
      keyIdentifier '01'H,
      keyVersionNumber '20'H,
      keyCompontents {
        {
          keyType '80'H,
          keyData 'D19C18B468707A56B51C0869E1FC80DB'H,
          macLength 8
        }
      }
    },
    {
      keyUsageQualifier '34'H,
      keyAccess '00'H,
      keyIdentifier '02'H,
      keyVersionNumber '20'H,
      keyCompontents {
        {
          keyType '80'H,
          keyData 'B4EC6B44C44775723412EE388509364F'H,
          macLength 8
        }
      }
    },
    {
      keyUsageQualifier 'C8'H,
      keyAccess '00'H,
      keyIdentifier '03'H,
      keyVersionNumber '20'H,
      keyCompontents {
        {
          keyType '80'H,
          keyData '9AA14ED6A894798D7CC145EAAFCB06B1'H,
          macLength 8
        }
      }
    }
  },
  sdPersoData {
    '00703F663D733B06072A864886FC6B01600B06092A864886FC6B020202630906072A864886FC6B03640B06092A864886FC6B040255640B06092A864886FC6B048000'H
  }
}
rec11value ProfileElement ::= application : {
  app-Header {
    mandated NULL,
    identification 9
  },
  loadBlock {
    loadPackageAID 'A0000000770307601110010000000050'H,
    securityDomainAID 'A000000151000000'H,
    loadBlockObject '01001ADECAFFED010204000110A000000077030760111001000000005002001F001A001F0014006100EE00390F0E004001270000000000100008001E06010004006106000107A0000000620001020107A0000000620101000110A0000000090005FFFFFFFF8912000000010110A0000000871005FFFFFFFF8913200000000110A0000000090005FFFFFFFF8911000000000110A0000000090005FFFFFFFF89130000000300140110A00000007703076011100101000000500049060039804581030700070505000000B2FFFF00AF009400B681020082080109820700830000000000008000020002010500000D820D9F0E020E530EB2070F0E090116001901310014013900180153001400E180950179001405668200076800150847804208E90015088A803508E9001508C0802108E9001508E2800508E900150B41800A0B50001505318F00113D8C00102E1B181D0441181D258B00131B8B000A1B110168048D000F87001B102B048D000F87011B06048D001287021B8D000987031B8303048E02000B007A0110188C000C7A0310188F000D3D8C000E870418AD048305870618AD04830787087A00207A01301877800802131D7506B800020001000D000900DDAD02050539AD0310098E02000B03610BAD0310098E02000B00AD06113F008E02001601AD06117FDE8E02001601AD061117818E02001601AD061117808E02001601AD0603AD0003048E050016043BAD04117F20116F388B0017AD061006AD0006048E050016043B700A2D1A8B0018076B02AD04117FFF116F388B0019AD0806AD0007048E050016043B700A2D1A8B0018076B02AD00062510C05310C06A0DAD00072510205310206B07AD02050439A805FD2D1A8B0018076B16AD0310098E02000B03600BAD0310098E02000B02A805DE032906032907032908AD00280903290A03290B03290C03290D03290E03290FAD04117FDE1117808B0017AD0603150903048E050016043BAD020526046B0915090325026A037A8D001A28108D001B281115101013048E03001C029802EE15108E01001C031007A402E215101006048E03001C026021AD020415101006AD01038E04001C0839AD0104AD02042604438D001DA802B804290715101052048E03001C0298009B1052290F15101052150916088E04001C082908AD02041608391509032510705310706B0708290A700507290A160A1509160A25410441290A160A1509160A25410441290A1509160A2510276B14160A031509160A0441258D001E410541290A1509160A2510286B31031509160A0441258D001E290B160A0541290C1608290D18160C160BAD00160D8C001F29081608160D43290EA802117A1510107C048E03001C02980095107C290F1510107C150916088E04001C082908AD020416083907290A1509160A2510F05310D06B05590A011509160A2510276B14160A031509160A0441258D001E410541290A1509160A2510286B44031509160A0541258D001E290B160A0641290C031509160A0441258D001E290B160A0541290C1608290D18160C160BAD00160D8C001F29081608160D43290EA801727A15101031048E03001C029801621031290F15101031150916088E04001C082908AD02041608391509037B0020037B0020928D00216153150903AD0204267B0022037B0022928D0023290A160A623A160A7B00229241290D1509160DAD020426160D437B002403048D0023290A160A620C160A160D43290EA800EBAD020426160D43290EA800DF7A1509037B0025037B0025928D00219900CC150907AD02042607437B002603048D0023290D160D6207590D01700507290D1509160DAD020426160D437B002403048D0023290A160A160D6F28160A160D43290E1509160D160E7B002703048D0023290A160A160D6F75160A160D43290E706C1509160DAD020426160D437B002803048D0023290A160A160D6F28160A160D43290E1509160D160E7B002703048D0023290A160A160D6F34160A160D43290E702BAD020426160D43290E1509160D160E7B002703048D0023290A160A160D6F0C160A160D43290E70037A160D290C160E290B70047A7A1510101304031509110165068E07001C093BAD04117FDE1117818B002931AD0203AD04117FDE1117818B002A3904321F1E6E32AD04117FDE1117818B0017AD061F070315091608AD0203268E070016063B1816088C002B6007042906700759030170CE16066120AD04117FDE1117818B0017AD0604070315091608AD0203268E070016063B0329121607610C1816088C002C2912700E181608160D160E8C002D291216129801D81608150916080641251100FF5341290415091604251072A301BE5904021509160425610E151104038E03002E18A801A81509160425046B0E151104048E03002E18A80194150916042505A3018B15091604044325044329051607990100590401AD0110201509160425381509160404411509160405432505431100FF538D001D181509160404411509160405432505431100FF538C002F29051605100A6F0B151104038E03002E18AD0110212510F05310A06A0EAD0110212510F05310B06B761608291315091613AD0110202538032914161416056D3F15095913011613AD01102116144125074F100F533818150916138C003015095913011613AD01102116144125100F533818150916138C003059140170BF1509161325100F6B055913FF1511100A1509160859130116138E05002E0F701DAD01102116058D001D15111006AD01102059050116058E05002E0F151104058E03002E18707D160F10316B07590401700D18160416058C00315905011605160B4329131613601F160C160B4129141509161415091614161341AD0204261614438D00323B150916041509160C16058D00323B160F10316A0F1509160C0443160B1613415B381511160F150903AD0204261613418E05002E0F151104058E03002E18151104038E03002E18700D2812151104038E03002E187A0430191E25100A6A09191E25100B6B0C191E3E25102055387011191E25100A6D0A191E3E25103055387A0554AD0028051D290603290703290816081E6D3A0315051606590601258D001E2907150516061B160416078D0032290415051604590401102E381606160741290616081607410441290870C55904FF1604780334AD002E0329041D290503290616061E6D2A1B1D160641044125102E6B161B160516045B381D16064104412905032904700559040159060170D516051D1E416D091B160516045B387A0525AD002D1A1D2510EFA300A81DAD02032641321A1D04418D00332904AD04117FDE16048B00342905AD06031A1F16058E050016043B1F290616061F1605416D6E1A16067B003503068D0021610403781A1606251A110165256B4F1A1606044125100F531A11016625100F536B3C1A160604412510F05310F06B0D1A1606054125026B0404781A160604412510F0531A1101662510F0536B111A16060541251A110167256B040478590603708E70572E03781A1D251A110165256B4A1A1D044125100F531A11016625100F536B381A1D04412510F05310F06B0C1A1D054125026B0404781A1D04412510F0531A1101662510F0536B101A1D0541251A110167256B040478037804281D0641310332032905AD01100B0338032906042907AD0028081E9D01FB1EAD020326A501F303290515081E2510716B4815081E044125AD0204266F3CAD020426321F15081E0441256D2E15081E1F4105412510FD6A0E15081E1F4105412510DD6B0B160504415B2905700803290503290759030170CD15081E251071A3016B15081E044125AD020426A4015E160798015904290615081E0641AD02042604438D001D1E0541320329041F1E0541AD02042641A5012F15081F25AD011604256B05A8011815081F2510DD5310DD6B6D15081F250253026A6415081F2510DF5310DF6A5915081F2510FD5310FD6A4EAD01100B3E2504415B38AD01100BAD01100B2541AD01160425074F100F5338AD01160425100F53100FA200C2AD01100B3E2504415B38AD01100BAD01100B2541AD01160425100F5338A800A215081F2510D05310D06B3F15081F2510F05310F06A3415081F25100F53AD01160425100F536B23AD01100B3E2504415B38AD01100BAD01100B2541AD01160425074F100F5338705915081F25100D53100D6B4915081F25100F53100F6A3E15081F2510F053AD0116042510F0536B2DAD01160425100F53100F6A26AD01100B3E2504415B38AD01100BAD01100B2541AD01160425100F53387007032906700B590301590401A8FECB1606600470301E15081E0441251100FF5305414131AD01100B033804290715081E25026B07032906700CA8FE0E280903290670021606600D15081D06411F1605415B3816067805431D06412904AD0028050329061604656916041DAD020326416D5F03150516040441258D001E2906150516042510736B3E1606611415051D0641160416064105411D435B38047816061F6B2315051604054115051E1F8D0021611415051D0641160416064105411D435B3804781604160641054129047097037805401F0549046B13AD011D1F0547413E251E100F5355387011AD011D1F0547411E074D5B10F053381F04417805440329060329071F1E41321E290416041FA500FE1916042510D05310D06B3E1916042510F05310F06A331607612FAD01100B2560570329051605AD01100B256D4B181021AD01100C1605412516068C0036290604290759050170DF1916042510D05310D06B131916042510F05310F06A0816076004701518102119160425074F100F5316068C0036290619160425100D53100D6B3E19160425100F53100F6A331607612FAD01100B2560600329051605AD01100B256D54181021AD01100C1605412516068C0036290604290759050170DF19160425100D53100D6B1319160425100F53100F6A0816076004701E19160425100F53100F6A1318102119160425100F5316068C00362906590401A8FF0216060549046B0E181021100F16068C00362906160605475B780811188C00371011900B2C18048D0038870518113F00112F008B0017AD050407061903048E070016063BAD0504070719041903258E070016063B181904190325048D00398707012C7A0230AD07117FFF8E02001601AD071D8E02001601AD071E8E020016017A0230AD05113F008E02001601AD051D8E02001601AD051E8E020016017A07301E5902FF6519181D181D25074D5B181D59010125074F100F53553870E57A05611D290616061D1E411605436E171816061B160416058D0021610516067859060170E3027805338D003A2EAD05113F008E02001601AD051D8E02001601AD051E1B031B928E050016002904052905160516046D221B16052510806B0B1B160505418D00337816051B1605044125054141290570DC037805338D003A2EAD05113F008E02001601AD051D8E02001601AD051E1B031B928E050016002904052905160516046D301B16052510826B191B1605044125086B0E1B1605100641251100FF5378027816051B1605044125054141290570CE027805338D003A2EAD05113F008E02001601AD051D8E02001601AD051E1B031B928E050016002904052905160516046D2D1B16052510826B161B1605044125086B0B1B160507418D003378027816051B1605044125054141290570D10278080040001000080008030003FFFFFF03000F3B70686F6E652D636F6E746578743D0300013B0300013F0300013A0300014003000474656C3A0300047369703A000000000500EE003B020001030200010402000105020001020200010602002500020001000200250102000101068210000300010801820900068103000100250006000D390681080D0600008D010001000681080F0381030201840200018002000184000003002502038402010300250106820C0006820F000182030006000DBC068110050600079E0500000C068110000500000206000DDC050000040500000E0500000A050000080500000603002504030025050600083A0600094006000B690182040006000C1006000774060007F00681100206811004030025030500000006000BE40680000006840300068403020685010009012700886B090806031A0302020302021C050B090A0A0A0A030A0B04140B03140B0B100B15160B0D3E0709032F71346B3419221F2120413D470C03120B09200B098C480C14161D390D4C3358524A0C0F0CE80C0918064914140C340A04060B0D0A0406290A0A04062A0A0C0A04062E380E6E11420C0A600C0A7E0C0E1A070A080B0A08550A083F0A084D0A08009B07080808080808080805040C04080908060A080704370B0A0A0A0A0C0D0C090E0B09260B0B2E0B14050B0A0F110E0F144A191816143119131816140F04040C04040B12052304040F051C0518051F05180523052E0D0F1211081B110F0E33144017104D1B220C1109162A0E2609090D4C0E890C0D1305FF5FFFAD339F373F40190E0A0C0C100C1008080D08083C110A080C1C170A080C410A080C25'H
  },
  instanceList {
    {
      applicationLoadPackageAID 'A0000000770307601110010000000050'H,
      classAID 'A0000000770307601110010100000050'H,
      instanceAID 'A000000077030760110000FE00005000'H,
      applicationPrivileges '000000'H,
      lifeCycleState '07'H,
      applicationSpecificParametersC9 ''H,
      systemSpecificParameters {
        volatileMemoryQuotaC7 '0001'H,
        nonVolatileMemoryQuotaC8 '0001'H
      },
      applicationParameters {
        uiccToolkitApplicationSpecificParametersField 'FF001000000201020000'H,
        uiccAccessApplicationSpecificParametersField '0001000010A0000000871002F110FFFF89B00140FF010000'H,
        uiccAdministrativeAccessApplicationSpecificParametersField '0001000010A0000000871002F110FFFF89B00140FF010000'H
      }
    }
  }
}
rec12value ProfileElement ::= rfm : {
  rfm-header {
    mandated NULL,
    identification 10
  },
  instanceAID 'A00000055910100001'H,
  securityDomainAID 'A000000151000000'H,
  tarList {
    'B00001'H
  },
  minimumSecurityLevel '0E'H,
  uiccAccessDomain '00'H,
  uiccAdminAccessDomain '00'H,
  adfRFMAccess {
    adfAID 'A0000000871002F110FFFF89B00140FF'H,
    adfAccessDomain '00'H,
    adfAdminAccessDomain '00'H
  }
}
rec13value ProfileElement ::= end : {
  end-header {
    mandated NULL,
    identification 11
  }
}
