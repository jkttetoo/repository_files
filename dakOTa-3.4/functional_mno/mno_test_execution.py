import sys
from Oberthur import *
sys.path.insert(0, "lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('./esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import model
from mno_testplan import TestLink
import testlink
import json
import string
import binascii

SetCurrentReader('OMNIKEY CardMan 3x21 0')
displayAPDU(True)
SetLogLevel('info')

SERVER 	= model.Smsr()
EUICC 	= model.Euicc()
DEVICE 	= model.Device()
GSMA 	= model.Gsma()

SERVER_URL  = 'http://i2j6serv11.jakarta.oberthurcs.com:8081/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
DEVKEY      = 'e9e16b76f0e041fac8dff421de139039'
tls = testlink.TestlinkAPIClient(SERVER_URL, DEVKEY, verbose=False)

USER_NAME                       = 'm.ridharrahman@oberthur.com'
PROJECT_NAME                    = 'DakOTa 3.1'
PROJECT_PREFIX                  = 'DAKO3.1'
TEST_PLAN_NAME                  = 'MNO SD Automated Test'
PLATFORM_NAME					= 'Python 3.5'
BUILD 							= 'DakOTA 3.1 RC 8 HTTP valuation'
MANUAL 							= 1
AUTOMATED 						= 2

TEST_SUITE_HTTPS                = 'HTTPS'
TEST_SUITE_SMS                  = 'SMS'
TEST_SUITE_RAM                 	= 'RAM'
TEST_SUITE_RFM                  = 'RFM'

TEST_CASE_CARD_AUDIT            = 'Card Audit'
TEST_CASE_INSTALL_APPLET        = 'Install Applet'
TEST_CASE_LOCK_APPLET           = 'Lock Applet'
TEST_CASE_UNLOCK_APPLET         = 'Unlock Applet'
TEST_CASE_DELETE_APPLET         = 'Delete Applet'
TEST_CASE_RFM_3G_UPDATE_SPN		= 'update 3G SPN'
TEST_CASE_RFM_2G_UPDATE_ADN		= 'update 2G ADN'


tlink=TestLink()

with open(file.db('testcase_id.txt')) as f:
	testplan_info = json.load(f)
	testplan_id = testplan_info[TEST_PLAN_NAME]

build_id = tlink.create_build(test_plan_id=testplan_id, build_name = BUILD )

''' 
It's mandatory to have pprint.h1() method in each test case,
mechanism to find the related log for each test case is by the header
'''

pprint.h1('TEST CASE 1 CARD AUDIT')
EUICC.init()
result 				= SERVER.audit_isdp_list(scp=80, apdu_format='indefinite') 
status 				= tlink.check_por(por=result['list_aid'][1], expected_result = 'A0000005591010FFFFFFFF8900001100', test_case_name = TEST_CASE_CARD_AUDIT)
test_log 			= tlink.get_log()
test_case_id 		= testplan_info[TEST_CASE_CARD_AUDIT]
store 				= tls.reportTCResult(testcaseid=test_case_id, testplanid=testplan_id, buildname=BUILD, status=status,notes=test_log)[0]
tlink.clear_log()

pprint.h1('TEST CASE 2 INSTALL APPLET')
EUICC.init()
capdus, por, sw 	= EUICC.mno.install_applet(scp=80, apdu_format='indefinite')
status			 	= tlink.check_por(por=por, expected_result = 'AF80230290002302900023029000230290009001020000', test_case_name = TEST_CASE_INSTALL_APPLET)
test_log			= tlink.get_log()
test_case_id 		= testplan_info[TEST_CASE_INSTALL_APPLET]
store 				= tls.reportTCResult(testcaseid=test_case_id, testplanid=testplan_id, buildname=BUILD, status=status,notes=test_log)[0]
tlink.clear_log()

pprint.h1('TEST CASE 3 LOCK APPLET')

EUICC.init()
capdu, por, sw 		= EUICC.mno.lock_applet(scp=80, apdu_format='indefinite') 
status			 	= tlink.check_por(por=por, expected_result = 'AF80230290000000', test_case_name = TEST_CASE_LOCK_APPLET)
test_log			= tlink.get_log()
test_case_id 		= testplan_info[TEST_CASE_LOCK_APPLET]
store 				= tls.reportTCResult(testcaseid=test_case_id, testplanid=testplan_id, buildname=BUILD, status=status,notes=test_log)[0]
tlink.clear_log()

pprint.h1('TEST CASE 4 UNLOCK APPLET')

EUICC.init()
capdu, por, sw 		= EUICC.mno.unlock_applet(scp=80, apdu_format='indefinite') 
status			 	= tlink.check_por(por=por, expected_result = 'AF80230290000000', test_case_name = TEST_CASE_UNLOCK_APPLET)
test_log			= tlink.get_log()
test_case_id 		= testplan_info[TEST_CASE_UNLOCK_APPLET]
store 				= tls.reportTCResult(testcaseid=test_case_id, testplanid=testplan_id, buildname=BUILD, status=status,notes=test_log)[0]
tlink.clear_log()

pprint.h1('TEST CASE 5 DELETE APPLET')

EUICC.init()
capdu, por, sw 		= EUICC.mno.delete_applet(scp=80, apdu_format='indefinite') 
status			 	= tlink.check_por(por=por, expected_result = 'AF8023030090000000', test_case_name = TEST_CASE_DELETE_APPLET)
test_log			= tlink.get_log()
test_case_id 		= testplan_info[TEST_CASE_DELETE_APPLET]
store 				= tls.reportTCResult(testcaseid=test_case_id, testplanid=testplan_id, buildname=BUILD, status=status,notes=test_log)[0]
tlink.clear_log()


# EUICC.init()

# #CreateISDP
# h1('create isdp and establish keysetnumber')
# SERVER.create_isdp_and_establish_keyset("A0000005591010FFFFFFFF8900001900", scp=80, apdu_format='definite')
# #Download Profile with MNO SD
# h1('download saipv2 http')
# SERVER.download_profile("A0000005591010FFFFFFFF8900001900", saipv2_txt='saipv2_SD_NOPIN_SCP81_notoken_Expanded.der')
# #Enable
# h1('enable profile')
# SERVER.enable_profile("A0000005591010FFFFFFFF8900001900", scp=80, network_service=False, apdu_format='definite')
# # with open(Oberthur.log.currentLoggingFile, 'w'):f.write('new')





# dns_data_raw = 'D037810301430182028121362C53780100000100000000000010677032622D76616C2D6461696D6C6572066F746C6162730266720000010001'
# # scale = 16
# # num_of_bits=8
# # tlvs = bin(int(data[:26],16))[2:].zfill(8)

# data_dns        = dns_data_raw[26:]
# transaction_id  = data_dns[:4]
# dns_flag        = data_dns[4:8]
# dns_header      = data_dns[:24]
# dns_query       = data_dns[24:-10]

# first_length    = dns_query[:2]
# first_limit     = 2+int(first_length,16)*2
# first_query     = dns_query[2:first_limit]


# second_length   = dns_query[first_limit:first_limit+2]
# second_limit    = first_limit + 2 + (int(second_length,16)*2)
# second_query    = dns_query[first_limit+2:second_limit]

# third_length    = dns_query[second_limit:second_limit+2]
# third_limit     = second_limit + 2 + (int(second_length,16)*2)
# third_query     = dns_query[second_limit+2:third_limit]

# data_class      = data_dns[-10:]


# first_query_char= binascii.unhexlify(first_query).decode("utf-8")
# second_query_char= binascii.unhexlify(second_query).decode("utf-8")
# third_query_char= binascii.unhexlify(third_query).decode("utf-8")


# LogInfo("\n")
# LogInfo('#'*78)
# LogInfo('dns request from euicc:')
# LogInfo('-----------------------------')
# LogInfo(data_dns)
# LogInfo("DNS transaction id     : {}".format(transaction_id))
# LogInfo("DNS flag               : {}".format(dns_flag))
# LogInfo("DNS query              : {} {} {}".format(first_query_char, second_query_char, third_query_char))
# LogInfo("DNS data class         : {}".format(data_class))




# tlvs = ("53 78 0100000100000000000010677032622D76616C2D6461696D6C6572066F746C6162730266720000010001")





# print(result)
# EUICC.init()
# print(store['message'])
# EUICC.mno.unlock_applet(scp=80, apdu_format="compact")
# EUICC.isdr.scp03()

