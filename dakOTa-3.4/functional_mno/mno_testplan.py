import sys
from Oberthur import *
sys.path.insert(0, "../../lib")
from util import *
import testlink
import json



SERVER_URL  = 'http://i2j6serv11.jakarta.oberthurcs.com:8081/testlink/lib/api/xmlrpc/v1/xmlrpc.php'
DEVKEY      = 'e9e16b76f0e041fac8dff421de139039'
tls = testlink.TestlinkAPIClient(SERVER_URL, DEVKEY, verbose=False)

USER_NAME                       = 'm.ridharrahman@oberthur.com'
PROJECT_NAME                    = 'DakOTa 3.1'
PROJECT_PREFIX                  = 'DAKO3.1'
TEST_PLAN_NAME                  = 'MNO SD Automated Test'
PLATFORM_NAME					= 'Python 3.5'
BUILD 							= 'DakOTA 3.1 RC 7 HTTP valuation'
MANUAL 							= 1
AUTOMATED 						= 2

TEST_SUITE_HTTPS                = 'HTTPS'
TEST_SUITE_SMS                  = 'SMS'
TEST_SUITE_RAM                 	= 'RAM'
TEST_SUITE_RFM                  = 'RFM'

TEST_CASE_CARD_AUDIT            = 'Card Audit'
TEST_CASE_INSTALL_APPLET        = 'Install Applet'
TEST_CASE_LOCK_APPLET           = 'Lock Applet'
TEST_CASE_UNLOCK_APPLET         = 'Unlock Applet'
TEST_CASE_DELETE_APPLET         = 'Delete Applet'
TEST_CASE_RFM_3G_UPDATE_SPN		= 'update 3G SPN'
TEST_CASE_RFM_2G_UPDATE_ADN		= 'update 2G ADN'



class TestLink:

	def __init__(self):
		self.project_name = None


	def create_test_suite(self, project_id, test_plan_id, test_suite_name, parent_id) :
		test_suites = tls.getTestSuitesForTestPlan(test_plan_id)
		new = True
		for test_suite in test_suites:
			if test_suite['name'] == test_suite_name:
				test_suite_info= test_suite
				new = False
				break	
		if new:
			LogInfo('Create new test suite {}'.format(test_suite_name)) 

			test_suite_info = tls.createTestSuite(project_id, test_suite_name, 'Bearer SCP81', parentid=parent_id)[0]
		
		LogInfo( 'Test suite 	:' + ' {} '.format(test_suite_info['id']) + test_suite_name)

		return test_suite_info['id']


	def checking_test_case(self, test_suite_id, test_case_name) :
		test_case_card_audit_https = tls.getTestCasesForTestSuite(testsuiteid = test_suite_id, deep = "true", details = 'simple')
		i=0
		new = True
		for testcases in test_case_card_audit_https:
			if test_case_card_audit_https[i]['name'] == test_case_name:
				LogInfo('Test case {} already created'.format(test_case_name))
				new = False 
				break
			i=i+1
		return new

	def create_build(self, test_plan_id, build_name) :
		new = False
		build = next( ( build for build in tls.getBuildsForTestPlan(test_plan_id) if build['name'] == build_name ), None )
		if not build:
		    new = True
		    build = tls.createBuild(test_plan_id, build_name, '')[0]
		build_id = build['id']
		LogInfo('Build 	 :' + '{} '.format(build_id) + build_name + ' [NEW]' if new else 'Build 	 	:' + ' {} '.format(build_id) + build_name)

		return build_id


	def get_log(self):
		with open(Oberthur.log.currentLoggingFile, 'r') as f: 
			file_string = f.read()
			test_log = file_string[file_string.find('='):]
		return test_log

	def clear_log(self):
		with open(Oberthur.log.currentLoggingFile, 'w') as f: f.write('')


	def check_por(self,por, expected_result, test_case_name):
		if expected_result == por : 
			status = 'p'
			print("execution of test case {} SUCCESS ".format(test_case_name))
		else : 
			status = 'f'
			print("execution of test case {} FAILED ".format(test_case_name))
		return status



if __name__ == '__main__':


################################################################## CREATE PROJECT

	tlink = TestLink()

	project_id = tls.getProjectIDByName(PROJECT_NAME)

	if project_id == -1:
	    LogInfo('Creation of the TestLink project {} : {}'.format(PROJECT_PREFIX, PROJECT_NAME))

	    project_info = tls.createTestProject(
	    	PROJECT_NAME, 
	    	PROJECT_PREFIX, 
	    	notes	=	'', 
	    	active	=	1, 
	    	public	=	1, 
	    	options	=	{'requirementsEnabled' : 0, 'testPriorityEnabled' : 1, 'automationEnabled' : 1, 'inventoryEnabled' : 0})[0]

	    print(project_info)

	else : 
		LogInfo("Executing\nProject  	: {} (ID {}) ".format(PROJECT_NAME,project_id))
		
################################################################## CREATE TEST PLAN

	new = False
	test_plan = next( ( test_plan for test_plan in tls.getProjectTestPlans(project_id) if test_plan['name'] == TEST_PLAN_NAME ), None )
	if not test_plan:
		new = True
		LogInfo('Creation of the test plan {}'.format(TEST_PLAN_NAME))

		test_plan_info = tls.createTestPlan(
			TEST_PLAN_NAME, 
			PROJECT_NAME, 
			notes 	= 'Testing Remote Application Management Over-The-Air thru SCP80 and SCP81', 
			active 	= 1, 
			public 	= 1)[0]

	LogInfo("Testplan : {} (ID {}) [NEW]".format(TEST_PLAN_NAME,test_plan_info['id']) if new else "Testplan 	: {} (ID {}) ".format(TEST_PLAN_NAME,test_plan['id']))


################################################################## CREATE BUILD AND TEST SUITE

	build_id 			= tlink.create_build(test_plan_id = test_plan['id'],build_name=BUILD) 
	test_suite_HTTPS_id = tlink.create_test_suite(project_id=project_id,test_plan_id=test_plan['id'],test_suite_name='HTTPS',parent_id=None)
	test_suite_RAM_id	= tlink.create_test_suite(project_id=project_id,test_plan_id=test_plan['id'],test_suite_name='RAM',parent_id=test_suite_HTTPS_id)
	test_suite_RFM_id	= tlink.create_test_suite(project_id=project_id,test_plan_id=test_plan['id'],test_suite_name='RFM',parent_id=test_suite_HTTPS_id)

################################################################## CREATE TEST CASE TEST_CASE_CARD_AUDIT

	new = tlink.checking_test_case(test_suite_id = test_suite_RAM_id, test_case_name = TEST_CASE_CARD_AUDIT)

	tc_version=1	
	if new:
	    LogInfo('Create new test case {}'.format(TEST_CASE_CARD_AUDIT)) 

	    test_case_card_audit_https = tls.createTestCase(
	    	testcasename 	=	TEST_CASE_CARD_AUDIT,
	    	testsuiteid 	=	test_suite_RAM_id, 
	    	testprojectid 	=	project_id, 
	    	authorlogin 	=	USER_NAME, 
	    	summary 		=	'Server is requesting a card audit thru SCP81', 
	    	preconditions 	=	'Java Composer is connected to 172.16.220.35:6015',
			steps 			=  [
			{'step_number' : 1, 'actions' : "Open ALM session" , 									'expected_results' : "MD connected", 	'execution_type' : AUTOMATED},
	        {'step_number' : 2, 'actions' : "Send CardAudit request to ALM for +6281807774546" ,	'expected_results' : "ALM ACK", 		'execution_type' : AUTOMATED},
	        {'step_number' : 3, 'actions' : "Receive CardAudit Result (async ALM notification)" , 	'expected_results' : "List of AID",		'execution_type' : AUTOMATED}])[0]

	    test_case_card_audit_https_full_ext_id = tls.getTestCase(test_case_card_audit_https["id"])[0]['full_tc_external_id']
	    tls.addTestCaseToTestPlan(project_id, test_plan['id'], test_case_card_audit_https_full_ext_id, tc_version)
	    LogInfo("ID :"+ test_case_card_audit_https['id'])
	else:
		test_case_card_audit_https = tls.getTestCaseIDByName(
		testcasename = TEST_CASE_CARD_AUDIT, 
		testsuitename = TEST_SUITE_RAM, 
		testprojectname = PROJECT_NAME)[0]
		LogInfo( 'Test case 	:' + ' {} '.format(test_case_card_audit_https['id'] ) + TEST_CASE_CARD_AUDIT )

################################################################## CREATE TEST CASE TEST_CASE_INSTALL APPLET

	new = tlink.checking_test_case(test_suite_id = test_suite_RAM_id,test_case_name = TEST_CASE_INSTALL_APPLET)

	if new:
	    LogInfo('Create new test case {}'.format(TEST_CASE_INSTALL_APPLET)) 
	    test_case_install_applet_https = tls.createTestCase(
	    	testcasename=TEST_CASE_INSTALL_APPLET,
	    	testsuiteid=test_suite_RAM_id, 
	    	testprojectid=project_id, 
	    	authorlogin=USER_NAME, 
	    	summary='Server is send install command to the card', 
	    	preconditions='Java Composer is connected to 172.16.220.35:6015',
			steps=  [
			{'step_number' : 1, 'actions' : "Open ALM session" , 									'expected_results' : "MD connected", 	'execution_type' : AUTOMATED},
	        {'step_number' : 2, 'actions' : "Send install command to the card" ,					'expected_results' : "ALM ACK", 		'execution_type' : AUTOMATED}])[0]

	    # LogInfo(test_case_card_audit_https)

	    test_case_install_applet_https_full_ext_id = tls.getTestCase(test_case_install_applet_https["id"])[0]['full_tc_external_id']
	    tls.addTestCaseToTestPlan(project_id, test_plan['id'], test_case_install_applet_https_full_ext_id, tc_version)
	    LogInfo("ID :"+ test_case_install_applet_https['id'])
	else:
		test_case_install_applet_https= tls.getTestCaseIDByName(
		testcasename=TEST_CASE_INSTALL_APPLET, 
		testsuitename=TEST_SUITE_RAM, 
		testprojectname=PROJECT_NAME)[0]
		LogInfo( 'Test case 	:' + ' {} '.format(test_case_install_applet_https['id'] ) + TEST_CASE_INSTALL_APPLET )


################################################################## CREATE TEST CASE TEST_CASE_LOCK_APPLET

	new = tlink.checking_test_case(test_suite_id = test_suite_RAM_id,test_case_name = TEST_CASE_LOCK_APPLET)

	if new:
	    LogInfo('Create new test case {}'.format(TEST_CASE_LOCK_APPLET)) 
	    test_case_lock_applet_https = tls.createTestCase(
	    	testcasename=TEST_CASE_LOCK_APPLET,
	    	testsuiteid=test_suite_RAM_id, 
	    	testprojectid=project_id, 
	    	authorlogin=USER_NAME, 
	    	summary='Server is send install command to the card', 
	    	preconditions='Java Composer is connected to 172.16.220.35:6015',
			steps=  [
			{'step_number' : 1, 'actions' : "Open ALM session" , 									'expected_results' : "MD connected", 	'execution_type' : AUTOMATED},
	        {'step_number' : 2, 'actions' : "Send install command to the card" ,					'expected_results' : "ALM ACK", 		'execution_type' : AUTOMATED}])[0]

	    test_case_lock_applet_https_full_ext_id = tls.getTestCase(test_case_lock_applet_https["id"])[0]['full_tc_external_id']
	    tls.addTestCaseToTestPlan(project_id, test_plan['id'], test_case_lock_applet_https_full_ext_id, tc_version)
	    LogInfo("ID :"+ test_case_lock_applet_https['id'])
	else:
		test_case_lock_applet_https= tls.getTestCaseIDByName(
		testcasename=TEST_CASE_LOCK_APPLET, 
		testsuitename=TEST_SUITE_RAM, 
		testprojectname=PROJECT_NAME)[0]
		LogInfo( 'Test case 	:' + ' {} '.format(test_case_lock_applet_https['id'] ) + TEST_CASE_LOCK_APPLET )

################################################################## CREATE TEST CASE TEST_CASE_UNLOCK_APPLET

	new = tlink.checking_test_case(test_suite_id = test_suite_RAM_id,test_case_name = TEST_CASE_UNLOCK_APPLET)

	if new:
	    LogInfo('Create new test case {}'.format(TEST_CASE_UNLOCK_APPLET)) 
	    test_case_unlock_applet_https = tls.createTestCase(
	    	testcasename=TEST_CASE_UNLOCK_APPLET,
	    	testsuiteid=test_suite_RAM_id, 
	    	testprojectid=project_id, 
	    	authorlogin=USER_NAME, 
	    	summary='Server is send install command to the card', 
	    	preconditions='Java Composer is connected to 172.16.220.35:6015',
			steps=  [
			{'step_number' : 1, 'actions' : "Open ALM session" , 									'expected_results' : "MD connected", 	'execution_type' : AUTOMATED},
	        {'step_number' : 2, 'actions' : "Send install command to the card" ,					'expected_results' : "ALM ACK", 		'execution_type' : AUTOMATED}])[0]

	    # LogInfo(test_case_card_audit_https)

	    test_case_lock_applet_https_full_ext_id = tls.getTestCase(test_case_unlock_applet_https["id"])[0]['full_tc_external_id']
	    tls.addTestCaseToTestPlan(project_id, test_plan['id'], test_case_lock_applet_https_full_ext_id, tc_version)
	    LogInfo("ID :"+ test_case_unlock_applet_https['id'])
	else:
		test_case_unlock_applet_https= tls.getTestCaseIDByName(
		testcasename=TEST_CASE_UNLOCK_APPLET, 
		testsuitename=TEST_SUITE_RAM, 
		testprojectname=PROJECT_NAME)[0]
		LogInfo( 'Test case 	:' + ' {} '.format(test_case_unlock_applet_https['id'] ) + TEST_CASE_UNLOCK_APPLET )

################################################################## CREATE TEST CASE TEST_CASE_DELETE_APPLET

	new = tlink.checking_test_case(test_suite_id = test_suite_RAM_id,test_case_name = TEST_CASE_DELETE_APPLET)

	if new:
	    LogInfo('Create new test case {}'.format(TEST_CASE_DELETE_APPLET)) 
	    test_case_delete_applet_https = tls.createTestCase(
	    	testcasename=TEST_CASE_DELETE_APPLET,
	    	testsuiteid=test_suite_RAM_id, 
	    	testprojectid=project_id, 
	    	authorlogin=USER_NAME, 
	    	summary='Server is send install command to the card', 
	    	preconditions='Java Composer is connected to 172.16.220.35:6015',
			steps=  [
			{'step_number' : 1, 'actions' : "Open ALM session" , 									'expected_results' : "MD connected", 	'execution_type' : AUTOMATED},
	        {'step_number' : 2, 'actions' : "Send install command to the card" ,					'expected_results' : "ALM ACK", 		'execution_type' : AUTOMATED}])[0]

	    # LogInfo(test_case_card_audit_https)

	    test_case_lock_applet_https_full_ext_id = tls.getTestCase(test_case_delete_applet_https["id"])[0]['full_tc_external_id']
	    tls.addTestCaseToTestPlan(project_id, test_plan['id'], test_case_lock_applet_https_full_ext_id, tc_version)
	    LogInfo("ID :"+ test_case_delete_applet_https['id'])
	else:
		test_case_delete_applet_https= tls.getTestCaseIDByName(
		testcasename=TEST_CASE_DELETE_APPLET, 
		testsuitename=TEST_SUITE_RAM, 
		testprojectname=PROJECT_NAME)[0]
		LogInfo( 'Test case 	:' + ' {} '.format(test_case_delete_applet_https['id'] ) + TEST_CASE_DELETE_APPLET )


################################################################## CREATE TEST CASE TEST_CASE_RFM_3G_UPDATE_SPN

	new = tlink.checking_test_case(test_suite_id = test_suite_RFM_id,test_case_name = TEST_CASE_RFM_3G_UPDATE_SPN)

	if new:
	    LogInfo('Create new test case {}'.format(TEST_CASE_RFM_3G_UPDATE_SPN)) 
	    test_case_3g_update_spn_https = tls.createTestCase(
	    	testcasename=TEST_CASE_RFM_3G_UPDATE_SPN,
	    	testsuiteid=test_suite_RFM_id, 
	    	testprojectid=project_id, 
	    	authorlogin=USER_NAME, 
	    	summary='Server is send install command to the card', 
	    	preconditions='Java Composer is connected to 172.16.220.35:6015',
			steps=  [
			{'step_number' : 1, 'actions' : "Open ALM session" , 									'expected_results' : "MD connected", 	'execution_type' : AUTOMATED},
	        {'step_number' : 2, 'actions' : "Send install command to the card" ,					'expected_results' : "ALM ACK", 		'execution_type' : AUTOMATED}])[0]

	    # LogInfo(test_case_card_audit_https)

	    test_case_3g_update_spn_https_full_ext_id = tls.getTestCase(test_case_3g_update_spn_https["id"])[0]['full_tc_external_id']
	    tls.addTestCaseToTestPlan(project_id, test_plan['id'], test_case_3g_update_spn_https_full_ext_id, tc_version)
	    LogInfo("ID :"+ test_case_3g_update_spn_https['id'])
	else:
		test_case_3g_update_spn_https= tls.getTestCaseIDByName(
		testcasename=TEST_CASE_RFM_3G_UPDATE_SPN, 
		testsuitename=TEST_SUITE_RFM, 
		testprojectname=PROJECT_NAME)[0]
		LogInfo( 'Test case 	:' + ' {} '.format(test_case_3g_update_spn_https['id'] ) + TEST_CASE_RFM_3G_UPDATE_SPN )

################################################################## CREATE TEST CASE TEST_CASE_RFM_2G_UPDATE_ADN

	new = tlink.checking_test_case(test_suite_id = test_suite_RFM_id,test_case_name = TEST_CASE_RFM_2G_UPDATE_ADN)

	if new:
	    LogInfo('Create new test case {}'.format(TEST_CASE_RFM_2G_UPDATE_ADN)) 
	    test_case_2g_update_adn_https = tls.createTestCase(
	    	testcasename=TEST_CASE_RFM_2G_UPDATE_ADN,
	    	testsuiteid=test_suite_RFM_id, 
	    	testprojectid=project_id, 
	    	authorlogin=USER_NAME, 
	    	summary='Server is send install command to the card', 
	    	preconditions='Java Composer is connected to 172.16.220.35:6015',
			steps=  [
			{'step_number' : 1, 'actions' : "Open ALM session" , 									'expected_results' : "MD connected", 	'execution_type' : AUTOMATED},
	        {'step_number' : 2, 'actions' : "Send install command to the card" ,					'expected_results' : "ALM ACK", 		'execution_type' : AUTOMATED}])[0]

	    # LogInfo(test_case_card_audit_https)

	    test_case_2g_update_adn_https_full_ext_id = tls.getTestCase(test_case_2g_update_adn_https["id"])[0]['full_tc_external_id']
	    tls.addTestCaseToTestPlan(project_id, test_plan['id'], test_case_2g_update_adn_https_full_ext_id, tc_version)
	    LogInfo("ID :"+ test_case_2g_update_adn_https['id'])
	else:
		test_case_2g_update_adn_https= tls.getTestCaseIDByName(
		testcasename=TEST_CASE_RFM_2G_UPDATE_ADN, 
		testsuitename=TEST_SUITE_RFM, 
		testprojectname=PROJECT_NAME)[0]
		LogInfo( 'Test case 	:' + ' {} '.format(test_case_2g_update_adn_https['id'] ) + TEST_CASE_RFM_2G_UPDATE_ADN )

	print("\n\n")

	test_case_id = {
			TEST_PLAN_NAME				: test_plan['id'],
			TEST_CASE_CARD_AUDIT 		: test_case_card_audit_https['id'],
			TEST_CASE_INSTALL_APPLET 	: test_case_install_applet_https['id'],
			TEST_CASE_LOCK_APPLET 		: test_case_lock_applet_https['id'],
			TEST_CASE_UNLOCK_APPLET 	: test_case_unlock_applet_https['id'],
			TEST_CASE_DELETE_APPLET 	: test_case_delete_applet_https['id'],
			TEST_CASE_RFM_3G_UPDATE_SPN	: test_case_3g_update_spn_https['id'],
			TEST_CASE_RFM_2G_UPDATE_ADN	: test_case_2g_update_adn_https['id']
			}

	print(test_case_id)

	with open(file.db('testcase_id.txt'), 'w') as f :
		json.dump(test_case_id, f)



	##################### Store Test Result	

		# tls.reportTCResult(
		# 	testcaseid=testcase_id, 
		# 	testplanid=testplan_id, 
		# 	buildname="DakOTA RC 9 with API", 
		# 	status="f")



	######### CREATE PLATFORM

	# new = False
	# platform = next( ( platform for platform in tls.getTestPlanPlatforms(test_plan['id']) if platform['name'] == PLATFORM_NAME ), None )
	# if not platform:
	#     new = True
	#     LogInfo('Creation of the platform {}'.format(PLATFORM_NAME))
	#     platform = tls.createPlatform(
	#     	PROJECT_NAME, 
	#     	PLATFORM_NAME, 
	#     	notes='Offline testing environment dedicated to ESF Jakarta Validation and Integration team for MNO projects')
	#     assert tls.addPlatformToTestPlan(test_plan['id'], PLATFORM_NAME, notes='')['msg'] == 'link done'

	# LogInfo( 'Platform:' + '{}'.format(platform['id'])+ PLATFORM_NAME + ' [NEW]' if new else 'Platform 	:' + ' {} '.format(platform['id'])+ PLATFORM_NAME)

