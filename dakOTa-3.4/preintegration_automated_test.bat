@echo off
if not "%1" == "max" start /MAX cmd /c %0 max & exit/b
title Pre-Integration Automated Test
rem echo Hello, this is automation test for preintegration test for dakOTa 3.2
rem is a comment
rem echo ## Dependencies:
rem echo ## 1. Python35 with all Oberthur API already installed
echo ## Additional Dependencies:
echo ## 1. Additional Unittest XML Reporting API @ C:\Python35\Lib\site-packages
echo ## 2. Additional Pre-Integration API @ C:\Python35\Lib\site-packages
echo ##    (model, util, const)
echo ## 3. Additional Pre-Integration Database for counter  
echo ##    @ C:\ProgramData\Oberthur Technologies\Oberthur_Python_API
echo ##    (db_dakota3 folder)
echo.
pause
echo.
echo ### TESTING START ###
echo.
cd automation
python35 execute_all_coba_aji_withClass.py
echo.
echo ### TESTING FINISH ###
echo.
pause