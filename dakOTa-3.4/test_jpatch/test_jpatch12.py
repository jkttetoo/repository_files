import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE 		= model.Device()
EUICC 		= model.Euicc()
SERVER		= model.Smsr()
GSMA 		= model.Gsma()

displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())

# SetCurrentReader('eSIM 3.1.rc1')
SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('DE620 Contact Reader')
ipv4 		= '148.251.218.229'
tcp_port 	= '6500'


EUICC.init()
capdu, rapdu, sw = SERVER.ram(GSMA.update_smsr_addressing_parameters(ipv4, tcp_port), 81, apdu_format='indefinite', chunk = '01')
LogInfo('SMSR1 Update = {}'.format(rapdu))
capdu, rapdu, sw = SERVER.ram(GSMA.update_jpatch_rc12(), 81, apdu_format='indefinite', chunk = '01')
LogInfo('JPATCH Update = {}'.format(rapdu))
capdu, rapdu, sw = SERVER.ram(GSMA.update_smsr_addressing_parameters(ipv4, tcp_port), 81, apdu_format='indefinite', chunk = '01')
LogInfo('SMSR2 Update = {}'.format(rapdu))