import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Ot.GlobalPlatform import IssuerSecurityDomain_Profile, SecurityDomain
from Ot.GlobalPlatform import getVersion as GlobalPlatformModuleGetVersion
from util import pprint, aid, hexastring
from Mobile import *
from util import *
import unittest
import configparser
import model


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'

DEVICE      = model.Device()
OTA         = model.SCP80()
EUICC       = model.Euicc()
SERVER      = model.Smsr()

displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('Dell Smart Card Reader Keyboard 0')
# SetCurrentReader('DakOTaV31SLE97PC41387XSecureRC7 Reader')


EUICC.init()
OTA.spi1 = 0x16
OTA.spi2 = 0x39
OTA.kic  = 0x12
OTA.kid  = 0x12

# LogInfo('master delete ISD-P {} thru SMS/SCP03'.format(aid.short_isdp('A0000005591010FFFFFFFF8900001100')))
# keyset_isdp = EUICC.get_isdp_keyset_scp03t('A0000005591010FFFFFFFF8900001100')
# LogInfo('ISD-P SCP03 keyset value before operation: {}'.format(keyset_isdp))
# isdp = IssuerSecurityDomain_Profile('A0000005591010FFFFFFFF8900001100', keyset_isdp)
# apdu_open_scp03 = isdp.openDefaultSecureChannel(secureMessaging='33')
# profile_download_payload = apdu_open_scp03[0] + apdu_open_scp03[1]
# ram_targeted_app = aid.format_for_http_header_targeted_app('A0000005591010FFFFFFFF8900001100')

# LogInfo("profile download payload : HOST_CHALLENGE = {}, HOST_CRYPTOGRAM = {}".format(apdu_open_scp03[0][8:],apdu_open_scp03[1][6:]))


SERVER.master_delete('A0000005591010FFFFFFFF8900001200',80, apdu_format='definite', chunk = '01')

# capdu, rapdu, sw = OTA.push_sms()
# DEVICE.fetch_one(sw)

