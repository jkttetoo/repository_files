/***************************************************************************  
    Projet      : GOP REF V57.01.02
    Fichier     : gopref_check.h                                              
    Description : Fichier de verification des options de compilation          
 ****************************************************************************/

#include <vm21/include/jc21defs.h>

#if !defined(COP_GP2_2) || !defined(COP_GP2_2_1)
	#error Missing define
#endif

#ifndef COP_GSM
	#error Missing define
#endif

#ifdef NO_RSA
	#error Unexpected define
#endif

#if (_OPT_SD_TOKEN_AES != OPT_ON)
	#error Wrong Option
#endif

#if (_OPT_SD_RECEIPT_AES != OPT_ON)
	#error Wrong Option
#endif

#if (_OPT_CASD_OBKG != OPT_ON)
	#error Missing define
#endif

#ifdef NO_CASD
	#error Unexpected define
#endif

#ifdef COP_GP2_2_MEMRES_MNGT
	#error Unexpected define
#endif

#ifndef COP_GP2_2_AMD_A
	#error Missing define
#endif

#if defined(COP_GP2_2_AMD_A_SCENARIO_1_PK) && (COP_GP2_2_AMD_A_SCENARIO_1_PK == OPT_ON)
	#error Wrong Option
#endif

#if defined(COP_GP2_2_AMD_A_SCENARIO_1_NPK) && (COP_GP2_2_AMD_A_SCENARIO_1_NPK == OPT_ON)
	#error Wrong Option
#endif

#if defined(COP_GP2_2_AMD_A_SCENARIO_2A) && (COP_GP2_2_AMD_A_SCENARIO_2A == OPT_ON)
	#error Wrong Option
#endif

#if ((_OPT_SCENARIO_3_ECC != OPT_ON) || (_OPT_API_EC_LDS!= OPT_ON))
	#error Wrong Option
#endif

#ifdef SCENARIO_3S
	#error Wrong Option
#endif

#ifndef API_SHA384
	#error Missing Option
#endif

#if !defined(COP_GP2_2_AMD_B) || (_OPT_SD_SCP_81 != OPT_ON) || (_OPT_TLS_NULL_SHA != OPT_ON)
	#error Wrong Option
#endif

#if (_OPT_AMD_B_1_1_3 != OPT_ON)
	#error Wrong Option
#endif

#if defined(COP_GP2_2_AMD_C) || defined(COP_GP2_2_AMD_C_PERSISTENT_PRIORITY) || defined(COP_GP2_2_AMD_C_VOLATILE_PRIORITY) || defined(COP_GP2_2_AMD_C_CPP)
	#error Unexpected define
#endif

#if !defined(COP_GP2_2_AMD_C_CGM)
	#error Missing define
#endif

#ifndef AMD_C_DELETE_SD_HIERARCHY
	#error Missing define
#endif

#ifdef COP_GP2_2_AMD_C_CUMULATIVE_DELETE
	#error Unexpected define
#endif

#ifndef API_LISTING
	#error Missing Option
#endif

#ifdef CIC_V2
	#error Unexpected define
#endif

#ifndef UICC_CONFIG
	#error Missing define
#endif

#ifdef SE_CONFIG
	#error Unexpected define
#endif

#if (_OPT_SD_SCP_03S != OPT_ON) || (_OPT_API_AES_CMAC_PROPRIETARY != OPT_ON)
	#error Wrong Option
#endif

#if defined(_OPT_SD_SCP_03) && (_OPT_SD_SCP_03 == OPT_ON)
	#error Wrong Option
#endif

#ifndef RMAC_FUNCTIONALITY
	#error Missing Option
#endif

#ifndef BEGIN_END_RMAC_SUPPORTED
	#error Missing Option
#endif

#if defined(_OPT_SD_SCP_03ISO) && (_OPT_SD_SCP_03ISO == OPT_ON)
	#error Unexpected Option
#endif

#if defined(_OPT_SD_SCP_11) && (_OPT_SD_SCP_11 == OPT_ON)
	#error Wrong Option
#endif

#if (defined(_OPT_SD_SCP_11_05_AX) && (_OPT_SD_SCP_11_05_AX == OPT_ON))
	#error Wrong Option
#endif

#if (defined(_OPT_SD_SCP_11_KEY_UPGD) && (_OPT_SD_SCP_11_KEY_UPGD == OPT_ON))
	#error Wrong Option
#endif

#ifdef COP_GP2_2_AMD_H
	#error Wrong Option
#endif

#if defined(NO_KEYACCESS)
	#error Wrong Option
#endif

#if defined(_OPT_SD_DAP_AES) && (_OPT_SD_DAP_AES == OPT_ON)
	#error Unexpected Option
#endif

#if _CMCC_FEATURES == OPT_ON
	#error Wrong Option
#endif

#ifdef IMPLICIT_SELECTION_SUPPORTED
	#error Wrong Option
#endif

#ifdef FINAL_APPLICATION_SUPPORTED
	#error Wrong Option
#endif

#ifndef VOP_SUPPORTED
	#error Missing Option
#endif

#ifndef GSMA_EUICC_GP_ARCHI
	#error Missing Option
#endif

#ifndef GSMA_EUICC_SUPPORTED
	#error Missing Option
#endif

#ifndef SIMA_VERSION
	#error Missing Option
#endif

#if(SIMA_VERSION != 0x0202)
	#error Wrong Option Value
#endif

#ifndef EUICC_MNOSD_AS_ISD
	#error Missing Option
#endif

#ifdef SPARKY_EUICC_SUPPORTED
	#error Unexpected Option
#endif

#ifndef ECASD_SUPPORTED
	#error Missing Option
#endif

#ifndef EUICC_PROFILE_PKG_SUPPORTED
	#error Missing Option
#endif

#ifndef SIMA_EUICC_SUPPORTED
	#error Missing Option
#endif

#if(defined(_OPT_TELF_EUICC_SUPPORTED) && (_OPT_TELF_EUICC_SUPPORTED != OPT_OFF))
	#error Unexpected Option
#endif

#ifdef COP_GP2_2_AMD_C_READER_MODE
	#error Wrong Option
#endif

#if defined(_OPT_AMD_B_SOFTBANK) && (_OPT_AMD_B_SOFTBANK == OPT_ON)
	#error Unexpected Option
#endif

#if defined(_OPT_AMD_B_ISIS) && (_OPT_AMD_B_ISIS == OPT_ON)
	#error Unexpected Option
#endif

#if defined(_OPT_AMD_B_LGU_PLUS) && (_OPT_AMD_B_LGU_PLUS == OPT_ON)
	#error Unexpected Option
#endif

#if (_OPT_AMD_B_DNSRESOLVER != OPT_ON)
	#error Wrong Option
#endif

#ifdef CU_SPECIFIC_FEATURES
	#error Wrong Option
#endif

#if defined(_OPT_DELETE_KEY_BY_ID) && (_OPT_DELETE_KEY_BY_ID == OPT_ON)
	#error Unexpected Option
#endif

#ifdef DISABLE_SIMPLE_DES
	#error Wrong Option
#endif

#if (_OPT_SD_PUTKEY_AES != OPT_ON)
	#error Wrong Option
#endif

#ifdef TEST_LOCK_TCL
	#error Unexpected Option
#endif

#ifdef HIGH_SECURITY
	#error Unexpected Option
#endif

#ifdef MULTIPLE_COMPLIANCES
	#error Wrong Option
#endif

#if (_OPT_SD_SCP_80 != OPT_ON)
	#error Wrong Option
#endif

#if defined(_OPT_DONT_USE_ADM_AS_TRANSPORT_KEY) && (_OPT_DONT_USE_ADM_AS_TRANSPORT_KEY == OPT_ON)
	#error Wrong Option
#endif

#ifdef MULTIPLE_DOMAINS
	#error Wrong Option
#endif

#if defined(_OPT_SD_KVN_RANGE_CTRL) && (_OPT_SD_KVN_RANGE_CTRL != OPT_ON)
	#error Wrong Option
#endif

#if !defined (VM_INTS)
	#error Wrong Option
#endif

#ifdef INSTALL_BUFFER_RESTRICTED_ACCES
	#error Missing define
#endif

