/**
*	Package name: com.sec.mobile.fra for ClearList class implementation
*	Only FRA SHALL use this API.
*	version 1.0 
*/ 
@APISourcer_Package_Description(
    AID="A000000220160301041000534D465241",
    version="1.0",
    jcVersion="2.1",
    expmap=true,
    expmapFile="com/sec/mobile/fra/javacard/fra_map.exp",
    intSupport=false,
    stringSupport=false)

package com.sec.mobile.fra;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;