package com.sec.mobile.fra;
import com.oberthur.apisourcer.annotation.*;

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="equals",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false)
/// @endcond

public class ClearList {

	/**
	 * Retrieves the numbers of whole entries on CLEAR-LIST.
	 * @return Number of entries (0x0000~7FFF)
	 */
    public static short getNumberOfEntries()
    {
          return (short) 0;
    }

	/**
	 * Triggering �factory reset� operation.
	 */
    public static void doFactoryReset()
    {
    }

}
