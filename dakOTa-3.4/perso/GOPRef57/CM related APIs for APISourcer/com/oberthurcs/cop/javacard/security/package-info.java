@APISourcer_Package_Description(
    AID="636F70736563757269747920",
    version="2.4",
    jcVersion="2.2",
    expmap=true,
    expmapFile="com/oberthurcs/cop/javacard/security/javacard/security_map.exp",
    intSupport=false,
    stringSupport=false,
    expToImport="javacard/framework/javacard/framework.exp")

package com.oberthurcs.cop.javacard.security;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;