/***************************************************************************
 *
 *	Projet :			COP   ( API native com.oberthurcs.cop.javacard.security.SecurityDomain )
 *
 *	Module:             SD.c
 *
 *   Description:        COP SecurityDomain  functions
 *
 *   Edition:            OCS 02/09/02
 *
 *   Modifications:        
 *
 *   07/01/03            suppression de la reference KeySet Implicite
 *                       lie a la definition donnee par Visa GlobalPlatform 2.1 Card Implementation Requirements 60 of 88
 *						Version 1.0 gestion du keySet mise a jour entete 
 *
 ***************************************************************************/
package com.oberthurcs.cop.javacard.security;
import javacard.framework.APDU;
import javacard.security.Key;
import javacardx.crypto.Cipher;
import com.oberthur.apisourcer.annotation.*;

/**
* SecurityDomain class use as stub to native code <br>
* This  module contains the Class  is  the  native  implementation  of  the  Security  Domain 
* allowing to optimize the process and memory size since it benefits of the direct access to the BIOS and VM method. 
* <br>
*  This module is directly linked to SFRs from the installer group (InstG). It is a SFR-enforcing module. 
*/

/// @addtogroup M_API_SecurityDomain CM Security Domain API
/// @{

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
		first_virtual_method="equals",
		exception_number_to_raise="0",
		number_of_references="NUMBER_REF_SD",
		number_field_instance="NUMBER_WORD_SD",
		implemented_interface="",
		OnCardDebugger_implementation=false
	    )
/// @endcond
public class SecurityDomain 
{
	/** Entity authentication has occurred  */
	public static final byte AUTHENTICATED = (byte) 0x80;
	/** The unwrap method will decrypt incoming command data (0x02) */
	public static final byte C_DECRYPTION = (byte) 0x02;
	/** The unwrap method will verify the MAC on an incoming command (0x01).*/
	public static final byte C_MAC= (byte) 0x01;
	/** Entity Authentication has not occurred (0x00). */
	public static final byte NO_SECURITY_LEVEL= (byte) 0x00;
	/** The wrap method will encrypt the outgoing response data (0x20).*/
	public static final byte R_ENCRYPTION= (byte) 0x20;
	/** The wrap method will generate a MAC for outgoing response data (0x10).*/ 
	public static final byte R_MAC	= (byte) 0x10;

    /** Secure Channel Protocol Identifier   */
	public static final  byte  SECURE_CHANNEL_PROTOCOL_1	= (byte) 01;

    /** Secure Channel Protocol Identifier   */
	public static final  byte  SECURE_CHANNEL_PROTOCOL_2	= (byte) 02;

    /** Secure Channel Protocol Explicit Initiation, ICV set to zero and KeySet contains 1 key     */
	public static final  byte  INIT_EXPLICIT_ICV_0_1KEYS 	= (byte) 0x04;

    /** Secure Channel Protocol Explicit Initiation, ICV set to zero and KeySet contains 3 keys     */
	public static final  byte  INIT_EXPLICIT_ICV_0_3KEYS 	= (byte) 0x05;

    /** Secure Channel Protocol Implicit Initiation, ICV set to MAC over AID and KeySet contains 1 key     */
	public static final  byte  INIT_IMPLICITE_ICV_MAC_1KEYS 	= (byte) 0x0A;

    /** Secure Channel Protocol Implicit Initiation, ICV set to MAC over AID and KeySet contains 3 keys     */
	public static final  byte  INIT_IMPLICITE_ICV_MAC_3KEYS 	= (byte) 0x0B;

    /** Internal constant meaning that the Initialize Update has been successfully processed */ 
	protected static final byte INIT_CHANNEL	= (byte) (	~(AUTHENTICATED|C_DECRYPTION|C_MAC|NO_SECURITY_LEVEL|R_ENCRYPTION|R_MAC)) ;

	/** Key version related with the Key Set DAP */ 
	public static final byte KEY_SET_DAP_VERIFICATION = (byte) 0x73;

	/** Key version related with the Initialization Key Set */ 
	public static final byte INITIALIZATION_KEY = (byte) 0xFF;

	/** Constant meaning that Secure Channel has been opened in OTA mode  */ 
	public final byte secure_channel_OTA = (byte) 0x70;
		
	//------------------------------------------
	//	Session Data Offset Identifiers
	//------------------------------------------
	/** Key length of the GP's Key */ 
	private  static final byte KEY_LENGTH = (byte) 16;

	/** Length of the ICV  */ 
	private  static final byte  ICV_LENGTH = (byte ) 8 ;
	/**	Index of security level into sessionData (holds the security level see AUTHENTICATED etc...)  */
	private  static final byte SESSION_SECURITY_LEVEL_INDEX = (byte )	0 ;
	/**	Index of ICV  into sessionData   */
	private  static final byte SESSION_ICV	= (byte)	(SESSION_SECURITY_LEVEL_INDEX + 1) ;
	/**	Index of SESSION_ENC_KEY   */
	private static final byte SESSION_ENC_KEY = (byte)	(SESSION_ICV + ICV_LENGTH) ;
	/**	Index of SESSION_MAC_KEY   */
	private  static final byte SESSION_MAC_KEY = (byte)	(SESSION_ENC_KEY + KEY_LENGTH);
	/**	Index of SESSION_KEY_ENCRYPTION_KEY   */
	private  static final byte SESSION_KEY_ENCRYPTION_KEY	= (byte)(SESSION_MAC_KEY + KEY_LENGTH);
	/**	Size of  sessionData   */
	private  static final byte  SESSION_DATA_SIZE	= (byte) (SESSION_KEY_ENCRYPTION_KEY + KEY_LENGTH + 1);

/// @cond (!DOXYGEN)

// uncomment all fields to acces then in On card Debugger.
// !!!! be carefull to not let field visible for security issue!!!!
//	private byte  [] sessionData;
//	/** keySetList		*/
//	private KeySet keySetList;
//	/** Key-Set used while key loading	*/
//	private KeySet loadingkeySet;
//	/** Data Store Array	*/
//	private byte	[] dataStore;
//	/**	Secure Channel Protocole Identifier	*/
//	private byte   scpIdentifier;
//	/**	Secure Channel Protocole Mode	*/
//	private byte   scpMode;

 	short ocd_dummy_1;
	short ocd_dummy_2;
	short ocd_dummy_3;
	short ocd_dummy_4;
	short ocd_dummy_5;
// private short ocd_pack_B8scpIdentifier_B8scpMode ;
	short ocd_dummy_6;
	short ocd_dummy_7;
	short ocd_dummy_8;
	short ocd_dummy_9;
	short ocd_dummy_10;
/// @endcond

	/**
     * This class implements a <i>Security Domain </i> stub for native implementation. <br> 
     * This class allows to implement an Applet Security Domain either for GP2.2.1, GP2.1, or GSM<br> 
     * It features class handling mechanism, including secure channel initialization as well as 
     * Management KeySet (load,update,Delete).<br>
     * It also provides the public utility methods allowing to build the Security Domain's API services 
	 *    used by the applet associated with this Security Domain  ( i.e Interface org.globalplatform SecureChannel )
	 *
     * @param scpIdentifier SCP identifier 
     * @param scpMode SCP mode
     */
     /// @see #SecurityDomain_init(void) for native implementation
	public SecurityDomain(byte scpIdentifier, byte scpMode)
	{;}

	/**
	 *	This method is the entry point for the process of the commands related to the SCP 
     * <p>Example: In GP2.1 either SCP01 or  SCP02 :  Initialize Update, External Authenticate, beginRMACSession etc... 
     * <p>Notes :
     * <ul>
     * <li>This method is also used by an applet to process APDU commands relate to the security
	 *	mechanism used by the Security Domain. As the intention is to allow an Application to be associated with a
	 *	Security Domain without having any knowledge of the security mechanisms used by the Security Domain,
	 *	the applet assumes that APDU commands that it does not recognize are part of the security mechanism and
	 *	will be recognized by the Security Domain. The applet can either invoke this method prior to determining if
	 *	it recognizes the instruction or only invoke this method for instructions it does not recognize.
     * <li>for when this method process the InitializeUpdate command VOPO2.01' the lsb Byte return Alea Byte 
     *          which can be used as a channel number
     *  </ul> 
     * <br>
     * <br>
     * Previously to call this method the Security Domain Applet or Applet has received the data
	 *
	 *	@param p_ba_buffer buffer containing the instruction and data to process,
	 *	@param p_s_inOffset offset in buffer at which input data shall be read,
     * @param p_s_outOffset offset in buffer at which output data shall be written.
	 *	@return short length of data filled for answer.
	 */
    /// @see #SecurityDomain_doScpProcess(void) for native implementation
	public short doScpProcess (byte[] p_ba_buffer, short p_s_inOffset, short p_s_outOffset)
	{return (short) 0 ;}


	/**
     *<p>This method is the entry point for the process of the commands related to the Keyset 
	 *   Management 
     * <p>Example:  PutKey DeleteKey 
     * <p>Previously to call this method the Security Domain Applet has received the data 
	 *
     * @param   apdu the APDU object containing command in buffer (not used) 
     **/
    /// @see #SecurityDomain_doKeySetProcess(void) for native implementation
	public void doKeySetProcess(APDU apdu)
	{;}

    /**
     *<p>This method is the entry point for the process of the commands related to the Keyset 
     * Management 
     * <p>Example:  GetData 
     * <p>Previously to call this method the Security Domain Applet has received the data 
     *
     * @param p_ba_buffer buffer containing the instruction and data to process,
     * @param p_s_inOffset offset in buffer at which input data shall be read,
     * @param p_s_outOffset offset in buffer at which output data shall be written.
     * 
     * @return short length of data filled for answer.
     **/
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("BSS")
	/// @endcond		 
    /// @see #SecurityDomain_doKeySetProcess_BSS(void) for native implementation
    public short doKeySetProcess(byte[] p_ba_buffer, short p_s_inOffset, short p_s_outOffset)
    {
    	return 0;
    }

	/**
	 *
	 *	This method is an utility method which allows to reset Session Data related with the Secure Channel 
     *  Protocol. <br>
     *  for example this method shall directly called by the API resetSecurity Interface {@link org.globalplatform SecureChannel}
     **/
    /// @see #SecurityDomain_resetSecurity(void) for native implementation  
	public void resetSecurity()
	{;}

	/**
	 *	This method is an utility method which allows to Decrypt Data located in the global array associated 
	 *   with the Apdu Buffer. The algorithm used to Decrypt the data depends of the choice of the implementation
     * GP2.1, GP2.2.1 or GSM. However the key used is the current static Decipher key of the KeySet actif
	 *   (Keyset used to open the channel)
     *<p>
	 *   Previously to call this method the Applet has received the data 
     *<pre>
     *  public boolean decryptVerifyKey(byte channel, APDU apdu, short offset) throws CardRuntimeException
     *  {
     *  //  the APDU buffer should be:
     *  //  offset:         algorithm Ident
     *  //  offset+1:       len = length of key
     *  //  offset+2:       key data
     *  //  offset+2+len:   length of key check value
     *  //  offset+3+len:   key check value
     *
     *  if (( channel!= channel_actif ) ||  getSecurityLevel() != AUTHENTICATED )
     *      ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
     *  
     *      byte algoID = buffer[offset++];
     *      byte[] buffer = apdu.getBuffer();
     *      ..............
     *      //  get key length, it must be a multiple of 8, this is checked by Cipher instance
     *      short length = (short)(0x00FF & buffer[offset++]);
     *      ..............
     *      offset = securityDomain.decryptData(buffer, offset, length);
     * }
	 *
     *</pre>
     * <p>Note : this method shall be used to implement the API interface ProviderSecurityDomain
	 *   decryptVerifyKey(byte channel, APDU apdu, short offset) throws CardRuntimeException;
	 *   the code in the java shall be </p>
	 *
     *  @param  baBuffer byte array where the data are located 
     *  @param  sOffset short points the first byte of the data to decrypt 
     *  @param  sLength short length of the data to decrypt 
     *  @return length of the data decrypted 
     */
    /// @see #SecurityDomain_decryptData(void) for native implementation
	public short decryptData( byte [] baBuffer, short sOffset,  short sLength)
	{return (short) 0;}

	/**
	 *	This method is used to process the APDU buffer content after receiving it
	 *	from off-card. The processing is according to the requirements for integrity
	 *	and confidentiality that are established when a Secure Channel is opened. ( security level )
	 *
     * @param   baBuffer the byte array associated with the APDU handle
	 *	@param	sOffset points on the Class Byte ( always 0 )
	 *	@param	sLength of the data to unwrap ( Lc + 5)
	 *	@return length of the data unwrapped 
     **/
    /// @see #SecurityDomain_unwrap(void) for native implementation
	public short unwrap( byte [] baBuffer, short sOffset, short sLength )
	{return (short) 0;}

	/**
     * This method is used to process the APDU buffer content before sending it 
	 *   to the off-card.The processing is according to the requirements for integrity
	 *	and confidentiality that are established when a Secure Channel is opened.( security level )
     * @param   baBuffer the byte array associated with the APDU handle
	 *	@param	sOffset points on the first Data byte ( always 5 )
	 *	@param	sLength of the data to wrap 
	 *	@return length of the data wrapped 
	 */
    /// @see #SecurityDomain_wrap(void) for native implementation
	public short wrap( byte[] baBuffer, short sOffset, short sLength )
	{return (short) 0;}


	/**
	 *	This method is used to encrypt the Data located in the global array associated 
	 *   with the Apdu Buffer. The algorithm used to encrypt the data depend of the choice of the implementation
     * GP2.1,GP2.2.1 or GSM. However the key used is the current static Decipher key of the KeySet actif
	 *   (Keyset used to open the channel)
	 *
     * @param   baBuffer the byte array associated with the APDU handle
	 *	@param	sOffset points on the first byte  to encrypt 
	 *	@param	sLength of the data to encrypt  
	 *	@return length of the data encrypted 
	 */
    /// @see #SecurityDomain_encryptData(void) for native implementation
	public short encryptData( byte[] baBuffer, short sOffset, short sLength )

	{return (short) 0;}


	/**
	 *	This method is used to determine whether the Security Domain has performed authentication and to
     * determine what level of security will be applied by the {@link #wrap(byte[], short, short)} and {@link #unwrap(byte[], short, short)} methods.
	 *
     * @return security level byte
	 */
    /// @see #SecurityDomain_getSecurityLevel(void) for native implementation
	public byte getSecurityLevel()
	{ return (byte) 0;}

	/**
     * This method is used to update the current security level used by the all {@link #wrap(byte[], short, short)} 
     * and  {@link #unwrap(byte[], short, short)} methods calls.
	 *
	 *	@param	bSecurityLevel new security level 
	 */
    /// @see #SecurityDomain_setSecurityLevel(void) for native implementation
	public void setSecurityLevel(byte bSecurityLevel)
	{};
	
	/**
	 *	This method is used to determine if the Security Domain has at least one keyset
	 *
	 *	@param	scpId 	the SCP Identifier which enables to differentiate SCP0x and SCP80 KeySetList. 
     * @return  FALSE if keysetList is empty TRUE otherwise
	 */
    /// @see #SecurityDomain_keySetListIsInitialized(void) for native implementation
	public boolean keySetListIsInitialized(byte scpId)
	{ return false;}

/// @cond (VOP_SUPPORTED)
	/**
     * This method is used to verify the key check value of the key located into the apdu buffer<br>
	 *   This method expect the following template data.
	 *<pre>
     * ---------------------------------------
     * | lg |  key  ....| 03 | keycheckvalue  |
     * ---------------------------------------
	 *</pre>
     * @param   apdu <code>APDU</code> object 
	 *	@param	offset points on lg key 
	 *
     * @return TRUE if key check value is correct FALSE otherwise
	 */
    /// @see #SecurityDomain_verifyKeyCheckValue(void) for native implementation
	public boolean verifyKeyCheckValue(APDU apdu, short offset) 
	{ return false;}
/// @endcond

/// @cond (COP_GP2_2_AMD_B)
	/**
    * This method is called in Amdt B to obtain the security level in OTA mode SCP81
	 *
    * @return the logical channel number :
    * <ul>
    * <li>0xA5 in OTA mode SCP81       10100101  if security is ensured by the SD 
    * <li>0xAA in OTA mode SCP81       10101010  if security is ensured by an other SD 
    * <li>0x00 if no OTA mode SCP81 Secure Channel
    * </ul>
	 */
    /// @see #SecurityDomain_getAssignedSecureChannel(void) for native implementation
	public byte getAssignedSecureChannel()
	{ return (byte) 0;}
/// @endcond
	/**
	 * This method checks for a SecurityDomain if the assigned secure channel is equal to the logical
	 * channel on which the command is received.
     * <p>If the command is received trough OTA interface, this method returns
     * <ul> 
     *  <li> TRUE when the SSD is processing the security scp81 or it has its own key set. 
     *  <li> FALSE otherwise
     *  </ul>
	 * @return <code>true</code> if equals, <code>false</code> otherwise.
	 */
   /// @see #SecurityDomain_checkChannelContext(void) for native implementation
	public boolean checkChannelContext()
	{ return false;}

	
	/**
	 *	This method checks if the Secure Channel Protocol is supported by the platform.
	 *  If the protocol is supported this method return silently. If not, an exception is raised.
	 *	<p>Notes:<ul>
	 *	<li><em>b1 represents SCP01,</em>
	 *	<li><em>b2 represents SCP02,</em>
	 *	<li><em>b3 represents SCP03.</em>
	 *	</ul>
	 *	<p>
     * @param p_baBuffer    source buffer 
     * @param p_sOffset     <code>short</code> offset within source buffer where information started
	 */
    /// @see #SecurityDomain_isSCPSupported(void) for native implementation
	public static void isSCPSupported(byte[] p_baBuffer, short p_sOffset)
	{    
		;
	}

    /**
 	 *	This method is the handle to store data in the Data Store area of the Security Domain. This Data Store is used to maintain 
	 *  GlobalPlatform specified data (tags '42', '45' and '66'), tag 'CF'  and issuer proprietary TLV coded data that may be personalized 
	 *  by the issuer. No data element can be larger than 127 bytes.
	 * 
	 *	@param	p_baBuffer the byte array associated with the APDU handle
	 *	@param	p_sLengthOffset points on the first byte of data that will be stored 
	 *	@param	p_sTag Tag which will be stored   
	 *	@return	byte	containing flags representing supported SCP. 
	 */
    /// @see #SecurityDomain_setDataStore(void) for native implementation
	public byte setDataStore (byte[] p_baBuffer, short p_sLengthOffset, short p_sTag)
	{
		return (byte) 0;
	}

    /**
     * This method is the handle to retrieve store data in the Data Store area of the Security Domain. This Data Store is used to maintain 
	 *  GlobalPlatform specified data (tags '42', '45' and '66'), tag 'CF'  and issuer proprietary TLV coded data that may be personalized 
	 *  by the issuer. No data element can be larger than 127 bytes.
	 * 
	 *	@param	p_baBuffer the byte array associated with the APDU handle
	 *	@param	p_sLengthOffset points on the first byte where the data must be copied 
     * @param   p_sTag          Tag which will be retrieved   
	 *	@return	byte	length of data returned
	 */
    /// @see #SecurityDomain_getDataStore(void) for native implementation
	public byte getDataStore (byte[] p_baBuffer, short p_sLengthOffset, short p_sTag)
	{
		return (byte) 0;
	}
	
    /**
 	 *	This method enables to create an object Key.
	 * 
	 *	@param	p_ba_buffer the byte array associated with the APDU handle
	 *	@param	p_s_offset points on the first byte of the key value 
     * @return  Allocated Key object 
	 */
    /// @see #SecurityDomain_createKey(void) for native implementation
	public Key createKey (byte[] p_ba_buffer, short p_s_offset)
	{
		return null;
	}
	
/// @cond (!(NO_CASD))  
    /**
     * <p>This method is used to recover a cryptographic key from a set of data 
     * structures provided in the input buffer <code>inBuff</code>.</p>
     * <p>As a mandatory step, the recovery mechanism includes the verification of
     * the origin and integrity of the recovered key.<br>
     * This method knows, from the set of data structures present in the input 
     * buffer, which recovery mechanism is to be used.</p>
     * The recovered key is written in the output buffer <code>outBuff</code> at specified
     * offset <code>outOffset</code>, in the form of a key data structure whose format 
     * depends on the type of the key. <br>
     * <p>A call to this method resets this instance of the Authority interface to
     * the state it was in when previously initialized via a call to <code>init()</code>. 
     * That is, the object is reset and available to recover another key. 
     * The input and output buffers may overlap and shall be global arrays.</p>
     * @param p_ocipher Cipher object reference of the CASD.
     * @param inBuff    byte[] containing input data.
     * @param inOffset  short offset of input data.
     * @param inLength  short length of input data.
     * @param outBuff   byte[] the buffer where recovered key data structure shall be written.
     * @param outOffset short offset where recovered key data structure shall be written.
     * @return <code>Length</code> of the recovered key data structure written 
     * in outBuff at outOffset,or 0 if the recovery mechanism failed 
     * (e.g. recovered key was considered invalid).
	 */
    /// @see #SecurityDomain_recoverKey(void) for native implementation
	public short recoverKey (Cipher p_ocipher, byte[] inBuff, short inOffset, short inLength, byte[] outBuff, short outOffset)
	{
		return (short)0;
	}

    /// @cond ((SCENARIO_1PK || SCENARIO_1NPK) && !(NOT_CONFIDENTIAL_EXCHANGE))
/**
 * This method is used by the confidential setup Secure Channel Keys. 
 * It ensures that keys or random are exchanged in secure way between the java
     * and the native code.  
 * This method knows, from the set of data structures present in the input 
     * buffer, the type of the Object1 and Object2 and which process shall be executed.
 * The input and output buffers may overlap and shall be global arrays.
 * @param p_Object1 typically reference of the Cipher in scenario 1 PK    
 * @param p_Object2 typically reference of the Key  in scenario 1 PK    
 * @param inBuff the buffer containing input data.
 * @param inOffset offset of input data.
 * @param inLength length of input data.
 * @param outBuff the buffer where the output data shall be written 
 * @param outOffset offset  output data shall be written
 * @return <code>Length</code> Output data shall be written  
 * in outBuff at outOffset,or 0 if the confidentialExchange mechanism failed 
 * 
     * @since version 2.1
 */
    /// @see #SecurityDomain_ConfidentialExchange(void) for native implementation
	public short confidentialExchange(Object p_Object1,Object p_Object2,byte[] inBuff, short inOffset, short inLength, byte[] outBuff, short outOffset)
	{
		return (short)0;
	}
    /// @endcond
/// @endcond
    /**
     * add SCP identifier and mode into current security domain 
     * @param scpIdentifier SCP identifier to add
     * @param scpMode       SCP mode to add
     * @return returns <code>SEC_TRUE</code> if SCP can be added <code>SEC_FALSE</code> otherwise
     */
    /// @see #SecurityDomain_addSCP(void) for native implementation
	public byte addSCP(byte scpIdentifier, byte scpMode)
	{
		return (byte)0;
	}

/// @cond (DEPRECATED)
    /**
     * used to retrieve Key Template information
     * @param KvnIndex  KVN index   
     * @param outBuff   byte array to fulfill
     * @param outOffset offset to start writing
     * @return keySet offset 
     * @deprecated
     */
    /// @see #SecurityDomain_getKeySet(void) for native implementation
	public short getKeySet(short KvnIndex, byte[] outBuff, short outOffset)
	{
		return (short)0;
	}
/// @endcond
/// @cond (DEPRECATED)
    /**
     * used to retrieve Key Template information  
     * @param KvnIndex  KVN index
     * @return key length
     * @deprecated
     */
    /// @see #SecurityDomain_getKeySetLength(void) for native implementation
	public short getKeySetLength(short KvnIndex)
	{
		return (short)0;
	}
/// @endcond
    
}
/// @}
