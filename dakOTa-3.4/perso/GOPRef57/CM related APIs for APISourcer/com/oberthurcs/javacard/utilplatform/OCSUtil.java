package com.oberthurcs.javacard.utilplatform;

import javacard.framework.APDU;
import com.oberthur.apisourcer.annotation.*;




/// @addtogroup M_API_UtilPlatform CM API UtilPlatform
/// @{

 /**
 * OCSUtil class provide many useful public method <br>
 * This module provides the Card Domain and the Oberthur Applet 
  *			with the no standard API such additional methods for OwnerPin or 
  *			CPLC data access. 
  *
  *	This module is categorized SFR-supporting. 
  *
  */

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="equals",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false
	)
/// @endcond
public class OCSUtil
{
	private static final byte major_version = 2;

	private static final byte minor_version = 8;

/// @cond (COP_GP2_2_AMD_B)
/* Constants linked to Amendment B */
	
	// These are offsets to reach information after the HTTP header in the server 
	// response has been analyzed
	
	/** offset of header status in response HTTP header */
	public static final byte RESP_HEADER_STATUS 						= (byte)0x00;
	/** offset of application content type in response HTTP header */
	public static final byte RESP_APP_CONTENT_TYPE 						= (byte)0x02;
	/** offset of x administration protocol in response HTTP header */
	public static final byte RESP_OFF_HEADER_X_ADMIN_PROTOCOL 			= (byte)0x03;
	/** offset of x administration protocol length in response HTTP header */
	public static final byte RESP_LEN_HEADER_X_ADMIN_PROTOCOL 			= (byte)0x05;
	/** offset of x administration next URI in response HTTP header */
	public static final byte RESP_OFF_HEADER_X_AMDIN_NEXT_URI			= (byte)0x07;
	/** offset of x administration next URI length in response HTTP header */
	public static final byte RESP_LEN_HEADER_X_AMDIN_NEXT_URI			= (byte)0x09;
	/** offset of content type in response HTTP header */
	public static final byte RESP_OFF_HEADER_CONTENT_TYPE				= (byte)0x0B;
	/** offset of content type length in response HTTP header */
	public static final byte RESP_LEN_HEADER_CONTENT_TYPE				= (byte)0x0D;
	/** offset of content length in response HTTP header */
	public static final byte RESP_HEADER_CONTENT_LENGTH					= (byte)0x0F;
	/** offset of chunk message in response HTTP header */
	public static final byte RESP_MESSAGE_CHUNKED						= (byte)0x13;
	/** offset of targeted AID length in response HTTP header */
	public static final byte RESP_LEN_AID_TARGETED_APPLICATION_AID		= (byte)0x17;
	/** offset of targeted AID in response HTTP header */
	public static final byte RESP_BEGINNING_TARGETED_APPLICATION_AID	= (byte)0x19;
	
	// These are offsets to provide information in order to build the HTTP header
	
	/** offset of URI length in request HTTP header */		
	public static final byte REQ_HEADER_URI_LEN							= (byte)0x00;
	/** offset of host administration length in request HTTP header */
	public static final byte REQ_HEADER_ADMINISTRATION_HOST_LEN			= (byte)0x01;
	/** offset of agent ID length in request HTTP header */
	public static final byte REQ_HEADER_AGENT_ID_LEN					= (byte)0x02;
	/** offset of resume flag in request HTTP header */
	public static final byte REQ_HEADER_RESUME_FLAG						= (byte)0x03;
	/** offset of status script in request HTTP header */
	public static final byte REQ_HEADER_SCRIPT_STATUS					= (byte)0x04;
	/** offset of application content type in request HTTP header */
	public static final byte REQ_APP_CONTENT_TYPE						= (byte)0x05;
	/** offset of application content length in request HTTP header */
	public static final byte REQ_HEADER_CONTENT_LENGTH					= (byte)0x06;
	/** offset of start of string in request HTTP header */
	public static final byte REQ_HEADER_START_OF_STRINGS				= (byte)0x08;
		
	/** HTTP status script OK */
	public static final byte SCRIPT_STATUS_OK							= (byte)0x00;
	/** HTTP status script UNKNOWN application */
	public static final byte SCRIPT_STATUS_UNKNOWN_APPLICATION			= (byte)0x01;
	/** HTTP status script NOT a security domain */
	public static final byte SCRIPT_STATUS_NOT_A_SECURITY_DOMAIN		= (byte)0x02;
	/** HTTP status script security error */
	public static final byte SCRIPT_STATUS_SECURITY_ERROR				= (byte)0x03;
	/** HTTP status script proprietary initial post */
	public static final byte SCRIPT_STATUS_PROPRIETARY_INITIAL_POST		= (byte)0x05;
/// @endcond    
	/**
	 * This method allows an application to resize a byte array handled by a JavaCard applet.
	 *
	 * <p>Notes:<ul>
	 * <li><em> if the newLength is set to (short) 0 the null reference is returned </em>
	 * </ul>
	 * <p>
	 * @param buffer	a byte array reference
	 * @param newLength short value of the new length requested
	 * @return buffer a byte array reference or null
	 * @exception javacard.framework.CardRuntimeException if the resize isn't possible
	 * @exception java.lang.ArrayIndexOutOfBoundsException if newLength is negative 
	 */
	/// @see OCSUtil_resize(void) for native implementation
	public static byte[]  resize (byte[] buffer, short newLength)
	{ return null;}


	/**
	 * This method returns the card's production life cycle data.<br>
	 * The calling applet passes a handle to APDU object and an 
	 * offset where CPLC data shall be placed.
	 * @param	apdu		handle to APDU object used to store returned data
	 * @param	apdu_offset offset within APDU buffer to start CPLC writing
	 * @param	cplc_offset	offset within CPLC string to start reading
	 * @param	length 		length to be returned
	 *
	 * @deprecated since GOP Ref V01.08
	 */
	/// @see OCSUtil_getCPLCData(void) for native implementation
	public static void getCPLCData(APDU apdu, short apdu_offset, short cplc_offset, short length)
	{ ;}

	/**
	 * This method returns the card's production life cycle data.
	 * The calling applet passes a handle to APDU object and an
	 * offset where CPLC data shall be placed.
	 * @param	buffer		buffer used to store returned data
	 * @param	offset		offset within buffer to start CPLC writing
	 * @param	cplc_offset	offset within CPLC string to start reading
	 * @param	length 		length to be returned
	 */
	/// @see OCSUtil_getCPLCData_BaSSS(void) for native implementation
	
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("BaSSS")
	/// @endcond	
	public static void getCPLCData(byte[] buffer, short offset, short cplc_offset, short length)
	{ ;}

	/**
	 * This method enables to known if last selection was Implicit or not  .
	 * <p>
	 * @return <code>true</code> if last selection was implicit, <code>false</code> otherwise.
	 */
	/// @see OCSUtil_isImplicitelySelected(void) for native implementation
	public static boolean isImplicitelySelected()
	{
		return false;
	}

	/**
	 * This method returns the size available in EEprom .
	 * <p>
	 * @return <code>short</code> free EEprom size.
	 */
	/// @see OCSUtil_getSize(void) for native implementation
	public static short getSize()
	{
		return 0;
	}


	/**
	 * This method enables to known if last Reset is a Warm reset  or not  .
	 * <p>
	 * @return <code>true</code> if reset is WarmReset , <code>false</code> otherwise.
	 */
	/// @see OCSUtil_isWarmReset(void) for native implementation
	public static boolean isWarmReset()
	{
		return false;
	}


	/**
	 * This method converts data located in APDU buffer from ASCII to BCD.<br>
	 * The calling applet passes a handle to APDU object, offset where the first data shall be placed and the length
	 * <p>Notes:<ul>
	 * <li><em>Conversion from ASCII format to BCD format is valid for numeric characters only:
	 * the numeric characters coded on one byte are converted to numeric nibbles, padded together in bytes, and a padding nibble
	 * 'F' is added on the right if necessary </em>
	 * <li><em> e.g  the conversion of the following data 303132 is 012F and lg returned is 2 </em>
	 * </ul>
	 * @param	babuffer byte array ( APDU byte array ) used to passe the data
	 * @param	offset offset within APDU buffer to start data
	 * @param	length of the data
	 * @return	length of the new data or 0 if the conversion is unsuccessful
	 */
    /// @see OCSUtil_a2bcd(void) for native implementation
	public static byte a2bcd(byte[] babuffer, short offset, byte length)
	{
		return length;
	}

	/**
	 * This method checks that the data located in the APDU buffer are a numerical value, excepted for the last nibble
	 * whom the value could be F. <br>
	 *	The calling applet passes a handle to APDU object, offset where the first data shall be placed and the length
	 * <p>Notes:<ul>
	 * <li><em> e.g  The following data 12 34 56 78 9F or 01 23 45 67 89 are valid data </em>
	 * </ul>
	 * @param	babuffer byte array ( APDU byte array ) used to passe the data
	 * @param	offset offset within APDU buffer to start data
	 * @param	length of the data
	 * @return	length of data or 0 if error
	 */
    /// @see OCSUtil_checkBCDformat(void) for native implementation	
	public static byte checkBCDformat (byte[] babuffer, short offset, byte length)
	{
		return length;
	}
	
	/** constant to check array length in TLVReadLength() method*/
	public final static byte CHECK_ARRAY_LENGTH = 0x01;
	/** constant to refuse length format 0x80 in TLVReadLength() method*/
	public final static byte REFUSE_0x80 = 0x02;
	
	
	/**
     * This method read an ASN.1 length in a buffer. with specific behavior on check.
	 * @param buffer byte array where length need to be read
	 * @param offset of the length
     * @param   check byte value to specify the check<ul><li>{@link CHECK_ARRAY_LENGTH} if bit 1 set to 1 : check byte array length </li>  
                      <li>{@link REFUSE_0x80} if bit 2 set to 1 : refuse 0x80 coded on one byte</li></ul>
	 * @return length
	 * @throws javacard.framework.ISOException 
	 */
    /// @see OCSUtil_TLVReadLength(void) for native implementation	
	public static short TLVReadLength(byte[] buffer, short offset, byte check) throws javacard.framework.ISOException
	{
		return 0;
	}

	/**
	 * This method return the offset after length when TLVReadLength(byte[], short) was previously used
	 * @param buffer buffer byte array where offset need to be read
	 * @param offset to start reading
	 * @return the offset of data
	 */
	 /// @see OCSUtil_TLVGetDataOffset(void) for native implementation	
	public static short TLVGetDataOffset(byte[] buffer, short offset)
	{
		return 0;
	}
	
/// @cond(TIMER_WAIT)
	/**
	 * This method wait 100 millisecond use to waste time
	 * @since export file version 2.1
	 */
	/// @see OCSUtil_TimerWait100ms(void) for native implementation	
	public static void TimerWait100ms()
	{
	}
/// @endcond

/// @cond(COP_GP2_2_AMD_B)
	/**
	 * This method is used to check GP Amendment B HTTP header.
	 * 
	 * @param headerArray buffer to store the AMD_B HTTP parameters 
	 * @param headerArrayOffset offset in the buffer where data will be stored
	 * @param inBuffer buffer containing AMD_B HTTP header
	 * @param inBufferOffset offset in the buffer where data are stored
	 * @param inBufferLength length of buffer
	 * @return <ul><li>1 if AMD_B HTTP header is valid.
	 * 		   <li>0 if AMD_B HTTP header is invalid.</ul>
	 * 
	 */
	/// @see OCSUtil_AMDB_getAndCheckHTTPHeader(void) for native implementation	
	public static final boolean AMDB_getAndCheckHTTPHeader(	byte headerArray[],
															short headerArrayOffset,
															byte inBuffer[],
															short inBufferOffset,
															short inBufferLength)
	{
		return false;
	}

	/**
	 * This method is used to create GP Amendment B HTTP header.
	 * 
	 * @param outBuffer buffer to store the AMD_B HTTP header 
	 * @param outBufferOffset offset in the buffer where data will be stored
	 * @param outBufferLength length of buffer available
	 * @param ressourceBuffer buffer containing AMD_B HTTP header parameters
	 * @param ressourceBufferOffset offset in the buffer where data are stored
	 * @param ressourceBufferLength length of buffer
	 * @return offset of the end of data in the outBuffer.
	 * 		   0xFFFF if error during creating AMD_B HTTP header.
	 * 
	 */
	/// @see OCSUtil_AMDB_setHTTPHeader(void) for native implementation	
	public static final short AMDB_setHTTPHeader(	byte outBuffer[],
													short outBufferOffset,
													short outBufferLength,
													byte ressourceBuffer[],
													short ressourceBufferOffset,
													short ressourceBufferLength)
	{
		return -1;
	}

	/**
	 * This method is used to retrieve the length of GP Amendment B HTTP header.
	 * 
	 * @param outBuffer buffer containing AMD_B HTTP header 
	 * @param outBufferOffset offset in the buffer where data are
	 * @param outBufferLength length of buffer 
	 * @return offset of the end of HTTP header in the outBuffer.
	 * 		   0xFFFF if AMD_B HTTP header is incomplete.
	 */
	/// @see OCSUtil_AMDB_getHTTPHeaderLength(void) for native implementation
	public static final short AMDB_getHTTPHeaderLength(	byte outBuffer[],
														short outBufferOffset,
														short outBufferLength)
	{
		return -1;
	}

	/**
	 * This method is used to create GP Amendment B HTTP header.
	 * 
	 * @param inBuffer buffer containing AMD_B HTTP body chunked formatted 
	 * @param inBufferOffset offset in the buffer where data are stored
	 * @param inBufferLength length of buffer available
	 * @param processedLen buffer where the length of processed data will be stored if CHUNKED is incomplete, 0xFFFF if CHUNKED is complete
	 * @param processedLenOff offset in the buffer where data will be stored
	 * @return offset of the end of data in inBuffer.
	 * 
	 */
	/// @see OCSUtil_AMDB_unchunkHTTPBody(void) for native implementation
	public static final short AMDB_unchunkHTTPBody(	byte inBuffer[],
													short inBufferOffset,
													short inBufferLength,
													byte processedLen[],
													short processedLenOff)
	{
		return -1;
	}

/// @endcond

	/**
	 * This method is used to propagate GP Amendment B Connectivity Parameters.<BR>
	 * if there are any TLV error found, a corresponding exception shall be thrown
	 * 
	 * @param inBuffer buffer containing AMD_B Connectivity parameter (Tag 84/8F included)
	 * @param inBufferOffset offset in the buffer where data are stored
	 * @param inBufferLength length of buffer
	 * @return <ul><li>1 if AMD_B HTTP header is valid.
	 * 		   <li>0 if AMD_B HTTP header is invalid.</ul>
	 * 
	 */
	/// @see OCSUtil_AmdB_HTTPparameterUpdate(void) for native implementation	
	public static final byte AmdB_HTTPparameterUpdate(byte inBuffer[], short inBufferOffset, short inBufferLength)
	{
		return 0;
	}
	
	
	/**
	 * This method is used to Get the GP Amendment B Administration URI Parameters.
	 *  
	 * @param outBuffer buffer containing AMD_B URI parameter 
	 * @param outBufferOffset offset in the buffer where data will be stored
	 * @return length of the Amendment B URI Paramaters
	 * 		   0xFFFF if error during process
	 *  
	 */
	/// @see OCSUtil_AmdB_getURIParameter(void) for native implementation	
	public static final short AmdB_getURIParameter(byte outBuffer[], short outBufferOffset)
	{
		return 0;
	}
	
	/**
	 * This method is used to update GP Amendment B DNS IP Parameters.
	 * 
	 * @param inBuffer buffer containing AMD_B DNS IP parameter (Tag 8E)
	 * @param inBufferOffset offset in the buffer where data are stored
	 * @param inBufferLength length of buffer
	 * @return <ul><li>1 if AMD_B HTTP header is valid.
	 * 		   <li>0 if AMD_B HTTP header is invalid.</ul>
	 * 
	 */
	/// @see OCSUtil_AmdB_updateDNSIPParameter (void) for native implementation	
	public static final byte AmdB_updateDNSIPParameter(byte inBuffer[], short inBufferOffset, short inBufferLength)
	{
		return 0;
	}
	
	/**
	* This method fill the APDU buffer with the available size in EEprom .
    * <p>
    * set in Lc field the length of remaining size ( 3 ou 2 )<br>  
	* set in C_Data offset the  <b>MSB</b> of remaining size  <br>
	* set in C_Data+1 offset the  <b>LSB</b> of remaining size (or the second byte of remaining size)  <br>
	* set in C_Data+2 offset 0 value (or the <b>LSB</b> byte of remaining size)  <br>
    */
	/// @see OCSUtil_getSizeExtended(void) for native implementation
	public static void getSizeExtended()
    {;}
	
	/**
	 * Clear the Object array beginning at the specified position, for the specified length.
	 *
	 * @param oArray the Object array 
	 * @param sOff offset within Object array to start filling oValue into
	 * @param sLen length to be filled
	 */
    /// @see #OCSUtil_clearObjectArray(void) for native implementation
	public static void clearObjectArray(Object[] oArray, short sOff, short sLen)
	{
	}
	
	/**
	 * Fills the short array beginning at the specified position, for the specified length with the specified value.
	 *
	 * @param sArray the short array 
	 * @param sOff offset within Object array to start filling oValue into
	 * @param sLen length to be filled
	 * @param sValue the value to fill the short array with.
	 */
    /// @see #OCSUtil_fillShortArray(void) for native implementation
	public static void fillShortArray(short[] sArray, short sOff, short sLen, short sValue)
	{
	}
}
/// @} // end of group M_API_UtilPlatform
