/**
 * Package providing proprietary classes for useful API
 */
@APISourcer_Package_Description(
    AID="A0000000770100000010000000000002",
    version="2.8",
    jcVersion="2.2",
    expmap=true,
    expmapFile="com/oberthurcs/javacard/utilplatform/javacard/utilplatform_map.exp",
    intSupport=false,
    stringSupport=false)

package com.oberthurcs.javacard.utilplatform;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;