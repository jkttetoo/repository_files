package com.oberthurcs.javacard.utilplatform;

import javacard.framework.OwnerPIN;

import com.oberthur.apisourcer.annotation.*;


/// @addtogroup M_API_UtilPlatform
/// @{

/**
 * OwnerPINUtil class using to manage Owner Pin object
 *
 */

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="equals",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false
	)
/// @endcond
public class OwnerPINUtil
{
	private static final byte major_version = 1;
    
	private static final byte minor_version = 0;


	/**
	 * This method sets the maximum number of tries for the OwnerPIN passed in parameter .
	 * <p>Notes:<ul>
	 * <li><em>The OPEN verifies that the OwnerPIN belong to the application .</em>
	 * <li><em>The OwnerPIN try counter is reset when setting the maximum number of tries.</em>
	 * <li><em>The validated flag and invalid_submission are set to false.</em>
	 * </ul>
	 * @param myPIN the OwnerPIN's reference,
	 * @param tryLimit the maximum number of tries for the OwnerPIN.
	 * @return <code>true</code> if the try limit is set, <code>false</code> otherwise.
	 */
	 ///@see #OwnerPINUtil_b_setTryLimit(void) for native implementation
	 ///@addtogroup M_API_UtilPlatform
	public static boolean setTryLimit(OwnerPIN myPIN, byte tryLimit) {
		return false;
	}

	/**
	 * This method indicates whether an attempt has been made to compare the CVM value.
	 * <p>Note:<ul>
	 * <li><em> The OPEN verifies that the OwnerPIN belong to the application .</em>
	 * <li><em> This method does not differentiate whether the flag validated or invalid_submission is set to true
	 * </em>
	 * </ul>
	 * @param myPIN the OwnerPIN's reference.
	 * @return <code>true</code> if the flag validated or invalid_submission is set to true, <code>false</code> otherwise.
	 */
 	 /// @see #OwnerPINUtil_isSubmitted(void) for native implementation
	public static boolean isSubmitted(OwnerPIN myPIN) {
		return false;
	}

	/**
	 * This method sets the OwnerPINtry counter to 0  for the OwnerPIN passed in parameter .
	 * <p>Notes:<ul>
	 * <li><em>The OPEN verifies that the OwnerPIN belong to the application .</em>
	 * <li><em>The OwnerPINtry counter set to 0.</em>
	 * <li><em>The validated flag and invalid_submission are set to false.</em>
	 * </ul>
	 * @param myPIN the OwnerPIN's reference.
	 */
	 /// @see #OwnerPINUtil_blockState(void) for native implementation
	public static void blockState(OwnerPIN myPIN){
		;
	}

	/**
	 * This method resets the flags of the OwnerPIN  passed in parameter .
	 * <p>Notes:<ul>
	 * <li><em>The OPEN verifies that the OwnerPIN belong to the application .</em>
	 * <li><em>The validated flag and invalid_submission are set to false.</em>
	 * <li><em>The OwnerPIN tries left still be unchanged .</em>
	 * </ul>
	 * @param myPIN the OwnerPIN's reference.
	 */
	 /// @see #OwnerPINUtil_resetState(void) for native implementation
	public static void resetState(OwnerPIN myPIN){
		;
	}
}
/// @} // end of group M_API_UtilPlatform
