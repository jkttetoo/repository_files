package com.oberthurcs.javacard.eSIM;

import javacard.framework.Shareable;
import com.oberthur.apisourcer.annotation.*;

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="discarded",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false)

/// @endcond

public abstract interface ServicesForPSMInterface
  extends Shareable
{
	/** PSMService familly identifier */
	public static final short FAMILY_PSM_SERVICE = (short) 0x05;
	
   /**
   * This method is used to Delete a Profile eUICC in case of local management
   * <p>
   *
   * @param ISDPBuffer
   *              buffer contains the ISD-P associated to the profile to be deleted
   * @param sOffset
   *              offset within ISDPBuffer
   * @param sLength
   *              length of ISD-P
   */
  public abstract void deleteProfile(byte[] ISDPAID, short sOffset, short sLength);
}