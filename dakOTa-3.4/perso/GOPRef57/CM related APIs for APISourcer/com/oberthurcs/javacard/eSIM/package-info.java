/**
 * <p>Package providing an interface to the Card Manager for the purpose of eUICC profile loading and deletion.
 * </p>
 * <p>The package is made up of two classes <tt>com.oberthurcs.javacard.eSIM.ESIMManagerInterface</tt> and <tt>com.oberthurcs.javacard.eSIM.ISDRDispatchManager</tt>.
 * </p>
 */

@APISourcer_Package_Description(
    AID="A0000000770100000010000000000067",
    version="1.3",
    jcVersion="2.2",
    expmap=false,
    expmapFile="com/oberthurcs/javacard/eSIM/javacard/eSIM_map.exp",
    intSupport=false,
    stringSupport=false)

package com.oberthurcs.javacard.eSIM;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;