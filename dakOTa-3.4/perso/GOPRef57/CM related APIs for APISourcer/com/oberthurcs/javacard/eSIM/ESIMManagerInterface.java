package com.oberthurcs.javacard.eSIM;

import javacard.framework.*;
import com.oberthur.apisourcer.annotation.*;

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="discarded",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false)
/// @endcond

public abstract interface ESIMManagerInterface
  extends Shareable
{
	@APISourcer_InterfaceEntryPoint
	public abstract boolean forwardToESIMRootManager(byte[] paramArrayOfByte, byte paramByte);
  
	@APISourcer_InterfaceEntryPoint
	public abstract boolean forwardToProcessData(byte[] paramArrayOfByte, short arrayLen, AID tgtAid);
  
/// @cond (GSMA_EUICC_SUPPORTED && !GSMA_EUICC_GP_ARCHI)
  	@APISourcer_InterfaceEntryPoint
	public abstract boolean forwardDeleteProfile(AID tgtAid);
/// @endcond  
}