package com.oberthurcs.javacard.eSIM;

import com.oberthur.apisourcer.annotation.APISourcer_Class_Description;

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="equals",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false)
/// @endcond
public class ISDRDispatchManager {
	
	public static void reservedPersoInit()
	{
	}
	
	public static void reservedPersoEnd()
	{
	}
	
	public static void reservedPersoData(byte[] bufferIn, short offsetIn, short length)
	{
	}
	
	public static void paramDispatch(short dgi, byte[] bufferIn, short offsetIn, short length)
	{
	}
}
