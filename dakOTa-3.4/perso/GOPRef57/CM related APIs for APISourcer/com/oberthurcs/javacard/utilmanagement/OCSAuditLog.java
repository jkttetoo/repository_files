//----------------------------------------------
//	Project:	PLATFORM
//	Class:		OCSAuditLog
//	Purpose:	Implement System interface.
//
//	(c) Oberthur Card System 01-2003
//----------------------------------------------

package com.oberthurcs.javacard.utilmanagement;

import javacard.framework.AID;
import com.oberthur.apisourcer.annotation.*;

/**
 * OCSAuditLog class to manage card log information
 *
 * @deprecated
 */


/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="equals",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false
	)
/// @endcond

/// @cond (DEPRECATED)
public class OCSAuditLog
{


	/**
	 * constructor
	 */
	public OCSAuditLog()
	{
	}
	
    /**
     * Fulfill array in parameter with audit log information at offset parameter
     * 
     * @param bArray 	output byte array buffer
     * @param offset	offset into bArray buffer to start output data
     * @return reading length
     */
    public static short readAuditLog(byte bArray[], short offset)
    {
        return 0;
    }

    /**
     * delete audit log information in card
     */
    public static void eraseLogFile()
    {
    }

    /**
     * save in audit log file information that authentication was fail
     * @param aid1 SD aid where authentification fail
     */
    public static void recordAuthenticationFailure(AID aid1)
    {
    }

    /**
     * remove in audit log file information that authentication was fail
     * @param aid1 SD aid where information is removed
     */
    public static void invalidateAuthenticationFailure(AID aid1)
    {
    }
	
    private static final byte major_version = 1;
    private static final byte minor_version = 0;
}
/// @endcond