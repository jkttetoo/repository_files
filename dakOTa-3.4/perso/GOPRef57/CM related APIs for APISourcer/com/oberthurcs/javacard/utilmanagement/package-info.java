@APISourcer_Package_Description(
    AID="A0000000770100000010000000000001",
    version="2.48",
    jcVersion="2.2",
    expmap=true,
    expmapFile="com/oberthurcs/javacard/utilmanagement/javacard/utilmanagement_map.exp",
    intSupport=false,
    stringSupport=false,
    expToImport="")

package com.oberthurcs.javacard.utilmanagement;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;