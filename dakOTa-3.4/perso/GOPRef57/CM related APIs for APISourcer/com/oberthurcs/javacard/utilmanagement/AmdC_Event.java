package com.oberthurcs.javacard.utilmanagement;

import javacard.framework.AID;

import com.oberthur.apisourcer.annotation.*;
/**
 * class Event
 *
 */
/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
		first_virtual_method="equals",
		exception_number_to_raise="0",
		number_of_references="3",
		number_field_instance="5",
		implemented_interface="",
		OnCardDebugger_implementation=false
		)
/// @endcond

/// @cond (CRS_SUPPORTED)
public final class AmdC_Event
{
	/** Constant for notification restrictions for ALL */
	public final static byte ALL = 0x00;
	/** Constant for notification restrictions on only one CREL */
	public final static byte ONLY_ONE_CREL = 0x01;
	/** Constant for notification restrictions to exclude Application */
	public final static byte EXCLUDE_APP = 0x02;
	
	/** GPCLEntry of the event */
	private Object m_gpcle_modified;
	
	/** Caller aid */
	private AID m_aid_caller;

	/** Reference to the next event in case of a chained list. */
	private AmdC_Event m_event_next;
	
	/** Event number */
	private short m_s_event;

	/** notify only the application itself if this flag is on */
	private byte m_b_flagAppCL;


	/**
	 * Append an Event at the end of the chained list
	 * @param p_aid_caller 		contains the data to be stored in the new element,
	 * @param p_gpcle_modified 	at which the data to be copied are,
	 * @param p_s_event 		of data to be copied,
	 * @param p_b_notifyFlag 	is notification is targeted only on the application itself,
	 * @param p_z_isFirst 		is first event in the list.
	 * @see #AmdC_Event_addEvent(void) for native implementation
	 */
	public void addEvent(AID p_aid_caller, Object p_gpcle_modified, short p_s_event, byte p_b_notifyFlag, boolean p_z_isFirst)
	{
	}

	/**
	 * Clear chained list and first element
	 * @see #AmdC_Event_clearEventList(void) for native implementation
	 */
	public void clearEventList()
	{
	}
	
	/**
	 * Check if the first Event node is defined
	 * @return true if the first event is defined, false otherwise
	 * @see #AmdC_Event_isEmpty(void) for native implementation
	 */
	public boolean isEmpty()
	{
		return (m_gpcle_modified == null);
	}

	/**
	 * @return caller aid object
	 * @see #AmdC_Event_getAIDCaller(void) for native implementation
	 */
	public AID getAIDCaller()
	{
		return m_aid_caller;
	}

	/**
	 * @return modified GPCLEntry of the event 
	 * @see #AmdC_Event_getModifiedEntry(void) for native implementation
	 */
	public Object getModifiedEntry()
	{
		return m_gpcle_modified;
	}

	/**
	 * @return Next Event object
	 * @see #AmdC_Event_getNextElement(void) for native implementation
	 */
	public AmdC_Event getNextElement()
	{
		return m_event_next;
	}

	/**
	 * @return current Event number 
	 * @see #AmdC_Event_getEvent(void) for native implementation
	 */
	public short getEvent()
	{
		return m_s_event;
	}

	/**
	 * @return application Flag
	 * @see #AmdC_Event_getFlag(void) for native implementation
	 */
	public byte getFlag()
	{
		return m_b_flagAppCL;
	}
}
/// @endcond
