package com.oberthurcs.javacard.utilmanagement;

import javacard.framework.*;
import com.oberthur.apisourcer.annotation.*;

/**
 * OCSELFUpgrade class to manage ELF Upgrade as per AMD H
 */


/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="equals",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false
	)
/// @endcond


public class ELFUpgradeUtil
{
	/**
	 * Vonstructor not available
	 */
	private ELFUpgradeUtil()
	{
	}
	
    /**
     * Get the current Upgrade Session State
     * 
     * @return the current Upgrade Session State
     */
    public static byte getELFUpgradeSessionState()
    {
        return 0;
    }
    
    /**
     * Set the current Upgrade Session State
     * 
     * @param state new Upgrade Session State
     */
    public static void setELFUpgradeSessionState(byte state)
    {
    }
    
    /**
     * Get the current Upgrade Session Information
     * 
     * @param bArray 	output byte array buffer
     * @param offset	offset into bArray buffer to start output data
     * @return reading length
     */
    public static byte getELFUpgradeSessionInformation(byte bArray[], short offset)
    {
        return 0;
    }
    
    /**
     * Start the ELF Upgrade session
     * The parameters provided within the ADPU need to be in the APDU buffer
     * 
     * @param offset	offset into bArray buffer 
     * @param length 	length of the provided ELF Informations 
     */
    public static void startELFUpgradeSession(short offset, short length)
    {
    }
    
    /**
     * Resume the ELF Upgrade session
     */
    public static void resumeELFUpgradeSession()
    {
    }
}
/// @endcond