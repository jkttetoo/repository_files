//----------------------------------------------
//Project:	GOP Ref V01.09 
//Package:	com.oberthur.javacard.domain
//Target:		VM 2.2.2 / API 2.2.2
//Class:		AmdB_AdministrationSession
//Author:		J.YANG
//(c) Oberthur Technologies 2011
//
//----------------------------------------------
package com.oberthurcs.javacard.utilmanagement;

/// @cond (COP_GP2_2_AMD_B)
import com.oberthur.apisourcer.annotation.*;

/**
 * This class defines Administration Session Parameter object which is demanded in Amendment B V1.1
 * It is an object of each SecurityDomain which supports SCP81.
 * With this object the targeted SecurityDomain allows to create a secure connection with remote server.
 * All the manipulation on this object shall be implemented in native C.
 */
/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
		first_virtual_method="equals",
		exception_number_to_raise="0",
		number_of_references="0",
		number_field_instance="0",
		implemented_interface="",
		OnCardDebugger_implementation=false
		)
/// @endcond
public final class AmdB_AdministrationSession
{
	
	// -------------------------------------------------------------------------
	// Defines 
	// -------------------------------------------------------------------------
		
	/**
	 *<pre>
	 *	     Administration session parameter object format 
	 *<p>
	 *	 	  -------------------------------------------------
	 *	  	 | 2 | 1 | 2 | 2 | 2 | 2 | 2 | 2 | 2 | Unit: byte  | 
	 *	  	  -------------------------------------------------
	 *	       |   |   |   |   |   |   |   |   |
	 *	 	   |   |   |   |   |   |   |   |   |
	 *	 	   |   |   |   |   |   |   |   |   |_ Retry Policy parameters
	 *	 	   |   |   |   |   |   |   |   |_____ Security parameters	
	 *	 	   |   |   |   |   |   |   |_________ Connection parameters
	 *	 	   |   |   |   |   |   |_____________ Admin URI parameters	
	 *	 	   |   |   |   |   |_________________ Agent ID parameters
	 *	 	   |   |   |   |_____________________ Admin Host parameters	
	 *	 	   |   |   |_________________________ HTTP POST parameter length
	 *	 	   |   |_____________________________ HTTP POST parameter tag	
	 *	 	   |_________________________________ Admin session parameters total length	 	 
	 *<p>
	 *</pre>
	 */
	
	/**
	 * internal constant for Connection parameters tag
	 */
	public static final byte TAG_AMD_B_CONNECTION = (byte)0x84;
	/**
	 * internal constant for Security parameters tag
	 */
	public static final byte TAG_AMD_B_SECURITY = (byte)0x85;
	/**
	 * internal constant for Retry policy parameters tag
	 */
	public static final byte TAG_AMD_B_RETRY_POLICY = (byte)0x86;
	/**
	 * internal constant for HTTP POST parameter tag
	 */
	public static final byte TAG_AMD_B_HTTP_POST = (byte)0x89;
	/**
	 * internal constant for Administration host parameters tag
	 */
	public static final byte TAG_AMD_B_ADMIN_HOST = (byte)0x8A;
	/**
	 * internal constant for Agent ID parameters tag
	 */
	public static final byte TAG_AMD_B_AGENT_ID = (byte)0x8B;
	/**
	 * internal constant for Administration URI parameters tag
	 */
	public static final byte TAG_AMD_B_ADMIN_URI = (byte)0x8C;
	/**
	 * internal constant indicating the presence of all administration session parameters 
	 */
	public static final byte ALL_ADMINSESSION_TAGS = (byte)0xAA;
	
	/** flag start indicator */
	public static final byte FLAG_START = (byte)0x00;

	/** flag next indicator */
	public static final byte FLAG_NEXT = (byte)0x01;

	/** flag end indicator */
	public static final byte FLAG_END = (byte)0x03;

	/** flag only one APDU indicator */
	public static final byte FLAG_ONE_APDU = (byte)0x02;

	
	/** flag mask end indicator */
	public static final byte MASK_FLAG_END = (byte)0x02;

	/** flag mask start/next indicator */
	public static final byte MASK_FLAG_START_NEXT = (byte)0x01;
	
	
	// -------------------------------------------------------------------------
	// Methods
	// -------------------------------------------------------------------------
	/**
	 * Constructor method
	 */
	protected AmdB_AdministrationSession() 
	{
	}
	
	/**
     * Creates a <code>AmdB_AdministrationSession</code> object instance for the current SD.
     * @param type of object: TRUE for default object; FALSE for normal object
     * @return the <code>AmdB_AdministrationSession</code> object instance for the requested SD
     * @see #AmdB_AdministrationSession_getInstance(void) for native implementation
     */
	public static final AmdB_AdministrationSession getInstance(byte type) 
    {
    	return null;
    }

	/**
	 * This method sets the administration session parameters when SD was installed
	 * @param buffer input buffer
	 * @param offset offset in <B>buffer</B> where the input data will be stored
	 * @param length length of the input buffer
	 * @throws java.lang.NullPointerException
	 * @throws java.lang.ArrayIndexOutOfBoundsException  
	 * @throws javacard.framework.ISOException  0x6A84 NOT_ENOUGH_MEMORY_SPACE
	 * @see #AmdB_AdministrationSession_setParameters(void) for native implementation 
	 * @deprecated if project not define NO_AMD_B_1_1_3
	 */
	public void setParameters(byte[] buffer, short offset, short length) throws java.lang.NullPointerException, 
																									java.lang.ArrayIndexOutOfBoundsException
	{
	}
	
	/**
	  * This method updates the administration session parameters used by STORE DATA command
 	  * @param buffer input buffer
	  * @param offset offset in <B>buffer</B> where the input data will be stored
	  * @param length length of the input buffer
	  * @throws java.lang.NullPointerException
	  * @throws java.lang.ArrayIndexOutOfBoundsException
	  * @see #AmdB_AdministrationSession_updateParameters(void) for native implementation
	  * @deprecated if project not define NO_AMD_B_1_1_3
	  */
	public void updateParameters(byte[] buffer, short offset, short length) throws java.lang.NullPointerException, 
																										java.lang.ArrayIndexOutOfBoundsException
	{
	}

	/**
	 * This method sets the administration session parameters when SD was installed
	 * @param buffer input buffer
	 * @param offset offset in <B>buffer</B> where the input data will be stored
	 * @param length length of the input buffer
	 * @param totalTLVLen send total TLV length to process  
	 * @param flag indicating if start or next update
	 * 
	 * @throws java.lang.NullPointerException
	 * @throws java.lang.ArrayIndexOutOfBoundsException  
	 * @throws javacard.framework.ISOException  0x6A84 NOT_ENOUGH_MEMORY_SPACE
	 * 
	 * @since v2.40 
	 */
	public void setParametersChain(byte[] buffer, short offset, short length, short totalTLVLen, byte flag) throws java.lang.NullPointerException, 
																									java.lang.ArrayIndexOutOfBoundsException
	{
	}
	
	/**
	  * This method updates the administration session parameters used by STORE DATA command
 	  * @param buffer input buffer
	  * @param offset offset in <B>buffer</B> where the input data will be stored
	  * @param length length of the input buffer
	  * @param flag indicating if start(0) or next(1) or last(3) update
	  * @param totalTLVLen send total TLV length to process  
	  * 
	  * @throws java.lang.NullPointerException
	  * @throws java.lang.ArrayIndexOutOfBoundsException
	  * 
	  * @see #AmdB_AdministrationSession_updateParameters(void) for native implementation
	  * 
	  * @since v2.40
	  */
	public void updateParametersChain(byte[] buffer, short offset, short length, short totalTLVLen, byte flag) throws java.lang.NullPointerException, 
																										java.lang.ArrayIndexOutOfBoundsException
	{
	}

	/**
	 * Initialize returned search context and return total length of TLV in BER format
	 * @param bTag tag to search
	 * @return length
	 */
	public short getTagLength(byte bTag)
	{
		return (short)0;
	}	
	
}

/****************************************************************************
 * History  :
 *
 *  $Log::  $
 * 
 *    Rev 1.2   Feb 11 2016 10:25:12   BOISDE
 * Support Amdt B v1.1.3
 * 
 *    Rev 1.1   Jan 02 2015 11:13:40   guerin
 * Update doxygen comments
 * 
 *    Rev 1.0   Feb 10 2012 18:17:52   guerin
 * Initial revision.
 * 
**************************************************************************/
/// @endcond
