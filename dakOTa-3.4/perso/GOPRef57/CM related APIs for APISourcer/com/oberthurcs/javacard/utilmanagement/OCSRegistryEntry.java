//----------------------------------------------
//  Project:    PLATFORM
//  Class:      OCSRegistryEntry
//  Purpose:    Add AID object to GPRegistryEntry
//
//  (c) Oberthur Technologies
//----------------------------------------------
package com.oberthurcs.javacard.utilmanagement;

import javacard.framework.AID;

import com.oberthur.apisourcer.annotation.*;

 
/// @addtogroup M_API_UtilManagement 
/// @{
    
/**
 * internal OCS RegistryEntry class 
 */
 
/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
    first_virtual_method="equals",
    exception_number_to_raise="0",
    number_of_references="1",
    number_field_instance="1",
    implemented_interface="",
    OnCardDebugger_implementation=false
    )
/// @endcond
public class OCSRegistryEntry { 
        
    /** ALL FIELD MUST BE PRIVATE - INACCESSIBLE FROM JAVA */
        
    /** AID of my "Applications" */
    private AID m_aid_myAid;
    
    /**
     * This method returns the Application's AID registered in the current GlobalPlatform Registry's entry.
     * @return the <code>AID</code> object.
     * @see #OCSRegistryEntry_getAID(void) for native implementation
     */
    public AID getAID()
    {
        return m_aid_myAid;
    }

    /**
     * Setter for my AID.
     * <p>Note:<ul>
     * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
     * </ul>
     * @param p_aid my <code>AID</code>.
     * @see #OCSRegistryEntry_setAID(void) for native implementation
     */
    public void setAID(AID p_aid)
    {
    }
}

/// @} // end of group M_API_UtilManagement