//----------------------------------------------
//Project:	GOP Global Platform 2.2 
//Package:	com.oberthur.javacard.domain
//Target:		VM 2.2.2 / API 2.2.2
//Class:		CardManager
//(c) 2008	Oberthur Technologies
//----------------------------------------------


package com.oberthurcs.javacard.utilmanagement;

import org.globalplatform.GPRegistryEntry;
import javacard.framework.AID;
import javacard.framework.Shareable;
import javacard.framework.TransactionException;

import com.oberthur.apisourcer.annotation.*;

/**
 * OCSManagement is a native package containing all native API dedicated to the card manager implementation
 *
 * This module provides the Card Domain with the VM registry access. 
 * It allows performing the Card content management. 
 *
 * This module is directly linked to SFRs from 
 * the card content management. It is a SFR-enforcing module.
 */
/// @addtogroup M_API_UtilManagement CM API UtilManagement

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="equals",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false)
/// @endcond
public class OCSManagement
{

	/** internal constant used to identify the major version of the package */
	private static final byte major_version = 2;

	/** internal constant used to identify the minor version of the package */
	private static final byte minor_version = 48;

	/**
	 * constant used to indicate behavior of the platform when a RESET occurs whatever the kind of reset
	 * Warm reset or Cold
	 * @see OCSManagement#setDefaultApplet(AID aid, byte mode)
	 */
	public static final byte MODE_BOTH_RESET = 0;
	/**
	 * constant used to indicate behavior of the platform when a "COLD RESET occurs 
	 * @see OCSManagement#setDefaultApplet(AID aid, byte mode) 
	 */
	public static final byte MODE_COLD_RESET = 1;
	/**
	 * constant used to indicate behavior of the platform when a "WARM RESET occurs 
	 * @see OCSManagement#setDefaultApplet(AID aid, byte mode)
	 */
	public static final byte MODE_WARM_RESET = 2;


	/** The package in a CAP file has been loaded onto the card. PACKAGE_LOADED (0x01). */
	public static final byte PACKAGE_LOADED	 = (byte)0x01;

	/**
	 * The current applet context is in the Life Cycle State of APPLET_INSTALLED (0x03).
	 * <p>Note:<ul>
	 * <li><em>The Life Cycle State value APPLET_INSTALLED indicates that the applet has been made installed but yet available for selection proccess.</em>
	 * </ul>
	 */
	public static final byte APPLET_INSTALLED = (byte)0x03;

	/**
	 * The current applet context is in the Life Cycle State of APPLET_SELECTABLE (0x07).
	 * <p>Note:<ul>
	 * <li><em>The Life Cycle State value APPLET_SELECTABLE indicates that the applet has been made selectable.</em>
	 * </ul>
	 * */
	public static final byte APPLET_SELECTABLE = (byte)0x07;

	/**
	 * The current applet context is in the Life Cycle State of APPLET_LOCKED bit8 set to 1 i.e (0x80).
	 * <p>Note:<ul>
	 * <li><em>The Life Cycle State APPLET_LOCKED (bit8 set to 1) indicates that the applet isn't avalaible for selection process.</em>
	 * </ul>
	 */
	public static final byte APPLET_LOCKED = (byte)0x80;

	/** Internal constant means that the deletion is done in cumulative mode */
	public static final byte COMMIT_DELETION_CUMULATIVE = (byte)0x5A;

	/** Internal constant means that the deletion is done in cumulative mode */
	public static final byte COMMIT_DELETION  = (byte)0xA5;

	/// @cond (CRYPTO_RIGHTS)
	/**@deprecated Internal constant means that the crypto RSA API are available */
	private static final byte RSA_CRYPTO_RIGHTS = (byte)0x20;

	/**@deprecated Internal constant means that the crypto crypto API using a 3DES Keys are available */
	private static final byte TRIPLE_DES_CRYPTO_RIGHTS = (byte)0x40;

	/**@deprecated Internal constant means that the crypto crypto API using a 3DES Keys are available */
	private static final byte DES_CRYPTO_RIGHTS = (byte)0x80;
	/// @endcond
/// @cond (AMD_C_1_2)	
	/**
	 * Constant used to indicate the behavior of the setProtocolDataRF method, in this case, it checks the parameters' consistency.
	 * This value shall be used during the check of INSTALL[for Install] command parameters because the AID given in parameters doesn't exist in the registry.
	 */
	public static final byte MODE_CHECK_RF = (byte)0x0A;
	/**
	 * Constant used to indicate the behavior of the setProtocolDataRF method, in this case, it sets the parameters.
	 * This value shall be used during an INSTALL[for Install] command after the register of the application.
	 */
	public static final byte MODE_SET_RF = (byte)0x50;
	/**
	 * Constant used to indicate the behavior of the setProtocolDataRF method, in this case, it checks and sets the parameters.
	 * This value could be used during an INSTALL[for Registry Update] command or with the API.
	 */
	public static final byte MODE_BOTH_RF = (byte)0x5A;
/// @endcond
	/**
	 * Constants used to indicate that BFC compliance has to run on non BFC platform.
	 */
	public static final byte LOCK_BFC_BEHAVIOR = (byte)0x01;

/// @cond (AMD_C_1_2)	
	/**
	 * Constant used to indicate the behavior of the setProtocolDataRFActivationDeactivation method, in this case, it checks the parameters' consistency.
	 */
	public static final byte MODE_CHECK_RF_PROTOCOL_TYPE = (byte)0x0A;
	/**
	 * Constant used to indicate the behavior of the setProtocolDataRFActivationDeactivation method, in this case, it sets the parameters.
	 */
	public static final byte MODE_SET_RF_PROTOCOL_TYPE = (byte)0x50;
/// @endcond
	/**
	 * Deletes the specified instance of applet or package and recovers storage space as free memory via the Garbage Collector.
	 * <p>
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>	If object is an AID associated with an applet, this applet is removed from registry and Garbage Collection is initiated. If object is an AID associated with a package, this package is removed if possible (i.e. if there are no remaining applets instantiated on this package and if no other package references this package. </em>
	 * <li><em>	This may generate 0x6A80 (BadLinkException).</em>
	 * </ul>
	 * <p>
	 * @see #OCSManagement_commitDeletion(void) for native implementation
	 * @ingroup M_API_UtilManagement 
	 */
	public static void commitDeletion()
	{
	}
		
	/**
	 * Deletes the specified instance of applet or package and recovers storage space as free memory via the Garbage Collector.
	 * <p>
	 * @param obj <code>Object</code> to delete.
	 * @param andAssociatedApplet <code>boolean</code> only relevant for the Package if true delete the package and all the Applet otherwise delete only the package
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>	If object is an AID associated with an applet, this applet is removed from registry and Garbage Collection is initiated. If object is an AID associated with a package, this package is removed if possible (i.e. if there are no remaining applets instantiated on this package and if no other package references this package. </em>
	 * <li><em>	This may generate 0x6A80 (BadLinkException).</em>
	 * </ul>
	 * <p>
	 * @see #OCSManagement_delete(void) for native implementation 
	 * @ingroup M_API_UtilManagement
	 */
	public static void delete(Object obj, boolean andAssociatedApplet)
	{
	}

	/** 
	 * This method allows the card manager to initiate the deletion whatever the VM version
	 * @see #OCSManagement_initDeletion(void) for native implementation 
	 * @ingroup M_API_UtilManagement
	 */
	public static void initDeletion()
	{;}
/// @cond (DEPRECATED)
	/**
	 * This method installs an instance of the Applet from the package given in parameter 
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>The APDU buffer shall hence contain at indicated offset the following template: LV package AID, the LV applet AID, LV desired Instance AID, LV Application privilege and Application Parameter.</em> 
	 * <li><em>The method of the application is APDU buffer shall hence contain at indicated offset the following template: LV package AID, the LV applet AID, LV desired Instance AID, LV Application privilege and Application Parameter.</em> 
	 * <li><em>This method invoke the <code> javacard.framework.Applet install(byte[] bArray, short bOffset, byte bLength)</code> of the Applet where :</em> 
	 * <li><em>The array, bArray, shall contain the following consecutive LV coded data:</em> 
	 * <li><em>Length of the instance AID,</em> 
	 * <li><em>The instance AID,</em> 
	 * <li><em>Length of the Application Privileges,</em> 
	 * <li><em>The Application Privileges,</em> 
	 * <li><em>Length of the Application Specific Parameters and,</em> 
	 * <li><em>The Application Specific Parameters.</em> 
	 * <li><em>The byte, bOffset, shall contain an offset within the array pointing to the length of the instance AID.</em> 
	 * <li><em>The byte, bLength, shall contain a length indicating the total length of the above-defined data in the array.</em> 
	 * <li><em>The applet is required to utilize the instance AID as a parameter when invoking the register (byte []</em> 
	 * <li><em>bArray, short bOffset, byte bLength) method of the Class Applet of the Java Card 2.1.1</em> 
	 * <li><em>Application Programming Interface specification.</em> 
	 * <li><em>Installing is an independent process from loading (i.e. a load can be ongoing when an install occurs).This may generate 0x6640 (StackOverflowException).</em>
	 * </ul>
	 * <p>
	 * @param offset <code>byte</code> offset indicates the length of the package AID,
	 * @param gpEntry <code>GPRegistryEntry</code> gpEntry is the application GPRegistryEntry,
	 * @param associated <code>AID</code> associated is the associated Security Domain.
	 * @return <code>AID</code> of the installed application.
	 * @see #APPLET_INSTALLED
	 * @see #APPLET_SELECTABLE
	 * @deprecated
	 */
	public static AID install(byte offset, GPRegistryEntry gpEntry, AID associated)
	{
		return null;
	}

	/**
	 * Same as method public static AID install(byte offset, GPRegistryEntry gpEntry, AID associated) with one additional parameter for Memory Resource Management.
	 * 
	 * @param offset <code>byte</code> offset indicates the length of the package AID,
	 * @param gpEntry <code>GPRegistryEntry</code> gpEntry is the application GPRegistryEntry,
	 * @param associated <code>AID</code> associated is the associated Security Domain,
	 * @param baMrmParams <code>byte[]</code> baMrmParams Memory Resource Management install parameters.
	 * @return <code>AID</code> of the installed application.
	 * @see #install(byte offset,GPRegistryEntry gpEntry, AID associated)
	 * @deprecated
	 */
	public static AID install (byte offset, org.globalplatform.GPRegistryEntry gpEntry, AID associated, byte[] baMrmParams)
	{
		return null;
	}

	/**
	 * Same as method public static AID install(byte offset, GPRegistryEntry gpEntry, AID associated) with two additional parameters for GP Amendment C.
	 * 
	 * @param offset <code>byte</code> offset indicates the length of the package AID,
	 * @param gpEntry <code>GPRegistryEntry</code> gpEntry is the application GPRegistryEntry,
	 * @param associated <code>AID</code> associated is the associated Security Domain,
	 * @param gpclEntryAvailability <code>byte</code> gpclEntryAvailability the Communication Interface Access Parameter of the applet,
	 * @param gpclEntryClState <code>byte</code> gpclEntryClState requested Availability state on Contactless interface.
	 * @return <code>AID</code> of the installed application.
	 * @see #install(byte offset, GPRegistryEntry gpEntry, AID associated)
	 * @deprecated
	 */
	public static AID install (byte offset, org.globalplatform.GPRegistryEntry gpEntry, AID associated, byte gpclEntryAvailability, byte gpclEntryClState)
	{
		return null;
	}

	/**
	 * Same as method public static AID install(byte offset, GPRegistryEntry gpEntry, AID associated, byte gpclEntryAvailability, byte gpclEntryClState) with one additional parameter for Memory Resource Management.
	 * 
	 * @param offset <code>byte</code> offset indicates the length of the package AID,
	 * @param gpEntry <code>GPRegistryEntry</code> gpEntry is the application GPRegistryEntry,
	 * @param associated <code>AID</code> associated is the associated Security Domain,
	 * @param gpclEntryAvailability <code>byte</code> gpclEntryAvailability the Communication Interface Access Parameter of the applet,
	 * @param gpclEntryClState <code>byte</code> gpclEntryClState requested Availability state on Contactless interface.
	 * @param baMrmParams <code>byte[]</code> baMrmParams Memory Resource Management install parameters.
	 * @return <code>AID</code> of the installed application.
	 * @see #install(byte offset, GPRegistryEntry gpEntry, AID associated)
	 * @deprecated
	 */
	public static AID install (byte offset, org.globalplatform.GPRegistryEntry gpEntry, AID associated, byte gpclEntryAvailability, byte gpclEntryClState, byte[] baMrmParams)
	{
		return null;
	}
/// @endcond
	/**
	 * This method installs an instance of the Applet from the package given in paramter 
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>The APDU buffer shall hence contain at indicated offset the following template: LV package AID, the LV applet AID, LV desired Instance AID, LV Application privilege and Application Parameter.</em> 
	 * <li><em>The method of the application is APDU buffer shall hence contain at indicated offset the following template: LV package AID, the LV applet AID, LV desired Instance AID, LV Application privilege and Application Parameter.</em> 
	 * <li><em>This method invoke the <code> javacard.framework.Applet install(byte[] bArray, short bOffset, byte bLength)</code> of the Applet where :</em> 
	 * <li><em>The array, bArray, shall contain the following consecutive LV coded data:</em> 
	 * <li><em>Length of the instance AID,</em> 
	 * <li><em>The instance AID,</em> 
	 * <li><em>Length of the Application Privileges,</em> 
	 * <li><em>The Application Privileges,</em> 
	 * <li><em>Length of the Application Specific Parameters and,</em> 
	 * <li><em>The Application Specific Parameters.</em> 
	 * <li><em>The byte, bOffset, shall contain an offset within the array pointing to the length of the instance AID.</em> 
	 * <li><em>The byte, bLength, shall contain a length indicating the total length of the above-defined data in the array.</em> 
	 * <li><em>The applet is required to utilize the instance AID as a parameter when invoking the register (byte []</em> 
	 * <li><em>bArray, short bOffset, byte bLength) method of the Class Applet of the Java Card 2.1.1</em> 
	 * <li><em>Application Programming Interface specification.</em> 
	 * <li><em>Installing is an independent process from loading (i.e. a load can be ongoing when an install occurs).This may generate 0x6640 (StackOverflowException).</em>
	 * </ul>
	 * <p>
	 * @param offset <code>byte</code> offset indicates the length of the package AID,
	 * @param installParams <code>InstallParameters</code> structure of all install parameters
	 * @return <code>AID</code> of the installed application.
	 * @ingroup M_API_UtilManagement
	 * @see #OCSManagement_install_Evolutive(void) for native implementation 
	 */
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("Evolutive")
	/// @endcond	
	public static AID install(byte offset, com.oberthurcs.javacard.domain.InstallParameters installParams) 
	{
		return null;
	}

	/**
	 * This method enables to retrieve the applicative reference within Entry associated with the Applet identified by <code>AID</code> .
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>Usually this method is used to store the handle on the Secure Channel services of the application </em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> AID of the application .
	 * @return <code>Object</code> Applicative reference .
	 * @see #setGPEntryRef(AID aid, Object nativeAPP)
	 * @see #OCSManagement_getAIDRef(void) for native implementation
	 * @ingroup M_API_UtilManagement 
	 */
	public static Object getGPEntryRef(AID aid)
	{
		return null;
	}

	/**
	 * This method enables to retrieve the state of of the applet associated with the <code>AID</code> given in parameter.
	 * The state is represented on 1 byte.
	 * <p>
	 * Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>The System used the bit8,bit3,bit2 and bit1. bit7,bit6,bit5 and bit4 have not consistency for the system and can be used by the application</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> AID of the application .
	 * @return <code>byte</code> Applicative byte ( usually privilege).
	 * @see #APPLET_INSTALLED 
	 * @see #APPLET_SELECTABLE
	 * @see #APPLET_LOCKED
	 * @see #OCSManagement_getStatus(void) for native implementation
	 * @ingroup M_API_UtilManagement 
	 */
	public static byte getStatus(AID aid)
	{
		return 0;
	}

	/**
	 * This method Continues loading process.
	 * <p>
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>If a previous loading was not ongoing, or an error occurs during loading, this may generate a non catchable exception which will desactivate Card Manager. 0x6985 (SequenceErrorException), 0x6693 (FileFormatException). </em>
	 * </ul>
	 * <p>
	 * @param offset in APDU buffer where to find next data of installed cap file.
	 * @param length number of bytes of installed cap file which can be found in APDU buffer.
	 * @see #OCSManagement_loadNext(void) for native implementation
	 * @ingroup M_API_UtilManagement 
	 */
	public static void loadNext(byte offset, byte length)
	{
	}

	/**
	 * This method allows to set the application related to the passed AID as the default applet.
	 * <p>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aid AID to be settled as default applet.
	 * @param mode the mode can be any of the three value MODE_BOTH_RESET, MODE_COLD_RESET, MODE_WARM_RESET
	 * @see #MODE_BOTH_RESET
	 * @see #MODE_COLD_RESET
	 * @see #MODE_WARM_RESET
	 * @see #OCSManagement_setDefaultApplet(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setDefaultApplet(AID aid, byte mode)
	{
	}

	/**
	 * This method allows to set the application related to the passed AID as the Final Application.
	 * @param aid to be set as Final Application.
	 * @see #OCSManagement_setFinalApplication(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setFinalApplication(AID aid)
	{
	}
/// @cond (DEPRECATED)
	/**
	 * This method enables to retrieve the EEPROM quota information about the applet associated with the given <code>AID</code> in parameter.
	 *  
	 * <p>
	 * Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aid AID of the application .
	 * @return <code>short</code> EEPROM information 
	 * @deprecated
	 */
	public static short getEEPROMQuota(AID aid)
	{
		return (short) 0;
	}
/// @endcond

	/**
	 * This method returns the minimal number of nibble authorized for Global PIN. This method result of the FIPS
	 * constraints 
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * 	
	 * <li>In fact the value stored at ADDRESS_LOCK_CVM is the ~minimal number.This feature allows to returns
	 *        ( FF <=> no constraint ) if the Lock isn't personalized )
	 *        FIPS requires at least 06 nibbles => so the correspond value is 0xF9
	 * </ul>
	 * <p>
	 * @return <code>byte</code> number of minimal number of nibble ( FF <=> no constraint )
	 * @see #OCSManagement_getCVM_MiniNibbleValue(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static byte getCVM_MiniNibbleValue()
	{ return (byte) 6; }

/// @cond (!COP_GP2_2)
	/** 
	 * This method allows the card manager to build the getstatus response in mode Package and Executable Module AID
	 * <p>
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>This method returns all the Executable Module AID contained in the Package in TLV format or LV regarding the paramater P2 (0000 001x or 0000 000x ) </em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> aidPackage Package whom the Executable Module AID must be returned
	 * @param p_offset <code>short</code> offset in APDU buffer pointing on the first byte where the LV Executable(s) Module AID should be copied.
	 * @param tlv <code>boolean</code> tlv indicates if the response is tlv formatted
	 * @return <code>short</code> new value of the offset pointing on the first free byte
	 * 
	 * @deprecated since GOP Ref V01.08 
	 */
	public static short getExecutableModuleAID(AID aid, short p_offset, boolean tlv )
	{ return p_offset;}
/// @endcond
	
	/** 
	 * This method allows the card manager to build the getstatus response in mode Package and Executable Module AID
	 * <p>
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>This method returns all the Executable Module AID contained in the Package in TLV format or LV regarding the paramater P2 (0000 001x or 0000 000x ) </em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> aidPackage Package whom the Executable Module AID must be returned
	 * @param buffer buffer in which data shall be written,
	 * @param p_offset <code>short</code> offset in buffer pointing on the first byte where the LV Executable(s) Module AID should be copied.
	 * @param tlv <code>boolean</code> tlv indicates if the response is tlv formatted
	 * @return <code>short</code> new value of the offset pointing on the first free byte
	 * @see #OCSManagement_getExecutableModuleAID_AidBaSZ(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("AidBaSZ")
	/// @endcond
	public static short getExecutableModuleAID(AID aid, byte[] buffer, short p_offset, boolean tlv )
	{ return p_offset;}

/// @cond !(COP_GP2_2)
	/** 
	 * This method allows the card manager to build a response to the getStatus command when it is used to retrieve information 
	 * related to an applet's  instance. This method fills the apdu buffer with the Package AID and Executable Module AID 
	 *
	 *<p> 	'C6'	1-n		Executable Load File 's  AID	Optional</p>
	 *<p> 	'84'	1-n		AID Executable Module 's  AID	Optional</p>
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aidApplet <code>aidApplet</code> aidInstance
	 * @param p_offset offset within buffer  pointing  on the first byte where the information must be filled 
	 * @param flag flag which indicates the to be written in the buffer 
	 * @return <code>short</code> new value of the offset pointing on the first free byte
	 *  
	 * @deprecated since GOP Ref V01.08 
	 */
	public static short getInstanceInformation(AID aidApplet, short p_offset, byte flag)
	{return p_offset;}
/// @endcond

	/** 
	 * This method allows the card manager to build a response to the getStatus command when it is used to retrieve information 
	 * related to an applet's  instance. This method fills the apdu buffer with the Package AID and Executable Module AID 
	 * <pre>
	 *	'C6'    1-n     Executable Load File 's  AID    Optional
	 *	'CE'    02      Version                         Optional
	 *	'84'    1-n     AID Executable Module 's  AID   Optional
	 * </pre>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>    
	 * @param aidApplet <code>aidApplet</code> aidInstance
	 * @param p_offset 	offset within buffer  pointing  on the first byte where the information must be filled 
	 * @param buffer 	in which data shall be written,
	 * @param flag 		flag which indicates the to be written in the buffer 
	 * @return <code>short</code> new value of the offset pointing on the first free byte
	 * @see #OCSManagement_getInstanceInformation_AidBaSZ(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("AidBaSZ")
	/// @endcond
	public static short getInstanceInformation(AID aidApplet, byte[] buffer, short p_offset, byte flag)
	{ return p_offset;}

/// @cond (!COP_GP2_2)
	/** 
	 * This method allows the card manager to build the getstatus response in mode Package and Executable Module AID and generate the appropriate Status 
	 * when the response is greater than the APDU buffer 
	 * <p>
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>This method returns The length of package AID and Executable Module AID contained in the next package (only the LV value )</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> aidPackage Package currently processed
	 * @return <code>short</code> length of the next LV package's information 
	 * @deprecated since GOP Reg V01.08
	 */
	public static short getLengthOfInfoNextPack(AID aid )
	{ return (short) 0;}
/// @endcond

	/** 
	 * This method returns the next Instance's AID belonging of the package AID and AID Applet given in parameter
	 * <p>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 *
	 * @param  aidPack    Package AID .
	 * @param  aidApplet  Applet AID .
	 * @return <code>AID</code> 
	 * @see #OCSManagement_getNextInstance(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static AID getNextInstance(AID aidPack, AID aidApplet)
	{ return (AID) null;}

	/** 
	 * This method extends the GetData process of the cardmanager and allows it to return the informations related to the special OberthurCS Data
	 * ( as Manufacturing Data, state of the lock (FIPS and so on ) 
	 * This methode assumes that a correct getData command is located into the apdu buffer ( CLA INS P1P2 Lc )
	 * The Tag processed are ( TAG DF50 , DF51, DF52, DF53 see SRS Mask for more details )
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>    
	 * @return short length of the data returned if the Tag is valid ; 0 otherwise )
	 * @see #OCSManagement_getOCSData(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short getOCSData()
	{return (short)0;}

	/** 
	 * This method allows the cardmanager to get the Package Version related with the Package AID provided in parameter
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>    
	 * @param aidPack AID Package currently processed
	 * @return short version value 
	 * @see #OCSManagement_getPckVersion(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short getPckVersion(AID aidPack)
	{return (short)0;}

	/**
	 * This method allows to retrieve the ROM Identifiers usually present in the CPLC string.
	 * <p>
	 * <p>Note:<ul>
	 * <li><em>This may generate 0x6603 (ArrayIndexOutOfBoundsException).</em>
	 * </ul>
	 * <p>
	 * @param  ROM_offset offset within ROM Identifiers to start copy from.
	 * @param  buffer	destination byte array.
	 * @param  destOff	offset within destination byte array to start copy into.
	 * @param  length to be copied.
	 * @see #OCSManagement_getROMID(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void getROMID(byte ROM_offset, byte[] buffer, short destOff, byte length)
	{
	}

	/** 
	 * This method allows the card manager to return the handle of the Applet instance which implements a shareable interface   
	 * when the response is greater than the APDU buffer<br>
	 * The card manager must control itself if applet implement interface org.globalplatform.Application
 	 * <p>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aidApplet  <code>AID</code> of the acurrently processed
	 * @return <code>Object</code> this of the Applet
	 * @see #OCSManagement_getShareableEntry(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static Object getShareableEntry(AID aidApplet)
	{ return (Object)null ;}

	/**
	 * This method returns the size available in EEprom.This method writes directly the remaining EEprom size 
	 * in the apdu buffer and following the format described  below
	 *    CLA INS P1P2 LC 
	 *                 lg  < Size>....>
	 * <p>
	 * <p>Note:<ul>
 	 * <li><em> this format allows to return the value in the GetData Command</em>
	 * </ul>
	 * <p>
	 * @see #OCSManagement_getSize(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void getSize()
	{ ; }

/// @cond (COP_GP2_2_AMD_C_CGM)
	/**
	 * This method returns the size available in EEprom.This method writes directly the remaining EEprom size 
	 * in the buffer in parameter. Size in always formatted on 4 bytes.
 	 * @param dstArray - Destination buffer
	 * @param dstOff - Offset in destination buffer
	 * @see #OCSManagement_getSize_AidBaB(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("AidBaB")
	/// @endcond
	public static void getSize(byte [] dstArray, byte dstOff )
	{ ; }
/// @endcond
	
/// @cond (CMP_TCL)
	/**
	 * This method return the lock TCLmanagement value.
	 * <ul> 
	 * <li>TCL_SCP_LOCK         0x01 if 1 Secure Channel authorized in CL
	 * <li>TCL_SD_SELECT_LOCK   0x02 if 1 Security Domain Selection is authorized in CL
	 * <li>TCL_GETDATA_LOCK     0x04 if 1 Get Data command authorized in CL for all data otherwise not authorized for 0045 and DF50	
	 * <li>TCL_FROSTED          0x10 if 1 Contact less protocol is available
	 * </ul>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * if projet GP_2_2 et GSM then it is  @deprecated 
	 * 
	 * @return <code>byte</code> TCLmanagement value
	 * @see #OCSManagement_getTCLmanagementlock(void) for native implementation
	 */
	public static byte getTCLmanagementlock()
	{ return (byte)0; }
/// @endcond

	/**
	 * This method allows Security Domain to know if an applet has been recently selected on the 
	 * logical channel provided in parameter. The behavior of this method is like TestAndClear  
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * @param channel indicates the logical channel number.   
	 * @return boolean true if a selection has been performed until the previous call, false otherwise. 
	 * @see #OCSManagement_isSelectionPerformed(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static boolean isSelectionPerformed(byte channel)
	{return true;}

/// @cond (DEPRECATED)
	/**
	 * This method enables to retrieve the Transient quota information about the applet associated with the given <code>AID</code> in parameter.
	 *  
	 * <p>
	 * Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> of the application .
	 * @return <code>short</code> Transient information 
	 * @deprecated
	 */
	public static short getTransientQuota(AID aid)
	{
		return (short) 0;
	}
/// @endcond

/// @cond (CMP_TCL)
	/**
	 * This method return true if all bits in the parameter byte is enable false if one of them is disable. is used for TCLmanagement lock.
	 * <ul>
	 * <li>IS_TCL_SCP_LOCK         0x01 if true Secure Channel disabled in CL
	 * <li>IS_TCL_SD_SELECT_LOCK   0x02 if true Security Domain Selection is disabled in CL
	 * <li>IS_TCL_GETDATA_LOCK     0x04 if true Get Data not available in CL for tags 0045 and DF50	
	 * <li>IS_TCL                  0x80 if true the protocol is Contact less
	 * </ul>
	 * it's possible to concatenate the flags
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * 	if projet GP_2_2 et GSM then it is  @deprecated
	 * @param islock <code>byte</code> to check
	 * @return <code>boolean</code> true or false
	 * @see #OCSManagement_isTCLmanagementlock(void) for native implementation
	 */
	public static boolean isTCLmanagementlock(byte islock)
	{ return  false; }
/// @endcond

	/** 
	 * This method allows a Java Optional Code to finish its execution and go back to a specific bytecode in ROM
	 *
	 * <p>Note:<ul>
	 * <li><em>This method can only be invoked from a Java Optional code. If it is invoked from a standard applet,  a SecurityException is thrown.</em>
	 * </ul>
	 * <p>
	 * if projet not define VM_CODOP_ROM_APPLETS then it is  @deprecated 
	 * @param bcOffset <code>short</code> Offset of next bytecode in ROM. 
	 * @exception SecurityException
	 * @see #OCSManagement_jumpToBytecode(void) for native implementation
	 */
	public static void jumpToBytecode(short bcOffset)
	{
	}

	/**
	 * This method block or terminate the card. depending on byte parameter. if parameter equal 0x7F then terminate card otherwise the card is blocked 
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>The Card manager processe the applicative control before to invoke  this method (i.e check the Crad lock privilege ).</em>
	 * </ul>
	 * <p>
	 * @param p_b_state <code>byte</code> state  
	 * @see #OCSManagement_lockCard(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void lockCard(byte p_b_state)
	{
	}

	/** 
	 * This method allows the cardmanager to look up an AID into the registry using its own pointer 
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p> 
	 * @param buffer <code>byte[]</code> of Source buffer containing the AID bytes
	 * @param offset <code>short</code> offset within the source buffer of the first AID byte.
	 * @param length <code>byte</code> Length of the AID.
	 * @return <code>AID</code> AID 
	 * @see #OCSManagement_lookupAID(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static AID lookupAID(byte[] buffer, short offset, byte length)
	{ return null;}

	/** 
	 * This method allows the cardmanager to look up an AID into the registry using its own pointer 
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p> 
	 * @param flags <code>byte</code> indicating flag of search.
	 * @return <code>AID</code> AID 
	 * @see #OCSManagement_lookupAID_b(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("b")
	/// @endcond
	public static AID lookupAID(byte flags)
	{ return null;}


	/** 
	 * This method allows the cardmanager to look up an AID into the registry using its own pointer 
	 * with modification of registry order (cf Amdt C)
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>    
	 * @param buffer <code>byte[]</code>indicating source buffer containing the AID bytes
	 * @param offset <code>short</code> indicating offset within source buffer of the first AID byte.
	 * @param length <code>byte</code> indicating Length of the AID.
	 * @return <code>AID</code> AID 
	 * @see #OCSManagement_lookupAIDCustomOrder(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static AID lookupAIDCustomOrder(byte[] buffer, short offset, byte length)
	{ return null;}


	/** 
	 * This method allows the cardmanager to look up an AID into the registry using its own pointer 
	 * with modification of registry order (cf Amdt C)
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>    
	 *
	 * @param flag <code>byte</code> indicating flag of search.
	 * @return <code>AID</code> AID 
	 * @see #OCSManagement_lookupAIDCustomOrder_b(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("b")
	/// @endcond
	public static AID lookupAIDCustomOrder(byte flag)
	{ return null;}

/// @cond (!COP_GP2_2_AMD_C)
	/** 
	 * This method retrieve the position of the application in the registry order (including the volatile priority)
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>    
	 * if projet not define  COP_GP2_2_AMD_C then it is  @deprecated 
	 * @param aid <code>AID</code> indicating AID of the application.
	 * @return <code>unsigned byte</code> position in the registry order 
	 * @see #OCSManagement_indexInRegistry(void) for native implementation
	 */
	public static byte indexInRegistry(AID aid)
	{ return (byte)0;}
/// @endcond

/// @cond (DEPRECATED)
	/** 
	 * this method makes an Object a JCRE Object. Can only be called by Card Manager.
	 * <p>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * @param object Reference of the object.
	 * @deprecated
	 */
	public static void 	makeJCRE(Object object)
	{
	}
/// @endcond
	/** 
	 * This method allows to modify or correct the process of the cardmanager and security domain
	 *
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>This method should be called at the very first step of the cardmanager's  method process</em>
	 * </ul>
	 * <p>    
	 * @return boolean true means that no additional process is needed false otherwise (i.e the cardmanager's method process shall continu )
	 * @exception javacard.framework.CardRuntimeException
	 *            NegativeArraySizeException
	 * @see #OCSManagement_processCmd(void) for native implementation
	 */
	public static boolean processCmd()
	{return false;}

	/**
	 * This method Ends loading process.
	 * <p>
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>	crypto rights associated with All the applet which will be instanciated from this package.</em>
	 * <li><em>	For the time being, 0xFF means DES and RSA alogorithms are available for this applets </em>
	 * <li><em>	0x20 means RSA  only </em>
	 * <li><em>	0x40 means 3DES only </em>
	 * <li><em>	0x80 means DES  only </em>
	 * </ul>
	 * <p>
	 * @param crypto <code>byte</code> indicating crypto rigth .
	 * @return <code>AID</code> package AID loaded.
	 * @cond (CRYPTO_RIGHTS)
	 * @see #RSA_CRYPTO_RIGHTS
	 * @see #TRIPLE_DES_CRYPTO_RIGHTS
	 * @see #DES_CRYPTO_RIGHTS
	 * @endcond
	 * @see #OCSManagement_loadEnd(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static AID loadEnd(byte crypto)
	{
		return null;
	}

	/**
	 * This method Initializes loading process.
	 *
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>If an error occurs during loading, this may generate a non catchable exception which will deactivate Card Manager , 0x6603(ArrayIndexOutOfBoundsException).</em>
	 * <li><em>If a previous loading was ongoing, it is automatically cancelled. </em>
	 * </ul>
	 * </p>
	 * @param offset in APDU buffer where to find the package AID.
	 * @see #OCSManagement_loadInit(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void loadInit(byte offset)
	{
	}

	/**
	 * This method Abort loading process.
	 * 
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>It has to be called to abort the loading process at the Virtual Machine level. </em>
	 * </ul>
	 * </p>
	 * @see #OCSManagement_loadAbort(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void loadAbort()
	{
	}

	/**
	 * This method resets the partial AID reference from the beginning.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @see #OCSManagement_resetSearch(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void resetSearch()
	{
	}

	/**
	 * This method	allows to change the AID of the Card management Application ( i.e Card Manager or first Applet Instancied on the Card)
	 * <p>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @param aid <code>AID</code> of the Card Manager (or first Applet Instancied) to update
	 * @param srcArray <code>byte[]</code> of source buffer containing the AID bytes
	 * @param srcOffset <code>short</code> indicating offset withing the buffer of the first AID byte.
	 * @param length <code>short</code> indicating length of the AID.
	 * @see #OCSManagement_setAID(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setAID(AID aid, byte srcArray[], short srcOffset, short length)
	{
	}

	/**
	 * This method sets the historical bytes of the Answer To Reset byte string .
	 * 
	 * @param buffer source byte array.
	 * @param offset short indicating offset within source byte array where ATR record begins.
	 * @param length byte indicating length to be read in source buffer.
	 * @param mode byte indicating the mode, it can be any of the three value MODE_BOTH_RESET, MODE_COLD_RESET, MODE_WARM_RESET, returned in historical bytes of the ATR.
	 * @see #OCSManagement_setATRRecord(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setATRRecord(byte buffer[], short offset, byte length, byte mode)
	{
	}

	/** 
	 * This method allows the cardmanager to set the CPLC data 
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>    
	 * @param buffer		a byte array reference where the CPLC data are located 
	 * @param offset 		offset within buffer  pointing  on the first byte of CPLC data 
	 * @param cplc_offset 	offset offset whithin CPLC string to start writing
	 * @param length byte length of CPLC Data 
	 * @exception javacard.framework.CardRuntimeException
	 *            NegativeArraySizeException
	 * @see #OCSManagement_setCPLCData(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setCPLCData(byte[] buffer, short offset, short cplc_offset, short length)
	{;}

/// @cond (DEPRECATED)
	/**
	 * This method allows the CM to store the EEPROM informations about the applet associated with the given <code>AID</code> given in parameter.
	 * The Quota information is coded in short  
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> of the application .
	 * @param Quota <code>short</code> indicating EEPROM Quota information 
	 * @deprecated
	 */
	public static void setEEPROMQuota(AID aid, short Quota)
	{
	}
/// @endcond

	/**
	 * This method sets an applicative reference within the Entry associated with the Applet identified by <code>AID</code> .
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>Usually this method is used to store the handle on the Secure Channel services of the application </em>
	 * </ul>
	 * </p>
	 * @param aid <code>AID</code> of the application .
	 * @param nativeAPP <code>Object</code> of Applicative reference .
	 * @see #getGPEntryRef(AID)
	 * @see #OCSManagement_setGPEntryRef(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setGPEntryRef(AID aid, Object nativeAPP)
	{
	}

	/**
	 * This method allows to set the special data belonging to the mask.(for instance the TCL_Lock_management etc...) The data are located into the APDU buffer. 
	 * theses data are TLV formatted. It is the responsability of the Card Manager to receive the Data and checks the command before to call this method.
	 * The boolean returned, indicates the correct processing of the the data by the mask.
	 * <p>
	 * The data are located into the APDU buffer.   
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @return <code>boolean</code> true if the data have been proccessed by the mask false otherwise
	 * @see #OCSManagement_setOCSData(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static boolean setOCSData()
	{ return  false; }

	/** 
	 * This method allows the cardmanager to set its own pointer used for the registry search 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> to search  
	 * @see #OCSManagement_setSearch(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setSearch(AID aid)
	{;}

	/**
	 * This method sets the state (1 byte) of the applet associated with the given <code>AID</code> in state given in parameter.
	 * The state is represented on 1 byte 
	 * 
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>The System used the bit8,bit3,bit2 and bit1. bit7,bit6,bit5 and bit4 have not consistency for the system and can be used by the application</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> of the application .
	 * @param mode <code>byte</code> indicating application state .
	 * @see #APPLET_INSTALLED
	 * @see #APPLET_SELECTABLE
	 * @see #APPLET_LOCKED
	 * @see #OCSManagement_setStatus(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setStatus(AID aid, byte mode)
	{
	}

/// @cond (DEPRECATED)
	/**
	 * This method allows the CM to store the transient quota informations about the applet associated with the given <code>AID</code> given in parameter.
	 * The Quota information is coded in short  
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> of the application .
	 * @param transientQuota <code>short</code> indicating transient Quota information 
	 * @deprecated
	 */
	public static void setTransientQuota(AID aid, short transientQuota)
	{
	}
/// @endcond

	/**
	 * This method verifies the MAC and decrypt the data of the APDU buffer with the Transport Key.
	 * This method uses the VOP 1.0 scheme to allow the loading of the first DES key.
	 * Refer to the annexe 9.1 SMWithTransportKey method for details of encryption and mac computation. 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul> 
	 * @see #OCSManagement_SMWithTransportKey(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void SMWithTransportKey()
	{    }

	/**
	 * This method sets the card in a USE state.
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>Set or restore the card state to USE mode, in that state the dispatcher allows the selection of an applet, other that the Card Manager, by its AID.</em>
	 * </ul>
	 * <p>
	 * @see #OCSManagement_useCard(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void useCard()
	{
	}

	/**
	 * This method sets the reference associated SD AID to the AID given in parameters.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param	aid <code>AID</code> of the Application
	 * @param	sdAID <code>AID</code> of the associated SD
	 * @see #OCSManagement_setAssociatedSD(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setAssociatedSD(AID aid, AID sdAID)
	{
	}

	/**
	 * This method gets the associated SD AID corresponding to the AID given in parameters.
	 * @param	aid <code>AID</code> of the Application
	 * @return	<code>AID</code> of the associated SD
	 * @see #OCSManagement_getAssociatedSD(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static AID getAssociatedSD(AID aid)
	{
		return null;
	}
	
	/**
	 * This method sets the reference of native SD Object to the AID given in parameters.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param	aid <code>AID</code> of the Application
	 * @param	nativeSD  native SD <code>Object</code> for a Security Domain or its associated for an application.
	 * @see #OCSManagement_setNativeSDObjectRef(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setNativeSDObjectRef(AID aid, Object nativeSD)
	{
	}
	
/// @cond (!NO_CASD || COP_GP2_2_AMD_B)
	/**
	 * This method allows to create a Temporary or Permanent JCRE Entry Point Object.
	 *
	 * <p>Note:<ul>
	 * <li><em>This may generate a SystemException with reason NO_RESOURCE.</em>
	 * </ul>
	 * </p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  and VM_ENABLE_EPO then it is  @deprecated
	 * @param  packageAID AID of the package containing the class.
	 * @param  classToken	class token in this packeg (see the export file).
	 * @param  temporaryFlag	set this flag to create a Temporary Entry Point Object otherwise the object will be a Permanent EPO.
	 * @return the reference of the newly created object. Don't forget to call your own initialization method.
	 * @see #OCSManagement_JCRE_EPO_BuilderFromToken(void) for native implementation
	 */
	public static Object JCRE_EPO_BuilderFromToken(AID packageAID, byte classToken, boolean temporaryFlag)
	{
		return null;
	}

	/**
	 * This method allows to create a Temporary or Permanent JCRE Entry Point Object.
	 *
	 * <p>Notes:<ul>
	 * <li><em>This method must be used only if the class to instanciate doesn't own a token which happens when the class is package visble. </em>
	 * <li><em>If the class is exported, it is strongly encouraged, for performance reasons, to use the JCRE_EPO_BuilderFromToken method. </em>
	 * <li><em>This may generate a SystemException with reason NO_RESOURCE.</em>
	 * </ul>
	 * </p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  and VM_ENABLE_EPO then it is  @deprecated
	 * @param  objectToClone the object that will be cloned to produce a new EPO object.
	 * @param  temporaryFlag	set this flag to create a Temporary Entry Point Object otherwise the object will be a Permanent EPO.
	 * @return the reference of the newly created object. Don't forget to call your own initialization method.
	 * @see #OCSManagement_JCRE_EPO_BuilderFromObject(void) for native implementation
	 */
	public static Object JCRE_EPO_BuilderFromObject(Object objectToClone, boolean temporaryFlag)
	{
		return null;
	}

	/**
	 * This method allows to create a Temporary or Permanent Global Byte Array.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This may generate a SystemException with reason NO_RESOURCE.</em>
	 * </ul>
	 * </p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  and VM_ENABLE_EPO then it is  @deprecated
	 * @param  length the length of the array.
	 * @param  volatileFlag	set this flag to create a volatile Global array. This array have some chances to be created in RAM. It can be created in EEPROM but will be cleared as it it was in RAM.
	 * @param  temporaryFlag	set this flag to create a Temporary Entry Point Object otherwise the object will be a Permanent EPO.
	 * @return the reference of the newly created array.
	 * @see #OCSManagement_JCRE_EPO_ByteArrayBuilder(void) for native implementation
	 */
	public static byte[] JCRE_EPO_ByteArrayBuilder(short length, boolean volatileFlag, boolean temporaryFlag)
	{
		return null;
	}
/// @endcond
	/**
	 * This method sets the reference of the AID which becomes the implicit selected on the
	 * logical channel given in parameters.
	 * If bit b6 of parameters is set, the method checks if it is possible to set this application as Implicit Selected
	 * on the logical channel. If bit b6 is not set, the AID is written.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @param	aid <code>AID</code> of the Application
	 * @param	param <code>byte</code> parameters to set for Implicit Selection
	 * @return	<code>true</code> if the Application is set, <code>false</code> otherwise.
	 * @see #OCSManagement_setImplicitSelection(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static boolean setImplicitSelection(AID aid, byte param)
	{
		return false;
	}
	
	/**
	 * This method resets the Implicit Selection of the AID given in parameters from all 
	 * logical channel.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @param	aid <code>AID</code> of the Application
	 * @see #OCSManagement_resetImplicitSelection(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void resetImplicitSelection(AID aid)
	{
	}
	
	/**
	 * This method allows the cardmanager to build a response to the getStatus command when it is used to retrieve information 
	 * related to an applet's  instance. This method fills the apdu buffer with implicit selection related to the applet 
	 *
	 *<p> 	'CF'	1		First Implicit Selection Parameter	Optional</p>
	 *<p> ... </p>
	 *<p> 	'CF'	1		Last Implicit Selection Parameter	Optional</p>
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>    
	 * @param aidApplet <code>AID</code> of the instance
	 * @param buffer <code>byte[]</code> in which data shall be written,
	 * @param p_offset <code>short</code> indicating offset within buffer  pointing  on the first byte where the information must be filled 
	 * @return <code>short</code> new value of the offset pointing on the first free byte
	 * @see #OCSManagement_getImplicitSelection(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short getImplicitSelection (AID aidApplet, byte[] buffer, short p_offset)
	{ return p_offset;}
	
/// @cond (COP_GP2_2_AMD_C)	
	/**
	 * This method enables to retrieve the Contactless State of the applet associated with the given <code>AID</code> in parameter.
	 * The state is represented on 1 byte.
	 * <p>
	 * Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>The System used the bit8,bit3,bit2 and bit1. bit7,bit6,bit5 and bit4 have not consistency for the system and can be used by the application</em>
	 * </ul>
	 * </p>
	 * @param aid <code>AID</code> of the Application
	 * @return <code>byte</code> Contactless Applicative state
	 * @see org.globalplatform.contactless.GPCLRegistryEntry#STATE_CL_ACTIVATED
	 * @see org.globalplatform.contactless.GPCLRegistryEntry#STATE_CL_DEACTIVATED
	 * @see org.globalplatform.contactless.GPCLRegistryEntry#STATE_CL_NON_ACTIVATABLE
	 * @see #OCSManagement_getCLStatus(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static byte getCLStatus(AID aid)
	{
		return 0;
	}
	
	/**
	 * This method sets the Contactless State (1 byte) of the applet associated with the given <code>AID</code> in state given in parameter.
	 * The state is represented on 1 byte
	 * <p> Description of byte coding for CLState : </p> 
	 * <pre>
	 * 	| b8 b7 b6 b5  b4 b3 b2 b1 |  description
	 * 	|----------------------------------------
	 * 	| 0  -  -  -   -  -  -  1  |  ACTIVATED
	 * 	| 0  -  -  -   -  -  -  0  |  DEACTIVATED
	 * 	| 1  -  -  -   -  -  -  -  |  NON-ACTIVATABLE
	 * 	| -  X  X  X   X  X  X  -  |  RFU
	 * </pre>
	 * 
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>The System used the bit8,bit3,bit2 and bit1. bit7,bit6,bit5 and bit4 have not consistency for the system and can be used by the application</em>
	 * </ul>
	 * </p>
	 * @param aid <code>AID</code> of the Application
	 * @param CLState <code>byte</code> indicating contactless application state
	 * @see org.globalplatform.contactless.GPCLRegistryEntry#STATE_CL_ACTIVATED
	 * @see org.globalplatform.contactless.GPCLRegistryEntry#STATE_CL_DEACTIVATED
	 * @see org.globalplatform.contactless.GPCLRegistryEntry#STATE_CL_NON_ACTIVATABLE
	 * @see #OCSManagement_setCLStatus(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setCLStatus(AID aid, byte CLState)
	{
	}
/// @endcond

	/**
	 * This method set the GPCL Event notification flag. It allow to resume the notification process
	 * in case of power loss.
	 * @param flag <code>byte</code> indicating flag state : 1 = flag is raised.
	 * @see #OCSManagement_setGPCLEventFlag(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setGPCLEventFlag(byte flag)
	{
	}

/// @cond (COP_GP2_2_AMD_C)
	/**
	 * This method enables to retrieve the Communication Interface Access Parameter of the applet associated with the <code>AID</code> given in parameter.
	 * This parameter is represented on 1 byte.
	 * <pre>
	 * Description of byte coding for InterfaceAccess
	 *	b8	b7	b6	b5	b4	b3	b2	b1		Description
	 *	1	-	-	-	-	-	-	-		ISO7816 supported
	 *	-	1	-	-	-	-	-	-		ISO14443 supported
	 *	-	-	1	-	-	-	-	-		Assigned Implicit Protocol Type F
	 *	-	-	-	1	-	-	-	-		Assigned Implicit Protocol Type B'
	 *	-	-	-	-	1	-	-	-		Assigned Implicit Protocol Type B
	 *	-	-	-	-	-	1	-	-		Assigned Implicit Protocol Type A
	 *	-	-	-	-	-	-	1	-		Continuous Processing On
	 *	-	-	-	-	-	-	-	1		Available when Battery Off
	 *</pre>
	 * 
	 * <p>Note:
	 * <em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </p>
	 * @param aid <code>AID</code> of the Application
	 * @return <code>byte</code> Communication Interface access Parameter.
	 * @see OCSManagement_getInterfaceAccess(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static byte getInterfaceAccess(AID aid)
	{
		return (byte)0;
	}

	/**
	 * This method sets the Communication Interface Access Parameter (1 byte) of the applet associated with the <code>AID</code> given in parameter.
	 * This parameter is represented on 1 byte 
	 * 
	 * <p>Note:
	 * <em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </p>
	 * @param aid <code>AID</code> of the Application
	 * @param mode <code>byte</code> indicating communication interface access parameter.
	 * @see OCSManagement_setInterfaceAccess(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setInterfaceAccess(AID aid, byte mode)
	{
	}

/// @cond (AMD_C_1_2)
	/**
	 * This method sets the Contactless Protocol Type State system parameter.
	 * This parameter is represented on 1 byte 
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param protocolTypeState Contactless Protocol Type State coded as per AMD C 11.2.4.
	 * @param mode indicating if data will be checked, written or both.
	 * The possible mode values are
	 * <ul>
	 * 	<li>MODE_CHECK	0xA5,
	 * 	<li>MODE_SET	0x5A,
	 * </ul>
	 * @see #OCSManagement_setProtocolDataRFActivationDeactivation(void) for native implementation	 
	 * @ingroup M_API_UtilManagement
	 */
	public static void setProtocolDataRFActivationDeactivation(byte protocolTypeState, byte mode)
	{
	}

	/**
	 * This method gets the Contactless Protocol Type State system parameter.<br>
	 * This value is represented on 1 byte 
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @return <code>byte</code> Contactless Protocol Type State coded as per AMD C 11.2.4.
	 * @see #OCSManagement_getProtocolDataRFActivationDeactivation(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static byte getProtocolDataRFActivationDeactivation()
	{
		return (byte)0;
	}
/// @endcond
	
	/**
	 * This method gets the Protocol Data RF related to the Type (A, B, B') specified in parameters.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param RFobj <code>Object</code> associated to the Protocol Data RF (reserved for future use, could be an AID),
	 * @param buffer <code>byte[]</code> in which data will be written,
	 * @param p_offset <code>short</code> in the buffer where data will be written.
	 * @param typeRF <code>short</code> indicating RF type.
	 * @return offset + length of data filled in the buffer.
	 * @see #OCSManagement_getProtocolDataRF(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short getProtocolDataRF(Object RFobj, byte[] buffer, short p_offset, short typeRF)
	{
		return (short)0;
	}

	/**
	 * This method parses, checks and sets the Protocol Data RF related to the Type (A, B, B'). The offset is set on the tag which specified the RF type.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @param RFobj <code>Object</code> associated to the Protocol Data RF (reserved for future use, could be an AID),
	 * @param buffer <code>byte[]</code> containing data,
	 * @param p_offset <code>short</code> in the buffer where data will be checked,
	 * @param length <code>byte</code> indicating length of data to process,
	 * @param RFType <code>short</code> indicating protocol data RF to be checked (A, B, B', F)
	 * @param mode <code>byte</code> indicating if data will be checked, written or both.
	 * The possible mode values are
	 * <ul>
	 * 	<li>MODE_CHECK	0x0A,
	 * 	<li>MODE_SET	0x50,
	 * 	<li>MODE_BOTH	0x5A.
	 * </ul>
	 * @see #OCSManagement_setProtocolDataRF(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setProtocolDataRF(Object RFobj, byte[] buffer, short p_offset, byte length, short RFType, byte mode)
	{
	}
/// @endcond
	
/// @cond (DEPRECATED)
	/**
	 * This method returns a new Protocol Parameters Structure.
	 * 
	 * @return object to store the protocol parameters for protocol data type A, B and B'.
	 * @deprecated
	 */
	public static Object newProtocolParameters()
	{
		return null;
	}
/// @endcond

/// @cond (DEPRECATED)
	/**
	 * Check for a conflict between two contactless protocol parameters.
	 * The object A is the evaluated parameters, and the object B is the
	 * "Current Protocol Data" or the "Application participating in the
	 * conflict resolution".
	 * 	
	 * @param protocolParamA  <code>Object</code> indicating the Evaluated parameters Against B
	 * @param protocolParamB <code>Object</code> indicating the Current Protocol (if null) or the
	 * 							Protocol of the Application participating in the conflict,
	 * @return <code>byte</code> value TRUE (0x5A) is a conflict is detected, or FALSE (0xA5) otherwise.
	 * @deprecated
	 */
	public static byte checkConflictProfile(Object protocolParamA, Object protocolParamB)
	{
		return 0;
	}
/// @endcond

/// @cond (COP_GP2_2_AMD_C_CPP)	
	/**
	 * Check for a conflict between two contactless protocol parameters.
	 * The object A is the evaluated parameters, and the object B is the
	 * "Current Protocol Data" or the "Application participating in the
	 * conflict resolution".
	 * 	
	 * @param appletA <code>AID</code> indicating the current applet
	 * @param protocolParamA <code>Object</code> indicating the Evaluated parameters Against B
	 * @param appletB <code>AID</code> null for Current Protocol, or indicating the applet possibly in conflict
	 * @param protocolParamB <code>Object</code> indicating the Current Protocol (if null) or the
	 * Protocol of the Application participating in the conflict,
	 * @return <code>byte</code> value TRUE (0x5A) is a conflict is detected, or FALSE (0xA5) otherwise.
	 * @see #OCSManagement_checkConflictProfile(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static byte checkConflictProfile(AID appletA, Object protocolParamA, AID appletB, Object protocolParamB)
	{
		return 0;
	}
	
	/**
	 * compute current protocol parameter with Current Protocol Parameter
	 * and Protocol Parameter given in parameter.<br/>
	 * The computation must be finalized with a call to currentRFProtParamRefreshRequested()
	 * 	
	 * @param protocolParam <code>Object</code> indicating protocol parameter object to be computed on activation,
	 * or null to initialize Current protocol parameters with default value.
	 * @see #currentRFProtParamRefreshRequested()
	 * @see #OCSManagement_computeCurrentRFProtParam(void) for native implementation
	 */
	public static void computeCurrentRFProtParam(Object protocolParam)
	{
	}

	/**
	 * Finalize computation of the current RF protocol parameters 
	 * @see #OCSManagement_currentRFProtParamRefreshRequested(void) for native implementation
	 */
	public static void currentRFProtParamRefreshRequested()
	{
	}

	/**
	 * This method switches ON or OFF persistently the specified communication interface at
	 * GlobalPlatform card level.  The technical means for switching an interface
	 * ON and OFF are implementation dependent.
	 * When the contactless interface is switched off, a reset of the contactless
	 * interface shall be performed.
	 * 
 	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 *
	 * @param state <code>SEC_TRUE</code> to activate the CL interface, <code>SEC_FALSE</code> otherwise.
	 * @see #OCSManagement_setCommunicationInterface(void) for native implementation
	 */
	public static void setCommunicationInterface(byte state)
	{
	}
/// @endcond
	
/// @cond (DEPRECATED)
	/**
	 * This method returns a byte indicating if the continous processing is activated or not on the card.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @return 1 if disabled, 2 if enabled.
	 * @deprecated
	 */
	public static byte isContinuousProcessing()
	{
		return (byte)0;
	}

	/**
	 * This method gets the continuous Processing Timer Value.
	 *  
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @return the timer value coding on a short.
	 * @deprecated
	 */
	public static short getContinuousProcessingTimerValue()
	{
		return (short)0;
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C)
	/**
	 * This method sets the continuous Processing Timer Value.
	 *  
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @param buffer <code>byte[]</code> containing data,
	 * @param p_offset <code>short</code> indicating offset in the buffer where data will be checked
	 * @see #OCSManagement_setContinuousProcessingTimerValue(void) for native implementation
	 */
	public static void setContinuousProcessingTimerValue(byte[] buffer, short p_offset)
	{
		
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C || COP_GP2_2_AMD_B)
	/**
	 * Overwritten of the JCSystem.getAppletShareableInterfaceObject() in order to call the serverAID with a clientAID equals to <code>null</code>. 
	 * @param serverAID <code>AID</code> of server applet,
	 * @param parameter <code>byte</code> indicating optional parameter data
	 * @return the Shareable interface object or <code>null</code>. 
	 * @see #OCSManagement_getAppletShareableInterfaceObject(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static Shareable getAppletShareableInterfaceObject(AID serverAID, byte parameter)
	{
		return null;
	}
/// @endcond
	
/// @cond (DEPRECATED)	
	/**
	 * This method enables to copy data in a buffer from a different context. Its behaviour is the same as Util.arrayCopyNonAtomic.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management or security domain Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @param srcBuf source byte array
	 * @param offSrc offset within source byte array to start copy from
	 * @param dstBuf destination byte array
	 * @param offDst offset within destination byte array to start copy into
	 * @param length <code>short</code> length to be copied
	 * @return <code>offDst+length</code>
	 * @see #OCSManagement_arrayCopyNonAtomicPreviousContext(void) for native implementation
	 * @deprecated
	 * @ingroup M_API_UtilManagement
	 */
	public static short arrayCopyNonAtomicPreviousContext(byte[] srcBuf, short offSrc, byte[] dstBuf, short offDst, short length)
	{
		return (short)0;
	}
/// @endcond
	
	/**
	 * This method enables to copy data in a buffer from a different context. Its behaviour is the same as Util.arrayCopy.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management or security domain Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param srcBuf source byte array
	 * @param offSrc offset within source byte array to start copy from
	 * @param dstBuf destination byte array
	 * @param offDst offset within destination byte array to start copy into
	 * @param length <code>short</code> length to be copied
	 * @return <code>offDst+length</code>
	 * @see #OCSManagement_arrayCopyPreviousContext(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short arrayCopyPreviousContext(byte[] srcBuf, short offSrc, byte[] dstBuf, short offDst, short length)
	{
		return (short)0;
	}
	
/// @cond (COP_GP2_2_AMD_B|| COP_GP2_2_AMD_C)		
	/**
	 * This method enables to paste data from a buffer from a different context. Its behaviour is the same as Util.arrayCopyNonAtomic.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management or security domain Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param srcBuf source byte array
	 * @param offSrc offset within source byte array to start copy from
	 * @param dstBuf destination byte array
	 * @param offDst offset within destination byte array to start copy into
	 * @param length <code>short</code> length to be copied
	 * @return <code>offDst+length</code>
	 * @see #OCSManagement_arrayPasteNonAtomicPreviousContext(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short arrayPasteNonAtomicPreviousContext(byte[] srcBuf, short offSrc, byte[] dstBuf, short offDst, short length)
	{
		return (short)0;
	}
/// @endcond
	
/// @cond (COP_GSM || COP_GP2_2_AMD_C || SCP11_SUPPORTED)
	/**
	 * This method enables to paste data from a buffer from a different context. Its behaviour is the same as Util.arrayCopy.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management or security domain Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @param srcBuf source byte array
	 * @param offSrc offset within source byte array to start copy from
	 * @param dstBuf destination byte array
	 * @param offDst offset within destination byte array to start copy into
	 * @param length <code>short</code> length to be copied
	 * @return <code>offDst+length</code>
	 * @see #OCSManagement_arrayPastePreviousContext(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short arrayPastePreviousContext(byte[] srcBuf, short offSrc, byte[] dstBuf, short offDst, short length)
	{
		return (short)0;
	}
/// @endcond
	
/// @cond (COP_GSM)
	/**
	* This method is used to return API librairies on a GET_STATUS.
	* <p>Notes:<ul>
	* <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	* <li><em>Usually this method is invoked during process of getStatus command for OTA card audit </em>
	* </ul>
	* <p>
	* @param buffer contains the return AID of Native Package and its version(major, minor) depending the parameter flag.
	* @param p_offset offset of the buffer where data will be added
	* @param flag first bit8 1; next bit8 0 ; bit7 tlv formated ; bit 6 RFU ; bit 5,4,3,2,1 length of the AID to be retrieved
	* @return the length of data returned in buffer.
	* @see #OCSManagement_lookupAIDIntoPackageLibrairie(void) for native implementation
	* @ingroup M_API_UtilManagement
	*/
	public static short lookupAIDIntoPackageLibrairie(byte[] buffer, short p_offset, byte flag)
	{
		return (short)0;
	}
/// @endcond
	
/// @cond (DEPRECATED)
	/**
	 * This method provides a debbuger for GP API.
	 * It displays in the console the name of the method called.
	 *  
	 * @param methodToken an identifier of the method which defines the displayed message.
	 * @deprecated
	 */
	public static void DEBUGF(byte methodToken)
	{
	}
/// @endcond

/// @cond (VM_ENABLE_JCRE_OBJECT_CREATION)
	/**
	 * This method is used to tag some client objects passed as parameter during a SIO call in order 
	 * to make it accessible (via dedicated methods) from the server context.
	 * <p>Notes:<ul>
	 * <li><em>So far, only arrays can be tagged "Server Accessible".</em>
	 * <li><em>The server accesses the tagged array using the methods: 
	 * <code>OCSManagement.arrayReadNonAtomicPreviousContext</code>, <code>OCSManagement.arrayReadPreviousContext</code>,
	 * <code>OCSManagement.arrayWriteNonAtomicPreviousContext</code>, <code>OCSManagement.arrayWritePreviousContext</code>.</em>	
	 * <li><em>The object to tag must be in the context of the method caller (i.e an applet can only tag its own objects as server accesible).</em>
	 * <li><em>If too much objects have been tagged, a <code>SecurityException</code> is launched.</em>
	 * <li><em>If the <code>object</code> has been previously tagged with <code>enableAccessFromServer</code>, 
	 * this method does nothing.</em> 
	 * </ul>
	 * </p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  then it is  @deprecated 
	 * @param object the array to tag as "Server Accessible".
	 * @throws NullPointerException if <code>object</code> is null
	 * @throws SecurityException <code>object</code> is not an array or 
	 * if <code>object</code> does not belong to the current context or 
	 * if the maximum number of tagged objects has been reached 
	 * @see #OCSManagement_enableAccessFromServer(void) for native implementation
	 */
	public static void enableAccessFromServer (Object object)
								throws 	NullPointerException,
										SecurityException
	{		
	}
	
	/**
	 * This method is used to untag objects previously tagged as "Server Accessible" through a call to 
	 * <code>enableAccessFromServer()</code>
	 * <p>Note:<ul>
	 * <li><em>If the <code>object</code> has not been previously tagged with <code>enableAccessFromServer</code>, 
	 * this method does nothing.</em> 
	 * </ul>
	 * </p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  then it is  @deprecated 
	 * @param object the array to untag.
	 * @throws NullPointerException if <code>object</code> is null:  
	 * @see #OCSManagement_disableAccessFromServer(void) for native implementation
	 */
	public static void disableAccessFromServer (Object object)
								throws 	NullPointerException
	{		
	}
	 
	/**
	 * This method is used to untag all objects previously tagged as "Server Accessible" calls to 
	 * {@link #enableAccessFromServer()}
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  then it is deprecated
	 * @see #enableAccessFromServer(Object)
	 * @see #OCSManagement_disableAccessFromServerForAll(void) for native implementation
	 */
	public static void disableAccessFromServerForAll ()
	{		
	}
	
	/**
	 * Copies an array from the specified source array, beginning at the specified position, 
	 * to the specified position of the destination array. 
	 * This method behaves exactly as <code>Util.arrayCopy</code> does excepted that, in case of sharing,
	 * the source array, if tagged as "Server Accessible", is accessed in the context of the client.
	 * <p>Notes:<ul>
	 * <li><em>If the <code>src</code> array has not been tagged as "Server accessible", it is accessed "traditionally" 
	 * in the current context</em>
	 * <li><em>If this method is called "out of sharing", the <code>src</code> array is also accessed 
	 * "traditionally" in the current context</em>
	 * </ul>
	 * <p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  then it is  deprecated
	 * @param src source byte array
	 * @param srcOff offset within source byte array to start copy from
	 * @param dest destination byte array
	 * @param destOff offset within destination byte array to start copy into
	 * @param length <code>short</code> length to be copied
	 * @throws NullPointerException if <code>object</code> is null:  
	 * @throws ArrayIndexOutOfBoundsException if copying would cause access of data outside array bounds
	 * @throws TransactionException if copying would cause the commit capacity to be exceeded
	 * @return <code>offDst+length</code>
	 * @see #OCSManagement_arrayReadPreviousContext(void) for native implementation
	 */
	public static final short arrayReadPreviousContext(	byte[] src,
														short srcOff,
														byte[] dest,
														short destOff,
														short length)
										throws 	ArrayIndexOutOfBoundsException,
												NullPointerException,
												TransactionException
	{
		return 0;
	}
	
	/**
	 * Copies an array from the specified source array, beginning at the specified position, 
	 * to the specified position of the destination array (non-atomically). 
	 * This method behaves exactly as <code>Util.arrayCopyNonAtomic</code> does excepted that, in case of sharing,
	 * the source array, if tagged as "Server Accessible", is accessed in the context of the client.
	 * <p>Notes:<ul>
	 * <li><em>If the <code>src</code> array has not been tagged as "Server accessible", it is accessed "traditionally" 
	 * in the current context</em>
	 * <li><em>If this method is called "out of sharing", the <code>src</code> array is also accessed 
	 * "traditionally" in the current context</em>
	 * </ul>
	 * </p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  then it is  deprecated
	 * @param src source byte array
	 * @param srcOff offset within source byte array to start copy from
	 * @param dest destination byte array
	 * @param destOff offset within destination byte array to start copy into
	 * @param length <code>short</code> length to be copied
	 * @throws NullPointerException if <code>object</code> is null:  
	 * @throws ArrayIndexOutOfBoundsException if copying would cause access of data outside array bounds
	 * @return <code>offDst+length</code>
	 * @see #OCSManagement_arrayReadNonAtomicPreviousContext(void) for native implementation
	 */
	public static final short arrayReadNonAtomicPreviousContext(byte[] src,
																short srcOff,
																byte[] dest,
																short destOff,
																short length)
													throws 	ArrayIndexOutOfBoundsException,
															NullPointerException
	{
		return 0;
	}
	
	/**
	 * Copies an array from the specified source array, beginning at the specified position, 
	 * to the specified position of the destination array. 
	 * This method behaves exactly as <code>Util.arrayCopy</code> does excepted that, in case of sharing,
	 * the destination array, if tagged as "Server Accessible", is accessed in the context of the client.
	 * <p>Notes:<ul>
	 * <li><em>If the <code>dest</code> array has not been tagged as "Server accessible", it is accessed "traditionally" 
	 * in the current context</em>
	 * <li><em>If this method is called "out of sharing", the <code>dest</code> array is also accessed 
	 * "traditionally" in the current context</em>
	 * </ul>
	 * <p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  then it is deprecated
	 * @param src source byte array
	 * @param srcOff offset within source byte array to start copy from
	 * @param dest destination byte array
	 * @param destOff offset within destination byte array to start copy into
	 * @param length <code>short</code> length to be copied
	 * @throws NullPointerException if <code>object</code> is null:  
	 * @throws ArrayIndexOutOfBoundsException if copying would cause access of data outside array bounds
	 * @throws TransactionException if copying would cause the commit capacity to be exceeded
	 * @return <code>offDst+length</code>
	 * @see #OCSManagement_arrayWritePreviousContext(void) for native implementation
	 */
	public static final short arrayWritePreviousContext(byte[] src,
														short srcOff,
														byte[] dest,
														short destOff,
														short length)
											throws 	ArrayIndexOutOfBoundsException,
													NullPointerException,
													TransactionException
	{
		return 0;
	}

	/**
	 * Copies an array from the specified source array, beginning at the specified position, 
	 * to the specified position of the destination array (non-atomically). 
	 * This method behaves exactly as <code>Util.arrayCopyNonAtomic</code> does excepted that, in case of sharing,
	 * the destination array, if tagged as "Server Accessible", is accessed in the context of the client.
	 * <p>Notes:<ul>
	 * <li><em>If the <code>dest</code> array has not been tagged as "Server accessible", it is accessed "traditionally" 
	 * in the current context</em>
	 * <li><em>If this method is called "out of sharing", the <code>dest</code> array is also accessed 
	 * "traditionally" in the current context</em>
	 * </ul>
	 * <p>
	 * if projet not define VM_ENABLE_JCRE_OBJECT_CREATION  then it is deprecated
	 * @param src source byte array
	 * @param srcOff offset within source byte array to start copy from
	 * @param dest destination byte array
	 * @param destOff offset within destination byte array to start copy into
	 * @param length <code>short</code> length to be copied
	 * @throws NullPointerException if <code>object</code> is null:  
	 * @throws ArrayIndexOutOfBoundsException if copying would cause access of data outside array bounds
	 * @return <code>offDst+length</code>
	 * @see #OCSManagement_arrayWriteNonAtomicPreviousContext(void) for native implementation
	 */
	public static final short arrayWriteNonAtomicPreviousContext(byte[] src,
															 	 short srcOff,
																 byte[] dest,
																 short destOff,
																 short length)
												throws 	ArrayIndexOutOfBoundsException,
														NullPointerException
	{
		return 0;
	}
/// @endcond
	
	/**
	 * Looks up into the registry the AID given in parameter. This method accepts to process an AID that has been created by an Applet 
	 *  
	 * <p>Notes:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * <li><em>This method returns The real AID object if it is present in the registry,  Null otherwise </em>
	 * </ul>
	 * <p>
	 * @param requestedAID <code>AID</code> of Package currently processed
	 * @return real <code>AID</code> object present in the registry
	 * @see #OCSManagement_lookUpFakeAID(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */	
	public static AID lookUpFakeAID (AID requestedAID)
	{
		return null;
	}

/// @cond (COP_GP2_2_AMD_B)	
	/**
	 * This method is used to retrieve the admin session parameters value from default/current object or SD one.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param buffer <code>byte[]</code> to store the admin session parameters 
	 * @param p_offset <code>short</code> offset in the buffer where data will be stored
	 * @param length <code>short</code> indicating length of buffer available
	 * @param tag <code>byte</code> indicating a sub tag of admin session parameters or 0xAA for all tags, see {@link AmdB_AdministrationSession}
	 * @param AdminSessionParam <code>Object</code> associated to the SD's admin session parameter object (Null for current admin parameter)
	 * @return number of bytes that were read.
	 * @exception javacard.framework.ISOException SW_EXCEPTION_NOT_ENOUGH_MEMORY_SPACE (status 0x6A84)
	 * @see #OCSManagement_AMDB_getAdminSessionParameters(void) for native implementation 
	 * @ingroup M_API_UtilManagement
	 * @deprecated if project not define NO_AMD_B_1_1_3
	 */
	public static short AMDB_getAdminSessionParameters(byte[] buffer, short p_offset, short length, byte tag, Object AdminSessionParam)
	{
		return (short)0;
	}
	
	/**
	 * This method is used to retrieve the admin session parameters TLV from default/current object or SD one. using chaining mecanisme
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param buffer	<code>byte[]</code> to store the admin session parameters 
	 * @param p_offset	<code>short</code> offset in the buffer where data will be stored
	 * @param length	<code>short</code> indicating length of buffer available
	 * @param tag 		<code>byte</code> indicating a sub tag of admin session parameters or 0xAA for all tags, see {@link AmdB_AdministrationSession}
	 * @param tagOffset	<code>short</code> indicating offset in stored data to begin output data
	 * @param AdminSessionParam <code>Object</code> associated to the SD's admin session parameter object (Null for current admin parameter)
	 * @param isTLV <code>boolean</code> true to indicate TLV is return otherwise false for only value returned 
	 * 
	 * @return number of bytes that were read.
	 * @see #OCSManagement_AMDB_getAdminSessionParameters(void) for native implementation 
	 * @ingroup M_API_UtilManagement
	 */
	public static short AMDB_getAdminSessionParametersChain(byte[] buffer, short p_offset, short length, byte tag, short tagOffset, Object AdminSessionParam, boolean isTLV)
	{
		return (short)0;
	}
		
	/**
	 * This method merges the admin session parameters provided by PUSH SMS, SD and ISD(default parameters).
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param buffer <code>byte[]</code> containing the data payload of the envelope PUSH SMS 
	 * @param p_offset <code>short</code> offset in the buffer where data will be used
	 * @param length <code>short</code> indicating length of data to process
	 * @param AdminSessionParam <code>Object</code> associated to the SD's admin session parameter object
	 * @return process result: OK(SEC_TRUE) or KO(SEC_FALSE).
	 * @exception javacard.framework.ISOException SW_EXCEPTION_INCORRECT_DATA (status 0x6A80)
	 * @see #OCSManagement_AMDB_mergeAdminSessionParameters(void) for native implementation
	 * @ingroup M_API_UtilManagement  
	 */
	public static byte AMDB_mergeAdminSessionParameters(byte[] buffer, short p_offset, short length, Object AdminSessionParam)
	{
		return (byte)0;
	}

	/**
	 * This method clear the admin session parameters in prarameter. if object in parameter is null this method do nothing except security exception 
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param AdminSessionParam <code>Object</code> associated to the SD's admin session parameter object
	 * @see #OCSManagement_AMDB_CleanAdminSessionParameters(void) for native implementation
	 * @ingroup M_API_UtilManagement  
	 */
	public static void AMDB_CleanAdminSessionParameters(Object AdminSessionParam)
	{
	}
	
	/**
	 * check if given AID is under the SCP8x security domain 
	 *  
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p> 
	 * @param refAID <code>AID</code> to be check
	 * @param criteria <code>byte</code> to set the checking condition  
	 * @return <code>AID</code> who supported SCP81 in the hierarchy otherwise null(is not under SCP81 SD)   
	 * @see #OCSManagement_isUnderSCP8xSD(void) for native implementation  
	 * @ingroup M_API_UtilManagement
	 */	
	public static AID isUnderSCP8xSD(AID refAID, byte criteria)
	{
		return null;
	}
	
	/**
	 * This method is used to create GP Amendment B HTTP header. URI already set in m_baRemoteAdminResponseBuffer
	 * 
	 * @param responseBuffer	<code>byte[]</code> output buffer containing new HTTP header
	 * @param responseBufferOff	<code>short</code> offset in output buffer to start write header after URI
	 * @param responseBufferLen <code>short</code> total length of output buffer
	 * @param nextURILen <code>short</code> next URI length
	 * @param CurrentAdminParam	<code>Object</code> associated to the SD's admin session parameter object (Null for current admin parameter)
	 * @param isResume	<code>byte</code> is a resume header (true or false) 
	 * @param AppContentType <code>byte</code> Application content type (RFM or RAM)
	 * @param scriptStatus	<code>byte</code> Script status (ok , unknown-application, not-a-security-domain or security-error)
	 * @return <code>short</code> offset of the end of data in the responseBuffer.
	 * 		   0xFFFF if error during creating AMD_B HTTP header.
	 * @see #OCSManagement_AMDB_createHTTPHeader(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static final short AMDB_createHTTPHeader(	byte[] responseBuffer, 
														short responseBufferOff,
														short responseBufferLen,
														short nextURILen, 
														Object CurrentAdminParam, 
														byte isResume,
														byte AppContentType, byte scriptStatus) 
	{
		return 0;
	}
	
/// @endcond	
	
/// @cond (COP_GP2_2_AMD_C_PERSISTENT_PRIORITY)		
	/**
	 * Move application to top or bottom of the GP Registry Order list.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p> 
	 * @param topBottom <code>boolean</code> param , true to move to list top, false to move to list bottom
	 * @param aid <code>AID</code> of application
	 * @see #OCSManagement_setSelectionPriority(void) for native implementation  
	 * @ingroup M_API_UtilManagement
	 */
	public static void setSelectionPriority(boolean topBottom, AID aid)
	{
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C_VOLATILE_PRIORITY)
	/**
	 * Add (or append if boolean is true) to volatile priority list
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> of application
	 * @param append <code>boolean</code> param ,true to add the AID at the end of the volatile list.
	 * @see #OCSManagement_setVolatilePriority(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setVolatilePriority(AID aid, boolean append)
	{
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C_CGM)
	/**
	 * Set a Security Domain Cumulative Granted Memory parameters
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param aid <code>AID</code> of application
	 * @param buffer <code>byte[]</code> containing consecutively the CGM values for Non-Volatile Memory and Volatile Memory (4 bytes each) 
	 * @param p_offset <code>short</code> offset of the ROM CGM value in <code>buffer</code>
	 * @return <code>short</code> 0
	 * @see #OCSManagement_setCumulativeGrantedMemory(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short setCumulativeGrantedMemory(AID aid, byte[] buffer, short p_offset)
	{
		return 0;
	}
	
	/**
	 * Get information on Cumulative Granted Memory parameters.<br>
	 * The requested value is written in the specified <code>buffer</code>, preceded by its length (2 or 4 bytes) at position <code>offset</code>.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>   
	 * @param aid <code>AID</code> of application
	 * @param valueType <code>short</code> code determining the type of value to be returned <ul>
	 * 								<li>1st byte: 0=Quota, !0=Remaining
	 * 								<li>2nd byte: 0=Non-Volatile, !0=Volatile </ul>
	 * @param buffer <code>byte[]</code> where to write the requested value preceded by its length
	 * @param p_offset <code>short</code> offset at which to write the data
	 * @see #OCSManagement_getCumulativeGrantedMemory(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void getCumulativeGrantedMemory(AID aid, short valueType, byte[] buffer, short p_offset) {
	}
/// @endcond
	
/// @cond (DEPRECATED)
	/**
	 * Set an application Memory Resource Management parameters
	 * 
	 * @param aid <code>AID</code> of application
	 * @param mrmParams <code>byte[]</code> containing the values of the MRM parameters for (in this order): Reserved NVM (2B), Reserved VM (2B), NVM Quota (4B), VM Quota (4B)
	 * @param p_offset <code>short</code> offset of the MRM params in <code>buffer</code>
	 * 
	 * @return <ul><li>0 if successful<li>not 0 otherwise</ul>
	 * @deprecated
	 */
	public static short setAppMemoryReserveAndQuota(AID aid, byte[] mrmParams, short p_offset) {
		return 0;
	}

	/**
	 * Set a package Reserved Memory parameters
	 *
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>  
	 * @param mrmParams <code>byte[]</code> containing the values of the Reserved Memory parameters for Non-Volatile and Volatile memories
	 * @param p_offset <code>short</code> offset of the MRM params in <code>buffer</code>
	 * 
	 * @return <ul><li>0 if successful<li>not 0 otherwise</ul>
	 * @deprecated
	 * @see #OCSManagement_setPkgMemoryReserve(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static short setPkgMemoryReserve(byte[] mrmParams, short p_offset) {
		return 0;
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C_CGM)
	/**
	 * Set the associated SD for the package being loaded. It is required to update Cumulative Granted Memory properly.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>  
	 * @param associatedSD <code>AID</code> of the SD to be associated to the package  
	 * @see #OCSManagement_setPkgAssociatedSD(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setPkgAssociatedSD(AID associatedSD) {
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C_READER_MODE)	
	/**
	 * Called on reception of an Install for registry update / Install for install with a Tag B0
	 * 
	 * <p>Store the parameters on an array owned by the applet AID
	 * If the applet is already ACTIVATED (update reader RF parameters), HCI shall "push" new reader parameters
	 * 
	 * <p>Check the parameter and throw an IsoException (wrong data (0x6A80) ) if an error occur (for example inconsistencies in parameter length TLV ...)
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p> 
	 * @param associatedApplet <code>AID</code> of associated applet 
	 * @param parameters <code>byte[]</code> containing protocol parameter infomation
	 * @see #OCSManagement_setReaderProtocol(void) for native implementation
	 */
	public static void setReaderProtocol(AID associatedApplet, byte[] parameters)
	{
		return;
	}

	/**
	 * Called on Amendment joinGroup to check is applet is Reader Mode
	 * 
	 * <p>Applet with Reader Mode cannot join a group.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * 
	 * @param appletAid <code>AID</code> of applet
	 * @return <code>boolean</code> TRUE if applet is Reader Mode FALSE otherwise
	 * @see #OCSManagement_isReaderModeApplet(void) for native implementation
	 */
	public static boolean isReaderModeApplet(AID appletAid)
	{
		return false;
	}
/// @endcond
	
	/**
	 * Check if an AID is a package
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * @param aid <code>AID</code> to check
	 * @return <code>boolean</code> TRUE if AID of package FALSE otherwise
	 * @see #OCSManagement_isPackage(void) for native implementation
	 */
	public static boolean isPackage(AID aid)
	{
		return false;
	}
	
/// @cond (AMD_C_DELETE_SD_HIERARCHY)	
	/**
	 * Return AID of associated package, same AID if input AID is a package
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * @param aid <code>AID</code> of application
	 * @return <code>AID</code> of associated package
	 * @see #OCSManagement_getPackageAID(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static AID getPackageAID(AID aid)
	{
		return null;
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C_RECOGNITION_ALGORITHM)
	/**
	 * Set a new recognition algorithm for the referenced application
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * @param aid - AID of the application
	 * @param recognitionAlgorithm - new recognition algorithm as specified by AmdC $6.5
	 * @see #OCSManagement_setRecognitionAlgorithm(void) for native implementation
	 */
	public static void setRecognitionAlgorithm(AID aid, byte[] recognitionAlgorithm) {
	}

	
	/**
	 * Get the referenced application's recognition algorithm
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * if project not define  COP_GP2_2_AMD_C_RECOGNITION_ALGORITHM then it is @deprecated 
	 * @param aid - AID of the application
	 * @param buffer - output buffer
	 * @param p_offset - starting offset in the output buffer
	 * 
	 * @return offset + length of the Recognition Algorithm buffer
	 * @see #OCSManagement_getRecognitionAlgorithm(void) for native implementation
	 */
	public static short getRecognitionAlgorithm(AID aid, byte[] buffer, short p_offset) {
		return 0;
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_B)	
	/**
	 * return information on the availlability of the Key version 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * @param triggeredSDAID	AID of the Security Domain to check
	 * @param KVN	Key Version Number
	 * @param KID	Key Identifier
	 * @return	TRUE if the Key Version Number (KVN) and the Key ID (KID) is present under triggeredSDAID key set list, FALSE otherwise.
	 * @see #OCSManagement_isKeySetInSD(void) for native implementation
	 * @ingroup M_API_UtilManagement
 	 */
	public static boolean isKeySetInSD(AID triggeredSDAID,byte KVN,byte KID){
		return false;
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C_VOLATILE_PRIORITY)
	/**
	 * Indicates whether the referenced AID appears in the Volatile Priority applet list.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * 
	 * @param aid - {@link AID} of the applet to look for in the Volatile Priority applet list.
	 * @return <code>true</code> if the applet with given <code>AID</code> has the Volatile Priority, <code>false</code> otherwise.
	 * @ingroup M_API_UtilManagement
	 */
	public static boolean hasVolatilePriority(AID aid) {
		return false;
	}

	/// @cond (COP_GP2_2_AMD_C_VOLATILE_PRIORITY_V_1_1)
	/**
	 * Returns the <code>AID</code> following <code>aid</code> in the Volatile Priority applet list.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * 
	 * @param aid - {@link AID} of the applet from which to search, <code>null</code> to start over.
	 * @return <code>AID</code> of the next applet in the Volatile Priority applet list, <code>null</code> if finished.
	 * @ingroup M_API_UtilManagement
	 */
	public static AID getNextVolatilePriorityApp(AID aid) {
		return null;
	}
	/// @endcond
/// @endcond
	
/// @cond (COP_GP2_2_AMD_B)
	/**
	 * Set the KVN to use for SCP81 at the platform level (ex : ISIS mode use KVN 0x4X to 0x5X). 
	 * in standard mode kvn value is 0x4X.
	 * @param KVN <code>byte</code> indicating Key Version number to set.
	 * @see #OCSManagement_setSCP81KVN(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setSCP81KVN(byte KVN) {
	}
/// @endcond

/// @cond (AMD_C_1_1)	
	/**
	 * Stub to native implementation of method getSecureElementType from  {@link org.globalplatform.contactless org.globalplatform.contactless} package
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p> 
	 * @return <code>byte</code> secure element type
	 * @see #OCSManagement_getSecureElementType(void) for native implementation
	 * 
	 */
	public static byte getSecureElementType() {
		return (byte)0;
	}
	
	/**
	 * Stub to native implementation of method getHostDeviceUserInterfaceState from {@link org.globalplatform.contactless} package
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @return <code>short</code> indicating state of host device user interface
	 * @see #OCSManagement_getHostDeviceUserInterfaceState(void) for native implementation 
	 */
	public static short getHostDeviceUserInterfaceState() {
		return (short)0;
	}
	
	/**
	 * Stub to native implementation of method launchHostDeviceApplication from {@link org.globalplatform.contactless} package
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * <p>
	 * @param parameters byte array containing the parameters transmitted to the host
	 *            device application
	 * @param p_offset 	offset of the parameters
	 * @param length	length of the parameters
	 * @see #OCSManagement_launchHostDeviceApplication(void) for native implementation 
	 */
	public static void launchHostDeviceApplication(byte[] parameters, short p_offset, short length) {
	}
/// @endcond
	
/// @cond (COP_GP2_2_AMD_C_CPP)
	/**
	 * Tells the mask to switch to volatile activation CPP mode
	 * callback API
	 * @see #OCSManagement_switchCurrentRFProtParamForVolatilePriority(void) for native implementation
	 */
	public static void switchCurrentRFProtParamForVolatilePriority() {
	}
	
	/**
	 * Tells the mask to switch back to normal CPP mode
	 * callback API 
	 * @see #OCSManagement_resetCurrentRFProtParamForVolatilePriority(void) for native implementation
	 */
	public static void resetCurrentRFProtParamForVolatilePriority() {
	}
/// @endcond
	
/// @cond (DEPRECATED)
	/**
	 * This method is related to eUICC.
	 * It is called by the ISDR whan a ISDP under creation is extradited on itself
	 * Since at this time it becomes a real ISDP, it can be unlinked from the generic profile 
	 * and associated to its own profile
	 * @param ISDPAID <code>AID</code> of ISDP
	 * @deprecated
	 */
	public static void setAssociatedProfile(AID ISDPAID)
	{
	}
/// @endcond
	
/// @cond (SPARKY_EUICC_SUPPORTED || EUICC_PROFILE_PKG_SUPPORTED)		
		public static void switchESIMMode(boolean activate)
		{
		}
/// @endcond

/// @cond (GSMA_EUICC_GP_ARCHI)	
	/**
	 * This method is related to eUICC.
	 * It has to be used in case of profile deletion
	 * It temporarly disables the profil segregation.
	 * @see #OCSManagement_disableProfileControl(void) for native implementation
	 */
	public static void disableProfileControl()
	{
	}
	
	/**
	 * This method is related to eUICC.
	 * It reactivate profile segregation after profile deletion
	 * @see #OCSManagement_reactivateProfileControl(void) for native implementation
	 */
	public static void reactivateProfileControl()
	{
	}

	/**
	 * This method is related to eUICC.
	 * It is called on Store Data for all GSMA DGIs
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p> 
	 * @param apduBuffer <code>APDU</code> buffer containing data
	 * @param p_offset <code>short</code> offset whithin APDU buffer where data start
	 * @param dgi <code>short</code> indicating DGI to store
	 * @return <code>short</code> value <ul><li>0 if no DGI found <li>1 if DGI found</ul> 
	 * @see #OCSManagement_dispatchCommand(void) for native implementation
	 */
	public static short dispatchCommand(byte[] apduBuffer, short p_offset, short dgi)
	{
		return 0;
	}
		
	/**
	 * Indicate that the ongoing deletion process is an eUICC Master Delete
	 * and that verification of the ISD-P's POL1 should be bypassed.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p>
	 * @see #OCSManagement_setBypassPOL1(void) for native implementation  
	 */
	public static void setBypassPOL1()
	{
	}
/// @endcond
	
/// @cond (DISABLE_SIMPLE_DES)	
	/**
	 * This method is used to forward proprietary CM locks defined in 
	 * Install Parameters to the native implementation.
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * @param lockValue <code>byte</code> value of the prorietary lock
	 * @see #OCSManagement_setProprietaryCMLocks(void) for native implementation
	 * @ingroup M_API_UtilManagement
	 */
	public static void setProprietaryCMLocks( byte lockValue )
	{
	}	
/// @endcond

/// @cond (DEPRECATED)	
	/**
	 * Returns whether the specified AID corresponds to a package in ROM
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 * @param aid AID object to check
	 * @return <code>true</code> if <code>aid</code> is a package residing in ROM, <code>false</code> otherwise.
	 * @see #OCSManagement_isRomPackage(void) for native implementation
	 * @deprecated
	 */
	public static boolean isRomPackage(AID aid)
	{
		return false;
	}
/// @endcond

/// @cond (GSMA_EUICC_SUPPORTED || SPARKY_EUICC_SUPPORTED)
	/** Constant use in getInfo to know AID corresponds to an ISD-P */
	public static final short GETINFO_ISISDP					=	(short)0xC16E;

	/** Constant use in getInfo to know AID corresponds to a MNO-SD (also called eSIMSD for Sparky) */
	public static final short GETINFO_ISMNOSD					=	(short)0x6EC1;

	/** Constant use in getInfo to know if active profile is Generic profile */
	public static final short GETINFO_UNDER_GENERIC_PROFILE		=	(short)0x5BA3;

	/**
	 * Generic method to return an info from the specified Object.
	 * for info isISDP, check if an AID corresponds to an ISD-P.
	 * <p>This method is related to eUICC.</p>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p> 
	 * @param obj <code>Object</code> on which the information must be retrieve 
	 * @param info <code>short</code> indicating wich info to retrieve
	 * @return <code>short</code> retrieved information.
	 * @see #OCSManagement_getInfo(void) for native implementation
	 */
	public static short getInfo(Object obj, short info)
	{
		return 0;
	}
	
	/// @cond (GSMA_EUICC_GP_ARCHI)
	/** Constant use in getInfoObject to retrieve the ISP-P associated to an application or package */
	public static final short GETINFO_ASSOCIATED_ISDP	=	(short)0xC26F;
	/// @endcond
	
	/// @cond (GENERIC_EUICC_SUPPORTED)	
	/**
	 * Generic method to return an info (of tye Object) from the specified Object.
	 * <p>This method is related to eUICC.</p>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p> 
	 * @param obj <code>Object</code> on which the information must be retrieved 
	 * @param info <code>short</code> indicating wich info to retrieve
	 * @return <code>Object</code> retrieved information.
	 * @see #OCSManagement_getInfoObject(void) for native implementation
	 */
	public static Object getInfoObject(Object obj, short info)
	{
		return null;
	}

	/**
	 * Retrieve the list of all AIDs (applets, packages) present in the profile identified by the given ISD-P.
	 * @param aidTab - AID array used as output buffer
	 * @param isdp - AID of the profile's ISD-P
	 * @return number of AIDs found.
	 */
	public static byte getProfileContent(AID[] aidTab, AID isdp)
	{
		return 0;
	}
	/// @endcond		
/// @endcond

/// @cond (TELF_EUICC_SUPPORTED)
	public static void eUiccResetProfile()
	{
	}
	
	public static void eUiccReceiveProfile(byte[] buffer, short dgi, short length, short p_offset, short available)
	{
	}
	
	public static boolean eUiccProcessProfile()
	{
		return false;
	}
/// @endcond

/// @cond (SCP11_SUPPORTED)
	/**
	 * Generic method to return the AID of the selected Applet.
	 * <p>This method is related to SCP11 (Amendment F).</p>
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * </ul>
	 * </p> 
	 * @return <code>AID</code> AID of the applet currently selected.
	 */
	public static AID getSelectedAID()
	{
		return null;
	}
/// @endcond

/// @cond (GSMA_EUICC_SUPPORTED || SCENARIO_3S)
	/** MNO-SD can receive an Install for Perso targetting its associated ISD-P but with generic ISD-P AID */
	public static final short RESERVED_AID_ISDP		= (short)0x0001; // GSMA_EUICC_SUPPORTED
	/** ELF ISD-P AID */
	public static final short ISDP_PACKAGE_AID		= (short)0x0002; // GSMA_EUICC_SUPPORTED
	/** customer Host Id */
	public static final short SAMSUNG_MOBILE_LCCM	= (short)0x0003; // SCENARIO_3S
	
	/**
	 * Compare to a constant buffer stored in Native code.
	 * @param p_ba_buffer input buffer to compare data with,
	 * @param p_s_offset offset of data,
	 * @param p_s_length length of data,
	 * @param dataToCompare id of constant data.
	 * @return <code>0</code> if data match, <code>-1</code> otherwise.
	 * @see #OCSManagement_compareConstantBuffer(void) for native implementation  
	 */
	public static byte compareConstantBuffer(byte[] p_ba_buffer, short p_s_offset, short p_s_length, short dataToCompare)
	{
		return 0;
	}
/// @endcond
	
/// @cond (SE_CONFIG || UICC_CONFIG || CIC_V2)
	/** Get Data tag '66' */
	public static final short DEFAULT_TAG_66_FOR_SSD_STATIC		= (short)0x0001;
	public static final short DEFAULT_TAG_66_FOR_SSD_SCP		= (short)0x0002;
	
	/**
	 * Copy in p_ba_buffer constant data.
	 * @param p_ba_buffer ouput buffer to copy data to,
	 * @param l_s_offset offset where to copy data,
	 * @param dataToGet id of constant data.
	 * @return <code>l_s_offset</code> + length of copied data.
	 * @see #OCSManagement_getConstantBuffer(void) for native implementation  
	 */
	public static short getConstantBuffer(byte[] p_ba_buffer, short l_s_offset, short dataToGet)
	{
		return 0;
	}
/// @endcond

/// @cond (API_GPCL_1_3)	
	/**
	 * This method returns whether the Contactless interface is active.
	 * 
 	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em>
	 * </ul>
	 *
	 * @return <code>SEC_TRUE</code> if the CL interface is active, <code>SEC_FALSE</code> otherwise.
	 * @see #OCSManagement_getCommunicationInterface(void) for native implementation
	 */
	public static byte getCommunicationInterface()
	{
		return 0;
	}
/// @endcond
	
/// @cond (SAMSUNG_FRA_FEATURES)
	/**
	 * add or remove  iTemAID in CLEAR LIST of sd AID
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * 
	 * @param sdAID			AID of SD to modiy CLEAR LIST
	 * @param iTemAID		AID to add or remove in CLEAR LIST
	 * @param p_b_Action	action to execute (ADD/REMOVE)
	 */
	public static void ClearListAction(AID sdAID, AID iTemAID, byte p_b_Action) {
	}

	/**
	 * remove CLEAR LIST associeted to AID in parameter
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * 
	 * @param sdAID		SD AID to remove CLEAR LIST
	 */
	public static void ClearListRemove(AID sdAID) {
	}

	/**
	 * is itemAID in CLEAR LIST
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to a Security Domain Application if not 0x660A (SecurityException)</em>
	 * 
	 * @param itemAID	AID to check
	 * @return true is AID in parameter is in CLEAR LIST
	 */
	public static boolean isInClearList(AID itemAID) {
		return false;
	}
/// @endcond	
	
/// @cond (GP_FELICA_SUPPORTED)
	/**
	 * Prepare a RAM buffer for type F anti-collision registry full computation.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em></li>
	 * </ul>
	 */
	public static void typeFRegistryReset() {
	}
	
	/**
	 * Check whether the given Protocol Parameter Data TLV is conflicting with currently activated applet parameters.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em></li>
	 * </ul>
	 * 
	 * @param params Buffer containing only the Protocol Parameter Data TLV, starting at offset 0
	 * @param maxEntries Maximum Number of anti-collision parameter entries (maintained at GP level)
	 * @param useRamBuf <code>true</code> if check is part of a full computation (see {@link #typeFRegistryReset()}), <code>false</code> otherwise
	 * @return {@link com.oberthurcs.javacard.security.Constants#TRUE} if a conflict is detected, {@link com.oberthurcs.javacard.security.Constants#FALSE} otherwise 
	 */
	public static byte typeFCheckConflict(byte[] params, byte maxEntries, boolean useRamBuf) {
		return 0;
	}
	

	/**
	 * Check whether the given Protocol Parameter Data TLV is conflicting with currently activated applet parameters.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em></li>
	 * </ul>
	 * 
	 * @param params Buffer containing only the Protocol Parameter Data TLV, starting at offset 0
	 * @param maxEntries Maximum Number of anti-collision parameter entries (maintained at GP level)
	 * @param useRamBuf <code>true</code> if check is part of a full computation (see {@link #typeFRegistryReset()}), <code>false</code> otherwise
	 * @param AIDref if not null check this AID ref is the conflit issue (<code>true</code> is this AID generate a conflict with type F parameter in params array, <code>false</code> otherwise)
	 * @return {@link com.oberthurcs.javacard.security.Constants#TRUE} if a conflict is detected, {@link com.oberthurcs.javacard.security.Constants#FALSE} otherwise 
	 */
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("AID")
	/// @endcond
	public static byte typeFCheckConflict(byte[] params, byte maxEntries, boolean useRamBuf,AID AIDref) {
		return 0;
	}
	
	/**
	 * Append whether the given Protocol Parameter Data TLV to the anti-collision registry.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em></li>
	 * </ul>
	 * 
	 * @param appAid Reference of the applet whose parameters are being appended
	 * @param params buffer containing only the Protocol Parameter Data TLV, starting at offset 0
	 * @param useRamBuf <code>true</code> if check is part of a full computation (see {@link #typeFRegistryReset()}), <code>false</code> otherwise
	 */
	public static void typeFRegistryAppend(AID appAid, byte[] params, boolean useRamBuf) {
	}
	
	/**
	 * Commit the RAM buffer for type F anti-collision registry to SWP module.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em></li>
	 * </ul>
	 */
	public static void typeFRegistryPush() {
	}
	
	/**
	 * Querry SWP module for the AIDs of every applet that has registered anti-collision parameters.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em></li>
	 * </ul>
	 */
	public static AID[] typeFGetRegisteredAids() {
		return null;
	}

	/**
	 * Set the Mode Flag value for the anti-collision parameters identified by the given system code and applet AID.
	 * 
	 * <p>Note:<ul>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em></li>
	 * </ul>
	 * 
	 * @param aid AID of the applet
	 * @param sc System Code
	 * @param mf Mode Flag value should be (0 or 1)
	 * @return 0 if successful</br>
	 *         1 if the System Code is registered with a different AID</br>
	 *         2 if the System Code is not in the anti-collision table
	 */
	public static byte typeFSetModeFlag(AID aid, short sc, byte mf) {
		return 0;
	}

	/**
	 * Retrieve all the registered System Codes of the given applet along with the corresponding Mode Flag.
	 * 
	 * <p>Note:<ul>
	 * <li><em>Output format is (System Code, Mode Flag), (System Code, Mode Flag), ...</em></li>
	 * <li><em>This method is reserved to the Card management Application ( i.e Card Manager or first Applet Instancied on the Card) if not 0x660A (SecurityException)</em></li>
	 * </ul>
	 * 
	 * @param aid Applet whose entries should be retrieved
	 * @param buffer Buffer where the retrieved data is to be written
	 * @param p_offset Output offset in <code>buffer</code>
	 * @return Number of entries retrieved
	 */
	public static byte typeFGetModeFlag(AID aid, byte[] buffer, short p_offset) {
		return 0;
	}
/// @endcond
	
/// @cond (COP_GSM)
	/**
	 * save or restore all Install APDU command from/to APDU buffer (256+5) byte.
	 * 
	 * @param mode byte 0 to save install APDU and 1 to restore install APDU in APDU buffer 
	 */
	public static void SaveRestoreInstallAPDU(byte mode) {
	}
/// @endcond
	
}

/****************************************************************************
 * Historique  :
 *
 *  $Log::   P:/gcl/PLATFORM/archives/Platform Ref/Dev/API/source/com/oberthurcs/javacard/utilmanagement/OCSManagement.java-arc         $
 * 
 *    Rev 1.57   Feb 11 2016 10:25:14   BOISDE
 * Support Amdt B v1.1.3
 * 
 *    Rev 1.56   Dec 22 2015 12:09:36   guerin
 * Add method getCommunicationInterface() to support GPCL API 1.3
 * 
 *    Rev 1.55   Nov 24 2015 15:30:20   BOISDE
 * Release v2.40: add methods compareConstantBuffer and getConstantBuffer.
 * 
 *    Rev 1.54   Nov 19 2015 15:28:50   guerin
 * Add method getInfoObject()
 * 
 *    Rev 1.53   Sep 30 2015 18:15:56   BOISDE
 * Add support of GSME eUICC profile.
 * Move to V2.38
 * 
 *    Rev 1.52   Jul 09 2015 17:12:56   guerin
 * Split EUICC behaviours in core behaviours and 2 specifics modes : SPARKY and GSMA
 * Add method switchESIMMode (SPARKY support)
 * Go to release 2.37
 * 
 *    Rev 1.51   Jun 17 2015 16:56:02   BOISDE
 * Add new method OCSManagement.getSelectedAID (SCP11 support)
 * Go to release 2.36
 * 
 *    Rev 1.50   May 21 2015 16:17:06   guerin
 * Add new method OCSManagement.getProfileContent (eUICC support) 
 * Go to release 2.35
 * 
 *    Rev 1.49   Jan 02 2015 11:13:40   guerin
 * Update doxygen comments
 * 
 *    Rev 1.48   Nov 28 2014 09:38:24   guerin
 * Change the value of couple of constants
 * 
 *    Rev 1.47   Nov 25 2014 15:39:50   guerin
 * Go to release v2.34 
 * Add setProtocolDataRFActivationDeactivation and getProtocolDataRFActivationDeactivation
 * 
 *    Rev 1.46   Nov 10 2014 15:35:14   BOISDE
 * Go to release v2.33
 * Add OCSManagement.setBypassPOL1
 * 
 *    Rev 1.45   Nov 06 2014 12:02:36   guerin
 * setProprietaryCMLocks() is no more deprecated
 * 
 *    Rev 1.44   Oct 21 2014 15:47:34   BOISDE
 * Go to release v2.32.
 * Add two methods for eUICC.
 * 
 *    Rev 1.43   Oct 02 2014 10:53:04   BOISDE
 * Go to release 2.31. Add shorter method for lookupAID.
 * 
 *    Rev 1.42   Sep 12 2014 14:40:16   guerin
 * Add method getImplicitSelection 
 * Go to release 2.30
 * 
 *    Rev 1.41   Aug 21 2014 10:19:02   BOISDE
 * Mark setProprietaryCMLocks as deprecated.
 * No need to update release, export is the same.
 * 
 *    Rev 1.40   Jul 29 2014 15:18:06   guerin
 * Add methods setFinalApplication & isRomPackage
 * Go to release 2.29
 * 
 *    Rev 1.39   Jun 27 2014 11:44:40   guerin
 * Add method setProprietaryCMLocks
 * Go to release 2.28
 * 
 *    Rev 1.38   Apr 17 2014 17:24:50   guerin
 * Add method loadAbort()
 * Cleanup
 * Go to release 2.27
 * 
 *    Rev 1.37   Apr 08 2014 16:53:12   BOISDE
 * Add OCSManagement.getPackageAID(AID). 
 * Go to release 2.26
 * 
 *    Rev 1.36   Feb 21 2014 16:36:04   guerin
 * Add methods setAssociatedProfile, disableProfileControl & reactivateProfileControl
 * 
 *    Rev 1.35   Jan 24 2014 16:45:42   BOISDE
 * Add methods switchCurrentRFProtParamForVolatilePriority and resetCurrentRFProtParamForVolatilePriority.
 * Go to release 24.
 * 
 *    Rev 1.34   Jul 26 2013 19:20:32   guerin
 * Add methods isPackage & isReaderModeApplet
 * Deprecate several unused methods
 * Cleanup
 * 
 *    Rev 1.33   Jul 15 2013 13:08:26   guerin
 * Add several methods for AMD B and AMD V v1.1 
 * - OCSManagement_hasVolatilePriority 
 * - OCSManagement_getNextVolatilePriorityApp 
 * - OCSManagement_setSCP81KVN 
 * - OCSManagement_getHostDeviceUserInterfaceState 
 * - OCSManagement_launchHostDeviceApplication 
 * - OCSManagement_getSecureElementType 
 * Go to release 22
 * 
 *    Rev 1.32   Jul 08 2013 15:34:10   guerin
 * Add method isKeySetInSD
 * 
 *    Rev 1.31   Apr 24 2013 11:16:00   guerin
 * Add new method for reader Mode Support 
 * Go to release 20
 * 
 *    Rev 1.30   Mar 12 2013 16:26:50   guerin
 * Add new methof for Protocol Parameters computation (currentRFProtParamRefreshRequested)
 * Go to release 19
 * 
 *    Rev 1.29   Feb 12 2013 17:20:24   guerin
 * Bypass an issue in APISourcer
 * 
 *    Rev 1.28   Feb 08 2013 15:40:28   guerin
 * Add new methods:
 * - setRecognitionAlgorithm
 * - getRecognitionAlgorithm
 * - install(byte offset, InstallParameters installParams)
 * 
 *    Rev 1.27   Oct 09 2012 15:41:40   guerin
 * Add another Memory Resource Management & Amendment C quotas supporting method
 * 
 *    Rev 1.27   Oct 08 2012 11:09:04   guerin
 * Add another Memory Resource Management & Amendment C quotas supporting method
 * 
 *    Rev 1.26   Aug 22 2012 13:56:38   guerin
 * Add Memory Resource Management & Amendment C quotas supporting methods
 * 
 *    Rev 1.25   Jun 12 2012 14:46:46   guerin
 * Add method indexInRegistry()
 * 
 *    Rev 1.24   Apr 19 2012 15:35:48   guerin
 * Add new method for managing AMD C volatile priority
 * 
 *    Rev 1.23   Apr 13 2012 13:57:10   guerin
 * Add method setSelectionPriority to support Application Selection Priority in AMD C 
 * Switch to version 2.13
 * 
 *    Rev 1.22   Feb 10 2012 18:13:04   guerin
 * Release 2.12 after merge ID / GSM
 * 
 *    Rev 1.24   Nov 18 2011 17:14:38   jeancvin
 * GOP Ref V01.08.n
 * 
 *    Rev 1.23   Nov 04 2011 14:14:32   jeancvin
 * - add a new install method related to Amd C,
 * - increase minor
 * 
 *    Rev 1.22   Oct 14 2011 11:10:12   floreoul
 * -Add methods for CPP_SUPPORTED
 * -Increment Minor
 * 
 *    Rev 1.21   Oct 13 2011 11:48:12   jeancvin
 * - add method arrayPastePreviouscontext,
 * - increase minor
 * 
 *    Rev 1.20   Oct 07 2011 19:01:16   jeancvin
 * - add annotation s,
 * - update comments
 * 
 *    Rev 1.19   Aug 26 2011 15:21:20   jeancvin
 * change prototype of getInstanceInformation and getExecutableModule to use a buffer instead of APDU
 * 
 *    Rev 1.18   Jul 29 2011 11:55:50   jeancvin
 * - add method related to Amd C
 * - increase minor
 * 
 *    Rev 1.17   Jul 20 2011 11:02:42   jeancvin
 * add new methods related to Amd C
 * 
 *    Rev 1.15   Mar 10 2010 10:51:54   jeancvin
 * - add methods related to Amd B/SCWS,
 * - increase minor version
 * 
 *    Rev 1.14   Dec 17 2009 14:25:22   vinatier
 * add 2 methods lookupAIDinLibrariePackage and DEBUGF
 * 
 *    Rev 1.13   Dec 02 2009 12:04:56   vinatier
 * add methods arrayCopyNonAtomicPreviousContext, arrayCopyPreviousContext
 * 
 *    Rev 1.12   Nov 25 2009 17:10:20   vinatier
 * add method isContinuousProcessing
 * 
 *    Rev 1.11   Nov 03 2009 13:38:36   vinatier
 * add methods related to Amendment C
 * update style sheet
 * 
 *    Rev 1.10   Aug 19 2009 08:34:20   vinatier
 * - change prototype of install method,
 * - add setImplicitSelection and resetImplicitSelection methods
 * 
 *    Rev 1.9   Jul 24 2009 13:43:50   vinatier
 * change lockCard prototype
 * 
 *    Rev 1.8   Jul 15 2009 13:39:38   vinatier
 * merge GP2.2 and GP2.1.1 OCSManagement methods
 * 
 *    Rev 1.7   May 07 2009 16:21:40   ziat
 * - Security cheks added to the CASD invokation.
 * - #ifdef values added: CASD_SUPPORTED and CIPHERED_LOAD_FILE.
 * - Putkey.c updated to support the confidential loading of secure channel keys.
 * 
 *    Rev 1.6   Apr 30 2009 16:23:48   vinatier
 * add methods
 * - set.getAssociatedSD,
 * - setNativdeSDObjectRef
 * 
 *    Rev 1.5   Apr 17 2009 09:16:20   vinatier
 * remove get/setStoreData
 * 
 *    Rev 1.4   Mar 17 2009 12:22:28   ziat
 * * CaAppletSecurityDomain with most features.
 * * Enhanced StoreData supporting multiple DGIs in a single command and multiple tags in a single DGI.
 * * APSD confidential personalization almost done.
 * * DELETE refactoring.
 * * Code cleaned.
 * 
 *    Rev 1.3   Oct 22 2008 10:16:42   lesarte
 * Giant comit with all updated files.
 * There is also generated files for an updated project and to use them for Debugger with Bordeaux!
 * 
 *  - Errors verification for all Install
 *  - Clean and refactoring of Install For (Install/ Load/ Extradition/ Registry Update/ Make Selectable)
 *  - Correction on Bug for the Install for Perso
 *  - Modification of process in CM and SD.
 *  - TAG 81/ 82/ 83/ D9 coded
 *  - Clean of the install Method from SD and Card Manager
 *  - Correction on GetStatus & SetStatus
 *  - Added comment, correction and function for the GPEntry
 *  - Security Test on Privilege ( if privilege Exist )
 *  - Basic verification before the set privilege (SD Privilege condition for some other privilege)
 *  - Added restriction byte for Delete and Load command without coding all thoses two in GP22 version yet.
 *  - Correction of all cases and bugs by fully passing the two Test suit (GP211 API Test + ALL Install Test ( except Token verification))
 *  - Merge of the GetStatus and SetStatus
 *  - Update of the Nativ code from the Mask
 *  - Added the merge of the Getstatus modification in nativ code!
 * 
 *    Rev 1.1   Oct 10 2008 15:34:26   vinatier
 * change to be compliant with GP2.2 new version used in GET STATUS
 * 
 *    Rev 1.0   Sep 02 2008 14:10:22   lesarte
 * Initial revision.
 * 
 *
 **************************************************************************/



