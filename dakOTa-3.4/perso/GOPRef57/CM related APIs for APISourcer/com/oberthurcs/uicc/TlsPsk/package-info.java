/**
 * This package implements proprietary methods for the TLSPSK as defined in RFC 4279
 * (Pre-Shared Key Ciphersuites for Transport Layer Security )
 */
@APISourcer_Package_Description(
    AID="A0000000770100000010000000000016",
    version="2.3",
    jcVersion="2.1",
    expmap=true,
    intSupport=false,
    stringSupport=false,
	expmapFile="com/oberthurcs/uicc/tlsPsk/javacard/tlsPsk_map.exp")
package com.oberthurcs.uicc.tlsPsk;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;