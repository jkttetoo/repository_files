package com.oberthurcs.uicc.tlsPsk;
import javacard.framework.CardRuntimeException;

import com.oberthur.apisourcer.annotation.*;

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
first_virtual_method="setReason",
	exception_number_to_raise="EXCEPTION_VM_APDU-EXCEPTION_VM_EXCEPTION",
	number_of_references="0",
	number_field_instance="1",
	implemented_interface="",
	OnCardDebugger_implementation=false
	)
/// @endcond

public class TlsException extends CardRuntimeException{

	public static final short REASON_TLS_OK = 0x0000;
	public static final short REASON_INTERNAL_ERROR = 0x0001;
	public static final short REASON_BAD_INPUT_PARAMETER = 0x0002;
	public static final short REASON_BUFFER_TOO_SMALL = 0x0003;
	public static final short REASON_INVALID_MESSAGE =	0x0004;
	public static final short REASON_UNEXPECTED_CONTENT_TYPE = 0x0005;
	public static final short REASON_UNSUPPORTED_VERSION = 0x0006;
	public static final short REASON_INVALID_LENGTH = 0x0007;
	public static final short REASON_UNEXPECTED_MESSAGE_TYPE = 0x0009;
	public static final short REASON_INVALID_SESSION_ID = 0x000a;
	public static final short REASON_DATA_TOO_BIG = 0x000b;
	public static final short REASON_CRYPT_ERROR = 0x000c;
	public static final short REASON_INTEGRITY_ERROR = 0x000d;
	public static final short REASON_UNSUPPORTED_ALGO = 0x000e;
	public static final short REASON_UNSUPPORTED_COMP = 0x000f;
	public static final short REASON_INVALID_PACKET_SIZE = 0x0010;
	
	
	/**
   * Constructs a <code>TlsException</code> with the specified reason.
   * To conserve on resources use <code>throwIt()</code>
   * to use the Java Card runtime environment-owned instance of this class.
   * @param reason the reason for the exception
   */
  public TlsException(short reason) {
    super(reason);
  }

  /**
   * Throws the Java Card runtime environment-owned instance of <code>TlsException</code> with the specified reason.
   * <p>Java Card runtime environment-owned instances of exception classes are temporary Java Card runtime environment Entry Point Objects
   * and can be accessed from any applet context. References to these temporary objects
   * cannot be stored in class variables or instance variables or array components.
   * See <em>Runtime Environment Specification for the Java Card Platform</em>, section 6.2.1 for details.
   * @param reason the reason for the exception
   * @exception TlsException always
   */
  public static void throwIt (short reason){
  
  }
}

