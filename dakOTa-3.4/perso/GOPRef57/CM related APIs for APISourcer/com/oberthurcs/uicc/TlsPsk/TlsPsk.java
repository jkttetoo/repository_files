/*
   $Workfile:   TlsPsk.java  $
   $Revision:   1.8  $
   $Date:   Dec 30 2014 18:09:12  $
   $Author:   guerin  $
   $Archive:   P:/gcl/PLATFORM/archives/Platform Ref/Dev/API/source/com/oberthurcs/uicc/tlsPsk/TlsPsk.java-arc  $
   $Modtime:   Dec 11 2014 18:08:00  $
   $Log:   P:/gcl/PLATFORM/archives/Platform Ref/Dev/API/source/com/oberthurcs/uicc/tlsPsk/TlsPsk.java-arc  $
 * 
 *    Rev 1.8   Dec 30 2014 18:09:12   guerin
 * Update for Doxygen
 * 
 *    Rev 1.7   Dec 21 2012 16:45:44   guerin
 * Release 2.3
 * Add new TlsInitSession method
 * 
 *    Rev 1.6   Dec 05 2011 15:49:08   guerin
 * Fixes an issue on retry
 * 
 *    Rev 1.5   Oct 14 2011 11:54:22   guerin
 * One method Added
 * 
 *    Rev 1.2   Feb 02 2011 16:08:16   godel
 * add class comment
 * 
 *    Rev 1.1   Sep 30 2010 10:32:08   vasseur
 * ajout annotation pour l'apiSourcerV2
 * 
 *    Rev 1.2   Aug 30 2010 15:11:44   godel
 * update annotaion for new api sourcer
 */
//-----------------------------------------------------------------------------
// PACKAGE DEFINITION
//-----------------------------------------------------------------------------
package com.oberthurcs.uicc.tlsPsk;

import java.lang.*;
import javacard.framework.AID;
import com.oberthur.apisourcer.annotation.*;
						 
//-----------------------------------------------------------------------------
// CLASS DEFINITION
//-----------------------------------------------------------------------------

#if (_OPT_API_TLSPSK == OPT_OFF)
    /** @deprecated */
    @Deprecated
#endif
/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
	first_virtual_method="null",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false
	)
/// @endcond
/**
 * This Class implements proprietary methods for the TLSPSK as defined in RFC 4279
 * (Pre-Shared Key Ciphersuites for Transport Layer Security )
 */
public class TlsPsk
{
	
	public static final byte STATE_NOT_INITIALIZED = 0;
	public static final byte STATE_INIT	= 1;
	public static final byte STATE_RESUME_SESSION =	2;
	public static final byte STATE_HANDSHAKE_DATA_TO_SEND	= 3;
	public static final byte STATE_HANDSHAKE_SENDING_DATA	= 4;
	public static final byte STATE_HANDSHAKE_DATA_TO_COPY	= 5;
	public static final byte STATE_HANDSHAKE_DATA_TO_RECEIVE = 6;
	public static final byte STATE_HANDSHAKE_WAITING_DATA = 7;
	public static final byte STATE_HANDSHAKE_CIPHER_SPEC = 8;
	public static final byte STATE_HANDSHAKE_CHECK_CIPHER_SPEC = 9;
	public static final byte STATE_CONNEXION_DONE = 10;
	public static final byte STATE_DATA_TO_SEND = 11;
	public static final byte STATE_DATA_TO_RECEIVE = 12;
	public static final byte STATE_DATA_TO_PROCESS = 13;
	public static final byte STATE_DATA_TO_COPY	= 14;
	public static final byte STATE_NO_MORE_NVM	= 15;
	
	public static final byte TLS_PSK = 1;
	public static final byte TLS_RSA = 2;
	public static final byte TLS_PSK_RSA = 4;
	public static final byte TLS_CIPHER_NULL = 8;
	
	public static final byte TLS_CLIENT_V1_0 = 1;
	public static final byte TLS_CLIENT_V1_1 = 2;
	public static final byte TLS_CLIENT_V1_2 = 3;
	public static final byte TLS_SERVER_V1_0 = (byte)0x81; 
	public static final byte TLS_SERVER_V1_1 = (byte)0x82;
	public static final byte TLS_SERVER_V1_2 = (byte)0x83;

	/** 
	* Initialize TLS PSK session
	* 
	* @param supportedAlgo	supported algo type:
	*						TLS_PSK and/or TLS_RSA and/or TLS_CIPHER_NULL
	* @param maxPacketSize	TLS extension parameter that limits data buffer size.
	*						Should be 1 to limit to 512 bytes.	 
	* @param sessionType		Specify the type of session to create: TLS_CLIENT or TLS_SERVER
	*							TLS_CLIENT_V1_0 or TLS_CLIENT_V1_1 or TLS_CLIENT_V1_2 or
	*							TLS_SERVER_V1_0 or TLS_SERVER_V1_1 or TLS_SERVER_V1_2
	* @param pskId Identifier of PSK key used in handshake
	* @param pskIdLen length of the pskID up to 128 bytes
	* @param pskIdOffset offset of the pskId in the byte array 
	* @param SDaid AID of SD where key set is referenced
	*
	* @return The state of the initialization. 0 : initialization is correctly done.
	* 
	*/
	/// @cond (!DOXYGEN)
	@APISourcer_ExtraDefinition("SecuDomainAID")
	/// @endcond
	static public short TlsInitSession(
					byte	supportedAlgo,	//[in]
					byte	maxPacketSize,	//[in]
					byte	sessionType,	//[in]
					byte[] pskId,			//[in]
					short pskIdLen,			//[in]
					short pskIdOffset,		//[in]
					AID SDaid				//[in]
					)	
			throws java.lang.NullPointerException, java.lang.ArrayIndexOutOfBoundsException, TlsException
	{
		return 0;
	}	
	/** 
	* Initialize TLS PSK session
	* 
	* @param supportedAlgo	supported algo type:
	*							TLS_PSK and/or TLS_RSA and/or TLS_CIPHER_NULL
	* @param maxPacketSize	TLS extension parameter that limits data buffer size.
	*							Should be 1 to limit to 512 bytes.	 
	* @param sessionType		Specify the type of session to create: TLS_CLIENT or TLS_SERVER
	*							TLS_CLIENT_V1_0 or TLS_CLIENT_V1_1 or TLS_CLIENT_V1_2 or
	*							TLS_SERVER_V1_0 or TLS_SERVER_V1_1 or TLS_SERVER_V1_2
	* @param pskId Identifier of PSK key used in handshake
	* @param pskIdLen length of the pskID up to 128 bytes
	* @param pskIdOffset offset of the pskId in the byte array 
	*
	* @return The state of the initialization. 0 : initialization is correctly done.
	*/
	/// @cond (!DOXYGEN)
	 @APISourcer_ExtraDefinition("SecuDomain")
	/// @endcond
	static public short TlsInitSession(
					byte	supportedAlgo,	//[in]
					byte	maxPacketSize,	//[in]
					byte	sessionType,	//[in]
					byte[] pskId,			//[in]
					short pskIdLen,			//[in]
					short pskIdOffset)	//[in]
			throws java.lang.NullPointerException, java.lang.ArrayIndexOutOfBoundsException, TlsException
	{
		return 0;
	}
	 
	/** 
	* Initialize TLS PSK session
	* 
	* @param supportedAlgo	supported algo type:
	*							TLS_PSK and/or TLS_RSA and/or TLS_CIPHER_NULL
	* @param maxPacketSize	TLS extension parameter that limits data buffer size.
	*							Should be 1 to limit to 512 bytes.	 
	* @param sessionType		Specify the type of session to create: TLS_CLIENT or TLS_SERVER
	*							TLS_CLIENT_V1_0 or TLS_CLIENT_V1_1 or TLS_CLIENT_V1_2 or
	*							TLS_SERVER_V1_0 or TLS_SERVER_V1_1 or TLS_SERVER_V1_2
	* @param pskId Identifier of PSK key used in handshake
	* @param pskIdLen length of the pskID up to 128 bytes
	* @param pskIdOffset offset of the pskId in the byte array 
	* @param psk PSK key 
	* @param pskLen length of the PSK key up to 64 bytes
	* @param pskOffset offset of the PSK key in the byte array 
	*
	* @return The state of the initialization. 0 : initialization is correctly done.
	*/
	static public short TlsInitSession(
					byte	supportedAlgo,	//[in]
					byte	maxPacketSize,	//[in]
					byte	sessionType,	//[in]
					byte[] pskId,				//[in]
					byte	pskIdLen,			//[in]
					short pskIdOffset,	//[in]
					byte[] psk,				//[in]
					byte	pskLen,			//[in]
					short pskOffset)	//[in]
			throws java.lang.NullPointerException, java.lang.ArrayIndexOutOfBoundsException, TlsException
	{
		return 0;
	}

	/** 
	* Manage Client TLS handshake protocol.
	* 
	* @param in 		Buffer containing message sent by the server.
	* @param inLen		Message size. It should be less than 512 bytes long.
	* @param inOffset 	Offset of the message in the in buffer.	 
	* @param out 		Buffer containing message to send to the server.
	* @param outMaxLen 	Maximum size of the out buffer.
	* @param outOffset 	Offset of the message in the out buffer.	  
	* 
	* @return The size of the message to send to the server.
	*/
	static public short TlsHandshake(
						byte[]	in,			//[in]
						short	inLen,		//[in]
						short	inOffset,	//[in]
						byte[]	out,		//[out]
						short	outMaxLen,	//[in]
						short	outOffset)	//[in]
				throws java.lang.NullPointerException,java.lang.ArrayIndexOutOfBoundsException, TlsException
	{
		return 0;
	}

	/** 
	* Manage TLS received data.
	* 
	* @param in 		Buffer containing ciphered message.
	* @param inLen 		Message size.
	* @param inOffset 	Offset of the message in the in buffer.	 
	* @param out 		Buffer containing unciphered payload data.
	* @param outMaxLen 	Maximum size of the out buffer.
	* @param outOffset 	Offset of the data in the out buffer.	  
	* 
	* @return The size of the payload data.
	*/
	static public short TlsReceiveData(
						byte[]	in,			//[in]
						short	inLen,		//[in]
						short	inOffset,	//[in]
						byte[]	out,		//[out]
						short	outMaxLen,	//[in]
						short	outOffset)	//[in]
				throws java.lang.NullPointerException,java.lang.ArrayIndexOutOfBoundsException, TlsException
	{
		return 0;
	}
	/** 
	* Create TLS messages.
	* 
	* @param in 			Buffer containing payload data to send.
	* @param inLen 		Data size.
	* @param inOffset 	Offset of the data in the in buffer.	 
	* @param out 		Buffer containing ciphered message to send.
	* @param outMaxLen 	Maximum size of the out buffer.
	* @param outOffset 	Offset of the message in the out buffer.	  
	* 
	* @return The size of the ciphered message to send.
	*/
	static public short TlsSendData(
						byte[]	in,			//[in]
						short	inLen,		//[in]
						short	inOffset,	//[in]
						byte[]	out,		//[out]
						short	outMaxLen,	//[in]
						short	outOffset)	//[in]
				throws java.lang.NullPointerException,java.lang.ArrayIndexOutOfBoundsException, TlsException
	{
		return 0;
	}

	/** 
	* Give the state of the TLS connection. A connection is ready when the handshake protocol is done.
	* TlsClientHandshake should be called until the TLS connection is ready.
	* 
	* @return The state of the TLS connection:<br>
	* <ul>
	*	<li><code>STATE_NOT_INITIALIZED</code> Session not initialized </li>
	*	<li><code>STATE_INIT</code> Session initialized </li>
	*	<li><code>STATE_RESUME_SESSION</code> Resume Session</li> 
	*	<li><code>STATE_HANDSHAKE_DATA_TO_SEND</code> Handshake data to send</li>
	*	<li><code>STATE_HANDSHAKE_DATA_TO_RECEIVE</code> Handshake waiting data</li>
	*	<li><code>STATE_HANDSHAKE_CIPHER_SPEC</code> </li> Handshake data to send are secured</li>
	*	<li><code>STATE_HANDSHAKE_CHECK_CIPHER_SPEC</code> Handshake data received are secured</li>
	*	<li><code>STATE_CONNEXION_DONE</code> The connection is ready (handshake done)</li>
	*	<li><code>STATE_DATA_TO_SEND</code> Data to send</li>
	*	<li><code>STATE_DATA_TO_RECEIVE</code> Waiting data </li>
	*	<li><code>STATE_DATA_TO_PROCESS</code> Data are being processed</li>
	*	<li><code>STATE_DATA_TO_COPY</code> Data available to be sent and output buffer too small</li>
	* </ul>
	*/
	static public byte TlsGetCnxSet()
				throws java.lang.NullPointerException,java.lang.ArrayIndexOutOfBoundsException, TlsException
	{	
		return (byte)0;
	}
	
	/** 
	* Clear TLS session. Remove keys and structures.
	*/
	static public void TlsCloseSession()
				throws java.lang.NullPointerException, java.lang.ArrayIndexOutOfBoundsException, TlsException
	{
		return;
	}

	/** 
	*	Create a TLS Close Connection message.<br>
	*	Once the TLS connection has been done, this method should be called to generate a message to send to close the TLS session.
	*	It should be called before TlsEndSession.
	* 
	* @param out 		Buffer containing ciphered message to send.
	* @param outMaxLen 	Maximum size of the out buffer.
	* @param outOffset 	Offset of the message in the out buffer.	  
	* 
	* @return The size of the ciphered message to send.
	*/
	static public short TlsSendCloseConnexion(
						byte[]	out,		//[out]
						short	outMaxLen,	//[in]
						short	outOffset)	//[in]
				throws java.lang.NullPointerException,java.lang.ArrayIndexOutOfBoundsException, TlsException
	{
		return 0;
	}

	/** 
	*	Indicate TLS that the sending of data has failed.<br>
	*	This is used by TLS to decrement sending counter in order to avoid a loss of synchronisation.
	*	It should be called once the sending of data has failed.
	*/
	static public void TlsSendFailed()
	{
		return;
	}

}

