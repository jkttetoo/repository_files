package com.fcsd.service;

import com.oberthur.apisourcer.annotation.APISourcer_Class_Description;

import javacard.framework.Shareable;

/// @cond (!DOXYGEN)
@APISourcer_Class_Description(
first_virtual_method="discarded",
	exception_number_to_raise="0",
	number_of_references="0",
	number_field_instance="0",
	implemented_interface="",
	OnCardDebugger_implementation=false)
/// @endcond

public interface FCSDService extends Shareable {
	/**
	 * 
	 * @param keyRef - Used to identify the encryption key. It consists of Key Version // Key ID.
	 * @param buffer - the input buffer of data to be encrypted
	 * @param sOffset - the offset into the input buffer at which to begin encryption
	 * @param sLength - the length of data to be encrypted
	 * @param encBuffer - the output buffer
	 * @param destOffset - the offset into the output buffer where the resulting output data begins
	 * @return number of bytes output in encBuffer
	 */
	public short encrypt(short keyRef, byte[] buffer, short sOffset, short sLength, byte[] encBuffer, short destOffset);

	/**
	 * 
	 * @param keyRef - Used to identify the encryption key. It consists of Key Version // Key ID.
	 * @param buffer - the input buffer of data to be decrypted
	 * @param sOffset - the offset into the input buffer at which to begin decryption
	 * @param sLength - the length of data to be decrypted
	 * @param dekBuffer - the output buffer
	 * @param destOffset - the offset into the output buffer where the resulting output data begins
	 * @return number of bytes output in dekBuffer
	 */
	public short decrypt(short keyRef, byte[] buffer, short sOffset, short sLength, byte[] dekBuffer, short destOffset);

	/**
	 * 
	 * @param certTag - tag for the certificate to be read
	 * @param certBuff - the output buffer of certificate to be read
	 * @param buffOffset - the offset of output buffer at which the certificate begins
	 * @param sOffset - [BLX: offset inside certificate when chaining?]
	 * @param sLength - the length of the certificate to be read
	 * @return remainding length of the unread certificate data
	 */
	public short getCert(byte certTag, byte[] certBuff, short buffOffset, short sOffset, short sLength);
}
