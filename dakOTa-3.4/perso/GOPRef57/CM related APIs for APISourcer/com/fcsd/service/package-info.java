/**
*	Package name: com.fcsd.service for FCSD service support.
*	version 1.0 
*/ 
@APISourcer_Package_Description(
    AID="A000000333FE00000000000000160000",		 
    version="1.0",
    jcVersion="2.1",
    expmap=true,
    expmapFile="com/fcsd/service/javacard/service_map.exp",
    intSupport=false,
    stringSupport=false)

package com.fcsd.service;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;