/*
   $Workfile:   package-info.java  $
   $Revision:   1.7  $
   $Date:   Jul 10 2015 16:49:24  $
   $Author:   VASSEUR  $
   $Archive:   P:/gcl/PLATFORM/archives/Platform Ref/Dev/API/source/org/globalplatform/package-info.java-arc  $
   $Modtime:   Jul 10 2015 15:24:58  $
   $Log:   P:/gcl/PLATFORM/archives/Platform Ref/Dev/API/source/org/globalplatform/package-info.java-arc  $
 * 
 *    Rev 1.7   Jul 10 2015 16:49:24   VASSEUR
 * update 1.6 javadoc comment
 * 
 *    Rev 1.6   Aug 24 2011 15:05:56   vasseur
 * version 1.5
 *
 *    Rev 1.0   Sep 17 2010 15:30:04   godel
 * Initial revision.
 */
/**
 * Provides a framework of classes and interfaces related to core services
 * defined for smart cards based on GlobalPlatform specifications.
 * @version This documentation describes API elements and behaviors associated
 * with version 1.6 of this package export file.
 */
@APISourcer_Package_Description(
    AID="A00000015100",
    version="1.6",
    jcVersion="2.2",
    expmap=true,
    expmapFile="org/globalplatform/javacard/globalplatform_map.exp",
    intSupport=false,
    stringSupport=false)

package org.globalplatform;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;