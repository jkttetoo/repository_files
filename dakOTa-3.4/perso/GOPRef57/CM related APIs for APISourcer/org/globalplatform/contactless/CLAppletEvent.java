package org.globalplatform.contactless; 

import org.globalplatform.*;

/**
 * Definition of contactless events.
 *
 * These events shall be used to notify current state, or state change request in the following methods:<ul>
 * <li>{@link CRSApplication#processCLRequest(GPRegistryEntry, GPCLRegistryEntry, short)}
 * <li>{@link CRELApplication#notifyCLEvent(GPCLRegistryEntry, short)}
 * <li>{@link CLApplet#notifyCLEvent(short)}
 * </ul>
 *
 * @since <ul>
 * <li>export file version 1.0: initial version.
 * <li>export file version 1.1: new constants added.
 * <li>export file version 1.2: new constants added.
 * </ul>
 */
public interface CLAppletEvent 
{
	/**
	 * The applet is now NON ACTIVATABLE on the contactless interface.
	 */
	public static final short EVENT_NON_ACTIVATABLE = (short) 0x81;
	/**
	 * The applet is now ACTIVATED on the contactless interface.
	 */
	public static final short EVENT_ACTIVATED = (short) 0x02;
	/**
	 * The applet is now DEACTIVATED on the contactless interface.
	 */
	public static final short EVENT_DEACTIVATED = (short) 0x82;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_URI} has changed.
	 */
	public final static short EVENT_URI=(short)0x04;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_DISCRETIONARY_DATA} has changed.
	 */
	public final static short EVENT_DISCRETIONARY_DATA=(short)0x05;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_LOGO} has changed.
	 */
	public final static short EVENT_LOGO=(short)0x06;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_PROTOCOL_DATA_TYPE_A} has changed.
	 */
	public final static short EVENT_PROTOCOL_DATA_TYPE_A=(short)0x07;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_PROTOCOL_DATA_TYPE_B} has changed.
	 */
	public final static short EVENT_PROTOCOL_DATA_TYPE_B=(short)0x08;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_PROTOCOL_DATA_TYPE_F} has changed.
	 */
	public final static short EVENT_PROTOCOL_DATA_TYPE_F=(short)0x09;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_DISPLAY_MESSAGE} has changed.
   * @since export file version 1.1
	 */
	public final static short EVENT_DISPLAY_MESSAGE=(short)0x0A;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_IMPLICIT_SELECTION_PROTOCOLS} has changed.
	 */
	public final static short EVENT_IMPLICIT_SELECTION_PROTOCOLS=(short)0x0B;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_FAMILY_IDENTIFIER} has changed.
	 */
	public final static short EVENT_FAMILY_IDENTIFIER=(short)0x0C;
	
	/**
	 * Constant notifying that continuous processing is now enabled.
	 */
	public final static short EVENT_CONTINUOUS_PROCESS_ON=(short)0x0D;
	/**
	 * Constant notifying that continuous processing is now disabled.
	 */
	public final static short EVENT_CONTINUOUS_PROCESS_OFF=(short)0x8D;

	/**
	 * Constant notifying that a CREL application has been added to a CREL Application AID List
	 */
	public final static short EVENT_CREL_ADDED=(short)0x0E;
	/**
	 * Constant notifying that a CREL application has been removed from a CREL Application AID List
	 */
	public final static short EVENT_CREL_REMOVED =(short) 0x8E;
	/**
	 * The applet has joined an Application Group.
	 */
	public final static short EVENT_GROUP_MEMBER_ADDED=(short)0x0F;
	/**
	 * The applet has left an Application Group.
	 */
	public final static short EVENT_GROUP_MEMBER_REMOVED=(short)0x8F;
	/**
	 * The policy restricting the usage of the applet has changed.
   * @since export file version 1.1
	 */
	public final static short EVENT_POLICY_RESTRICTED_APPLICATIONS=(short)0x10;
	/**
	 * The applet has been installed and is in the {@link GPSystem#APPLICATION_INSTALLED} state. 
   *
   * This event shall *not* be notified to the applet being installed.
	 */
	public static final short EVENT_INSTALLED = (short) 0x11;
	/**
	 * The applet has transitioned from the {@link GPSystem#APPLICATION_INSTALLED}
	 * to the {@link GPSystem#APPLICATION_SELECTABLE} state.
   *
   * This event shall be notified to the applet upon successfull processing of
   * the INSTALL [for install and make selectable] or INSTALL [for make
   * selectable] command. Any subsequent return to the SELECTABLE state
   * (e.g. application unlocked or application requesting to return to the
   * SELECTABLE state) shall *not* trigger this event.
	 */
	public static final short EVENT_SELECTABLE = (short) 0x12;
	/**
	 * The applet has been locked.
	 */
	public static final short EVENT_LOCKED = (short) 0x13;
	/**
	 * The applet has been unlocked.
	 */
	public static final short EVENT_UNLOCKED = (short) 0x93;
	/**
	 * The applet has been deleted.
   *
   * This event shall *not* be notified to the applet being deleted.
	 */
	public static final short EVENT_DELETED = (short) 0x14;
	/**
	 * Applet's {@link GPCLRegistryEntry#INFO_DISPLAY_REQUIREMENT} has changed.
   * @since export file version 1.1
	 */
	public final static short EVENT_DISPLAY_REQUIREMENT=(short)0x15;
	
	/**
	 * The applet can be accessed through the Contactless Communication interface (eg ISO 14443).
   * @since export file version 1.1
   * @deprecated Replaced by {@link #EVENT_PROXIMITY_INTERFACE_ACCESS_ENABLED}.
	 */
	public static final short EVENT_COMMUNICATIONS_ISO14443_ENABLED = (short) 0x16;

	/**
	 * The applet cannot be accessed through the Contactless Communication interface (eg ISO 14443).
   * @since export file version 1.1
   * @deprecated Replaced by {@link #EVENT_PROXIMITY_INTERFACE_ACCESS_DISABLED}.
	 */
	public static final short EVENT_COMMUNICATIONS_ISO14443_DISABLED = (short) 0x96;

	/**
	 * The applet can be accessed through the Contactless Communication interface (eg ISO 14443).
   * @since export file version 1.2
	 */
	public static final short EVENT_PROXIMITY_INTERFACE_ACCESS_ENABLED = (short) 0x16;

	/**
	 * The applet cannot be accessed through the Contactless Communication interface (eg ISO 14443).
   * @since export file version 1.2
	 */
	public static final short EVENT_PROXIMITY_INTERFACE_ACCESS_DISABLED = (short) 0x96;

	/**
	 * The applet has been given the highest selection priority.
   * @since export file version 1.2
	 */
	public static final short EVENT_SELECTION_PRIORITY_HIGHEST = (short) 0x17;

	/**
	 * The applet has been given the lowest selection priority.
   * @since export file version 1.2
	 */
	public static final short EVENT_SELECTION_PRIORITY_LOWEST = (short) 0x97;

	/**
	 * The applet has been given the volatile selection priority.
   * @since export file version 1.2
	 */
	public static final short EVENT_VOLATILE_SELECTION_PRIORITY_SET = (short) 0x18;

	/**
	 * The volatile selection priority has been removed from the applet.
   * @since export file version 1.2
	 */
	public static final short EVENT_VOLATILE_SELECTION_PRIORITY_RESET = (short) 0x98;


  ////////////////////////////////////////////////////////

	/**
   * RESERVED FOR FUTURE USE.
   * @since export file version 1.1
	 */
	public static final short EVENT_RFU1 = (short) 0x7F;

}
