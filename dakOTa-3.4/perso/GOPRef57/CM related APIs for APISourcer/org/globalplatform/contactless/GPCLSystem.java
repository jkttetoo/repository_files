package org.globalplatform.contactless;

import javacard.framework.*;
import org.globalplatform.GPRegistryEntry;

/**
 * This class exposes a subset of the behavior of the CRS (OPEN extension) to
 * other on-card components.
 *
 * @since <ul>
 * <li>export file version 1.0: initial version.
 * <li>export file version 1.2: new constants and methods added to help supporting non-UICC secure elements.
 * <li>export file version 1.3:<ul>
 *   <li> updated behavior of method {@link #getHostDeviceUserInterfaceState}.
 *   <li> method {@link #setCommunicationInterface} supports new constants.
 *   <li> new method {@link #isCommunicationInterfaceEnabled}.
 *   </ul>
 * </ul>
 */
public class GPCLSystem 
{
	private GPCLSystem() {}

  /**
	 * The OPEN uses this identifier to retrieve the {@link CLApplet} interface implemented by an applet.
	 */
	public static final byte GPCL_CL_APPLICATION=(byte)0x86;
	/**
	 * The OPEN uses this identifier to retrieve the {@link CRSApplication} interface implemented by an applet.
	 */
	public static final byte GPCL_CRS_APPLICATION=(byte)0x84;
	/**
	 * The OPEN uses this identifier to retrieve the {@link CRELApplication} interface implemented by an applet.
	 */
	public static final byte GPCL_CREL_APPLICATION=(byte)0x85;	
  /**
	 * The OPEN uses this identifier to retrieve the {@link CLAppletActivationPolicy} interface implemented by an applet.
   * @since export file version 1.2
	 */
	public static final byte GPCL_CL_APPLICATION_ACTIVATION_POLICY=(byte)0x83;

	/**
	 * Gets a reference to a {@link GPCLRegistryEntry} interface. <p>
   *
	 * @param oAID AID of the applet whose {@link GPCLRegistryEntry} instance, if
	 * available, shall be retrieved. If <code>null</code>, the {@link GPCLRegistryEntry} 
   * instance corresponding of the applet invoking this method, if available, is returned.
	 * 
	 * @return the {@link GPCLRegistryEntry} interface object, or <code>null</code><ul> 
	 *      <li>if there is no applet with the specified AID or,</li>
	 * 			<li>if the caller 
	 * 				<ul>
	 * 					<li>has not GLOBAL REGISTRY privilege, and
	 * 					<li>has not CONTACTLESS ACTIVATION privilege, and
	 * 					<li>is not the Security Domain directly associated with the applet being investigated, and
	 * 					<li>is not the investigated applet itself, and
	 * 					<li>is a not CREL Application registered in the investigated applet's CREL list
	 * 				</ul></li>
   * </ul>
	 */
	public static GPCLRegistryEntry getGPCLRegistryEntry(AID oAID) {
		return null;
	}

	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for all Contactless applications that belong to any family
	 */
	public static final short AFI_ANY = (short)0x0000;
	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for applications that belong to the Transport family
	 */
	public static final short AFI_TRANSPORT=(short)0x10;
	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for applications that belong to the Financial family
	 */
	public static final short AFI_FINANCIAL=(short)0x20;
	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for applications that belong to the Identification family
	 */
	public static final short AFI_IDENTIFICATION=(short)0x30;
	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for applications that belong to the Telecommunication family
	 */
	public static final short AFI_TELECOMMUNICATION=(short)0x40;
	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for applications that belong to the Medical family
	 */
	public static final short AFI_MEDICAL=(short)0x50;
	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for applications that belong to the Multimedia family
	 */
	public static final short AFI_MULTIMEDIA=(short)0x60;
	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for applications that belong to the Gaming family
	 */
	public static final short AFI_GAMING=(short)0x70;	
	/**
	 * Constant to use with {@link #getNextGPCLRegistryEntry(GPCLRegistryEntry, short)} 
	 * to look for applications that belong to the Data Storage family
	 */
	public static final short AFI_DATA_STORAGE=(short)0x80;

	/**
	 * Iterates over contactless applets belonging to a particular Application Family.<p>
	 * 
	 * Contactless applets are those applets that have access to the contactless
	 * interface ({@link #GPCL_INTERFACE_ISO14443}), as specified by Communication
	 * Interface Access Parameters (see INSTALL system parameter Tag 'A5' or, if
	 * not present, implementation-dependent default OPEN values).
   *
   * This method returns the {@link GPCLRegistryEntry} object for the next
	 * contactless applet that matches the search criteria. This method shall only
	 * return entries corresponding to applets currently having access to the
	 * contactless interface.<p>
   *
	 * <ul>
	 * <li>Application with GLOBAL REGISTRY privilege can iterate over all contactless Applications.
	 * <li>Application with CONTACTLESS ACTIVATION privilege can iterate over all contactless Applications.
	 * <li>A CREL Application can only iterate over its referencing contactless Applications.
	 * <li>Security Domain can only iterate on its directly or indirectly associated contactless Application(s)
	 *     unless it has the GLOBAL REGISTRY privilege.
	 * </ul>
	 * @param oEntry
	 * <ul>
	 * 	<li>if <code>oEntry</code> is null, this method returns the first Entry matching the specified family.
	 *	<li>If the list is empty, the method shall return null.
	 *	<li>If <code>oEntry</code> is not null, and represents an application matching the specified family, 
	 *	this method retrieves the next application matching the specified family, otherwise it shall return null.
	 *	<li>If <code>oEntry</code> points to the last application matching the specified family, the method shall return null.
	 *</ul>
	 * @param sFamily the Application Family to look for (as defined in ISO
	 * 14443-3). This method shall filter registry entries based on an exact match
	 * with this parameter, with the following precisions:
   * <ul>
   * <li> The information carried by the most significant byte (MSB) of this
   * parameter is not defined, therefore the implementation may (but is not
   * required to) ignore it, assuming a value of '00' for it.
   * <li> The Application Family is expected to be encoded by the least
	 * significant byte (LSB) of this parameter. Special rules defined in ISO
	 * 14443-3 regarding sub-family identifiers shall not be taken into account.
	 * </ul>
   *
	 * @return the reference to the {@link GPCLRegistryEntry} interface object of the contactless applet matching the search
	 * criteria; <code>null</code> is returned if no application is matching or the end of the list is reached.
	 * 
	 * @exception ISOException with reason code
	 * <ul>
	 * <li> <code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if
	 * <ul>
	 * <li>the caller has not GLOBAL REGISTRY privilege, and
	 * <li>the caller has not CONTACTLESS ACTIVATION privilege, and
	 * <li>the caller is not a Security Domain or is a Security Domain but the <code>oEntry</code> is not 
	 *     directly associated with this Security Domain, and
	 * <li>the caller is not a CREL Application, or is a CRELApplication but the <code>oEntry</code> is not
	 *     referencing this CREL Application.
	 * </ul>
	 * </ul>
	 * @exception SecurityException if <code>oEntry</code> is not a JCRE-owned instance of {@link GPCLRegistryEntry}.
   * @exception SystemException with reason code <code>SystemException.ILLEGAL_USE</code> if the specified <code>oEntry</code> corresponds to an application that has been deleted.
	 *
	 * @see #AFI_ANY
	 * @see #AFI_TRANSPORT
	 * @see #AFI_TELECOMMUNICATION
	 * @see #AFI_MULTIMEDIA
	 * @see #AFI_MEDICAL
	 * @see #AFI_IDENTIFICATION
	 * @see #AFI_GAMING
	 * @see #AFI_FINANCIAL
	 * @see #AFI_DATA_STORAGE
	 */
	public static GPCLRegistryEntry getNextGPCLRegistryEntry(GPCLRegistryEntry oEntry, short sFamily) {
		return null;
	}

	/**
	 * Sets up or resets the volatile priority for the specified entry. <p>
	 * 
   * An Application may be assigned the volatile priority only if it is already
   * in the ACTIVATED state, or if it is in the DEACTIVATED state and can be
   * activated. Moreover, a Member Application (part of an Application Group)
   * cannot be assigned the volatile priority directly.<p>
   *
	 * Any Application currently having the volatile priority (e.g. possibly a
	 * Member Application whose group has been assigned the volatile priority),
	 * the Application with the {@link
	 * GPCLRegistryEntry#PRIVILEGE_CONTACTLESS_ACTIVATION} privilege, and any CREL
	 * Application referenced by an Application currently having the volatile
	 * priority, may reset the volatile priority.<p>
   *
   * An Application (or Application Group) may be assigned the Volatile Priority
   * even if the Volatile Priority is already assigned. In this case, the
   * implementation shall first check that the Application can be activated and
   * if so, the Volatile Priority shall be first reset before the Application
   * gets the Volatile Priority. The behavior in the case the same Application
   * already has and is assigned again the Volatile Priority remains
   * implementation specific, in particular with respect to notifications.<p>

   * When a request is made to assign the volatile priority, then <ul> 

   * <li>the caller of this method shall have the {@link
   * GPCLRegistryEntry#PRIVILEGE_CONTACTLESS_ACTIVATION} privilege, or

   * <li>the caller of this method shall be the Application corresponding to the
   * specified entry and have the {@link
   * GPCLRegistryEntry#PRIVILEGE_CONTACTLESS_SELF_ACTIVATION} privilege,

   * <li>otherwise: <ul>

	 *   <li>The OPEN shall locate the CRS Application.

	 *   <li>The OPEN shall call {@link
	 *   CRSApplication#processCLRequest(GPRegistryEntry, GPCLRegistryEntry,
	 *   short)} with the <code>requester</code> parameter set to the caller of
	 *   this method, the <code>target</code> parameter set to
	 *   <code>oEntry</code>, and the <code>event</code> parameter set to {@link
	 *   CLAppletEvent#EVENT_VOLATILE_SELECTION_PRIORITY_SET}. This method is
	 *   expected to check whether <code>oEntry</code> entry is allowed to acquire
	 *   the volatile priority before requesting the CRS Application to perform
	 *   the modification.

	 *   <li>If CRS Application accepts the request, then it shall use {@link
	 *   GPCLSystem#setVolatilePriority} to assign the volatile priority to the
	 *   Application.
   *
   * </ul></li></ul>
   *
   * If the volatile priority is assigned, then the {@link
   * CLAppletEvent#EVENT_VOLATILE_SELECTION_PRIORITY_SET} shall be notified. In
   * addition, an Application in the DEACTIVATED state that is assigned the
   * volatile priority shall have its contactless activation state automatically
   * transitioned to the ACTIVATED state: this latter modification shall NOT be
   * notified. <p>
   *
   * If the volatile priority is reset by this method, then the {@link
   * CLAppletEvent#EVENT_VOLATILE_SELECTION_PRIORITY_RESET} shall be
   * notified. In addition, any Application that previously had the volatile
   * priority shall have its contactless activation state reset to its previous
   * value (before being assigned the volatile priority): this latter
   * modification shall NOT be notified.<p>
   *
	 * The OPEN is responsible for notifying the change of volatile priority (if
	 * any) to the following Applications, except to the one that is at the origin
	 * of the request: <ul>

   * <li>the CRS Application if it implements the {@link CRSApplication} or
   * {@link CRELApplication} interface,

   * <li>any CREL Application associated with <code>oEntry</code>, if that CREL
   * Application implements the {@link CRELApplication} interface,

   * <li>the Application represented by <code>oEntry</code>, if it implements
	 * the {@link CLApplet} interface.

   * </ul>

	 * It shall be supported to assign or reset the volatile priority irrespective
	 * of the state of the contactless front end in the handset.  When the
	 * contactless functionality is enabled the OPEN shall ensure that the
	 * contactless front end is provisioned so that it reflects the configuration
	 * of the contactless applets in the OPEN.<p>
	 * 
	 * @param oEntry The entry for which the volatile priority is requested. The
	 * <code>null</code> value shall be used to reset the volatile priority. If
	 * <code>oEntry</code> corresponds to a Head Application, then all
	 * corresponding Member Applications shall be assigned the volatile priority
	 * in the same order as in the GlobalPlatform registry.
   *
	 * @exception ISOException with reason code <ul>

	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller does not
	 * have enough privileges, or the presence of the CRS Application is required
	 * and the CRS Application cannot be located or is not in a selectable state,
	 * or if the CRS Application rejects the request to get the volatile priority.

	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED </code> if the Application corresponding to <code>oEntry</code> <ul>
   *   <li>is not in a selectable state, or
	 *   <li>is in the NON ACTIVATABLE state, or 
   *   <li>cannot be accessed through the contactless interface (see {@link GPCLSystem#checkCommunicationInterfaceAccess}), or
   *   <li>corresponds to a Member Application
   * </ul>

	 * <li><code>ISO7816.SW_WRONG_DATA</code> if the Application cannot be
	 * activated on the contactless interface because of conflicting RF
	 * parameters.

	 * <li><code>ISO7816.SW_UNKNOWN</code> if the call to the {@link
	 * CRSApplication#processCLRequest(GPRegistryEntry, GPCLRegistryEntry, short)}
	 * method resulted in an exception being thrown.

	 * </ul>
   * @exception SystemException with reason code <code>SystemException.ILLEGAL_USE</code> if the specified <code>oEntry</code> corresponds to an Application that has been deleted.
	 * @exception SecurityException if <code>oEntry</code> is not a JCRE-owned instance of {@link GPCLRegistryEntry}.
	 */
	public static void setVolatilePriority(GPCLRegistryEntry oEntry) {}

	/**
	 * The requested information is the DEFAULT_PROTOCOL_DATA_TYPE_A  (i.e. content of INSTALL system parameter Tag '86' when sent to update OPEN parameters). 
	 */
	public final static short CARD_INFO_DEFAULT_PROTOCOL_DATA_TYPE_A=(short)0x01;
	/**
	 * The requested information is the DEFAULT_PROTOCOL_DATA_TYPE_B  (i.e. content of INSTALL system parameter Tag '87' when sent to update OPEN parameters).  
	 */
	public final static short CARD_INFO_DEFAULT_PROTOCOL_DATA_TYPE_B=(short)0x02;
	/**
	 * The requested information is the DEFAULT_PROTOCOL_DATA_TYPE_F (i.e. content of INSTALL system parameter Tag '88' when sent to update OPEN parameters).
	 */
	public final static short CARD_INFO_DEFAULT_PROTOCOL_DATA_TYPE_F=(short)0x03;
	/**
	 * The requested information is the CURRENT_PROTOCOL_DATA_TYPE_A (i.e. encoded as the content of sub tag 'A0' of INSTALL system parameter Tag '86'). 
	 */
	public final static short CARD_INFO_CURRENT_PROTOCOL_DATA_TYPE_A=(short)0x04;
	/**
	 * The requested information is the CURRENT_PROTOCOL_DATA_TYPE_B (i.e. encoded as the content of sub tag 'A0' of INSTALL system parameter Tag '87'). 
	 */
	public final static short CARD_INFO_CURRENT_PROTOCOL_DATA_TYPE_B=(short)0x05;
	/**
	 * The requested information is the CURRENT_PROTOCOL_DATA_TYPE_F (i.e. encoded as the content of sub tag 'A0' of INSTALL system parameter Tag '88').
	 */
	public final static short CARD_INFO_CURRENT_PROTOCOL_DATA_TYPE_F=(short)0x06;
	/**
	 * The requested information is the global CRS update counter.  Each time a
	 * {@link GPCLRegistryEntry} information is updated, this counter is
	 * incremented. Used to indicate off-card applications the need for
	 * synchronization.
	 */
	public final static short CARD_INFO_COUNTER_UPDATE=0x07;
	
  /**
   * The requested information is the current Contactless Protocol Type State
   * Parameter (i.e. encoded as the content of INSTALL system parameter tag
   * '89').
   */
  public static final short CARD_INFO_CONTACTLESS_PROTOCOL_TYPE_STATE=0x08;

	/**
	 * Retrieve the OPEN's conctactless parameters.
	 *  
	 * @param buffer where requested information shall be written
	 * @param offset within <code>buffer</code>, where requested information shall be written
	 * @param info any CARD_INFO_XX constant.
	 * 
	 * @return (<code>offset</code> + length of data written in <code>buffer</code>)
	 *
	 * @exception  <code>ArrayIndexOutOfBoundsException</code>
	 * if storing the Application Information bytes would cause access 
	 * outside array bounds or the <code>offset</code> is negative.
	 * 
	 * @exception NullPointerException
	 * if <code>buffer</code> is <code>null</code>
	 */
	public static short getCardCLInfo(byte[] buffer,short offset,short info){
		return offset;
	}
	
	/**
	 * Constant is used
	 * to manage an ISO14443 based communication interface. 
   * @deprecated Replaced by {@link #GPCL_PROXIMITY_IO_INTERFACE}. 
   * @see #setCommunicationInterface
   * @see #isCommunicationInterfaceEnabled
	 */
	public final static short GPCL_INTERFACE_ISO14443=(short)0x01;

	/**
	 * Constant is used
	 * to manage the availability of the proximity based communication interface. 
   * @since export file version 1.2
   * @see #setCommunicationInterface
   * @see #isCommunicationInterfaceEnabled
	 */
	public final static short GPCL_PROXIMITY_IO_INTERFACE=(short)0x01;

  /**
	 * Constant is used
	 * to manage the state of Contactless Protocol Type A.
   * @since export file version 1.3
   * @see #setCommunicationInterface
   * @see #isCommunicationInterfaceEnabled
   */
  public static final short GPCL_PROXIMITY_IO_PROTOCOL_TYPE_A=(short)0x41;
  /**
	 * Constant is used
	 * to manage the state of the Contactless Protocol Type B.
   * @since export file version 1.3
   * @see #setCommunicationInterface
   * @see #isCommunicationInterfaceEnabled
   */
  public static final short GPCL_PROXIMITY_IO_PROTOCOL_TYPE_B=(short)0x42;
  /**
	 * Constant is used
	 * to manage the state of the Contactless Protocol Type F.
   * @since export file version 1.3
   * @see #setCommunicationInterface
   * @see #isCommunicationInterfaceEnabled
   */
  public static final short GPCL_PROXIMITY_IO_PROTOCOL_TYPE_F=(short)0x44;

	/**
	 * Enables or disables persistently and independently the specified
	 * communication interface or the specified contactless protocol type at
	 * GlobalPlatform card level.  The technical means for enabling or disabling
	 * an interface or contactless protocol type are implementation dependent.
   *
	 * When the contactless interface or contactless protocol type is disabled, a
	 * reset of the contactless interface shall be performed.
   *
	 * @param sInterface 
   * {@link #GPCL_PROXIMITY_IO_INTERFACE}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_A}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_B}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_F}. These values cannot be combined. 
	 * 
	 * @param onOff <code>true</code> to switch ON, <code>false</code> to switch OFF
	 * 
	 * @exception ISOException with reason code <ul>
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> <ul>
   *   <li> if the caller does not have the {@link GPCLRegistryEntry#PRIVILEGE_CONTACTLESS_ACTIVATION} privilege when managing one of the following:    
   * {@link #GPCL_PROXIMITY_IO_INTERFACE}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_A}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_B}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_F}, or
   *   <li> if the communication interface or contactless protocol type specified by <code>sInterface</code> is not supported.
   * </ul>
	 * <li><code>ISO7816.SW_WRONG_DATA</code> if <code>sInterface</code> identifier is not defined.
	 * </ul>
   *
   * @since <ul>
   * <li>export file version 1.0: initial version.
   * <li>export file version 1.3: now also manages contactless protocol types.
   * </ul>
	 */
	public static void setCommunicationInterface(short sInterface, boolean onOff) {}

  /**
   * Retrieve the status of the specified communication interface or contactless protocol type at GlobalPlatform card level.<p>
   * 
	 * @param sInterface 
   * {@link #GPCL_PROXIMITY_IO_INTERFACE}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_A}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_B}, 
   * {@link #GPCL_PROXIMITY_IO_PROTOCOL_TYPE_F}.  These values cannot be combined. 
   *
   * @return Current state of the specified communication interface or contactless protocol type:<li>
   * <li><code>true</code> if enabled.
   * <li><code>false</code> if disabled.
   * </ul>
   *
	 * @exception ISOException with reason code <ul>
	 * <li><code>ISO7816.SW_WRONG_DATA</code> if <code>sInterface</code> identifier is not defined.
	 * </ul>
   *
   * @since export file version 1.3
   */
  public static boolean isCommunicationInterfaceEnabled(short sInterface) {
    return false;
  }
  

  /**
   * Checks whether a {@link GPRegistryEntry} can be accessed through specified communication interface. 
	 * @param sInterface {@link #GPCL_PROXIMITY_IO_INTERFACE}
	 * @exception ISOException with reason code <ul>
	 * <li><code>ISO7816.SW_WRONG_DATA</code> if <code>sInterface</code> identifier is not defined.
	 * </ul>
   * @since export file version 1.1
   */  
  public static boolean checkCommunicationInterfaceAccess(GPRegistryEntry entry, short sInterface) {
    return false;
  }

  /*
   * **************************************************************************
   * * ADDED IN VERSION 1.2
   * **************************************************************************
   */

	/** 
	 * The Secure Element type is of unknown type.
   * @since export file 1.2
	 */
	public static final byte SECURE_ELEMENT_UNKNOWN = (byte) 0x00;
	
	/** 
	 * The Secure Element is of type UICC.
   * @since export file 1.2
	 */
	public static final byte SECURE_ELEMENT_UICC = (byte) 0x01;
	
	/** 
	 * The Secure Element is of type Embedded Secure Element.
   * @since export file 1.2
	 */
	public static final byte SECURE_ELEMENT_EMBEDDED = (byte) 0x02;
	
	/** 
	 * The Secure Element is of type Smart SD or Smart Micro SD Card.
   * @since export file 1.2
	 */
	public static final byte SECURE_ELEMENT_SDCARD = (byte) 0x03;
	
	
	/**
	 * Gets the type of the Secure Element. 
	 * <p>
	 * Possible values are: 
	 * <ul>
   * <li>{@link #SECURE_ELEMENT_UNKNOWN}</li>
   * <li>{@link #SECURE_ELEMENT_UICC}</li>
   * <li>{@link #SECURE_ELEMENT_EMBEDDED}</li>
   * <li>{@link #SECURE_ELEMENT_SDCARD}</li>
   * </ul>
   * 
	 * @return the secure element type
   * @since export file 1.2
	 */
	public static byte getSecureElementType()
	{
		return SECURE_ELEMENT_UNKNOWN;
	}

	/** 
	 * Bit mask meaning that implementation is not able to provide any information
	 * about the state of the host device UI. If this bit mask is set, the state
	 * of the UI is unknown and all other bits shall be ignored.
   * @since export file 1.2
   * @see #getHostDeviceUserInterfaceState
	 */
	public static final short HOST_DEVICE_UI_STATE_UNKNOWN = (short) 0x8000;

	/** 
	 * Bit mask meaning that the host device UI is currently available. If this
	 * bit mask is set, the UI shall be considered available, otherwise it shall
	 * be considered unavailable.
   * @since export file 1.2
   * @see #getHostDeviceUserInterfaceState
	 */
	public static final short HOST_DEVICE_UI_AVAILABLE = (short) 0x4000;

	/**
	 * Gets the current state of the host device user interface (UI).<p>
	 * 
	 * @return 

   * If the implementation is based on version 12.1 or higher of the HCI
   * specification (ETSI TS 102 622), then the following behaviour shall be
   * implemented:<ul>

   * <li>If the connectivity gate is not available, or the GET PARAMETER HCI
   * command cannot be sent on the connectivity gate, or the GET PARAMETER HCI
   * command is rejected because not implemented (older version of the
   * connectivity gate), then this method shall return
   * HOST_DEVICE_UI_STATE_UNKNOWN.

   * <li>If the value of the UI_STATE registry entry is '00' (UI availability
   * unknown), then {@link #HOST_DEVICE_UI_STATE_UNKNOWN}.

   * <li>If the value of the UI_STATE registry entry is '01' (UI is fully
   * available) or '03' (UI is locked and user can be notified), then this
   * method shall return {@link #HOST_DEVICE_UI_AVAILABLE}.

   * <li>If the value of the UI_STATE registry entry is '02' (UI is locked and
   * user cannot be notified) or '04' (UI is unlocked but user cannot be
   * notified), then this method shall return a value of '00' (i.e. no bit set).

   * <li>For any other value of the UI_STATE registry entry, this method shall
   * return {@link #HOST_DEVICE_UI_STATE_UNKNOWN}.

   * </ul>

   * Otherwise the implementation shall return an answer based on proprietary
   * mechanisms.
   *
   * @since <ul>
   * <li>export file version 1.2: initial version.
   * <li>export file version 1.3: updated behavior following the publication of version 12.1 of ETSI TS 102 622.
   * </ul>
	 */
	public static short getHostDeviceUserInterfaceState()
	{
		return HOST_DEVICE_UI_STATE_UNKNOWN;
	}
	    
  /**
   * Notifies the host device that it shall launch an application (on the host
   * device) that is associated with the applet instance invoking this
   * method.<p>
   *
   * This is a non-blocking method.<p>
   *
   * For an implementation based on the HCI specified in [ETSI TS 102 622], the
   * AID of the applet instance invoking this method and the parameters passed
   * to this method are typically used to create an event of type
   * EVENT_TRANSACTION. 
   *
   * @param parameters byte array containing the parameters transmitted to the host device application
   * @param offset offset of the parameters
   * @param length length of the parameters
   *
	 * @exception ISOException with reason code <ul>
   * <li><code>ISO7816.SW_FUNCTION_NOT_SUPPORTED</code> if the card does not support host device notifications.
   * <li><code>ISO7816.SW_FILE_FULL</code> if the card does not have enough resources to perform the notification.
   * <li><code>ISO7816.SW_WRONG_LENGTH</code> if the length of the parameters is invalid (i.e. greater than 255).
   * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the conditions to call this method are not satisfied.
   * </ul>
   * @exception NullPointerException if <code>parameters</code> is null.
   * @exception ArrayIndexOutOfBoundsException if operation would cause access of data outside array bounds.
   * @exception SecurityException if <code>parameters</code>is not accessible in
   * the caller's context e.g. <code>parameters</code> is not a global array nor
   * an array belonging to the caller context.
   *
   * @since export file 1.2
   */
	static public void launchHostDeviceApplication(byte[] parameters, short offset, short length) {} 

}
