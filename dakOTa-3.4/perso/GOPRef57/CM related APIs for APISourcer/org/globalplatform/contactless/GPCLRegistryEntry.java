package org.globalplatform.contactless;

import javacard.framework.*;

import org.globalplatform.GPRegistryEntry;

/**
 * Defines methods that manage the GlobalPlatform registry extension on contactless interface
 * for an applet.<p>
 *
 * All OPEN-owned objects implementing the {@link GPRegistryEntry} interface
 * shall also implement and may be casted to the {@link GPCLRegistryEntry}
 * interface.<p>
 *
 * When an Application is deleted, then the {@link GPCLRegistryEntry} object
 * corresponding to this Application shall be disabled and all its methods,
 * except the {@link GPRegistryEntry#getAID} method, shall throw a {@link
 * SystemException} with reason code
 * <code>SystemException.ILLEGAL_USE</code>. The OPEN shall ensure that this
 * {@link GPCLRegistryEntry} object can never be re-enabled, even if an
 * Application with the same AID as the Application previously bound to this
 * {@link GPCLRegistryEntry} object is installed.  The OPEN shall not return
 * this {@link GPCLRegistryEntry} object as the result of any of the methods
 * this API. An application holding a reference to a disabled {@link
 * GPCLRegistryEntry} object should release it, as it has become useless and
 * will only throw exceptions.<p>
 *
 * Note: Developers shall pay attention to the {@link CRELApplication}, 
 * {@link CLApplet} and {@link CRSApplication} interfaces being notified upon usage of
 * {@link GPCLRegistryEntry} services. Therefore, it is strongly recommended
 * *not* to call the services provided by this interface when a Java Card
 * transaction is under progress.
 *  
 * @see GPCLSystem#getGPCLRegistryEntry(AID)
 * @see GPCLSystem#getNextGPCLRegistryEntry(GPCLRegistryEntry, short)
 *
 * @since <ul>
 * <li>export file version 1.0: initial version.
 * <li>export file version 1.1: new constants added.
 * <li>export file version 1.2: new constants added.
 * <li>export file version 1.3: updated behavior of method {@link #setCLState}.
 * </ul>
 */
public interface GPCLRegistryEntry extends GPRegistryEntry 
{
	/**
	 * Contactless Activation privilege (18).
	 * This privilege allows <ul>
	 * <li> Enable/Disable ISO 14443 interface.</li>
	 * <li> Activate/Deactivate an applet on the ISO 14443 interface.</li>
	 * <li> Managing the GlobalPlatform Registry order (for partial selection).</li>
	 * <li> Managing the GlobalPlatform Volatile Priority List.</li>
	 * </ul>
	 */
	public final static byte PRIVILEGE_CONTACTLESS_ACTIVATION = (byte) 18;

	/**
	 * Self Activation privilege (19). This privilege
	 * allows an Applet to transition to ACTIVATED on the Contactless interface.
	 */
	public final static byte PRIVILEGE_CONTACTLESS_SELF_ACTIVATION = (byte) 19;

	/**
	 * Constant used to change the state of an applet to NON ACTIVATABLE over the
	 * contactless interface. When applet is NON ACTIVATABLE, it is also
	 * implicitly DEACTIVATED.
	 */
	public static final byte STATE_CL_NON_ACTIVATABLE = (byte) 0x80;

	/**
	 * Constant used to activate an applet on the contactless interface.
	 * An applet that is currently NON ACTIVATABLE can not be ACTIVATED.
	 */
	public static final byte STATE_CL_ACTIVATED = (byte) 0x01;

	/**
	 * Constant used to deactivate an applet on the contactless interface.
	 */
	public static final byte STATE_CL_DEACTIVATED = (byte) 0x00;

	/**
	 * Changes the contactless activation state of the Application represented by
	 * this entry. <p>
   *
	 * Only the Application represented by this entry may transition to or from
	 * the NON ACTIVATABLE state.  When this entry represents a
	 * Group Member currently in the NON ACTIVATABLE state and
	 * that Group Member requests transioning to the DEACTIVATED
	 * state (i.e. only allowed transition request at this time), then it shall be
	 * automatically transitioned to the same activation state as its associated
	 * Head Application (except if the Head Application is in the {@link
	 * #STATE_CL_NON_ACTIVATABLE} state).<p>
   *
	 * The Application represented by this entry, or the Application with the
	 * {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege, or CREL Applications
	 * registered in the CREL list of the Application represented by this entry,
	 * may transition this entry to the DEACTIVATED state.<p>
   *
	 * When a request is made to transition this entry to the {@link
	 * #STATE_CL_ACTIVATED} state, then <ul>

   * <li>If the caller of this method is not the Application represented by this entry then:<ul>

   *   <li>If it doesn't have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION}
   *   privilege then the caller doesn't have enough privileges and an exception
   *   shall be thrown (see below).

   *   <li>Otherwise, the OPEN shall attempt retrieving the {@link CLAppletActivationPolicy} instance 
   *   of the Application represented by this entry and calling its {@link CLAppletActivationPolicy#acceptActivation()} method. 
   *   If the {@link CLAppletActivationPolicy} instance is available and the {@link CLAppletActivationPolicy#acceptActivation()} 
   *   method returns false, an exception shall be thrown (see below).

   *   </ul>

   * <li>If the caller of this method is the Application represented by this entry then:<ul>

   *   <li>If it doesn't have the {@link #PRIVILEGE_SELF_ACTIVATION} privilege
   *   and doesn't have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege,
   *   then the OPEN shall locate the CRS Application and call its 
   *   {@link CRSApplication#processCLRequest(GPRegistryEntry, GPCLRegistryEntry, short)} method
   *   with the <code>requester</code> parameter set to the caller of
   *   this method, the <code>target</code> parameter set to this entry, and the
   *   <code>event</code> parameter set to {@link CLAppletEvent#EVENT_ACTIVATED}
   *   event. This call allows the CRS Application to check whether the
   *   requested transition is permitted and if so, to perform the requested
   *   transition using the {@link #setCLState(byte)} method. If the transition is not
   *   permitted, the activation state of the Application remains unchanged.

   *   </ul>

   * </ul>
   *
	 * The OPEN is responsible for notifying the transition (if any) to the
	 * following Applications, except to the one that is at the origin of the
	 * request: <ul>

   * <li>the CRS Application if it implements the {@link CRSApplication} or
   * {@link CRELApplication} interface,

   * <li>any CREL Application associated with this entry, if that CREL
   * Application implements the {@link CRELApplication} interface,

   * <li> the Application represented by this entry, if it implements the
	 * {@link CLApplet} interface.

   * </ul>
   *
	 * It shall be supported to change the activation state of a contactless
	 * applet irrespective of the state of the contactless front end in the handset.
	 * When the contactless functionality is enabled the OPEN shall ensure that
	 * the contactless front end is provisioned so that it reflects the
	 * configuration of the contactless applets in the OPEN.<p>
	 * 
	 * @param state requested activation state on contactless interface.
	 * The possible states are: <ul>
	 * 	<li>{@link #STATE_CL_ACTIVATED}</li>
	 * 	<li>{@link #STATE_CL_DEACTIVATED}</li>
	 * 	<li>{@link #STATE_CL_NON_ACTIVATABLE}</li>
	 * </ul>
   *
	 * @return the resulting activation state on contactless interface. 
	 * 
	 * @exception ISOException with reason code
	 * <ul>

	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller does not
	 * have enough privileges, or the presence of the CRS Application is required
	 * and the CRS Application cannot be located or is not in a selectable
	 * state.</li>

	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the requested state is ACTIVATED and the Application corresponding to this entry either <ul>
   *   <li>is not in a selectable state, or
	 *   <li>is in the NON ACTIVATABLE state, or 
	 *   <li>cannot be accessed through the contactless interface (see {@link GPCLSystem#checkCommunicationInterfaceAccess}), or
   *   <li>is a Member Application and corresponding Head Application is not in the ACTIVATED state.
   *   <li>rejected the activation request (i.e. its {@link CLAppletActivationPolicy#acceptActivation()} method returned false).
   * </ul>

	 * <li><code>ISO7816.SW_WRONG_DATA</code> if the Application cannot be
	 * activated on the contactless interface because of conflicting RF
	 * parameters.</li>

	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the requested
	 * activation state transition is not valid.</li>

	 * <li><code>ISO7816.SW_UNKNOWN</code> if the call to {@link
	 * CRSApplication#processCLRequest(GPRegistryEntry, GPCLRegistryEntry, short)}
	 * or {@link CLAppletActivationPolicy#acceptActivation()} resulted in an
	 * exception being thrown.</li> 
   *
   * </ul>
	 * 
	 * @see GPRegistryEntry
	 */
	public byte setCLState(byte state);

	/**
	 * Retrieves the contactless activation state of the Application.
	 * 
	 * @return byte value of contactless activation state: <ul>
	 * 		<li>{@link #STATE_CL_ACTIVATED}
	 * 		<li>{@link #STATE_CL_DEACTIVATED}
	 * 		<li>{@link #STATE_CL_NON_ACTIVATABLE}
   * </ul>
	 */
	public byte getCLState();

	/**
	 * The requested information is an URI (i.e. content of Uniform Ressource
	 * Identifier Tag '5F50').
	 */
	public final static short INFO_URI=(short)0x0004;
	/**
	 * The requested information is discretionary data.
	 */
	public final static short INFO_DISCRETIONARY_DATA=(short)0x0005;
	/**
	 * The requested information is a logo (i.e. content of Application Image
	 * Template - Tag '6D').
	 */
	public final static short INFO_LOGO=(short)0x0006;
	/**
	 * The requested information is the PROTOCOL_DATA_TYPE_A (i.e. content of
	 * INSTALL system parameter Tag '86'). If the application was personalized
	 * with a reference to a Protocol Parameter Profile (tag 'A2'), the value of
	 * the protocol parameter data shall be returned.
	 */
	public final static short INFO_PROTOCOL_DATA_TYPE_A=(short)0x0007;
	/**
	 * The requested information is the PROTOCOL_DATA_TYPE_B (i.e. content of
	 * INSTALL system parameter Tag '87'). If the application was personalized
	 * with a reference to a Protocol Parameter Profile (tag 'A2'), the value of
	 * the protocol parameter data shall be returned.
	 */
	public final static short INFO_PROTOCOL_DATA_TYPE_B=(short)0x0008;
	/**
	 * The requested information is the PROTOCOL_DATA_TYPE_F (i.e. content of
	 * INSTALL system parameter Tag '88'). 
	 */
	public final static short INFO_PROTOCOL_DATA_TYPE_F=(short)0x0009;
	
	/**
	 * The requested information is a diplay message (i.e. content of Display
	 * Message - Tag '5F45').
   * @since export file version 1.1
	 */
	public final static short INFO_DISPLAY_MESSAGE=(short)0x000A;

	/**
	 * The requested information is an Application Family Identifier.<p>
   *
   * This information is encoded as follows:
	 * <ul>
	 * <li>MSB byte 1 is set to 00.</li>
   * <li>LSB byte 2 holds the AFI as defined in table 12 of ISO/IEC 14443-3.</li>
	 * <li>Other values of are reserved for proprietary usage.</li>
   *</ul>
	 */
	public final static short INFO_FAMILY_IDENTIFIER=(short)0x000B;
	
	/**
	 * The requested information is a list of protocols (Type A, Type B and/or Type F) used for implicit selection.<p>
   *
   * This information is encoded as a sequence of bytes 
	 * representing the value(s) of one or more of the following constants:<ul>
	 * <li>{@link #IMPLICIT_SELECTION_TYPE_A}</li>
	 * <li>{@link #IMPLICIT_SELECTION_TYPE_B}</li>
	 * <li>{@link #IMPLICIT_SELECTION_TYPE_F}</li>
	 * </ul>
   *
	 * If the sequence is empty, the application can be implicity selected 
	 * using any of these protocol types (see definition of TLV "Assigned 
	 * Protocol for Implicit Selection").
	 */
	public final static short INFO_IMPLICIT_SELECTION_PROTOCOLS=(short)0x000C;

	/**
	 *  Constant indicating that the application supports contactless protocol Type A for implicit selection
	 */
	public static final byte IMPLICIT_SELECTION_TYPE_A = (byte)0x81;

	/**
	 *  Constant indicating that the application supports contactless protocol Type B for implicit selection
	 */
	public static final byte IMPLICIT_SELECTION_TYPE_B = (byte)0x82;

	/**
	 *  Constant indicating that the application supports contactless protocol Type F for implicit selection
	 */
	public static final byte IMPLICIT_SELECTION_TYPE_F = (byte)0x84;

	/**
	 * The requested information is the Continuous Processing Indicator.<p>
   *
   * This information may have one of the following values:
   * <ul>
   * <li>{@link #CONTINUOUS_PROCESS_ENABLED}</li>
   * <li>{@link #CONTINUOUS_PROCESS_DISABLED}</li>
   * </ul>
	 */
	public final static short INFO_CONTINUOUS_PROCESS=(short)0x000D;

  /**
   * Constant indicating that the application requires Continuous Processing.
   * @since export file version 1.1
   */
	public final static byte CONTINUOUS_PROCESS_ENABLED=(byte)0x02;

  /**
   * Constant indicating that the application supports Interleaved Processing.
   * @since export file version 1.1
   */
	public final static byte CONTINUOUS_PROCESS_DISABLED=(byte)0x01;

	/**
	 * The requested information is the Update Counter. This is a READ ONLY
	 * information (i.e. cannot be set using {@link #setInfo}). <p>
   * 
   * This counter is automatically incremented each time this
	 * {@link GPCLRegistryEntry} is updated. This counter is used by CRS/CREL
	 * applications to synchronize with the current state of the GlobalPlatform
	 * registry.
	 */
	public final static short INFO_COUNTER_UPDATE=(short)0x000E;

	/**
	 * The requested information is the Display Required Indicator.  Used to know
	 * if the application can work depending on the availability of display
	 * capabilities.
   *
   * This information may have one of the following values:
   * <ul>
   * <li>{@link #DISPLAY_REQUIREMENT_YES}</li>
   * <li>{@link #DISPLAY_REQUIREMENT_NO}</li>
   * </ul>
	 */
	public final static short INFO_DISPLAY_REQUIREMENT=(short)0x000F;

  /**
   * Constant indicating that the application does not require the availability of
   * display capabilities (i.e. can work when the display is off or not available).
   * @since export file version 1.1
   */
	public final static byte DISPLAY_REQUIREMENT_NO=(byte)1;

  /**
   * Constant indicating that the application requires the availability of
   * display capabilities (i.e. cannot work when the display is off or not available).
   * @since export file version 1.1
   */
	public final static byte DISPLAY_REQUIREMENT_YES=(byte)0;

	/**
	 * The requested information is a policy restricting the joint usage of some
   * applications (i.e. content of INSTALL system parameter Tag 'A5').<p>
   *
   * This information is encoded as a sequence (possibly empty) of TLV
   * structures, each one representing an AID (tag '4F').<p>
   *
   * @since export file version 1.1
	 */
	public final static short INFO_POLICY_RESTRICTED_APPLICATIONS=(short)0x0010;
	
	/**
	 * The requested information is a Recognition Algorithm to be used for
	 * Implicit Selection (i.e. content of INSTALL system parameter Tag '83').<p>
	* @since export file version 1.1
	 */
	public final static short INFO_IMPLICIT_SELECTION_RECOGNITION_ALGORITHM=(short)0x0011;

	/**
	 * The requested information is the Selection Priority encoded as a single
	 * unsigned byte. This is a READ ONLY information (i.e. cannot be set using
	 * {@link #setInfo}).<p>
	 *
	 * The Selection Priority is calculated based on the absolute position of the
	 * Contactless Application's entry in the GlobalPlatform Registry, and the
	 * Volatile Priority, if assigned.
	 *
   * @since export file version 1.1
	 */
	public final static short INFO_SELECTION_PRIORITY=(short)0x0012;

  /**
   * The requested information is the AID of the Group Head Application
   * (i.e. content of INSTALL system parameter Tag 'A0') if this entry
   * correspond to a Member Application. This is a READ ONLY information
   * (i.e. cannot be set using {@link #setInfo}).
	 *
   * This information is encoded as a single TLV structure (tag '4F')
   * encapsulating an AID value.<p>
   *
   * @see #isGroupMember
   * @since export file version 1.1
   */
	public final static short INFO_GROUP_HEAD_APPLICATION=(short)0x0013;

	/**
	 * The requested information is the transient Mode Flag associated with a
	 * specific System Code as part of Type F Protocol anti-collision
	 * process. <p>
   *
   * This data does not belong to the set of persistent Type F protocol
	 * parameters but is a transient protocol data that must be accessed by Type F
	 * Applets during a contactless transaction.<p>
   *
   * This information is encoded as the concatenation of a 2-byte System Code
   * and a 1-byte Mode Flag. The Mode Flag may have one of the following values:<ul>
   * <li>{@link #PROTOCOL_TYPE_F_MODE_FLAG_TRUE}
   * <li>{@link #PROTOCOL_TYPE_F_MODE_FLAG_FALSE}
   * </ul>
   *
   * @since export file version 1.2
	 */
	public final static short INFO_PROTOCOL_TYPE_F_MODE_FLAG=(short)0x0014;

  /**
   * Constant indicating that a Type F Mode Flag is set to <code>true</code>.
   * @since export file version 1.2
   */
	public final static byte PROTOCOL_TYPE_F_MODE_FLAG_TRUE=(byte)0x01;

  /**
   * Constant indicating that a Type F Mode Flag is set to <code>false</code>.
   * @since export file version 1.2
   */
	public final static byte PROTOCOL_TYPE_F_MODE_FLAG_FALSE=(byte)0x00;

	/**
	 * Retrieves the contents of an Application Information stored by this entry.
	 * 
	 * Returned data is formatted as specified for the INSTALL system parameter relating to the specified INFO_XX constant.
	 *
	 * @param buffer where requested information shall be written. For the coding of the data
	 * see the documentation of the constants INFO_XX.
	 * 
	 * @param offset within <code>buffer</code>, where requested information shall be written.
	 * 
	 * @param info is any constant with name INFO_XX which is defined in this interface.
	 * 
	 * @return (<code>offset</code> + length of data written in <code>buffer</code>) 
	 *
	 * @exception ISOException with reason code
	 * <ul>
	 * <li> <code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller <ul>
	 * <li>is not the Application represented by this entry, and</li>
   * <li>does not have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege and
	 * <li>does not have the {@link #PRIVILEGE_GLOBAL_REGISTRY} privilege, and</li>
	 * <li>is not a Security Domain directly associated to the Application represented by this entry, and</li>
	 * <li>is not a CREL Application registered in the CREL list of the Application represented by this entry.</li>
	 * </ul></li>
	 * <li><code>ISO7816.SW_WRONG_DATA</code> if the value of <code>info</code> is unknown.</li>
	 * <li><code>ISO7816.SW_RECORD_NOT_FOUND</code> if the info is not present.</li>
	 * </ul>
	 * @exception ArrayIndexOutOfBoundsException
	 *             if storing the Application Information bytes would cause
	 *             access outside array bounds or the <code>offset</code> is
	 *             negative.
	 * @exception NullPointerException
	 *             if <code>buffer</code> is <code>null</code>
	 */
	public short getInfo(byte[] buffer, short offset, short info);
	
	/**
	 * Updates the contents of an Application Information stored by this entry.
	 * 
	 * Received data is expected to be formatted as specified for the INSTALL system parameter relating to the specified INFO_XX constant.
	 *
	 * @param buffer contains the updated information. For the coding of the data
	 * see the documentation of the constants INFO_XX.
	 * @param offset within <code>buffer</code>, where updated information can be found
	 * @param length of the updated information
	 * @param info is any constant with name INFO_XX which is defined in this interface.
	 *            
	 * @return (<code>offset</code> + <code>length</code>)
	 * 
	 * @exception ISOException with reason code
	 *  <ul>
	 *  <li> <code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller
	 *    <ul>
	 *    <li>is not the Application represented by this entry, and</li>
	 *    <li>is not a Security Domain directly or indirectly associated to the Application represented by this entry.</li>
	 *    </ul>
	 *  <li> <code>ISO7816.SW_WRONG_DATA</code> if the value is not correctly formatted.</li>
	 *  <li> <code>ISO7816.SW_WRONG_DATA</code> if the value of <code>info</code> is unknown.</li>
	 *  <li> <code>ISO7816.SW_FUNC_NOT_SUPPORTED</code> if the <code>info</code> is not supported by the platform (e.g. unsupported RF protocol data).</li>
	 *  <li> <code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the <code>info</code> indicates a READ ONLY information.</li>
	 *  </ul>
	 *  
	 * @exception ArrayIndexOutOfBoundsException
	 *             if storing the Application Information bytes would cause
	 *             access outside array bounds or the <code>offset</code> is
	 *             negative.
	 * @exception NullPointerException
	 *             if <code>buffer</code> is <code>null</code>
	 */
	public short setInfo(byte[] buffer, short offset,short length, short info);

	/**
   * Retrieves the list of currently activated Contactless Applications whose RF
	 * parameters are conflicting with the RF parameters of the Contactless
	 * Application represented by this entry.<p>
	 * 
   * NOTE: For Applications belonging to an Application Group, Contactless
   * Protocol Parameters (used to perform conflict detection) are provided by
   * the Head Application of that group, even if the Member Application was
   * personalized with its own parameter values.<p>
   *
	 * @param oEntry {@link GPCLRegistryEntry}
   *
   * @return
	 * <ul>
	 * 	<li>If the list is empty, this method returns null.</li>
	 * 	<li>If <code>oEntry</code> is null, this method returns the first entry of the list.</li>
	 * 	<li>If <code>oEntry</code> is not null and belongs to the list, </li>
   *  this method returns the next entry of the list following <code>oEntry</code>.</li>
	 * 	<li>If <code>oEntry</code> is not null and points to the last entry of the list, this method returns null.</li>
	 * 	<li>If <code>oEntry</code> is not null but does not belong to the list, this method returns null.</li>
	 * </ul>
	 * 
	 * @exception ISOException with reason code
	 * <ul>
	 * <li> <code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the
	 * caller does not have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege.
	 * </ul>
	 * @exception SecurityException if <code>oEntry</code> is not a JCRE-owned instance of {@link GPCLRegistryEntry}.
   * @exception SystemException with reason code <code>SystemException.ILLEGAL_USE</code> if the specified <code>oEntry</code> corresponds to an application that has been deleted.
	 */
	public GPCLRegistryEntry getNextConflictingApplication(GPCLRegistryEntry oEntry);
	
	/**
	 * Sets up this entry as a member of the Application
	 * Group with specified Head Application.
	 *
	 * @param oHead Head Application's AID. The <code>null</code> value shall be
	 * used to leave any Application Group to which this <code>entry</code> may
	 * belong.
	 * 
	 * @exception ISOException with reason code
	 * <ul>
	 * <li> <code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> <ul>
	 * <li>if the caller of this method is not the Application represented by this entry, or</li>
	 * <li>if the caller of this method is a Head application, or</li>
	 * <li>if the <code>oHead</code> parameter is not <code>null</code> and this entry is already associated to a Head Application, or</li>
	 * <li>if the <code>oHead</code> parameter is not <code>null</code> and cannot be found in the GlobalPlatform registry, or</li>
	 * <li>if the <code>oHead</code> parameter is not <code>null</code> and does not identify a Head Application, or</li>
	 * <li>if the AID of this entry cannot be found in the Group Authorization List of the Head application identified by the <code>oHead</code> parameter.</li>
	 * </ul>
	 */
	public void joinGroup(AID oHead);

	/**
	 * Retrieves the list of member Applications belonging to the Application Group represented by this entry (Head Application).
	 * 
	 * @param oEntry {@link GPCLRegistryEntry}

   * @return
	 * <ul>
   *  <li>If this entry does not represent a Head Application, this method returns null.</li>
	 * 	<li>If the list is empty, this method returns null.</li>
	 * 	<li>If <code>oEntry</code> is null, this method returns the first entry of the list.</li>
	 * 	<li>If <code>oEntry</code> is not null and belongs to the list, </li>
   *  this method returns the next entry of the list following <code>oEntry</code>.</li>
	 * 	<li>If <code>oEntry</code> is not null and points to the last entry of the list, this method returns null.</li>
	 * 	<li>If <code>oEntry</code> is not null but does not belong to the list, this method returns null.</li>
	 * </ul>
	 * 
	 * @exception ISOException with reason code
	 * <ul>
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller
   *  <ul>
	 *   <li>does not have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege and
	 *   <li>is not a CREL Application associated to this entry and
	 *   <li>is not the Application represented by this entry.
	 *  </ul>
	 * </ul>
	 * @exception SecurityException if <code>oEntry</code> is not a JCRE-owned instance of {@link GPCLRegistryEntry}.
   * @exception SystemException with reason code <code>SystemException.ILLEGAL_USE</code> if the specified <code>oEntry</code> corresponds to an application that has been deleted.
	 */
	public GPCLRegistryEntry getNextGroupMember(GPCLRegistryEntry oEntry);
	
	/**
	 * Adds an AID to the Group Authorization List. The application represented by
	 * this entry shall be either a Head Application or a Standalone
	 * Application.
	 * 
	 * @param baAID byte array containing the AID value
	 * @param offsetAID start offset of the AID value
	 * @param lengthAID length of the AID value
	 * 
	 * @exception ISOException with reason code
	 * <ul>
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> <ul>
   * <li>if the application represented by this entry is already a Group Member
   * (i.e. associated with a Head Application), or</li>
   * <li>if the caller of this method is neither the Application represented by
	 * this entry nor a Security Domain directly or indirectly associated with the
	 * Application represented by this entry.</li>
	 * </ul></li>
   * </ul>
	 */
	public void addToGroupAuthorizationList(byte[] baAID, short offsetAID, short lengthAID);

	/**
	 * Removes an AID from the Group Authorization List. If the application
	 * represented by this entry is not a Head Application, or the
	 * specified AID does not belong to the Group Authorization List, then the
	 * method simply returns with no error.
	 *
	 * @param baAID byte array containing the AID value
	 * @param offsetAID start offset of the AID value
	 * @param lengthAID length of the AID value
	 * 
	 * @exception ISOException with reason code <ul>
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code><ul>
   * <li>if the caller of this method is neither the Application represented by
	 * this entry nor a Security Domain directly or indirectly associated with the
	 * Application represented by this entry.</li>
	 * </ul></li>
   * </ul>
	 */
	public void removeFromGroupAuthorizationList(byte[] baAID, short offsetAID, short lengthAID);

	/**
	 * Sets the permanent order of the GlobalPlatform Registry used by application selection by AID matching.<p>
   *
   * If this entry is a Head Application, all Group Members are moved together with the Head Application in the same order.
   *
	 * @param highestLowest 
	 * <ul>
	 * 	<li>true: this entry (and associated Group Members, possibly) acquires highest priority for partial selection (i.e. first selected).</li>
	 * 	<li>false: this entry (and associated Group Members, possibly) acquires lowest priority for partial selection (i.e. last selected).</li>
	 * </ul>
	 * 
	 * @exception ISOException with reason code<ul>
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller does not have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege.</li>
	 * </ul>
	 */
	public void setPartialSelectionOrder(boolean highestLowest);
	
	/**
	 * Retrieves the list of CREL Applications referenced by this entry.
	 * 
	 * @param oEntry {@link GPCLRegistryEntry}
	 * 
	 * @return 
	 * <ul> 
	 * 	<li>If the list is empty, this method returns null.</li>
	 * 	<li>If <code>oEntry</code> is null, this method returns the first entry of the list.</li>
	 * 	<li>If <code>oEntry</code> is not null and belongs to the list, </li>
   *  this method returns the next entry of the list following <code>oEntry</code>.</li>
	 * 	<li>If <code>oEntry</code> is not null and points to the last entry of the list, this method returns null.</li>
	 * 	<li>If <code>oEntry</code> is not null but does not belong to the list, this method returns null.</li>
	 * </ul>
   *
	 * @exception ISOException with reason code<ul>
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code><ul>
   *   <li>if the caller does not have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege and
	 *   <li>is not the Application represented by this entry.
	 *   </ul>
	 * </ul>
	 * @exception SecurityException if <code>oEntry</code> is not a JCRE-owned instance of {@link GPCLRegistryEntry}.
   * @exception SystemException with reason code <code>SystemException.ILLEGAL_USE</code> if the specified <code>oEntry</code> corresponds to an application that has been deleted.
	 */
	public GPCLRegistryEntry getNextCRELApplication(GPCLRegistryEntry oEntry);
	
	/**
	 * Adds an AID to the list of CREL Applications referenced by this entry. 
	 * 
	 * @param baAID byte array containing the AID value
	 * @param offsetAID start offset of the AID value
	 * @param lengthAID length of the AID value
	 * 
	 * @exception ISOException with reason code
	 * <ul> 
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller of this method <ul>
	 * <li>is not a Security Domain directly or indirectly associated with the Application represented by this entry, and</li>
	 * <li>is not the Application represented by this entry.</li>
	 * </ul></li>
	 * </ul>
	 */
	public void addToCRELApplicationList(byte[]baAID, short offsetAID, short lengthAID);

	/**
	 * Removes an AID from the list of CREL Applications referenced by this entry. 
	 * 
	 * @param baAID byte array containing the AID value
	 * @param offsetAID start offset of the AID value
	 * @param lengthAID length of the AID value
	 * 
	 * @exception ISOException  with reason code
	 * <ul> 
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller of this method
	 * <ul>
	 * <li>is not a Security Domain directly or indirectly associated with the Application represented by this entry, and
	 * <li>is not the Application represented by this entry.
	 * </ul>
	 * </ul>
	 */
	public void removeFromCRELApplicationList(byte[]baAID, short offsetAID, short lengthAID);
	
	/**
	 * Retrieves the list of Applications that reference this entry as one of their CREL Applications.
	 * 
	 * @param oEntry {@link GPCLRegistryEntry}
   *
   * @return
	 * <ul> 
	 * 	<li>If the list is empty, this method returns null.
	 * 	<li>If <code>oEntry</code> is null, this method returns the first entry of the list.
	 * 	<li>If <code>oEntry</code> is not null and belongs to the list, 
   *  this method returns the next entry of the list following <code>oEntry</code>.
	 * 	<li>If <code>oEntry</code> is not null and points to the last entry of the list, this method returns null.
	 * 	<li>If <code>oEntry</code> is not null but does not belong to the list, this method returns null.
	 * </ul>
   *
	 * @exception ISOException with reason code
	 * <ul>
	 * <li><code>ISO7816.SW_CONDITIONS_NOT_STATISFIED</code> if the caller <ul>
	 *   <li>does not have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege and
   *   <li>is not the Application represented by this entry.
   *   </ul>
	 * </ul>
	 * @exception SecurityException if <code>oEntry</code> is not a JCRE-owned instance of {@link GPCLRegistryEntry}.
   * @exception SystemException with reason code <code>SystemException.ILLEGAL_USE</code> if the specified <code>oEntry</code> corresponds to an application that have been deleted.
	 */
	public GPCLRegistryEntry getNextReferencingApplication(GPCLRegistryEntry oEntry);

	/**
	 * Whether this entry represents a Group Head.
	 * 
	 * @return <code>true</code> if the Application represented by this entry is a Group Head, 
	 * <code>false</code> otherwise (either a Group Member or a standalone Application).
	 * 
	 * @exception ISOException with reason code <ul>
	 * 	<li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller <ul>
	 *    <li>does not have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege and
	 *    <li>does not have the {@link #PRIVILEGE_GLOBAL_REGISTRY} privilege and
	 *    <li>is not a CRELApplication associated to this entry and
	 *    <li>is not the Application represented by this entry.
   *    </ul>
   * </ul>
	 */
	public boolean isGroupHead();

	/**
	 * Whether this entry represents a Group Member.
	 * 
	 * @return <code>true</code> if the Application represented by this entry is a Group Member, 
	 * 		   <code>false</code> otherwise (either a Group Head or a standalone Application).
	 * 
	 * @exception ISOException with reason code <ul>
	 * 	<li><code>ISO7816.SW_CONDITIONS_NOT_SATISFIED</code> if the caller<ul>
	 *    <li>does not have the {@link #PRIVILEGE_CONTACTLESS_ACTIVATION} privilege and
	 *    <li>does not have the {@link #PRIVILEGE_GLOBAL_REGISTRY} privilege and
	 *    <li>is not a CRELApplication associated to this entry and </li>
	 *    <li>is not the Application represented by this entry.</li>
   *    </ul></li>
   * </ul>
	 */
	public boolean isGroupMember();

}
