/**
 * Provides a framework of classes and interfaces related to contactless
 * services defined for smart cards based on GlobalPlatform specifications.
 * @version This documentation describes API elements and behaviors associated
 * with version 1.3 of this package export file.
 */
@APISourcer_Package_Description(
    AID="A00000015102",
    version="1.3",
    jcVersion="2.2",
    expmap=true,
    expmapFile="org/globalplatform/contactless/javacard/contactless_map.exp",
    intSupport=false,
    stringSupport=false)
package org.globalplatform.contactless;
import com.oberthur.apisourcer.annotation.APISourcer_Package_Description;

