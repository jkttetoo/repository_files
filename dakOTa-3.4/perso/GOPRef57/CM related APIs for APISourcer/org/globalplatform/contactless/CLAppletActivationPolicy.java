
package org.globalplatform.contactless;

import javacard.framework.JCSystem;
import javacard.framework.Shareable;

/**
 * An Application may implement and expose this interface to indicate whether it
 * can be activated or not and to return a reason code together with the list of
 * Applications preventing the activation. This interface allows an application
 * provider to implement a specific Activation Policy for its application.<p>
 *
 * A reason code is coded on 2 bytes. The range from '0000' to '7FFF' is reserved for GlobalPlatform. <p>
 *
 * Such an application may expose the <code>CLAppletActivationPolicy</code>
 * interface object(s) through {@link
 * javacard.framework.Applet#getShareableInterfaceObject(javacard.framework.AID,
 * byte)} with the parameter set to {@link
 * GPCLSystem#GPCL_CL_APPLICATION_ACTIVATION_POLICY}.<p>
 * 
 * NOTE: A client Application should never store a reference to an object
 * implementing this interface, as this would prevent the deletion of the
 * Application owning this object.<p>
 *
 * @since <ul>
 * <li>export file version 1.2: initial version.
 * <li>export file version 1.3: updated behavior of method {@link #getNextApplicationConflictInfo}.
 * </ul>
 */
public interface CLAppletActivationPolicy extends Shareable 
{
	/**
	 * The Application cannot be activated because its business policy conflicts
	 * with some Applications that are already activated.
	 */
	public static final short REASON_BUSINESS_POLICY_CONFLICT = (short)0x0001;

	/**
	 * A successful user verification (ex: PIN validation) is required prior to the Application being activated.
	 */
	public static final short REASON_USER_NOT_VERIFIED = (short)0x0002;

	/**
	 * The Application cannot be activated for unspecified reasons.
	 */
	public static final short REASON_UNSPECIFIED = (short)0x0003;
	
  /**
   * Checks whether the Application exposing this interface accepts an
   * activation request for itself or, if the Application implementing this
   * interface is a CREL Application, for a referencing Application.<p>
   *
   * If a request is made to activate an Application, and this Application
   * exposes this interface, then the OPEN shall invoke this method to check
   * whether the Application accepts the activation request, except if the
   * activation request originates from the Application itself. The OPEN shall
   * call this method before processing the Protocol Parameter Conflict
   * Detection Procedure.  In the case of an Application Group, the OPEN is only
   * expected to call this method on the Head Application.<p>
   *
   * Depending on the application provider policy, if the Application exposing
   * this interface is a Head Application, then the implementation of this
   * method may check whether associated Member Applications (also implementing
   * and exposing this interface) accept the activation request.  If one of its
   * Member Applications rejects the activation request (because of some
   * proprietary policy), then the Head Application may also reject the
   * activation request.<p>
   *
   * Depending on the application provider policy, if the Application exposing
   * this interface is referencing CREL Applications, then the implementation of
   * this method may check whether referenced CREL Applications (also
   * implementing and exposing this interface) accept the activation request. If
   * one of the CREL Applications rejects the activation request (because of
   * some proprietary policy), then the Application may reject the activation
   * request.<p>
   *
   * The {@link JCSystem#getPreviousContextAID()} method allows the Application
   * exposing this interface to discover the caller of this method:<ul>
   *
   * <li>If the {@link JCSystem#getPreviousContextAID()} method returns
   * <code>null</code>, then this method is invoked by the OPEN, otherwise it is
   * invoked by another Application.
   *
   * <li>If this method is invoked by the OPEN or the Application implementing
   * this interface is NOT a CREL Application, then the Application shall check
   * whether it can be activated or not. The Application should not modify its
   * Contactless Activation State.
   *
   * <li>If the Application implementing this interface is a CREL Application and
   * this method is invoked by another Application, then the CREL Application
   * checks whether some policy prevents the activation of the caller
   * Application.
   *
   * </ul>
   *
   * When this method is invoked, any policy conflict information previously
   * computed by the Application implementing this interface shall be
   * discarded. When rejecting the activation request, this method shall build
   * new policy conflict information that may be subsequently retrieved using
   * the {@link #getNextApplicationConflictInfo} method.<p>
   *
   * @return <code>true</code> if the Application accepts the activation
   * request, <code>false</code> otherwise.
   */
  public boolean acceptActivation();

  /**
   * Retrieves TLV-encoded policy conflict information, which consists of one or
   * several conflict reason codes and, for each of them, of an optional list of currently
   * activated Applications conflicting with the Application implementing this
   * interface for that particular reason.<p>
   *
   * This method shall write TLV-encoded policy conflict information in the
   * <code>out</code> buffer. Tag '48' shall be used to indicate a 2-byte
   * conflict reason code and tag '4F' shall be used to indicate the AID of a
   * conflicting Application. One or several conflict reason codes may be
   * present, each of them optionally followed by a list of Applications conflicting for
   * that particular reason.<p>
   *
   * Example: <code>'<b>48 02 xx xx</b> 4F (L) xx ... xx 4F (L) xx ... xx <b>48 02 xx xx</b> 4F (L) xx ... xx 4F (L) xx ... xx 4F (L) xx ... xx ... ...'</code><p>
   *
   * This method shall be invoked one or several times until all data (policy
   * conflict information) have been retrieved. The following behaviors shall be
   * implemented:<ul>

   * <li>If there are no remaining data, then this method shall return 0,
   * indicating to the caller of this method that all data were retrieved, and
   * all policy conflict information shall be discarded. In particular, this
   * behavior shall apply when all remaining data were retrieved by a previous
   * call to this method, or when no prior call to the {@link #acceptActivation}
   * method was performed, or when the call to the {@link #acceptActivation}
   * method returned <code>true</code> (i.e. no conflict detected).

   * <li>If there are remaining data but none of these data can be consistently
   * written to the <code>out</code> buffer without causing an array out of
   * bounds access, then this method shall throw an {@link
   * ArrayIndexOutOfBoundsException}, indicating to the caller of this method
   * that more buffer space is required to retrieve remaining data.

   * <li>If there are remaining data and at least some of these data can be
   * consistently written to the <code>out</code> buffer without causing an
   * array out of bounds access, then this method shall write as much data as
   * possible without causing an array out of bounds access and shall return the
   * number of bytes written to the <code>out</code> buffer. If all remaining
   * data were actually written the <code>out</code> buffer, then a subsequent
   * call to this method (returning 0) will be needed for the caller to discover
   * that all data were retrieved.

   * </ul>
   *
   * Depending on the application provider policy, if the Application exposing
   * this interface is a Head Application, then the implementation of this
   * method may retrieve conflict information from associated Member
   * Applications (also implementing and exposing this interface) that
   * previously rejected the activation request.<p>
   *
   * Depending on the application provider policy, if the Application exposing
   * this interface is referencing CREL Applications, then the implementation of
   * this method may retrieve conflict information from the CREL Applications
   * (also implementing and exposing this interface) that previously rejected
   * the activation request.<p>
   *
   * @param out byte array buffer where policy conflict information shall be
   * written. This buffer must be global.
   * @param outOffset starting offset within the <code>out</code> byte array. 
   *
   * @return the number of bytes written in <code>out</code> buffer.
   *
   * @exception ArrayIndexOutOfBoundsException If writing remaining data (even
   * partially) to the <code>out</code> buffer would cause an array out of
   * bounds access.
   *
   * @since <ul>
   * <li>export file version 1.2: initial version.
   * <li>export file version 1.3: the list of conflicting Application(s) is now optionally present in the data generated by this method.
   * </ul>
   */
  public short getNextApplicationConflictInfo(byte[] out,  short outOffset);

}
