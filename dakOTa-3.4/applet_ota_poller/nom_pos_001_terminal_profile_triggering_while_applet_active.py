import sys
from Oberthur import *
sys.path.insert(0, "../../")
sys.path.insert(0, "../../lib")
import configparser
Constants.config = configparser.ConfigParser()
Constants.config.read('esim_tool_configuration.ini')
from Oberthur import *
from Mobile import *
from util import *
import unittest
import const
import configparser
import model, const


__author__      = 'Muhammad Hasbi Firmansyah'
__maintainer__  = 'Muhammad Hasbi Firmansyah'
__status__      = 'dev'
__version__     = '0.1.0'
__email__       = 'm.firmansyah@oberthur.com'


OTA_POLLER	= model.OtaPoller()
EUICC 		= model.Euicc()
GP 			= model.Gp()
AMDB		= model.SCP81()

displayAPDU(True)
SetLogLevel('debug')
print(GetAllReaders())

SetCurrentReader('eSIM 3.1.rc11')
# SetCurrentReader('eSIM 3.1.rc9')
# SetCurrentReader('DE620 Contact Reader')

EUICC.init()
data, sw = OTA_POLLER.set_applet_config(interface='applet',applet_state=True, profile_download_event=True, retry_duration=60, retry_number=2)
assert sw == '9000', 'Expected to be 9000'

sw = EUICC.init()
AMDB.remote_management(GP.audit_applet_list, pull=sw, apdu_format='definite')
