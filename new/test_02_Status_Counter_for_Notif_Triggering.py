import pytest
import datetime
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *

global gui

gui = False
PiPort = '/dev/ttyACM0'
openSerial()

# Detail Envelope Server
# OA: +6281298265649
# SPI KIC KID: 1639 12 12
# KiC Key: F8195839102919DEAB908947198438FA
# KiD Key: F8195839102919DEAB908947198438FA
# Note: For Initial Condition if the process is failed, please do manually (perso the card via PCOM)

def CSIM(csimApdu):
    CSIMCommand(csimApdu,'_outAUTO_',gui)

def APDU_PSM(apdu,SW='9000'):
#    Open Channel
    CSIM('0070000001')
    assert csim_result[0][-4:] == '9000'
#    Select PSM Applet
    CSIM(list_csim[12])
    assert csim_result[0][-4:] == '9000'    
#    Apdu
    CSIM(apdu)
    assert csim_result[0][-4:] == SW    
#    Close Channel
    CSIM('0070800100')
    assert csim_result[0][-4:] == '9000'    
#    Select USIM AID
    CSIM(list_csim[16])
    assert (csim_result[0][-4:] == '6127' or csim_result[0][-4:] == '9000')

print('# ==================================')
print('# Status Counter for notif Trigger =')
print('# ==================================')
def test_DK4809():
    time.sleep(30)     
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4809\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set Poll Intervall 001E and Fallback 000A
    APDU_PSM(list_csim[13] + '0A37088102001E8202000A')
#     Check Connectivity
    APDU_PSM(list_csim[15])    
#     Set event counter status 00
# Counter: 0000000009
# Data: 80E2880006DF6D03400100
    CSIM('D14F8202838106069126180100008B41400D91261883388832F97F16000000000000282D02700000281516391212000001FCBFF09F60D12F48ED7A7EB2E2C5521A7D72B79D329DD0AA8409E8808753334E')
#     Check Proprietary
    APDU_PSM(list_csim[14])

@pytest.mark.skip
def test_DK4810():
    time.sleep(30)     
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4810\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set event counter status 01
# Counter: 0000000009
# Data: 80E2880006DF6D03400101
    CSIM('D14F8202838106069126180100008B41400D91261883388832F97F16000000000000282D0270000028151639121200000152A38EF9AF197BE30FCCAD2B413AC89131874038C709DF7E66D335ADAAC56DC7')
#     Check Proprietary
    APDU_PSM(list_csim[14])
    time.sleep(30)
    
#    Enable Profile 1000
# TAR: 000001
# Counter: 000000000C
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('D15F8202838106069126180100008B51400D91261883388832F97F16000000000000283D02700000381516391212000001CC425F5ACB4D4795CD50E503D419FA33BEE3FBF6DAFF6E4CF8DC3F646BAB0160B54F979251C0987574A5B295D5CAD53F')
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0007
# Counter: 000000000D
# Data: 80E28900073A08044E020007
    apdu = 'D14F8202838106069126180100008B41400D91261883388832F97F16000000000000282D027000002815163912120000014F53BC2655103972191CA21E25A8BE66BAA9545BC184087E8DAA1D64E87F368D'
    CSIMCommand(apdu, '_outAUTO_',gui)
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(15500)
#     fallback to 1100
    setOperator('Automatic')
        
def test_DK4811():
    time.sleep(30)     
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4811\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set event counter status 7F
# Counter: 0000000009
# Data: 80E2880006DF6D0340017F
    CSIM('D14F8202838106069126180100008B41400D91261883388832F97F16000000000000282D02700000281516391212000001BC7A565867BFD52EE136F0BF627F65EB4E2CBE036FC0B89E4FB21AF2AF46AFCF')
#     Check Proprietary
    APDU_PSM(list_csim[14])   
