import pytest
import datetime
import sys 
import os
sys.path.append(os.path.abspath("/home/pi/Documents/ATCA"))
from allFunctions import *
from allLists import *

global gui

gui = False
PiPort = '/dev/ttyACM0'
openSerial()

# Detail Envelope Server
# OA: +6281298265649
# SPI KIC KID: 1639 12 12
# KiC Key: F8195839102919DEAB908947198438FA
# KiD Key: F8195839102919DEAB908947198438FA
# Note: For Initial Condition if the process is failed, please do manually (perso the card via PCOM)

def CSIM(csimApdu):
    CSIMCommand(csimApdu,'_outAUTO_',gui)

def APDU_PSM(apdu,SW='9000'):
#    Open Channel
    CSIM('0070000001')
    assert csim_result[0][-4:] == '9000'
#    Select PSM Applet
    CSIM(list_csim[12])
    assert csim_result[0][-4:] == '9000'    
#    Apdu
    CSIM(apdu)
#    assert csim_result[0][-4:] == SW    
#    Close Channel
    CSIM('0070800100')
    assert csim_result[0][-4:] == '9000'    
#    Select USIM AID
    CSIM(list_csim[16])
    assert (csim_result[0][-4:] == '6127' or csim_result[0][-4:] == '9000')
        
def AuditISDP():
#    Open Channel    
    CSIM('0070000001')
    assert csim_result[0][-4:] == '9000'     
#    Select LM Applet
    CSIM(list_csim[3])
    assert csim_result[0][-4:] == '9000'     
#    Get eUICC Info
    CSIM(list_csim[2])
    assert csim_result[0][-4:] == '9000'
#    Close Channel
    CSIM('0070800100')
    assert csim_result[0][-4:] == '9000'      
#    Select USIM AID    
    CSIM(list_csim[16])
    assert (csim_result[0][-4:] == '6127' or csim_result[0][-4:] == '9000')    

def test_Date():
    x = datetime.datetime.now()
    LogFiles('_outAUTO_','# ============================\n')
    LogFiles('_outAUTO_','# '+str(x)+' =\n')
    LogFiles('_outAUTO_','# ============================\n')
    
print('# ==========================')
print('# Test Suite APFv2 Setting =')
print('# ==========================')   
def test_DK4801():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4801\n')
    LogFiles('_outAUTO_','# =======\n')     
#     Check Connectivity
    APDU_PSM(list_csim[15])

def test_DK4802():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4802\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set Fallback 0000
    APDU_PSM(list_csim[13] + '0A37088102001E82020000','6A80')
#     Check Connectivity
    APDU_PSM(list_csim[15])
    
def test_DK4803():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')     
    LogFiles('_outAUTO_','# DK-4803\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set Fallback 0001
    APDU_PSM(list_csim[13] + '0A37088102001E82020001')
#     Check Connectivity
    APDU_PSM(list_csim[15])

#    Enable Profile 1000
# TAR: 000001
# Counter: 0000000001
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D027000003815163912120000015F36E58D814E1CD1324A3DE06725EDF61A37C184EE8AC6339B1C1015A3BC04119B33946D535422119BB5B868695DB34D')
    assert csim_result[0][-4:] == '914C'
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0001
# Counter: 0000000002
# Data: 80E28900073A08044E020001
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D027000002815163912120000012F18897B550314C3560E64E71088731C5362CFF05B8C0A84E51C99479A166D92')
#    assert csim_result[0][-4:] == '914C'
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(120)
#     fallback to 1100
    setOperator('Automatic')
    time.sleep(30)

# Wait for 30s until switch on profile 1100 and receive notif profile changed
# Send notif confirmation 0002
# Counter: 0000000003
# Data: 80E28900073A08044E020002
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D027000002815163912120000015B32C31B03C5D8864E93E05893A6994041C3C9439804D17A3B2CB185CAC8DAD8')
#    assert csim_result[0][-4:] == '914C'
#     audit ISDP
    AuditISDP()
    
def test_DK4804():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4804\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set Fallback FFFF
    APDU_PSM(list_csim[13] + '0A37088102001E8202FFFF')
#     Check Connectivity
    APDU_PSM(list_csim[15])
    
def test_DK4805():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4805\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set Poll Intervall 0000 and fallback FFFF
    APDU_PSM(list_csim[13] + '0A37088102000082027FFF')
#     Check Connectivity
    APDU_PSM(list_csim[15])
    
def test_DK4806():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4806\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set Poll Intervall 0001 and fallback 000A
    APDU_PSM(list_csim[13] + '0A3708810200018202000A')
#     Check Connectivity
    APDU_PSM(list_csim[15])

#    Enable Profile 1000
# TAR: 000001
# Counter: 0000000004
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D02700000381516391212000001E148F2F63071FE2300A47F82BFFAC3AEEFC176D8E386606E1249769492D5E0D8E87A3BA180D8A9D659BA88469ADBFD23')
#    assert csim_result[0][-4:] == '914C'
    time.sleep(40)

# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0003
# Counter: 0000000005
# Data: 80E28900073A08044E020003
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001155AD711058E8CF48AD2F3E3ECCDBA73B416143DEFE6A04679F2BB87CE0A5C24')
#    assert csim_result[0][-4:] == '914C'
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(120)
    
#     fallback to 1100
    setOperator('Automatic')
    time.sleep(30)

# Wait for 30s until switch on profile 1100 and receive notif profile changed
# Send notif confirmation 0004
# Counter: 0000000006
# Data: 80E28900073A08044E020004
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D027000002815163912120000012EB4A57FC85992BE56B1A31FE5EE363447FC2DBF2279D67DB3BC0A677B023821')
#    assert csim_result[0][-4:] == '914C'
#     audit ISDP
    AuditISDP()

@pytest.mark.skip
def test_DK4807():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4807\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set Poll Intervall 3BC4 and fallback 0001
    APDU_PSM(list_csim[13] + '0A370881023BC482020001')
#     Check Connectivity
    APDU_PSM(list_csim[15])

#    Enable Profile 1000
# TAR: 000001
# Counter: 0000000007
# Data: 
# 80E28800153A03124F10A0000005591010FFFFFFFF8900001000
    CSIM('80C2000061D15F8202838106069126180100008B51400D91261892285646F97F16000000000000283D027000003815163912120000015D6EE7E9C75884058CA4B213A622284597EDF054E857B692A456375A574F541ABDFF9F5463E12B34594B3CEC448326E6')
#    assert csim_result[0][-4:] == '914C'
    time.sleep(40)
    
# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0005
# Counter: 0000000008
# Data: 80E28900073A08044E020005
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D02700000281516391212000001DA93541029B7BF0826C6EF327CE20190456AFC83562D21EC417EE02C1F2F3F4A')
#    assert csim_result[0][-4:] == '914C'
#     set network to limited service
    setOperator('Manual - INDOSAT')
    time.sleep(15500)
#     fallback to 1100
    setOperator('Automatic')
    time.sleep(40)

# Wait for 40s until switch on profile 1000 and receive notif profile changed
# Send notif confirmation 0006
# Counter: 0000000009
# Data: 80E28900073A08044E020006
    CSIM('80C2000051D14F8202838106069126180100008B41400D91261892285646F97F16000000000000282D0270000028151639121200000158722205BDAFF101DA64E624B086E7F2CB0E7203EED5C76561F52FD5FA7583D1')
#    assert csim_result[0][-4:] == '914C'
#     audit ISDP
    AuditISDP()
    
def test_DK4808():
    time.sleep(30)
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4808\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set Poll Intervall 3BC5 and fallback 0002
    APDU_PSM(list_csim[13] + '0A370881023BC582020002','6A80')
#     Check Connectivity
    APDU_PSM(list_csim[15])
    
def test_DK4832():
    time.sleep(30)    
    LogFiles('_outAUTO_','# =======\n')    
    LogFiles('_outAUTO_','# DK-4832\n')
    LogFiles('_outAUTO_','# =======\n')
#     Set APF with wrong TAG
    APDU_PSM(list_csim[13] + '0A37088102001E83020168') 
